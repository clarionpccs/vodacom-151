  MEMBER('cellmain.clw')

  INCLUDE('ABFILE.INC'),ONCE

  MAP
CELLMBCH:DctInit    PROCEDURE
CELLMBCH:DctKill    PROCEDURE
CELLMBCH:FilesInit  PROCEDURE
  END

Hide:Access:WAYBAWT  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:WAYBAWT  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:WAYITEMS CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:WAYITEMS CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:WAYLAWT  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:WAYLAWT  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:WAYSUND  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:WAYSUND  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:WAYJOBS  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:WAYJOBS  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:WAYSUND_TEMP CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:WAYSUND_TEMP CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:WAYCNR   CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:WAYCNR   CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:WAYBILLJ CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:WAYBILLJ CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:MANUFACT CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:MANUFACT CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:USMASSIG CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:USMASSIG CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:EXPWARPARTS CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:EXPWARPARTS CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:WEBJOB   CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
TryUpdate              PROCEDURE(),BYTE,PROC,DERIVED
Update                 PROCEDURE(),BYTE,PROC,DERIVED
                     END


Hide:Relate:WEBJOB   CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:WIPEXC   CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:WIPEXC   CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:USUASSIG CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:USUASSIG CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:DEFEDI   CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:DEFEDI   CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:WIPSCAN  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:WIPSCAN  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:REPMAIL  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:REPMAIL  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:PRIBAND_ALIAS CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:PRIBAND_ALIAS CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:SMSRECVD_ALIAS CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:SMSRECVD_ALIAS CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:SMSText_Alias CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:SMSText_Alias CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

CELLMBCH:DctInit PROCEDURE
  CODE
  Relate:WAYBAWT &= Hide:Relate:WAYBAWT
  Relate:WAYITEMS &= Hide:Relate:WAYITEMS
  Relate:WAYLAWT &= Hide:Relate:WAYLAWT
  Relate:WAYSUND &= Hide:Relate:WAYSUND
  Relate:WAYJOBS &= Hide:Relate:WAYJOBS
  Relate:WAYSUND_TEMP &= Hide:Relate:WAYSUND_TEMP
  Relate:WAYCNR &= Hide:Relate:WAYCNR
  Relate:WAYBILLJ &= Hide:Relate:WAYBILLJ
  Relate:MANUFACT &= Hide:Relate:MANUFACT
  Relate:USMASSIG &= Hide:Relate:USMASSIG
  Relate:EXPWARPARTS &= Hide:Relate:EXPWARPARTS
  Relate:WEBJOB &= Hide:Relate:WEBJOB
  Relate:WIPEXC &= Hide:Relate:WIPEXC
  Relate:USUASSIG &= Hide:Relate:USUASSIG
  Relate:DEFEDI &= Hide:Relate:DEFEDI
  Relate:WIPSCAN &= Hide:Relate:WIPSCAN
  Relate:REPMAIL &= Hide:Relate:REPMAIL
  Relate:PRIBAND_ALIAS &= Hide:Relate:PRIBAND_ALIAS
  Relate:SMSRECVD_ALIAS &= Hide:Relate:SMSRECVD_ALIAS
  Relate:SMSText_Alias &= Hide:Relate:SMSText_Alias

CELLMBCH:FilesInit PROCEDURE
  CODE
  Hide:Relate:WAYBAWT.Init
  Hide:Relate:WAYITEMS.Init
  Hide:Relate:WAYLAWT.Init
  Hide:Relate:WAYSUND.Init
  Hide:Relate:WAYJOBS.Init
  Hide:Relate:WAYSUND_TEMP.Init
  Hide:Relate:WAYCNR.Init
  Hide:Relate:WAYBILLJ.Init
  Hide:Relate:MANUFACT.Init
  Hide:Relate:USMASSIG.Init
  Hide:Relate:EXPWARPARTS.Init
  Hide:Relate:WEBJOB.Init
  Hide:Relate:WIPEXC.Init
  Hide:Relate:USUASSIG.Init
  Hide:Relate:DEFEDI.Init
  Hide:Relate:WIPSCAN.Init
  Hide:Relate:REPMAIL.Init
  Hide:Relate:PRIBAND_ALIAS.Init
  Hide:Relate:SMSRECVD_ALIAS.Init
  Hide:Relate:SMSText_Alias.Init


CELLMBCH:DctKill PROCEDURE
  CODE
  Hide:Relate:WAYBAWT.Kill
  Hide:Relate:WAYITEMS.Kill
  Hide:Relate:WAYLAWT.Kill
  Hide:Relate:WAYSUND.Kill
  Hide:Relate:WAYJOBS.Kill
  Hide:Relate:WAYSUND_TEMP.Kill
  Hide:Relate:WAYCNR.Kill
  Hide:Relate:WAYBILLJ.Kill
  Hide:Relate:MANUFACT.Kill
  Hide:Relate:USMASSIG.Kill
  Hide:Relate:EXPWARPARTS.Kill
  Hide:Relate:WEBJOB.Kill
  Hide:Relate:WIPEXC.Kill
  Hide:Relate:USUASSIG.Kill
  Hide:Relate:DEFEDI.Kill
  Hide:Relate:WIPSCAN.Kill
  Hide:Relate:REPMAIL.Kill
  Hide:Relate:PRIBAND_ALIAS.Kill
  Hide:Relate:SMSRECVD_ALIAS.Kill
  Hide:Relate:SMSText_Alias.Kill


Hide:Access:WAYBAWT.Init PROCEDURE
  CODE
  SELF.Init(WAYBAWT,GlobalErrors)
  SELF.Buffer &= wya:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(wya:WAYBAWTIDKey,'wya:WAYBAWTIDKey',1)
  SELF.AddKey(wya:AccountJobNumberKey,'wya:AccountJobNumberKey',0)
  SELF.AddKey(wya:JobNumberKey,'wya:JobNumberKey',0)
  SELF.LazyOpen = False
  Access:WAYBAWT &= SELF


Hide:Relate:WAYBAWT.Init PROCEDURE
  CODE
  Hide:Access:WAYBAWT.Init
  SELF.Init(Access:WAYBAWT,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:WAYBAWT.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:WAYBAWT &= NULL


Hide:Relate:WAYBAWT.Kill PROCEDURE

  CODE
  Hide:Access:WAYBAWT.Kill
  PARENT.Kill
  Relate:WAYBAWT &= NULL


Hide:Access:WAYITEMS.Init PROCEDURE
  CODE
  SELF.Init(WAYITEMS,GlobalErrors)
  SELF.Buffer &= wai:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(wai:RecordNumberKey,'wai:RecordNumberKey',1)
  SELF.AddKey(wai:WayBillNumberKey,'wai:WayBillNumberKey',0)
  SELF.AddKey(wai:WaybillLookupKey,'IsExchange - Ref_Number',0)
  SELF.LazyOpen = False
  Access:WAYITEMS &= SELF


Hide:Relate:WAYITEMS.Init PROCEDURE
  CODE
  Hide:Access:WAYITEMS.Init
  SELF.Init(Access:WAYITEMS,1)


Hide:Access:WAYITEMS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:WAYITEMS &= NULL


Hide:Access:WAYITEMS.PrimeFields PROCEDURE

  CODE
  wai:IsExchange = 0
  wai:IsReplenishment = 0
  PARENT.PrimeFields


Hide:Relate:WAYITEMS.Kill PROCEDURE

  CODE
  Hide:Access:WAYITEMS.Kill
  PARENT.Kill
  Relate:WAYITEMS &= NULL


Hide:Access:WAYLAWT.Init PROCEDURE
  CODE
  SELF.Init(WAYLAWT,GlobalErrors)
  SELF.Buffer &= wal:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(wal:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(wal:IMEINumberKey,'By IMEI Number',0)
  SELF.AddKey(wal:LOANRefNumberKey,'By Loan Ref Number',0)
  SELF.AddKey(wal:ReadyIMEINumberKey,'By IMEI Number',0)
  SELF.LazyOpen = False
  Access:WAYLAWT &= SELF


Hide:Relate:WAYLAWT.Init PROCEDURE
  CODE
  Hide:Access:WAYLAWT.Init
  SELF.Init(Access:WAYLAWT,1)


Hide:Access:WAYLAWT.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:WAYLAWT &= NULL


Hide:Relate:WAYLAWT.Kill PROCEDURE

  CODE
  Hide:Access:WAYLAWT.Kill
  PARENT.Kill
  Relate:WAYLAWT &= NULL


Hide:Access:WAYSUND.Init PROCEDURE
  CODE
  SELF.Init(WAYSUND,GlobalErrors)
  SELF.Buffer &= was:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(was:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(was:EnteredKey,'By Record Number',0)
  SELF.AddKey(was:DescriptionKey,'By Description',0)
  SELF.LazyOpen = False
  Access:WAYSUND &= SELF


Hide:Relate:WAYSUND.Init PROCEDURE
  CODE
  Hide:Access:WAYSUND.Init
  SELF.Init(Access:WAYSUND,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:WAYBILLS)


Hide:Access:WAYSUND.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:WAYSUND &= NULL


Hide:Relate:WAYSUND.Kill PROCEDURE

  CODE
  Hide:Access:WAYSUND.Kill
  PARENT.Kill
  Relate:WAYSUND &= NULL


Hide:Access:WAYJOBS.Init PROCEDURE
  CODE
  SELF.Init(WAYJOBS,GlobalErrors)
  SELF.FileName &= glo:FileName
  SELF.Buffer &= wayjob:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(wayjob:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(wayjob:JobNumberKey,'By Job Number',0)
  SELF.LazyOpen = False
  Access:WAYJOBS &= SELF


Hide:Relate:WAYJOBS.Init PROCEDURE
  CODE
  Hide:Access:WAYJOBS.Init
  SELF.Init(Access:WAYJOBS,1)


Hide:Access:WAYJOBS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:WAYJOBS &= NULL


Hide:Access:WAYJOBS.PrimeFields PROCEDURE

  CODE
  wayjob:Processed = 0
  PARENT.PrimeFields


Hide:Relate:WAYJOBS.Kill PROCEDURE

  CODE
  Hide:Access:WAYJOBS.Kill
  PARENT.Kill
  Relate:WAYJOBS &= NULL


Hide:Access:WAYSUND_TEMP.Init PROCEDURE
  CODE
  SELF.Init(WAYSUND_TEMP,GlobalErrors)
  SELF.FileName &= glo:FileName
  SELF.Buffer &= wastmp:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(wastmp:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(wastmp:DescriptionKey,'By Description',0)
  SELF.LazyOpen = False
  Access:WAYSUND_TEMP &= SELF


Hide:Relate:WAYSUND_TEMP.Init PROCEDURE
  CODE
  Hide:Access:WAYSUND_TEMP.Init
  SELF.Init(Access:WAYSUND_TEMP,1)


Hide:Access:WAYSUND_TEMP.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:WAYSUND_TEMP &= NULL


Hide:Relate:WAYSUND_TEMP.Kill PROCEDURE

  CODE
  Hide:Access:WAYSUND_TEMP.Kill
  PARENT.Kill
  Relate:WAYSUND_TEMP &= NULL


Hide:Access:WAYCNR.Init PROCEDURE
  CODE
  SELF.Init(WAYCNR,GlobalErrors)
  SELF.Buffer &= wcr:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(wcr:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(wcr:EnteredKey,'wcr:EnteredKey',0)
  SELF.AddKey(wcr:PartNumberKey,'By Part Number',0)
  SELF.AddKey(wcr:ProcessedKey,'By Part Number',0)
  SELF.LazyOpen = False
  Access:WAYCNR &= SELF


Hide:Relate:WAYCNR.Init PROCEDURE
  CODE
  Hide:Access:WAYCNR.Init
  SELF.Init(Access:WAYCNR,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:WAYBILLS)


Hide:Access:WAYCNR.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:WAYCNR &= NULL


Hide:Relate:WAYCNR.Kill PROCEDURE

  CODE
  Hide:Access:WAYCNR.Kill
  PARENT.Kill
  Relate:WAYCNR &= NULL


Hide:Access:WAYBILLJ.Init PROCEDURE
  CODE
  SELF.Init(WAYBILLJ,GlobalErrors)
  SELF.Buffer &= waj:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(waj:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(waj:JobNumberKey,'By Job Number',0)
  SELF.AddKey(waj:DescRecordNoKey,'By Record Number',0)
  SELF.AddKey(waj:DescWaybillNoKey,'By Waybill Number',0)
  SELF.AddKey(waj:DescJobNumberKey,'By Job Number',0)
  SELF.LazyOpen = False
  Access:WAYBILLJ &= SELF


Hide:Relate:WAYBILLJ.Init PROCEDURE
  CODE
  Hide:Access:WAYBILLJ.Init
  SELF.Init(Access:WAYBILLJ,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:WAYBILLS)


Hide:Access:WAYBILLJ.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:WAYBILLJ &= NULL


Hide:Relate:WAYBILLJ.Kill PROCEDURE

  CODE
  Hide:Access:WAYBILLJ.Kill
  PARENT.Kill
  Relate:WAYBILLJ &= NULL


Hide:Access:MANUFACT.Init PROCEDURE
  CODE
  SELF.Init(MANUFACT,GlobalErrors)
  SELF.Buffer &= man:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(man:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(man:Manufacturer_Key,'By Manufacturer',0)
  SELF.AddKey(man:EDIFileTypeKey,'By Manufacturer',0)
  SELF.LazyOpen = False
  Access:MANUFACT &= SELF


Hide:Relate:MANUFACT.Init PROCEDURE
  CODE
  Hide:Access:MANUFACT.Init
  SELF.Init(Access:MANUFACT,1)
  DO AddRelations_1
  DO AddRelations_2

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:MANREJR,RI:CASCADE,RI:CASCADE,mar:CodeKey)
  SELF.AddRelationLink(man:RecordNumber,mar:MANRecordNumber)
  SELF.AddRelation(Relate:REPTYDEF,RI:CASCADE,RI:CASCADE,rtd:ManRepairTypeKey)
  SELF.AddRelationLink(man:Manufacturer,rtd:Manufacturer)
  SELF.AddRelation(Relate:NOTESFAU,RI:CASCADE,RI:CASCADE,nof:Notes_Key)
  SELF.AddRelationLink(man:Manufacturer,nof:Manufacturer)
  SELF.AddRelation(Relate:MODELNUM,RI:CASCADE,RI:CASCADE,mod:Manufacturer_Key)
  SELF.AddRelationLink(man:Manufacturer,mod:Manufacturer)
  SELF.AddRelation(Relate:REPAIRTY,RI:CASCADE,RI:CASCADE,rep:Manufacturer_Key)
  SELF.AddRelationLink(man:Manufacturer,rep:Manufacturer)
  SELF.AddRelation(Relate:JOBS,RI:CASCADE,RI:None,job:EDI_Key)
  SELF.AddRelationLink(man:Manufacturer,job:Manufacturer)
  SELF.AddRelation(Relate:MANFAULO,RI:CASCADE,RI:CASCADE,mfo:Field_Key)
  SELF.AddRelationLink(man:Manufacturer,mfo:Manufacturer)
  SELF.AddRelation(Relate:MANFAUPA,RI:CASCADE,RI:CASCADE,map:Field_Number_Key)
  SELF.AddRelationLink(man:Manufacturer,map:Manufacturer)
  SELF.AddRelation(Relate:MANFPALO,RI:CASCADE,RI:CASCADE,mfp:Field_Key)
  SELF.AddRelationLink(man:Manufacturer,mfp:Manufacturer)
  SELF.AddRelation(Relate:MANFAULT,RI:CASCADE,RI:CASCADE,maf:Field_Number_Key)
  SELF.AddRelationLink(man:Manufacturer,maf:Manufacturer)

AddRelations_2 ROUTINE
  SELF.AddRelation(Relate:STDCHRGE,RI:CASCADE,RI:CASCADE,sta:Model_Number_Charge_Key)
  SELF.AddRelationLink(man:Manufacturer,sta:Model_Number)


Hide:Access:MANUFACT.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:MANUFACT &= NULL


Hide:Access:MANUFACT.PrimeFields PROCEDURE

  CODE
  man:Use_MSN = 'NO'
  man:IncludeAdjustment = 'NO'
  man:AdjustPart = 0
  man:ForceParts = 0
  man:RemAccCosts = 0
  man:SiemensNewEDI = 0
  man:DOPCompulsory = 0
  man:UseQA = 0
  man:UseElectronicQA = 0
  man:QALoanExchange = 0
  man:QAAtCompletion = 0
  man:UseProductCode = 0
  man:ApplyMSNFormat = 0
  man:ValidateDateCode = 0
  man:ForceStatus = 0
  man:QAParts = 0
  man:QANetwork = 0
  man:POPRequired = 0
  man:UseInvTextForFaults = 0
  man:ForceAccessoryCode = 0
  man:UseInternetValidation = 0
  man:AutoRepairType = 0
  man:BillingConfirmation = 0
  man:CreateEDIReport = 0
  man:CreateEDIFile = 0
  man:QALoan = 0
  man:ForceCharFaultCodes = 0
  man:IncludeCharJobs = 0
  man:UseProdCodesForEXC = 0
  man:UseFaultCodesForOBF = 0
  man:KeyRepairRequired = 0
  man:UseReplenishmentProcess = 0
  man:UseResubmissionLimit = 0
  man:AutoTechnicalReports = 0
  man:LimitResubmissions = 0
  man:CopyDOPFromBouncer = 0
  man:ExcludeAutomaticRebookingProcess = 0
  PARENT.PrimeFields


Hide:Relate:MANUFACT.Kill PROCEDURE

  CODE
  Hide:Access:MANUFACT.Kill
  PARENT.Kill
  Relate:MANUFACT &= NULL


Hide:Access:USMASSIG.Init PROCEDURE
  CODE
  SELF.Init(USMASSIG,GlobalErrors)
  SELF.Buffer &= usm:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(usm:Model_Number_Key,'By Model Number',0)
  SELF.LazyOpen = False
  Access:USMASSIG &= SELF


Hide:Relate:USMASSIG.Init PROCEDURE
  CODE
  Hide:Access:USMASSIG.Init
  SELF.Init(Access:USMASSIG,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:USERS)


Hide:Access:USMASSIG.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:USMASSIG &= NULL


Hide:Relate:USMASSIG.Kill PROCEDURE

  CODE
  Hide:Access:USMASSIG.Kill
  PARENT.Kill
  Relate:USMASSIG &= NULL


Hide:Access:EXPWARPARTS.Init PROCEDURE
  CODE
  SELF.Init(EXPWARPARTS,GlobalErrors)
  SELF.FileName &= glo:file_name
  SELF.Buffer &= expwar:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.LazyOpen = False
  Access:EXPWARPARTS &= SELF


Hide:Relate:EXPWARPARTS.Init PROCEDURE
  CODE
  Hide:Access:EXPWARPARTS.Init
  SELF.Init(Access:EXPWARPARTS,1)


Hide:Access:EXPWARPARTS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:EXPWARPARTS &= NULL


Hide:Relate:EXPWARPARTS.Kill PROCEDURE

  CODE
  Hide:Access:EXPWARPARTS.Kill
  PARENT.Kill
  Relate:EXPWARPARTS &= NULL


Hide:Access:WEBJOB.Init PROCEDURE
  CODE
  SELF.Init(WEBJOB,GlobalErrors)
  SELF.Buffer &= wob:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(wob:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(wob:RefNumberKey,'By Ref Number',0)
  SELF.AddKey(wob:HeadJobNumberKey,'By Job Number',0)
  SELF.AddKey(wob:HeadOrderNumberKey,'By Order Number',0)
  SELF.AddKey(wob:HeadMobileNumberKey,'By Mobile Number',0)
  SELF.AddKey(wob:HeadModelNumberKey,'By Job Number',0)
  SELF.AddKey(wob:HeadIMEINumberKey,'By I.M.E.I. Number',0)
  SELF.AddKey(wob:HeadMSNKey,'By M.S.N.',0)
  SELF.AddKey(wob:HeadEDIKey,'By Job Number',0)
  SELF.AddKey(wob:ValidationIMEIKey,'By I.M.E.I. Number',0)
  SELF.AddKey(wob:HeadSubKey,'wob:HeadSubKey',0)
  SELF.AddKey(wob:HeadRefNumberKey,'wob:HeadRefNumberKey',0)
  SELF.AddKey(wob:OracleNumberKey,'By Oracle Number',0)
  SELF.AddKey(wob:HeadCurrentStatusKey,'By Job Status',0)
  SELF.AddKey(wob:HeadExchangeStatus,'By Exchange Status',0)
  SELF.AddKey(wob:HeadLoanStatusKey,'By Loan Status',0)
  SELF.AddKey(wob:ExcWayBillNoKey,'By Exchange Waybill Number',0)
  SELF.AddKey(wob:LoaWayBillNoKey,'By Loan Waybill Number',0)
  SELF.AddKey(wob:JobWayBillNoKey,'By Job WayBill Number',0)
  SELF.AddKey(wob:RRCWInvoiceNumberKey,'By Job Number',0)
  SELF.AddKey(wob:DateBookedKey,'By Date Booked',0)
  SELF.AddKey(wob:DateCompletedKey,'By Date Completed',0)
  SELF.AddKey(wob:CompletedKey,'By Completed',0)
  SELF.AddKey(wob:DespatchKey,'By Job Number',0)
  SELF.AddKey(wob:DespatchSubKey,'By Job Number',0)
  SELF.AddKey(wob:DespatchCourierKey,'By Job Number',0)
  SELF.AddKey(wob:DespatchSubCourierKey,'By Job Number',0)
  SELF.AddKey(wob:EDIKey,'By Job Number',0)
  SELF.AddKey(wob:EDIRefNumberKey,'By Job Number',0)
  SELF.LazyOpen = False
  Access:WEBJOB &= SELF


Hide:Relate:WEBJOB.Init PROCEDURE
  CODE
  Hide:Access:WEBJOB.Init
  SELF.Init(Access:WEBJOB,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS,RI:None,RI:None,job:Ref_Number_Key)
  SELF.AddRelationLink(wob:RefNumber,job:Ref_Number)


Hide:Access:WEBJOB.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:WEBJOB &= NULL


Hide:Access:WEBJOB.PrimeFields PROCEDURE

  CODE
  wob:ReconciledMarker = 0
  wob:DateBooked = TODAY()
  wob:TimeBooked = Clock()
  wob:Completed = 'NO'
  wob:ReadyToDespatch = 0
  PARENT.PrimeFields


Hide:Access:WEBJOB.TryUpdate PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TryUpdate()
  ! Inserting (DBH 16/09/2008) # 10253 - Update Date/Time Stamp
  UpdateDateTimeStamp(wob:RefNumber)
  ! End (DBH 16/09/2008) #10253
  RETURN ReturnValue


Hide:Access:WEBJOB.Update PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Update()
  ! Inserting (DBH 16/09/2008) # 10253 - Update Date/Time Stamp
  UpdateDateTimeStamp(wob:RefNumber)
  ! End (DBH 16/09/2008) #10253
  RETURN ReturnValue


Hide:Relate:WEBJOB.Kill PROCEDURE

  CODE
  Hide:Access:WEBJOB.Kill
  PARENT.Kill
  Relate:WEBJOB &= NULL


Hide:Access:WIPEXC.Init PROCEDURE
  CODE
  SELF.Init(WIPEXC,GlobalErrors)
  SELF.Buffer &= wix:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(wix:WIPEXCKey,'wix:WIPEXCKey',1)
  SELF.AddKey(wix:StatusTypeKey,'wix:StatusTypeKey',0)
  SELF.LazyOpen = False
  Access:WIPEXC &= SELF


Hide:Relate:WIPEXC.Init PROCEDURE
  CODE
  Hide:Access:WIPEXC.Init
  SELF.Init(Access:WIPEXC,1)


Hide:Access:WIPEXC.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:WIPEXC &= NULL


Hide:Access:WIPEXC.PrimeFields PROCEDURE

  CODE
  wix:NoIMEI = 0
  PARENT.PrimeFields


Hide:Relate:WIPEXC.Kill PROCEDURE

  CODE
  Hide:Access:WIPEXC.Kill
  PARENT.Kill
  Relate:WIPEXC &= NULL


Hide:Access:USUASSIG.Init PROCEDURE
  CODE
  SELF.Init(USUASSIG,GlobalErrors)
  SELF.Buffer &= usu:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(usu:Unit_Type_Key,'By Unit Type',0)
  SELF.AddKey(usu:Unit_Type_Only_Key,'By Unit Type',0)
  SELF.LazyOpen = False
  Access:USUASSIG &= SELF


Hide:Relate:USUASSIG.Init PROCEDURE
  CODE
  Hide:Access:USUASSIG.Init
  SELF.Init(Access:USUASSIG,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:USERS)
  SELF.AddRelation(Relate:UNITTYPE)


Hide:Access:USUASSIG.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:USUASSIG &= NULL


Hide:Relate:USUASSIG.Kill PROCEDURE

  CODE
  Hide:Access:USUASSIG.Kill
  PARENT.Kill
  Relate:USUASSIG &= NULL


Hide:Access:DEFEDI.Init PROCEDURE
  CODE
  SELF.Init(DEFEDI,GlobalErrors)
  SELF.Buffer &= edi:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(edi:record_number_key,'edi:record_number_key',1)
  SELF.LazyOpen = False
  Access:DEFEDI &= SELF


Hide:Relate:DEFEDI.Init PROCEDURE
  CODE
  Hide:Access:DEFEDI.Init
  SELF.Init(Access:DEFEDI,1)


Hide:Access:DEFEDI.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:DEFEDI &= NULL


Hide:Relate:DEFEDI.Kill PROCEDURE

  CODE
  Hide:Access:DEFEDI.Kill
  PARENT.Kill
  Relate:DEFEDI &= NULL


Hide:Access:WIPSCAN.Init PROCEDURE
  CODE
  SELF.Init(WIPSCAN,GlobalErrors)
  SELF.Buffer &= wsc:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(wsc:Internal_No_Key,'wsc:Internal_No_Key',1)
  SELF.AddKey(wsc:Audit_Number_Key,'wsc:Audit_Number_Key',0)
  SELF.AddKey(wsc:Completed_Audit_Key,'wsc:Completed_Audit_Key',0)
  SELF.LazyOpen = False
  Access:WIPSCAN &= SELF


Hide:Relate:WIPSCAN.Init PROCEDURE
  CODE
  Hide:Access:WIPSCAN.Init
  SELF.Init(Access:WIPSCAN,1)


Hide:Access:WIPSCAN.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:WIPSCAN &= NULL


Hide:Relate:WIPSCAN.Kill PROCEDURE

  CODE
  Hide:Access:WIPSCAN.Kill
  PARENT.Kill
  Relate:WIPSCAN &= NULL


Hide:Access:REPMAIL.Init PROCEDURE
  CODE
  SELF.Init(REPMAIL,GlobalErrors)
  SELF.FileNameValue = 'REPMAIL'
  SELF.Buffer &= rpm:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(rpm:Ref_Number_Key,'rpm:Ref_Number_Key',1)
  SELF.AddKey(rpm:Reports_Key,'rpm:Reports_Key',0)
  SELF.AddKey(rpm:Date_Key,'rpm:Date_Key',0)
  SELF.LazyOpen = False
  Access:REPMAIL &= SELF


Hide:Relate:REPMAIL.Init PROCEDURE
  CODE
  Hide:Access:REPMAIL.Init
  SELF.Init(Access:REPMAIL,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:REPMAILP,RI:CASCADE,RI:CASCADE,rpp:Ref_Number_Key)
  SELF.AddRelationLink(rpm:Ref_Number,rpp:Ref_Number)


Hide:Access:REPMAIL.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:REPMAIL &= NULL


Hide:Access:REPMAIL.PrimeFields PROCEDURE

  CODE
  rpm:On_Hold = 0
  PARENT.PrimeFields


Hide:Relate:REPMAIL.Kill PROCEDURE

  CODE
  Hide:Access:REPMAIL.Kill
  PARENT.Kill
  Relate:REPMAIL &= NULL


Hide:Access:PRIBAND_ALIAS.Init PROCEDURE
  CODE
  SELF.Init(PRIBAND_ALIAS,GlobalErrors)
  SELF.Buffer &= prb_ali:Record
  SELF.AliasedFile &= Access:PRIBAND                  !This is a File Alias, so assign aliased file
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(prb_ali:KeyRecordNumber,'prb_ali:KeyRecordNumber',1)
  SELF.AddKey(prb_ali:KeyBandName,'prb_ali:KeyBandName',0)
  SELF.AddKey(prb_ali:KeyMinPrice,'prb_ali:KeyMinPrice',0)
  SELF.LazyOpen = False
  Access:PRIBAND_ALIAS &= SELF


Hide:Relate:PRIBAND_ALIAS.Init PROCEDURE
  CODE
  Hide:Access:PRIBAND_ALIAS.Init
  SELF.Init(Access:PRIBAND_ALIAS,1)
  SELF.SetAlias(Relate:PRIBAND)


Hide:Access:PRIBAND_ALIAS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:PRIBAND_ALIAS &= NULL


Hide:Relate:PRIBAND_ALIAS.Kill PROCEDURE

  CODE
  Hide:Access:PRIBAND_ALIAS.Kill
  PARENT.Kill
  Relate:PRIBAND_ALIAS &= NULL


Hide:Access:SMSRECVD_ALIAS.Init PROCEDURE
  CODE
  SELF.Init(SMSRECVD_ALIAS,GlobalErrors)
  SELF.Buffer &= SMR1:Record
  SELF.AliasedFile &= Access:SMSRECVD                 !This is a File Alias, so assign aliased file
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(SMR1:KeyRecordNo,'SMR1:KeyRecordNo',1)
  SELF.AddKey(SMR1:KeyJob_Ref_Number,'SMR1:KeyJob_Ref_Number',0)
  SELF.AddKey(SMR1:KeyJob_Date_Time_Dec,'SMR1:KeyJob_Date_Time_Dec',0)
  SELF.AddKey(SMR1:KeyAccount_Date_Time,'SMR1:KeyAccount_Date_Time',0)
  SELF.LazyOpen = False
  Access:SMSRECVD_ALIAS &= SELF


Hide:Relate:SMSRECVD_ALIAS.Init PROCEDURE
  CODE
  Hide:Access:SMSRECVD_ALIAS.Init
  SELF.Init(Access:SMSRECVD_ALIAS,1)
  SELF.SetAlias(Relate:SMSRECVD)


Hide:Access:SMSRECVD_ALIAS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:SMSRECVD_ALIAS &= NULL


Hide:Relate:SMSRECVD_ALIAS.Kill PROCEDURE

  CODE
  Hide:Access:SMSRECVD_ALIAS.Kill
  PARENT.Kill
  Relate:SMSRECVD_ALIAS &= NULL


Hide:Access:SMSText_Alias.Init PROCEDURE
  CODE
  SELF.Init(SMSText_Alias,GlobalErrors)
  SELF.Buffer &= SMT1:Record
  SELF.AliasedFile &= Access:SMSText                  !This is a File Alias, so assign aliased file
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(SMT1:Key_Record_No,'SMT1:Key_Record_No',1)
  SELF.AddKey(SMT1:Key_StatusType_Location_Trigger,'SMT1:Key_StatusType_Location_Trigger',0)
  SELF.AddKey(SMT1:Key_Description,'SMT1:Key_Description',0)
  SELF.AddKey(SMT1:Key_StatusType_Description,'SMT1:Key_StatusType_Description',0)
  SELF.AddKey(SMT1:Key_TriggerStatus,'SMT1:Key_TriggerStatus',0)
  SELF.AddKey(SMT1:Key_Location_CSI,'SMT1:Key_Location_CSI',0)
  SELF.AddKey(SMT1:Key_Location_Duplicate,'SMT1:Key_Location_Duplicate',0)
  SELF.AddKey(SMT1:Key_Location_BER,'SMT1:Key_Location_BER',0)
  SELF.AddKey(SMT1:Key_Location_Liquid,'SMT1:Key_Location_Liquid',0)
  SELF.LazyOpen = False
  Access:SMSText_Alias &= SELF


Hide:Relate:SMSText_Alias.Init PROCEDURE
  CODE
  Hide:Access:SMSText_Alias.Init
  SELF.Init(Access:SMSText_Alias,1)
  SELF.SetAlias(Relate:SMSText)


Hide:Access:SMSText_Alias.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:SMSText_Alias &= NULL


Hide:Relate:SMSText_Alias.Kill PROCEDURE

  CODE
  Hide:Access:SMSText_Alias.Kill
  PARENT.Kill
  Relate:SMSText_Alias &= NULL

