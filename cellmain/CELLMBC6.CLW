  MEMBER('cellmain.clw')

  INCLUDE('ABFILE.INC'),ONCE

  MAP
CELLMBC6:DctInit    PROCEDURE
CELLMBC6:DctKill    PROCEDURE
CELLMBC6:FilesInit  PROCEDURE
  END

Hide:Access:DEFWEB   CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:DEFWEB   CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:EXPCITY  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:EXPCITY  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:PROCCODE CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
ValidateFieldServer    PROCEDURE(UNSIGNED Id,BYTE HandleErrors),BYTE,PROC,DERIVED
                     END


Hide:Relate:PROCCODE CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:COLOUR   CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:COLOUR   CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:VODAIMP  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:VODAIMP  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBSTAMP CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:JOBSTAMP CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBSVODA CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
ValidateFieldServer    PROCEDURE(UNSIGNED Id,BYTE HandleErrors),BYTE,PROC,DERIVED
                     END


Hide:Relate:JOBSVODA CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:XREPACT  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:XREPACT  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:ACCESDEF CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:ACCESDEF CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:STANTEXT CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:STANTEXT CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:NOTESENG CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:NOTESENG CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBACCNO CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:JOBACCNO CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:EXPLABG  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:EXPLABG  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBRPNOT CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:JOBRPNOT CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBSOBF  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
TryUpdate              PROCEDURE(),BYTE,PROC,DERIVED
Update                 PROCEDURE(),BYTE,PROC,DERIVED
                     END


Hide:Relate:JOBSOBF  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBSINV  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:JOBSINV  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBSCONS CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:JOBSCONS CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBSWARR CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:JOBSWARR CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:CURRENCY CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:CURRENCY CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:STOCKRECEIVETMP CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:STOCKRECEIVETMP CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

CELLMBC6:DctInit PROCEDURE
  CODE
  Relate:DEFWEB &= Hide:Relate:DEFWEB
  Relate:EXPCITY &= Hide:Relate:EXPCITY
  Relate:PROCCODE &= Hide:Relate:PROCCODE
  Relate:COLOUR &= Hide:Relate:COLOUR
  Relate:VODAIMP &= Hide:Relate:VODAIMP
  Relate:JOBSTAMP &= Hide:Relate:JOBSTAMP
  Relate:JOBSVODA &= Hide:Relate:JOBSVODA
  Relate:XREPACT &= Hide:Relate:XREPACT
  Relate:ACCESDEF &= Hide:Relate:ACCESDEF
  Relate:STANTEXT &= Hide:Relate:STANTEXT
  Relate:NOTESENG &= Hide:Relate:NOTESENG
  Relate:JOBACCNO &= Hide:Relate:JOBACCNO
  Relate:EXPLABG &= Hide:Relate:EXPLABG
  Relate:JOBRPNOT &= Hide:Relate:JOBRPNOT
  Relate:JOBSOBF &= Hide:Relate:JOBSOBF
  Relate:JOBSINV &= Hide:Relate:JOBSINV
  Relate:JOBSCONS &= Hide:Relate:JOBSCONS
  Relate:JOBSWARR &= Hide:Relate:JOBSWARR
  Relate:CURRENCY &= Hide:Relate:CURRENCY
  Relate:STOCKRECEIVETMP &= Hide:Relate:STOCKRECEIVETMP

CELLMBC6:FilesInit PROCEDURE
  CODE
  Hide:Relate:DEFWEB.Init
  Hide:Relate:EXPCITY.Init
  Hide:Relate:PROCCODE.Init
  Hide:Relate:COLOUR.Init
  Hide:Relate:VODAIMP.Init
  Hide:Relate:JOBSTAMP.Init
  Hide:Relate:JOBSVODA.Init
  Hide:Relate:XREPACT.Init
  Hide:Relate:ACCESDEF.Init
  Hide:Relate:STANTEXT.Init
  Hide:Relate:NOTESENG.Init
  Hide:Relate:JOBACCNO.Init
  Hide:Relate:EXPLABG.Init
  Hide:Relate:JOBRPNOT.Init
  Hide:Relate:JOBSOBF.Init
  Hide:Relate:JOBSINV.Init
  Hide:Relate:JOBSCONS.Init
  Hide:Relate:JOBSWARR.Init
  Hide:Relate:CURRENCY.Init
  Hide:Relate:STOCKRECEIVETMP.Init


CELLMBC6:DctKill PROCEDURE
  CODE
  Hide:Relate:DEFWEB.Kill
  Hide:Relate:EXPCITY.Kill
  Hide:Relate:PROCCODE.Kill
  Hide:Relate:COLOUR.Kill
  Hide:Relate:VODAIMP.Kill
  Hide:Relate:JOBSTAMP.Kill
  Hide:Relate:JOBSVODA.Kill
  Hide:Relate:XREPACT.Kill
  Hide:Relate:ACCESDEF.Kill
  Hide:Relate:STANTEXT.Kill
  Hide:Relate:NOTESENG.Kill
  Hide:Relate:JOBACCNO.Kill
  Hide:Relate:EXPLABG.Kill
  Hide:Relate:JOBRPNOT.Kill
  Hide:Relate:JOBSOBF.Kill
  Hide:Relate:JOBSINV.Kill
  Hide:Relate:JOBSCONS.Kill
  Hide:Relate:JOBSWARR.Kill
  Hide:Relate:CURRENCY.Kill
  Hide:Relate:STOCKRECEIVETMP.Kill


Hide:Access:DEFWEB.Init PROCEDURE
  CODE
  SELF.Init(DEFWEB,GlobalErrors)
  SELF.Buffer &= dew:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(dew:Record_Number_Key,'dew:Record_Number_Key',1)
  SELF.AddKey(dew:Account_Number_Key,'By Account Number',0)
  SELF.LazyOpen = False
  Access:DEFWEB &= SELF


Hide:Relate:DEFWEB.Init PROCEDURE
  CODE
  Hide:Access:DEFWEB.Init
  SELF.Init(Access:DEFWEB,1)


Hide:Access:DEFWEB.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:DEFWEB &= NULL


Hide:Access:DEFWEB.PrimeFields PROCEDURE

  CODE
  dew:AllowJobBooking = 0
  dew:AllowJobProgress = 0
  dew:AllowOrderParts = 0
  dew:AllowOrderProgress = 0
  dew:Option5 = 0
  dew:Option6 = 0
  dew:Option7 = 0
  dew:Option8 = 0
  dew:Option9 = 0
  dew:Option10 = 0
  PARENT.PrimeFields


Hide:Relate:DEFWEB.Kill PROCEDURE

  CODE
  Hide:Access:DEFWEB.Kill
  PARENT.Kill
  Relate:DEFWEB &= NULL


Hide:Access:EXPCITY.Init PROCEDURE
  CODE
  SELF.Init(EXPCITY,GlobalErrors)
  SELF.FileName &= glo:file_name
  SELF.Buffer &= epc:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.LazyOpen = False
  Access:EXPCITY &= SELF


Hide:Relate:EXPCITY.Init PROCEDURE
  CODE
  Hide:Access:EXPCITY.Init
  SELF.Init(Access:EXPCITY,1)


Hide:Access:EXPCITY.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:EXPCITY &= NULL


Hide:Relate:EXPCITY.Kill PROCEDURE

  CODE
  Hide:Access:EXPCITY.Kill
  PARENT.Kill
  Relate:EXPCITY &= NULL


Hide:Access:PROCCODE.Init PROCEDURE
  CODE
  SELF.Init(PROCCODE,GlobalErrors)
  SELF.Buffer &= pro:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(pro:Code_Number_Key,'By Code Number',0)
  SELF.LazyOpen = False
  Access:PROCCODE &= SELF


Hide:Relate:PROCCODE.Init PROCEDURE
  CODE
  Hide:Access:PROCCODE.Init
  SELF.Init(Access:PROCCODE,1)


Hide:Access:PROCCODE.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:PROCCODE &= NULL


Hide:Access:PROCCODE.ValidateFieldServer PROCEDURE(UNSIGNED Id,BYTE HandleErrors)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.ValidateFieldServer(Id,HandleErrors)
  CASE Id
  OF 3
    GlobalErrors.SetField('pro:Allow_Loan')
    IF INSTRING(']' & CLIP(pro:Allow_Loan) & '[', ']YES[]NO[', 1, 1) = 0
      ReturnValue = Level:Notify
    END
    IF ReturnValue <> Level:Benign
      IF HandleErrors
        ReturnValue = GlobalErrors.ThrowMessage(Msg:FieldNotInList,'''YES'',''NO''')
      END
    END
  END
  RETURN ReturnValue


Hide:Relate:PROCCODE.Kill PROCEDURE

  CODE
  Hide:Access:PROCCODE.Kill
  PARENT.Kill
  Relate:PROCCODE &= NULL


Hide:Access:COLOUR.Init PROCEDURE
  CODE
  SELF.Init(COLOUR,GlobalErrors)
  SELF.Buffer &= col:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(col:Colour_Key,'By Colour',0)
  SELF.LazyOpen = False
  Access:COLOUR &= SELF


Hide:Relate:COLOUR.Init PROCEDURE
  CODE
  Hide:Access:COLOUR.Init
  SELF.Init(Access:COLOUR,1)


Hide:Access:COLOUR.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:COLOUR &= NULL


Hide:Relate:COLOUR.Kill PROCEDURE

  CODE
  Hide:Access:COLOUR.Kill
  PARENT.Kill
  Relate:COLOUR &= NULL


Hide:Access:VODAIMP.Init PROCEDURE
  CODE
  SELF.Init(VODAIMP,GlobalErrors)
  SELF.FileName &= glo:file_name
  SELF.Buffer &= vdi:Record
  SELF.Create = 0
  SELF.LockRecover = 10
  SELF.LazyOpen = False
  Access:VODAIMP &= SELF


Hide:Relate:VODAIMP.Init PROCEDURE
  CODE
  Hide:Access:VODAIMP.Init
  SELF.Init(Access:VODAIMP,1)


Hide:Access:VODAIMP.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:VODAIMP &= NULL


Hide:Relate:VODAIMP.Kill PROCEDURE

  CODE
  Hide:Access:VODAIMP.Kill
  PARENT.Kill
  Relate:VODAIMP &= NULL


Hide:Access:JOBSTAMP.Init PROCEDURE
  CODE
  SELF.Init(JOBSTAMP,GlobalErrors)
  SELF.Buffer &= jos:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jos:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(jos:JOBSRefNumberKey,'By RefNumber',0)
  SELF.AddKey(jos:DateTimeKey,'By Date Time',0)
  SELF.LazyOpen = False
  Access:JOBSTAMP &= SELF


Hide:Relate:JOBSTAMP.Init PROCEDURE
  CODE
  Hide:Access:JOBSTAMP.Init
  SELF.Init(Access:JOBSTAMP,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:JOBSTAMP.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBSTAMP &= NULL


Hide:Relate:JOBSTAMP.Kill PROCEDURE

  CODE
  Hide:Access:JOBSTAMP.Kill
  PARENT.Kill
  Relate:JOBSTAMP &= NULL


Hide:Access:JOBSVODA.Init PROCEDURE
  CODE
  SELF.Init(JOBSVODA,GlobalErrors)
  SELF.Buffer &= jvf:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jvf:Ref_Number_Key,'By Ref Number',1)
  SELF.AddKey(jvf:Ref_Pending_Key,'By Ref Number',0)
  SELF.AddKey(jvf:Order_Number_Key,'By Order Number',0)
  SELF.AddKey(jvf:Process_Code_Key,'By Process Code',0)
  SELF.AddKey(jvf:Model_Number_Key,'By Unit Type',0)
  SELF.AddKey(jvf:ESN_Key,'By ESN',0)
  SELF.AddKey(jvf:Job_Number_Key,'By Job Number',0)
  SELF.AddKey(jvf:Model_Ref_key,'By Model Number',0)
  SELF.LazyOpen = False
  Access:JOBSVODA &= SELF


Hide:Relate:JOBSVODA.Init PROCEDURE
  CODE
  Hide:Access:JOBSVODA.Init
  SELF.Init(Access:JOBSVODA,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBVODAC,RI:CASCADE,RI:CASCADE,jva:Ref_Number_Key)
  SELF.AddRelationLink(jvf:Ref_Number,jva:Ref_Number)


Hide:Access:JOBSVODA.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBSVODA &= NULL


Hide:Access:JOBSVODA.PrimeFields PROCEDURE

  CODE
  jvf:Pending = 'NO'
  PARENT.PrimeFields


Hide:Access:JOBSVODA.ValidateFieldServer PROCEDURE(UNSIGNED Id,BYTE HandleErrors)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.ValidateFieldServer(Id,HandleErrors)
  CASE Id
  OF 33
    GlobalErrors.SetField('jvf:Pending')
    IF INSTRING(']' & CLIP(jvf:Pending) & '[', ']NO[]YES[', 1, 1) = 0
      ReturnValue = Level:Notify
    END
    IF ReturnValue <> Level:Benign
      IF HandleErrors
        ReturnValue = GlobalErrors.ThrowMessage(Msg:FieldNotInList,'''NO'',''YES''')
      END
    END
  END
  RETURN ReturnValue


Hide:Relate:JOBSVODA.Kill PROCEDURE

  CODE
  Hide:Access:JOBSVODA.Kill
  PARENT.Kill
  Relate:JOBSVODA &= NULL


Hide:Access:XREPACT.Init PROCEDURE
  CODE
  SELF.Init(XREPACT,GlobalErrors)
  SELF.FileName &= glo:File_Name
  SELF.Buffer &= xre:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.LazyOpen = False
  Access:XREPACT &= SELF


Hide:Relate:XREPACT.Init PROCEDURE
  CODE
  Hide:Access:XREPACT.Init
  SELF.Init(Access:XREPACT,1)


Hide:Access:XREPACT.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:XREPACT &= NULL


Hide:Relate:XREPACT.Kill PROCEDURE

  CODE
  Hide:Access:XREPACT.Kill
  PARENT.Kill
  Relate:XREPACT &= NULL


Hide:Access:ACCESDEF.Init PROCEDURE
  CODE
  SELF.Init(ACCESDEF,GlobalErrors)
  SELF.Buffer &= acd:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(acd:Accessory_Key,'By Accessory',0)
  SELF.LazyOpen = False
  Access:ACCESDEF &= SELF


Hide:Relate:ACCESDEF.Init PROCEDURE
  CODE
  Hide:Access:ACCESDEF.Init
  SELF.Init(Access:ACCESDEF,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:ACCESSOR,RI:CASCADE,RI:None,acr:Model_Number_Key)
  SELF.AddRelationLink(acd:Accessory,acr:Accessory)


Hide:Access:ACCESDEF.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:ACCESDEF &= NULL


Hide:Relate:ACCESDEF.Kill PROCEDURE

  CODE
  Hide:Access:ACCESDEF.Kill
  PARENT.Kill
  Relate:ACCESDEF &= NULL


Hide:Access:STANTEXT.Init PROCEDURE
  CODE
  SELF.Init(STANTEXT,GlobalErrors)
  SELF.Buffer &= stt:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(stt:Description_Key,'By Description',0)
  SELF.LazyOpen = False
  Access:STANTEXT &= SELF


Hide:Relate:STANTEXT.Init PROCEDURE
  CODE
  Hide:Access:STANTEXT.Init
  SELF.Init(Access:STANTEXT,1)


Hide:Access:STANTEXT.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:STANTEXT &= NULL


Hide:Relate:STANTEXT.Kill PROCEDURE

  CODE
  Hide:Access:STANTEXT.Kill
  PARENT.Kill
  Relate:STANTEXT &= NULL


Hide:Access:NOTESENG.Init PROCEDURE
  CODE
  SELF.Init(NOTESENG,GlobalErrors)
  SELF.Buffer &= noe:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(noe:Notes_Key,'By Notes',0)
  SELF.LazyOpen = False
  Access:NOTESENG &= SELF


Hide:Relate:NOTESENG.Init PROCEDURE
  CODE
  Hide:Access:NOTESENG.Init
  SELF.Init(Access:NOTESENG,1)


Hide:Access:NOTESENG.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:NOTESENG &= NULL


Hide:Relate:NOTESENG.Kill PROCEDURE

  CODE
  Hide:Access:NOTESENG.Kill
  PARENT.Kill
  Relate:NOTESENG &= NULL


Hide:Access:JOBACCNO.Init PROCEDURE
  CODE
  SELF.Init(JOBACCNO,GlobalErrors)
  SELF.Buffer &= joa:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(joa:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(joa:AccessoryNumberKey,'By Accessory Number',0)
  SELF.AddKey(joa:AccessoryNoOnlyKey,'By Accessory Number',0)
  SELF.LazyOpen = False
  Access:JOBACCNO &= SELF


Hide:Relate:JOBACCNO.Init PROCEDURE
  CODE
  Hide:Access:JOBACCNO.Init
  SELF.Init(Access:JOBACCNO,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:JOBACCNO.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBACCNO &= NULL


Hide:Relate:JOBACCNO.Kill PROCEDURE

  CODE
  Hide:Access:JOBACCNO.Kill
  PARENT.Kill
  Relate:JOBACCNO &= NULL


Hide:Access:EXPLABG.Init PROCEDURE
  CODE
  SELF.Init(EXPLABG,GlobalErrors)
  SELF.FileName &= glo:file_name
  SELF.Buffer &= epg:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.LazyOpen = False
  Access:EXPLABG &= SELF


Hide:Relate:EXPLABG.Init PROCEDURE
  CODE
  Hide:Access:EXPLABG.Init
  SELF.Init(Access:EXPLABG,1)


Hide:Access:EXPLABG.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:EXPLABG &= NULL


Hide:Relate:EXPLABG.Kill PROCEDURE

  CODE
  Hide:Access:EXPLABG.Kill
  PARENT.Kill
  Relate:EXPLABG &= NULL


Hide:Access:JOBRPNOT.Init PROCEDURE
  CODE
  SELF.Init(JOBRPNOT,GlobalErrors)
  SELF.Buffer &= jrn:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jrn:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(jrn:TheDateKey,'By Date',0)
  SELF.LazyOpen = False
  Access:JOBRPNOT &= SELF


Hide:Relate:JOBRPNOT.Init PROCEDURE
  CODE
  Hide:Access:JOBRPNOT.Init
  SELF.Init(Access:JOBRPNOT,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:JOBRPNOT.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBRPNOT &= NULL


Hide:Access:JOBRPNOT.PrimeFields PROCEDURE

  CODE
  jrn:TheDate = Today()
  jrn:TheTime = Clock()
  PARENT.PrimeFields


Hide:Relate:JOBRPNOT.Kill PROCEDURE

  CODE
  Hide:Access:JOBRPNOT.Kill
  PARENT.Kill
  Relate:JOBRPNOT &= NULL


Hide:Access:JOBSOBF.Init PROCEDURE
  CODE
  SELF.Init(JOBSOBF,GlobalErrors)
  SELF.Buffer &= jof:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jof:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(jof:RefNumberKey,'By Job Number',0)
  SELF.AddKey(jof:StatusRefNumberKey,'By Job Number',0)
  SELF.AddKey(jof:StatusIMEINumberKey,'By I.M.E.I. Number',0)
  SELF.AddKey(jof:HeadAccountCompletedKey,'By Date Completed',0)
  SELF.AddKey(jof:HeadAccountProcessedKey,'By Date Processed',0)
  SELF.LazyOpen = False
  Access:JOBSOBF &= SELF


Hide:Relate:JOBSOBF.Init PROCEDURE
  CODE
  Hide:Access:JOBSOBF.Init
  SELF.Init(Access:JOBSOBF,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:JOBSOBF.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBSOBF &= NULL


Hide:Access:JOBSOBF.PrimeFields PROCEDURE

  CODE
  jof:Status = 0
  jof:Replacement = 0
  PARENT.PrimeFields


Hide:Access:JOBSOBF.TryUpdate PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.TryUpdate()
  ! Inserting (DBH 16/09/2008) # 10253 - Update Date/Time Stamp
  UpdateDateTimeStamp(jof:RefNumber)
  ! End (DBH 16/09/2008) #10253
  RETURN ReturnValue


Hide:Access:JOBSOBF.Update PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Update()
  ! Inserting (DBH 16/09/2008) # 10253 - Update Date/Time Stamp
  UpdateDateTimeStamp(jof:RefNumber)
  ! End (DBH 16/09/2008) #10253
  RETURN ReturnValue


Hide:Relate:JOBSOBF.Kill PROCEDURE

  CODE
  Hide:Access:JOBSOBF.Kill
  PARENT.Kill
  Relate:JOBSOBF &= NULL


Hide:Access:JOBSINV.Init PROCEDURE
  CODE
  SELF.Init(JOBSINV,GlobalErrors)
  SELF.Buffer &= jov:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jov:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(jov:RefNumberKey,'By Record Number',0)
  SELF.AddKey(jov:DateCreatedKey,'By Date Created',0)
  SELF.AddKey(jov:DateCreatedOnlyKey,'By Job Number',0)
  SELF.AddKey(jov:TypeRecordKey,'By Record Number',0)
  SELF.AddKey(jov:TypeSuffixKey,'By Suffix',0)
  SELF.AddKey(jov:InvoiceNumberKey,'By Invoice Number',0)
  SELF.AddKey(jov:InvoiceTypeKey,'By Invoice Number',0)
  SELF.AddKey(jov:BookingDateTypeKey,'By Type',0)
  SELF.AddKey(jov:TypeDateKey,'By Type',0)
  SELF.LazyOpen = False
  Access:JOBSINV &= SELF


Hide:Relate:JOBSINV.Init PROCEDURE
  CODE
  Hide:Access:JOBSINV.Init
  SELF.Init(Access:JOBSINV,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:JOBSINV.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBSINV &= NULL


Hide:Access:JOBSINV.PrimeFields PROCEDURE

  CODE
  jov:DateCreated = Today()
  jov:TimeCreated = Clock()
  PARENT.PrimeFields


Hide:Relate:JOBSINV.Kill PROCEDURE

  CODE
  Hide:Access:JOBSINV.Kill
  PARENT.Kill
  Relate:JOBSINV &= NULL


Hide:Access:JOBSCONS.Init PROCEDURE
  CODE
  SELF.Init(JOBSCONS,GlobalErrors)
  SELF.Buffer &= joc:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(joc:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(joc:DateKey,'By Date',0)
  SELF.AddKey(joc:ConsignmentNumberKey,'By Consignment Number',0)
  SELF.AddKey(joc:DespatchFromDateKey,'By Date',0)
  SELF.AddKey(joc:DateOnlyKey,'By Date',0)
  SELF.AddKey(joc:CourierKey,'By Courier',0)
  SELF.AddKey(joc:DespatchFromCourierKey,'By Job Number',0)
  SELF.LazyOpen = False
  Access:JOBSCONS &= SELF


Hide:Relate:JOBSCONS.Init PROCEDURE
  CODE
  Hide:Access:JOBSCONS.Init
  SELF.Init(Access:JOBSCONS,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:JOBSCONS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBSCONS &= NULL


Hide:Access:JOBSCONS.PrimeFields PROCEDURE

  CODE
  joc:TheDate = Today()
  joc:TheTime = Clock()
  PARENT.PrimeFields


Hide:Relate:JOBSCONS.Kill PROCEDURE

  CODE
  Hide:Access:JOBSCONS.Kill
  PARENT.Kill
  Relate:JOBSCONS &= NULL


Hide:Access:JOBSWARR.Init PROCEDURE
  CODE
  SELF.Init(JOBSWARR,GlobalErrors)
  SELF.Buffer &= jow:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jow:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(jow:RefNumberKey,'By Ref Number',0)
  SELF.AddKey(jow:StatusManFirstKey,'By Job Number',0)
  SELF.AddKey(jow:StatusManKey,'By Job Number',0)
  SELF.AddKey(jow:ClaimStatusManKey,'By Job Number',0)
  SELF.AddKey(jow:ClaimStatusManFirstKey,'By Job Number',0)
  SELF.AddKey(jow:RRCStatusKey,'By Job Number',0)
  SELF.AddKey(jow:RRCStatusManKey,'By Job Number',0)
  SELF.AddKey(jow:RRCReconciledManKey,'By Job Number',0)
  SELF.AddKey(jow:RRCReconciledKey,'By Job Number',0)
  SELF.AddKey(jow:RepairedRRCStatusKey,'By RRC Status',0)
  SELF.AddKey(jow:RepairedRRCStatusManKey,'By Job Number',0)
  SELF.AddKey(jow:RepairedRRCReconciledKey,'By Job Number',0)
  SELF.AddKey(jow:RepairedRRCReconManKey,'By Job Number',0)
  SELF.AddKey(jow:SubmittedRepairedBranchKey,'By Branch',0)
  SELF.AddKey(jow:SubmittedBranchKey,'By Job Number',0)
  SELF.AddKey(jow:ClaimSubmittedKey,'By Job Number',0)
  SELF.AddKey(jow:RepairedRRCAccManKey,'By Job Number',0)
  SELF.AddKey(jow:AcceptedBranchKey,'By Job Number',0)
  SELF.AddKey(jow:DateAcceptedKey,'By Job Number',0)
  SELF.AddKey(jow:RejectedBranchKey,'By Job Number',0)
  SELF.AddKey(jow:RejectedKey,'By Job Number',0)
  SELF.AddKey(jow:FinalRejectionBranchKey,'By Job Number',0)
  SELF.AddKey(jow:FinalRejectionKey,'By Job Number',0)
  SELF.LazyOpen = False
  Access:JOBSWARR &= SELF


Hide:Relate:JOBSWARR.Init PROCEDURE
  CODE
  Hide:Access:JOBSWARR.Init
  SELF.Init(Access:JOBSWARR,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:JOBSWARR.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBSWARR &= NULL


Hide:Access:JOBSWARR.PrimeFields PROCEDURE

  CODE
  jow:FirstSecondYear = 0
  jow:FromApproved = 0
  PARENT.PrimeFields


Hide:Relate:JOBSWARR.Kill PROCEDURE

  CODE
  Hide:Access:JOBSWARR.Kill
  PARENT.Kill
  Relate:JOBSWARR &= NULL


Hide:Access:CURRENCY.Init PROCEDURE
  CODE
  SELF.Init(CURRENCY,GlobalErrors)
  SELF.Buffer &= cur:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(cur:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(cur:CurrencyCodeKey,'By Currency Code',0)
  SELF.AddKey(cur:LastUpdateDateKey,'By Currency Code',0)
  SELF.AddKey(cur:CorrelationCodeKey,'By Correlation Code',0)
  SELF.LazyOpen = False
  Access:CURRENCY &= SELF


Hide:Relate:CURRENCY.Init PROCEDURE
  CODE
  Hide:Access:CURRENCY.Init
  SELF.Init(Access:CURRENCY,1)


Hide:Access:CURRENCY.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:CURRENCY &= NULL


Hide:Access:CURRENCY.PrimeFields PROCEDURE

  CODE
  cur:LastUpdateDate = Today()
  PARENT.PrimeFields


Hide:Relate:CURRENCY.Kill PROCEDURE

  CODE
  Hide:Access:CURRENCY.Kill
  PARENT.Kill
  Relate:CURRENCY &= NULL


Hide:Access:STOCKRECEIVETMP.Init PROCEDURE
  CODE
  SELF.Init(STOCKRECEIVETMP,GlobalErrors)
  SELF.FileName &= glo:FileName
  SELF.Buffer &= stotmp:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(stotmp:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(stotmp:PartNumberKey,'By Part Number',0)
  SELF.AddKey(stotmp:ReceivedPartNumberKey,'stotmp:ReceivedPartNumberKey',0)
  SELF.LazyOpen = False
  Access:STOCKRECEIVETMP &= SELF


Hide:Relate:STOCKRECEIVETMP.Init PROCEDURE
  CODE
  Hide:Access:STOCKRECEIVETMP.Init
  SELF.Init(Access:STOCKRECEIVETMP,1)


Hide:Access:STOCKRECEIVETMP.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:STOCKRECEIVETMP &= NULL


Hide:Relate:STOCKRECEIVETMP.Kill PROCEDURE

  CODE
  Hide:Access:STOCKRECEIVETMP.Kill
  PARENT.Kill
  Relate:STOCKRECEIVETMP &= NULL

