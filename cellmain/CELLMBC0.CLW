  MEMBER('cellmain.clw')

  INCLUDE('ABFILE.INC'),ONCE

  MAP
CELLMBC0:DctInit    PROCEDURE
CELLMBC0:DctKill    PROCEDURE
CELLMBC0:FilesInit  PROCEDURE
  END

Hide:Access:STDCHRGE CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:STDCHRGE CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:SOAPERRS CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:SOAPERRS CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBSE3   CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:JOBSE3   CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:INIDATA  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:INIDATA  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:EXCHPREP CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:EXCHPREP CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:EVO_HEAD CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:EVO_HEAD CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:STOAUUSE CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:STOAUUSE CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:PREJOB   CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:PREJOB   CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:APITOKEN CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:APITOKEN CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:PRIBAND  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:PRIBAND  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:TRADEAC2 CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:TRADEAC2 CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:AUDIT2   CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:AUDIT2   CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:SMSRECVD CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:SMSRECVD CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:TRAHUBAC CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:TRAHUBAC CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBSLOCK CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:JOBSLOCK CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:ASVACC   CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:ASVACC   CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:TagFile  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:TagFile  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:VETTREAS CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:VETTREAS CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:ErrorVCPJob CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:ErrorVCPJob CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:SRNTEXT  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:SRNTEXT  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

CELLMBC0:DctInit PROCEDURE
  CODE
  Relate:STDCHRGE &= Hide:Relate:STDCHRGE
  Relate:SOAPERRS &= Hide:Relate:SOAPERRS
  Relate:JOBSE3 &= Hide:Relate:JOBSE3
  Relate:INIDATA &= Hide:Relate:INIDATA
  Relate:EXCHPREP &= Hide:Relate:EXCHPREP
  Relate:EVO_HEAD &= Hide:Relate:EVO_HEAD
  Relate:STOAUUSE &= Hide:Relate:STOAUUSE
  Relate:PREJOB &= Hide:Relate:PREJOB
  Relate:APITOKEN &= Hide:Relate:APITOKEN
  Relate:PRIBAND &= Hide:Relate:PRIBAND
  Relate:TRADEAC2 &= Hide:Relate:TRADEAC2
  Relate:AUDIT2 &= Hide:Relate:AUDIT2
  Relate:SMSRECVD &= Hide:Relate:SMSRECVD
  Relate:TRAHUBAC &= Hide:Relate:TRAHUBAC
  Relate:JOBSLOCK &= Hide:Relate:JOBSLOCK
  Relate:ASVACC &= Hide:Relate:ASVACC
  Relate:TagFile &= Hide:Relate:TagFile
  Relate:VETTREAS &= Hide:Relate:VETTREAS
  Relate:ErrorVCPJob &= Hide:Relate:ErrorVCPJob
  Relate:SRNTEXT &= Hide:Relate:SRNTEXT

CELLMBC0:FilesInit PROCEDURE
  CODE
  Hide:Relate:STDCHRGE.Init
  Hide:Relate:SOAPERRS.Init
  Hide:Relate:JOBSE3.Init
  Hide:Relate:INIDATA.Init
  Hide:Relate:EXCHPREP.Init
  Hide:Relate:EVO_HEAD.Init
  Hide:Relate:STOAUUSE.Init
  Hide:Relate:PREJOB.Init
  Hide:Relate:APITOKEN.Init
  Hide:Relate:PRIBAND.Init
  Hide:Relate:TRADEAC2.Init
  Hide:Relate:AUDIT2.Init
  Hide:Relate:SMSRECVD.Init
  Hide:Relate:TRAHUBAC.Init
  Hide:Relate:JOBSLOCK.Init
  Hide:Relate:ASVACC.Init
  Hide:Relate:TagFile.Init
  Hide:Relate:VETTREAS.Init
  Hide:Relate:ErrorVCPJob.Init
  Hide:Relate:SRNTEXT.Init


CELLMBC0:DctKill PROCEDURE
  CODE
  Hide:Relate:STDCHRGE.Kill
  Hide:Relate:SOAPERRS.Kill
  Hide:Relate:JOBSE3.Kill
  Hide:Relate:INIDATA.Kill
  Hide:Relate:EXCHPREP.Kill
  Hide:Relate:EVO_HEAD.Kill
  Hide:Relate:STOAUUSE.Kill
  Hide:Relate:PREJOB.Kill
  Hide:Relate:APITOKEN.Kill
  Hide:Relate:PRIBAND.Kill
  Hide:Relate:TRADEAC2.Kill
  Hide:Relate:AUDIT2.Kill
  Hide:Relate:SMSRECVD.Kill
  Hide:Relate:TRAHUBAC.Kill
  Hide:Relate:JOBSLOCK.Kill
  Hide:Relate:ASVACC.Kill
  Hide:Relate:TagFile.Kill
  Hide:Relate:VETTREAS.Kill
  Hide:Relate:ErrorVCPJob.Kill
  Hide:Relate:SRNTEXT.Kill


Hide:Access:STDCHRGE.Init PROCEDURE
  CODE
  SELF.Init(STDCHRGE,GlobalErrors)
  SELF.Buffer &= sta:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(sta:Model_Number_Charge_Key,'By Charge Type',0)
  SELF.AddKey(sta:Charge_Type_Key,'By Charge Type',0)
  SELF.AddKey(sta:Unit_Type_Key,'By Unit Type',0)
  SELF.AddKey(sta:Repair_Type_Key,'By Repair Type',0)
  SELF.AddKey(sta:Cost_Key,'By Cost',0)
  SELF.AddKey(sta:Charge_Type_Only_Key,'sta:Charge_Type_Only_Key',0)
  SELF.AddKey(sta:Repair_Type_Only_Key,'sta:Repair_Type_Only_Key',0)
  SELF.AddKey(sta:Unit_Type_Only_Key,'sta:Unit_Type_Only_Key',0)
  SELF.LazyOpen = False
  Access:STDCHRGE &= SELF


Hide:Relate:STDCHRGE.Init PROCEDURE
  CODE
  Hide:Access:STDCHRGE.Init
  SELF.Init(Access:STDCHRGE,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:CHARTYPE)
  SELF.AddRelation(Relate:REPAIRTY)
  SELF.AddRelation(Relate:UNITTYPE)
  SELF.AddRelation(Relate:MANUFACT)


Hide:Access:STDCHRGE.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:STDCHRGE &= NULL


Hide:Relate:STDCHRGE.Kill PROCEDURE

  CODE
  Hide:Access:STDCHRGE.Kill
  PARENT.Kill
  Relate:STDCHRGE &= NULL


Hide:Access:SOAPERRS.Init PROCEDURE
  CODE
  SELF.Init(SOAPERRS,GlobalErrors)
  SELF.Buffer &= SOP:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(SOP:KeyRecordNumber,'SOP:KeyRecordNumber',1)
  SELF.AddKey(SOP:KeyErrorCode,'SOP:KeyErrorCode',0)
  SELF.LazyOpen = False
  Access:SOAPERRS &= SELF


Hide:Relate:SOAPERRS.Init PROCEDURE
  CODE
  Hide:Access:SOAPERRS.Init
  SELF.Init(Access:SOAPERRS,1)


Hide:Access:SOAPERRS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:SOAPERRS &= NULL


Hide:Relate:SOAPERRS.Kill PROCEDURE

  CODE
  Hide:Access:SOAPERRS.Kill
  PARENT.Kill
  Relate:SOAPERRS &= NULL


Hide:Access:JOBSE3.Init PROCEDURE
  CODE
  SELF.Init(JOBSE3,GlobalErrors)
  SELF.Buffer &= jobe3:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jobe3:KeyRecordNumber,'By Record Number',1)
  SELF.AddKey(jobe3:KeyRefNumber,'By Ref Number',0)
  SELF.LazyOpen = False
  Access:JOBSE3 &= SELF


Hide:Relate:JOBSE3.Init PROCEDURE
  CODE
  Hide:Access:JOBSE3.Init
  SELF.Init(Access:JOBSE3,1)


Hide:Access:JOBSE3.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBSE3 &= NULL


Hide:Relate:JOBSE3.Kill PROCEDURE

  CODE
  Hide:Access:JOBSE3.Kill
  PARENT.Kill
  Relate:JOBSE3 &= NULL


Hide:Access:INIDATA.Init PROCEDURE
  CODE
  SELF.Init(INIDATA,GlobalErrors)
  SELF.Buffer &= INI:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(INI:KeyRecordNo,'INI:KeyRecordNo',1)
  SELF.AddKey(INI:KeyFileSectionEntry,'INI:KeyFileSectionEntry',0)
  SELF.LazyOpen = False
  Access:INIDATA &= SELF


Hide:Relate:INIDATA.Init PROCEDURE
  CODE
  Hide:Access:INIDATA.Init
  SELF.Init(Access:INIDATA,1)


Hide:Access:INIDATA.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:INIDATA &= NULL


Hide:Relate:INIDATA.Kill PROCEDURE

  CODE
  Hide:Access:INIDATA.Kill
  PARENT.Kill
  Relate:INIDATA &= NULL


Hide:Access:EXCHPREP.Init PROCEDURE
  CODE
  SELF.Init(EXCHPREP,GlobalErrors)
  SELF.FileNameValue = 'EXCHPREP'
  SELF.Buffer &= EAP:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(EAP:LeyRefNumber,'EAP:LeyRefNumber',1)
  SELF.AddKey(EAP:KeyAudit,'EAP:KeyAudit',0)
  SELF.AddKey(EAP:KeyAuditUser,'EAP:KeyAuditUser',0)
  SELF.LazyOpen = False
  Access:EXCHPREP &= SELF


Hide:Relate:EXCHPREP.Init PROCEDURE
  CODE
  Hide:Access:EXCHPREP.Init
  SELF.Init(Access:EXCHPREP,1)


Hide:Access:EXCHPREP.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:EXCHPREP &= NULL


Hide:Relate:EXCHPREP.Kill PROCEDURE

  CODE
  Hide:Access:EXCHPREP.Kill
  PARENT.Kill
  Relate:EXCHPREP &= NULL


Hide:Access:EVO_HEAD.Init PROCEDURE
  CODE
  SELF.Init(EVO_HEAD,GlobalErrors)
  SELF.FileNameValue = 'EVO_HEAD'
  SELF.Buffer &= EVOH:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(EVOH:KeyRecord_number,'EVOH:KeyRecord_number',1)
  SELF.AddKey(EVOH:KeySentToEVO,'EVOH:KeySentToEVO',0)
  SELF.LazyOpen = False
  Access:EVO_HEAD &= SELF


Hide:Relate:EVO_HEAD.Init PROCEDURE
  CODE
  Hide:Access:EVO_HEAD.Init
  SELF.Init(Access:EVO_HEAD,1)


Hide:Access:EVO_HEAD.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:EVO_HEAD &= NULL


Hide:Relate:EVO_HEAD.Kill PROCEDURE

  CODE
  Hide:Access:EVO_HEAD.Kill
  PARENT.Kill
  Relate:EVO_HEAD &= NULL


Hide:Access:STOAUUSE.Init PROCEDURE
  CODE
  SELF.Init(STOAUUSE,GlobalErrors)
  SELF.Buffer &= STOU:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(STOU:KeyRecordNo,'STOU:KeyRecordNo',1)
  SELF.AddKey(STOU:KeyAuditTypeNo,'STOU:KeyAuditTypeNo',0)
  SELF.AddKey(STOU:KeyAuditTypeNoUser,'STOU:KeyAuditTypeNoUser',0)
  SELF.AddKey(STOU:KeyTypeNoPart,'STOU:KeyTypeNoPart',0)
  SELF.LazyOpen = False
  Access:STOAUUSE &= SELF


Hide:Relate:STOAUUSE.Init PROCEDURE
  CODE
  Hide:Access:STOAUUSE.Init
  SELF.Init(Access:STOAUUSE,1)


Hide:Access:STOAUUSE.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:STOAUUSE &= NULL


Hide:Relate:STOAUUSE.Kill PROCEDURE

  CODE
  Hide:Access:STOAUUSE.Kill
  PARENT.Kill
  Relate:STOAUUSE &= NULL


Hide:Access:PREJOB.Init PROCEDURE
  CODE
  SELF.Init(PREJOB,GlobalErrors)
  SELF.Buffer &= PRE:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(PRE:KeyRefNumber,'PRE:KeyRefNumber',1)
  SELF.AddKey(PRE:KeyAPIRecordNo,'PRE:KeyAPIRecordNo',0)
  SELF.AddKey(PRE:KeyJobRef_Number,'PRE:KeyJobRef_Number',0)
  SELF.AddKey(PRE:KeyESN,'PRE:KeyESN',0)
  SELF.AddKey(PRE:KeyDateTime,'PRE:KeyDateTime',0)
  SELF.AddKey(PRE:KeyESNDateTime,'PRE:KeyESNDateTime',0)
  SELF.AddKey(PRE:KeyPortalRef,'PRE:KeyPortalRef',0)
  SELF.LazyOpen = False
  Access:PREJOB &= SELF


Hide:Relate:PREJOB.Init PROCEDURE
  CODE
  Hide:Access:PREJOB.Init
  SELF.Init(Access:PREJOB,1)


Hide:Access:PREJOB.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:PREJOB &= NULL


Hide:Relate:PREJOB.Kill PROCEDURE

  CODE
  Hide:Access:PREJOB.Kill
  PARENT.Kill
  Relate:PREJOB &= NULL


Hide:Access:APITOKEN.Init PROCEDURE
  CODE
  SELF.Init(APITOKEN,GlobalErrors)
  SELF.FileNameValue = 'APITOKEN'
  SELF.Buffer &= API:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(API:KeyRecordNumber,'API:KeyRecordNumber',1)
  SELF.AddKey(API:KeyToken,'API:KeyToken',0)
  SELF.LazyOpen = False
  Access:APITOKEN &= SELF


Hide:Relate:APITOKEN.Init PROCEDURE
  CODE
  Hide:Access:APITOKEN.Init
  SELF.Init(Access:APITOKEN,1)


Hide:Access:APITOKEN.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:APITOKEN &= NULL


Hide:Relate:APITOKEN.Kill PROCEDURE

  CODE
  Hide:Access:APITOKEN.Kill
  PARENT.Kill
  Relate:APITOKEN &= NULL


Hide:Access:PRIBAND.Init PROCEDURE
  CODE
  SELF.Init(PRIBAND,GlobalErrors)
  SELF.Buffer &= prb:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(prb:KeyRecordNumber,'prb:KeyRecordNumber',1)
  SELF.AddKey(prb:KeyBandName,'prb:KeyBandName',0)
  SELF.AddKey(prb:KeyMinPrice,'prb:KeyMinPrice',0)
  SELF.LazyOpen = False
  Access:PRIBAND &= SELF


Hide:Relate:PRIBAND.Init PROCEDURE
  CODE
  Hide:Access:PRIBAND.Init
  SELF.Init(Access:PRIBAND,1)


Hide:Access:PRIBAND.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:PRIBAND &= NULL


Hide:Relate:PRIBAND.Kill PROCEDURE

  CODE
  Hide:Access:PRIBAND.Kill
  PARENT.Kill
  Relate:PRIBAND &= NULL


Hide:Access:TRADEAC2.Init PROCEDURE
  CODE
  SELF.Init(TRADEAC2,GlobalErrors)
  SELF.FileNameValue = 'TRADEAC2'
  SELF.Buffer &= TRA2:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(TRA2:KeyRecordNo,'TRA2:KeyRecordNo',1)
  SELF.AddKey(TRA2:KeyAccountNumber,'By Account Number',0)
  SELF.AddKey(TRA2:KeyPreBookRegion,'TRA2:KeyPreBookRegion',0)
  SELF.LazyOpen = False
  Access:TRADEAC2 &= SELF


Hide:Relate:TRADEAC2.Init PROCEDURE
  CODE
  Hide:Access:TRADEAC2.Init
  SELF.Init(Access:TRADEAC2,1)


Hide:Access:TRADEAC2.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:TRADEAC2 &= NULL


Hide:Relate:TRADEAC2.Kill PROCEDURE

  CODE
  Hide:Access:TRADEAC2.Kill
  PARENT.Kill
  Relate:TRADEAC2 &= NULL


Hide:Access:AUDIT2.Init PROCEDURE
  CODE
  SELF.Init(AUDIT2,GlobalErrors)
  SELF.Buffer &= aud2:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(aud2:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(aud2:AUDRecordNumberKey,'Link To AUDIT',0)
  SELF.LazyOpen = False
  Access:AUDIT2 &= SELF


Hide:Relate:AUDIT2.Init PROCEDURE
  CODE
  Hide:Access:AUDIT2.Init
  SELF.Init(Access:AUDIT2,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:AUDIT)


Hide:Access:AUDIT2.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:AUDIT2 &= NULL


Hide:Relate:AUDIT2.Kill PROCEDURE

  CODE
  Hide:Access:AUDIT2.Kill
  PARENT.Kill
  Relate:AUDIT2 &= NULL


Hide:Access:SMSRECVD.Init PROCEDURE
  CODE
  SELF.Init(SMSRECVD,GlobalErrors)
  SELF.Buffer &= SMR:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(SMR:KeyRecordNo,'SMR:KeyRecordNo',1)
  SELF.AddKey(SMR:KeyJob_Ref_Number,'SMR:KeyJob_Ref_Number',0)
  SELF.AddKey(SMR:KeyJob_Date_Time_Dec,'SMR:KeyJob_Date_Time_Dec',0)
  SELF.AddKey(SMR:KeyAccount_Date_Time,'SMR:KeyAccount_Date_Time',0)
  SELF.LazyOpen = False
  Access:SMSRECVD &= SELF


Hide:Relate:SMSRECVD.Init PROCEDURE
  CODE
  Hide:Access:SMSRECVD.Init
  SELF.Init(Access:SMSRECVD,1)


Hide:Access:SMSRECVD.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:SMSRECVD &= NULL


Hide:Relate:SMSRECVD.Kill PROCEDURE

  CODE
  Hide:Access:SMSRECVD.Kill
  PARENT.Kill
  Relate:SMSRECVD &= NULL


Hide:Access:TRAHUBAC.Init PROCEDURE
  CODE
  SELF.Init(TRAHUBAC,GlobalErrors)
  SELF.Buffer &= TRA1:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(TRA1:RecordNoKey,'TRA1:RecordNoKey',1)
  SELF.AddKey(TRA1:HeadAccSubAccKey,'TRA1:HeadAccSubAccKey',0)
  SELF.AddKey(TRA1:HeadAccSubAccNameKey,'TRA1:HeadAccSubAccNameKey',0)
  SELF.AddKey(TRA1:HeadAccSubAccBranchKey,'TRA1:HeadAccSubAccBranchKey',0)
  SELF.LazyOpen = False
  Access:TRAHUBAC &= SELF


Hide:Relate:TRAHUBAC.Init PROCEDURE
  CODE
  Hide:Access:TRAHUBAC.Init
  SELF.Init(Access:TRAHUBAC,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:TRADEACC)


Hide:Access:TRAHUBAC.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:TRAHUBAC &= NULL


Hide:Relate:TRAHUBAC.Kill PROCEDURE

  CODE
  Hide:Access:TRAHUBAC.Kill
  PARENT.Kill
  Relate:TRAHUBAC &= NULL


Hide:Access:JOBSLOCK.Init PROCEDURE
  CODE
  SELF.Init(JOBSLOCK,GlobalErrors)
  SELF.Buffer &= lock:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(lock:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(lock:JobNumberKey,'By Job Number',0)
  SELF.LazyOpen = False
  Access:JOBSLOCK &= SELF


Hide:Relate:JOBSLOCK.Init PROCEDURE
  CODE
  Hide:Access:JOBSLOCK.Init
  SELF.Init(Access:JOBSLOCK,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:JOBSLOCK.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBSLOCK &= NULL


Hide:Access:JOBSLOCK.PrimeFields PROCEDURE

  CODE
  lock:DateLocked = Today()
  lock:TimeLocked = Clock()
  PARENT.PrimeFields


Hide:Relate:JOBSLOCK.Kill PROCEDURE

  CODE
  Hide:Access:JOBSLOCK.Kill
  PARENT.Kill
  Relate:JOBSLOCK &= NULL


Hide:Access:ASVACC.Init PROCEDURE
  CODE
  SELF.Init(ASVACC,GlobalErrors)
  SELF.Buffer &= ASV:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(ASV:RecordNoKey,'ASV:RecordNoKey',1)
  SELF.AddKey(ASV:TradeACCManufKey,'ASV:TradeACCManufKey',0)
  SELF.LazyOpen = False
  Access:ASVACC &= SELF


Hide:Relate:ASVACC.Init PROCEDURE
  CODE
  Hide:Access:ASVACC.Init
  SELF.Init(Access:ASVACC,1)


Hide:Access:ASVACC.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:ASVACC &= NULL


Hide:Relate:ASVACC.Kill PROCEDURE

  CODE
  Hide:Access:ASVACC.Kill
  PARENT.Kill
  Relate:ASVACC &= NULL


Hide:Access:TagFile.Init PROCEDURE
  CODE
  SELF.Init(TagFile,GlobalErrors)
  SELF.FileName &= glo:TagFile
  SELF.Buffer &= tag:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(tag:keyTagged,'tag:keyTagged',0)
  SELF.AddKey(tag:keyID,'tag:keyID',0)
  SELF.LazyOpen = False
  Access:TagFile &= SELF


Hide:Relate:TagFile.Init PROCEDURE
  CODE
  Hide:Access:TagFile.Init
  SELF.Init(Access:TagFile,1)


Hide:Access:TagFile.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:TagFile &= NULL


Hide:Relate:TagFile.Kill PROCEDURE

  CODE
  Hide:Access:TagFile.Kill
  PARENT.Kill
  Relate:TagFile &= NULL


Hide:Access:VETTREAS.Init PROCEDURE
  CODE
  SELF.Init(VETTREAS,GlobalErrors)
  SELF.Buffer &= vet:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(vet:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(vet:VettingReasonKey,'By Vetting Reason',0)
  SELF.LazyOpen = False
  Access:VETTREAS &= SELF


Hide:Relate:VETTREAS.Init PROCEDURE
  CODE
  Hide:Access:VETTREAS.Init
  SELF.Init(Access:VETTREAS,1)


Hide:Access:VETTREAS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:VETTREAS &= NULL


Hide:Relate:VETTREAS.Kill PROCEDURE

  CODE
  Hide:Access:VETTREAS.Kill
  PARENT.Kill
  Relate:VETTREAS &= NULL


Hide:Access:ErrorVCPJob.Init PROCEDURE
  CODE
  SELF.Init(ErrorVCPJob,GlobalErrors)
  SELF.Buffer &= err:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.LazyOpen = False
  Access:ErrorVCPJob &= SELF


Hide:Relate:ErrorVCPJob.Init PROCEDURE
  CODE
  Hide:Access:ErrorVCPJob.Init
  SELF.Init(Access:ErrorVCPJob,1)


Hide:Access:ErrorVCPJob.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:ErrorVCPJob &= NULL


Hide:Relate:ErrorVCPJob.Kill PROCEDURE

  CODE
  Hide:Access:ErrorVCPJob.Kill
  PARENT.Kill
  Relate:ErrorVCPJob &= NULL


Hide:Access:SRNTEXT.Init PROCEDURE
  CODE
  SELF.Init(SRNTEXT,GlobalErrors)
  SELF.Buffer &= srn:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(srn:KeyRefNo,'srn:KeyRefNo',1)
  SELF.AddKey(srn:KeySRN,'srn:KeySRN',0)
  SELF.LazyOpen = False
  Access:SRNTEXT &= SELF


Hide:Relate:SRNTEXT.Init PROCEDURE
  CODE
  Hide:Access:SRNTEXT.Init
  SELF.Init(Access:SRNTEXT,1)


Hide:Access:SRNTEXT.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:SRNTEXT &= NULL


Hide:Relate:SRNTEXT.Kill PROCEDURE

  CODE
  Hide:Access:SRNTEXT.Kill
  PARENT.Kill
  Relate:SRNTEXT &= NULL

