

   MEMBER('vodr0052.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('VODR0001.INC'),ONCE        !Local module procedure declarations
                     END


CostAdjustmentReport PROCEDURE                        !Generated from procedure template - Window

tmp:VersionNumber    STRING(30)
Automatic            BYTE
Parameter_Group      GROUP,PRE(param)
StartDate            DATE
EndDate              DATE
                     END
Debug_Group          GROUP,PRE(debug)
Active               LONG
Count                LONG(0)
                     END
ExcelStuff           GROUP,PRE()
Excel                SIGNED !OLE Automation holder
excel:ColumnName     STRING(50)
excel:ColumnWidth    REAL
excel:CommentText    STRING(255)
excel:DateFormat     STRING('dd mmm yyyy')
excel:OperatingSystem REAL
excel:Visible        BYTE
                     END
Local_Stuff          GROUP,PRE(LOC)
Account_Name         STRING(100)
AccountNumber        STRING(15)
ApplicationName      STRING('ServiceBase')
CompanyName          STRING(30)
Courier              STRING(30)
DesktopPath          STRING(255)
EmailSubject         STRING(200)
FileName             STRING(255)
JobNumber            LONG
LastColumn           STRING('L')
LineCost             DECIMAL(7,2)
MainStore            STRING(3)
Path                 STRING('C:\ {252}')
ProgramName          STRING(100)
SectionName          STRING(100)
Text                 STRING(255)
UserCode             STRING(3)
UserName             STRING(100)
VATCode              STRING(2)
Version              STRING('3.1.000 {1}')
                     END
ProgressBar_Stuff    GROUP,PRE()
Progress:Text        STRING(100)
RecordCount          LONG
                     END
Misc_Group           GROUP,PRE()
OPTION1              SHORT
Result               BYTE
AccountChange        BYTE
tmp:FirstModel       STRING(30)
StockOK              BYTE
InvoiceOK            BYTE
                     END
Worksheet_Stuff      GROUP,PRE(sheet)
HeadSummaryRow       LONG(9)
TempLastCol          STRING(1)
HeadLastCol          STRING('Q')
DataLastCol          STRING('Q')
DataSectionRow       LONG(9)
DataHeaderRow        LONG(11)
                     END
Clipboard_Group      GROUP,PRE(clip)
OriginalValue        STRING(255)
Saved                BYTE
Value                CSTRING(1024)
                     END
HeaderQueue          QUEUE,PRE(head)
ColumnName           STRING(30)
ColumnWidth          REAL
NumberFormat         STRING(20)
                     END
HTML_Queue           QUEUE,PRE(html)
Line                 STRING(1024)
                     END
LocationBrowse_Group GROUP,PRE()
LocationValue        STRING(30)
LocationTag          STRING(1)
LocationCount        LONG
LocationChange       BYTE
                     END
Location_Queue       QUEUE,PRE(locQ)
Location             STRING(30)
Report               LONG
SheetName            STRING(30)
RecordCount          LONG
                     END
SummaryQueue         QUEUE,PRE(sq)
Location             STRING(30),NAME('Location')
PartNumber           STRING(30),NAME('PartNumber')
Manufacturer         STRING(30),NAME('Manufacturer')
Description          STRING(30),NAME('Description')
Notes                STRING(255)
Quantity             REAL
QuantityIn           REAL
QuantityOut          REAL
AmountIn             REAL
AmountOut            REAL
Price                REAL
QuantityStock        REAL
QuantityOnOrder      REAL
                     END
Calculation_Group    GROUP,PRE(calc)
TrnNumber            LONG
TrnDate              STRING(20)
Reason               STRING(3)
Notes                STRING(50)
Location             STRING(30),NAME('Location')
PartNumber           STRING(30),NAME('PartNumber')
Manufacturer         STRING(30),NAME('Manufacturer')
Description          STRING(30),NAME('Description')
Sign                 BYTE
Quantity             REAL
QuantityIn           REAL
QuantityOut          REAL
Amount               REAL
LineAmount           REAL
                     END
tmp:old_cost         REAL
tmp:new_cost         REAL
tmp:qty              REAL
MainWindow           WINDOW('Export'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(240,146,200,144),USE(?PanelMain),FILL(0D6EAEFH)
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,164,192,44),USE(?PanelTip),FILL(0D6EAEFH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(396,172,36,32),USE(?ButtonHelp),TRN,FLAT,ICON('F1Help.jpg')
                       STRING(@s255),AT(252,184,140,),USE(SRN:TipText),TRN
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Cost Adjustment Report Criteria'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       GROUP('Top Tip'),AT(248,168,184,36),USE(?GroupTip),BOXED,TRN
                         SHEET,AT(244,212,192,42),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                           TAB('Criteria'),USE(?Tab1)
                             STRING('Start Date'),AT(248,218),USE(?String4),LEFT,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             ENTRY(@D6),AT(328,218,64,10),USE(param:StartDate),IMM,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),TIP('Enter the earliest exchange date to show on report'),REQ
                             STRING('End Date'),AT(248,234),USE(?String5),LEFT,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             ENTRY(@D6),AT(328,234,64,10),USE(param:EndDate),IMM,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),TIP('Enter the latest exchange date to show on report'),REQ
                             PROMPT('Report Version'),AT(248,244),USE(?ReportVersion),TRN,FONT(,7,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             BUTTON,AT(400,230),USE(?PopCalendarEnd),IMM,TRN,FLAT,LEFT,TIP('Click to show Calendar'),ICON('lookupp.jpg')
                             BUTTON,AT(400,214),USE(?PopCalendar),IMM,TRN,FLAT,LEFT,TIP('Click to show Calendar<13,10>'),ICON('lookupp.jpg')
                             CHECK('Show Excel'),AT(248,266,52,12),USE(excel:Visible),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH),TIP('Tick To Show Excel During Report Creation.<13,10>Only Tick if you are having problems' &|
   ' <13,10>with Excel reports.'),VALUE('1','0')
                           END
                           TAB('Repair Categories'),USE(?Tab2)
                           END
                         END
                         BUTTON,AT(304,258),USE(?OkButton),TRN,FLAT,LEFT,TIP('Click to print report'),ICON('printp.jpg'),DEFAULT
                         BUTTON,AT(368,258),USE(?CancelButton),TRN,FLAT,LEFT,TIP('Click to close this form'),ICON('cancelp.jpg')
                       END
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!-----------------------------------------------
    MAP
CheckLocation           PROCEDURE( STRING ), LONG ! BOOL
CreateLocationSheet            PROCEDURE( STRING, LONG )
DateToString PROCEDURE( DATE ), STRING
LoadJOBS PROCEDURE( LONG ), LONG ! BOOL
LoadLOCATION  PROCEDURE( STRING ), LONG ! BOOL
LoadORDERS  PROCEDURE( LONG ), LONG ! BOOL
LoadSTOCK  PROCEDURE( LONG ), LONG ! BOOL
MemberOfSummaryQueue    PROCEDURE(STRING), LONG ! BOOL
NumberToColumn PROCEDURE(LONG), STRING
SetSheetTo          PROCEDURE( STRING ), LONG ! BOOL
WriteColumn PROCEDURE( STRING, LONG=False )

WriteDebug  PROCEDURE( STRING )
    END !MAP
!-----------------------------------------------
! ProgressWindow Declarations
 
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte
 
progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY,DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
 
!---   Excel EQUATES   -------------------------------------------------------------
xlCalculationManual    EQUATE(0FFFFEFD9h) ! XlCalculation
xlCalculationAutomatic EQUATE(0FFFFEFF7h) ! XlCalculation
!----------------------------------------------------

!----------------------------------------------------
xlNone       EQUATE(0FFFFEFD2h)
xlContinuous EQUATE(        1 ) ! xlFillStyle
xlThin       EQUATE(        2 ) ! XlBorderWeight
!----------------------------------------------------

!----------------------------------------------------
xlAutomatic   EQUATE(0FFFFEFF7h) ! Constants.
xlSolid       EQUATE(        1 ) ! Constants.
xlLeft        EQUATE(0FFFFEFDDh) ! Constants.
xlRight       EQUATE(0FFFFEFC8h) ! Constants.
xlCenter      EQUATE(0FFFFEFF4h) ! Constants.
xlLastCell    EQUATE(       11 ) ! Constants.
xlTop         EQUATE(0FFFFEFC0h) ! Constants.
xlBottom      EQUATE(0FFFFEFF5h) ! Constants.

xlTopToBottom EQUATE(        1 ) ! Constants.
!----------------------------------------------------

!----------------------------------------------------
xlDiagonalDown     EQUATE( 5) ! XlBordersIndex
xlDiagonalUp       EQUATE( 6) ! XlBordersIndex
xlEdgeLeft         EQUATE( 7) ! XlBordersIndex
xlEdgeTop          EQUATE( 8) ! XlBordersIndex
xlEdgeBottom       EQUATE( 9) ! XlBordersIndex
xlEdgeRight        EQUATE(10) ! XlBordersIndex
xlInsideVertical   EQUATE(11) ! XlBordersIndex
xlInsideHorizontal EQUATE(12) ! XlBordersIndex
!----------------------------------------------------

!----------------------------------------------------
xlA1         EQUATE(        1 ) ! XlReferenceStyle
xlR1C1       EQUATE(0FFFFEFCAh) ! XlReferenceStyle
!----------------------------------------------------

!----------------------------------------------------
xlFilterCopy    EQUATE(2) ! XlFilterAction
xlFilterInPlace EQUATE(1) ! XlFilterAction
!----------------------------------------------------

!----------------------------------------------------
xlGuess EQUATE(0) ! XlYesNoGuess
xlYes   EQUATE(1) ! XlYesNoGuess
xlNo    EQUATE(2) ! XlYesNoGuess
!----------------------------------------------------

!----------------------------------------------------
xlAscending  EQUATE(1) ! XlSortOrder
xlDescending EQUATE(2) ! XlSortOrder
!----------------------------------------------------

!----------------------------------------------------
xlLandscape EQUATE(2) ! XlPageOrientation
xlPortrait  EQUATE(1) ! XlPageOrientation
!----------------------------------------------------

!----------------------------------------------------
xlDownThenOver EQUATE( 1 ) ! XlOrder
xlOverThenDown EQUATE( 2 ) ! XlOrder
!-----------------------------------------------------------------------------------
! Office EQUATES

!----------------------------------------------------
msoCTrue          EQUATE(        1 ) ! MsoTriState
msoFalse          EQUATE(        0 ) ! MsoTriState
msoTriStateMixed  EQUATE(0FFFFFFFEh) ! MsoTriState
msoTriStateToggle EQUATE(0FFFFFFFDh) ! MsoTriState
msoTrue           EQUATE(0FFFFFFFFh) ! MsoTriState
!----------------------------------------------------

!----------------------------------------------------
msoScaleFromBottomRight EQUATE(2) ! MsoScaleFrom
msoScaleFromMiddle      EQUATE(1) ! MsoScaleFrom
msoScaleFromTopLeft     EQUATE(0) ! MsoScaleFrom
!----------------------------------------------------

!----------------------------------------------------
msoPropertyTypeBoolean EQUATE(2) ! MsoDocProperties
msoPropertyTypeDate    EQUATE(3) ! MsoDocProperties
msoPropertyTypeFloat   EQUATE(5) ! MsoDocProperties
msoPropertyTypeNumber  EQUATE(1) ! MsoDocProperties
msoPropertyTypeString  EQUATE(4) ! MsoDocProperties
!----------------------------------------------------
CommandLine STRING(255)
tmpPos      LONG
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!-----------------------------------------------
OKButton_Pressed                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        CancelPressed = False
        debug:Active = True

        !-----------------------------------------------
        ! 23 Oct 2002 John
        ! R003, berrjo :Insert the report exe name EG VODR56.V01
        ! Do not attach the EXE extenstion but show the version number.  Increment the version number each time changed.
        ! Make this standard on all reports sent for correction.
        ! LOC:Version STRING(8) 3.1.0000 3.1 to match sevicebase version (and dictionary) .0000 increment before each compile
        !
        !-----------------------------------------------
        LocationQueue.LocationValue = 'MAIN STORE'
        ADD(LocationQueue)
        !-----------------------------------------------------------------
        DO ExportSetup
        DO ExportBody
        DO ExportFinalize

        If Command('/SCHEDULE')
            If Access:REPSCHLG.PrimeRecord() = Level:Benign
                rlg:REPSCHEDRecordNumber = rpd:RecordNumber
                rlg:Information = 'Report Name: ' & Clip(rpd:ReportName) & ' - ' & Clip(rpd:ReportCriteriaType) & |
                                  '<13,10>Report Finished'
                If Access:REPSCHLG.TryInsert() = Level:Benign
                    !Insert
                Else ! If Access:REPSCHLG.TryInsert() = Level:Benign
                    Access:REPSCHLG.CancelAutoInc()
                End ! If Access:REPSCHLG.TryInsert() = Level:Benign
            End ! If Access.REPSCHLG.PrimeRecord() = Level:Benign
        End

      
        POST(Event:CloseWindow)
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
ExportSetup                             ROUTINE
    DATA
temp DATE
    CODE
        !-----------------------------------------------------------------
        WriteDebug('ExportSetup')
        !-----------------------------------------------------------------
        IF RECORDS(LocationQueue) < 1
            !POST(Event:CloseWindow)
            CancelPressed = True

            EXIT
        END !IF
        !-----------------------------------------------------------------
        DO GetFileName
        IF CLIP(LOC:FileName) = ''
            DO LoadFileDialog
        END !IF

        IF LOC:FileName = ''

            CancelPressed = True

            EXIT
        END !IF LOC:FileName = ''

        IF LOWER(RIGHT(LOC:FileName, 4)) <> '.xls'
            LOC:FileName = CLIP(LOC:FileName) & '.xls'
        END !IF
        !-----------------------------------------------------------------
        SETCURSOR(CURSOR:Wait)

        DO ProgressBar_Setup
        !-----------------------------------------------------------------
        DO XL_Setup

        DO XL_AddWorkbook
        !-----------------------------------------------------------------
        DO InitColumns
        DO CreateLocationSheets
        !-----------------------------------------------------------------
    EXIT
ExportBody                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        WriteDebug('CreateLocationSheets')

        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        DO CreateTitleSheet
        DO CreateDataSheet
        !DO WriteHeadSummary

        DO SaveAllRRCSheets
        !-----------------------------------------------------------------
    EXIT
ExportFinalize                          ROUTINE
    DATA
FilenameLength LONG
StartAt        LONG
SUBLength      LONG
    CODE
        !-----------------------------------------------------------------
        WriteDebug('ExportFinalize')

        IF CancelPressed
            DO XL_DropAllWorksheets
            DO XL_Finalize

            EXIT
        END !IF
        !-----------------------------------------------------------------
        !
        Excel{'Sheets("Sheet3").Select'}
        IF Excel{'ActiveSheet.Name'} = 'Sheet3'
            Excel{'ActiveWindow.SelectedSheets.Delete'}
        END !IF

        Excel{'Sheets("Summary").Select'}
        Excel{'Range("A1").Select'}

        Excel{'Application.ActiveWorkBook.SaveAs("' & LEFT(CLIP(LOC:FileName)) & '")'}
        Excel{'Application.ActiveWorkBook.Close()'}
        !-----------------------------------------------------------------
        DO XL_Finalize

        DO ProgressBar_Finalise
        !-----------------------------------------------------------------
        SETCURSOR()
        IF AUtomatic = FALSE
          IF MATCH(LOC:Filename, CLIP(LOC:DesktopPath) & '*')
              FilenameLength = LEN(CLIP(LOC:Filename   ))
              StartAt        = LEN(CLIP(LOC:DesktopPath)) + 1
              SUBLength      = FilenameLength - StartAt + 1

          ELSE
          END !IF
        END
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
CreateTitleSheet                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        EXIT
        WriteDebug('CreateTitleSheet')
        !-----------------------------------------------------------------
        LOC:Text          = 'Summary'
        sheet:TempLastCol = sheet:HeadLastCol

        DO CreateWorksheetHeader
        !-----------------------------------------------------------------
        DO XL_SetWorksheetPortrait

        Excel{'ActiveSheet.Columns("A:C").ColumnWidth'} = 20
        Excel{'ActiveSheet.Columns("D:' & sheet:HeadLastCol & '").ColumnWidth'} = 10

        Excel{'ActiveSheet.Columns("E:E").ColumnWidth'} = 12
        Excel{'ActiveSheet.Columns("I:I").ColumnWidth'} = 12
        Excel{'ActiveSheet.Columns("N:N").ColumnWidth'} = 12
        Excel{'ActiveSheet.Columns("Q:Q").ColumnWidth'} = 12
        !-----------------------------------------------------------------
        Excel{'Range("A' & sheet:HeadSummaryRow & ':' & sheet:HeadLastCol & sheet:HeadSummaryRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A' & sheet:HeadSummaryRow & '").Select'}
            DO XL_SetBold
            Excel{'ActiveCell.Formula'} = 'Summary'
        !-----------------------------------------------------------------
        ! Prepare For summary details
        !
        !Excel{'Range("A' & sheet:HeadSummaryRow+1 & '").Select'}
        !-----------------------------------------------------------------
    EXIT
CreateDataSheet                                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        WriteDebug('CreateDataSheet')

        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        Progress:Text    = 'Checking Stock History'
        RecordsToProcess = RECORDS(Stock)/RECORDS(LocationQueue)

        DO ProgressBar_LoopPre
        !-----------------------------------------------------------------

        LOOP j# = 1 TO RECORDS(LocationQueue)
          GET(LocationQueue,j#)
          Access:Stock.ClearKey(sto:Location_Key)
          sto:Location = LocationQueue.LocationValue
          SET(sto:location_key,sto:location_key)
          LOOP
            IF Access:Stock.Next()
              BREAK
            END
            IF sto:Location <> LocationQueue.LocationValue
              BREAK
            END
            !-------------------------------------------------------------
            DO ProgressBar_Loop
            IF CancelPressed
                EXIT
            END !IF
            !-------------------------------------------------------------
            Access:STOHIST.ClearKey(shi:Ref_Number_Key)
            shi:ref_number = sto:Ref_Number
            shi:Date = param:StartDate
            SET(shi:Ref_Number_Key,shi:Ref_Number_Key)
            LOOP
              IF Access:STOHIST.NEXT()
                BREAK
              END
              IF shi:ref_number <> sto:Ref_Number
                BREAK
              END
              IF shi:Date > param:EndDate
                BREAK
              END !IF
              IF CLIP(shi:Notes) <> 'PREVIOUS DETAILS'
                CYCLE
              END
              DO WriteColumns
            END
          END  !-------------------------------------------------------------
        END !LOOP

        DO ProgressBar_LoopPost
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        DO WriteDataSummary
        !-----------------------------------------------------------------
    EXIT
CreateDataSheet2                                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        WriteDebug('CreateDataSheet')

        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        Progress:Text    = 'Checking Stock History'
        RecordsToProcess = 100 * (param:EndDate - param:StartDate + 1)

        DO ProgressBar_LoopPre
        !-----------------------------------------------------------------
        Access:STOHIST.ClearKey(shi:DateKey)
            shi:Date = param:StartDate
        SET(shi:DateKey, shi:DateKey)

        LOOP WHILE Access:STOHIST.NEXT() = Level:benign
            !-------------------------------------------------------------
            DO ProgressBar_Loop
            IF CancelPressed
                EXIT
            END !IF
            !-------------------------------------------------------------
            IF shi:Date > param:EndDate
                BREAK
            END !IF

            DO WriteColumns
            !-------------------------------------------------------------
        END !LOOP

        DO ProgressBar_LoopPost
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        DO WriteDataSummary
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
CreateLocationSheets                                             ROUTINE
    DATA
Count LONG
    CODE
        !-----------------------------------------------------------------
        WriteDebug('CreateLocationSheets')

        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        Count = RECORDS(LocationQueue) !LocationQueue)
        Progress:Text    = 'Creating Location Sheets'
        RecordsToProcess = Count

        DO ProgressBar_LoopPre
        !-----------------------------------------------------------------
        LOOP u# = 1 TO Count+1
            !-------------------------------------------------------------
            DO ProgressBar_Loop
            IF CancelPressed
                EXIT
            END !IF
            !-------------------------------------------------------------
            GET(LocationQueue, u#)

            WriteDebug('CreateLocationSheets(' & x# & ', "' & CLIP(LocationQueue.LocationValue) & '")')

            CreateLocationSheet(LocationQueue.LocationValue, True)
            !-------------------------------------------------------------
        END !LOOP

        DO ProgressBar_LoopPost
        IF CancelPressed
            EXIT
        END !IF

        WriteDebug('CreateLocationSheets(END)')
        !-----------------------------------------------------------------
    EXIT
CreateSectionHeader                                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOC:Text          = CLIP(LOC:SectionName)
        sheet:TempLastCol = sheet:DataLastCol

        DO CreateWorksheetHeader
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataSectionRow & ':' & sheet:DataLastCol & sheet:DataSectionRow &'").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A' & sheet:DataSectionRow & '").Select'}
            Excel{'ActiveCell.Formula'}     = 'Section Name:'

        Excel{'Range("B' & sheet:DataSectionRow & '").Select'}
                Excel{'ActiveCell.Formula'} = CLIP(LOC:SectionName)
                DO XL_SetBold
                DO XL_HorizontalAlignmentLeft

        Excel{'Range("D' & sheet:DataSectionRow & '").Select'}
            Excel{'ActiveCell.Formula'}     = 'Total Records:'
            DO XL_HorizontalAlignmentRight

        Excel{'Range("E' & sheet:DataSectionRow & '").Select'}
                Excel{'ActiveCell.Formula'} = 0
                DO XL_HorizontalAlignmentLeft

        Excel{'Range("F' & sheet:DataSectionRow & '").Select'}
            Excel{'ActiveCell.Formula'}     = 'Showing:'
            DO XL_HorizontalAlignmentRight

        Excel{'Range("G' & sheet:DataSectionRow & '").Select'}
                Excel{'ActiveCell.Formula'} = 0
                DO XL_HorizontalAlignmentLeft
        !-----------------------------------------------
        DO SetColumns
        !-----------------------------------------------
    EXIT
CreateWorksheetHeader                                                               ROUTINE
    DATA
CurrentRow LONG(3)
    CODE
        !-----------------------------------------------
        DO CheckSheetName ! remove dodgy characters

        Excel{'ActiveSheet.Name'} = LEFT(CLIP(LOC:Text), 30)
        !-----------------------------------------------       
        Excel{'Range("A1:' & sheet:TempLastCol & '1").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A1").Select'}
            Excel{'ActiveCell.Font.Size'}        = 14
            DO XL_SetBold
            Excel{'ActiveCell.Formula'}          = CLIP(LOC:ProgramName)

        Excel{'Range("D3").Select'}
            Excel{'ActiveCell.Formula'}   = Clip(tmp:VersionNumber)
            DO XL_SetBold
            Excel{'ActiveCell.Font.Size'} = 8
        !-----------------------------------------------
        Excel{'Range("A' & CurrentRow & ':' & sheet:TempLastCol & CurrentRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A' & CurrentRow & '").Select'}
            DO XL_SetBold
            Excel{'ActiveCell.Formula'}          = 'Criteria'
        !-----------------------------------------------
        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.Formula'}          = 'Stock Adjustment Start Date:'
                DO XL_ColRight
                    Excel{'ActiveCell.Formula'}      = DateToString(param:StartDate)
                    DO XL_FormatDate
                    DO XL_HorizontalAlignmentLeft

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.Formula'}          = 'Stock Adjustment End Date:'
                DO XL_ColRight
                    Excel{'ActiveCell.Formula'}      = DateToString(param:EndDate)
                    DO XL_FormatDate
                    DO XL_HorizontalAlignmentLeft

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.Formula'}          = 'Created By:'
                DO XL_ColRight
                    Excel{'ActiveCell.Formula'}      = LOC:UserName
                    DO XL_HorizontalAlignmentLeft

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.Formula'}          = 'Created Date:'
                DO XL_ColRight
                    Excel{'ActiveCell.Formula'}      = DateToString(TODAY())
                    DO XL_FormatDate
                    DO XL_HorizontalAlignmentLeft

        Excel{'Range("A4:' & sheet:TempLastCol & CurrentRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------
        sheet:DataSectionRow = CurrentRow + 2
        sheet:DataHeaderRow  = CurrentRow + 4

        sheet:HeadSummaryRow = CurrentRow + 2
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
InitColumns                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        WriteDebug('InitColumns')

        FREE(HeaderQueue)

        head:ColumnName       = 'Trn Number'
            head:ColumnWidth  = 25.00
            head:NumberFormat = '0000' ! chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Trn Date'
            head:ColumnWidth  = 12.00
            head:NumberFormat = excel:DateFormat
            ADD(HeaderQueue)

        head:ColumnName       = 'Part Number'
            head:ColumnWidth  = 15.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Description'
            head:ColumnWidth  = 15.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Quantity'
            head:ColumnWidth  = 10.00
            head:NumberFormat = '###0'
            ADD(HeaderQueue)

        head:ColumnName       = 'Adjustment Amount'
            head:ColumnWidth  = 20.00
            head:NumberFormat = '###0.00'
            ADD(HeaderQueue)

        head:ColumnName       = 'Line Amount'
            head:ColumnWidth  = 12.00
            head:NumberFormat = '###0.00'
            ADD(HeaderQueue)
        !-----------------------------------------------
        sheet:DataLastCol = NumberToColumn(RECORDS(HeaderQueue))
        !-----------------------------------------------
    EXIT
FormatColumns                                                   ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        WriteDebug('FormatColumns')

        IF RecordCount < 1
            EXIT
        END !IF
        !-----------------------------------------------
        LOOP x# = 1 TO RECORDS(HeaderQueue)
            !-------------------------------------------
            GET(HeaderQueue, x#)
            IF ERRORCODE()
                CYCLE
            END !IF

            Excel{'Range("' & CHR(64+x#) & sheet:DataHeaderRow+1 & ':' & CHR(64+x#) & sheet:DataHeaderRow+RecordCount & '").Select'}
                Excel{'Selection.NumberFormat'} = head:NumberFormat
                DO XL_HorizontalAlignmentRight
            !-------------------------------------------
        END !LOOP
        !-----------------------------------------------
    EXIT
SetColumns                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataHeaderRow & ':' & sheet:DataLastCol & sheet:DataHeaderRow & '").Select'}
            DO XL_SetColumnHeader
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataHeaderRow  & '").Select'}

        LOOP x# = 1 TO RECORDS(HeaderQueue)
            GET(HeaderQueue, x#)

            Excel{'ActiveCell.Formula'}         = head:ColumnName
                Excel{'ActiveCell.ColumnWidth'} = head:ColumnWidth
                DO XL_ColRight
        END !LOOP
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataHeaderRow+1  & '").Select'}
            Excel{'ActiveWindow.FreezePanes'} = True
        !-----------------------------------------------
    EXIT
WriteColumns                                                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        WriteDebug('WriteColumns')

        tmp:new_cost = 0
        tmp:old_cost = 0
        tmp:qty = 0
        x# = INSTRING('NEW DETAILS:-<13,10>PURCHASE COST:',shi:Information,1,1)
        IF x# <> 0
          LOOP y# = x#+29 TO LEN(CLIP(shi:Information))
            IF SUB(shi:Information,y#,1) = CHR(13)
              BREAK
            END
          END
          tmp:new_cost = DEFORMAT(CLIP(SUB(shi:Information,x#+29,y#-(x#+29))))
        END
        x# = INSTRING('OLD DETAILS:-<13,10>PURCHASE COST:',shi:Information,1,1)
        IF x# <> 0
          LOOP y# = x#+29 TO LEN(CLIP(shi:Information))
            IF SUB(shi:Information,y#,1) = CHR(13)
              BREAK
            END
          END
          tmp:old_cost = DEFORMAT(CLIP(SUB(shi:Information,x#+29,y#-(x#+29))))
        END
        x# = INSTRING('QUANTITY:',shi:Information,1,1)
        IF x# <> 0
          LOOP y# = x#+9 TO LEN(CLIP(shi:Information))
            IF SUB(shi:Information,y#,1) = CHR(13)
              BREAK
            END
          END
          tmp:qty = DEFORMAT(CLIP(SUB(shi:Information,x#+9,y#-(x#+9))))
        END


        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        !IF NOT LoadSTOCK(shi:Ref_Number)
        !    EXIT
!        ELSIF sto:Accessory = 'YES'
!            EXIT
        IF NOT CheckLocation(sto:Location)
            EXIT
        ELSIF NOT SetSheetTo(locQ:SheetName)
           EXIT
        END !IF
        !=================================================================
        WriteDebug('WriteColumns(START)')

        locQ:RecordCount += 1
        PUT(Location_Queue)

        RecordCount += 1

        DO UpdateSummaryQueue
        !-----------------------------------------------------------------
        WriteColumn( calc:TrnNumber, True ) ! Trn number
        WriteColumn( calc:TrnDate         ) ! Trn date
        WriteColumn( sq:PartNumber        ) ! Part number
        WriteColumn( sq:Description       ) ! Description
        WriteColumn( tmp:qty      ) ! Qty In
        WriteColumn( tmp:old_cost - tmp:new_cost ) ! Amount
        WriteColumn( (tmp:old_cost - tmp:new_cost) * tmp:qty  ) ! Line amount
        !-----------------------------------------------------------------
        IF excel:OperatingSystem < 5
            DO PassViaClipboard
        END !IF
        !-----------------------------------------------------------------
        DO XL_ColFirst
        DO XL_RowDown

        WriteDebug('WriteColumns(END)')
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
WriteDataSummary                                              ROUTINE
    DATA
CurrentRow   LONG
SummaryCol   STRING('A')
    CODE
        EXIT
        !-----------------------------------------------------------------
        LOOP gg# = 1 TO RECORDS(Location_Queue)
            GET(Location_Queue, gg#)

            IF NOT SetSheetTo(locQ:SheetName)
                CYCLE
            END !IF
            !-----------------------------------------------------------------
            RecordCount = locQ:RecordCount
            !-----------------------------------------------------------------
            ! summary details
            !
            IF RecordCount < 1
                Excel{'ActiveCell.Formula'} = 'No Stock Movements Found'

                EXIT
            END !IF
            !-----------------------------------------------------------------
            DO FormatColumns

            Excel{'Range("E' & sheet:DataSectionRow & '").Select'}
                DO XL_HorizontalAlignmentLeft
                Excel{'ActiveCell.Formula'}         = RecordCount

            Excel{'Range("G' & sheet:DataSectionRow & '").Select'}
                DO XL_HorizontalAlignmentLeft
                Excel{'ActiveCell.NumberFormat'}    = '0'
                Excel{'ActiveCell.Formula'}         = '=SUBTOTAL(2, ' & SummaryCol & sheet:DataHeaderRow+1 & ':' & SummaryCol & sheet:DataHeaderRow+RecordCount & ')'
            !-----------------------------------------------------------------
            Excel{'Range("A' & sheet:DataHeaderRow & ':' & sheet:DataLastCol & sheet:DataHeaderRow+RecordCount & '").Select'}
                DO XL_DataAutoFilter
            !-----------------------------------------------------------------
        END !LOOP
        !-----------------------------------------------------------------
    EXIT
WriteHeadSummary                                               ROUTINE
    DATA
QueueIndex   LONG
ResultsCount LONG

HeaderRow    LONG
FirstRow     LONG
LastRow      LONG
TotalsRow    LONG
    CODE
        !-----------------------------------------------------------------
        !Excel{'Sheets("Summary").Select'}
        EXIT
        SetSheetTo(locQ:SheetName)
        DO XL_ColFirst

        ResultsCount = Records(SummaryQueue)
        IF ResultsCount < 1
            Excel{'ActiveCell.Formula'} = 'No Stock Movements Found'

            EXIT
        END !IF
        !-----------------------------------------------------------------
        Progress:Text    = 'Writing Summary'
        RecordsToProcess = ResultsCount 

        DO ProgressBar_LoopPre
        !-----------------------------------------------------------------
        !
        HeaderRow = Excel{'ActiveCell.Row'}
        FirstRow  = HeaderRow + 1
        LastRow   = HeaderRow + ResultsCount
        TotalsRow = LastRow   + 1

        Excel{'ActiveCell.Offset(0, 00).Formula'} = 'Manufacturer'
        Excel{'ActiveCell.Offset(0, 01).Formula'} = 'Part Number'
        Excel{'ActiveCell.Offset(0, 02).Formula'} = 'Description'
        Excel{'ActiveCell.Offset(0, 03).Formula'} = 'Notes'

        Excel{'ActiveCell.Offset(0, 04).Formula'} = 'Location Count'
        Excel{'ActiveCell.Offset(0, 05).Formula'} = 'Quantity In'
        Excel{'ActiveCell.Offset(0, 06).Formula'} = 'Quantity Out'

        Excel{'ActiveCell.Offset(0, 07).Formula'} = 'Amount In'
        Excel{'ActiveCell.Offset(0, 08).Formula'} = 'Amount Out'

        sheet:HeadLastCol = NumberToColumn(8)
        !-----------------------------------------------
        Excel{'Range("A' & HeaderRow & ':' & sheet:HeadLastCol & HeaderRow & '").Select'}
                DO XL_SetColumnHeader

        Excel{'Range("A' & FirstRow & '").Select'}
        !-----------------------------------------------------------------
        SORT(SummaryQueue, 'sq_Manufacturer,sq_PartNumber')
        LOOP QueueIndex = 1 TO ResultsCount 
            !-------------------------------------------------------------
            RecordCount += 1

            DO ProgressBar_Loop

            IF CancelPressed
                BREAK
            END !IF
            !-------------------------------------------------------------
            GET(SummaryQueue, QueueIndex)
            !-------------------------------------------------------------
            Excel{'ActiveCell.Offset(0,  0).NumberFormat'} = chr(64) ! Text (AT) Sign
            Excel{'ActiveCell.Offset(0,  1).NumberFormat'} = chr(64) ! Text (AT) Sign
            Excel{'ActiveCell.Offset(0,  2).NumberFormat'} = chr(64) ! Text (AT) Sign
            Excel{'ActiveCell.Offset(0,  3).NumberFormat'} = chr(64) ! Text (AT) Sign

            Excel{'ActiveCell.Offset(0,  4).NumberFormat'} = '#,##0'
            Excel{'ActiveCell.Offset(0,  5).NumberFormat'} = '#,##0'
            Excel{'ActiveCell.Offset(0,  6).NumberFormat'} = '#,##0'

            Excel{'ActiveCell.Offset(0,  7).NumberFormat'} = '#,##0.00'
            Excel{'ActiveCell.Offset(0,  8).NumberFormat'} = '#,##0'
            !-------------------------------------------------------------
            Excel{'ActiveCell.Offset(0,  0).HorizontalAlignment'} = xlRight
            Excel{'ActiveCell.Offset(0,  1).HorizontalAlignment'} = xlRight
            Excel{'ActiveCell.Offset(0,  2).HorizontalAlignment'} = xlRight
            !-------------------------------------------------------------
            Excel{'ActiveCell.Offset(0, 00).Formula'} = sq:Manufacturer
            Excel{'ActiveCell.Offset(0, 01).Formula'} = '''' & sq:PartNumber
            Excel{'ActiveCell.Offset(0, 02).Formula'} = sq:Description
            Excel{'ActiveCell.Offset(0, 03).Formula'} = sq:Notes

            Excel{'ActiveCell.Offset(0, 04).Formula'} = 0!sq:LocationCount
            Excel{'ActiveCell.Offset(0, 05).Formula'} = sq:QuantityIn
            Excel{'ActiveCell.Offset(0, 06).Formula'} = sq:QuantityOut

            Excel{'ActiveCell.Offset(0, 07).Formula'} = sq:AmountIn
            Excel{'ActiveCell.Offset(0, 08).Formula'} = sq:AmountOut
            !-------------------------------------------------------------
            DO XL_RowDown
            !-------------------------------------------------------------
        END !LOOP

        Excel{'Range("a' & HeaderRow & ':' & sheet:HeadLastCol & (LastRow) & '").Select'}
            DO XL_DataAutoFilter
        !-----------------------------------------------------------------
        Excel{'Range("a' & TotalsRow & ':' & sheet:HeadLastCol & (TotalsRow) & '").Select'}
            DO XL_SetTitle
            DO XL_SetGrid
        !-----------------------------------------------------------------
        Excel{'Range("A' & TotalsRow & '").Select'}
            DO XL_SetBold

            Excel{'ActiveCell.Offset(0,  0).NumberFormat'} = chr(64) ! Text (AT) Sign
            Excel{'ActiveCell.Offset(0,  1).NumberFormat'} = chr(64) ! Text (AT) Sign
            Excel{'ActiveCell.Offset(0,  2).NumberFormat'} = chr(64) ! Text (AT) Sign
            Excel{'ActiveCell.Offset(0,  3).NumberFormat'} = chr(64) ! Text (AT) Sign

            Excel{'ActiveCell.Offset(0,  4).NumberFormat'} = '#,##0'
            Excel{'ActiveCell.Offset(0,  5).NumberFormat'} = '#,##0'
            Excel{'ActiveCell.Offset(0,  6).NumberFormat'} = '#,##0'

            Excel{'ActiveCell.Offset(0,  7).NumberFormat'} = '#,##0.00'
            Excel{'ActiveCell.Offset(0,  8).NumberFormat'} = '#,##0.00'
            !-------------------------------------------------------------
            Excel{'ActiveCell.Offset(0,  0).Formula'} = 'Totals'

            Excel{'ActiveCell.Offset(0,  4).Formula'} = '=SUBTOTAL(9,D' & FirstRow & ':D' & LastRow & ')'
            Excel{'ActiveCell.Offset(0,  5).Formula'} = '=SUBTOTAL(9,E' & FirstRow & ':E' & LastRow & ')'
            Excel{'ActiveCell.Offset(0,  6).Formula'} = '=SUBTOTAL(9,F' & FirstRow & ':F' & LastRow & ')'

            Excel{'ActiveCell.Offset(0,  7).Formula'} = '=SUBTOTAL(9,G' & FirstRow & ':G' & LastRow & ')'
            Excel{'ActiveCell.Offset(0,  8).Formula'} = '=SUBTOTAL(9,H' & FirstRow & ':H' & LastRow & ')'
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF

        DO ProgressBar_LoopPost
        !-----------------------------------------------------------------
        Excel{'Range("A1").Select'}
        !-----------------------------------------------------------------
    EXIT
SaveAllRRCSheets            ROUTINE
    DATA
Sheets    LONG
SheetName STRING(30)
FileName  STRING(255)
WorkBook  STRING(255)
    CODE
        !-----------------------------------------------------------------
        Sheets = Excel{'Sheets.Count'}
        WriteDebug('SaveAllRRCSheets(' & Sheets & ')')

        WorkBook = Excel{'ActiveWorkbook.Name'}
        WriteDebug('SaveAllRRCSheets(WorkBook="' & CLIP(WorkBook) & '")')

        LOOP x# = 0 TO Sheets
            IF (x# = Sheets)
                BREAK
            END !IF

            Excel{'Sheets(' & x# & ').Select'}
            SheetName = Excel{'ActiveSheet.Name'}
                !-----------------------------------------------
                ! 23 Oct 2002 John
                ! R003, berrjo :Insert the report exe name EG VODR56.V01
                ! Do not attach the EXE extenstion but show the version number.  Increment the version number each time changed.
                ! Make this standard on all reports sent for correction.
                ! LOC:Version STRING(8) 3.1.0000 3.1 to match sevicebase version (and dictionary) .0000 increment before each compile
                !
                !LOC:FileName = SHORTPATH(CLIP(LOC:Path)) & CLIP(LOC:ProgramName) & ' VODR0057 v' & CLIP(LOC:Version) & ' ' & FORMAT(TODAY(), @D12) & '.xls'
                !FileName  = SHORTPATH(CLIP(LOC:Path)) & CLIP(SheetName) & ' ' & FORMAT(TODAY(), @D12) & '.xls'
            FileName  = SHORTPATH(CLIP(LOC:Path)) & CLIP(SheetName) & ' VODR0052 ' & FORMAT(TODAY(), @D12) & '.xls'
                !-----------------------------------------------
            
            WriteDebug('SaveAllRRCSheets(SheetName="' & CLIP(SheetName) & '", FileName="' & CLIP(FileName) & '")')

            CASE CLIP(SheetName)
            OF 'Summary'
                ! NULL
            !OF 'MAIN_STORE'
            !    ! NULL
            OF 'Sheet3'
                ! NULL
            ELSE
                Excel{'ActiveSheet.Copy'}
                WriteDebug('SaveAllRRCSheets(COPY)')

                Excel{'ActiveWorkbook.SaveAs("' & CLIP(FileName) & '")'}
                WriteDebug('SaveAllRRCSheets(SaveAs)')

                Excel{'ActiveWindow.Close'}
                WriteDebug('SaveAllRRCSheets(Close)')
            END ! CASE
        END !LOOP
        !-----------------------------------------------------------------
    EXIT


!SaveAllRRCSheets            ROUTINE
!    DATA
!SheetName STRING(30)
!FileName  STRING(255)
!WorkBook  STRING(255)
!    CODE
!        !-----------------------------------------------------------------
!        debug:Active = True
!
!        WriteDebug('SaveAllRRCSheets')
!
!        WorkBook = Excel{'ActiveWorkbook.Name'}
!
!        LOOP x# = 1 TO Excel{'ActiveWorkbook.Sheets'}
!            Excel{'Workbooks("' & CLIP(WorkBook) & '").Sheet(' & x# & ').Select'}
!            SheetName = Excel{'ActiveSheet.Name'}
!            FileName  = SHORTPATH(CLIP(LOC:Path)) & CLIP(SheetName) & ' ' & FORMAT(TODAY(), @D12) & '.xls'
!
!            WriteDebug('SaveAllRRCSheets(SheetName="' & CLIP(SheetName) & '", FileName="' & CLIP(FileName) & '")')
!
!            CASE SheetName 
!            OF 'Summary'
!                ! NULL
!            OF 'Sheet3'
!                ! NULL
!            ELSE
!                Excel{'ActiveSheet.Copy'}
!                WriteDebug('SaveAllRRCSheets(COPY)')
!
!                Excel{'ActiveWorkbook.SaveAs("' & CLIP(FileName) & '")'}
!                WriteDebug('SaveAllRRCSheets(SaveAs)')
!
!                Excel{'ActiveWindow.Close'}
!                WriteDebug('SaveAllRRCSheets(Close)')
!            END ! CASE
!        END !LOOP
!
!        debug:Active = False
!        !-----------------------------------------------------------------
!    EXIT
!
!                !Sheets("AA55_PRETORIA_METRO").Select
!                !Sheets("AA55_PRETORIA_METRO").Copy
!                !ActiveWorkbook.SaveAs Filename:= _
!                !    "C:\Documents and Settings\john\Desktop\ServiceBase 2000 Export\Parts Usage Export\Book5.xls" _
!                !    , FileFormat:=xlNormal, Password:="", WriteResPassword:="", _
!                !    ReadOnlyRecommended:=False, CreateBackup:=False
!                !ActiveWindow.Close
!
!        !LOC:FileName = SHORTPATH(CLIP(LOC:Path)) & CLIP(LOC:ProgramName) & ' ' & FORMAT(TODAY(), @D12) & '.xls'
!-----------------------------------------------
GetFileName                             ROUTINE ! Generate default file name
    DATA
local:Desktop         CString(255)
DeskTopExists   BYTE
ApplicationPath STRING(255)
excel:ProgramName       Cstring(255)
    CODE
    excel:ProgramName = 'Cost Adjustment Report'
    SHGetSpecialFolderPath( GetDesktopWindow(), local:Desktop, 5, FALSE )
    local:Desktop = Clip(local:Desktop) & '\ServiceBase Export'

    !Does the Export Folder already Exists?
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    local:Desktop = Clip(local:Desktop) & '\' & Clip(excel:ProgramName)
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    loc:Path = local:Desktop
    loc:FileName = Clip(local:Desktop) & '\' & Clip(excel:ProgramName) & ' ' & Format(Today(),@d12)


!GetFileName                             ROUTINE ! Generate default file name
!    DATA
!Desktop         CString(255)
!DeskTopExists   BYTE
!ApplicationPath STRING(255)
!    CODE
!        !-----------------------------------------------
!        ! Generate default file name
!        !
!        !-----------------------------------------------
!        ! 4 Dec 2001 John Stop using def:Export_Path as previously directed
!        ! 4 Dec 2001 John Use generated date to create file name not parameters LOC:StartDate,LOC:EndDate
!        !
!        IF CLIP(LOC:FileName) <> ''
!            ! Already generated 
!            EXIT
!        END !IF
!
!        DeskTopExists = False
!
!        SHGetSpecialFolderPath( GetDesktopWindow(), Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
!        LOC:DesktopPath = Desktop
!
!        ApplicationPath = Desktop & '\' & LOC:ApplicationName & ' Export'
!        IF EXISTS(CLIP(ApplicationPath))
!            LOC:Path      = CLIP(ApplicationPath) & '\'
!            DeskTopExists = True
!
!        ELSE
!            Desktop  = CLIP(ApplicationPath)
!
!            IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!!                MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!!            ELSE
!!                MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
!            END !IF
!
!            IF EXISTS(CLIP(ApplicationPath))
!                LOC:Path      = CLIP(ApplicationPath) & '\'
!                DeskTopExists = True
!            END !IF
!
!        END !IF
!
!        IF DeskTopExists
!            ApplicationPath = CLIP(LOC:Path) & CLIP(LOC:ProgramName)
!            Desktop         = CLIP(ApplicationPath)
!
!            IF NOT EXISTS(CLIP(ApplicationPath))
!                IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!!                    MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!!                ELSE
!!                    MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
!                END !IF
!            END !IF
!
!            IF EXISTS(CLIP(ApplicationPath))
!                LOC:Path = CLIP(ApplicationPath) & '\'
!            END !IF
!        END !IF
!
!        !-----------------------------------------------
!        ! 23 Oct 2002 John
!        ! R003, berrjo :Insert the report exe name EG VODR56.V01
!        ! Do not attach the EXE extenstion but show the version number.  Increment the version number each time changed.
!        ! Make this standard on all reports sent for correction.
!        ! LOC:Version STRING(8) 3.1.0000 3.1 to match sevicebase version (and dictionary) .0000 increment before each compile
!        !
!        !LOC:FileName = SHORTPATH(CLIP(LOC:Path)) & CLIP(LOC:ProgramName) & ' ' & FORMAT(TODAY(), @D12) & '.xls'
!        LOC:FileName = SHORTPATH(CLIP(LOC:Path)) & CLIP(LOC:ProgramName) & ' VODR0052 ' & FORMAT(TODAY(), @D12) & '.xls'
!        !-----------------------------------------------
!        
!        !-----------------------------------------------
!    EXIT
GetUserName                            ROUTINE
    DATA
CommandLine STRING(255)
tmpPos      LONG
    CODE
        !-----------------------------------------------
        CommandLine = CLIP(COMMAND(''))

!        LOC:UserName = LOC:ApplicationName
!
!        Case MessageEx('Fail User Check For "' & CLIP(CommandLine) & '"?', LOC:ApplicationName & ' DEBUG', |
!                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
!            Of 1 ! &Yes Button
!                POST(Event:CloseWindow)
!
!                EXIT
!            Of 2 ! &No Button
!                EXIT
!        End!Case MessageEx
        !-----------------------------------------------
        tmpPos = INSTRING('%', CommandLine)
        IF NOT tmpPos
            Case Missive('You must run this report from ServiceBase''s "Custom Reports".','ServiceBase 3g',|
                           'mstop.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive

            POST(Event:CloseWindow)

           EXIT
        END !IF tmpPos
        !-----------------------------------------------
        SET(USERS)
        Access:USERS.Clearkey(use:Password_Key)
        glo:Password = CLIP(SUB(CommandLine, tmpPos + 1, 30))
      
        Access:USERS.Clearkey(use:Password_Key)
        use:Password = glo:Password
        If Access:USERS.Tryfetch(use:Password_Key)
            Case Missive('Error! Cannot find the user''s details.','ServiceBase 3g',|
                           'mstop.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive

            POST(Event:CloseWindow)

           EXIT
        End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        !-----------------------------------------------
        LOC:UserName = use:Forename

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = use:Surname
        ELSE
            LOC:UserName = CLIP(use:Forename) & ' ' & use:Surname 
        END !IF

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = '<' & use:User_Code & '>'
        END !IF
        !-----------------------------------------------
    EXIT
GetUserName2                            ROUTINE
    DATA
CommandLine STRING(255)
tmpPos      LONG
    CODE
        !-----------------------------------------------
        If Command('/SCHEDULE')
            Automatic = False
            Exit
        End ! If Command('/SCHEDULE')
        CommandLine = CLIP(COMMAND(''))

!        LOC:UserName = LOC:ApplicationName
!
!        Case MessageEx('Fail User Check For "' & CLIP(CommandLine) & '"?', LOC:ApplicationName & ' DEBUG',|
!                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
!            Of 1 ! &Yes Button
!                POST(Event:CloseWindow)
!
!                EXIT
!            Of 2 ! &No Button
!                EXIT
!        End!Case MessageEx
        !-----------------------------------------------
        Automatic = FALSE
        tmpPos = INSTRING('%', CommandLine)
!        IF NOT tmpPos
!            Case MessageEx('Attempting to use Workshop Performance Report<10,13>'            & |
!                '   without using ' & LOC:ApplicationName & '.<10,13>'                       & |
!                '   Start ' & LOC:ApplicationName & ' and run the report from there.<10,13>',  |
!                LOC:ApplicationName,                                                           |
!                'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,LocalTimeOut)
!            Of 1 ! &OK Button
!            END!Case MessageEx
!
!           POST(Event:CloseWindow)
!
!           EXIT
!        END !IF tmpPos
        !-----------------------------------------------
        skip# = 0
        SET(USERS)
        Access:USERS.Clearkey(use:Password_Key)
        glo:Password = CLIP(SUB(CommandLine, tmpPos + 1, 30))
      
        Access:USERS.Clearkey(use:Password_Key)
        use:Password = glo:Password
        If Access:USERS.Tryfetch(use:Password_Key)
            !Assert(0,'<13,10>Fetch Error<13,10>')
!            Case MessageEx('Unable to find your logged in user details.', |
!                    LOC:ApplicationName,                                  |
!                    'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,LocalTimeOut)
!                Of 1 ! &OK Button
!            End!Case MessageEx
!
!            POST(Event:CloseWindow)
!
!           EXIT
            LOC:UserName = 'AUTOMATED PROCEDURE'
            Automatic = TRUE
            skip# = 1
        End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        !-----------------------------------------------
        IF skip# = 0
            LOC:UserName = use:Forename

            IF CLIP(LOC:UserName) = ''
                LOC:UserName = use:Surname
            ELSE
                LOC:UserName = CLIP(use:Forename) & ' ' & use:Surname 
            END !IF

            IF CLIP(LOC:UserName) = ''
                LOC:UserName = '<' & use:User_Code & '>'
            END !IF
            !-----------------------------------------------
        END
    EXIT
LoadFileDialog                          ROUTINE ! Ask user if file name is empty
    DATA
OriginalPath STRING(255)
    CODE
        !-----------------------------------------------
        IF CLIP(LOC:Filename) = ''
            DO GetFileName 
        END !IF

        OriginalPath = PATH()
        SETPATH(LOC:Path) ! Required for Win95/98
            IF NOT FILEDIALOG('Save Spreadsheet', LOC:Filename, 'Microsoft Excel Workbook|*.XLS', |
                FILE:KeepDir + FILE:Save + FILE:NoError + FILE:LongName)

                LOC:Filename = ''
            END !IF
        SETPATH(OriginalPath)

        UPDATE()
        DISPLAY()
        !-----------------------------------------------
    EXIT
PassViaClipboard                                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF clip:Saved = False
            clip:OriginalValue = CLIPBOARD()
            clip:Saved         = True
        END !IF
        !-----------------------------------------------
        SETCLIPBOARD(CLIP(clip:Value))
            Excel{'ActiveSheet.Paste()'}
        clip:Value = ''
        !-----------------------------------------------
    EXIT
ResetClipboard                                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF clip:Saved = False
            EXIT
        END !IF

        SETCLIPBOARD(clip:OriginalValue)
        
        clip:Saved = False
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
!CalcStockUsage                                            ROUTINE
!    DATA
!    CODE
!        !-----------------------------------------------------------------
!        IF CancelPressed
!            EXIT
!        END !IF
!        !-----------------------------------------------------------------
!        calc:QuantityUsedOnJobs         = 0
!        calc:Adjustments                = 0
!        calc:DatePartWasCreatedOnSystem = 0
!        calc:LastJobUsed                = 0
!        calc:JobBookedDate              = 0
!        !-----------------------------------------------------------------
!        ! Ref_Number_Key       KEY( shi:Ref_Number,                       shi:Date ),DUP,NOCASE
!        ! record_number_key    KEY(                 shi:Record_Number              ),NOCASE,PRIMARY
!        ! Transaction_Type_Key KEY( shi:Ref_Number, shi:Transaction_Type, shi:Date ),DUP,NOCASE
!        ! DateKey              KEY(                                       shi:Date ),DUP,NOCASE
!        !
!        Access:STOHIST.ClearKey(shi:Ref_Number_Key)
!            shi:Ref_Number = sto:Ref_Number
!        SET(shi:Ref_Number_Key, shi:Ref_Number_Key)
!        !-----------------------------------------------------------------
!        LOOP UNTIL Access:STOHIST.Next()
!            !-------------------------------------------------------------
!            DO ProgressBar_CancelCheck
!
!            IF CancelPressed
!                BREAK
!            END !IF
!            !-------------------------------------------------------------
!            IF shi:Ref_Number <> sto:Ref_Number
!                BREAK
!            END !IF
!            !=============================================================
!            IF shi:Transaction_Type = 'DEC'    ! 19 mar 2002 testing
!                calc:Adjustments += shi:Quantity  ! 19 mar 2002 testing
!
!            ELSIF shi:Transaction_Type = 'REC'    ! 19 mar 2002 testing
!                calc:Adjustments += -shi:Quantity ! 19 mar 2002 testing
!
!            END !IF
!            !-------------------------------------------------------------
!            IF (shi:Job_Number > calc:LastJobUsed)
!                calc:LastJobUsed = shi:Job_Number
!            END !IF
!            !-------------------------------------------------------------
!            IF    shi:Date = 0
!                ! null
!            ELSIF calc:DatePartWasCreatedOnSystem = 0
!                calc:DatePartWasCreatedOnSystem = shi:Date
!            ELSIF shi:Date < calc:DatePartWasCreatedOnSystem
!                calc:DatePartWasCreatedOnSystem = shi:Date
!            END !IF
!            !-------------------------------------------------------------
!        END !LOOP
!        !-----------------------------------------------------------------
!    EXIT
!
!
!
CheckSheetName                                                  ROUTINE
    DATA
MaxLength LONG(30)
TextLen   LONG
    CODE
        !-----------------------------------------------------------------
        TextLen = LEN(CLIP(LOC:Text))

        IF TextLen > MaxLength
            TextLen = MaxLength
        END !IF

        LOOP x# = 1 TO TextLen
            CASE LOC:Text[x#]
            OF 'a' TO 'z'
                ! NULL
            OF 'A' TO 'Z'
                ! NULL
            OF '0' TO '9'
                ! NULL
            ELSE
                LOC:Text[x#] = '_'
            END !CASE
        END !LOOP

        LOC:Text = LEFT(LOC:Text, TextLen)
        !-----------------------------------------------------------------
    EXIT
UpdateSummaryQueue                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        sq:PartNumber = sto:Part_Number
        sq:Location   = sto:Location
        sq:Notes      = shi:notes
        GET(SummaryQueue, +sq:PartNumber, +sq:Location, +sq:Notes)

        CASE ERRORCODE()
        OF 0 ! Found
            IF NOT (sq:PartNumber = sto:Part_Number)
                DO UpdateSummaryQueue_ADD
            END !IF

        OF 30 ! NOT Found
            DO UpdateSummaryQueue_ADD

        ELSE
            CancelPressed = True

            EXIT
        END ! IF
        !-----------------------------------------------------------------
        CLEAR(Calculation_Group)
            !-------------------------------------------------------------
            calc:TrnNumber       = shi:Record_Number
            calc:TrnDate         = DateToString(shi:Date)

            calc:PartNumber      = sq:PartNumber
            calc:Location        = sq:Location
            calc:Manufacturer    = sq:Manufacturer
            calc:Description     = sq:Description

            calc:Reason          = shi:Transaction_Type
            calc:Notes           = shi:Notes

            sq:notes             = shi:Notes

            calc:Quantity        = shi:Quantity

            IF shi:Transaction_Type = 'DEC'
                calc:Sign        = -1
                calc:QuantityOut = shi:Quantity
            ELSE
                calc:Sign        = 1
                calc:QuantityIn  = shi:Quantity
            END !IF

            calc:Amount          = sq:Price
            calc:LineAmount      = calc:Amount * calc:Quantity
            !-------------------------------------------------------------
            sq:Quantity        += calc:Quantity

            IF shi:Transaction_Type = 'DEC'
                sq:QuantityOut += calc:Quantity
                sq:AmountOut   += calc:LineAmount
            ELSE
                sq:QuantityIn  += calc:Quantity
                sq:AmountIn    += calc:LineAmount
            END !IF
            !-------------------------------------------------------------
        PUT(SummaryQueue, +sq:PartNumber, +sq:Location, +sq:notes)
        !-----------------------------------------------------------------
    EXIT
UpdateSummaryQueue_ADD                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        CLEAR(SummaryQueue)
            !-------------------------------------------------------------
            sq:PartNumber      = sto:Part_Number     ! Part Number
            sq:Location        = sto:Location        ! Location
            sq:Manufacturer    = sto:Manufacturer    ! Manufacturer
            sq:Description     = sto:Description     ! Description
            sq:Notes           = Shi:Notes
            sq:Price           = sto:Purchase_Cost
            sq:QuantityStock   = sto:Quantity_Stock
            sq:QuantityOnOrder = sto:Quantity_On_Order
            !-------------------------------------------------------------
        ADD(SummaryQueue, +sq:PartNumber, +sq:Location, +sq:notes)
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
LoadEngineer                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !
        Result = False

        Access:USERS.ClearKey(use:User_Code_Key)
        use:User_Code = job:Engineer

        IF Access:USERS.TryFetch(use:User_Code_Key)
            EXIT
        END !IF

        IF use:User_Code <> job:Engineer
            EXIT
        END !IF

        Result = True
        !-----------------------------------------------
    EXIT
LoadORDSTOCK                                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !
        Result = False

        Access:ORDSTOCK.ClearKey(orstmp:LocationKey)
            orstmp:Location   = sto:Location
            orstmp:PartNumber = sto:Part_Number
        set(orstmp:LocationKey, orstmp:LocationKey)

        IF Access:ORDSTOCK.NEXT()
            EXIT
        END !IF

        IF orstmp:Location <> sto:Location
            EXIT
        ELSIF orstmp:PartNumber <> sto:Part_Number
            EXIT
        END !IF

        Result = True
        !-----------------------------------------------
    EXIT
LoadSTOMODEL                                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !
        Result = False

        Access:STOMODEL.ClearKey(stm:Model_Number_Key)
        stm:Ref_Number    = shi:Ref_Number
        !stm:manufacturer = sto:manufacturer
        set(stm:Model_Number_Key, stm:Model_Number_Key)

        IF Access:STOMODEL.NEXT()
            EXIT
        END !IF

        IF stm:Ref_Number <> shi:Ref_Number
            EXIT
        END !IF

        Result = True
        !-----------------------------------------------
    EXIT
LoadWARPARTS                                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !
        Result = False

        Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
        wpr:Ref_Number = job:Ref_Number

        IF Access:WARPARTS.TryFetch(wpr:Part_Number_Key)
            EXIT
        END !IF

        IF wpr:Ref_Number <> job:Ref_Number
            EXIT
        END !IF

        Result = True
        !-----------------------------------------------
    EXIT
LoadUSERS                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !
        Result = False

        Access:USERS.ClearKey(use:User_Code_Key)
        use:User_Code = LOC:UserCode

        IF Access:USERS.TryFetch(use:User_Code_Key)
            EXIT
        END !IF

        IF use:User_Code <> LOC:UserCode
            EXIT
        END !IF

        Result = True
        !-----------------------------------------------
    EXIT
SyncINVOICE                                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !
        Result    = False
        InvoiceOK = False

        IF job:Invoice_Number = 0
            EXIT
        END !IF

        Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
        inv:Invoice_Number = job:Invoice_Number
        set(inv:Invoice_Number_Key, inv:Invoice_Number_Key)

        IF Access:INVOICE.NEXT()
            EXIT
        END !IF

        IF inv:Invoice_Number <> job:Invoice_Number
            EXIT
        END !IF

        Result    = True
        InvoiceOK = True
        !-----------------------------------------------
    EXIT
Load_StockHistory                                             ROUTINE
!    DATA
!    CODE
!        !-----------------------------------------------------------------
! !putini('CreateDataSheet_AllPartsOrdered', format(CLOCK(), @t1), '0', 'c:\debug.ini')
!        IF CancelPressed
!            EXIT
!        END !IF
!        !------------------------------------------
!        !
!        Access:STOHIST.ClearKey(shi:DateKey)
!        shi:Date = LOC:StartDate
!        SET(shi:DateKey, shi:DateKey)
!        !------------------------------------------
!        Progress:Text    = 'Loading Stock History Details'
!        RecordsToProcess = RECORDS(STOHIST)
!        RecordsProcessed = 0
!        RecordCount      = 0
!
!        DO ProgressBar_LoopPre
! !message('Start of main loop')
!        !-----------------------------------------------------------------
!        LOOP UNTIL Access:STOHIST.Next()
!            !-------------------------------------------------------------
!            DO ProgressBar_Loop
!
!            IF CancelPressed
!                BREAK
!            END !IF
!            !-------------------------------------------------------------
!            IF shi:Date < LOC:StartDate
!                CYCLE
!            ELSIF shi:Date > LOC:EndDate
!                BREAK
!            END !IF
!
!            DO SaveStockDetailsToQueue
!            !-------------------------------------------------------------
!        END !LOOP
! !message('End of main loop')
!
!        IF CancelPressed
!            EXIT
!        END !IF
!
!        DO ProgressBar_LoopPost
!        !-----------------------------------------------------------------
    EXIT

LoadVATCODE                                                ROUTINE

SyncSUBTRACCfromJOBS                                                 ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !
        Result = False

        Access:SUBTRACC.ClearKey(sub:Main_Account_Key)
        sub:Account_Number = job:Account_Number

        IF Access:SUBTRACC.TryFetch(sub:Main_Account_Key)
            EXIT
        END !IF

        IF sub:Account_Number <> job:Account_Number
            EXIT
        END !IF

        Result = True
        !-----------------------------------------------
    EXIT
SyncTRADEACCfromSUBTRACC                                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !
        Result = False

        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = sub:Main_Account_Number

        IF NOT Access:TRADEACC.TryFetch(tra:Account_Number_Key)
            EXIT
        END !IF

        IF tra:Account_Number <> sub:Main_Account_Number
            EXIT
        END !IF

        Result = True
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
ProgressBar_Setup                       ROUTINE
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
            RecordsPerCycle      = 25
            RecordsProcessed     = 0
            RecordsToProcess     = 10 !***The Number Of Records, or at least a guess***
            PercentProgress      = 0
            Progress:thermometer = 0

            thiswindow.reset(1) !This may error, if so, remove this line.
            open(progresswindow)
            progresswindow{PROP:Timer} = 1
         
            ?progress:userstring{prop:text} = 'Running...'
            ?progress:pcttext{prop:text}    = '0% Completed'
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***

ProgressBar_LoopPre          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***

        RecordsProcessed = 0
        RecordCount      = 0

        ?Progress:UserString{PROP:Text} = CLIP(Progress:Text)

        !RecordsToProcess = RECORDS(JOBS) !***The Number Of Records, or at least a guess***
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
        !-----------------------------------------------
    EXIT

ProgressBar_Loop             ROUTINE
    DATA
newPctText STRING(255)
    CODE
        !-----------------------------------------------
        !***Place the following code inside the loop. The first line updates the progress bar, and the rest allows the user to cancel the routine. This can be removed if necessary. ****

        Do ProgressBar_GetNextRecord2 !Necessary

        Do ProgressBar_CancelCheck
        If CancelPressed
            CLOSE(ProgressWindow)
        End!If 
        !-----------------------------------------------
        newPctText = 'Checked(' & RecordsProcessed & '), Found(' & RecordCount & ')'

        IF ?progress:pcttext{prop:text} <> newPctText
            ?progress:pcttext{prop:text} = newPctText

            Display()
        END !IF

        !***Place the following code inside the loop. The first line updates the progress bar, and the rest allows the user to cancel the routine. This can be removed if necessary. ****
        !-----------------------------------------------
    EXIT

ProgressBar_LoopPost         ROUTINE
    DATA
newPctText STRING(255)
    CODE
        !-----------------------------------------------
        ?Progress:UserString{PROP:Text} = CLIP(Progress:Text) & ' Done(' & RecordsProcessed & '/' & RecordCount & ')'

        newPctText = 'Checked(' & RecordsProcessed & '), Found(' & RecordCount & ')'

        IF ?progress:pcttext{prop:text} <> newPctText
            ?progress:pcttext{prop:text} = newPctText
        END !IF
        !-----------------------------------------------
    EXIT

ProgressBar_Finalise                        ROUTINE
        !**** End Of Loop ***
            Do ProgressBar_EndPrintRun
            close(progresswindow)
        !**** End Of Loop ***
        ! when in report procedure->LocalResponse = RequestCompleted

ProgressBar_GetNextRecord2                  routine
    recordsprocessed += 1
    recordsthiscycle += 1

    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        !?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end

ProgressBar_CancelCheck                     routine
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                IF NOT CancelPressed = True
                    cancel# = 1
                END !IF

                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept

    If cancel# = 1
        BEEP(BEEP:SystemAsterisk)  ;  YIELD()

        Case Missive('Are you sure you want to cancel?','ServiceBase 3g',|
                       'mquest.jpg','\No|/Yes') 
            Of 2 ! Yes Button
                CancelPressed = True
            Of 1 ! No Button
        End ! Case Missive
    End!If cancel# = 1

ProgressBar_EndPrintRun                     routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()

RefreshWindow                          ROUTINE
    !|
    !| This routine is used to keep all displays and control templates current.
    !|
!    IF window{Prop:AcceptAll} THEN EXIT.
    IF MainWindow{Prop:AcceptAll} THEN EXIT.
    
    DISPLAY()
    !ForceRefresh = False
XL_Setup                                                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel = Create(0, Create:OLE)
        Excel{PROP:Create} = 'Excel.Application'

        Excel{'Application.DisplayAlerts'}  = False         ! no alerts to the user (do you want to save the file?) ! MOVED 10 BDec 2001 John
        Excel{'Application.ScreenUpdating'} = excel:Visible ! False
        Excel{'Application.Visible'}        = excel:Visible ! False

        Excel{'Application.Calculation'}    = xlCalculationManual
        
        DO XL_GetOperatingSystem
        !-----------------------------------------------
    EXIT
XL_Finalize                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Application.Calculation'}    = xlCalculationAutomatic

        Excel{PROP:DEACTIVATE}
        !-----------------------------------------------
    EXIT
XL_AddSheet                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveWorkBook.Sheets("Sheet3").Select'}

        Excel{'ActiveWorkBook.Sheets.Add'}
        !-----------------------------------------------
    EXIT
XL_AddWorkbook               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Application.Workbooks.Add()'}

        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Title")'}            = CLIP(LOC:ProgramName)
        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Author")'}           = CLIP(LOC:UserName)
        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Application Name")'} = LOC:ApplicationName
        !-----------------------------------------------
        ! 4 Dec 2001 John
        ! Delete empty sheets

        Excel{'Sheets("Sheet2").Select'}
        Excel{'ActiveWindow.SelectedSheets.Delete'}

        Excel{'Sheets("Sheet1").Select'}
        !-----------------------------------------------
    EXIT
XL_SaveAllWorksheets                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOOP WHILE Excel{'WorkBooks.Count'} > 0
            Excel{'ActiveWorkBook.Save()'}
            Excel{'ActiveWorkBook.Close()'}
        END !LOOP
        !-----------------------------------------------
    EXIT
XL_DropAllWorksheets                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOOP WHILE Excel{'WorkBooks.Count'} > 0
            Excel{'ActiveWorkBook.Close(' & FALSE & ')'}
        END !LOOP
        !-----------------------------------------------
    EXIT
XL_AddComment                ROUTINE
    DATA
xlComment CSTRING(20)
    CODE
        !-----------------------------------------------
        Excel{'Selection.AddComment("' & CLIP(excel:CommentText) & '")'}

!        xlComment{'Shape.IncrementLeft'} = 127.50
!        xlComment{'Shape.IncrementTop'}  =   8.25
!        xlComment{'Shape.ScaleWidth'}    =   1.76
!        xlComment{'Shape.ScaleHeight'}   =   0.69
        !-----------------------------------------------
    EXIT
XL_RowDown                   ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(1, 0).Select'}
        !-----------------------------------------------
    EXIT
XL_ColLeft                   ROUTINE
    DATA
CurrentCol LONG
    CODE
        !-----------------------------------------------
        CurrentCol = Excel{'ActiveCell.Col'}
        IF CurrentCol = 1
            EXIT
        END !IF

        Excel{'ActiveCell.Offset(0, -1).Select'}
        !-----------------------------------------------
    EXIT
XL_ColRight                  ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(0, 1).Select'}
        !-----------------------------------------------
    EXIT
XL_ColFirstSelect                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, Cells(' & Excel{'ActiveCell.Row'} & ', 1)).Select'}
        !Excel{'ActiveCell.Offset(0, -' & Excel{'ActiveCell.Column'} - 1 & ').Select'}
        !-----------------------------------------------
    EXIT
XL_ColFirst                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(0, -' & Excel{'ActiveCell.Column'} - 1 & ').Select'}
        !-----------------------------------------------
    EXIT
XL_SetLastCell                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, ActiveCell.SpecialCells(' & xlLastCell & ')).Select'}
        !-----------------------------------------------
    EXIT
XL_SetGrid                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlDiagonalDown & ').LineStyle'} = xlNone
        Excel{'Selection.Borders(' & xlDiagonalUp   & ').LineStyle'} = xlNone

        DO XL_SetBorder

        Excel{'Selection.Borders(' & xlInsideVertical & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlInsideVertical & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlInsideVertical & ').ColorIndex'} = xlAutomatic

        Excel{'Selection.Borders(' & xlInsideHorizontal & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlInsideHorizontal & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlInsideHorizontal & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorder                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_SetBorderLeft
        DO XL_SetBorderRight
        DO XL_SetBorderBottom
        DO XL_SetBorderTop
        !-----------------------------------------------
    EXIT
XL_SetBorderLeft                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeLeft & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeLeft & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeLeft & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderRight                           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeRight & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeRight & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeRight & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderBottom                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeBottom & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeBottom & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeBottom & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderTop                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeTop & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeTop & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeTop & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_HighLight                           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Interior.ColorIndex'}        = 6 ! YELLOW
        Excel{'Selection.Interior.Pattern'}           = xlSolid
        Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic
        !-----------------------------------------------
XL_SetTitle                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Interior.ColorIndex'}        = 15 ! GREY
        Excel{'Selection.Interior.Pattern'}           = xlSolid
        Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetWrapText                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.WrapText'}    = True
        !-----------------------------------------------
    EXIT
XL_SetGreyText                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Font.ColorIndex'}    = 16 ! grey
        !-----------------------------------------------
    EXIT
XL_SetBold                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Font.Bold'} = True
        !-----------------------------------------------
    EXIT
XL_FormatDate                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.NumberFormat'} = excel:DateFormat
        !-----------------------------------------------
    EXIT
XL_FormatNumber                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.NumberFormat'} = '#,##0.00'
        !-----------------------------------------------
    EXIT
XL_FormatTextBox                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.ShapeRange.IncrementLeft'} = 127.5
        Excel{'Selection.ShapeRange.IncrementTop'}  =   8.25
!        Excel{'Selection.ShapeRange.ScaleWidth'}    =  '1.76, ' & msoFalse & ', ' & msoScaleFromBottomRight
!        Excel{'Selection.ShapeRange.ScaleHeight'}   =  '0.69, ' & msoFalse & ', ' & msoScaleFromTopLeft
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentCentre            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlCenter
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentLeft              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlLeft
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentRight             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlRight
        !-----------------------------------------------
    EXIT
XL_VerticalAlignmentTop                 ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.VerticalAlignment'} = xlTop
        !-----------------------------------------------
    EXIT
XL_VerticalAlignmentCentre            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.VerticalAlignment'} = xlCenter
        !-----------------------------------------------
    EXIT
XL_SetColumnHeader                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_SetTitle
        DO XL_SetGrid
        DO XL_SetWrapText
        DO XL_SetBold
        !-----------------------------------------------
    EXIT
XL_SetColumn_Date                       ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = 'dd Mmm yyyy'

!        !Excel{'ActiveCell.FormulaR1C1'}  = LEFT(FORMAT(ret:Invoice_Date, @D8-))
        !-----------------------------------------------
    EXIT
XL_SetColumn_Number                     ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = '#,##0.00'
        !-----------------------------------------------
    EXIT
XL_SetColumn_Percent                    ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = '0.00%'
!        Excel{'ActiveCell.NumberFormat'} = CHR(64)       ! Text (AT) Sign
        !-----------------------------------------------
    EXIT
XL_SetColumn_Text                       ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'} = CHR(64) ! Text (AT) Sign
        !-----------------------------------------------
    EXIT
XL_DataSelectRange                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, ActiveCell.SpecialCells(' & xlLastCell & ')).Select'}
        !-----------------------------------------------
    EXIT
XL_DataHideDuplicates                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.AdvancedFilter Action:=' & xlFilterInPlace & ', Unique:=' & True }
        !-----------------------------------------------
    EXIT
XL_DataSortRange                            ROUTINE
    DATA
    CODE                     
        !-----------------------------------------------
        DO XL_DataSelectRange
        Excel{'ActiveWindow.ScrollColumn'} = 1
        Excel{'Selection.Sort Key1:=Range("' & LOC:Text & '"), Order1:=' & xlAscending & ', Header:=' & xlGuess & |
            ', OrderCustom:=1, MatchCase:=' & False & ', Orientation:=' & xlTopToBottom}
        !-----------------------------------------------
    EXIT
XL_DataAutoFilter                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.AutoFilter'}
        !-----------------------------------------------
    EXIT
XL_SetWorksheetLandscape                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.Orientation'}     = xlLandscape
!        Excel{'ActiveSheet.PageSetup.FitToPagesWide'} = 1
!        Excel{'ActiveSheet.PageSetup.FitToPagesTall'} = 9999
!        Excel{'ActiveSheet.PageSetup.Order'}          = xlOverThenDown
        !-----------------------------------------------
    EXIT
XL_SetWorksheetPortrait                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.Orientation'} = xlPortrait
        !-----------------------------------------------
    EXIT
XL_PrintTitleRows                               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.PrintTitleRows'} = CLIP(LOC:Text)
        !-----------------------------------------------
    EXIT
XL_GetCurrentCell                               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOC:Text = Excel{'Application.ConvertFormula( "RC", ' & xlR1C1 & ', ' & xlA1 & ')'}
        !-----------------------------------------------
    EXIT

XL_MergeCells                                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlCenter
        Excel{'Selection.VerticalAlignment'}   = xlBottom
        Excel{'Selection.MergeCells'}          = True
        !-----------------------------------------------
    EXIT

XL_GetOperatingSystem                                                          ROUTINE
    DATA
OperatingSystem     STRING(50)
tmpLen              LONG
LEN_OperatingSystem LONG
    CODE
        !-----------------------------------------------
        excel:OperatingSystem = 0.0    ! Default/error value
        OperatingSystem       = Excel{'Application.OperatingSystem'}
        LEN_OperatingSystem   = LEN(CLIP(OperatingSystem))

        LOOP x# = LEN_OperatingSystem TO 1 BY -1

            CASE SUB(OperatingSystem, x#, 1)
            OF '0' OROF'1' OROF '2' OROF '3' OROF '4' OROF '5' OROF '6' OROF '7' OROF '8' OROF '9' OROF '.'
                ! NULL
            ELSE
                tmpLen                = LEN_OperatingSystem-x#+1
                excel:OperatingSystem = CLIP(SUB(OperatingSystem, x#+1, tmpLen))

                EXIT
            END !CASE
        END !LOOP
        !-----------------------------------------------
    EXIT
!-----------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020610'&'0'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('CostAdjustmentReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:JOBS.Open
  Relate:ORDSTOCK.Open
  Relate:REPSCHCR.Open
  Relate:RETSTOCK.Open
  Access:USERS.UseFile
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  Access:ORDPARTS.UseFile
  Access:ORDERS.UseFile
  Access:ORDPEND.UseFile
  Access:PARTS.UseFile
  Access:WARPARTS.UseFile
  Access:REPSCHED.UseFile
  Access:REPSCHLG.UseFile
  SELF.FilesOpened = True
  OPEN(MainWindow)
  SELF.Opened=True
  ! ========= Set Report Version =============
  Include('..\ReportVersion.inc')
  tmp:VersionNumber = 'Version Number: ' & Clip(tmp:VersionNumber) & '5003'
  ?ReportVersion{prop:Text} = tmp:VersionNumber
      LOC:ProgramName         = 'Cost Adjustment Report' !          Job=2090        Cust=
      param:EndDate   = TODAY()
      param:StartDate = DATE(MONTH(param:EndDate), 1, YEAR(param:EndDate))
  
      excel:Visible           = False
  
      SET(defaults)
      access:defaults.next()
  
      DO GetUserName2
  
      IF Automatic = TRUE
        CommandLine = CLIP(COMMAND(''))
        tmpPos = INSTRING('&', CommandLine)
        param:StartDate      = DATE( MONTH(CLIP(SUB(CommandLine, tmpPos + 1, 30))), 1, YEAR(CLIP(SUB(CommandLine, tmpPos + 1, 30))) )
        param:EndDate       = CLIP(SUB(CommandLine, tmpPos + 1, 30)) !
      END
  Bryan.CompFieldColour()
  ?param:StartDate{Prop:Alrt,255} = MouseLeft2
  ?param:EndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
      IF Automatic = TRUE
  
        DO OKButton_Pressed
  
        POST(Event:CloseWindow)
      END
  SELF.SetAlerts()
  ! Inserting (DBH 31/08/2007) # 9125 - Is this an automatic report?
  If Command('/SCHEDULE')
      x# = Instring('%',Command(),1,1)
      Access:REPSCHED.ClearKey(rpd:RecordNumberKey)
      rpd:RecordNumber = Clip(Sub(Command(),x# + 1,20))
      If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
          !Found
          Access:REPSCHCR.ClearKey(rpc:ReportCriteriaKey)
          rpc:ReportName = rpd:ReportName
          rpc:ReportCriteriaType = rpd:ReportCriteriaType
          If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
              !Found
  
              Case rpc:DateRangeType
              Of 1 ! Today Only
                  param:StartDate = Today()
                  param:EndDate = Today()
              Of 2 ! 1st Of Month
                  param:StartDate = Date(Month(Today()),1,Year(Today()))
                  param:EndDate = Today()
              Of 3 ! Whole Month
  ! Changing (DBH 31/01/2008) # 9711 - Does not account for a change of year
  !                loc:StartDate = Date(Month(Today()) - 1, 1, Year(Today()))
  ! to (DBH 31/01/2008) # 9711
                  param:EndDate = Date(Month(Today()),1,Year(Today())) - 1
                  param:StartDate = Date(Month(param:EndDate),1,Year(param:EndDate))
  ! End (DBH 31/01/2008) #9711
  
              End ! Case rpc:DateRangeType
  
              Do OKButton_Pressed
              Post(Event:CloseWindow)
          Else ! If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
              !Error
          End ! If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
  
      Else ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
          !Error
      End ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
  
  End ! If Command('/SCHEDULE')
  ! End (DBH 31/08/2007) #9125
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:JOBS.Close
    Relate:ORDSTOCK.Close
    Relate:REPSCHCR.Close
    Relate:RETSTOCK.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OkButton
    DO OKButton_Pressed
    OF ?CancelButton
       POST(Event:CloseWindow)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020610'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020610'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020610'&'0')
      ***
    OF ?PopCalendarEnd
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          param:EndDate = TINCALENDARStyle1(param:EndDate)
          Display(?param:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          param:StartDate = TINCALENDARStyle1(param:StartDate)
          Display(?param:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?param:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?param:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendarEnd)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

!-----------------------------------------------
CheckLocation           PROCEDURE( IN:Location )! LONG ! BOOL
    CODE
        !-----------------------------------------------------------------
        WriteDebug('CheckLocation(' & CLIP(IN:Location) & ')')
        ! Check that we are reporting on IN:Locaton
        !-----------------------------------------------------------------
        SORT(LocationQueue,LocationQueue.LocationValue)
        LocationQueue.LocationValue = IN:Location
        GET(LocationQueue,LocationQueue.LocationValue)
        CASE ERRORCODE()
        OF 00 ! Found
            !-------------------------------------------------------------
            !IF LocationQueue.LocationValue = IN:Location
                WriteDebug('CheckLocation(OK)')
                SORT(Location_Queue,Location_Queue.locQ:Location)
                Location_Queue.locQ:Location = IN:Location
                GET(Location_Queue,Location_Queue.locQ:Location)
                RETURN True
            !END !IF
            !-------------------------------------------------------------
            ! Partial Match
            !-------------------------------------------------------------
        OF 30
            !-------------------------------------------------------------
            ! NOT Found - ADD
            !-------------------------------------------------------------
        ELSE
            !-------------------------------------------------------------
            CancelPressed = True

            RETURN False
            !-------------------------------------------------------------
        END !CASE
        !-----------------------------------------------------------------
        WriteDebug('CheckLocation(FAIL)')

        RETURN False
        !-----------------------------------------------------------------
CreateLocationSheet            PROCEDURE(IN:Location, IN:Report)
    CODE
        !-----------------------------------------------------------------
        WriteDebug('CreateLocationSheet(' & CLIP(IN:Location) & ')')

        DO XL_AddSheet
        DO XL_SetWorksheetLandscape

        LOC:SectionName = CLIP(IN:Location)
        excel:CommentText = ''
        DO CreateSectionHeader
        !-----------------------------------------------------------------
        CLEAR(Location_Queue)
            locQ:Location  = IN:Location
            locQ:Report    = IN:Report
            locQ:SheetName = Excel{'ActiveSheet.Name'}
        ADD(Location_Queue, +locQ:Location)

        WriteDebug('CreateLocationSheet(END="' & CLIP(locQ:SheetName) & '")')
        !-----------------------------------------------------------------
DateToString PROCEDURE( IN:Date )! STRING
    CODE
        !-----------------------------------------------
        IF IN:Date = 0
            RETURN ''
        ELSE
            RETURN LEFT(FORMAT(IN:Date, @D8))
        END !IF
        !-----------------------------------------------
LoadJOBS PROCEDURE( IN:JobNumber )!LONG ! BOOL
    CODE
        !-----------------------------------------------
        ! Ref_Number_Key KEY( job:Ref_Number ),NOCASE,PRIMARY
        !
        Access:JOBS.ClearKey(job:Ref_Number_Key)
            job:Ref_Number = IN:JobNumber
        IF Access:JOBS.TryFetch(job:Ref_Number_Key) <> Level:Benign
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadLOCATION  PROCEDURE( IN:Location )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:LOCATION.ClearKey(loc:Location_Key)
            loc:Location = IN:Location
        SET(loc:Location_Key, loc:Location_Key)

        IF Access:LOCATION.NEXT()
            RETURN False
        END !IF

        IF NOT loc:Location = IN:Location
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadORDERS  PROCEDURE( IN:OrderNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        ! Received_Key             KEY(ord:All_Received,                              ord:Order_Number),DUP,NOCASE
        ! Order_Number_Key         KEY(                                               ord:Order_Number),NOCASE,PRIMARY
        ! Printed_Key              KEY(ord:Printed,                                   ord:Order_Number),DUP,NOCASE
        ! Supplier_Printed_Key     KEY(ord:Printed, ord:Supplier,                     ord:Order_Number),DUP,NOCASE
        ! Supplier_Received_Key    KEY(ord:Supplier,                ord:All_Received, ord:Order_Number),DUP,NOCASE
        ! Supplier_Key             KEY(ord:Supplier,                                  ord:Order_Number),DUP,NOCASE
        !
        Access:ORDERS.ClearKey(ord:Order_Number_Key)
        ord:Order_Number = IN:OrderNumber

        IF Access:ORDERS.TryFetch(ord:Order_Number_Key)
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadSTOCK  PROCEDURE( IN:StockNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        ! Ref_Number_Key KEY( sto:Ref_Number ),NOCASE,PRIMARY
        !
        Access:STOCK.ClearKey(sto:Ref_Number_Key)
        sto:Ref_Number = IN:StockNumber

        IF Access:STOCK.TryFetch(sto:Ref_Number_Key)
            RETURN False
        END !IF

        IF sto:Ref_Number <> IN:StockNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
MemberOfSummaryQueue    PROCEDURE(IN:PartNumber)! LONG ! BOOL
    CODE
        !-----------------------------------------------------------------
        sq:PartNumber   = IN:PartNumber
        GET(SummaryQueue, 'sq_PartNumber')
        IF ERRORCODE()
            RETURN False
        END ! IF

        RETURN True
        !-----------------------------------------------------------------
NumberToColumn PROCEDURE(IN:Long)!STRING
FirstCol STRING(1)
Temp     LONG
    CODE
        !-----------------------------------------------
        Temp = IN:Long - 1

        FirstCol = Temp / 26
        IF FirstCol = 0
            RETURN CHR(Temp+65)
        END !IF

        RETURN CHR(FirstCol+64) & CHR( (Temp % 26)+65 )
        !-----------------------------------------------
SetSheetTo          PROCEDURE( IN:SheetName )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        WriteDebug('SetSheetTo(' & CLIP(IN:SheetName) & ')')

        IF CLIP(Excel{'ActiveSheet.Name'}) = CLIP(IN:SheetName)
            WriteDebug('SetSheetTo(Already Set)')

            RETURN True
        END !IF

        Excel{'Sheets("' & CLIP(IN:SheetName) & '").Select'}

        IF CLIP(Excel{'ActiveSheet.Name'}) = CLIP(IN:SheetName)
            WriteDebug('SetSheetTo(OK=' & Excel{'ActiveSheet.Name'} & ')')

            RETURN True
        END !IF

        WriteDebug('SetSheetTo(FAIL)@"' & Excel{'ActiveSheet.Name'} & '"')

        RETURN False
        !-----------------------------------------------
WriteColumn PROCEDURE( IN:String, IN:StartNewRow )
Temp STRING(255)
    CODE
        !-----------------------------------------------
        Temp = IN:String
        IF CLIP(Temp) = ''
            Temp = ''' '
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            IF IN:StartNewRow
                clip:Value = Temp
            ELSE
                clip:Value = CLIP(clip:Value) & '<09>' & Temp
            END !IF

            RETURN
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
WriteDebug  PROCEDURE( IN:Message )
    CODE
        !------------------------------------------------------------------
        !IF NOT debug:Active
            RETURN
       ! END !IF

        debug:Count += 1

        PUTINI(CLIP(LOC:ProgramName), debug:Count, IN:Message, 'c:\Debug.ini')
        !------------------------------------------------------------------
!-----------------------------------------------
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
