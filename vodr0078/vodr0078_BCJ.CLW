  MEMBER('vodr0078.clw')

  INCLUDE('ABFILE.INC'),ONCE

  MAP
vodr0078_BCJ:DctInit    PROCEDURE
vodr0078_BCJ:DctKill    PROCEDURE
vodr0078_BCJ:FilesInit  PROCEDURE
  END

Hide:Access:STOMODEL_ALIAS CLASS(FileManager),TYPE         ! FileManager for STOMODEL_ALIAS
Init                   PROCEDURE(),DERIVED
Kill                   PROCEDURE(),DERIVED
UseFile                PROCEDURE(BYTE UseType = UseType:Uses),BYTE,PROC,DERIVED
                     END


Hide:Relate:STOMODEL_ALIAS CLASS(RelationManager),TYPE     ! RelationManager for STOMODEL_ALIAS
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:TRADEACC_ALIAS CLASS(FileManager),TYPE         ! FileManager for TRADEACC_ALIAS
Init                   PROCEDURE(),DERIVED
Kill                   PROCEDURE(),DERIVED
UseFile                PROCEDURE(BYTE UseType = UseType:Uses),BYTE,PROC,DERIVED
                     END


Hide:Relate:TRADEACC_ALIAS CLASS(RelationManager),TYPE     ! RelationManager for TRADEACC_ALIAS
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:SUBCHRGE_ALIAS CLASS(FileManager),TYPE         ! FileManager for SUBCHRGE_ALIAS
Init                   PROCEDURE(),DERIVED
Kill                   PROCEDURE(),DERIVED
UseFile                PROCEDURE(BYTE UseType = UseType:Uses),BYTE,PROC,DERIVED
                     END


Hide:Relate:SUBCHRGE_ALIAS CLASS(RelationManager),TYPE     ! RelationManager for SUBCHRGE_ALIAS
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:SUBTRACC_ALIAS CLASS(FileManager),TYPE         ! FileManager for SUBTRACC_ALIAS
Init                   PROCEDURE(),DERIVED
Kill                   PROCEDURE(),DERIVED
UseFile                PROCEDURE(BYTE UseType = UseType:Uses),BYTE,PROC,DERIVED
                     END


Hide:Relate:SUBTRACC_ALIAS CLASS(RelationManager),TYPE     ! RelationManager for SUBTRACC_ALIAS
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:MANUFACT_ALIAS CLASS(FileManager),TYPE         ! FileManager for MANUFACT_ALIAS
Init                   PROCEDURE(),DERIVED
Kill                   PROCEDURE(),DERIVED
UseFile                PROCEDURE(BYTE UseType = UseType:Uses),BYTE,PROC,DERIVED
                     END


Hide:Relate:MANUFACT_ALIAS CLASS(RelationManager),TYPE     ! RelationManager for MANUFACT_ALIAS
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:CURRENCY_ALIAS CLASS(FileManager),TYPE         ! FileManager for CURRENCY_ALIAS
Init                   PROCEDURE(),DERIVED
Kill                   PROCEDURE(),DERIVED
UseFile                PROCEDURE(BYTE UseType = UseType:Uses),BYTE,PROC,DERIVED
                     END


Hide:Relate:CURRENCY_ALIAS CLASS(RelationManager),TYPE     ! RelationManager for CURRENCY_ALIAS
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:RTNAWAIT_ALIAS CLASS(FileManager),TYPE         ! FileManager for RTNAWAIT_ALIAS
Init                   PROCEDURE(),DERIVED
Kill                   PROCEDURE(),DERIVED
UseFile                PROCEDURE(BYTE UseType = UseType:Uses),BYTE,PROC,DERIVED
                     END


Hide:Relate:RTNAWAIT_ALIAS CLASS(RelationManager),TYPE     ! RelationManager for RTNAWAIT_ALIAS
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


_Hide:Access:STOMODEL_ALIAS &Hide:Access:STOMODEL_ALIAS,AUTO,THREAD
_Hide:Relate:STOMODEL_ALIAS &Hide:Relate:STOMODEL_ALIAS,AUTO,THREAD
_Hide:Access:TRADEACC_ALIAS &Hide:Access:TRADEACC_ALIAS,AUTO,THREAD
_Hide:Relate:TRADEACC_ALIAS &Hide:Relate:TRADEACC_ALIAS,AUTO,THREAD
_Hide:Access:SUBCHRGE_ALIAS &Hide:Access:SUBCHRGE_ALIAS,AUTO,THREAD
_Hide:Relate:SUBCHRGE_ALIAS &Hide:Relate:SUBCHRGE_ALIAS,AUTO,THREAD
_Hide:Access:SUBTRACC_ALIAS &Hide:Access:SUBTRACC_ALIAS,AUTO,THREAD
_Hide:Relate:SUBTRACC_ALIAS &Hide:Relate:SUBTRACC_ALIAS,AUTO,THREAD
_Hide:Access:MANUFACT_ALIAS &Hide:Access:MANUFACT_ALIAS,AUTO,THREAD
_Hide:Relate:MANUFACT_ALIAS &Hide:Relate:MANUFACT_ALIAS,AUTO,THREAD
_Hide:Access:CURRENCY_ALIAS &Hide:Access:CURRENCY_ALIAS,AUTO,THREAD
_Hide:Relate:CURRENCY_ALIAS &Hide:Relate:CURRENCY_ALIAS,AUTO,THREAD
_Hide:Access:RTNAWAIT_ALIAS &Hide:Access:RTNAWAIT_ALIAS,AUTO,THREAD
_Hide:Relate:RTNAWAIT_ALIAS &Hide:Relate:RTNAWAIT_ALIAS,AUTO,THREAD


vodr0078_BCJ:DctInit PROCEDURE
  CODE
  _Hide:Access:STOMODEL_ALIAS &= NEW(Hide:Access:STOMODEL_ALIAS)
  _Hide:Relate:STOMODEL_ALIAS &= NEW(Hide:Relate:STOMODEL_ALIAS)
  _Hide:Access:TRADEACC_ALIAS &= NEW(Hide:Access:TRADEACC_ALIAS)
  _Hide:Relate:TRADEACC_ALIAS &= NEW(Hide:Relate:TRADEACC_ALIAS)
  _Hide:Access:SUBCHRGE_ALIAS &= NEW(Hide:Access:SUBCHRGE_ALIAS)
  _Hide:Relate:SUBCHRGE_ALIAS &= NEW(Hide:Relate:SUBCHRGE_ALIAS)
  _Hide:Access:SUBTRACC_ALIAS &= NEW(Hide:Access:SUBTRACC_ALIAS)
  _Hide:Relate:SUBTRACC_ALIAS &= NEW(Hide:Relate:SUBTRACC_ALIAS)
  _Hide:Access:MANUFACT_ALIAS &= NEW(Hide:Access:MANUFACT_ALIAS)
  _Hide:Relate:MANUFACT_ALIAS &= NEW(Hide:Relate:MANUFACT_ALIAS)
  _Hide:Access:CURRENCY_ALIAS &= NEW(Hide:Access:CURRENCY_ALIAS)
  _Hide:Relate:CURRENCY_ALIAS &= NEW(Hide:Relate:CURRENCY_ALIAS)
  _Hide:Access:RTNAWAIT_ALIAS &= NEW(Hide:Access:RTNAWAIT_ALIAS)
  _Hide:Relate:RTNAWAIT_ALIAS &= NEW(Hide:Relate:RTNAWAIT_ALIAS)
  Relate:STOMODEL_ALIAS &= _Hide:Relate:STOMODEL_ALIAS
  Relate:TRADEACC_ALIAS &= _Hide:Relate:TRADEACC_ALIAS
  Relate:SUBCHRGE_ALIAS &= _Hide:Relate:SUBCHRGE_ALIAS
  Relate:SUBTRACC_ALIAS &= _Hide:Relate:SUBTRACC_ALIAS
  Relate:MANUFACT_ALIAS &= _Hide:Relate:MANUFACT_ALIAS
  Relate:CURRENCY_ALIAS &= _Hide:Relate:CURRENCY_ALIAS
  Relate:RTNAWAIT_ALIAS &= _Hide:Relate:RTNAWAIT_ALIAS


vodr0078_BCJ:FilesInit PROCEDURE
  CODE
  _Hide:Relate:STOMODEL_ALIAS.Init
  _Hide:Relate:TRADEACC_ALIAS.Init
  _Hide:Relate:SUBCHRGE_ALIAS.Init
  _Hide:Relate:SUBTRACC_ALIAS.Init
  _Hide:Relate:MANUFACT_ALIAS.Init
  _Hide:Relate:CURRENCY_ALIAS.Init
  _Hide:Relate:RTNAWAIT_ALIAS.Init


vodr0078_BCJ:DctKill PROCEDURE
  CODE
  _Hide:Relate:STOMODEL_ALIAS.Kill
  DISPOSE(_Hide:Relate:STOMODEL_ALIAS)
  _Hide:Relate:TRADEACC_ALIAS.Kill
  DISPOSE(_Hide:Relate:TRADEACC_ALIAS)
  _Hide:Relate:SUBCHRGE_ALIAS.Kill
  DISPOSE(_Hide:Relate:SUBCHRGE_ALIAS)
  _Hide:Relate:SUBTRACC_ALIAS.Kill
  DISPOSE(_Hide:Relate:SUBTRACC_ALIAS)
  _Hide:Relate:MANUFACT_ALIAS.Kill
  DISPOSE(_Hide:Relate:MANUFACT_ALIAS)
  _Hide:Relate:CURRENCY_ALIAS.Kill
  DISPOSE(_Hide:Relate:CURRENCY_ALIAS)
  _Hide:Relate:RTNAWAIT_ALIAS.Kill
  DISPOSE(_Hide:Relate:RTNAWAIT_ALIAS)


Hide:Relate:STOMODEL_ALIAS.Init PROCEDURE
  CODE
  _Hide:Access:STOMODEL_ALIAS.Init
  SELF.Init(Access:STOMODEL_ALIAS,1)
  SELF.SetAlias(Relate:STOMODEL)


Hide:Access:STOMODEL_ALIAS.Init PROCEDURE

  CODE
  SELF.Initialized = False
  SELF.Buffer &= stm_ali:Record
  SELF.AliasedFile &= Access:STOMODEL                      !This is a File Alias, so assign aliased file manager
  SELF.FileNameValue = 'STOMODEL_ALIAS'
  SELF.SetErrors(GlobalErrors)
  SELF.File &= STOMODEL_ALIAS
  PARENT.Init
  Access:STOMODEL_ALIAS &= SELF


Hide:Access:STOMODEL_ALIAS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:STOMODEL_ALIAS &= NULL                            ! File manager has been destroyed to assign null, it is an error to reference this after this point


Hide:Access:STOMODEL_ALIAS.UseFile PROCEDURE(BYTE UseType = UseType:Uses)

ReturnValue          BYTE,AUTO

  CODE
  IF UseType ~= UseType:Initialize
    SELF.UseFile(UseType:Initialize)                       !Recursive call to ensure initialization takes place
  END
  IF UseType = UseType:Initialize AND ~SELF.Initialized
    SELF.InUseFile = True
    SELF.Init(STOMODEL_ALIAS,GlobalErrors)
    SELF.Create = 1
    SELF.LockRecover = 10
    SELF.AddKey(stm_ali:RecordNumberKey,'By Record Number',1)
    SELF.AddKey(stm_ali:Model_Number_Key,'By Model Number',0)
    SELF.AddKey(stm_ali:Model_Part_Number_Key,'By Model Number',0)
    SELF.AddKey(stm_ali:Description_Key,'By Description',0)
    SELF.AddKey(stm_ali:Manufacturer_Key,'By Manufacturer',0)
    SELF.AddKey(stm_ali:Ref_Part_Description,'Why?',0)
    SELF.AddKey(stm_ali:Location_Part_Number_Key,'stm_ali:Location_Part_Number_Key',0)
    SELF.AddKey(stm_ali:Location_Description_Key,'stm_ali:Location_Description_Key',0)
    SELF.AddKey(stm_ali:Mode_Number_Only_Key,'By Model Number',0)
  END
                                                           !SELF.InUseFile will be set to False in PARENT
  ReturnValue = PARENT.UseFile(UseType)
  RETURN ReturnValue


Hide:Relate:STOMODEL_ALIAS.Kill PROCEDURE

  CODE
  _Hide:Access:STOMODEL_ALIAS.Kill                         ! Kill the file manager
  PARENT.Kill
  Relate:STOMODEL_ALIAS &= NULL                            ! Assign NULL to the RelationManager's FileManager reference, it is an error to reference this after this point
  DISPOSE(_Hide:Access:STOMODEL_ALIAS)                     ! destroy the file manager


Hide:Relate:TRADEACC_ALIAS.Init PROCEDURE
  CODE
  _Hide:Access:TRADEACC_ALIAS.Init
  SELF.Init(Access:TRADEACC_ALIAS,1)
  SELF.SetAlias(Relate:TRADEACC)


Hide:Access:TRADEACC_ALIAS.Init PROCEDURE

  CODE
  SELF.Initialized = False
  SELF.Buffer &= tra_ali:Record
  SELF.AliasedFile &= Access:TRADEACC                      !This is a File Alias, so assign aliased file manager
  SELF.FileNameValue = 'TRADEACC_ALIAS'
  SELF.SetErrors(GlobalErrors)
  SELF.File &= TRADEACC_ALIAS
  PARENT.Init
  Access:TRADEACC_ALIAS &= SELF


Hide:Access:TRADEACC_ALIAS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:TRADEACC_ALIAS &= NULL                            ! File manager has been destroyed to assign null, it is an error to reference this after this point


Hide:Access:TRADEACC_ALIAS.UseFile PROCEDURE(BYTE UseType = UseType:Uses)

ReturnValue          BYTE,AUTO

  CODE
  IF UseType ~= UseType:Initialize
    SELF.UseFile(UseType:Initialize)                       !Recursive call to ensure initialization takes place
  END
  IF UseType = UseType:Initialize AND ~SELF.Initialized
    SELF.InUseFile = True
    SELF.Init(TRADEACC_ALIAS,GlobalErrors)
    SELF.Create = 1
    SELF.LockRecover = 10
    SELF.AddKey(tra_ali:RecordNumberKey,'By Record Number',1)
    SELF.AddKey(tra_ali:Account_Number_Key,'By Account Number',0)
    SELF.AddKey(tra_ali:Company_Name_Key,'By Company Name',0)
    SELF.AddKey(tra_ali:ReplicateFromKey,'By Account Number',0)
    SELF.AddKey(tra_ali:StoresAccountKey,'By Stores Account',0)
    SELF.AddKey(tra_ali:SiteLocationKey,'By Site Location',0)
    SELF.AddKey(tra_ali:RegionKey,'By Region',0)
  END
                                                           !SELF.InUseFile will be set to False in PARENT
  ReturnValue = PARENT.UseFile(UseType)
  RETURN ReturnValue


Hide:Relate:TRADEACC_ALIAS.Kill PROCEDURE

  CODE
  _Hide:Access:TRADEACC_ALIAS.Kill                         ! Kill the file manager
  PARENT.Kill
  Relate:TRADEACC_ALIAS &= NULL                            ! Assign NULL to the RelationManager's FileManager reference, it is an error to reference this after this point
  DISPOSE(_Hide:Access:TRADEACC_ALIAS)                     ! destroy the file manager


Hide:Relate:SUBCHRGE_ALIAS.Init PROCEDURE
  CODE
  _Hide:Access:SUBCHRGE_ALIAS.Init
  SELF.Init(Access:SUBCHRGE_ALIAS,1)
  SELF.SetAlias(Relate:SUBCHRGE)


Hide:Access:SUBCHRGE_ALIAS.Init PROCEDURE

  CODE
  SELF.Initialized = False
  SELF.Buffer &= suc_ali:Record
  SELF.AliasedFile &= Access:SUBCHRGE                      !This is a File Alias, so assign aliased file manager
  SELF.FileNameValue = 'SUBCHRGE_ALIAS'
  SELF.SetErrors(GlobalErrors)
  SELF.File &= SUBCHRGE_ALIAS
  PARENT.Init
  Access:SUBCHRGE_ALIAS &= SELF


Hide:Access:SUBCHRGE_ALIAS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:SUBCHRGE_ALIAS &= NULL                            ! File manager has been destroyed to assign null, it is an error to reference this after this point


Hide:Access:SUBCHRGE_ALIAS.UseFile PROCEDURE(BYTE UseType = UseType:Uses)

ReturnValue          BYTE,AUTO

  CODE
  IF UseType ~= UseType:Initialize
    SELF.UseFile(UseType:Initialize)                       !Recursive call to ensure initialization takes place
  END
  IF UseType = UseType:Initialize AND ~SELF.Initialized
    SELF.InUseFile = True
    SELF.Init(SUBCHRGE_ALIAS,GlobalErrors)
    SELF.Create = 1
    SELF.LockRecover = 10
    SELF.AddKey(suc_ali:Model_Repair_Type_Key,'By Repair Type',0)
    SELF.AddKey(suc_ali:Account_Charge_Key,'By Charge Type',0)
    SELF.AddKey(suc_ali:Unit_Type_Key,'By Unit Type',0)
    SELF.AddKey(suc_ali:Repair_Type_Key,'By Repair Type',0)
    SELF.AddKey(suc_ali:Cost_Key,'By Cost',0)
    SELF.AddKey(suc_ali:Charge_Type_Only_Key,'suc_ali:Charge_Type_Only_Key',0)
    SELF.AddKey(suc_ali:Repair_Type_Only_Key,'suc_ali:Repair_Type_Only_Key',0)
    SELF.AddKey(suc_ali:Unit_Type_Only_Key,'suc_ali:Unit_Type_Only_Key',0)
    SELF.AddKey(suc_ali:Model_Repair_Key,'suc_ali:Model_Repair_Key',0)
  END
                                                           !SELF.InUseFile will be set to False in PARENT
  ReturnValue = PARENT.UseFile(UseType)
  RETURN ReturnValue


Hide:Relate:SUBCHRGE_ALIAS.Kill PROCEDURE

  CODE
  _Hide:Access:SUBCHRGE_ALIAS.Kill                         ! Kill the file manager
  PARENT.Kill
  Relate:SUBCHRGE_ALIAS &= NULL                            ! Assign NULL to the RelationManager's FileManager reference, it is an error to reference this after this point
  DISPOSE(_Hide:Access:SUBCHRGE_ALIAS)                     ! destroy the file manager


Hide:Relate:SUBTRACC_ALIAS.Init PROCEDURE
  CODE
  _Hide:Access:SUBTRACC_ALIAS.Init
  SELF.Init(Access:SUBTRACC_ALIAS,1)
  SELF.SetAlias(Relate:SUBTRACC)


Hide:Access:SUBTRACC_ALIAS.Init PROCEDURE

  CODE
  SELF.Initialized = False
  SELF.Buffer &= sub_ali:Record
  SELF.AliasedFile &= Access:SUBTRACC                      !This is a File Alias, so assign aliased file manager
  SELF.FileNameValue = 'SUBTRACC_ALIAS'
  SELF.SetErrors(GlobalErrors)
  SELF.File &= SUBTRACC_ALIAS
  PARENT.Init
  Access:SUBTRACC_ALIAS &= SELF


Hide:Access:SUBTRACC_ALIAS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:SUBTRACC_ALIAS &= NULL                            ! File manager has been destroyed to assign null, it is an error to reference this after this point


Hide:Access:SUBTRACC_ALIAS.UseFile PROCEDURE(BYTE UseType = UseType:Uses)

ReturnValue          BYTE,AUTO

  CODE
  IF UseType ~= UseType:Initialize
    SELF.UseFile(UseType:Initialize)                       !Recursive call to ensure initialization takes place
  END
  IF UseType = UseType:Initialize AND ~SELF.Initialized
    SELF.InUseFile = True
    SELF.Init(SUBTRACC_ALIAS,GlobalErrors)
    SELF.Create = 1
    SELF.LockRecover = 10
    SELF.AddKey(sub_ali:RecordNumberKey,'By Record Number',1)
    SELF.AddKey(sub_ali:Main_Account_Key,'By Account Number',0)
    SELF.AddKey(sub_ali:Main_Name_Key,'By Company Name',0)
    SELF.AddKey(sub_ali:Account_Number_Key,'By Account Number',0)
    SELF.AddKey(sub_ali:Branch_Key,'By Branch',0)
    SELF.AddKey(sub_ali:Main_Branch_Key,'By Branch',0)
    SELF.AddKey(sub_ali:Company_Name_Key,'By Company Name',0)
    SELF.AddKey(sub_ali:ReplicateFromKey,'By Account Number',0)
    SELF.AddKey(sub_ali:GenericAccountKey,'sub_ali:GenericAccountKey',0)
    SELF.AddKey(sub_ali:GenericCompanyKey,'sub_ali:GenericCompanyKey',0)
    SELF.AddKey(sub_ali:GenericBranchKey,'sub_ali:GenericBranchKey',0)
    SELF.AddKey(sub_ali:RegionKey,'By Region',0)
  END
                                                           !SELF.InUseFile will be set to False in PARENT
  ReturnValue = PARENT.UseFile(UseType)
  RETURN ReturnValue


Hide:Relate:SUBTRACC_ALIAS.Kill PROCEDURE

  CODE
  _Hide:Access:SUBTRACC_ALIAS.Kill                         ! Kill the file manager
  PARENT.Kill
  Relate:SUBTRACC_ALIAS &= NULL                            ! Assign NULL to the RelationManager's FileManager reference, it is an error to reference this after this point
  DISPOSE(_Hide:Access:SUBTRACC_ALIAS)                     ! destroy the file manager


Hide:Relate:MANUFACT_ALIAS.Init PROCEDURE
  CODE
  _Hide:Access:MANUFACT_ALIAS.Init
  SELF.Init(Access:MANUFACT_ALIAS,1)
  SELF.SetAlias(Relate:MANUFACT)


Hide:Access:MANUFACT_ALIAS.Init PROCEDURE

  CODE
  SELF.Initialized = False
  SELF.Buffer &= man_ali:Record
  SELF.AliasedFile &= Access:MANUFACT                      !This is a File Alias, so assign aliased file manager
  SELF.FileNameValue = 'MANUFACT_ALIAS'
  SELF.SetErrors(GlobalErrors)
  SELF.File &= MANUFACT_ALIAS
  PARENT.Init
  Access:MANUFACT_ALIAS &= SELF


Hide:Access:MANUFACT_ALIAS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:MANUFACT_ALIAS &= NULL                            ! File manager has been destroyed to assign null, it is an error to reference this after this point


Hide:Access:MANUFACT_ALIAS.UseFile PROCEDURE(BYTE UseType = UseType:Uses)

ReturnValue          BYTE,AUTO

  CODE
  IF UseType ~= UseType:Initialize
    SELF.UseFile(UseType:Initialize)                       !Recursive call to ensure initialization takes place
  END
  IF UseType = UseType:Initialize AND ~SELF.Initialized
    SELF.InUseFile = True
    SELF.Init(MANUFACT_ALIAS,GlobalErrors)
    SELF.Create = 1
    SELF.LockRecover = 10
    SELF.AddKey(man_ali:RecordNumberKey,'By Record Number',1)
    SELF.AddKey(man_ali:Manufacturer_Key,'By Manufacturer',0)
    SELF.AddKey(man_ali:EDIFileTypeKey,'By Manufacturer',0)
  END
                                                           !SELF.InUseFile will be set to False in PARENT
  ReturnValue = PARENT.UseFile(UseType)
  RETURN ReturnValue


Hide:Relate:MANUFACT_ALIAS.Kill PROCEDURE

  CODE
  _Hide:Access:MANUFACT_ALIAS.Kill                         ! Kill the file manager
  PARENT.Kill
  Relate:MANUFACT_ALIAS &= NULL                            ! Assign NULL to the RelationManager's FileManager reference, it is an error to reference this after this point
  DISPOSE(_Hide:Access:MANUFACT_ALIAS)                     ! destroy the file manager


Hide:Relate:CURRENCY_ALIAS.Init PROCEDURE
  CODE
  _Hide:Access:CURRENCY_ALIAS.Init
  SELF.Init(Access:CURRENCY_ALIAS,1)
  SELF.SetAlias(Relate:CURRENCY)


Hide:Access:CURRENCY_ALIAS.Init PROCEDURE

  CODE
  SELF.Initialized = False
  SELF.Buffer &= cur_ali:Record
  SELF.AliasedFile &= Access:CURRENCY                      !This is a File Alias, so assign aliased file manager
  SELF.FileNameValue = 'CURRENCY_ALIAS'
  SELF.SetErrors(GlobalErrors)
  SELF.File &= CURRENCY_ALIAS
  PARENT.Init
  Access:CURRENCY_ALIAS &= SELF


Hide:Access:CURRENCY_ALIAS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:CURRENCY_ALIAS &= NULL                            ! File manager has been destroyed to assign null, it is an error to reference this after this point


Hide:Access:CURRENCY_ALIAS.UseFile PROCEDURE(BYTE UseType = UseType:Uses)

ReturnValue          BYTE,AUTO

  CODE
  IF UseType ~= UseType:Initialize
    SELF.UseFile(UseType:Initialize)                       !Recursive call to ensure initialization takes place
  END
  IF UseType = UseType:Initialize AND ~SELF.Initialized
    SELF.InUseFile = True
    SELF.Init(CURRENCY_ALIAS,GlobalErrors)
    SELF.Create = 1
    SELF.LockRecover = 10
    SELF.AddKey(cur_ali:RecordNumberKey,'By Record Number',1)
    SELF.AddKey(cur_ali:CurrencyCodeKey,'By Currency Code',0)
    SELF.AddKey(cur_ali:LastUpdateDateKey,'By Currency Code',0)
    SELF.AddKey(cur_ali:CorrelationCodeKey,'By Correlation Code',0)
  END
                                                           !SELF.InUseFile will be set to False in PARENT
  ReturnValue = PARENT.UseFile(UseType)
  RETURN ReturnValue


Hide:Relate:CURRENCY_ALIAS.Kill PROCEDURE

  CODE
  _Hide:Access:CURRENCY_ALIAS.Kill                         ! Kill the file manager
  PARENT.Kill
  Relate:CURRENCY_ALIAS &= NULL                            ! Assign NULL to the RelationManager's FileManager reference, it is an error to reference this after this point
  DISPOSE(_Hide:Access:CURRENCY_ALIAS)                     ! destroy the file manager


Hide:Relate:RTNAWAIT_ALIAS.Init PROCEDURE
  CODE
  _Hide:Access:RTNAWAIT_ALIAS.Init
  SELF.Init(Access:RTNAWAIT_ALIAS,1)
  SELF.SetAlias(Relate:RTNAWAIT)


Hide:Access:RTNAWAIT_ALIAS.Init PROCEDURE

  CODE
  SELF.Initialized = False
  SELF.Buffer &= rta_ali:Record
  SELF.AliasedFile &= Access:RTNAWAIT                      !This is a File Alias, so assign aliased file manager
  SELF.FileNameValue = 'RTNAWAIT_ALIAS'
  SELF.SetErrors(GlobalErrors)
  SELF.File &= RTNAWAIT_ALIAS
  PARENT.Init
  Access:RTNAWAIT_ALIAS &= SELF


Hide:Access:RTNAWAIT_ALIAS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:RTNAWAIT_ALIAS &= NULL                            ! File manager has been destroyed to assign null, it is an error to reference this after this point


Hide:Access:RTNAWAIT_ALIAS.UseFile PROCEDURE(BYTE UseType = UseType:Uses)

ReturnValue          BYTE,AUTO

  CODE
  IF UseType ~= UseType:Initialize
    SELF.UseFile(UseType:Initialize)                       !Recursive call to ensure initialization takes place
  END
  IF UseType = UseType:Initialize AND ~SELF.Initialized
    SELF.InUseFile = True
    SELF.Init(RTNAWAIT_ALIAS,GlobalErrors)
    SELF.Create = 1
    SELF.LockRecover = 10
    SELF.AddKey(rta_ali:RecordNumberKey,'rta_ali:RecordNumberKey',1)
    SELF.AddKey(rta_ali:ArcProPartKey,'rta_ali:ArcProPartKey',0)
    SELF.AddKey(rta_ali:ArcProDescKey,'rta_ali:ArcProDescKey',0)
    SELF.AddKey(rta_ali:ArcProExcPartKey,'rta_ali:ArcProExcPartKey',0)
    SELF.AddKey(rta_ali:ArcProExcDescKey,'rta_ali:ArcProExcDescKey',0)
    SELF.AddKey(rta_ali:ArcProCNRKey,'By Credit Note Number',0)
    SELF.AddKey(rta_ali:ArcProExcCNRKey,'By Credit Note Number',0)
    SELF.AddKey(rta_ali:RTNORDERKey,'rta_ali:RTNORDERKey',0)
    SELF.AddKey(rta_ali:CREDNOTRKey,'rta_ali:CREDNOTRKey',0)
    SELF.AddKey(rta_ali:GRNNumberKey,'rta_ali:GRNNumberKey',0)
  END
                                                           !SELF.InUseFile will be set to False in PARENT
  ReturnValue = PARENT.UseFile(UseType)
  RETURN ReturnValue


Hide:Relate:RTNAWAIT_ALIAS.Kill PROCEDURE

  CODE
  _Hide:Access:RTNAWAIT_ALIAS.Kill                         ! Kill the file manager
  PARENT.Kill
  Relate:RTNAWAIT_ALIAS &= NULL                            ! Assign NULL to the RelationManager's FileManager reference, it is an error to reference this after this point
  DISPOSE(_Hide:Access:RTNAWAIT_ALIAS)                     ! destroy the file manager

