

   MEMBER('vodr0027.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('VODR0002.INC'),ONCE        !Local module procedure declarations
                     END


TurnaroundTimeProgressReport PROCEDURE                !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::6:TAGFLAG          BYTE(0)
DASBRW::6:TAGMOUSE         BYTE(0)
DASBRW::6:TAGDISPSTATUS    BYTE(0)
DASBRW::6:QUEUE           QUEUE
Pointer2                      LIKE(glo:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
                     MAP   ! UpdateSummaryQueue
!After                       PROCEDURE (STRING, STRING), STRING
AppendString         PROCEDURE( STRING, STRING, STRING ),STRING

DateToString        PROCEDURE( DATE ), STRING
EmptySummaryQueue PROCEDURE()
FillSummaryQueue PROCEDURE()
GetARCBranchIdentification      PROCEDURE()
        
GetDeliveryDateAtARC        PROCEDURE( LONG ), BYTE
GetHeadAccount PROCEDURE( STRING )
GetReturnDateToRRC        PROCEDURE( LONG, DATE ), DATE
GetSubAccount PROCEDURE( STRING )
!Before                      PROCEDURE (STRING, STRING), STRING
HoursBetween                PROCEDURE  (TIME, TIME),LONG

IsRepairAtARC        PROCEDURE( LONG ), LONG ! BOOL
LoadEngineer         PROCEDURE( STRING ),BYTE ! BOOL
LoadSUBTRACC   PROCEDURE( STRING ), LONG ! BOOL
LoadTRADEACC   PROCEDURE( STRING ), LONG ! BOOL
LoadWEBJOB      PROCEDURE( LONG ), LONG ! BOOL
NumberToColumn PROCEDURE( LONG ), STRING
TimeToString        PROCEDURE( TIME ), STRING
UpdateEngineerQueue  PROCEDURE( STRING )
UpdateSummaryQueue   PROCEDURE( STRING, STRING )
WorkingDaysBetween          PROCEDURE  (DATE, DATE, LONG, LONG), LONG
WorkingHoursBetween PROCEDURE  (DATE, DATE, TIME, TIME, TIME, TIME, LONG, LONG), LONG

WriteColumn PROCEDURE( STRING, LONG=False )
WriteDebug      PROCEDURE( STRING )
    END !MAP
tmp:VersionNumber    STRING(30)
Param_Group          GROUP,PRE()
LOC:ReportType       STRING(20)
LOC:EndDate          DATE
LOC:StartDate        DATE
                     END
Clipboard_Group      GROUP,PRE(clip)
OriginalValue        STRING(255)
Saved                BYTE
Value                CSTRING(200)
                     END
Excel_Group          GROUP,PRE()
Excel                SIGNED
excel:DateFormat     STRING('dd mmm yyyy')
excel:ColumnName     STRING(50)
excel:ColumnWidth    REAL
excel:CommentText    STRING(255)
excel:OperatingSystem REAL
excel:Visible        BYTE
                     END
Sheet_Group          GROUP,PRE(sheet)
TempLastCol          STRING(1)
HeadLastCol          STRING('C')
DataLastCol          STRING('O')
HeadSummaryRow       LONG(9)
DataSectionRow       LONG(9)
DataHeaderRow        LONG(11)
                     END
HeaderQueue          QUEUE,PRE(head)
ColumnName           STRING(30)
ColumnWidth          REAL
NumberFormat         STRING(20)
                     END
Local_Group          GROUP,PRE(LOC)
RRCtoARC             BYTE(False)
ARCLocation          STRING(30)
RRCLocation          STRING(30)
ARCBranchIdentification STRING('00')
ARCDateBooked        DATE
RRCDateBooked        DATE
RRCDateReturned      DATE
ApplicationName      STRING('ServiceBase')
DesktopPath          STRING(255)
Filename             STRING(255)
Path                 STRING(255)
ProgramName          STRING(100)
RepairCentreType     STRING(3)
SectionName          STRING(32)
Text                 STRING(255)
Username             STRING(3)
WorkingHours         DECIMAL(10,4)
Version              STRING('3.1.0000')
                     END
Progress_Group       GROUP,PRE()
Progress:Text        STRING(100)
RecordCount          LONG
                     END
Misc_Group           GROUP,PRE()
DoAll                STRING(1)
GUIMode              BYTE
LocalHeadAccount     STRING(30)
LocalTag             STRING(1)
LocalTimeOut         LONG
Result               BYTE
                     END
SummaryQueue         QUEUE,PRE(sq)
Status               STRING(30)
Type                 STRING(10)
Count                LONG
                     END
EngineerQueue        QUEUE,PRE(engq)
UserCode             STRING(3)
Forename             STRING(30)
Surname              STRING(30)
JobsBooked           LONG
                     END
Debug_Group          GROUP,PRE(debug)
Active               LONG
Count                LONG
                     END
HeadAccount_Queue    QUEUE,PRE(haQ)
AccountNumber        STRING(15)
AccountName          STRING(30)
BranchIdentification STRING(2)
StartWorkHours       TIME
EndWorkHours         TIME
IncludeSaturday      LONG
IncludeSunday        LONG
InvoiceSubAccounts   BYTE
                     END
SubAccount_Queue     QUEUE,PRE(saQ)
AccountNumber        STRING(15)
AccountName          STRING(30)
HeadAccountNumber    STRING(15)
HeadAccountName      STRING(30)
BranchIdentification STRING(2)
StartWorkHours       TIME
EndWorkHours         TIME
IncludeSaturday      LONG
IncludeSunday        LONG
Count                LONG
                     END
BRW5::View:Browse    VIEW(TRADEACC)
                       PROJECT(tra:Account_Number)
                       PROJECT(tra:Company_Name)
                       PROJECT(tra:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
LocalTag               LIKE(LocalTag)                 !List box control field - type derived from local data
LocalTag_Icon          LONG                           !Entry's icon ID
tra:Account_Number     LIKE(tra:Account_Number)       !List box control field - type derived from field
tra:Company_Name       LIKE(tra:Company_Name)         !List box control field - type derived from field
tra:RecordNumber       LIKE(tra:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
MainWindow           WINDOW('Report'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,4),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Turnaround Time Progress Report Criteria'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,54,552,310),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Criteria'),USE(?Tab1)
                           STRING('Date Complete From'),AT(251,88),USE(?String4),TRN,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           ENTRY(@D08B),AT(339,88,64,10),USE(LOC:StartDate),IMM,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),TIP('Enter the earliest date to show on report'),REQ
                           BUTTON,AT(407,85),USE(?PopCalendar:1),IMM,TRN,FLAT,LEFT,TIP('Click to show calendar'),ICON('lookupp.jpg')
                           BUTTON('&Rev tags'),AT(312,208,50,13),USE(?DASREVTAG),DISABLE,HIDE
                           BUTTON,AT(496,212),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           STRING('Date Complete To'),AT(251,109),USE(?String5),TRN,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           ENTRY(@D08B),AT(339,109,64,10),USE(LOC:EndDate),IMM,FONT('Tahoma',8,010101H,FONT:bold),COLOR(COLOR:White),TIP('Enter the latest date to show on report'),REQ
                           BUTTON,AT(407,104),USE(?PopCalendar),IMM,TRN,FLAT,LEFT,TIP('Click to show calendar'),ICON('lookupp.jpg')
                           BUTTON('sho&W tags'),AT(312,236,52,12),USE(?DASSHOWTAG),DISABLE,HIDE
                           BUTTON,AT(496,245),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(496,280),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                           CHECK('All Repair Centres'),AT(192,132),USE(DoAll),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('Y','N')
                           CHECK('Show Excel'),AT(432,132),USE(excel:Visible),TRN,LEFT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),TIP('Tick To Show Excel During Report Creation.<13,10>Only Tick if you are having problems' &|
   ' <13,10>with Excel reports.'),VALUE('1','0')
                           LIST,AT(192,144,296,190),USE(?List),IMM,VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)FJ@s1@60L(2)|F~Account Number~@s15@120L(2)|F~Company Name~@s30@'),FROM(Queue:Browse)
                         END
                       END
                       BUTTON,AT(480,366),USE(?OkButton),TRN,FLAT,LEFT,TIP('Click to print report'),ICON('printp.jpg'),DEFAULT
                       BUTTON,AT(548,366),USE(?CancelButton),TRN,FLAT,LEFT,TIP('Click to close this form'),ICON('cancelp.jpg')
                       PROMPT('Report Version'),AT(68,374),USE(?ReportVersion),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                     END

!---   ProgressBar Declarations   --------------------------------------------------
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte
 
progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY,DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
!-----------------------------------------------------------------------------------
!---   Excel EQUATES   -------------------------------------------------------------
xlCalculationManual    EQUATE(0FFFFEFD9h) ! XlCalculation
xlCalculationAutomatic EQUATE(0FFFFEFF7h) ! XlCalculation
!----------------------------------------------------

!----------------------------------------------------
xlNone       EQUATE(0FFFFEFD2h)
xlContinuous EQUATE(        1 ) ! xlFillStyle
xlThin       EQUATE(        2 ) ! XlBorderWeight
!----------------------------------------------------

!----------------------------------------------------
xlAutomatic   EQUATE(0FFFFEFF7h) ! Constants.
xlSolid       EQUATE(        1 ) ! Constants.
xlLeft        EQUATE(0FFFFEFDDh) ! Constants.
xlRight       EQUATE(0FFFFEFC8h) ! Constants.
xlCenter      EQUATE(0FFFFEFF4h) ! Constants.
xlLastCell    EQUATE(       11 ) ! Constants.
xlTop         EQUATE(0FFFFEFC0h) ! Constants.
xlBottom      EQUATE(0FFFFEFF5h) ! Constants.

xlTopToBottom EQUATE(        1 ) ! Constants.
!----------------------------------------------------

!----------------------------------------------------
xlDiagonalDown     EQUATE( 5) ! XlBordersIndex
xlDiagonalUp       EQUATE( 6) ! XlBordersIndex
xlEdgeLeft         EQUATE( 7) ! XlBordersIndex
xlEdgeTop          EQUATE( 8) ! XlBordersIndex
xlEdgeBottom       EQUATE( 9) ! XlBordersIndex
xlEdgeRight        EQUATE(10) ! XlBordersIndex
xlInsideVertical   EQUATE(11) ! XlBordersIndex
xlInsideHorizontal EQUATE(12) ! XlBordersIndex
!----------------------------------------------------

!----------------------------------------------------
xlA1         EQUATE(        1 ) ! XlReferenceStyle
xlR1C1       EQUATE(0FFFFEFCAh) ! XlReferenceStyle
!----------------------------------------------------

!----------------------------------------------------
xlFilterCopy    EQUATE(2) ! XlFilterAction
xlFilterInPlace EQUATE(1) ! XlFilterAction
!----------------------------------------------------

!----------------------------------------------------
xlGuess EQUATE(0) ! XlYesNoGuess
xlYes   EQUATE(1) ! XlYesNoGuess
xlNo    EQUATE(2) ! XlYesNoGuess
!----------------------------------------------------

!----------------------------------------------------
xlAscending  EQUATE(1) ! XlSortOrder
xlDescending EQUATE(2) ! XlSortOrder
!----------------------------------------------------

!----------------------------------------------------
xlLandscape EQUATE(2) ! XlPageOrientation
xlPortrait  EQUATE(1) ! XlPageOrientation
!----------------------------------------------------

!----------------------------------------------------
xlDownThenOver EQUATE( 1 ) ! XlOrder
xlOverThenDown EQUATE( 2 ) ! XlOrder
!-----------------------------------------------------------------------------------
!---   MS Office EQUATES   ---------------------------------------------------------
!----------------------------------------------------
msoCTrue          EQUATE(        1 ) ! MsoTriState
msoFalse          EQUATE(        0 ) ! MsoTriState
msoTriStateMixed  EQUATE(0FFFFFFFEh) ! MsoTriState
msoTriStateToggle EQUATE(0FFFFFFFDh) ! MsoTriState
msoTrue           EQUATE(0FFFFFFFFh) ! MsoTriState
!----------------------------------------------------

!----------------------------------------------------
msoScaleFromBottomRight EQUATE(2) ! MsoScaleFrom
msoScaleFromMiddle      EQUATE(1) ! MsoScaleFrom
msoScaleFromTopLeft     EQUATE(0) ! MsoScaleFrom
!----------------------------------------------------

!----------------------------------------------------
msoPropertyTypeBoolean EQUATE(2) ! MsoDocProperties
msoPropertyTypeDate    EQUATE(3) ! MsoDocProperties
msoPropertyTypeFloat   EQUATE(5) ! MsoDocProperties
msoPropertyTypeNumber  EQUATE(1) ! MsoDocProperties
msoPropertyTypeString  EQUATE(4) ! MsoDocProperties
!----------------------------------------------------
!-----------------------------------------------------------------------------------
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW5                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW5::Sort0:Locator  StepLocatorClass                 !Default Locator
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::6:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW5.UpdateBuffer
   glo:Queue2.Pointer2 = tra:RecordNumber
   GET(glo:Queue2,glo:Queue2.Pointer2)
  IF ERRORCODE()
     glo:Queue2.Pointer2 = tra:RecordNumber
     ADD(glo:Queue2,glo:Queue2.Pointer2)
    LocalTag = 'Y'
  ELSE
    DELETE(glo:Queue2)
    LocalTag = ''
  END
    Queue:Browse.LocalTag = LocalTag
  IF (LocalTag = 'Y')
    Queue:Browse.LocalTag_Icon = 2
  ELSE
    Queue:Browse.LocalTag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW5.Reset
  FREE(glo:Queue2)
  LOOP
    NEXT(BRW5::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue2.Pointer2 = tra:RecordNumber
     ADD(glo:Queue2,glo:Queue2.Pointer2)
  END
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue2)
  BRW5.Reset
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::6:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue2)
    GET(glo:Queue2,QR#)
    DASBRW::6:QUEUE = glo:Queue2
    ADD(DASBRW::6:QUEUE)
  END
  FREE(glo:Queue2)
  BRW5.Reset
  LOOP
    NEXT(BRW5::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::6:QUEUE.Pointer2 = tra:RecordNumber
     GET(DASBRW::6:QUEUE,DASBRW::6:QUEUE.Pointer2)
    IF ERRORCODE()
       glo:Queue2.Pointer2 = tra:RecordNumber
       ADD(glo:Queue2,glo:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASSHOWTAG Routine
   CASE DASBRW::6:TAGDISPSTATUS
   OF 0
      DASBRW::6:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::6:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::6:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW5.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------
OKButton_Pressed            ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        CancelPressed = False
        !debug:Active = True
        !-----------------------------------------------------------------


        !-----------------------------------------------------------------
        ! SB2KDef.ini
        ![RRC]
        !- LOCATION ------------------------------------------------------
        ! RRCLocation            = AT FRANCHISE
        ! InTransit              = IN-TRANSIT TO ARC
        ! ARCLocation            = RECEIVED AT ARC
        ! RTMLocation            = SENT TO 3RD PARTY
        ! DespatchToCustomer     = DESPATCHED
        !- STATUS --------------------------------------------------------
        ! StatusSendToARC        = 450 SEND TO ARC
        ! StatusDespatchedToARC  = 451 DESPATCHED TO ARC
        ! StatusReceivedAtARC    = 452 RECEIVED AT ARC
        ! StatusSendToRRC        = 453 SEND TO RRC
        ! StatusDespatchedToRRC  = 454 DESPATCHED TO RRC
        ! StatusReceivedAtRRC    = 455 RECEIVED AT RRC
        ! InTransitRRC           = IN-TRANSIT TO RRC
        ! StatusARCReceivedQuery = 456 RECEIVED AT ARC (QUERY)
        ! StatusRRCReceivedQuery = 457 RECEIVED AT RRC (QUERY)
        !
        LOC:ARCLocation         = GETINI('RRC',         'ARCLocation',     'RECEIVED AT ARC', '.\SB2KDEF.INI') ! [RRC]ARCLocation=RECEIVED AT ARC
        LOC:RRCLocation         = GETINI('RRC',         'RRCLocation',        'AT FRANCHISE', '.\SB2KDEF.INI') ! [RRC]RRCLocation=AT FRANCHISE
        !OC:StatusReceivedAtRRC = GETINI('RRC', 'StatusReceivedAtRRC', '455 RECEIVED AT RRC', '.\SB2KDEF.INI') ! [RRC]StatusReceivedAtRRC=455 RECEIVED AT RRC
        !OC:DespatchToCustomer  = GETINI('RRC', 'DespatchToCustomer',      'DESPATCHED', '.\SB2KDEF.INI') ! [RRC]DespatchToCustomer=DESPATCHED
        !-----------------------------------------------------------------



        !-----------------------------------------------
        ! 23 Oct 2002 John
        ! R003, berrjo :Insert the report exe name EG VODR56.V01
        ! Do not attach the EXE extenstion but show the version number.  Increment the version number each time changed.
        ! Make this standard on all reports sent for correction.
        ! LOC:Version STRING(8) 3.1.0000 3.1 to match sevicebase version (and dictionary) .0000 increment before each compile
        !
        !LOC:Version = '3.1.0002'
        !-----------------------------------------------
        DO ExportSetup

        GetARCBranchIdentification()

        DO InitColumns
        DO CreateTitleSheet

        !-------------------------------------------------------------
        ! 12 Nov 2002 John Job 2104
        ! MICHALAN: Not all Statuses are included here eg. Awaiting Allocation, Allocated to Engineer
        !IF RECORDS(SummaryQueue) = 0
            FillSummaryQueue()
        !ELSE
        !    EmptySummaryQueue()
        !END !IF
        !-------------------------------------------------------------

        !-----------------------------------------------------------------
        Access:TRADEACC_ALIAS.CLEARKEY(tra_ali:Account_Number_Key)
        SET(tra_ali:Account_Number_Key,tra_ali:Account_Number_Key)

        LOOP WHILE Access:TRADEACC_ALIAS.NEXT() = Level:Benign
            !-------------------------------------------------------------
            WriteDebug('OKButton_Pressed(LOOP ' & CLIP(tra_ali:Account_Number) & ')')

            IF tra_ali:Account_Number = 'XXXRRC'
                WriteDebug('OKButton_Pressed(LOOP XXXRRC)')

                CYCLE
            END !IF

            IF tra_ali:Account_Number = LocalHeadAccount
                LOC:RepairCentreType = 'ARC'
            ELSIF tra_ali:RemoteRepairCentre = 0
                WriteDebug('OKButton_Pressed(LOOP NOT RRC)')

                CYCLE
            ELSE
                LOC:RepairCentreType = 'RRC'
            END !IF

            WriteDebug('OKButton_Pressed("' & LOC:RepairCentreType & '")')
            !-------------------------------------------------------------
            
!            FREE(EngineerQueue)
!            CLEAR(EngineerQueue)

!            FREE(HeaderQueue)
!            CLEAR(HeaderQueue)

!            FREE(SummaryQueue)
!            CLEAR(SummaryQueue)
!
!            FREE(SubAccount_Queue)
!            CLEAR(SubAccount_Queue)
            !-------------------------------------------------------------
            IF DoAll <> 'Y' THEN
                glo:Queue2.Pointer2 = tra_ali:RecordNumber
                GET(glo:Queue2,glo:Queue2.Pointer2)
                IF ERRORCODE() THEN
                    WriteDebug('OKButton_Pressed(DoAll <> "Y"=ERRORCODE(' & ERRORCODE() & ') )')

                    CYCLE
                END !IF
            END !IF
            !-------------------------------------------------------------


            !-------------------------------------------------------------
            DO ExportBody

            IF CancelPressed
                WriteDebug('OKButton_Pressed(LOOP CancelPressed)')

                BREAK
            END !IF
            !-------------------------------------------------------------


            !-------------------------------------------------------------
        END !LOOP
        !-----------------------------------------------------------------
        DO WriteHeadSummary
        DO SaveAllRRCSheets
        DO ExportFinalize

        POST(Event:CloseWindow)
        !-----------------------------------------------------------------
    EXIT
!--------------------------------------------------------------
ExportSetup                             ROUTINE
    DATA
temp DATE
    CODE
        !-----------------------------------------------------------------
        IF CLIP(LOC:FileName) = ''
            DO LoadFileDialog
        END !IF

        IF LOC:FileName = ''
            Case Missive('Invalid Filename.','ServiceBase 3g',|
                           'mstop.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive

            CancelPressed = True

            EXIT
        END !IF LOC:FileName = ''

        IF LOC:FileName <> '' THEN
           IF LOWER(RIGHT(LOC:FileName, 4)) <> '.xls'
              LOC:FileName = CLIP(LOC:FileName) & '.xls'
           END !IF
           !-----------------------------------------------------------------
           SETCURSOR(CURSOR:Wait)

           DO ProgressBar_Setup
           !-----------------------------------------------------------------
           DO XL_Setup

           DO XL_AddWorkbook
           !-----------------------------------------------------------------
        END !IF
    EXIT

ExportBody                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        WriteDebug('ExportBody')

        DO CreateDataSheet
        !-----------------------------------------------------------------
    EXIT
ExportFinalize                          ROUTINE
    DATA
FilenameLength LONG
StartAt        LONG
SUBLength      LONG
    CODE
        !-----------------------------------------------------------------
        DO ResetClipboard

        IF CancelPressed
            DO XL_DropAllWorksheets
            DO XL_Finalize

            EXIT
        END !IF
        !-----------------------------------------------------------------
        Excel{'Sheets("Sheet3").Select'}
        Excel{'ActiveWindow.SelectedSheets.Delete'}

        Excel{'Sheets("Summary").Select'}
        Excel{'Range("A1").Select'}

        Excel{'Application.ActiveWorkBook.SaveAs("' & LEFT(CLIP(LOC:FileName)) & '")'}
        Excel{'Application.ActiveWorkBook.Close()'}
        !-----------------------------------------------------------------
        DO XL_Finalize

        DO ProgressBar_Finalise
        !-----------------------------------------------------------------
        SETCURSOR()

!        IF MATCH(LOC:Filename, CLIP(LOC:DesktopPath) & '*')
!            FilenameLength = LEN(CLIP(LOC:Filename   ))
!            StartAt        = LEN(CLIP(LOC:DesktopPath)) + 1
!            SUBLength      = FilenameLength - StartAt + 1
!
!            Case MessageEx('Export Completed.'                                & |
!                           '<13,10>'                                          & |
!                           '<13,10>Spreadsheet saved to your Desktop'         & |
!                           '<13,10>'                                          & |
!                           '<13,10>' & SUB(LOC:Filename, StartAt, SUBLength),   |
!                         LOC:ApplicationName,                                   |
!                         'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
!            Of 1 ! &OK Button
!            End!Case MessageEx
!        ELSE
!            Case MessageEx('Export Completed.'                        & |
!                           '<13,10>'                                  & |
!                           '<13,10>Spreadsheet saved to '             & |
!                           '<13,10>'                                  & |
!                           '<13,10>' & CLIP(LOC:Filename),              |
!                         LOC:ApplicationName,                           |
!                         'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
!            Of 1 ! &OK Button
!            End!Case MessageEx
!        END !IF
        !-----------------------------------------------------------------
    EXIT
!--------------------------------------------------------------
CreateTitleSheet                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        WriteDebug('CreateTitleSheet()')

        Excel{'ActiveWorkBook.Sheets("Sheet1").Select'}
        !-----------------------------------------------------------------
        LOC:Text          = 'Summary'
        sheet:TempLastCol = sheet:HeadLastCol

        DO CreateWorksheetHeader
        !-----------------------------------------------------------------
        DO XL_SetWorksheetPortrait

        Excel{'ActiveSheet.Columns("A:A").ColumnWidth'}                         = 35
        Excel{'ActiveSheet.Columns("B:B").ColumnWidth'}                         = 35
        Excel{'ActiveSheet.Columns("C:' & sheet:HeadLastCol & '").ColumnWidth'} = 12
        !-----------------------------------------------------------------
        Excel{'Range("A' & sheet:HeadSummaryRow & '").Select'}
            DO XL_SetBold
            Excel{'ActiveCell.Formula'} = 'Summary'

        Excel{'Range("A' & sheet:HeadSummaryRow & ':' & sheet:HeadLastCol & sheet:HeadSummaryRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------------------------
        ! Prepare For summary details
        !
        Excel{'Range("A' & sheet:HeadSummaryRow+1 & '").Select'}
        !-----------------------------------------------------------------
    EXIT
CreateDataSheet                                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        WriteDebug('CreateDataSheet(' & CLIP(tra_ali:Account_Number) & ')')

        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        DO XL_AddSheet
            DO XL_SetWorksheetLandscape
        !-----------------------------------------------------------------
        LOC:SectionName   = CLIP(tra_ali:Account_Number) ! 'Detailed'
        excel:CommentText = ''
        sheet:TempLastCol = sheet:DataLastCol

        DO CreateSectionHeader
        !------------------------------------------
        ! Date_Booked_Key          KEY( job:date_booked    ),DUP,NOCASE
        ! DateCompletedKey         KEY( job:Date_Completed ),DUP,NOCASE
        !
        Access:JOBS.ClearKey(job:DateCompletedKey)
            job:Date_Completed = LOC:StartDate
        SET(job:DateCompletedKey, job:DateCompletedKey)
        !------------------------------------------
        Progress:Text    = CLIP(LOC:SectionName)
        RecordsToProcess = (LOC:EndDate - LOC:StartDate) * 1000

        DO ProgressBar_LoopPre
        !-----------------------------------------------------------------
        LOOP UNTIL Access:JOBS.Next()
            !-------------------------------------------------------------
            DO ProgressBar_Loop

            IF CancelPressed
                BREAK
            END !IF
            !-------------------------------------------------------------
            IF job:Date_Completed > LOC:EndDate
                WriteDebug('CreateDataSheet(job:Date_Completed"' & DateToString(job:Date_Completed) & '" > LOC:EndDate"' & DateToString(LOC:EndDate) & '")')

                BREAK
            END !IF
            !-------------------------------------------------------------
            DO WriteColumns
            !-------------------------------------------------------------
        END !LOOP

        DO ProgressBar_LoopPost
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        DO WriteDataSummary
        !-----------------------------------------------------------------
    EXIT
!--------------------------------------------------------------
CreateSectionHeader                                        ROUTINE
    DATA
LastColumn STRING(1)
    CODE
        !-----------------------------------------------       
        WriteDebug('CreateSectionHeader("' & LEFT(tra_ali:Account_Number, 30) & '")')
        LOC:Text = LEFT(tra_ali:Account_Number, 30)

        DO CreateWorksheetHeader
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataSectionRow & ':' & sheet:DataLastCol & sheet:DataSectionRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A' & sheet:DataSectionRow & '").Select'}
            Excel{'ActiveCell.Formula'}         = 'Section Name:'
            DO XL_ColRight
                Excel{'ActiveCell.Formula'}     = CLIP(LOC:SectionName)
                DO XL_SetBold
                DO XL_HorizontalAlignmentLeft

        Excel{'Range("E' & sheet:DataSectionRow & '").Select'}
            Excel{'ActiveCell.Formula'}         = 'Total Records:'
            DO XL_HorizontalAlignmentRight
            DO XL_ColRight
                Excel{'ActiveCell.Formula'}     = 0
                DO XL_HorizontalAlignmentLeft
        !-----------------------------------------------
        DO SetColumns

        Excel{'Range("A' & sheet:DataHeaderRow+1 & '").Select'}
        !-----------------------------------------------
    EXIT
CreateWorksheetHeader                                                               ROUTINE
    DATA
CurrentRow LONG(4)
    CODE
        !-----------------------------------------------
        WriteDebug('CreateWorksheetHeader("' & CLIP(LOC:Text) & '")')

        Excel{'ActiveSheet.Name'} = CLIP(LOC:Text)
        !-----------------------------------------------       
        Excel{'Range("A1").Select'}
            Excel{'ActiveCell.Formula'}   = CLIP(LOC:ProgramName) & ' ' & CLIP(tra_ali:Account_Number)
            DO XL_SetBold
            Excel{'ActiveCell.Font.Size'} = 16

        Excel{'Range("B3").Select'}
            Excel{'ActiveCell.Formula'}   = Clip(tmp:VersionNumber)
            DO XL_SetBold
            Excel{'ActiveCell.Font.Size'} = 8

        Excel{'Range("A1:' & sheet:TempLastCol & '1").Select'}
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------
        Excel{'Range("A3:' & sheet:TempLastCol & '3").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A3").Select'}
            Excel{'ActiveCell.Formula'}          = 'Criteria'
            DO XL_SetBold
        !-----------------------------------------------
        Excel{'Range("A' & CurrentRow & '").Select'}
            Excel{'ActiveCell.FormulaR1C1'}      = 'Date Complete From:'
            DO XL_ColRight
                DO XL_FormatDate
                DO XL_HorizontalAlignmentLeft
                Excel{'ActiveCell.Formula'}      = DateToString(LOC:StartDate)

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.FormulaR1C1'}      = 'Date Complete To:'
                DO XL_ColRight
                    DO XL_FormatDate
                    DO XL_HorizontalAlignmentLeft
                    Excel{'ActiveCell.Formula'}      = DateToString(LOC:EndDate)

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.Formula'}          = 'Created By:'
                DO XL_ColRight
                    Excel{'ActiveCell.Formula'}      = LOC:UserName

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.Formula'}          = 'Date Created:'
                DO XL_ColRight
                    DO XL_FormatDate
                    DO XL_HorizontalAlignmentLeft
                    Excel{'ActiveCell.Formula'}      = DateToString(TODAY())
        !-----------------------------------------------
        Excel{'Range("A4:' & sheet:TempLastCol & CurrentRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------
        sheet:DataSectionRow = CurrentRow           + 2
        sheet:DataHeaderRow  = sheet:DataSectionRow + 2
        sheet:HeadSummaryRow = sheet:DataSectionRow
        !-----------------------------------------------
    EXIT
!--------------------------------------------------------------
InitColumns                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        WriteDebug('InitColumns()')

        FREE(HeaderQueue)

        !-----------------------------------------------
        ! 11 Nov 2002 John
        ! MICHALAN: This should read Service Base Job Number
        ! John "SB Job Number" is standard
        head:ColumnName       = 'SB Job Number'
            head:ColumnWidth  = 22.00
            head:NumberFormat = '###0'
            ADD(HeaderQueue)
        !-----------------------------------------------

        !-----------------------------------------------
        ! 11 Nov 2002 John
        ! MICHALAN: Insert  2 columns before Date Booked:
        ! 1 = Franchise Branch Code
        ! 2 = Franchise Job Number
        head:ColumnName       = 'Franchise Branch Number'
            head:ColumnWidth  = 12.00
            head:NumberFormat = '00'
            ADD(HeaderQueue)
        !-----------------------------------------------

        !-----------------------------------------------
        ! 11 Nov 2002 John
        ! MICHALAN: Insert  2 columns before Date Booked:
        ! 1 = Franchise Branch Code
        ! 2 = Franchise Job Number
        head:ColumnName       = 'Franchise Job Number'
            head:ColumnWidth  = 12.00
            head:NumberFormat = '###0'
            ADD(HeaderQueue)
        !-----------------------------------------------

        head:ColumnName       = 'Date Booked'
            head:ColumnWidth  = 11.00
            head:NumberFormat = excel:DateFormat
            ADD(HeaderQueue)

        !-----------------------------------------------
        ! 11 Nov 2002 John
        ! berrjo: Insert New Columns:
        ! Date Booked - *Time Booked* - Date Complete - *Time Complete* - Days Turnaround Time - *Hours Turnaround Time*
        ! Calculate the hours turnaround time between the date booked and the date completed.
        ! Reference the working hours in the Trade Account file.
        ! Exclude weekends if the trade account file Hours Of Business does not include them.
        !
        ! If Job booked on monday at 9am and finished next monday at 9am and weekends excluded and Hours of business are 9 to 5
        !       the the turn around time will = 8hours X 5days excluding weekends = 40 hours.
        !
        head:ColumnName       = 'Time Booked'
            head:ColumnWidth  = 11.00
            head:NumberFormat = 'h:mm'
            ADD(HeaderQueue)
        !-----------------------------------------------

        head:ColumnName       = 'Date Complete'
            head:ColumnWidth  = 11.00
            head:NumberFormat = excel:DateFormat
            ADD(HeaderQueue)

        !-----------------------------------------------
        ! 11 Nov 2002 John
        ! berrjo: Insert New Columns:
        ! Date Booked - *Time Booked* - Date Complete - *Time Complete* - Days Turnaround Time - *Hours Turnaround Time*
        !
        head:ColumnName       = 'Time Complete'
            head:ColumnWidth  = 11.00
            head:NumberFormat = 'h:mm'
            ADD(HeaderQueue)
        !-----------------------------------------------

        !-----------------------------------------------
        ! 11 Nov 2002 John
        ! MICHALAN: Please rename to Days Turn Around Time. (Was "Turnaround" John)
        ! Within the Trade Account File there are fields that indicate whether a particular franchise works weekends.
        ! When calculating the turn around time reference the Trade Account Fields to include or exclude the weekends per franchise
        head:ColumnName       = 'Days Turn Around Time' ! 
            head:ColumnWidth  = 11.00
            head:NumberFormat = '#,##0'
            ADD(HeaderQueue)
        !-----------------------------------------------

        !-----------------------------------------------
        ! 11 Nov 2002 John
        ! berrjo: Insert New Columns:
        ! Date Booked - *Time Booked* - Date Complete - *Time Complete* - Days Turnaround Time - *Hours Turnaround Time*
        !
        head:ColumnName       = 'Hours Turnaround Time'
            head:ColumnWidth  = 11.00
            head:NumberFormat = '#,##0'
            ADD(HeaderQueue)
        !-----------------------------------------------


        !-----------------------------------------------
        ! 12 Nov 2002 John        !MICHALAN:Insert 3 new columns:Current Location & Originating Site.
        !
        !Date Despatched - Turnaround Time - Current Location - Originating Site - Repair Location - Make
        !
        !Show the branch code for Originating Site & Repair Location  EG    01
        head:ColumnName       = 'Current Location'
            head:ColumnWidth  = 18.00
            head:NumberFormat = '00'
            ADD(HeaderQueue)
        !-----------------------------------------------


        !-----------------------------------------------
        ! 12 Nov 2002 John        !MICHALAN:Insert 3 new columns:Current Location & Originating Site.
        !
        !Date Despatched - Turnaround Time - Current Location - Originating Site - Repair Location - Make
        !
        !Show the branch code for Originating Site & Repair Location  EG    01
        head:ColumnName       = 'Originating Site'
            head:ColumnWidth  = 11.00
            head:NumberFormat = '00'
            ADD(HeaderQueue)
        !-----------------------------------------------


        !-----------------------------------------------
        ! 12 Nov 2002 John        !MICHALAN:Insert 3 new columns:Current Location & Originating Site.
        !
        !Date Despatched - Turnaround Time - Current Location - Originating Site - Repair Location - Make
        !
        !Show the branch code for Originating Site & Repair Location  EG    01
        head:ColumnName       = 'Repair Location'
            head:ColumnWidth  = 11.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)
        !-----------------------------------------------


        ! DO WriteColumn_Manufacturer
        head:ColumnName       = 'Make'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)


        ! DO WriteColumn_ModelNumber
        head:ColumnName       = 'Model'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        ! DO WriteColumn_UnitType               ! Unit Type
        head:ColumnName       = 'Unit Type'
            head:ColumnWidth  = 11.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        ! DO WriteColumn_TradeAccountNumber
        head:ColumnName       = 'Account'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        ! DO WriteColumn_TradeAccountName
        head:ColumnName       = 'Name'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        ! DO WriteColumn_ESN                    ! IMEI
        head:ColumnName       = 'IMEI'
            head:ColumnWidth  = 20.00
            head:NumberFormat = '###############'
            ADD(HeaderQueue)

        ! DO WriteColumn_MSN                    ! MSN
        head:ColumnName       = 'MSN'
            head:ColumnWidth  = 20.00
            head:NumberFormat = '#####'
            ADD(HeaderQueue)

        !-----------------------------------------------
        ! 11 Nov 2002 John
        ! MICHALAN: Please ensure that all Statuses are included
        ! Make this column wider so that the status displays correctly.
        ! DO WriteColumn_CurrentStatus_Job      ! Current  Status
        head:ColumnName       = 'Current Status'
            head:ColumnWidth  = 30.00 ! Was 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)
        !-----------------------------------------------


        ! DO WriteColumn_CurrentStatus_Exchange ! Exchange Status
        head:ColumnName       = 'Exchange Status'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        ! DO WriteColumn_CurrentStatus_Loan     ! Loan     Status
        head:ColumnName       = 'Loan Status'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        ! DO WriteColumn_Engineer
        head:ColumnName       = 'Engineer'
            head:ColumnWidth  = 15.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        !-----------------------------------------------
        sheet:DataLastCol = NumberToColumn(RECORDS(HeaderQueue))
        WriteDebug('InitColumns("' & sheet:DataLastCol & '")')
        !-----------------------------------------------
    EXIT

!        head:ColumnName       = 'zzz'
!            head:ColumnWidth  = 11.00
!            head:NumberFormat = '###0'
!            head:NumberFormat = '#,##0.00'
!            head:NumberFormat = '0'
!            head:NumberFormat = '0000'
!            head:NumberFormat = excel:DateFormat
!            head:NumberFormat = chr(64) ! Text (AT) Sign
!            ADD(HeaderQueue)
FormatColumns                                                   ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        WriteDebug('FormatColumns()')

        IF RecordCount < 1
            EXIT
        END !IF
        !-----------------------------------------------
        LOOP x# = 1 TO RECORDS(HeaderQueue)
            !-------------------------------------------
            GET(HeaderQueue, x#)
            IF ERRORCODE()
                CYCLE
            END !IF

            Excel{'Range("' & CHR(64+x#) & sheet:DataHeaderRow+1 & ':' & CHR(64+x#) & sheet:DataHeaderRow+RecordCount & '").Select'}
                Excel{'Selection.NumberFormat'} = head:NumberFormat
                !DO XL_HighLight ! DEBUG
                DO XL_HorizontalAlignmentRight
            !-------------------------------------------
        END !LOOP
        !-----------------------------------------------
    EXIT
SetColumns                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        WriteDebug('SetColumns()')

        Excel{'Range("A' & sheet:DataHeaderRow & ':' & sheet:DataLastCol & sheet:DataHeaderRow & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & sheet:DataHeaderRow  & '").Select'}
        !-----------------------------------------------
        LOOP x# = 1 TO RECORDS(HeaderQueue)
            GET(HeaderQueue, x#)

            Excel{'ActiveCell.Formula'}         = head:ColumnName
                Excel{'ActiveCell.ColumnWidth'} = head:ColumnWidth
                DO XL_ColRight
        END !LOOP
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataHeaderRow+1  & '").Select'}
        Excel{'ActiveWindow.FreezePanes'} = True
        !-----------------------------------------------
    EXIT
WriteColumns                                                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        WriteDebug('WriteColumns')
        !###############################################
        IF     LOC:RepairCentreType = 'RRC'
            !-------------------------------------------
            IF NOT LoadWEBJOB(job:Ref_Number)
                WriteDebug('WriteColumns(FAIL Unable to LOADWebjob(' & job:Ref_Number & '))')
                EXIT
            END !IF

            IF tra_ali:Account_Number <> wob:HeadAccountNumber
                WriteDebug('WriteColumns(EXIT IF tra_ali:Account_Number"' & CLIP(tra_ali:Account_Number) & '" <> wob:HeadAccountNumber"' & CLIP(wob:HeadAccountNumber) & '")')

                EXIT
            END !IF
            !-------------------------------------------
            IF GetDeliveryDateAtARC(job:Ref_Number)
                ! RRC -> ARC  <====================(1)
                !
                WriteDebug('WriteColumns(FAIL RRC->ARC)')
                EXIT
            END !IF
            !-------------------------------------------
            ! RRC -> RRC
            !
            LOC:RRCDateBooked   = job:Date_Booked
            LOC:ARCDateBooked   = ''
            LOC:RRCDateReturned = ''
            LOC:RRCtoARC        = False

            WriteDebug('WriteColumns(OK RRC->RRC)')
            DO WriteColumns_RRC

            EXIT
            !-------------------------------------------
        END !IF
        !###############################################
        !-----------------------------------------------
        ! LOC:RepairCentreType = 'ARC'
        !-----------------------------------------------
        IF NOT LoadWEBJOB(job:Ref_Number)
            WriteDebug('WriteColumns(FAIL Unable to LOADWebjob(' & job:Ref_Number & '))')
            EXIT
        END !IF

        GetHeadAccount(wob:HeadAccountNumber)
        !-----------------------------------------------
        IF HaQ:AccountNumber = LocalHeadAccount
            !---------------------------------------
            ! ARC -> ARC
            !---------------------------------------
            LOC:RRCDateBooked   = ''
            LOC:ARCDateBooked   = job:Date_Booked
            LOC:RRCDateReturned = ''
            LOC:RRCtoARC        = False

            WriteDebug('WriteColumns(OK ARC->ARC)')
            DO WriteColumns_ARC

            EXIT
            !-------------------------------------------
        END !IF
        !###############################################
        IF GetDeliveryDateAtARC(job:Ref_Number) = TRUE
            !---------------------------------------
            ! RRC -> ARC  <====================(1)
            !---------------------------------------
            LOC:RRCDateBooked   = job:Date_Booked
            !OC:ARCDateBooked   = set by GetDeliveryDateAtARC()
            LOC:RRCDateReturned = GetReturnDateToRRC(job:Ref_Number, LOC:ARCDateBooked)
            IF LOC:RRCDateReturned = 0
                LOC:RRCDateReturned = ''
            END
            LOC:RRCtoARC        = True

            WriteDebug('WriteColumns(OK RRC->ARC)')
            DO WriteColumns_ARC
            !---------------------------------------
        ELSE
            !---------------------------------------
            ! RRC -> RRC Unreachable code
            WriteDebug('WriteColumns(RRC -> RRC Unreachable code)')
            !---------------------------------------
            EXIT
            !---------------------------------------
        END !IF
        !###############################################
    EXIT

WriteColumns_ARC                                                       ROUTINE
    DATA
Turnaround      LONG
HoursTurnaround LONG
    CODE
        !-----------------------------------------------
        WriteDebug('WriteColumns_ARC()')


!        IF tra_ali:Account_Number <> haQ:AccountNumber
!            WriteDebug('WriteColumns(EXIT IF tra_ali:Account_Number"' & CLIP(tra_ali:Account_Number) & '" <> haQ:AccountNumber"' & CLIP(haQ:AccountNumber) & '")')
!
!            EXIT
!        END !IF
        !===============================================
        GetSubAccount(job:Account_Number)
        saQ:Count += 1
        PUT(SubAccount_Queue)
        GetHeadAccount(wob:HeadAccountNumber)

        Turnaround      = WorkingDaysBetween( job:date_booked, job:Date_Completed,                                                                            saQ:IncludeSaturday, saQ:IncludeSunday)
        HoursTurnaround = WorkingHoursBetween(job:date_booked, job:Date_Completed, job:Time_Booked, job:Time_Completed, saQ:StartWorkHours, saQ:EndWorkHours, saQ:IncludeSaturday, saQ:IncludeSunday)
        !-----------------------------------------------
        UpdateSummaryQueue( job:Current_Status, 'JOB' )

        !LoadEngineer(job:Engineer)

        UpdateEngineerQueue( job:Engineer )
        !-----------------------------------------------
        RecordCount += 1
            WriteColumn( job:Ref_Number, True             ) ! SB Job Number
            !-------------------------------------------
            ! 11 Nov 2002 John
            ! MICHALAN: Insert  2 columns before Date Booked:
            ! 1 = Franchise Branch Code
            ! 2 = Franchise Job Number
            WriteColumn( saQ:BranchIdentification         ) ! Branch Number
            !-------------------------------------------

            !-------------------------------------------
            ! 11 Nov 2002 John
            ! MICHALAN: Insert  2 columns before Date Booked:
            ! 1 = Franchise Branch Code
            ! 2 = Franchise Job Number
            WriteColumn( wob:JobNumber                    ) ! Franchise Job Number
            !-------------------------------------------

            WriteColumn( DateToString(job:Date_Booked)    ) ! Date Booked

            !-----------------------------------------------
            ! 11 Nov 2002 John
            ! berrjo: Insert New Columns:
            ! Date Booked - *Time Booked* - Date Complete - *Time Complete* - Days Turnaround Time - *Hours Turnaround Time*
            !
            WriteColumn( TimeToString(job:Time_Booked)    ) ! Time Booked
            !-----------------------------------------------

            WriteColumn( DateToString(job:Date_Completed) ) ! Date Complete

            !-----------------------------------------------
            ! 11 Nov 2002 John
            ! berrjo: Insert New Columns:
            ! Date Booked - *Time Booked* - Date Complete - *Time Complete* - Days Turnaround Time - *Hours Turnaround Time*
            !
            WriteColumn( TimeToString(job:Time_Completed) ) ! Time Complete
            !-----------------------------------------------

            WriteColumn( Turnaround                       ) ! Turnaround

            !-----------------------------------------------
            ! 11 Nov 2002 John
            ! berrjo: Insert New Columns:
            ! Date Booked - *Time Booked* - Date Complete - *Time Complete* - Days Turnaround Time - *Hours Turnaround Time*
            !
            WriteColumn( HoursTurnaround                  ) ! Hours Turnaround Time
            !-----------------------------------------------


            !-----------------------------------------------
            ! 12 Nov 2002 John        !MICHALAN:Insert 3 new columns:Current Location & Originating Site.
            !
            !Date Despatched - Turnaround Time - Current Location - Originating Site - Repair Location - Make
            !
            !Show the branch code for Originating Site & Repair Location  EG    01
            WriteColumn( job:Location                   ) ! Current Location
            !-----------------------------------------------


            !-----------------------------------------------
            ! 12 Nov 2002 John        !MICHALAN:Insert 3 new columns:Current Location & Originating Site.
            !
            !Date Despatched - Turnaround Time - Current Location - Originating Site - Repair Location - Make
            !
            !Show the branch code for Originating Site & Repair Location  EG    01
            WriteColumn( saQ:BranchIdentification         ) ! Originating Site
            !-----------------------------------------------


            !-----------------------------------------------
            ! 12 Nov 2002 John        !MICHALAN:Insert 3 new columns:Current Location & Originating Site.
            !
            !Date Despatched - Turnaround Time - Current Location - Originating Site - Repair Location - Make
            !
            !Show the branch code for Originating Site & Repair Location  EG    01
            IF LOC:RRCtoARC !IsRepairAtARC(job:Ref_Number)
                WriteColumn( LOC:ARCBranchIdentification  ) ! Repair Location
            ELSE
                WriteColumn( saQ:BranchIdentification     ) ! Repair Location
            END !IF
            !-----------------------------------------------


            WriteColumn( job:Manufacturer                 ) ! Make
            WriteColumn( job:Model_Number                 ) ! Model
            WriteColumn( job:Unit_Type                    ) ! Unit Type

            WriteColumn( saQ:AccountNumber                ) ! Account
            WriteColumn( saQ:AccountName                  ) ! Name

            WriteColumn( job:ESN                          ) ! IMEI
            WriteColumn( job:MSN                          ) ! MSN

            WriteColumn( job:Current_Status               ) ! Current  Status
            WriteColumn( job:Exchange_Status              ) ! Exchange Status
            WriteColumn( job:Loan_Status                  ) ! Loan     Status

            DO WriteColumn_Engineer
        IF excel:OperatingSystem < 5
            DO PassViaClipboard
        END !IF
        !-----------------------------------------------
        DO XL_ColFirst
        DO XL_RowDown
        !-----------------------------------------------
    EXIT
                                                                                                                                                                                
WriteColumns_RRC                                                       ROUTINE
    DATA
Turnaround      LONG
HoursTurnaround LONG
    CODE
        !-----------------------------------------------
        WriteDebug('WriteColumns_RRC()')
        !-----------------------------------------------
        !===============================================
        GetSubAccount(job:Account_Number)
        saQ:Count += 1
        PUT(SubAccount_Queue)

        GetHeadAccount(wob:HeadAccountNumber)

        Turnaround      = WorkingDaysBetween( job:date_booked, job:Date_Completed,                                                                            saQ:IncludeSaturday, saQ:IncludeSunday)
        HoursTurnaround = WorkingHoursBetween(job:date_booked, job:Date_Completed, job:Time_Booked, job:Time_Completed, saQ:StartWorkHours, saQ:EndWorkHours, saQ:IncludeSaturday, saQ:IncludeSunday)
        !-----------------------------------------------
        LoadWEBJOB(job:Ref_Number)

        UpdateSummaryQueue( job:Current_Status, 'JOB' )

        !LoadEngineer(job:Engineer)

        UpdateEngineerQueue( job:Engineer )
        !-----------------------------------------------
        RecordCount += 1
            WriteColumn( job:Ref_Number, True             ) ! SB Job Number
            !-------------------------------------------
            ! 11 Nov 2002 John
            ! MICHALAN: Insert  2 columns before Date Booked:
            ! 1 = Franchise Branch Code
            ! 2 = Franchise Job Number
            WriteColumn( haQ:BranchIdentification         ) ! Branch Number
            !-------------------------------------------

            !-------------------------------------------
            ! 11 Nov 2002 John
            ! MICHALAN: Insert  2 columns before Date Booked:
            ! 1 = Franchise Branch Code
            ! 2 = Franchise Job Number
            WriteColumn( wob:JobNumber                    ) ! Franchise Job Number
            !-------------------------------------------

            WriteColumn( DateToString(job:Date_Booked)    ) ! Date Booked

            !-----------------------------------------------
            ! 11 Nov 2002 John
            ! berrjo: Insert New Columns:
            ! Date Booked - *Time Booked* - Date Complete - *Time Complete* - Days Turnaround Time - *Hours Turnaround Time*
            !
            WriteColumn( TimeToString(job:Time_Booked)    ) ! Time Booked
            !-----------------------------------------------

            WriteColumn( DateToString(job:Date_Completed) ) ! Date Complete

            !-----------------------------------------------
            ! 11 Nov 2002 John
            ! berrjo: Insert New Columns:
            ! Date Booked - *Time Booked* - Date Complete - *Time Complete* - Days Turnaround Time - *Hours Turnaround Time*
            !
            WriteColumn( TimeToString(job:Time_Completed) ) ! Time Complete
            !-----------------------------------------------

            WriteColumn( Turnaround                       ) ! Turnaround

            !-----------------------------------------------
            ! 11 Nov 2002 John
            ! berrjo: Insert New Columns:
            ! Date Booked - *Time Booked* - Date Complete - *Time Complete* - Days Turnaround Time - *Hours Turnaround Time*
            !
            WriteColumn( HoursTurnaround                  ) ! Hours Turnaround Time
            !-----------------------------------------------


            !-----------------------------------------------
            ! 12 Nov 2002 John        !MICHALAN:Insert 3 new columns:Current Location & Originating Site.
            !
            !Date Despatched - Turnaround Time - Current Location - Originating Site - Repair Location - Make
            !
            !Show the branch code for Originating Site & Repair Location  EG    01
            WriteColumn( job:Location                   ) ! Current Location
            !-----------------------------------------------


            !-----------------------------------------------
            ! 12 Nov 2002 John        !MICHALAN:Insert 3 new columns:Current Location & Originating Site.
            !
            !Date Despatched - Turnaround Time - Current Location - Originating Site - Repair Location - Make
            !
            !Show the branch code for Originating Site & Repair Location  EG    01
            WriteColumn( haQ:BranchIdentification         ) ! Originating Site
            !-----------------------------------------------


            !-----------------------------------------------
            ! 12 Nov 2002 John        !MICHALAN:Insert 3 new columns:Current Location & Originating Site.
            !
            !Date Despatched - Turnaround Time - Current Location - Originating Site - Repair Location - Make
            !
            !Show the branch code for Originating Site & Repair Location  EG    01
            WriteColumn( haQ:BranchIdentification     ) ! Repair Location
            !-----------------------------------------------


            WriteColumn( job:Manufacturer                 ) ! Make
            WriteColumn( job:Model_Number                 ) ! Model
            WriteColumn( job:Unit_Type                    ) ! Unit Type

            WriteColumn( saQ:AccountNumber                ) ! Account
            WriteColumn( saQ:AccountName                  ) ! Name

            WriteColumn( job:ESN                          ) ! IMEI
            WriteColumn( job:MSN                          ) ! MSN

            WriteColumn( job:Current_Status               ) ! Current  Status
            WriteColumn( job:Exchange_Status              ) ! Exchange Status
            WriteColumn( job:Loan_Status                  ) ! Loan     Status

            DO WriteColumn_Engineer
        IF excel:OperatingSystem < 5
            DO PassViaClipboard
        END !IF
        !-----------------------------------------------
        DO XL_ColFirst
        DO XL_RowDown
        !-----------------------------------------------
    EXIT
                                                                                                                                                                                
!--------------------------------------------------------------
WriteColumn_Engineer                ROUTINE
    DATA
Temp STRING(100)
    CODE
        !-----------------------------------------------
        Temp = CLIP(AppendString(engq:Surname, engq:Forename, ', '))
        Temp = CLIP(AppendString(Temp,         engq:UserCode, '-' ))
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & Temp

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteDataSummary                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        ! summary details
        !                                                                             
        Excel{'ActiveWorkBook.Sheets("' & CLIP(LOC:SectionName) & '").Select'}

        DO FormatColumns

        Excel{'Range("F' & sheet:DataSectionRow & '").Select'}
            DO XL_HorizontalAlignmentLeft
            Excel{'ActiveCell.Formula'}         = RecordCount

        Excel{'Range("G' & sheet:DataSectionRow & '").Select'}
            DO XL_HorizontalAlignmentRight
            Excel{'ActiveCell.Formula'}         = 'Showing'

        Excel{'Range("H' & sheet:DataSectionRow & '").Select'}
            DO XL_HorizontalAlignmentLeft
            Excel{'ActiveCell.Formula'}         = '=SUBTOTAL(2, A' & sheet:DataHeaderRow+1 & ':A' & sheet:DataHeaderRow+RecordCount & ')'

        Excel{'Range("A' & sheet:DataHeaderRow  & '").Select'}
            DO XL_DataAutoFilter
        !-----------------------------------------------------------------      
    EXIT
WriteHeadSummary                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        ! summary sheet details
        !
        Excel{'ActiveWorkBook.Sheets("Summary").Select'}

        Excel{'Range("A' & sheet:HeadSummaryRow & '").Select'}
        !-----------------------------------------------------------------
        DO WriteHeadSummary_Summary
        DO WriteHeadSummary_TradeAccount
        DO WriteHeadSummary_Engineer
        !-----------------------------------------------------------------
    EXIT
WriteHeadSummary_Summary            ROUTINE
    DATA
QueueIndex   LONG
ResultsCount LONG

HeaderRow    LONG
FirstRow     LONG
LastRow      LONG
SubTotalRow  LONG
    CODE
        !-----------------------------------------------------------------
        ! Totals
        !
        ResultsCount = RECORDS(SummaryQueue)
        HeaderRow    = Excel{'ActiveCell.Row'}  ! first blank line
        FirstRow     = HeaderRow + 1
        LastRow      = HeaderRow + ResultsCount
        SubTotalRow  = LastRow   + 1

        Excel{'Range("A' & HeaderRow & ':' & sheet:HeadLastCol & HeaderRow & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & HeaderRow & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'}      = 'Status'
            Excel{'ActiveCell.Offset(0, 1).Formula'}      = 'Count'

        Excel{'Range("A' & FirstRow & '").Select'}

        IF ResultsCount < 1
            Excel{'ActiveCell.Formula'}      = 'No Jobs Found'
            Excel{'Range("A' & FirstRow+2 & '").Select'}

            EXIT
        END !IF
        !-----------------------------------------------------------------
        SORT(SummaryQueue, +sq:Status)
        LOOP QueueIndex = 1 TO ResultsCount
            !-------------------------------------------------------------
            GET(SummaryQueue, QueueIndex)

            Excel{'ActiveCell.Offset(0, 0).Formula'}      = sq:Status
            Excel{'ActiveCell.Offset(0, 1).Formula'}      = sq:Count

            DO XL_RowDown
            !-------------------------------------------------------------
        END !LOOP

        Excel{'Range("A' & SubTotalRow & ':' & sheet:HeadLastCol & SubTotalRow & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & SubTotalRow & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'}      = 'Sub-Totals'
            Excel{'ActiveCell.Offset(0, 1).Formula'}      = '=SUBTOTAL(9, B' & FirstRow & ':B' & LastRow & ')'
        !-----------------------------------------------------------------
        Excel{'Range("A' & SubTotalRow+2 & '").Select'}
        !-----------------------------------------------------------------
    EXIT
WriteHeadSummary_TradeAccount                    ROUTINE
    DATA
QueueIndex   LONG
ResultsCount LONG

HeaderRow    LONG
FirstRow     LONG
LastRow      LONG
SubTotalRow  LONG
    CODE
        !-----------------------------------------------------------------
        ! Summary Trade Account
        !
        !-----------------------------------------------------------------
        ! MICHALAN: Insert a new column to detail the originating franchise ie.
        ! If these jobs were booked in at ARC then franchise number would be 01
        !
        ! Not Done, All RRC Jobs are booked at RRC First
        !-----------------------------------------------------------------
        ResultsCount = RECORDS(SubAccount_Queue)

        HeaderRow   = Excel{'ActiveCell.Row'}  ! first blank line
        FirstRow    = HeaderRow + 1
        LastRow     = HeaderRow + ResultsCount
        SubTotalRow = LastRow   + 1

        Excel{'Range("A' & HeaderRow & ':' & sheet:HeadLastCol & HeaderRow & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & HeaderRow & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'}          = 'Account'
            Excel{'ActiveCell.Offset(0, 1).Formula'}          = 'Name'
            Excel{'ActiveCell.Offset(0, 2).Formula'}          = 'Jobs'
            !-------------------------------------------------------------
!            ! 12 Nov 2002 John Job 2104 "berrjo: Range Left"
!            Excel{'ActiveCell.Offset(0, 2).HorizontalAlignment'} = xlRight
            !-------------------------------------------------------------

        Excel{'Range("A' & FirstRow & '").Select'}

        IF ResultsCount < 1
            Excel{'ActiveCell.Formula'}                       = 'No Sub Accounts Found'
            Excel{'Range("A' & FirstRow+2 & '").Select'}

            EXIT
        END !IF
        !-----------------------------------------------------------------
        SORT(SubAccount_Queue, +saQ:AccountNumber)
        LOOP QueueIndex = 1 TO ResultsCount
            !-------------------------------------------------------------
            GET(SubAccount_Queue, QueueIndex)

            Excel{'ActiveCell.Offset(0, 0).Formula'}          = saQ:AccountNumber
            Excel{'ActiveCell.Offset(0, 1).Formula'}          = saQ:AccountName
            Excel{'ActiveCell.Offset(0, 2).Formula'}          = saQ:Count
                Excel{'ActiveCell.Offset(0, 2).NumberFormat'} = '###0'

            DO XL_RowDown
            !-------------------------------------------------------------
        END !LOOP

        Excel{'Range("A' & SubTotalRow & ':' & sheet:HeadLastCol & SubTotalRow & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & SubTotalRow & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'}          = 'Sub-Totals'
            Excel{'ActiveCell.Offset(0, 2).Formula'}          = '=SUBTOTAL(9, C' & FirstRow & ':C' & LastRow & ')'
        !-----------------------------------------------------------------
        Excel{'Range("A' & SubTotalRow+2 & '").Select'}
        !-----------------------------------------------------------------
    EXIT
WriteHeadSummary_Engineer                        ROUTINE
    DATA
QueueIndex   LONG
ResultsCount LONG

HeaderRow    LONG
FirstRow     LONG
LastRow      LONG
SubTotalRow  LONG
    CODE
        !-----------------------------------------------------------------
        ! Totals Engineer
        !
        ResultsCount = RECORDS(EngineerQueue)

        HeaderRow   = Excel{'ActiveCell.Row'}  ! first blank line
        FirstRow    = HeaderRow + 1
        LastRow     = HeaderRow + ResultsCount
        SubTotalRow = LastRow   + 1

        Excel{'Range("A' & HeaderRow & ':' & sheet:HeadLastCol & HeaderRow & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & HeaderRow & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'}          = 'Engineer'
            Excel{'ActiveCell.Offset(0, 1).Formula'}          = 'Name'
            Excel{'ActiveCell.Offset(0, 2).Formula'}          = 'Jobs'
        !-----------------------------------------------------------------
        Excel{'Range("A' & FirstRow & '").Select'}

        IF ResultsCount < 1
            Excel{'ActiveCell.Formula'}          = 'No Engineers Found'
            Excel{'Range("A' & FirstRow+1 & '").Select'}

            EXIT
        END !IF
        !-----------------------------------------------------------------
        SORT(EngineerQueue, +engq:UserCode)
        LOOP QueueIndex = 1 TO ResultsCount
            !-------------------------------------------------------------
            GET(EngineerQueue, QueueIndex)

            Excel{'ActiveCell.Offset(0, 0).Formula'}          = engq:UserCode
            Excel{'ActiveCell.Offset(0, 1).Formula'}          = AppendString(engq:Surname, engq:Forename, ', ')
            Excel{'ActiveCell.Offset(0, 2).Formula'}          = engq:JobsBooked
                Excel{'ActiveCell.Offset(0, 2).NumberFormat'} = '###0'

            DO XL_RowDown
            !-------------------------------------------------------------
        END !LOOP

        Excel{'Range("A' & SubTotalRow & ':' & sheet:HeadLastCol & SubTotalRow & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & SubTotalRow & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'}          = 'Sub-Totals'
            Excel{'ActiveCell.Offset(0, 2).Formula'}          = '=SUBTOTAL(9, C' & FirstRow & ':C' & LastRow & ')'
        !-----------------------------------------------------------------
        Excel{'Range("A' & SubTotalRow+2 & '").Select'}
        !-----------------------------------------------------------------
    EXIT
!--------------------------------------------------------------
GetFileName                             ROUTINE ! Generate default file name
    DATA
local:Desktop         CString(255)
DeskTopExists   BYTE
ApplicationPath STRING(255)
excel:ProgramName       Cstring(255)
    CODE
    excel:ProgramName = 'Turnaround Time Progress Report'
    SHGetSpecialFolderPath( GetDesktopWindow(), local:Desktop, 5, FALSE )
    local:Desktop = Clip(local:Desktop) & '\ServiceBase Export'

    !Does the Export Folder already Exists?
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    local:Desktop = Clip(local:Desktop) & '\' & Clip(excel:ProgramName)
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    loc:Path = local:Desktop
    loc:FileName = Clip(local:Desktop) & '\' & CLIP(excel:PRogramName) & ' ' & Format(Today(),@d12)
!
!
!GetFileName                             ROUTINE ! Generate default file name
!    DATA
!Desktop         CString(255)
!DeskTopExists   BYTE
!ApplicationPath STRING(255)
!    CODE
!        !-----------------------------------------------
!        ! Generate default file name
!        !
!        !-----------------------------------------------
!        ! 4 Dec 2001 John Stop using def:Export_Path as previously directed
!        ! 4 Dec 2001 John Use generated date to create file name not parameters LOC:StartDate,LOC:EndDate
!        !
!        IF CLIP(LOC:FileName) <> ''
!            ! Already generated 
!            EXIT
!        END !IF
!
!        DeskTopExists = False
!
!        SHGetSpecialFolderPath( GetDesktopWindow(), Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
!        LOC:DesktopPath = Desktop
!
!        ApplicationPath = Desktop & '\' & LOC:ApplicationName & ' Export'
!        IF EXISTS(CLIP(ApplicationPath))
!            LOC:Path      = CLIP(ApplicationPath) & '\'
!            DeskTopExists = True
!
!        ELSE
!            Desktop  = CLIP(ApplicationPath)
!
!            IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!!                MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!!            ELSE
!!                MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
!            END !IF
!
!            IF EXISTS(CLIP(ApplicationPath))
!                LOC:Path      = CLIP(ApplicationPath) & '\'
!                !DeskTopExists = True
!            END !IF
!
!        END !IF
!
!        IF DeskTopExists
!            ApplicationPath = CLIP(LOC:Path) & CLIP(LOC:ProgramName)
!            Desktop         = CLIP(ApplicationPath)
!
!            IF NOT EXISTS(CLIP(ApplicationPath))
!                IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!!                    MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!!                ELSE
!!                    MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
!                END !IF
!            END !IF
!
!            IF EXISTS(CLIP(ApplicationPath))
!                LOC:Path = CLIP(ApplicationPath) & '\'
!            END !IF
!        END !IF
!
!        !-----------------------------------------------
!        ! 23 Oct 2002 John
!        ! R003, berrjo :Insert the report exe name EG VODR56.V01
!        ! Do not attach the EXE extenstion but show the version number.  Increment the version number each time changed.
!        ! Make this standard on all reports sent for correction.
!        ! LOC:Version STRING(8) 3.1.0000 3.1 to match sevicebase version (and dictionary) .0000 increment before each compile
!        !
!        !LOC:FileName = SHORTPATH(CLIP(LOC:Path)) & CLIP(tra_ali:Account_Number) & ' VODR0027 ' & FORMAT(TODAY(), @D12) & '.xls'
!        LOC:FileName = SHORTPATH(CLIP(LOC:Path)) & CLIP(tra_ali:Account_Number) & ' VODR0027 ' & FORMAT(TODAY(), @D12) & '.xls'
!        !-----------------------------------------------
!        
!        !-----------------------------------------------
!    EXIT
GetUserName                            ROUTINE
    DATA
CommandLine STRING(255)
tmpPos      LONG
    CODE
        !-----------------------------------------------
        CommandLine = CLIP(COMMAND(''))

!        LOC:UserName = LOC:ApplicationName
!
!        Case MessageEx('Fail User Check For "' & CLIP(CommandLine) & '"?', LOC:ApplicationName & ' DEBUG',|
!                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
!            Of 1 ! &Yes Button
!                POST(Event:CloseWindow)
!
!                EXIT
!            Of 2 ! &No Button
!                EXIT
!        End!Case MessageEx
        !-----------------------------------------------
        tmpPos = INSTRING('%', CommandLine)
        IF NOT tmpPos
            Case Missive('You must run this report from ServiceBase''s "Custom Reports".','ServiceBase 3g',|
                           'mstop.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive

            POST(Event:CloseWindow)

           EXIT
        END !IF tmpPos
        !-----------------------------------------------
        SET(USERS)
        Access:USERS.Clearkey(use:Password_Key)
        glo:Password = CLIP(SUB(CommandLine, tmpPos + 1, 30))
      
        Access:USERS.Clearkey(use:Password_Key)
        use:Password = glo:Password
        If Access:USERS.Tryfetch(use:Password_Key)
            Case Missive('Error! Cannot find the user''s details.','ServiceBase 3g',|
                           'mstop.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive

            POST(Event:CloseWindow)

           EXIT
        End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        !-----------------------------------------------
        LOC:UserName = use:Forename

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = use:Surname
        ELSE
            LOC:UserName = CLIP(use:Forename) & ' ' & use:Surname 
        END !IF

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = '<' & use:User_Code & '>'
        END !IF
        !-----------------------------------------------
    EXIT
LoadFileDialog                          ROUTINE ! Ask user if file name is empty
    DATA
OriginalPath STRING(255)
    CODE
        !-----------------------------------------------
        IF CLIP(LOC:Filename) = ''
            DO GetFileName 
        END !IF

!        OriginalPath = PATH()
!        SETPATH(LOC:Path) ! Required for Win95/98
!            IF NOT FILEDIALOG('Save Spreadsheet', LOC:Filename, 'Microsoft Excel Workbook|*.XLS', |
!                FILE:KeepDir + FILE:Save + FILE:NoError + FILE:LongName)
!
!                LOC:Filename = ''
!            END !IF
!        SETPATH(OriginalPath)

        UPDATE()
        DISPLAY()
        !-----------------------------------------------
    EXIT
LoadSUBTRACC                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !
        Result = False

        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = job:Account_Number

        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key)
            EXIT
        END !IF

        IF sub:Account_Number <> job:Account_Number
            EXIT
        END !IF

        Result = True
        !-----------------------------------------------
    EXIT
SaveAllRRCSheets            ROUTINE
    DATA
Sheets    LONG
SheetName STRING(30)
FileName  STRING(255)
WorkBook  STRING(255)
    CODE
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        Sheets = Excel{'Sheets.Count'}
        WriteDebug('SaveAllRRCSheets(' & Sheets & ')')

        IF Sheets < 1
            WriteDebug('SaveAllRRCSheets(FAIL=IF Sheets < 1)')
            EXIT
        END !IF

        WorkBook = Excel{'ActiveWorkbook.Name'}
        WriteDebug('SaveAllRRCSheets(WorkBook="' & CLIP(WorkBook) & '")')

        LOOP x# = 0 TO Sheets
            IF (x# = Sheets)
                BREAK
            END !IF

            Excel{'Sheets(' & x# & ').Select'}
            SheetName = Excel{'ActiveSheet.Name'}
                !-----------------------------------------------
                ! 23 Oct 2002 John
                ! R003, berrjo :Insert the report exe name EG VODR56.V01
                ! Do not attach the EXE extenstion but show the version number.  Increment the version number each time changed.
                ! Make this standard on all reports sent for correction.
                ! LOC:Version STRING(8) 3.1.0000 3.1 to match sevicebase version (and dictionary) .0000 increment before each compile
                !
                !FileName  = SHORTPATH(CLIP(LOC:Path)) & CLIP(SheetName) & ' ' & FORMAT(TODAY(), @D12) & '.xls'
            FileName  = SHORTPATH(CLIP(LOC:Path)) & CLIP(SheetName) & ' VODR0027 ' & FORMAT(TODAY(), @D12) & '.xls'
                !-----------------------------------------------
            
            WriteDebug('SaveAllRRCSheets(SheetName="' & CLIP(SheetName) & '", FileName="' & CLIP(FileName) & '")')

            CASE CLIP(SheetName)
            OF 'Summary'
                ! NULL
            OF 'Sheet3'
                ! NULL
            ELSE
                Excel{'ActiveSheet.Copy'}
                WriteDebug('SaveAllRRCSheets(COPY)')

                Excel{'ActiveWorkbook.SaveAs("' & CLIP(FileName) & '")'}
                WriteDebug('SaveAllRRCSheets(SaveAs)')

                Excel{'ActiveWindow.Close'}
                WriteDebug('SaveAllRRCSheets(Close)')
            END ! CASE
        END !LOOP
        !-----------------------------------------------------------------
    EXIT
PassViaClipboard                                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF clip:Saved = False
            clip:OriginalValue = CLIPBOARD()
            clip:Saved         = True
        END !IF
        !-----------------------------------------------
        SETCLIPBOARD(CLIP(clip:Value))
            Excel{'ActiveSheet.Paste()'}
        clip:Value = ''
        !-----------------------------------------------
    EXIT
ResetClipboard                                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF clip:Saved = False
            EXIT
        END !IF

        SETCLIPBOARD(clip:OriginalValue)
        
        clip:Saved = False
        !-----------------------------------------------
    EXIT
!--------------------------------------------------------------
ProgressBar_Setup                       ROUTINE
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
            RecordsPerCycle      = 25
            RecordsProcessed     = 0
            RecordsToProcess     = 10 !***The Number Of Records, or at least a guess***
            PercentProgress      = 0
            Progress:thermometer = 0

            thiswindow.reset(1) !This may error, if so, remove this line.
            open(progresswindow)
            progresswindow{PROP:Timer} = 1
         
            ?progress:userstring{prop:text} = 'Running...'
            ?progress:pcttext{prop:text}    = '0% Completed'
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***

ProgressBar_LoopPre          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***

        RecordsProcessed = 0
        RecordCount      = 0

        ?Progress:UserString{PROP:Text} = CLIP(Progress:Text)

        !RecordsToProcess = RECORDS(JOBS) !***The Number Of Records, or at least a guess***
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
        !-----------------------------------------------
    EXIT

ProgressBar_Loop             ROUTINE
    DATA
newPctText STRING(255)
    CODE
        !-----------------------------------------------
        !***Place the following code inside the loop. The first line updates the progress bar, and the rest allows the user to cancel the routine. This can be removed if necessary. ****

        Do ProgressBar_GetNextRecord2 !Necessary

        Do ProgressBar_CancelCheck
        If CancelPressed
            CLOSE(ProgressWindow)
        End!If 
        !-----------------------------------------------
        newPctText = 'Checked(' & RecordsProcessed & '), Found(' & RecordCount & ')'

        IF ?progress:pcttext{prop:text} <> newPctText
            ?progress:pcttext{prop:text} = newPctText

            Display()
        END !IF

        !***Place the following code inside the loop. The first line updates the progress bar, and the rest allows the user to cancel the routine. This can be removed if necessary. ****
        !-----------------------------------------------
    EXIT

ProgressBar_LoopPost         ROUTINE
    DATA
newPctText STRING(255)
    CODE
        !-----------------------------------------------
        ?Progress:UserString{PROP:Text} = CLIP(Progress:Text) & ' Done(' & RecordsProcessed & '/' & RecordCount & ')'

        newPctText = 'Checked(' & RecordsProcessed & '), Found(' & RecordCount & ')'

        IF ?progress:pcttext{prop:text} <> newPctText
            ?progress:pcttext{prop:text} = newPctText
        END !IF
        !-----------------------------------------------
    EXIT

ProgressBar_Finalise                        ROUTINE
        !**** End Of Loop ***
            Do ProgressBar_EndPrintRun
            close(progresswindow)
        !**** End Of Loop ***
        ! when in report procedure->LocalResponse = RequestCompleted

ProgressBar_GetNextRecord2                  routine
    recordsprocessed += 1
    recordsthiscycle += 1

    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        !?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end

ProgressBar_CancelCheck                     routine
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept

    If cancel# = 1
        BEEP(BEEP:SystemAsterisk)  ;  YIELD()
        Case Missive('Are you sure you want to cancel?','ServiceBase 3g',|
                       'mstop.jpg','\No|/Yes') 
            Of 2 ! Yes Button
                CancelPressed = True
            Of 1 ! No Button
        End ! Case Missive
    End!If cancel# = 1

ProgressBar_EndPrintRun                     routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()

RefreshWindow                          ROUTINE
    !|
    !| This routine is used to keep all displays and control templates current.
    !|
    !IF window{Prop:AcceptAll} THEN EXIT.
    IF MainWindow{Prop:AcceptAll} THEN EXIT.
    
    DISPLAY()
    !ForceRefresh = False
XL_Setup                                                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel = Create(0, Create:OLE)
        Excel{PROP:Create} = 'Excel.Application'

        Excel{'Application.DisplayAlerts'}  = False         ! no alerts to the user (do you want to save the file?) ! MOVED 10 BDec 2001 John
        Excel{'Application.ScreenUpdating'} = excel:Visible ! False
        Excel{'Application.Visible'}        = excel:Visible ! False

        Excel{'Application.Calculation'}    = xlCalculationManual
        
        DO XL_GetOperatingSystem
        !-----------------------------------------------
    EXIT
XL_Finalize                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Application.Calculation'}    = xlCalculationAutomatic

        Excel{PROP:DEACTIVATE}
        !-----------------------------------------------
    EXIT
XL_AddSheet                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveWorkBook.Sheets("Sheet3").Select'}

        Excel{'ActiveWorkBook.Sheets.Add'}
        !-----------------------------------------------
    EXIT
XL_AddWorkbook               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Application.Workbooks.Add()'}

        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Title")'}            = CLIP(LOC:ProgramName) & ' ' & CLIP(tra_ali:Account_Number)
        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Author")'}           = CLIP(LOC:UserName)
        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Application Name")'} = LOC:ApplicationName
        !-----------------------------------------------
        ! 4 Dec 2001 John
        ! Delete empty sheets

        Excel{'Sheets("Sheet2").Select'}
        Excel{'ActiveWindow.SelectedSheets.Delete'}

        Excel{'Sheets("Sheet1").Select'}
        !-----------------------------------------------
    EXIT
XL_SaveAllWorksheets                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOOP WHILE Excel{'WorkBooks.Count'} > 0
            Excel{'ActiveWorkBook.Save()'}
            Excel{'ActiveWorkBook.Close()'}
        END !LOOP
        !-----------------------------------------------
    EXIT
XL_DropAllWorksheets                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOOP WHILE Excel{'WorkBooks.Count'} > 0
            Excel{'ActiveWorkBook.Close(' & FALSE & ')'}
        END !LOOP
        !-----------------------------------------------
    EXIT
XL_AddComment                ROUTINE
    DATA
xlComment CSTRING(20)
    CODE
        !-----------------------------------------------
        Excel{'Selection.AddComment("' & CLIP(excel:CommentText) & '")'}

!        xlComment{'Shape.IncrementLeft'} = 127.50
!        xlComment{'Shape.IncrementTop'}  =   8.25
!        xlComment{'Shape.ScaleWidth'}    =   1.76
!        xlComment{'Shape.ScaleHeight'}   =   0.69
        !-----------------------------------------------
    EXIT
XL_RowDown                   ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(1, 0).Select'}
        !-----------------------------------------------
    EXIT
XL_ColLeft                   ROUTINE
    DATA
CurrentCol LONG
    CODE
        !-----------------------------------------------
        CurrentCol = Excel{'ActiveCell.Col'}
        IF CurrentCol = 1
            EXIT
        END !IF

        Excel{'ActiveCell.Offset(0, -1).Select'}
        !-----------------------------------------------
    EXIT
XL_ColRight                  ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(0, 1).Select'}
        !-----------------------------------------------
    EXIT
XL_ColFirstSelect                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, Cells(' & Excel{'ActiveCell.Row'} & ', 1)).Select'}
        !Excel{'ActiveCell.Offset(0, -' & Excel{'ActiveCell.Column'} - 1 & ').Select'}
        !-----------------------------------------------
    EXIT
XL_ColFirst                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(0, -' & Excel{'ActiveCell.Column'} - 1 & ').Select'}
        !-----------------------------------------------
    EXIT
XL_SetLastCell                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, ActiveCell.SpecialCells(' & xlLastCell & ')).Select'}
        !-----------------------------------------------
    EXIT
XL_SetGrid                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlDiagonalDown & ').LineStyle'} = xlNone
        Excel{'Selection.Borders(' & xlDiagonalUp   & ').LineStyle'} = xlNone

        DO XL_SetBorder

        Excel{'Selection.Borders(' & xlInsideVertical & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlInsideVertical & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlInsideVertical & ').ColorIndex'} = xlAutomatic

        Excel{'Selection.Borders(' & xlInsideHorizontal & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlInsideHorizontal & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlInsideHorizontal & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorder                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_SetBorderLeft
        DO XL_SetBorderRight
        DO XL_SetBorderBottom
        DO XL_SetBorderTop
        !-----------------------------------------------
    EXIT
XL_SetBorderLeft                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeLeft & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeLeft & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeLeft & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderRight                           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeRight & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeRight & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeRight & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderBottom                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeBottom & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeBottom & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeBottom & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderTop                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeTop & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeTop & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeTop & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_HighLight                           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Interior.ColorIndex'}        = 6 ! YELLOW
        Excel{'Selection.Interior.Pattern'}           = xlSolid
        Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic
        !-----------------------------------------------
XL_SetTitle                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Interior.ColorIndex'}        = 15 ! GREY
        Excel{'Selection.Interior.Pattern'}           = xlSolid
        Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetWrapText                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.WrapText'}    = True
        !-----------------------------------------------
    EXIT
XL_SetGreyText                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Font.ColorIndex'}    = 16 ! grey
        !-----------------------------------------------
    EXIT
XL_SetBold                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Font.Bold'} = True
        !-----------------------------------------------
    EXIT
XL_FormatDate                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.NumberFormat'} = excel:DateFormat
        !-----------------------------------------------
    EXIT
XL_FormatNumber                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.NumberFormat'} = '#,##0.00'
        !-----------------------------------------------
    EXIT
XL_FormatTextBox                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.ShapeRange.IncrementLeft'} = 127.5
        Excel{'Selection.ShapeRange.IncrementTop'}  =   8.25
!        Excel{'Selection.ShapeRange.ScaleWidth'}    =  '1.76, ' & msoFalse & ', ' & msoScaleFromBottomRight
!        Excel{'Selection.ShapeRange.ScaleHeight'}   =  '0.69, ' & msoFalse & ', ' & msoScaleFromTopLeft
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentCentre            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlCenter
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentLeft              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlLeft
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentRight             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlRight
        !-----------------------------------------------
    EXIT
XL_VerticalAlignmentTop                 ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.VerticalAlignment'} = xlTop
        !-----------------------------------------------
    EXIT
XL_VerticalAlignmentCentre            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.VerticalAlignment'} = xlCenter
        !-----------------------------------------------
    EXIT
XL_SetColumnHeader                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_SetTitle
        DO XL_SetGrid
        DO XL_SetWrapText
        DO XL_SetBold
        !-----------------------------------------------
    EXIT
XL_SetColumn_Date                       ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = 'dd Mmm yyyy'

!        !Excel{'ActiveCell.FormulaR1C1'}  = LEFT(FORMAT(ret:Invoice_Date, @D8-))
        !-----------------------------------------------
    EXIT
XL_SetColumn_Number                     ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = '#,##0.00'
        !-----------------------------------------------
    EXIT
XL_SetColumn_Percent                    ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = '0.00%'
!        Excel{'ActiveCell.NumberFormat'} = CHR(64)       ! Text (AT) Sign
        !-----------------------------------------------
    EXIT
XL_SetColumn_Text                       ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'} = CHR(64) ! Text (AT) Sign
        !-----------------------------------------------
    EXIT
XL_DataSelectRange                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, ActiveCell.SpecialCells(' & xlLastCell & ')).Select'}
        !-----------------------------------------------
    EXIT
XL_DataHideDuplicates                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.AdvancedFilter Action:=' & xlFilterInPlace & ', Unique:=' & True }
        !-----------------------------------------------
    EXIT
XL_DataSortRange                            ROUTINE
    DATA
    CODE                     
        !-----------------------------------------------
        DO XL_DataSelectRange
        Excel{'ActiveWindow.ScrollColumn'} = 1
        Excel{'Selection.Sort Key1:=Range("' & LOC:Text & '"), Order1:=' & xlAscending & ', Header:=' & xlGuess & |
            ', OrderCustom:=1, MatchCase:=' & False & ', Orientation:=' & xlTopToBottom}
        !-----------------------------------------------
    EXIT
XL_DataAutoFilter                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.AutoFilter'}
        !-----------------------------------------------
    EXIT
XL_SetWorksheetLandscape                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.Orientation'}     = xlLandscape
!        Excel{'ActiveSheet.PageSetup.FitToPagesWide'} = 1
!        Excel{'ActiveSheet.PageSetup.FitToPagesTall'} = 9999
!        Excel{'ActiveSheet.PageSetup.Order'}          = xlOverThenDown
        !-----------------------------------------------
    EXIT
XL_SetWorksheetPortrait                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.Orientation'} = xlPortrait
        !-----------------------------------------------
    EXIT
XL_PrintTitleRows                               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.PrintTitleRows'} = CLIP(LOC:Text)
        !-----------------------------------------------
    EXIT
XL_GetCurrentCell                               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOC:Text = Excel{'Application.ConvertFormula( "RC", ' & xlR1C1 & ', ' & xlA1 & ')'}
        !-----------------------------------------------
    EXIT

XL_MergeCells                                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlCenter
        Excel{'Selection.VerticalAlignment'}   = xlBottom
        Excel{'Selection.MergeCells'}          = True
        !-----------------------------------------------
    EXIT

XL_GetOperatingSystem                                                          ROUTINE
    DATA
OperatingSystem     STRING(50)
tmpLen              LONG
LEN_OperatingSystem LONG
    CODE
        !-----------------------------------------------
        excel:OperatingSystem = 0.0    ! Default/error value
        OperatingSystem       = Excel{'Application.OperatingSystem'}
        LEN_OperatingSystem   = LEN(CLIP(OperatingSystem))

        LOOP x# = LEN_OperatingSystem TO 1 BY -1

            CASE SUB(OperatingSystem, x#, 1)
            OF '0' OROF'1' OROF '2' OROF '3' OROF '4' OROF '5' OROF '6' OROF '7' OROF '8' OROF '9' OROF '.'
                ! NULL
            ELSE
                tmpLen                = LEN_OperatingSystem-x#+1
                excel:OperatingSystem = CLIP(SUB(OperatingSystem, x#+1, tmpLen))

                EXIT
            END !CASE
        END !LOOP
        !-----------------------------------------------
    EXIT
!--------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020603'&'0'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('TurnaroundTimeProgressReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:JOBS.Open
  Relate:STATUS.Open
  Relate:TRADEACC_ALIAS.Open
  Relate:WEBJOB.Open
  Access:SUBTRACC.UseFile
  Access:USERS.UseFile
  Access:LOCATION.UseFile
  Access:LOCATLOG.UseFile
  SELF.FilesOpened = True
  BRW5.Init(?List,Queue:Browse.ViewPosition,BRW5::View:Browse,Queue:Browse,Relate:TRADEACC,SELF)
  OPEN(MainWindow)
  SELF.Opened=True
  ! ========= Set Report Version =============
  Include('..\ReportVersion.inc')
  tmp:VersionNumber = 'Version Number: ' & Clip(tmp:VersionNumber) & '5001'
  ?ReportVersion{prop:Text} = tmp:VersionNumber
      LOC:ProgramName         = 'Turnaround Time Progress Report'  ! Job=1600    Cust=in house
      MainWindow{PROP:Text}   = CLIP(LOC:ProgramName)
      ?Tab1{PROP:Text}        = CLIP(LOC:ProgramName) & ' Criteria'
      
      excel:Visible      = False
  
      LOC:StartDate      = TODAY()
      LOC:EndDate        = TODAY()
  
      SET(defaults)
      access:defaults.next()
  
      DO GetUserName
  
      IF GUIMode = 1 THEN
         LocalTimeOut = 500
         MainWindow{PROP:ICONIZE} = TRUE
      END !IF
  
      LocalHeadAccount = GETINI('BOOKING','HEADACCOUNT','',CLIP(PATH()) & '\SB2KDEF.INI')
      DISPLAY
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?LOC:StartDate{Prop:Alrt,255} = MouseLeft2
  ?LOC:EndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW5.Q &= Queue:Browse
  BRW5.AddSortOrder(,tra:Account_Number_Key)
  BRW5.AddLocator(BRW5::Sort0:Locator)
  BRW5::Sort0:Locator.Init(,tra:Account_Number,1,BRW5)
  BRW5.SetFilter('((tra:RemoteRepairCentre = 1 OR tra:Account_Number = LocalHeadAccount) AND (tra:Account_Number <<> ''XXXRRC''))')
  BIND('LocalTag',LocalTag)
  BIND('LocalHeadAccount',LocalHeadAccount)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW5.AddField(LocalTag,BRW5.Q.LocalTag)
  BRW5.AddField(tra:Account_Number,BRW5.Q.tra:Account_Number)
  BRW5.AddField(tra:Company_Name,BRW5.Q.tra:Company_Name)
  BRW5.AddField(tra:RecordNumber,BRW5.Q.tra:RecordNumber)
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List{Prop:Alrt,239} = SpaceKey
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:DEFAULTS.Close
    Relate:JOBS.Close
    Relate:STATUS.Close
    Relate:TRADEACC_ALIAS.Close
    Relate:WEBJOB.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?DoAll
      IF DoAll = 'Y' THEN
         HIDE(?List)
         HIDE(?DASTAG)
         HIDE(?DASTAGALL)
         HIDE(?DASUNTAGALL)
      ELSE
          UNHIDE(?List)
          UNHIDE(?DASTAG)
          UNHIDE(?DASTAGALL)
          UNHIDE(?DASUNTAGALL)
      END !IF
    OF ?OkButton
          DO OKButton_Pressed
    OF ?CancelButton
       POST(Event:CloseWindow)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020603'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020603'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020603'&'0')
      ***
    OF ?PopCalendar:1
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          LOC:StartDate = TINCALENDARStyle1(LOC:StartDate)
          Display(?LOC:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          LOC:EndDate = TINCALENDARStyle1(LOC:EndDate)
          Display(?LOC:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?LOC:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:1)
      CYCLE
    END
  OF ?LOC:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?List
    CASE EVENT()
    OF EVENT:AlertKey
      IF RECORDS(BRW5) <> 0 AND DoAll <> 'Y' AND KeyCode() = MouseLeft2 THEN
         POST(Event:Accepted,?DASTAG)
      END !IF
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::6:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

AppendString        PROCEDURE(IN:Start, IN:Add, IN:Separator)!STRING
ReturnValue STRING(255)
    CODE
        !-----------------------------------------------------------------
        ! IN:Start,     Accumulating string
        ! IN:Add,       String to add to accumulating string
        ! IN:Separator, Separator if both non null
        !               ie ',' for comma delimited, or ' AND ' for SQL
        !               Use (literals) or (variables without excesive trailing white space).
        !-----------------------------------------------------------------
        IF CLIP(IN:Start) = ''
            ReturnValue = IN:Add
        ELSIF CLIP(IN:Add) = ''
            ReturnValue = IN:Start
        ELSE
            ReturnValue = CLIP(IN:Start) & IN:Separator & CLIP(IN:Add)
        END !IF

        RETURN ReturnValue
        !-----------------------------------------------------------------
        ! Example
        !SQL STRING(255)
        !   CODE
        !   IF ?CHOICE1
        !       SQL = '"CANCELLED" = "YES"'
        !   END !IF
        !
        !   IF ?Date1 > 0
        !       SQL = AppendString( SQL, '"DATE_BOOKED" = "' LEFT(FORMAT(job:Date_Booked, @D10)) '"', ' AND ')
        !   END !IF
        !
        !   IF ?CHOICE2
        !       SQL = AppendString( SQL, '"COMPLETE" = "YES"', ' AND ')
        !   END !IF
        !-----------------------------------------------------------------
DateToString        PROCEDURE( IN:Date )! STRING
    CODE
        !-----------------------------------------------------------------
        RETURN LEFT(FORMAT(IN:Date, @D8))
        !-----------------------------------------------------------------
EmptySummaryQueue PROCEDURE()
    CODE
        !-----------------------------------------------------------------
        ! 12 Nov 2002 John Job 2104
        ! MICHALAN: Not all Statuses are included here eg. Awaiting Allocation, Allocated to Engineer
        !-----------------------------------------------------------------
        LOOP x# = 1 TO RECORDS(SummaryQueue)
            GET(SummaryQueue, x#)
                sq:Count        = 0
            ADD(SummaryQueue, +sq:Status, +sq:Type)

        END !LOOP
        !-----------------------------------------------------------------
FillSummaryQueue PROCEDURE()
    CODE
        WriteDebug('FillSummaryQueue()')
        !-----------------------------------------------------------------
        ! 12 Nov 2002 John Job 2104
        ! MICHALAN: Not all Statuses are included here eg. Awaiting Allocation, Allocated to Engineer
        !-----------------------------------------------------------------
        Access:STATUS.ClearKey(sts:Status_Key)
        SET(sts:Status_Key, sts:Status_Key)

        LOOP WHILE Access:STATUS.NEXT() = Level:Benign
            CLEAR(SummaryQueue)
                sq:Status       = sts:Status
                sq:Type         = 'JOB'
                sq:Count        = 0
            ADD(SummaryQueue)

        END !LOOP

        SORT(SummaryQueue, +sq:Status, +sq:Type)
        !-----------------------------------------------------------------
GetARCBranchIdentification      PROCEDURE()
    CODE
        !-----------------------------------------------------------------
        WriteDebug('GetARCBranchIdentification("' & CLIP(LocalHeadAccount) & '")')

        LOC:ARCBranchIdentification = '**'

        Access:TRADEACC_ALIAS.CLEARKEY(tra_ali:Account_Number_Key)
            tra_ali:Account_Number = LocalHeadAccount
        SET(tra_ali:Account_Number_Key, tra_ali:Account_Number_Key)

        IF NOT Access:TRADEACC_ALIAS.NEXT() = Level:Benign
            WriteDebug('GetARCBranchIdentification(EOF 00)')

            RETURN
        END !IF

        IF NOT tra_ali:Account_Number = LocalHeadAccount
            WriteDebug('GetARCBranchIdentification(EOI 00)')

            RETURN
        END !IF

        LOC:ARCBranchIdentification = tra_ali:BranchIdentification

        WriteDebug('GetARCBranchIdentification(' & LOC:ARCBranchIdentification & ')')
        !-----------------------------------------------------------------
GetDeliveryDateAtARC        PROCEDURE( IN:JobNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------------------------
        SentToARC# = False
        LOC:ARCDateBooked = ''

        Access:LOCATLOG.Clearkey(lot:DateKey)
            lot:RefNumber = in:JOBNumber
        Set(lot:DateKey,lot:DateKey)

        Loop WHILE Access:LOCATLOG.Next() = Level:Benign
            If lot:RefNumber <> in:JobNumber 
                Break
            End !

            If lot:NewLocation = loc:ARCLocation
                SentToARC# = True
                loc:ARCDateBooked = lot:TheDate
                !MESSAGE('TRUE CONDITION MET')
                Break
            End! IF
          
        End !Loop

        Return SentToARC#
        !-----------------------------------------------------------------


!GetDeliveryDateAtARC        PROCEDURE( IN:JobNumber )! LONG ! BOOL
!    CODE
!        !-----------------------------------------------------------------
!        IF IsRepairAtARC(IN:JobNumber)
!            LOC:ARCDateBooked = lot:TheDate
!
!            RETURN True
!        ELSE
!            LOC:ARCDateBooked = ''
!
!            RETURN False
!        END !IF
!        !-----------------------------------------------------------------
!

!GetDeliveryDateAtARC        PROCEDURE( IN:JobNumber )! LONG ! BOOL
!First LONG(True)
!    CODE
!        !-----------------------------------------------------------------
!        WriteDebug('GetDeliveryDateAtARC(' & IN:JobNumber & ')')
!
!        LOC:ARCDateBooked = ''
!        First = False
!    SentToARC# = False
!
!        LOOP WHILE LoadLOCATLOG_NewLocationKey( IN:JobNumber, LOC:ARCLocation, First)
!            !-------------------------------------------------------------
!            !First = False
!            IF CLIP(lot:NewLocation) = CLIP(LOC:ARCLocation)
!                WriteDebug('GetDeliveryDateAtARC(OK)"' & CLIP(lot:NewLocation) & '"')
!                LOC:ARCDateBooked = lot:TheDate
!                SentToARC# = True
!
!                RETURN True
!            END !IF
!            !-------------------------------------------------------------
!        END !LOOP
!
!        WriteDebug('GetDeliveryDateAtARC(FAIL)')
!        RETURN False
!        !-----------------------------------------------------------------



!GetDeliveryDateAtARC        PROCEDURE( IN:JobNumber )! LONG ! BOOL
!First LONG(True)
!    CODE
!        !-----------------------------------------------------------------
!        WriteDebug('GetDeliveryDateAtARC(' & IN:JobNumber & ')')
!
!        LOC:ARCDateBooked = ''
!        First = False
!
!        LOOP WHILE LoadLOCATLOG( IN:JobNumber, First)
!            !-------------------------------------------------------------
!            !First = False
!            IF CLIP(lot:NewLocation) = CLIP(LOC:ARCLocation)
!                WriteDebug('GetDeliveryDateAtARC(OK)"' & CLIP(lot:NewLocation) & '"')
!                LOC:ARCDateBooked = lot:TheDate
!
!                RETURN True
!            END !IF
!            !-------------------------------------------------------------
!        END !LOOP
!
!        WriteDebug('GetDeliveryDateAtARC(FAIL)')
!        RETURN False
!        !-----------------------------------------------------------------
GetHeadAccount PROCEDURE( IN:AccountNumber )
DoAdd LONG(False)
    CODE
        !-----------------------------------------------------------------  
        WriteDebug('GetHeadAccount("' & CLIP(IN:AccountNumber) & '")')

        haq:AccountNumber  = IN:AccountNumber
        GET(HeadAccount_Queue, +haq:AccountNumber)

        CASE ERRORCODE()
        OF 00 ! FOUND
            IF haq:AccountNumber  = IN:AccountNumber
                ! Found
                RETURN
            ELSE
                ! Partial Match - ADD
                DoAdd = True
            END !IF

        OF 30 ! NOT Found
            ! Not in queue - ADD
            DoAdd = True
        ELSE
            WriteDebug('GetHeadAccount(Error=' & ERRORCODE() & ', "' & CLIP(ERROR()) & '")')

            CancelPressed = True

            RETURN
        END !IF
        !-----------------------------------------------------------------
        IF DoAdd = True
            CLEAR(HeadAccount_Queue)
                haq:AccountNumber              = IN:AccountNumber

                IF LoadTRADEACC(IN:AccountNumber)
                    haq:AccountName            = tra:Company_Name
                    haq:BranchIdentification   = tra:BranchIdentification
                    WriteDebug('GetHeadAccount(BranchIdentification="' & tra:BranchIdentification & '")')

                    IF tra:UseTimingsFrom = 'TRA'
                        haQ:IncludeSaturday        = tra:IncludeSaturday
                        haQ:IncludeSunday          = tra:IncludeSunday
                        haQ:StartWorkHours         = tra:StartWorkHours
                        haQ:EndWorkHours           = tra:EndWorkHours

                    ELSE
                        IF def:Include_Saturday = 'YES'
                            haQ:IncludeSaturday        = True
                        ELSE
                            haQ:IncludeSaturday        = False
                        END !IF

                        IF def:Include_Sunday = 'YES'
                            haQ:IncludeSunday        = True
                        ELSE
                            haQ:IncludeSunday        = False
                        END !IF

                        haQ:StartWorkHours         = def:Start_Work_Hours
                        haQ:EndWorkHours           = def:End_Work_Hours

                    END !IF

                    IF tra:Invoice_Sub_Accounts = 'YES'
                        haQ:InvoiceSubAccounts = True
                    ELSE
                        haQ:InvoiceSubAccounts = False
                    END !IF

                ELSE
                    WriteDebug('GetHeadAccount(LoadTRADEACC("' & CLIP(IN:AccountNumber) & '") FAIL)')

                    haq:AccountName            = '*T'
                    haq:BranchIdentification   = '*T'

                    IF def:Include_Saturday = 'YES'
                        haQ:IncludeSaturday    = True
                    ELSE
                        haQ:IncludeSaturday    = False
                    END !IF
                    
                    IF def:Include_Sunday = 'YES'
                        haQ:IncludeSunday      = True
                    ELSE
                        haQ:IncludeSunday      = False
                    END !IF

                    haQ:StartWorkHours         = def:Start_Work_Hours
                    haQ:EndWorkHours           = def:End_Work_Hours
                END !IF
            ADD(HeadAccount_Queue, +haq:AccountNumber)

            WriteDebug('GetHeadAccount(ADD  "' & CLIP(IN:AccountNumber) & '", "' & haQ:BranchIdentification & '", UseTimingsFrom="' & CLIP(tra:UseTimingsFrom) & '")')
        END !IF
        !-----------------------------------------------------------------
GetReturnDateToRRC        PROCEDURE( IN:JobNumber, IN:DelivedToARCDate )! DATE
First LONG(True)
    CODE
        !-----------------------------------------------------------------
        WriteDebug('GetReturnDateToRRC(' & IN:JobNumber & ')')

        LOC:RRCDateReturned = ''
        First = False
        Access:LocatLog.ClearKey(lot:DateKey)
            lot:RefNumber = IN:JobNumber
            lot:TheDate = IN:DelivedToARCDate
        SET(lot:DateKey,lot:DateKey)

        LOOP WHILE Access:LocatLog.NEXT() = Level:Benign
            !-------------------------------------------------------------
            IF    lot:RefNumber <> IN:JobNumber
                BREAK
            END

            IF lot:NewLocation <> LOC:RRCLocation !LOC:StatusReceivedAtRRC
              CYCLE
            END
            !MESSAGE('OK')
            IF (lot:TheDate > IN:DelivedToARCDate)
                WriteDebug('GetReturnDateToRRC(OK) lot:TheDate"' & DateToString(lot:TheDate) & '" > IN:DelivedToARCDate"' & DateToString(IN:DelivedToARCDate) & '"')
                LOC:RRCDateReturned = lot:TheDate

                BREAK
            END !IF
            !-------------------------------------------------------------
        END !LOOP

        RETURN LOC:RRCDateReturned
        !-----------------------------------------------------------------
GetSubAccount PROCEDURE( IN:AccountNumber )
DoAdd LONG(False)
    CODE
        !-----------------------------------------------------------------  
        WriteDebug('GetSubAccount("' & CLIP(IN:AccountNumber) & '")')

        saQ:AccountNumber  = IN:AccountNumber
        GET(SubAccount_Queue, +saq:AccountNumber)

        CASE ERRORCODE()
        OF 00 ! FOUND
            IF saQ:AccountNumber  = IN:AccountNumber
                ! Found
                RETURN
            ELSE
                ! Partial Match - ADD
                DoAdd = True
            END !IF

        OF 30 ! NOT Found
            ! Not in queue - ADD
            DoAdd = True

        ELSE
            CancelPressed = True

            WriteDebug('GetSubAccount(Error=' & ERRORCODE() & ', "' & ERROR() & '")')

            RETURN
        END !CASE
        !-----------------------------------------------------------------
        IF DoAdd = True
            CLEAR(SubAccount_Queue)
                saQ:AccountNumber            = IN:AccountNumber

                IF LoadSUBTRACC(IN:AccountNumber)
                    GetHeadAccount(sub:Main_Account_Number)

                    saQ:AccountName          = sub:Company_Name
                    saQ:HeadAccountNumber    = sub:Main_Account_Number
                    saQ:HeadAccountName      = haQ:AccountName
                    saQ:BranchIdentification = haQ:BranchIdentification

                    saQ:IncludeSaturday      = haQ:IncludeSaturday
                    saQ:IncludeSunday        = haQ:IncludeSunday
                    saQ:StartWorkHours       = haQ:StartWorkHours
                    saQ:EndWorkHours         = haQ:EndWorkHours

                ELSE
                    saq:AccountName          = ''
                    saq:HeadAccountNumber    = ''
                    saQ:HeadAccountName      = ''
                    saQ:BranchIdentification = ''

                    IF def:Include_Saturday = 'YES'
                        saQ:IncludeSaturday  = True
                    ELSE
                        saQ:IncludeSaturday  = False
                    END !IF
                    
                    IF def:Include_Sunday = 'YES'
                        saQ:IncludeSunday    = True
                    ELSE
                        saQ:IncludeSunday    = False
                    END !IF

                    saQ:StartWorkHours       = def:Start_Work_Hours
                    saQ:EndWorkHours         = def:End_Work_Hours

                END !IF
            ADD(SubAccount_Queue, +saq:AccountNumber)

            WriteDebug('GetSubAccount(ADD  "' & CLIP(IN:AccountNumber) & '", "' & saQ:BranchIdentification & '")')
        END !IF
        !-----------------------------------------------------------------
HoursBetween    PROCEDURE  (StartTime, EndTime)!LONG
Hours LONG
    CODE
        IF StartTime = EndTime
            RETURN 0
        ELSIF StartTime > EndTime
            RETURN HoursBetween(EndTime, StartTime)
        END !IF

        Hours = (EndTime - StartTime) / (60 * 60 * 100)  ! 100 100ths = 1sec, 60x1sec = 1min, 60x1min=1 hour

        RETURN Hours
IsRepairAtARC        PROCEDURE( IN:JobNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------------------------
        !DateKey                  KEY(lot:RefNumber,lot:TheDate),DUP,NOCASE
        !NewLocationKey           KEY(lot:RefNumber,lot:NewLocation),DUP,NOCASE
        !RecordNumberKey          KEY(lot:RecordNumber),NOCASE,PRIMARY

        Access:LOCATLOG.Clearkey(lot:NewLocationKey)
            lot:RefNumber   = IN:JobNumber
            lot:NewLocation = LOC:ARCLocation
        !SET(lot:NewLocationKey, lot:NewLocationKey)

        IF Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign        !NEXT() = Level:Benign
            WriteDebug('IsRepairAtARC(' & IN:JobNumber & ') OK')

            RETURN True
        ELSE
            WriteDebug('IsRepairAtARC(' & IN:JobNumber & ') FAIL')

            RETURN False
        END !IF

!        IF NOT lot:RefNumber   = IN:JobNumber
!            WriteDebug('IsRepairAtARC(' & IN:JobNumber & ') FAIL EOI 1')
!
!            RETURN False
!        END !IF
!
!        IF NOT lot:NewLocation = LOC:ARCLocation
!            WriteDebug('IsRepairAtARC(' & IN:JobNumber & ') FAIL EOI 2')
!
!            RETURN False
!        END !IF
!
!        WriteDebug('IsRepairAtARC(' & IN:JobNumber & ') OK')
!
!        RETURN True
        !-----------------------------------------------------------------
LoadEngineer    PROCEDURE(IN:UserCode)!,BYTE BOOL
    CODE
        !-----------------------------------------------------------------
        Access:USERS.Clearkey(use:User_Code_Key)
        use:User_Code = IN:UserCode
        If Access:USERS.Tryfetch(use:User_Code_Key)
            RETURN False
        End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        !-----------------------------------------------------------------
        RETURN True
        !-----------------------------------------------------------------
LoadSUBTRACC   PROCEDURE( IN:AccountNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = IN:AccountNumber

        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key) <> Level:Benign
            RETURN False
        END !IF

        IF sub:Account_Number <> IN:AccountNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadTRADEACC   PROCEDURE( IN:AccountNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = IN:AccountNumber

        IF Access:TRADEACC.TryFetch(tra:Account_Number_Key) <> Level:Benign
            WriteDebug('LoadTRADEACC("' & CLIP(IN:AccountNumber) & '") EOF')

            RETURN False
        END !IF

        IF tra:Account_Number <> IN:AccountNumber
            WriteDebug('LoadTRADEACC("' & CLIP(IN:AccountNumber) & '") EOI')

            RETURN False
        END !IF

        WriteDebug('LoadTRADEACC("' & CLIP(IN:AccountNumber) & '") OK')
        RETURN True
        !-----------------------------------------------
LoadWEBJOB      PROCEDURE( IN:JobNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:WEBJOB.ClearKey(wob:RefNumberKey)
            wob:RefNumber = IN:JobNumber
        SET(wob:RefNumberKey, wob:RefNumberKey)

        IF Access:WEBJOB.NEXT() <> Level:Benign
            RETURN False
        END !IF

        IF NOT wob:RefNumber = IN:JobNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------

NumberToColumn PROCEDURE(IN:Long)! STRING
FirstCol STRING(1)
Temp     LONG
    CODE
        !-----------------------------------------------
        Temp = IN:Long - 1

        FirstCol = Temp / 26
        IF FirstCol = 0
            RETURN CHR(Temp+65)
        END !IF

        RETURN CHR(FirstCol+64) & CHR( (Temp % 26)+65 )
        !-----------------------------------------------
TimeToString        PROCEDURE( IN:Time )! STRING
    CODE
        !-----------------------------------------------------------------
        RETURN LEFT(FORMAT(IN:Time, @T1))
        !-----------------------------------------------------------------
UpdateEngineerQueue     PROCEDURE(IN:UserCode)
DoADD LONG(False)
Temp  STRING(100)
    CODE
        !-----------------------------------------------------------------
        engq:UserCode  = IN:UserCode
        GET(EngineerQueue, +engq:UserCode)
        CASE ERRORCODE()
        OF 00 ! Found
            IF engq:UserCode  = IN:UserCode
                ! NULL
            ELSE
                ! Partial Match - ADD
                DoADD = True
            END !IF
        OF 30
            ! Not in queue - ADD
            DoADD = True
        ELSE
            CancelPressed = True

            WriteDebug('UpdateEngineerQueue(Error=' & ERRORCODE() & ', "' & ERROR() & '")')

            RETURN
        END !CASE
        !-----------------------------------------------------------------
        IF DoADD
            CLEAR(EngineerQueue)
                engq:UserCode       = IN:UserCode

                IF LoadEngineer(IN:UserCode)
                    engq:Forename   = use:Forename
                    engq:Surname    = use:Surname
                ELSE
                    engq:Forename   = CLIP(IN:UserCode) & ' Not Found'
                    engq:Surname    = ''
                END !IF

                engq:JobsBooked     = 0
            ADD(EngineerQueue, +engq:UserCode)
        END !IF
        !-----------------------------------------------------------------
        ! In queue - AMEND
        engq:JobsBooked += 1

        PUT(EngineerQueue, +engq:UserCode)
        !-----------------------------------------------------------------
UpdateSummaryQueue PROCEDURE( IN:Status, IN:StatusType )
DoAdd LONG(False)
    CODE
        !-----------------------------------------------------------------
        sq:Status  = IN:Status
        sq:Type    = IN:StatusType
        GET(SummaryQueue, +sq:Status, +sq:Type)
        CASE ERRORCODE()
        OF 00
            IF (sq:Status  = IN:Status) AND (sq:Type    = IN:StatusType)
                ! NULL
            ELSE
                DoADD = True
            END !IF

        OF 30
            DoADD = True

        ELSE
            CancelPressed = True

            RETURN
        END !CASE
        !-----------------------------------------------------------------
        IF DoADD
            CLEAR(SummaryQueue)
                sq:Status       = IN:Status
                sq:Type         = IN:StatusType
                sq:Count        = 0
            ADD(SummaryQueue, +sq:Status, +sq:Type)
        END !IF
        !-----------------------------------------------------------------
        ! In queue - AMEND
        sq:Count += 1

        PUT(SummaryQueue, +sq:Status, +sq:Type)
        !-----------------------------------------------------------------
WorkingDaysBetween PROCEDURE  ( IN:StartDate, IN:EndDate, IN:IncludeSaturday, IN:IncludeSunday )!,long ! Declare Procedure
Weeks        LONG

DaysDiff     LONG
Days         LONG(0)

DaysPerWeek  LONG(5)
TempDate1    DATE
TempDate2    DATE
    CODE
        !-------------------------------------------
        WriteDebug('WorkingDaysBetween(' & DateToString(IN:StartDate) & ', ' & DateToString(IN:EndDate) & ', ' & IN:IncludeSaturday & ', ' & IN:IncludeSunday & ')')
        !-------------------------------------------
        IF (IN:StartDate = 0)
!            WriteDebug('WorkingDaysBetween(IF (IN:StartDate = 0))')
            RETURN 0
        ELSIF (IN:EndDate = 0)
!            WriteDebug('WorkingDaysBetween(ELSIF (IN:EndDate = 0))')
            RETURN 0
        ELSIF (IN:StartDate = IN:EndDate)
!            WriteDebug('WorkingDaysBetween(IF (IN:StartDate = IN:EndDate))')
            RETURN 0
        ELSIF (IN:StartDate > IN:EndDate)
!            WriteDebug('WorkingDaysBetween(IF (IN:StartDate > IN:EndDate)')
            RETURN WorkingDaysBetween(IN:EndDate, IN:StartDate, IN:IncludeSaturday, IN:IncludeSunday)
        END !IF
        !-------------------------------------------
        IF IN:IncludeSaturday
            DaysPerWeek += 1
        END !IF

        IF IN:IncludeSunday
            DaysPerWeek += 1
        END !IF
        !-------------------------------------------
        TempDate1 = IN:StartDate
        
        LOOP
            If (TempDate1 = IN:EndDate) Then
!                WriteDebug('WorkingDaysBetween(If (TempDate1 = IN:EndDate) Then)')
                RETURN Days
            End !If

            CASE (TempDate1 % 7)
            OF 0
                If IN:IncludeSunday
                    Days += 1
                End !If
            OF 5
                BREAK
            OF 6
                If IN:IncludeSaturday
                    Days += 1
                End !If
            ELSE
                Days += 1
            END !CASE

            TempDate1 += 1

        END !LOOP
        !-------------------------------------------
        TempDate2     = IN:EndDate
        
        LOOP
            If (TempDate1 = TempDate2) Then
!                WriteDebug('WorkingDaysBetween(If (TempDate1 = TempDate2) Then)')
                RETURN Days
            End !If

            CASE (TempDate2 % 7)
            OF 0
                If IN:IncludeSunday
                    Days += 1
                End !If
            OF 5
                BREAK
            OF 6
                If IN:IncludeSaturday
                    Days += 1
                End !If
            ELSE
                Days += 1
            END !CASE

            TempDate2 -= 1

        END !LOOP
        !-------------------------------------------
        Weeks = (TempDate2 - TempDate1) / 7
        !-------------------------------------------
!        message('DaysBefore           =' & Days                  & '<13,10>' & |
!                '(Weeks * DaysPerWeek)=' & (Weeks * DaysPerWeek) & '<13,10>' & |
!                ' ===================== <13,10>'                             & |
!                'TOTAL                =' & Days + (Weeks * DaysPerWeek) )
!        WriteDebug('WorkingDaysBetween(OK="' & Days + (Weeks * DaysPerWeek) & '")')
        RETURN Days + (Weeks * DaysPerWeek)
        !-------------------------------------------
WorkingHoursBetween PROCEDURE  (IN:StartDate, IN:EndDate, IN:StartTime, IN:EndTime, IN:StartWorkHours, IN:EndWorkHours, IN:IncludeSaturday, IN:IncludeSunday ) ! LONG
DaysBetween   LONG
WorkingHours  LONG
Hours         LONG
    CODE
        DaysBetween  = WorkingDaysBetween(IN:StartDate, IN:EndDate, IN:IncludeSaturday, IN:IncludeSunday)
        WorkingHours = (IN:EndWorkHours - IN:StartWorkHours) / (60 * 60 * 100)
        Hours        = 0

        IF DaysBetween = 0
            Hours += HoursBetween(IN:StartTime, IN:EndTime)
        ELSIF DaysBetween = 1
            Hours += HoursBetween(IN:StartTime,      IN:EndWorkHours)
            Hours += HoursBetween(IN:StartWorkHours, IN:EndTime     )
        ELSE
            Hours  = (DaysBetween - 1) * WorkingHours

            Hours += HoursBetween(IN:StartTime,      IN:EndWorkHours)
            Hours += HoursBetween(IN:StartWorkHours, IN:EndTime     )
        END !IF

        RETURN Hours
WriteColumn PROCEDURE( IN:String, IN:StartNewRow )
Temp STRING(255)
    CODE
        !-----------------------------------------------
        WriteDebug('WriteColumn("' & CLIP(IN:String) & '", ' & IN:StartNewRow & ')')

        Temp = IN:String
        IF CLIP(Temp) = ''
            Temp = ''' '
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            IF IN:StartNewRow
                clip:Value = Temp
            ELSE
                clip:Value = CLIP(clip:Value) & '<09>' & Temp
            END !IF

            RETURN
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
WriteDebug      PROCEDURE( IN:Message )
    CODE
        !-----------------------------------------------
        IF NOT debug:Active
            RETURN
        END !IF

        debug:Count += 1

        PUTINI(CLIP(LOC:ProgramName), debug:Count, IN:Message, 'C:\Debug.ini')
        !-----------------------------------------------
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW5.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = tra:RecordNumber
     GET(glo:Queue2,glo:Queue2.Pointer2)
    IF ERRORCODE()
      LocalTag = ''
    ELSE
      LocalTag = 'Y'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (LocalTag = 'Y')
    SELF.Q.LocalTag_Icon = 2
  ELSE
    SELF.Q.LocalTag_Icon = 1
  END


BRW5.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW5.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW5.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW5::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW5::RecordStatus=ReturnValue
  IF BRW5::RecordStatus NOT=Record:OK THEN RETURN BRW5::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = tra:RecordNumber
     GET(glo:Queue2,glo:Queue2.Pointer2)
    EXECUTE DASBRW::6:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW5::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW5::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW5::RecordStatus
  RETURN ReturnValue

