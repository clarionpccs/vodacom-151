

   MEMBER('vodr0100.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('VODR0001.INC'),ONCE        !Local module procedure declarations
                     END


MonthlyWarrantyClaimReport PROCEDURE                  !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::13:TAGFLAG         BYTE(0)
DASBRW::13:TAGMOUSE        BYTE(0)
DASBRW::13:TAGDISPSTATUS   BYTE(0)
DASBRW::13:QUEUE          QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::16:TAGFLAG         BYTE(0)
DASBRW::16:TAGMOUSE        BYTE(0)
DASBRW::16:TAGDISPSTATUS   BYTE(0)
DASBRW::16:QUEUE          QUEUE
Pointer2                      LIKE(glo:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
tmp:VersionNumber    STRING(30)
local:Desktop        CSTRING(255)
tmp:Clipboard        ANY
StatusQueue          QUEUE,PRE(staque)
StatusMessage        STRING(60)
                     END
TempFilePath         CSTRING(255)
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:LastDetailColumn STRING(2)
tmp:LastSummaryColumn STRING(2)
tmp:UserName         STRING(70)
tmp:HeadAccountNumber STRING(30)
tmp:ARCLocation      STRING(30)
Progress:Thermometer BYTE(0)
tmp:ARCAccount       BYTE(0)
tmp:ARCCompanyName   STRING(30)
tmp:ARCUser          BYTE(0)
TempFileQueue        QUEUE,PRE(tmpque)
AccountNumber        STRING(30)
CompanyName          STRING(30)
Count                LONG
FileName             STRING(255)
JobsBooked           LONG
JobsOpen             LONG
                     END
tag:ChargeType       STRING(1)
tmp:AllChargeTypes   BYTE(1)
MaxTabs              BYTE
TabNumber            BYTE
tag:Manufacturer     STRING(1)
tmp:AllManufacturers BYTE(1)
tmp:AllWarrantyStatus BYTE(1)
tmp:Pending          BYTE(0)
tmp:Approved         BYTE(0)
tmp:Query            BYTE(0)
tmp:OnTechnicalReport BYTE(0)
tmp:Rejected         BYTE(0)
tmp:SubmittedToMFTR  BYTE(0)
tmp:MFTRPaid         BYTE(0)
tmp:DateRangeType    BYTE(0)
local:RecordCount    LONG
tmp:Yes              STRING('YES')
gFaultCodesGroup     GROUP(gFaultCodes),PRE(gfcg)
                     END
locTotalParts        LONG
BRW12::View:Browse   VIEW(CHARTYPE)
                       PROJECT(cha:Charge_Type)
                       PROJECT(cha:Warranty)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tag:ChargeType         LIKE(tag:ChargeType)           !List box control field - type derived from local data
tag:ChargeType_Icon    LONG                           !Entry's icon ID
cha:Charge_Type        LIKE(cha:Charge_Type)          !List box control field - type derived from field
cha:Warranty           LIKE(cha:Warranty)             !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW15::View:Browse   VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
tag:Manufacturer       LIKE(tag:Manufacturer)         !List box control field - type derived from local data
tag:Manufacturer_Icon  LONG                           !Entry's icon ID
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Excel                SIGNED !OLE Automation holder
excel:ProgramName    CString(255)
excel:ActiveWorkBook CString(20)
excel:Selected       CString(20)
excel:FileName       CString(255)
loc:Version          Cstring(30)
window               WINDOW('** Report Title **'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),ICON('Cellular3g.ico'),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE,IMM
                       BUTTON,AT(252,366),USE(?VSBackButton),TRN,FLAT,LEFT,ICON('backp.jpg')
                       BUTTON,AT(368,366),USE(?VSNextButton),TRN,FLAT,RIGHT,ICON('nextp.jpg')
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(648,4),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Monthly Warranty Claim Report'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,56,552,306),USE(?Sheet1),COLOR(0D6E7EFH),SPREAD
                         TAB('Tab 2'),USE(?Tab2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           GROUP,AT(252,100,256,240),USE(?Group:ChargeTypes)
                             LIST,AT(252,102,176,236),USE(?List),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)J@s1@120L(2)|M~Charge Type~@s30@'),FROM(Queue:Browse)
                             BUTTON,AT(432,250),USE(?DASTAG),TRN,FLAT,ICON('tagitemp.jpg')
                             BUTTON,AT(432,282),USE(?DASTAGAll),TRN,FLAT,ICON('tagallp.jpg')
                             BUTTON,AT(432,312),USE(?DASUNTAGALL),TRN,FLAT,ICON('untagalp.jpg')
                           END
                           PROMPT('Select Charge Type(s)'),AT(252,74),USE(?Prompt8),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('All Charge Types'),AT(252,90),USE(tmp:AllChargeTypes),TRN,FONT(,,COLOR:White,FONT:bold),MSG('All Charge Types'),TIP('All Charge Types'),VALUE('1','0')
                           BUTTON('&Rev tags'),AT(309,222,1,1),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(309,244,1,1),USE(?DASSHOWTAG),HIDE
                         END
                         TAB('Tab 3'),USE(?Tab3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           GROUP,AT(252,100,256,242),USE(?Group:Manufacturers)
                             LIST,AT(252,102,176,236),USE(?List:2),IMM,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)J@s1@120L(2)|M~Manufacturer~@s30@'),FROM(Queue:Browse:1)
                             BUTTON,AT(432,248),USE(?DASTAG:2),TRN,FLAT,ICON('tagitemp.jpg')
                             BUTTON,AT(432,280),USE(?DASTAGAll:2),TRN,FLAT,ICON('tagallp.jpg')
                             BUTTON,AT(432,312),USE(?DASUNTAGALL:2),TRN,FLAT,ICON('untagalp.jpg')
                           END
                           PROMPT('Select Manufacturer(s)'),AT(252,74),USE(?Prompt9),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('All Manufacturers'),AT(252,90),USE(tmp:AllManufacturers),TRN,FONT(,,COLOR:White,FONT:bold),MSG('All Manufacturers'),TIP('All Manufacturers'),VALUE('1','0')
                           BUTTON('&Rev tags'),AT(301,231,1,1),USE(?DASREVTAG:2),HIDE
                           BUTTON('sho&W tags'),AT(293,266,1,1),USE(?DASSHOWTAG:2),HIDE
                         END
                         TAB('Criteria'),USE(?Tab1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           CHECK('Select All'),AT(196,114),USE(tmp:AllWarrantyStatus),TRN,FONT(,,COLOR:White,FONT:bold),MSG('Select All'),TIP('Select All'),VALUE('1','0')
                           GROUP('Select Warranty Status'),AT(188,128,299,54),USE(?Group:WarrantyStatus),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             CHECK('Pending'),AT(194,144),USE(tmp:Pending),TRN,FONT(,,COLOR:White,FONT:bold),MSG('Pending'),TIP('Pending'),VALUE('1','0')
                             CHECK('Approved'),AT(256,144),USE(tmp:Approved),TRN,FONT(,,COLOR:White,FONT:bold),MSG('Approved'),TIP('Approved'),VALUE('1','0')
                             CHECK('Query'),AT(334,144),USE(tmp:Query),TRN,FONT(,,COLOR:White,FONT:bold),MSG('Query'),TIP('Query'),VALUE('1','0')
                             CHECK('On Technical Report'),AT(388,144),USE(tmp:OnTechnicalReport),TRN,FONT(,,COLOR:White,FONT:bold),MSG('On Technical Report'),TIP('On Technical Report'),VALUE('1','0')
                             CHECK('Rejected'),AT(194,162),USE(tmp:Rejected),TRN,FONT(,,COLOR:White,FONT:bold),MSG('Rejected'),TIP('Rejected'),VALUE('1','0')
                             CHECK('Submitted To MFTR'),AT(254,162),USE(tmp:SubmittedToMFTR),TRN,FONT(,,COLOR:White,FONT:bold),MSG('Submitted To MFTR'),TIP('Submitted To MFTR'),VALUE('1','0')
                             CHECK('MFTR Paid'),AT(386,162),USE(tmp:MFTRPaid),TRN,FONT(,,COLOR:White,FONT:bold),MSG('MFTR Paid'),TIP('MFTR Paid'),VALUE('1','0')
                           END
                           PROMPT('Start Date'),AT(364,226),USE(?tmp:StartDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(440,226,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Start Date'),TIP('Start Date'),REQ,UPR
                           BUTTON,AT(508,224),USE(?PopCalendar),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT('End Date'),AT(364,250),USE(?tmp:EndDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@d6),AT(440,250,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Jobs Completed To'),TIP('Jobs Completed To'),REQ,UPR
                           BUTTON,AT(508,248),USE(?PopCalendar:2),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                           PROMPT(''),AT(68,154),USE(?StatusText)
                           OPTION('Date Range Type'),AT(144,206,168,80),USE(tmp:DateRangeType),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             RADIO('Completed Date Range'),AT(156,224),USE(?tmp:DateRangeType:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                             RADIO('Claim Submitted Date Range'),AT(156,242),USE(?tmp:DateRangeType:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                             RADIO('Claim Approved Date Range'),AT(156,259),USE(?tmp:DateRangeType:Radio3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('2')
                           END
                           PROMPT('Ensure Excel is NOT running before you begin!'),AT(238,342,204,12),USE(?Prompt4),CENTER,FONT(,,0D0FFD0H,FONT:bold),COLOR(09A6A7CH)
                         END
                       END
                       BUTTON,AT(472,366),USE(?Print),TRN,FLAT,ICON('printp.jpg'),DEFAULT
                       BUTTON,AT(544,366),USE(?Cancel),TRN,FLAT,ICON('cancelp.jpg')
                       PROMPT('Report Version'),AT(72,376),USE(?ReportVersion),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                     END

!Progress Window (DBH: 22-03-2004)
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte



progresswindow WINDOW('Progress...'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH), |
         CENTER,IMM,ICON('cellular3g.ico'),WALLPAPER('sbback.jpg'),TILED,TIMER(1),GRAY,DOUBLE
       PANEL,AT(160,64,360,300),USE(?Panel5),FILL(0D6E7EFH)
       PANEL,AT(164,68,352,12),USE(?Panel1),FILL(09A6A7CH)
       LIST,AT(208,88,268,206),USE(?List1),VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH), |
           FORMAT('20L(2)|M'),FROM(StatusQueue),GRID(COLOR:White)
       PANEL,AT(164,84,352,246),USE(?Panel4),FILL(09A6A7CH)
       PROGRESS,USE(progress:thermometer),AT(206,314,268,12),RANGE(0,100)
       STRING(''),AT(259,300,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,080FFFFH,FONT:bold), |
           COLOR(09A6A7CH)
       STRING(''),AT(232,136,161,10),USE(?progress:pcttext),TRN,HIDE,CENTER,FONT('Arial',8,,)
       PANEL,AT(164,332,352,28),USE(?Panel2),FILL(09A6A7CH)
       PROMPT('Report Progress'),AT(168,70),USE(?WindowTitle2),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
       BUTTON,AT(444,332),USE(?ProgressCancel),TRN,FLAT,LEFT,ICON('Cancelp.jpg')
       BUTTON,AT(376,332),USE(?Button:OpenReportFolder),TRN,FLAT,HIDE,ICON('openrepp.jpg')
       BUTTON,AT(444,332),USE(?Finish),TRN,FLAT,HIDE,ICON('Finishp.jpg')
     END
local       Class
DrawBox                 Procedure(String func:TL,String func:TR,String func:BL,String func:BR,Long func:Colour)
UpdateProgressWindow    Procedure(String  func:Text)
WriteLine               Procedure()
            End ! local       Class
!Export Files (DBH: 22-03-2004)
ExportFile    File,Driver('ASCII'),Pre(exp),Name(glo:ExportFile),Create,Bindable,Thread
Record                  Record
Line1                   String(2000)
                        End
                    End
Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
E1                   Class(oiExcel)
Init                   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0),byte,proc,virtual,name('INIT@F17OIEXCEL')
Kill                   PROCEDURE (byte pUnloadCOM=1),byte,proc,virtual,name('KILL@F17OIEXCEL')
TakeEvent              PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0),virtual,name('TAKEEVENT@F17OIEXCEL')
                     End

BRW12                CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW12::Sort0:Locator StepLocatorClass                 !Default Locator
BRW15                CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW15::Sort0:Locator StepLocatorClass                 !Default Locator
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
    Map
ExcelSetup          Procedure(Byte      func:Visible)

ExcelMakeWorkBook   Procedure(String    func:Title,String   func:Author,String  func:AppName)

ExcelMakeSheet      Procedure()

ExcelSheetType      Procedure(String    func:Type)

ExcelHorizontal     Procedure(String    func:Direction)

ExcelVertical        Procedure(String    func:Direction)

ExcelCell   Procedure(String    func:Text,Byte  func:Bold)

ExcelFormatCell     Procedure(String    func:Format)

ExcelFormatRange    Procedure(String    func:Range,String   func:Format)

ExcelNewLine    Procedure(Long  func:Number)

ExcelMoveDown   Procedure()

ExcelColumnWidth        Procedure(String    func:Range,Long   func:Width)

ExcelCellWidth          Procedure(Long  func:Width)

ExcelAutoFit            Procedure(String    func:Range)

ExcelGrayBox            Procedure(String    func:Range)

ExcelGrid   Procedure(String    func:Range,Byte  func:Left,Byte  func:Top,Byte   func:Right,Byte func:Bottom,Byte func:Colour)

ExcelSelectRange        Procedure(String    func:Range)

ExcelFontSize           Procedure(Byte  func:Size)

ExcelSheetName          Procedure(String    func:Name)

ExcelSelectSheet    Procedure(String    func:SheetName)

ExcelAutoFilter         Procedure(String    func:Range)

ExcelDropAllSheets      Procedure()

ExcelDeleteSheet        Procedure(String    func:SheetName)

ExcelClose              Procedure()

ExcelSaveWorkBook       Procedure(String    func:Name)

ExcelFontColour         Procedure(String    func:Range,Long func:Colour)

ExcelWrapText           Procedure(String    func:Range,Byte func:True)

ExcelGetFilename        Procedure(Byte      func:DontAsk),Byte

ExcelGetDirectory       Procedure(),Byte

ExcelCurrentColumn      Procedure(),String

ExcelCurrentRow         Procedure(),String

ExcelPasteSpecial       Procedure(String    func:Range)

ExcelConvertFormula     Procedure(String    func:Formula),String

ExcelColumnLetter Procedure(Long  func:ColumnNumber),String

ExcelOpenDoc            Procedure(String    func:FileName)

ExcelFreeze             Procedure(String    func:Cell)
    End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::13:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW12.UpdateBuffer
   glo:Queue.Pointer = cha:Charge_Type
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = cha:Charge_Type
     ADD(glo:Queue,glo:Queue.Pointer)
    tag:ChargeType = '*'
  ELSE
    DELETE(glo:Queue)
    tag:ChargeType = ''
  END
    Queue:Browse.tag:ChargeType = tag:ChargeType
  IF (tag:ChargeType = '*')
    Queue:Browse.tag:ChargeType_Icon = 2
  ELSE
    Queue:Browse.tag:ChargeType_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::13:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW12.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW12::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = cha:Charge_Type
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW12.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::13:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW12.Reset
  SETCURSOR
  BRW12.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::13:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::13:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::13:QUEUE = glo:Queue
    ADD(DASBRW::13:QUEUE)
  END
  FREE(glo:Queue)
  BRW12.Reset
  LOOP
    NEXT(BRW12::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::13:QUEUE.Pointer = cha:Charge_Type
     GET(DASBRW::13:QUEUE,DASBRW::13:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = cha:Charge_Type
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW12.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::13:DASSHOWTAG Routine
   CASE DASBRW::13:TAGDISPSTATUS
   OF 0
      DASBRW::13:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::13:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::13:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW12.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::16:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?List:2))
  BRW15.UpdateBuffer
   glo:Queue2.Pointer2 = man:Manufacturer
   GET(glo:Queue2,glo:Queue2.Pointer2)
  IF ERRORCODE()
     glo:Queue2.Pointer2 = man:Manufacturer
     ADD(glo:Queue2,glo:Queue2.Pointer2)
    tag:Manufacturer = '*'
  ELSE
    DELETE(glo:Queue2)
    tag:Manufacturer = ''
  END
    Queue:Browse:1.tag:Manufacturer = tag:Manufacturer
  IF (tag:Manufacturer)
    Queue:Browse:1.tag:Manufacturer_Icon = 2
  ELSE
    Queue:Browse:1.tag:Manufacturer_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::16:DASTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW15.Reset
  FREE(glo:Queue2)
  LOOP
    NEXT(BRW15::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue2.Pointer2 = man:Manufacturer
     ADD(glo:Queue2,glo:Queue2.Pointer2)
  END
  SETCURSOR
  BRW15.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::16:DASUNTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue2)
  BRW15.Reset
  SETCURSOR
  BRW15.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::16:DASREVTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::16:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue2)
    GET(glo:Queue2,QR#)
    DASBRW::16:QUEUE = glo:Queue2
    ADD(DASBRW::16:QUEUE)
  END
  FREE(glo:Queue2)
  BRW15.Reset
  LOOP
    NEXT(BRW15::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::16:QUEUE.Pointer2 = man:Manufacturer
     GET(DASBRW::16:QUEUE,DASBRW::16:QUEUE.Pointer2)
    IF ERRORCODE()
       glo:Queue2.Pointer2 = man:Manufacturer
       ADD(glo:Queue2,glo:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW15.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::16:DASSHOWTAG Routine
   CASE DASBRW::16:TAGDISPSTATUS
   OF 0
      DASBRW::16:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::16:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::16:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:2{PROP:Text} = 'Show All'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:2{PROP:Text})
   BRW15.ResetSort(1)
   SELECT(?List:2,CHOICE(?List:2))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
PrepareFaultCodes       ROUTINE
    gFaultCodesGroup.Manufacturer = job:Manufacturer
    gFaultCodesGroup.FaultCode[1] = job:Fault_Code1
    gFaultCodesGroup.FaultCode[2] = job:Fault_Code2
    gFaultCodesGroup.FaultCode[3] = job:Fault_Code3
    gFaultCodesGroup.FaultCode[4] = job:Fault_Code4
    gFaultCodesGroup.FaultCode[5] = job:Fault_Code5
    gFaultCodesGroup.FaultCode[6] = job:Fault_Code6
    gFaultCodesGroup.FaultCode[7] = job:Fault_Code7
    gFaultCodesGroup.FaultCode[8] = job:Fault_Code8
    gFaultCodesGroup.FaultCode[9] = job:Fault_Code9
    gFaultCodesGroup.FaultCode[10] = job:Fault_Code10
    gFaultCodesGroup.FaultCode[11] = job:Fault_Code11
    gFaultCodesGroup.FaultCode[12] = job:Fault_Code12
    gFaultCodesGroup.FaultCode[13] = wob:FaultCode13
    gFaultCodesGroup.FaultCode[14] = wob:FaultCode14
    gFaultCodesGroup.FaultCode[15] = wob:FaultCode15
    gFaultCodesGroup.FaultCode[16] = wob:FaultCode16
    gFaultCodesGroup.FaultCode[17] = wob:FaultCode17
    gFaultCodesGroup.FaultCode[18] = wob:FaultCode18
    gFaultCodesGroup.FaultCode[19] = wob:FaultCode19
    gFaultCodesGroup.FaultCode[20] = wob:FaultCode20
Reporting       Routine  !Do the Report (DBH: 22-03-2004)
Data
local:LocalPath              String(255)
local:CurrentAccount    String(30)
local:ReportStartDate   Date()
local:ReportStartTime   Time()

Code
    local:RecordCount = 0
    !Set the temp folder for the csv files (DBH: 10-03-2004)
    If GetTempPathA(255,TempFilePath)
        If Sub(TempFilePath,-1,1) = '\'
            local:LocalPath = Clip(TempFilePath) & ''
        Else !If Sub(TempFilePath,-1,1) = '\'
            local:LocalPath = Clip(TempFilePath) & '\'
        End !If Sub(TempFilePath,-1,1) = '\'
    End

    !Set the folder for the excel file (DBH: 10-03-2004)
    excel:ProgramName = 'Monthly Warranty Claim Report'

    ! Create Folder In My Documents
    SHGetSpecialFolderPath( GetDesktopWindow(), local:Desktop, 5, FALSE )
    local:Desktop = Clip(local:Desktop) & '\ServiceBase Export'

    !Does the Export Folder already Exists?
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK') 
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    local:Desktop = Clip(local:Desktop) & '\' & Clip(excel:ProgramName)
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    excel:FileName = Clip(local:Desktop) & '\' & Clip(excel:ProgramName) & ' ' & Format(Today(),@d12)

    If Exists(excel:FileName & '.xls')
        Remove(excel:FileName & '.xls')
        If Error()
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('Cannot get access to the selected document:'&|
                '|' & Clip(excel:FileName) & ''&|
                '|'&|
                '|Ensure the file is not in use and try again.','ServiceBase',|
                           'mstop.jpg','/&OK') 
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If Error()
    End !If Exists(excel:FileName)

    If COMMAND('/DEBUG')
        Remove('c:\debug.ini')
    End !If COMMAND('/DEBUG')

    !Open Program Window (DBH: 10-03-2004)
    recordspercycle         = 25
    recordsprocessed        = 0
    percentprogress         = 0
    progress:thermometer    = 0
    recordstoprocess        = 50
    Open(ProgressWindow)

    ?progress:userstring{prop:text} = 'Running...'
    ?progress:pcttext{prop:text} = '0% Completed'


    local:ReportStartDate = Today()
    local:ReportStartTime = Clock()

    If Command('/DEBUG')
        local.UpdateProgressWindow('========')
        local.UpdateProgressWindow('*** DEBUG MODE ***')
        local.UpdateProgressWindow('========')
        local.UpdateProgressWindow('')
    End !If Command('/DEBUG')

    local.UpdateProgressWindow('Report Started: ' & Format(local:ReportStartDate,@d6b) & ' ' & Format(local:ReportStartTime,@t1b))
    local.UpdateProgressWindow('')
    local.UpdateProgressWindow('Creating Export Files...')

    !_____________________________________________________________________

    !CREATE CSV FILES
    !_____________________________________________________________________

    local.UpdateProgressWindow('')

    glo:ExportFile = Clip(local:LocalPath) & 'vodr0100' & Clock() & '.CSV'
    Remove(glo:ExportFile)
    Create(ExportFile)
    Open(ExportFile)

    case (tmp:DateRangeType)
    of 0 ! Completed
        Access:JOBS.Clearkey(job:DateCompletedKey)
        job:Date_Completed    = tmp:StartDate
        set(job:DateCompletedKey,job:DateCompletedKey)

    of 1 ! Submitted
        Access:JOBSWARR.Clearkey(jow:ClaimSubmittedKey)
        jow:ClaimSubmitted    = tmp:StartDate
        set(jow:ClaimSubmittedKey,jow:ClaimSubmittedKey)
    of 2 ! Approved
        Access:JOBSWARR.Clearkey(jow:DateAcceptedKey)
        jow:DateAccepted    = tmp:StartDate
        set(jow:DateAcceptedKey,jow:DateAcceptedKey)
    end ! case (tmp:DateRangeType)

    loop

        Do GetNextRecord2
        Do CancelCheck
        If tmp:Cancel
            Break
        End !If tmp:Cancel = 1

        case (tmp:DateRangeType)
        of 0 ! Completed
            if (Access:JOBS.Next())
                Break
            end ! if (Access:JOBS.Next())
            if (job:Date_Completed    > tmp:EndDate)
                Break
            end ! if (job:Date_Completed    <> tmp:StartDate)

            Access:JOBSWARR.Clearkey(jow:RefNumberKey)
            jow:RefNumber    = job:Ref_Number
            if (Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign)
                ! Found
            else ! if (Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign)
                ! Error
                cycle
            end ! if (Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign)
        of 1 ! Submitted
            if (Access:JOBSWARR.Next())
                Break
            end ! if (Access:JOBSWARR.Next())
            if (jow:ClaimSubmitted    > tmp:EndDate)
                Break
            end ! if (jow:ClaimSubmitted    <> tmp:StartDate)

            Access:JOBS.Clearkey(job:Ref_Number_Key)
            job:Ref_Number    = jow:RefNumber
            if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                ! Found
            else ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                ! Error
                cycle
            end ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
        of 2 ! Approved
            if (Access:JOBSWARR.Next())
                Break
            end ! if (Access:JOBSWARR.Next())
            if (jow:DateAccepted    > tmp:EndDate)
                Break
            end ! if (jow:DateAccepted    <> tmp:StartDate)

            Access:JOBS.Clearkey(job:Ref_Number_Key)
            job:Ref_Number    = jow:RefNumber
            if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                ! Found
            else ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
                ! Error
                cycle
            end ! if (Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign)
        end ! case (tmp:DateRangeType)


        Access:JOBSE.Clearkey(jobe:RefNumberKey)
        jobe:RefNumber    = job:Ref_Number
        if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
            ! Found
        else ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)
            ! Error
            cycle
        end ! if (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign)

        Access:WEBJOB.Clearkey(wob:RefNumberKey)
        wob:RefNumber    = job:Ref_Number
        if (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
            ! Found
        else ! if (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)
            ! Error
            cycle
        end ! if (Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign)

        Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
        jobe2:RefNumber    = job:Ref_Number
        if (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
            ! Found
        else ! if (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
            ! Error
            cycle
        end ! if (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
        ! Criteria____________________________________________________________________________________

        if (job:Warranty_Job <> 'YES')
            cycle
        end ! if (job:Warranty_Job <> 'YES')

        if (tmp:AllChargeTypes <> 1)
            glo:Pointer = job:Warranty_Charge_Type
            get(glo:Queue,glo:Pointer)
            if (error())
                cycle
            end ! if (error())
        end ! if (tmp:AllChargeTypes <> 1)

        if (tmp:AllManufacturers <> 1)
            glo:Pointer2 = job:Manufacturer
            get(glo:Queue2,glo:Pointer2)
            if (error())
                cycle
            end ! if (error())
        end ! if (tmp:AllManufacturers <> 1)

        if (~tmp:AllWarrantyStatus)

            Case jow:Status
            Of 'NO'
                Case jobe:WarrantyClaimStatus
                Of 'ON TECHNICAL REPORT'
                    If ~tmp:OnTechnicalReport
                        cycle
                    End ! If ~tmp:OnTechnicalReport
                Of 'SUBMITTED TO MFTR'
                    If ~tmp:SubmittedToMFTR
                        cycle
                    End ! If ~tmp:SubmittedToMFTR
                Else
                    If ~tmp:Pending
                        cycle
                    End ! If ~tmp:Pending
                End ! Case jobe:WarrantyClaimStatus
            Of 'YES'
                If ~tmp:Approved
                    cycle
                End ! If ~tmp:Approved
            Of 'EXC'
                If ~tmp:Rejected
                    cycle
                End ! If ~tmp:Rejected
            Of 'QUE'
                If ~tmp:Query
                    cycle
                End ! If ~tmp:Query
            Of 'REJ'
                cycle
            Else
                cycle
            End ! Case jow:Status
        end ! if (~tmp:AllWarrantyStatus)


        local.WriteLine()
        local:RecordCount += 1

    End !Loop TRADEACC
    Close(ExportFile)

    if (local:RecordCount > 0)
        local.UpdateProgressWindow('Jobs Found: ' & local:RecordCount)
    else ! if (local:RecordCount > 1)
        local.UpdateProgressWindow('No Jobs Found..')
    end ! if (local:RecordCount > 1)
    local.UpdateProgressWindow('')
    !_____________________________________________________________________

    If ((tmp:Cancel = 0 Or tmp:Cancel = 2) and local:RecordCount > 0)

        ! Add one to account for the title (DBH: 09/03/2009)
        local:RecordCount += 1

        ?ProgressCancel{prop:Disable} = 1

        local.UpdateProgressWindow('================')
        local.UpdateProgressWindow('Building Excel Document..')
    !_____________________________________________________________________

    !CREATE EXPORT DOCUMENT, CREATE SHEETS, COLUMNS and AUTOFILTER
    !_____________________________________________________________________

        local.UpdateProgressWindow('Creating Excel Sheets...')
        local.UpdateProgressWindow('')

        If E1.Init(0,0) = 0
            Case Missive('An error has occurred finding your Excel document.'&|
              '<13,10>'&|
              '<13,10>Please quit and try again.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
                Of 1 ! OK Button
            End ! Case Missive
            Exit
        End !If E1.Init(0,0,1) = 0

        tmp:LastDetailColumn = E1.GetColumnAddressFromColumnNumber(45 + (locTotalParts * 4)) ! Get last column based on 4 columns per part

        SetClipBoard('')
        E1.OpenWorkBook(glo:ExportFile)
        E1.Copy('A1',Clip(tmp:LastDetailColumn) & local:RecordCount)
        tmp:Clipboard = ClipBoard()
        SBR.ClearClipboard()
        E1.CloseWorkBook(2)

        E1.NewWorkBook()

        titleCol# = 11
        col# = 1
        E1.WriteToCell('SB Job No',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('Franchise Branch ID',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('Completed',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('Claim Submission Date',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('Head Account Number',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('Head Account Name',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('Manufacturer',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('Model Number',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('Warranty Charge Type',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('Warranty Repair Type',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('Engineer',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('Repair',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('Exchange',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('Exchange Unit IMEI No',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('Original IMEI',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('Exchange/Handling Fee',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('Exchange Purchase Price',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('RRC Parts Cost',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('RRC Parts Sale',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('RRC Labour',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('RRC Sub Total',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('ARC Parts Cost',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('ARC Parts Sale',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('ARC Labour',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('ARC Sub Total',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('Billed MFTR Labour',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('Billed MFTR Parts',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('Billed MFTR Sub Total',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('Accepted Date',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('MFTR Parts',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('MFTR Labour',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('MFTR Sub Total',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('MFTR Vat',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('MFTR Total',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('In Fault',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('Out Fault',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('Reconciled Date',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('Warranty Status',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('Rejection Date 1',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('Rejection Reason 1',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('Rejection Date 2',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('Rejection Reason 2',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('Final Rejection Date',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('Final Rejection Reason',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        E1.WriteToCell('Rejection Accepted By',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1

        ! Add columns for the total number of parts on any job
        LOOP pp# = 1 TO locTotalParts
            E1.WriteToCell('Part Number',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
            E1.WriteToCell('Description',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
            E1.WriteToCell('Quantity Of Parts Attached To Job',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
            E1.WriteToCell('Total Parts Cost',E1.GetColumnAddressFromColumnNumber(col#) & titleCol#);col# += 1
        END

        E1.SelectCells('A12')
        SetClipBoard(tmp:ClipBoard)
        E1.SetCellNumberFormat(oix:NumberFormatText,,0,,'H12','N' & local:RecordCount + 12)
        E1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,'P12','Q' & local:RecordCount + 12)
        E1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,'AO12','AO' & local:RecordCount + 12)
        E1.Paste()
        SBR.ClearClipboard()
        

        E1.RenameWorkSheet(excel:ProgramName)

        Do DrawTitle


        local.DrawBox('A9',Clip(tmp:LastDetailColumn) & '9','A9',Clip(tmp:LastDetailColumn) & '9',color:Silver)
        local.DrawBox('A11',Clip(tmp:LastDetailColumn) & '11','A11',Clip(tmp:LastDetailColumn) & '11',color:Silver)

        E1.WriteToCell('Section Name','A9')
        E1.WriteToCell(excel:ProgramName,'B9')
        E1.WriteToCell('Total Records','E9')
        E1.WriteToCell(local:RecordCount - 1,'F9')
        E1.WriteToCell('Showing','G9')
        E1.WriteToCell('=SUBTOTAL(2,A12:A' & local:RecordCount + 12,'H9')

        E1.SetCellFontName('Tahoma','A9',Clip(tmp:LastDetailColumn) & local:RecordCount + 12)
        E1.SetCellFontSize(8,'A1',Clip(tmp:LastDetailColumn) & local:RecordCount + 12)
        E1.SetCellFontStyle('Bold','A11',Clip(tmp:LastDetailColumn) & '11')
        E1.SetCellFontStyle('Bold','A9')
        E1.SetCellFontStyle('Bold','F9')
        E1.SetCellFontStyle('Bold','H9')
        E1.SetCellBackgroundColor(color:Silver,'A9',Clip(tmp:LastDetailColumn) & '9')
        E1.SetCellBackgroundColor(color:Silver,'A11',Clip(tmp:LastDetailColumn) & '11')

!        E1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,'O12','Y' & local:RecordCount + 12)
!        E1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,'AB12','AE' & local:RecordCount + 12)
        E1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,'O12','AB' & local:RecordCount + 12)  ! #12228 Don't format the date column (DBH: 05/04/2012)
        E1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,'AD12','AE' & local:RecordCount + 12) ! #12228 Don't format the date column (DBH: 05/04/2012)

        

        E1.AutoFilter('A11',tmp:LastDetailColumn & '11')
        E1.SetColumnWidth('A',Clip(tmp:LastDetailColumn))
        E1.SelectCells('B12')
        E1.FreezePanes()

!        ! Create Summary Worksheet (DBH: 01/08/2006)
!        E1.InsertWorksheet()
!        E1.RenameWorkSheet('Summary')
!
!        Do DrawTitle
!
!        local.DrawBox('A9','E9','A9','E9',color:Silver)
!        local.DrawBox('A10','E10','A10','E10',color:Silver)
!
!        E1.WriteToCell('Summary','A9')
!
!        ! Define Summary Column Titles (DBH: 01/08/2006)
!        E1.WriteToCell('Head Account Number','A10')
!        E1.WriteToCell('Head Account Name','B10')
!
!        E1.SelectCells('A11')
!
!        tmp:LastSummaryColumn = 'ZZ'
!        local:RecordCount = 11
!!        Sort(SummaryQueue,que:AccountNumber)
!!        Loop x# = 1 To Records(SummaryQueue)
!!            Get(SummaryQueue,x#)
!!            local:RecordCount += 1
!!        End ! Loop x# = 1 To Records(SummaryQueue)
!
!        local.DrawBox('A' & local:RecordCount,Clip(tmp:LastSummaryColumn) & local:RecordCount,'A' & local:RecordCount,Clip(tmp:LastSummaryColumn) & local:RecordCount,color:Silver)
!        E1.SetCellFontName('Tahoma','A9',Clip(tmp:LastSummaryColumn) & local:RecordCount)
!        E1.SetCellFontStyle('Bold','A9',Clip(tmp:LastSummaryColumn) & '10')
!        E1.SetCellFontStyle('Bold','A' & local:RecordCount,Clip(tmp:LastSummaryColumn) & local:RecordCount)
!        E1.SetCellFontSize(8,'A1',Clip(tmp:LastSummaryColumn) & local:RecordCount)
!        E1.SetColumnWidth('A',Clip(tmp:LastSummaryColumn))

        E1.SelectCells('A11')

        E1.SaveAs(excel:FileName)
        E1.CloseWorkBook(2)
        E1.Kill()

         local.UpdatePRogressWindow('Finishing Off Formatting..')


        local.UpdateProgressWindow('================')
        local.UpdateProgressWindow('Report Finished: ' & Format(Today(),@d6b) & ' ' & Format(Clock(),@t1b))

    Else!If tmp:Cancel = False
        staque:StatusMessage = '=========='
        Add(StatusQueue)
        Select(?List1,Records(StatusQueue))

        staque:StatusMessage = 'Report CANCELLED: ' & Format(Today(),@d6b) & ' ' & Format(Clock(),@t1b)
        Add(StatusQueue)
        Select(?List1,Records(StatusQueue))

    End !If tmp:Cancel = False

    BHReturnDaysHoursMins(BHTimeDifference24Hr(local:ReportStartDate,Today(),local:ReportStartTime,Clock()),Days#,Hours#,Mins#)

    local.UpdateProgressWindow('Time To Finish: ' & Days# & ' Dys, ' & Format(Hours#,@n02) & ':' & Format(Mins#,@n02))
    Add(StatusQueue)
    Select(?List1,Records(StatusQueue))

    Do EndPrintRun
    ?progress:userstring{prop:text} = 'Finished...'

    Display()
    ?ProgressCancel{prop:Hide} = 1
    ?Finish{prop:Hide} = 0

    If Command('/SCHEDULE')
        If Access:REPSCHLG.PrimeRecord() = Level:Benign
            rlg:REPSCHEDRecordNumber = rpd:RecordNumber
            rlg:Information = 'Report Name: ' & Clip(rpd:ReportName) & ' - ' & Clip(rpd:ReportCriteriaType) & |
                              '<13,10>Report Finished'
            If Access:REPSCHLG.TryInsert() = Level:Benign
                !Insert
            Else ! If Access:REPSCHLG.TryInsert() = Level:Benign
                Access:REPSCHLG.CancelAutoInc()
            End ! If Access:REPSCHLG.TryInsert() = Level:Benign
        End ! If Access.REPSCHLG.PrimeRecord() = Level:Benign
    Else
        ?Button:OpenReportFolder{Prop:Hide} = 0
        Accept
            Case Field()
                Of ?Finish
                    Case Event()
                        Of Event:Accepted
                            Break

                    End !Case Event()
                Of ?Button:OpenReportFolder
                    Case Event()
                    Of Event:Accepted
                        RUN('EXPLORER.EXE ' & Clip(local:Desktop))
                    End ! Case Event()
            End !Case Field()
        End !Accept
    End ! If Command('/SCHEDULE')

    Close(ProgressWindow)
    Remove(ExportFile)
    ?StatusText{prop:Text} = ''
    Post(Event:CloseWindow)
DrawTitle     Routine     !Set Detail Title (DBH: 22-03-2004)
    E1.WriteToCell(excel:ProgramName, 'A1')
    E1.WriteToCell('Criteria','A3')
    E1.WriteToCell(tmp:VersionNumber,'D3')
    E1.WriteToCell('Start Date','A4')
    E1.WriteToCell(Format(tmp:StartDate,@d06),'B4')
    E1.WriteToCell('End Date','A5')
    E1.WriteToCell(Format(tmp:EndDate,@d06),'B5')
    If Clip(tmp:UserName) <> ''
        E1.WriteToCell('Created By','A6')
        E1.WriteToCell(tmp:UserName,'B6')
    End ! If Clip(tmp:UserName) <> ''
    E1.WriteToCell('Created Date','A7')
    E1.WriteToCell(Format(Today(),@d06),'B7')

    E1.SetCellFontStyle('Bold','A1','D3')
    E1.SetCellFontStyle('Bold','A4','A7')

    local.DrawBox('A1','D1','A1','D1',color:Silver)
    local.DrawBox('A3','D3','A7','D7',color:Silver)

    E1.SetCellBackgroundColor(color:Silver,'A1','D1')
    E1.SetCellBackgroundColor(color:Silver,'A3','D7')

    E1.SetCellFontName('Tahoma','A1','D7')



getnextrecord2      routine !Progress Window Routines (DBH: 22-03-2004)
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress >= 100
        recordsprocessed        = 0
        percentprogress         = 0
        progress:thermometer    = 0
        recordsthiscycle        = 0
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    !0{prop:Text} = ?progress:pcttext{prop:text}
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                Yield()
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case Missive('Do you want to finish off building the excel document with the data you have compiled so far, or just quit now?','ServiceBase 3g',|
                       'mquest.jpg','\Cancel|Quit|/Finish') 
            Of 3 ! Finish Button
                tmp:Cancel = 2
            Of 2 ! Quit Button
                tmp:Cancel = 1
            Of 1 ! Cancel Button
        End ! Case Missive
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    display()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020728'&'0'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('MonthlyWarrantyClaimReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?VSBackButton
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:AUDIT.Open
  Relate:EXCHANGE.Open
  Relate:REPSCHCR.Open
  Relate:WEBJOB.Open
  Access:TRADEACC.UseFile
  Access:JOBSE.UseFile
  Access:JOBS.UseFile
  Access:JOBSWARR.UseFile
  Access:AUDSTATS.UseFile
  Access:USERS.UseFile
  Access:JOBSE2.UseFile
  Access:INVOICE.UseFile
  Access:STDCHRGE.UseFile
  Access:REPSCHCT.UseFile
  Access:REPSCHMA.UseFile
  Access:REPSCHED.UseFile
  Access:EXCHHIST.UseFile
  Access:AUDIT2.UseFile
  SELF.FilesOpened = True
  !Initialize Dates and save Head Account Information (DBH: 22-03-2004)
  
  tmp:StartDate = Deformat('1/' & Month(Today()) & '/' & Year(Today()),@d6)
  tmp:EndDate = Today()
  
  tmp:HeadAccountNumber   = GETINI('BOOKING','HeadAccount',,CLIP(Path()) & '\SB2KDEF.INI')
  
  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
  tra:Account_Number  = tmp:HeadAccountNumber
  If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Found
      tmp:ARCLocation = tra:SiteLocation
      tmp:ARCCompanyName = tra:Company_Name
  Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      !Error
  End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
  
  !Put in user name IF run from ServiceBase (DBH: 25-03-2004)
  pos# = Instring('%',COMMAND(),1,1)
  If pos#
      Access:USERS.Clearkey(use:Password_Key)
      use:Password    = Clip(Sub(COMMAND(),pos#+1,30))
      If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          !Found
          tmp:UserName = Clip(use:Forename) & ' ' & Clip(use:Surname)
      Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          !Error
      End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
  End !pos#
  BRW12.Init(?List,Queue:Browse.ViewPosition,BRW12::View:Browse,Queue:Browse,Relate:CHARTYPE,SELF)
  BRW15.Init(?List:2,Queue:Browse:1.ViewPosition,BRW15::View:Browse,Queue:Browse:1,Relate:MANUFACT,SELF)
  OPEN(window)
  SELF.Opened=True
  ! ================ Set Report Version =====================
  tmp:VersionNumber = 'Version Number: ' & Clip(kVersionNumber) & '.' & kVODR0100
  ?ReportVersion{prop:Text} = tmp:VersionNumber
  ! Automated Report
      if command('/SCHEDULE')
          x# = instring('%',command(),1,1)
          Access:REPSCHED.Clearkey(rpd:RecordNumberKey)
          rpd:RecordNumber    = clip(sub(command(),x# + 1,20))
          if (Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign)
              ! Found
              Access:REPSCHCR.Clearkey(rpc:ReportCriteriaKey)
              rpc:ReportName    = rpd:ReportName
              rpc:ReportCriteriaType    = rpd:ReportCriteriaType
              if (Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign)
                  ! Found
                  tmp:AllManufacturers = rpc:AllManufacturers
                  tmp:AllChargeTypes = rpc:AllChargeTypes
  
                  Access:REPSCHCT.Clearkey(rpr:ChargeTypeKey)
                  rpr:REPSCHCRRecordNumber    = rpc:RecordNumber
                  set(rpr:ChargeTypeKey,rpr:ChargeTypeKey)
                  loop
                      if (Access:REPSCHCT.Next())
                          Break
                      end ! if (Access:REPSCHCT.Next())
                      if (rpr:REPSCHCRRecordNumber    <> rpc:RecordNumber)
                          Break
                      end ! if (rpr:RecordNumberKey    <> rpc:RecordNumber)
                      glo:Pointer2 = rpr:ChargeType
                      add(glo:Queue2)
                  end ! loop
  
                  Access:REPSCHMA.Clearkey(rpu:ManufacturerKey)
                  rpu:REPSCHCRRecordNumber    = rpc:RecordNumber
                  set(rpu:ManufacturerKey,rpu:ManufacturerKey)
                  loop
                      if (Access:REPSCHMA.Next())
                          Break
                      end ! if (Access:REPSCHMA.Next())
                      if (rpu:REPSCHCRRecordNumber    <> rpc:RecordNumber)
                          Break
                      end ! if (rpu:REPSCHCRRecordNumber    <> rpc:RecordNumber)
                      glo:Pointer = rpu:Manufacturer
                      add(glo:Queue)
                  end ! loop
  
                  case rpc:DateRangeType
                  of 1 ! Today Only
                      tmp:StartDate = Today()
                      tmp:EndDate = Today()
                  of 2 ! 1st Of Month
                      tmp:StartDate = Date(Month(Today()),1,Year(Today()))
                      tmp:EndDate = Today()
                  of 3 ! Whole Month
                      tmp:EndDate = Date(Month(Today()),1,Year(Today())) - 1
                      tmp:StartDate = Date(Month(tmp:EndDate),1,Year(tmp:EndDate))
                  end ! case rpc:DateRangeType
  
                  ! Have reused existing fields in the file, rather than adding new ones (DBH: 09/03/2009) #10615
                  tmp:AllWarrantyStatus   = rpc:WarIncAll
                  tmp:Pending             = rpc:WarIncSubmitted
                  tmp:Approved            = rpc:WarIncResubmitted
                  tmp:Query               = rpc:WarIncRejected
                  tmp:OnTechnicalReport   = rpc:WarIncRejectionAcknowledged
                  tmp:Rejected            = rpc:WarIncReconciled
                  tmp:SubmittedToMFTR     = rpc:WarIncIncludeARCRepairedJobs
                  tmp:MFTRPaid            = rpc:WarIncSuppressZeros
                  tmp:DateRangeType       = rpc:WarIncReportOrder
  
              else ! if (Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign)
                  ! Error
              end ! if (Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign)
          else ! if (Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign)
              ! Error
          end ! if (Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign)
          Do Reporting
          post(event:CloseWindow)
      end ! if command('/SCHEDULE')
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
    ?Sheet1{PROP:Wizard} = TRUE
    MaxTabs = ?Sheet1{PROP:NumTabs}
    TabNumber = 1
    Select(?Sheet1,TabNumber)
    DISABLE(?VSBackButton)
    UNHIDE(?Cancel)
    HIDE(?Print)
    ?Print{Prop:Default}=False
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW12.Q &= Queue:Browse
  BRW12.AddSortOrder(,cha:Warranty_Key)
  BRW12.AddRange(cha:Warranty,tmp:Yes)
  BRW12.AddLocator(BRW12::Sort0:Locator)
  BRW12::Sort0:Locator.Init(,cha:Charge_Type,1,BRW12)
  BIND('tag:ChargeType',tag:ChargeType)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW12.AddField(tag:ChargeType,BRW12.Q.tag:ChargeType)
  BRW12.AddField(cha:Charge_Type,BRW12.Q.cha:Charge_Type)
  BRW12.AddField(cha:Warranty,BRW12.Q.cha:Warranty)
  BRW15.Q &= Queue:Browse:1
  BRW15.AddSortOrder(,man:Manufacturer_Key)
  BRW15.AddLocator(BRW15::Sort0:Locator)
  BRW15::Sort0:Locator.Init(,man:Manufacturer,1,BRW15)
  BIND('tag:Manufacturer',tag:Manufacturer)
  ?List:2{PROP:IconList,1} = '~notick1.ico'
  ?List:2{PROP:IconList,2} = '~tick1.ico'
  BRW15.AddField(tag:Manufacturer,BRW15.Q.tag:Manufacturer)
  BRW15.AddField(man:Manufacturer,BRW15.Q.man:Manufacturer)
  BRW15.AddField(man:RecordNumber,BRW15.Q.man:RecordNumber)
  IF ?tmp:AllChargeTypes{Prop:Checked} = True
    DISABLE(?Group:ChargeTypes)
  END
  IF ?tmp:AllChargeTypes{Prop:Checked} = False
    ENABLE(?Group:ChargeTypes)
  END
  IF ?tmp:AllManufacturers{Prop:Checked} = True
    DISABLE(?Group:Manufacturers)
  END
  IF ?tmp:AllManufacturers{Prop:Checked} = False
    ENABLE(?Group:Manufacturers)
  END
  IF ?tmp:AllWarrantyStatus{Prop:Checked} = True
    DISABLE(?Group:WarrantyStatus)
  END
  IF ?tmp:AllWarrantyStatus{Prop:Checked} = False
    ENABLE(?Group:WarrantyStatus)
  END
  BRW12.AddToolbarTarget(Toolbar)
  BRW15.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List{Prop:Alrt,239} = SpaceKey
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  ?DASSHOWTAG:2{PROP:Text} = 'Show All'
  ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List:2{Prop:Alrt,239} = SpaceKey
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:AUDIT.Close
    Relate:EXCHANGE.Close
    Relate:REPSCHCR.Close
    Relate:WEBJOB.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?VSBackButton
      ThisWindow.Update
      BRW12.ResetSort(1)
      BRW15.ResetSort(1)
      TabNumber-=1
      CASE TabNumber
      OF 1
         HIDE(?Print)
         ?Print{Prop:Default}=False
         ?VSNextButton{Prop:Default}=True
         DISABLE(?VSBackButton)
         SELECT(?Sheet1,TabNumber)
      OF MaxTabs
         UNHIDE(?Cancel)
         DISABLE(?VSNextButton)
         SELECT(?Sheet1,TabNumber)
      ELSE
         HIDE(?Print)
         ?Print{Prop:Default}=False
         ?VSNextButton{Prop:Default}=True
         ENABLE(?VSBackButton)
         ENABLE(?VSNextButton)
         SELECT(?Sheet1,TabNumber)
      END
      DISPLAY
    OF ?VSNextButton
      ThisWindow.Update
      BRW12.ResetSort(1)
      BRW15.ResetSort(1)
      TabNumber+=1
      CASE TabNumber
      OF 1
         HIDE(?Print)
         ?Print{Prop:Default}=False
         ?VSNextButton{Prop:Default}=True
         DISABLE(?VSBackButton)
         SELECT(?Sheet1,TabNumber)
      OF MaxTabs
         UNHIDE(?Print)
         ?Print{Prop:Default}=True
         ?VSNextButton{Prop:Default}=False
         DISABLE(?VSNextButton)
         SELECT(?Sheet1,TabNumber)
      ELSE
         HIDE(?Print)
         ?Print{Prop:Default}=False
         ?VSNextButton{Prop:Default}=True
         ENABLE(?VSBackButton)
         ENABLE(?VSNextButton)
         SELECT(?Sheet1,TabNumber)
      END
      DISPLAY
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020728'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020728'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020728'&'0')
      ***
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?tmp:AllChargeTypes
      IF ?tmp:AllChargeTypes{Prop:Checked} = True
        DISABLE(?Group:ChargeTypes)
      END
      IF ?tmp:AllChargeTypes{Prop:Checked} = False
        ENABLE(?Group:ChargeTypes)
      END
      ThisWindow.Reset
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::16:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::16:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::16:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?tmp:AllManufacturers
      IF ?tmp:AllManufacturers{Prop:Checked} = True
        DISABLE(?Group:Manufacturers)
      END
      IF ?tmp:AllManufacturers{Prop:Checked} = False
        ENABLE(?Group:Manufacturers)
      END
      ThisWindow.Reset
    OF ?DASREVTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::16:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::16:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?tmp:AllWarrantyStatus
      IF ?tmp:AllWarrantyStatus{Prop:Checked} = True
        DISABLE(?Group:WarrantyStatus)
      END
      IF ?tmp:AllWarrantyStatus{Prop:Checked} = False
        ENABLE(?Group:WarrantyStatus)
      END
      ThisWindow.Reset
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Print
      ThisWindow.Update
      Do Reporting
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeEvent()
    E1.TakeEvent ('', '')
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?List:2
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::13:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:2
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:2{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:2{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::16:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:2)
               ?List:2{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

local.WriteLine         Procedure()
local:DateCompleted     Date()
Costs               Group(),Pre()
local:HandlingFee       Real
local:PartsSelling      Real
local:Labour            Real
local:VAT               Real
local:Total             Real
local:Parts             Real
locExchangePrice        REAL
                    End
locPartTotal        REAL
qParts              QUEUE(),PRE(qp)
PartNumber              STRING(30)
Description             STRING(30)
Quantity                LONG
PartCost                REAL
                    END

Code
    Do PrepareFaultCodes    ! Load group to lookup in/out fault

    Clear(exp:Record)
    ! SB Job No
    exp:Line1    = '"' & job:Ref_Number
    ! Franchise Branch ID
    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
    tra:Account_Number    = wob:HeadAccountNumber
    if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
        ! Found
        exp:Line1 = clip(exp:Line1) & '","' & tra:BranchIdentification
    else ! if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
        ! Error
        exp:Line1 = clip(exp:Line1) & '","??'
    end ! if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
    ! Completed
    local:DateCompleted = job:Date_Completed
    Access:AUDSTATS.Clearkey(aus:RefDateRecordKey)
    aus:RefNumber    = job:Ref_Number
    aus:DateChanged    = 0
    set(aus:RefDateRecordKey,aus:RefDateRecordKey)
    loop
        if (Access:AUDSTATS.Next())
            Break
        end ! if (Access:AUDSTATS.Next())
        if (aus:RefNumber    <> job:Ref_Number)
            Break
        end ! if (aus:RefNumber    <> job:Ref_Number)
        if (sub(aus:NewStatus,1,3) = '705' Or sub(aus:NewStatus,1,3) = '710')
            if (aus:DateChanged > local:DateCompleted)
                local:DateCompleted = aus:DateChanged
            end !@ if (aus:DateChanged > local:DateCompleted)
        end ! if (sub(aus:NewStatus,1,3) = '705' Or sub(aus:NewStatus,1,3) = '710')
    end ! loop
    exp:Line1 = clip(exp:Line1) & '","' & format(local:DateCompleted,@d06b)
    ! Claim Submission Date
    if (jow:ClaimSubmitted = 0)
        exp:Line1 = clip(exp:Line1) & '","' & format(job:Date_Completed,@d06b)
    else ! if (jow:ClaimSubmitted = 0)
        exp:Line1 = clip(exp:Line1) & '","' & format(jow:ClaimSubmitted,@d06b)
    end ! if (jow:ClaimSubmitted = 0)
    ! Head Account No
    exp:Line1 = clip(exp:Line1) & '","' & tra:Account_Number
    ! Head Account Name
    exp:Line1 = clip(exp:Line1) & '","' & BHStripReplace(tra:Company_Name,'","',' ')
    ! Manufacturer
    exp:Line1 = clip(exp:Line1) & '","' & job:Manufacturer
    ! Model Number
    exp:Line1 = clip(exp:Line1) & '","' & job:Model_Number
    ! Warranty Charge Type
    exp:Line1 = clip(exp:Line1) & '","' & job:Warranty_Charge_Type
    ! Warranty Repair Type
    exp:Line1 = clip(exp:Line1) & '","' & job:Repair_Type_Warranty
    ! Engineer
    Access:USERS.Clearkey(use:User_Code_Key)
    use:User_Code    = job:Engineer
    if (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
        ! Found
        exp:Line1 = clip(exp:Line1) & '","' & BHStripReplace(Clip(use:Forename) & ' ' & Clip(use:Surname),'","',' ')
    else ! if (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
        ! Error
        exp:Line1 = clip(exp:Line1) & '","'
    end ! if (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
    
    ! Repair
    exp:Line1 = Clip(exp:Line1) & '","' & jow:RepairedAt
    ! Exchange
    if (job:Exchange_Unit_Number > 0)
        exp:Line1 = clip(exp:Line1) & '","YES'
    else ! if (job:Exchange_Unit_Number > 0)
        exp:Line1 = clip(exp:Line1) & '","NO'
    end ! if (job:Exchange_Unit_Number > 0)
    ! Exchange Unit IMEI Number
    locExchangePrice = 0
    if (job:Exchange_Unit_Number > 0)
        Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
        xch:Ref_Number    = job:Exchange_Unit_Number
        if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
            ! Found
            exp:Line1 = clip(exp:Line1) & '","''' & xch:ESN

            IF (jobe:ExchangedAtRRC = 1)  ! #12228 Get exchange price if attached at RRC (Bryan: 19/08/2011)
                Access:EXCHHIST.Clearkey(exh:Ref_Number_Key)
                exh:Ref_Number = xch:Ref_Number
                exh:Date        = TODAY()
                SET(exh:Ref_Number_Key,exh:Ref_Number_Key)
                LOOP UNTIL Access:EXCHHIST.Next()
                    IF (exh:Ref_Number <> xch:Ref_Number)
                        BREAK
                    END
                    IF (exh:Status = 'IN TRANSIT')
                        priceStart# = INSTRING('PRICE',exh:Notes,1,1)
                        IF (priceStart# > 0)
                            locExchangePrice = DEFORMAT(CLIP(SUB(exh:Notes,priceStart# + 8,30)),@n14.2)
                            BREAK
                        END
                    END
                END
            END
        else ! if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
            ! Error
            exp:Line1 = clip(exp:Line1) & '","'
        end ! if (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign)
    else ! if (job:Exchange_Unit_Number > 0)
        exp:Line1 = clip(exp:Line1) & '","'
    end ! if (job:Exchange_Unit_Number > 0)
    ! Original IMEI    ! #12228 New Column (Bryan: 19/08/2011)
    exp:Line1 = CLIP(exp:Line1) & '","''' & job:ESN
    ! Exchange/Handling Fee ! #12228 New Column (Bryan: 19/08/2011)
    IF (SentToHub(job:Ref_Number) AND jobe:WebJob)
        IF (job:Invoice_NUmber <> 0)
            IF (job:Exchange_Unit_Number > 0)
                exp:Line1 = CLIP(exp:Line1) & '","' & Format(jobe:InvoiceExchangeRate,@n_14.2)
            ELSE
                exp:Line1 = CLIP(exp:Line1) & '","' & Format(jobe:InvoiceHandlingFee,@n_14.2)
            END
        ELSE
            IF (job:Exchange_Unit_Number > 0)
                exp:Line1 = CLIP(exp:Line1) & '","' & Format(jobe:ExchangeRate,@n_14.2)
            ELSE
                exp:Line1 = CLIP(exp:Line1) & '","' & Format(jobe:HandlingFee,@n_14.2)
            END
        END
    ELSE ! IF (SentToHub(job:Ref_Number) AND jobe:WebJob)
        exp:Line1 = clip(exp:Line1) & '","'
    END ! IF (SentToHub(job:Ref_Number) AND jobe:WebJob)
    ! Exchange Purchase Price   ! #12228 New Column (Bryan: 19/08/2011)
    IF (locExchangePrice > 0)
        exp:Line1 = CLIP(exp:Line1) & '","' & Format(locExchangePrice,@n_14.2)
    ELSE
        exp:Line1 = clip(exp:Line1) & '","'
    END
    ! RRC Parts Cost
    Clear(Costs)
    If jow:RepairedAT = 'RRC'
! Delete --- Don't use invoice costs for RRC side (DBH: 14/01/2010) #11260
!        If job:Invoice_Number_Warranty <> 0
!            Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
!            inv:Invoice_Number  = job:Invoice_Number_Warranty
!            If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
!                !Found
!                local:PartsSelling    = jobe:InvRRCWPartsCost
!                local:Labour   = jobe:InvRRCWLabourCost
!                local:VAT      = jobe:InvRRCWPartsCost * inv:Vat_Rate_Parts/100 + |
!                                    jobe:InvRRCWLabourCost * inv:Vat_Rate_Labour/100 +|
!                                    job:WInvoice_Courier_Cost * inv:Vat_Rate_Labour/100
!                local:Total    = local:PartsSelling + local:Labour + local:VAT + job:WInvoice_Courier_Cost
!            Else ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
!                !Error
!            End !If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
!
!        Else !If job:Invoice_Number_Warranty <> 0
! end --- (DBH: 14/01/2010) #11260
            local:PartsSelling    = jobe:RRCWPartsCost
            local:Labour   = jobe:RRCWLabourCost
            local:VAT      = jobe:RRCWPartsCost * VatRate(job:Account_Number,'L')/100 + |
                                jobe:RRCWLabourCost * VatRate(job:Account_Number,'P')/100 + |
                                job:Courier_Cost_Warranty * VatRate(job:Account_Number,'L')/100
            local:Total    = local:PartsSelling + local:Labour + local:VAT + job:Courier_Cost_Warranty
!        End !If job:Invoice_Number_Warranty <> 0
    Else ! If jow:RepairedAT = 'RRC'
        If job:Invoice_Number_Warranty > 0
            Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
            inv:Invoice_Number = job:Invoice_Number_Warranty
            If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
                !Found
                local:PartsSelling = job:WInvoice_Parts_Cost
                local:Labour = jobe:InvoiceClaimValue
                local:VAT = job:WInvoice_Parts_Cost * inv:Vat_Rate_Labour/100 + |
                                        jobe:InvoiceClaimValue * inv:Vat_Rate_Parts/100 + |
                                        job:WInvoice_Courier_Cost * inv:Vat_Rate_Labour/100
                local:Total    = local:PartsSelling + local:Labour + local:VAT + job:WInvoice_Courier_Cost
            Else ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
                !Error
            End ! If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign

        Else ! If job:Invoice_Number_Warranty > 0
            local:PartsSelling    = job:Parts_Cost_Warranty
            local:Labour   = jobe:ClaimValue
            local:VAT      = job:Parts_Cost_Warranty * VatRate(job:Account_Number,'L')/100 + |
                                jobe:ClaimValue * VatRate(job:Account_Number,'P')/100 + |
                                job:Courier_Cost_Warranty * VatRate(job:Account_Number,'L')/100
            local:Total    = local:PartsSelling + local:Labour + local:VAT + job:Courier_Cost_Warranty
        End ! If job:Invoice_Number_Warranty > 0

    End ! If jow:RepairedAT = 'RRC'

    Access:WARPARTS.Clearkey(wpr:Part_Number_Key)
    wpr:Ref_Number = job:Ref_Number
    Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
    Loop ! Begin Loop
        If Access:WARPARTS.Next()
            Break
        End ! If Access:WARPARTS.Next()
        If wpr:Ref_Number <> job:Ref_Number
            Break
        End ! If wpr:Ref_Number <> job:Ref_Number
        If wpr:Correction
            Cycle
        End ! If wpr:Correction
        If jow:RepairedAT = 'RRC'
            locPartTotal = (wpr:RRCAveragePurchaseCost * wpr:Quantity)
        Else ! If jow:RepairedAT = 'RRC'
            locPartTotal = (wpr:AveragePurchaseCost * wpr:Quantity)
        End ! If jow:RepairedAT = 'RRC'

        local:Parts += locPartTotal
        ! #12228 Add parts to q to add later. I'll assume they don't want exchanges adding (Bryan: 19/08/2011)
        IF (wpr:Part_Number = 'EXCH')
            CYCLE
        END
        qParts.PartNumber = wpr:Part_Number
        qParts.Description = wpr:Description
        qParts.Quantity = wpr:Quantity
        qParts.PartCost = locPartTotal
        Add(qParts)

    End ! Loop

    if (jow:RepairedAT = 'ARC')
        exp:Line1 = clip(exp:Line1) & '",,,,"'
    end ! if (jow:RepairAT = 'RRC')

    ! Parts Cost
    exp:Line1   = Clip(exp:Line1) & '","' & Format(local:Parts,@n_10.2)
    ! Parts Selling Cost
    exp:Line1   = Clip(exp:Line1) & '","' & Format(local:PartsSelling,@n_10.2)
    ! Labour Cost
    exp:Line1   = Clip(exp:Line1) & '","' & Format(local:Labour,@n_10.2)
    ! Sub Total
    exp:Line1   = Clip(exp:Line1) & '","' & Format(local:Labour + local:PartsSelling,@n_10.2)
    ! ARC Parts Cost
    ! ARC Parts Sale
    ! ARC Labour
    ! ARC Sub Total
    if (jow:RepairedAT = 'RRC')
        exp:Line1 = clip(exp:Line1) & '",,,,"'
    end ! if (jow:RepairAT = 'RRC')
    ! Billed MFTR Labour
    ! Billed MFTR Parts (Will asssume this is missing)
    ! Billed MFTR Sub Total
    Access:STDCHRGE.Clearkey(sta:Model_Number_Charge_Key)
    sta:Model_Number    = job:Model_Number
    sta:Charge_Type    = job:Warranty_Charge_Type
    sta:Unit_Type    = job:Unit_Type
    sta:Repair_Type    = job:Repair_Type_Warranty
    if (Access:STDCHRGE.TryFetch(sta:Model_Number_Charge_Key) = Level:Benign)
        ! Found
        exp:Line1   = Clip(exp:Line1) & '","' & Format(sta:WarrantyCLaimRate,@n_14.2)
        exp:Line1   = Clip(exp:Line1) & '","' & Format(jobe:ClaimPartsCost,@n_14.2)
        exp:Line1   = Clip(exp:Line1) & '","' & Format(sta:WarrantyClaimRate + jobe:CLaimPartsCost,@n_14.2)
    else ! if (Access:STDCHRGE.TryFetch(sta:Model_Number_Charge_Key) = Level:Benign)
        ! Error
        exp:Line1   = Clip(exp:Line1) & '","'
        exp:Line1   = Clip(exp:Line1) & '","'
        exp:Line1   = Clip(exp:Line1) & '","'
    end ! if (Access:STDCHRGE.TryFetch(sta:Model_Number_Charge_Key) = Level:Benign)

    ! Accepted Date
    exp:Line1   = Clip(exp:Line1) & '","' & format(jow:DateAccepted,@d06b)
    ! MFTR Parts
    exp:Line1   = Clip(exp:Line1) & '","' & Format(jobe2:WPartsPaid,@N_10.2)
    ! MFTR Labour
    exp:Line1   = Clip(exp:Line1) & '","' & Format(jobe2:WLabourPaid,@N_10.2)
    ! MFTR Sub Total
    exp:Line1   = Clip(exp:Line1) & '","' & Format(jobe2:WSubTotal,@N_10.2)
    ! MFTR Vat
    exp:Line1   = Clip(exp:Line1) & '","' & Format(jobe2:WVat,@N_10.2)
    ! MFTR Total
    exp:Line1   = Clip(exp:Line1) & '","' & Format(jobe2:WTotal,@N_10.2)
    ! In Fault
    exp:Line1   = CLIP(exp:Line1) & '","' & GetInFault(gFaultCodesGroup)
    ! Out Fault
    exp:Line1   = CLIP(exp:Line1) & '","' & GetOutFault(gFaultCodesGroup)
    ! Reconciled Date
    exp:Line1   = Clip(exp:Line1) & '","' & format(jow:DateReconciled,@d06b)
    ! Warranty Status
    exp:Line1   = clip(exp:Line1) & '","' & jobe:WarrantyClaimStatus
    ! Rejection Date 1
    ! Rejection Reason 1
    ! Rejection Date 2
    ! Rejection Reason 2
    count# = 0
    Access:AUDIT.Clearkey(aud:TypeActionKey)
    aud:Ref_Number    = job:Ref_Number
    aud:Type    = 'JOB'
    aud:Action    = 'WARRANTY CLAIM REJECTED'
    set(aud:TypeActionKey,aud:TypeActionKey)
    loop
        if (Access:AUDIT.Next())
            Break
        end ! if (Access:AUDIT.Next())
        if (aud:Ref_Number    <> job:Ref_Number)
            Break
        end ! if (aud:Ref_Number    <> job:Ref_Number)
        if (aud:Type    <> 'JOB')
            Break
        end ! if (aud:Type    <> 'JOB')
        if (aud:Action    <> 'WARRANTY CLAIM REJECTED')
            Break
        end ! if (aud:Action    <> 'WARRANTY CLAIM REJECTED')
        count# += 1
        if (count# > 2)
            break
        end ! if (count# > 2)
        exp:Line1   = Clip(exp:Line1) & '","' & Format(aud:Date,@d06b)

        Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
        aud2:AUDRecordNumber = aud:Record_Number
        IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)

        END ! IF
        exp:Line1   = Clip(exp:Line1) & '","' & Clip(BHStripNonAlphaNum(BHStripReplace(Sub(aud2:Notes,9,255),',',''),' '))
    end ! loop
    if (count# = 0)
        exp:Line1   = Clip(exp:Line1) & '","'
        exp:Line1   = Clip(exp:Line1) & '","'
        exp:Line1   = Clip(exp:Line1) & '","'
        exp:Line1   = Clip(exp:Line1) & '","'
    elsif (count# = 1)
        exp:Line1   = Clip(exp:Line1) & '","'
        exp:Line1   = Clip(exp:Line1) & '","'
    end ! if (count# = 0)
    ! Final Rejection Date
    ! Final Rejection Reason
    ! Rejection Accepted By
    found# = 0
    Access:AUDIT.Clearkey(aud:TypeActionKey)
    aud:Ref_Number    = job:Ref_Number
    aud:Type    = 'JOB'
    aud:Action    = 'WARRANTY CLAIM FINAL REJECTION'
    set(aud:TypeActionKey,aud:TypeActionKey)
    loop
        if (Access:AUDIT.Next())
            Break
        end ! if (Access:AUDIT.Next())
        if (aud:Ref_Number    <> job:Ref_Number)
            Break
        end ! if (aud:Ref_Number    <> job:Ref_Number)
        if (aud:Type    <> 'JOB')
            Break
        end ! if (aud:Type    <> 'JOB')
        if (aud:Action    <> 'WARRANTY CLAIM FINAL REJECTION')
            Break
        end ! if (aud:Action    <> 'WARRANTY CLAIM FINAL REJECTION')

        Access:AUDIT2.ClearKey(aud2:AUDRecordNumberKey)
        aud2:AUDRecordNumber = aud:Record_Number
        IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)

        END ! IF

        Access:USERS.Clearkey(use:User_Code_Key)
        use:User_Code    = aud:User
        if (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
            ! Found
            exp:Line1   = Clip(exp:Line1) & '","' & Format(aud:Date,@d06b)
            exp:Line1   = Clip(exp:Line1) & '","' & Clip(BHStripNonAlphaNum(BHStripReplace(Sub(aud2:Notes,9,255),',',''),' '))
            exp:Line1   = Clip(exp:Line1) & '","' & Clip(use:Forename) & ' ' & Clip(use:Surname)
            found# = 1
            break
        else ! if (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
            ! Error
        end ! if (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
    end ! loop
    if (found# = 0)
        exp:Line1   = Clip(exp:Line1) & '","'
        exp:Line1   = Clip(exp:Line1) & '","'
        exp:Line1   = Clip(exp:Line1) & '","'
    end ! if (found# = 0)

    ! #12228 Add remaining parts at the end (DBH: 17/02/2012)
    LOOP pp# = 1 TO RECORDS(qParts)
        GET(qParts,pp#)
        IF (ERROR())
            BREAK
        END
        ! Part Number
        exp:Line1 = CLIP(exp:Line1) & '","' & qParts.PartNumber
        ! Description
        exp:Line1 = CLIP(exp:Line1) & '","' & qParts.Description
        ! Quantity
        exp:Line1 = CLIP(exp:Line1) & '","' & qParts.Quantity
        ! Total Cost
        exp:Line1 = CLIP(exp:Line1) & '","' & Format(qParts.PartCost,@n_14.2)
    END ! LOOP pp# = 1 TO RECORDS(qParts)

    ! Save Total Number Of Parts, so we can draw the column headings later
    IF (RECORDS(qParts) > locTotalParts)
        locTotalParts = RECORDS(qParts)
    END

    exp:Line1   = Clip(exp:Line1) & '"'
    Add(ExportFile)

!    ! #12228 Show remaining parts on seperate lines (Bryan: 19/08/2011)
!    
!    Loop pp# = 2 TO Records(qParts)
!        GET(qParts,pp#)
!        IF (ERROR())
!            BREAK
!        END
!
!        Clear(ExportFile)
!        exp:Line1 = '"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","'
!        exp:Line1   = CLIP(exp:Line1) & '","' & qParts.PartNumber
!        exp:Line1   = CLIP(exp:Line1) & '","' & qParts.Quantity
!        exp:Line1   = CLIP(exp:Line1) & '","' & Format(qParts.PartCost,@n_14.2)
!        exp:Line1   = Clip(exp:Line1) & '"'
!        Add(ExportFile)
!        local:RecordCount += 1
!    END
Local.DrawBox       Procedure(String func:TL,String func:TR,String func:BL,String func:BR,Long func:Colour)
Code
    If func:BR = ''
        func:BR = func:TR
    End !If func:BR = ''

    If func:BL = ''
        func:BL = func:TL
    End !If func:BL = ''

    If func:Colour = 0
        func:Colour = oix:ColorWhite
    End !If func:Colour = ''
    E1.SetCellBackgroundColor(func:Colour,func:TL,func:BR)
    E1.SetCellBorders(func:BL,func:BR,oix:BorderEdgeBottom,oix:LineStyleContinuous)
    E1.SetCellBorders(func:TL,func:TR,oix:BorderEdgeTop,oix:LineStyleContinuous)
    E1.SetCellBorders(func:TL,func:BL,oix:BorderEdgeLeft,oix:LineStyleContinuous)
    E1.SetCellBorders(func:TR,func:BR,oix:BorderEdgeRight,oix:LineStyleContinuous)
Local.UpdateProgressWindow      Procedure(String    func:Text)
Code
    staque:StatusMessage = Clip(func:Text)
    Add(StatusQueue)
    Select(?List1,Records(StatusQueue))
    Display()
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
!---------------------------------------------------------------------------------
E1.Init   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0)
ReturnValue   byte
  CODE
  ReturnValue = PARENT.Init (pStartVisible,pEnableEvents)
  self.TakeSnapShotOfWindowPos()
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.Kill   PROCEDURE (byte pUnloadCOM=1)
ReturnValue   byte
  CODE
  self.RestoreSnapShotOfWindowPos()
  ReturnValue = PARENT.Kill (pUnloadCOM)
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.TakeEvent   PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0)
  CODE
  PARENT.TakeEvent (pEventString1,pEventString2,pEventNumber,pEventType,pEventStatus)
  if pEventType = 0  ! Generated by CapeSoft Office Inside
    case event()
      of event:accepted
        case field()
      end
    end
  end
!Initialise
ExcelSetup          Procedure(Byte      func:Visible)
    Code
    Excel   = Create(0,Create:OLE)
    Excel{prop:Create} = 'Excel.Application'
    Excel{'ASYNC'}  = False
    Excel{'Application.DisplayAlerts'} = False

    Excel{'Application.ScreenUpdating'} = func:Visible
    Excel{'Application.Visible'} = func:Visible
    Excel{'Application.Calculation'} = 0FFFFEFD9h
    excel:ActiveWorkBook    = Excel{'ActiveWorkBook'}
    Yield()

!Make a WorkBook
ExcelMakeWorkBook   Procedure(String    func:Title,String   func:Author,String  func:AppName)
    Code
    Excel{prop:Release} = excel:ActiveWorkBook
    excel:ActiveWorkBook = Excel{'Application.Workbooks.Add()'}

    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Title")'} = func:Title
    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Author")'} = func:Author
    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Application Name")'} = func:AppName

    excel:Selected = Excel{'Sheets("Sheet2").Select'}
    Excel{prop:Release} = excel:Selected

    Excel{'ActiveWindow.SelectedSheets.Delete'}

    excel:Selected = Excel{'Sheets("Sheet1").Select'}
    Excel{prop:Release} = excel:Selected
    Yield()

ExcelMakeSheet      Procedure()
ActiveWorkBook  CString(20)
    Code
    ActiveWorkBook = Excel{'ActiveWorkBook'}

    Excel{ActiveWorkBook & '.Sheets("Sheet3").Select'}
    Excel{prop:Release} = ActiveWorkBook

    Excel{excel:ActiveWorkBook & '.Sheets.Add'}
    Yield()
!Select A Sheet
ExcelSelectSheet    Procedure(String    func:SheetName)
    Code
    Excel{'Sheets("' & Clip(func:SheetName) & '").Select'}
    Yield()
!Setup Sheet Type (P = Portrait, L = Lanscape)
ExcelSheetType      Procedure(String    func:Type)
    Code
    Case func:Type
        Of 'L'
            Excel{'ActiveSheet.PageSetup.Orientation'}  = 2
        Of 'P'
            Excel{'ActiveSheet.PageSetup.Orientation'}  = 1
    End !Case func:Type
    Excel{'ActiveSheet.PageSetup.FitToPagesWide'}  = 1
    Excel{'ActiveSheet.PageSetup.FitToPagesTall'}  = 9999
    Excel{'ActiveSheet.PageSetup.Order'}  = 2

    Yield()
ExcelHorizontal     Procedure(String    func:Direction)
Selection   Cstring(20)
    Code
    Selection = Excel{'Selection'}
    Case func:Direction
        Of 'Centre'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFF4h
        Of 'Left'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFDDh
        Of 'Right'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFC8h
    End !Case tmp:Direction
    Excel{prop:Release} = Selection
    Yield()
ExcelVertical   Procedure(String func:Direction)
Selection   CString(20)
    Code
    Selection = Excel{'Selection'}
    Case func:Direction
        Of 'Top'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFC0h
        Of 'Centre'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFF4h
        Of 'Bottom'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFF5h
    End ! Case func:Direction
    Excel{prop:Release} = Selection
    Yield()

ExcelCell   Procedure(String    func:Text,Byte    func:Bold)
Selection   Cstring(20)
    Code
    Selection = Excel{'Selection'}
    If func:Bold
        Excel{Selection & '.Font.Bold'} = True
    Else
        Excel{Selection & '.Font.Bold'} = False
    End !If func:Bold
    Excel{prop:Release} = Selection

    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.Formula'}  = func:Text
    Excel{excel:Selected & '.Offset(0, 1).Select'}
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelFormatCell     Procedure(String    func:Format)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.NumberFormat'} = func:Format
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelFormatRange    Procedure(String    func:Range,String   func:Format)
Selection       Cstring(20)
    Code

    ExcelSelectRange(func:Range)
    Selection   = Excel{'Selection'}
    Excel{Selection & '.NumberFormat'} = func:Format
    Excel{prop:Release} = Selection
    Yield()

ExcelNewLine    Procedure(Long func:Number)
    Code
    Loop excelloop# = 1 to func:Number
        ExcelSelectRange('A' & (ExcelCurrentRow() + 1))
    End !Loop excelloop# = 1 to func:Number
    !excel:Selected = Excel{'ActiveCell'}
    !Excel{excel:Selected & '.Offset(0, -' & Excel{excel:Selected & '.Column'} - 1 & ').Select'}
    !Excel{excel:Selected & '.Offset(1, 0).Select'}
    !Excel{prop:Release} = excel:Selected
    Yield()

ExcelMoveDown   Procedure()
    Code
    ExcelSelectRange(ExcelCurrentColumn() & (ExcelCurrentRow() + 1))
    Yield()
!Set Column Width

ExcelColumnWidth        Procedure(String    func:Range,Long   func:Width)
    Code
    Excel{'ActiveSheet.Columns("' & Clip(func:Range) & '").ColumnWidth'} = func:Width
    Yield()
ExcelCellWidth          Procedure(Long  func:Width)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.ColumnWidth'} = func:Width
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelAutoFit            Procedure(String func:Range)
    Code
    Excel{'ActiveSheet.Columns("' & Clip(func:Range) & '").Columns.AutoFit'}
    Yield()
!Set Gray Box

ExcelGrayBox            Procedure(String    func:Range)
Selection   CString(20)
    Code
    Selection = Excel{'Selection'}
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Excel{Selection & '.Interior.ColorIndex'} = 15
    Excel{Selection & '.Interior.Pattern'} = 1
    Excel{Selection & '.Interior.PatternColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(7).LineStyle'} = 1
    Excel{Selection & '.Borders(7).Weight'} = 2
    Excel{Selection & '.Borders(7).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(10).LineStyle'} = 1
    Excel{Selection & '.Borders(10).Weight'} = 2
    Excel{Selection & '.Borders(10).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(8).LineStyle'} = 1
    Excel{Selection & '.Borders(8).Weight'} = 2
    Excel{Selection & '.Borders(8).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(9).LineStyle'} = 1
    Excel{Selection & '.Borders(9).Weight'} = 2
    Excel{Selection & '.Borders(9).ColorIndex'} = 0FFFFEFF7h
    Excel{prop:Release} = Selection
    Yield()
ExcelGrid   Procedure(String    func:Range,Byte  func:Left,Byte  func:Top,Byte   func:Right,Byte func:Bottom,Byte func:Colour)
Selection   Cstring(20)
    Code
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Selection = Excel{'Selection'}
    If func:Colour
        Excel{Selection & '.Interior.ColorIndex'} = func:Colour
        Excel{Selection & '.Interior.Pattern'} = 1
        Excel{Selection & '.Interior.PatternColorIndex'} = 0FFFFEFF7h
    End !If func:Colour
    If func:Left
        Excel{Selection & '.Borders(7).LineStyle'} = 1
        Excel{Selection & '.Borders(7).Weight'} = 2
        Excel{Selection & '.Borders(7).ColorIndex'} = 0FFFFEFF7h
    End !If func:Left

    If func:Right
        Excel{Selection & '.Borders(10).LineStyle'} = 1
        Excel{Selection & '.Borders(10).Weight'} = 2
        Excel{Selection & '.Borders(10).ColorIndex'} = 0FFFFEFF7h
    End !If func:Top

    If func:Top
        Excel{Selection & '.Borders(8).LineStyle'} = 1
        Excel{Selection & '.Borders(8).Weight'} = 2
        Excel{Selection & '.Borders(8).ColorIndex'} = 0FFFFEFF7h
    End !If func:Right

    If func:Bottom
        Excel{Selection & '.Borders(9).LineStyle'} = 1
        Excel{Selection & '.Borders(9).Weight'} = 2
        Excel{Selection & '.Borders(9).ColorIndex'} = 0FFFFEFF7h
    End !If func:Bottom
    Excel{prop:Release} = Selection
    Yield()
!Select a range of cells
ExcelSelectRange        Procedure(String    func:Range)
    Code
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Yield()
!Change font size
ExcelFontSize           Procedure(Byte  func:Size)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.Font.Size'}   = func:Size
    Excel{prop:Release} = excel:Selected
    Yield()
!Sheet Name
ExcelSheetName          Procedure(String    func:Name)
    Code
    Excel{'ActiveSheet.Name'} = func:Name
    Yield()
ExcelAutoFilter         Procedure(String    func:Range)
Selection   Cstring(20)
    Code
    ExcelSelectRange(func:Range)
    Selection = Excel{'Selection'}
    Excel{Selection & '.AutoFilter'}
    Excel{prop:Release} = Selection
    Yield()
ExcelDropAllSheets      Procedure()
    Code
    Excel{prop:Release} = excel:ActiveWorkBook
    Loop While Excel{'WorkBooks.Count'} > 0
        excel:ActiveWorkBook = Excel{'ActiveWorkBook'}
        Excel{'ActiveWorkBook.Close(1)'}
        Excel{prop:Release} = excel:ActiveWorkBook
    End !Loop While Excel{'WorkBooks.Count'} > 0
    Yield()
ExcelClose              Procedure()
!xlCalculationAutomatic
    Code
    Excel{'Application.Calculation'}= 0FFFFEFF7h
    Excel{'Application.Quit'}
    Excel{prop:Deactivate}
    Destroy(Excel)
    Yield()
ExcelDeleteSheet        Procedure(String    func:SheetName)
    Code
    ExcelSelectSheet(func:SheetName)
    Excel{'ActiveWindow.SelectedSheets.Delete'}
    Yield()
ExcelSaveWorkBook       Procedure(String    func:Name)
    Code
    Excel{'Application.ActiveWorkBook.SaveAs("' & LEFT(CLIP(func:Name)) & '")'}
    Excel{'Application.ActiveWorkBook.Close()'}
   Excel{'Application.Calculation'} = 0FFFFEFF7h
    Excel{'Application.Quit'}

    Excel{PROP:DEACTIVATE}
    YIELD()
ExcelFontColour         Procedure(String    func:Range,Long func:Colour)
    Code
    !16 = Gray
    ExcelSelectRange(func:Range)
    Excel{'Selection.Font.ColorIndex'} = func:Colour
    Yield()
ExcelWrapText           Procedure(String    func:Range,Byte func:True)
Selection   Cstring(20)
    Code
    ExcelSelectRange(func:Range)
    Selection = Excel{'Selection'}
    Excel{Selection & '.WrapText'} = func:True
    Excel{prop:Release} = Selection
    Yield()
ExcelCurrentColumn      Procedure()
CurrentColumn   String(20)
    Code
    excel:Selected = Excel{'ActiveCell'}
    CurrentColumn = Excel{excel:Selected & '.Column'}
    Excel{prop:Release} = excel:Selected
    Yield()
    Return CurrentColumn

ExcelCurrentRow         Procedure()
CurrentRow      String(20)
    Code
    excel:Selected = Excel{'ActiveCell'}
    CurrentRow = Excel{excel:Selected & '.Row'}
    Excel{prop:Release} = excel:Selected
    Yield()
    Return CurrentRow

ExcelPasteSpecial       Procedure(String    func:Range)
Selection       CString(20)
    Code
    ExcelSelectRange(func:Range)
    Selection   = Excel{'ActiveCell'}
    Excel{Selection & '.PasteSpecial'}
    Excel{prop:Release} = Selection
    Yield()

ExcelConvertFormula     Procedure(String    func:Formula)
    Code
    Return Excel{'Application.ConvertFormula("' & Clip(func:Formula) & '",' & 0FFFFEFCAh & ',' & 1 & ')'}

ExcelGetFilename        Procedure(Byte  func:DontAsk)
sav:Path        CString(255)
func:Desktop     CString(255)
    Code

        SHGetSpecialFolderPath( GetDesktopWindow(), func:Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
        func:Desktop = Clip(func:Desktop) & '\ServiceBase Export'

        !Does the Export Folder already Exists?
        If ~Exists(Clip(func:Desktop))
            If ~MkDir(func:Desktop)
                Return Level:Fatal

            End !If MkDir(func:Desktop)
        End !If Exists(Clip(tmp:Desktop))

        Error# = 0
        sav:Path = Path()
        SetPath(func:Desktop)

        func:Desktop = Clip(func:Desktop) & '\' & CLIP(Excel:ProgramName) & ' ' & FORMAT(TODAY(), @D12) & '.xls'

        If func:DontAsk = False
            IF NOT FILEDIALOG('Save Spreadsheet', func:Desktop, 'Microsoft Excel Workbook|*.XLS', |
                FILE:KeepDir + FILE:Save + FILE:NoError + FILE:LongName)
                Error# = 1
            End!IF NOT FILEDIALOG('Save Spreadsheet', tmp:Desktop, 'Microsoft Excel Workbook|*.XLS', |
        End !If func:DontAsk = True

        SetPath(sav:Path)

        If Error#
            Return Level:Fatal
        End !If Error#
        excel:FileName    = func:Desktop
        Return Level:Benign

ExcelGetDirectory       Procedure()
sav:Path        CString(255)
func:Desktop    CString(255)
    Code
        SHGetSpecialFolderPath( GetDesktopWindow(), func:Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
        func:Desktop = Clip(func:Desktop) & '\ServiceBase Export\'
        !Does the Export Folder already Exists?
        Error# = 0
        If ~Exists(Clip(func:Desktop))
            If ~MkDir(func:Desktop)
                If Not FileDialog('Save Spreadsheet To Folder', func:Desktop, ,FILE:KeepDir+ File:Save + File:NoError + File:LongName + File:Directory)
                    Return Level:Fatal
                End !+ File:LongName + File:Directory)
            End !If MkDir(func:Desktop)
        End !If Exists(Clip(tmp:Desktop))

        excel:FileName  = func:Desktop
        Return Level:Benign

ExcelColumnLetter     Procedure(Long func:ColumnNumber)
local:Over26        Long()
Code
    local:Over26 = 0
    If func:ColumnNumber > 26
        Loop Until func:ColumnNumber <= 26
            local:Over26 += 1
            func:ColumnNumber -= 26
        End !Loop Until ColumnNumber <= 26.
    End !If func:ColumnNumber > 26

    If local:Over26 > 26
        Stop('ExcelColumnLetter Procedure Out Of Range!')
    End !If local:Over26 > 26
    If local:Over26 > 0
        Return Clip(CHR(local:Over26 + 64)) & Clip(CHR(func:ColumnNumber + 64))
    Else !If local:Over26 > 0
        Return Clip(CHR(func:ColumnNumber + 64))
    End !If local:Over26 > 0
ExcelOpenDoc        Procedure(String func:FileName)
Code
    Excel{'Workbooks.Open("' & Clip(func:FileName) & '")'}
ExcelFreeze         Procedure(String func:Cell)
Code
    Excel{'Range("' & Clip(func:Cell) & '").Select'}
    Excel{'ActiveWindow.FreezePanes'} = True

BRW12.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = cha:Charge_Type
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tag:ChargeType = ''
    ELSE
      tag:ChargeType = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tag:ChargeType = '*')
    SELF.Q.tag:ChargeType_Icon = 2
  ELSE
    SELF.Q.tag:ChargeType_Icon = 1
  END


BRW12.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW12.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW12.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW12::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW12::RecordStatus=ReturnValue
  IF BRW12::RecordStatus NOT=Record:OK THEN RETURN BRW12::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = cha:Charge_Type
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::13:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW12::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW12::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW12::RecordStatus
  RETURN ReturnValue


BRW15.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = man:Manufacturer
     GET(glo:Queue2,glo:Queue2.Pointer2)
    IF ERRORCODE()
      tag:Manufacturer = ''
    ELSE
      tag:Manufacturer = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tag:Manufacturer)
    SELF.Q.tag:Manufacturer_Icon = 2
  ELSE
    SELF.Q.tag:Manufacturer_Icon = 1
  END


BRW15.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW15.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW15.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW15::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW15::RecordStatus=ReturnValue
  IF BRW15::RecordStatus NOT=Record:OK THEN RETURN BRW15::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = man:Manufacturer
     GET(glo:Queue2,glo:Queue2.Pointer2)
    EXECUTE DASBRW::16:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW15::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW15::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW15::RecordStatus
  RETURN ReturnValue

Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Window
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
