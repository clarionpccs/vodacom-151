

   MEMBER('connect.clw')                              ! This is a MEMBER module

                     MAP
                       INCLUDE('CONNE009.INC'),ONCE        !Local module procedure declarations
                     END


print_local_invoice  PROCEDURE                        ! Declare Procedure
Tmp:DefaultHeadAccount STRING(30)
Parameter_Group      GROUP,PRE(param)
JobNumber            LONG
Username             STRING(3)
                     END
Local_Group          GROUP,PRE(LOC)
Account_Name         STRING(100)
AccountNumber        STRING(15)
ApplicationName      STRING('ServiceBase 2000')
BatchNumber          LONG
CommentText          STRING(100)
DesktopPath          STRING(255)
FileName             STRING(255)
Path                 STRING(255)
ProgramName          STRING(100)
SectionName          STRING(100)
Text                 STRING(255)
UserCode             STRING(3)
UserName             STRING(100)
                     END
Excel_Group          GROUP,PRE()
Excel                SIGNED !OLE Automation holder
excel:ColumnName     STRING(100)
excel:ColumnWidth    REAL
excel:DateFormat     STRING('dd mmm yyyy')
excel:OperatingSystem REAL
Excel:Visible        BYTE(0)
                     END
Misc_Group           GROUP,PRE()
AccountValue         STRING(15)
AccountTag           STRING(1)
AccountCount         LONG
AccountChange        BYTE
debug:Count          LONG
debug:Active         LONG
OPTION1              SHORT
RecordCount          LONG
Result               BYTE
Save_Job_ID          ULONG,AUTO
SAVEPATH             STRING(255)
TotalCharacters      LONG
                     END
LOC:HeadAccountNumber STRING(15)
HeadAccountQueue     QUEUE,PRE(hq)
HeadAccountNumber    STRING(15)
HeadAccountName      STRING(30)
JobsBooked           LONG
                     END
LOC:SubAccountNumber STRING(15)
SubAccountQueue      QUEUE,PRE(sq)
HeadAccountNumber    STRING(15)
SubAccountNumber     STRING(15)
SubAccountName       STRING(30)
JobsBooked           LONG
                     END
Progress_Group       GROUP,PRE(progress)
Text                 STRING(100)
NextCancelCheck      TIME
Count                LONG
CancelPressed        LONG
                     END
Sheet_Group          GROUP,PRE(sheet)
TempLastCol          STRING(1)
HeadLastCol          STRING('E')
DataLastCol          STRING('S')
HeadSummaryRow       LONG(9)
DataSectionRow       LONG(9)
DataHeaderRow        LONG(11)
                     END
Clipboard_Group      GROUP,PRE(clip)
OriginalValue        STRING(255)
Saved                BYTE
Value                CSTRING(2048)
                     END
HeaderQueue          QUEUE,PRE(head)
ColumnName           STRING(30)
ColumnWidth          REAL
NumberFormat         STRING(20)
                     END
HTML_Queue           QUEUE,PRE(html)
CharPos              LONG
Length               LONG
LINE                 STRING(255)
                     END
NetTalk_Group        GROUP,PRE()
EmailServer          STRING(80)
EmailPort            USHORT
EmailFrom            STRING(255)
EmailTo              STRING(1024)
EmailSubject         STRING(255)
EmailCC              STRING(1024)
EmailFileList        STRING(1024)
EmailBCC             STRING(1024)
EmailMessageText     STRING(16384)
                     END
PrinterinfoGrp       GROUP(PRINTER_INFO_5),PRE()
                     END
DocInfoGrp           GROUP(DOC_INFO_1),PRE()
                     END
Printer_Group        GROUP,PRE(prn)
PageLength           LONG(48)
DetailRows           LONG(14)
Local_Printer_Name   STRING(80)
hPrinter             UNSIGNED
szPrinterName        CSTRING(260)
dwBytesCopied        LONG
dwWritten            LONG
szBuffer             CSTRING(30000)
szDocName            CSTRING(80)
szDataType           CSTRING(80)
                     END
BlankLine            STRING(80)
AddressLine          GROUP,PRE(address),OVER(BlankLine)
Delivery             STRING(30)
Filler1              STRING(4)
Business             STRING(30)
                     END
OrderLine            GROUP,PRE(order),OVER(BlankLine)
DespatchDate         STRING(10)
Filler1              STRING(4)
Filler2              STRING(7)
InvoiceNumber        STRING(10)
Filler4              STRING(1)
AccountNumber        STRING(12)
Filler3              STRING(1)
YourOrderNumber      STRING(20)
                     END
IMEILine             GROUP,PRE(imei),OVER(BlankLine)
Make                 STRING(14)
Model                STRING(15)
SerialNumber         STRING(23)
IMEINumber           STRING(20)
                     END
NarrativeLine        GROUP,PRE(narrative),OVER(BlankLine)
Text1                STRING(55)
Filler1              STRING(10)
Amount               STRING(10)
                     END
DateLine             GROUP,PRE(date),OVER(BlankLine)
Filler0              STRING(10)
DateOut              STRING(10)
Filler1              STRING(22)
TimeOut              STRING(10)
Filler2              STRING(5)
Engineer             STRING(3)
                     END
Detail_Queue         QUEUE,PRE(detail)
Line                 STRING(56)
Filler1              STRING(10)
Amount               STRING(10)
                     END
Address_Group        GROUP,PRE(address)
User_Name            STRING('''INSERT USER NAME'' {12}')
Address_Line1        STRING(30)
Address_Line2        STRING(30)
Address_Line3        STRING(30)
Postcode             STRING(15)
Telephone_Number     STRING(15)
                     END
AddressArray         STRING(30),DIM(2,8)
szHeader             CSTRING(5000)
szFooter             CSTRING(5000)
RowNum               LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
INFILE FILE,DRIVER('ASCII'),PRE(INF),NAME(InvoiceFileName),CREATE
INFILERec RECORD
TextLine  STRING(255)
    end !Record  (Part of infile)
    end !infile
    MAP
OpenPrinter                 PROCEDURE(), STRING ! WHERE '' = SUCCESS, ERROR MESSAGE OTHERWISE
ClosePrinter PROCEDURE()
    END ! MAP
  CODE
! message('In code to print invoice on client')
 InvoiceFileName = clip(LocalSavePath)&'\'&clip(logonid)&'I.TXA'
 x# = 0
 open(infile)
 set(infile)
 loop
    next(infile)
    if error() then break. !end of file assumed
    IF x# = 0
      prn:szBuffer = CLIP(inf:textline)
      x# = 1
    ELSE
      prn:szBuffer = CLIP(prn:szBuffer)&'<13,10>'&CLIP(inf:textline)
    END
 END
 tmp" = OpenPrinter()

!Print

 DO PrintJob
 ClosePrinter()
 Close(infile)
 Remove(infile)
 RETURN
PrintJob                    ROUTINE
    DATA
NumBytes LONG
    CODE
        !------------------------------------------------------------------
        !prn:szBuffer = CLIP(szHeader) & CLIP(prn:szBuffer) & CLIP(szFooter)

!===============================================================!
!
!   Everything is now in the prn:szbuffer
!
!   Used to dump this to printer -
!   now we write to a file, send file to client
!   set printer on the client
!   and then tell the client to print to print the invoice
!
!===============================================================!

        IF NOT StartPagePrinter( prn:hPrinter ) THEN
!            Case MessageEx('Failure reported by "StartPagePrinter()", attempting to continue.' & CLIP(Temp),LOC:ApplicationName,|
!                           'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,200)
!                Of 1 ! &OK Button
!            End!Case MessageEx

            EXIT
        END !IF


        IF NOT WritePrinter( prn:hPrinter, ADDRESS(prn:szBuffer), LEN(prn:szBuffer), NumBytes ) THEN
!            Case MessageEx('Failure reported by "WritePrinter()", attempting to continue.' & CLIP(Temp),LOC:ApplicationName,|
!                           'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,200)
!                Of 1 ! &OK Button
!            End!Case MessageEx

            EXIT
        END !IF

        EndPagePrinter( prn:hPrinter )

        prn:szBuffer = ''
        !------------------------------------------------------------------
    EXIT
OpenPrinter                 PROCEDURE()! STRING WHERE '' = SUCCESS, ERROR MESSAGE OTHERWISE
NumBytes LONG
    CODE
        !------------------------------------------------------------------
        prn:szPrintername = CLIP(Printer{PROPPRINT:Device})

        IF NOT OpenPrinter(prn:szPrinterName, prn:hPrinter, 0) THEN
            RETURN 'Unable to open printer "' & prn:szPrinterName & '"'
        END !IF

        CLEAR(DocInfoGrp)

        prn:szDocName  = CLIP(LOC:ProgramName) ! 'Delivery Note'
        prn:szDataType = 'RAW'

        DocInfoGrp.pDocName    = ADDRESS(prn:szDocName)
        DocInfoGrp.pOutputFile = 0
        DocInfoGrp.pDataType   = ADDRESS(prn:szDataType)

        IF NOT StartDocPrinter( prn:hPrinter, 1, ADDRESS(DocInfoGrp) ) THEN
            RETURN 'Unable to StartDocPrinter()'
        END !IF
        !------------------------------------------------------------------
        RETURN ''
        !------------------------------------------------------------------
ClosePrinter        PROCEDURE()
    CODE
        !-----------------------------------------------------------------
        IF prn:hPrinter <> 0
            ClosePrinter( prn:hPrinter )
        END !IF
        !-----------------------------------------------------------------
