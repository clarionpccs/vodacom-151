  MEMBER('connect.clw')

  INCLUDE('ABFILE.INC'),ONCE

  MAP
CONNEBC0:DctInit    PROCEDURE
CONNEBC0:DctKill    PROCEDURE
CONNEBC0:FilesInit  PROCEDURE
  END

Hide:Access:Labels   CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:Labels   CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:Reports  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:Reports  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:Server   CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:Server   CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:ServerAlias CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:ServerAlias CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

CONNEBC0:DctInit PROCEDURE
  CODE
  Relate:Labels &= Hide:Relate:Labels
  Relate:Reports &= Hide:Relate:Reports
  Relate:Server &= Hide:Relate:Server
  Relate:ServerAlias &= Hide:Relate:ServerAlias

CONNEBC0:FilesInit PROCEDURE
  CODE
  Hide:Relate:Labels.Init
  Hide:Relate:Reports.Init
  Hide:Relate:Server.Init
  Hide:Relate:ServerAlias.Init


CONNEBC0:DctKill PROCEDURE
  CODE
  Hide:Relate:Labels.Kill
  Hide:Relate:Reports.Kill
  Hide:Relate:Server.Kill
  Hide:Relate:ServerAlias.Kill


Hide:Access:Labels.Init PROCEDURE
  CODE
  SELF.Init(Labels,GlobalErrors)
  SELF.FileNameValue = 'Labels'
  SELF.Buffer &= LAB:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  Access:Labels &= SELF


Hide:Relate:Labels.Init PROCEDURE
  CODE
  Hide:Access:Labels.Init
  SELF.Init(Access:Labels,1)


Hide:Access:Labels.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:Labels &= NULL


Hide:Relate:Labels.Kill PROCEDURE

  CODE
  Hide:Access:Labels.Kill
  PARENT.Kill
  Relate:Labels &= NULL


Hide:Access:Reports.Init PROCEDURE
  CODE
  SELF.Init(Reports,GlobalErrors)
  SELF.FileNameValue = 'Reports'
  SELF.Buffer &= REP:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  Access:Reports &= SELF


Hide:Relate:Reports.Init PROCEDURE
  CODE
  Hide:Access:Reports.Init
  SELF.Init(Access:Reports,1)


Hide:Access:Reports.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:Reports &= NULL


Hide:Relate:Reports.Kill PROCEDURE

  CODE
  Hide:Access:Reports.Kill
  PARENT.Kill
  Relate:Reports &= NULL


Hide:Access:Server.Init PROCEDURE
  CODE
  SELF.Init(Server,GlobalErrors)
  SELF.Buffer &= SRV:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(SRV:KeyRecordNo,'SRV:KeyRecordNo',1)
  SELF.AddKey(SRV:KeyMainServerFlag,'SRV:KeyMainServerFlag',0)
  SELF.AddKey(SRV:KeyPriority,'SRV:KeyPriority',0)
  Access:Server &= SELF


Hide:Relate:Server.Init PROCEDURE
  CODE
  Hide:Access:Server.Init
  SELF.Init(Access:Server,1)


Hide:Access:Server.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:Server &= NULL


Hide:Relate:Server.Kill PROCEDURE

  CODE
  Hide:Access:Server.Kill
  PARENT.Kill
  Relate:Server &= NULL


Hide:Access:ServerAlias.Init PROCEDURE
  CODE
  SELF.Init(ServerAlias,GlobalErrors)
  SELF.Buffer &= SRA:Record
  SELF.AliasedFile &= Access:Server                   !This is a File Alias, so assign aliased file
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(SRA:KeyRecordNo,'SRA:KeyRecordNo',1)
  SELF.AddKey(SRA:KeyMainServerFlag,'SRA:KeyMainServerFlag',0)
  SELF.AddKey(SRA:KeyPriority,'SRA:KeyPriority',0)
  Access:ServerAlias &= SELF


Hide:Relate:ServerAlias.Init PROCEDURE
  CODE
  Hide:Access:ServerAlias.Init
  SELF.Init(Access:ServerAlias,1)
  SELF.SetAlias(Relate:Server)


Hide:Access:ServerAlias.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:ServerAlias &= NULL


Hide:Relate:ServerAlias.Kill PROCEDURE

  CODE
  Hide:Access:ServerAlias.Kill
  PARENT.Kill
  Relate:ServerAlias &= NULL

