  MEMBER('vcpimport.clw')

  INCLUDE('ABFILE.INC'),ONCE

  MAP
VCPIMBC5:DctInit    PROCEDURE
VCPIMBC5:DctKill    PROCEDURE
VCPIMBC5:FilesInit  PROCEDURE
  END

Hide:Access:SUBCHRGE CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:SUBCHRGE CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:DEFCHRGE CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:DEFCHRGE CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:SUBTRACC CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:SUBTRACC CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:CHARTYPE CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:CHARTYPE CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:WAYBAWT  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:WAYBAWT  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:MANUFACT CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:MANUFACT CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:USMASSIG CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:USMASSIG CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:USUASSIG CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:USUASSIG CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBSOBF_ALIAS CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:JOBSOBF_ALIAS CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBPAYMT_ALIAS CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:JOBPAYMT_ALIAS CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBS_ALIAS CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:JOBS_ALIAS CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

VCPIMBC5:DctInit PROCEDURE
  CODE
  Relate:SUBCHRGE &= Hide:Relate:SUBCHRGE
  Relate:DEFCHRGE &= Hide:Relate:DEFCHRGE
  Relate:SUBTRACC &= Hide:Relate:SUBTRACC
  Relate:CHARTYPE &= Hide:Relate:CHARTYPE
  Relate:WAYBAWT &= Hide:Relate:WAYBAWT
  Relate:MANUFACT &= Hide:Relate:MANUFACT
  Relate:USMASSIG &= Hide:Relate:USMASSIG
  Relate:USUASSIG &= Hide:Relate:USUASSIG
  Relate:JOBSOBF_ALIAS &= Hide:Relate:JOBSOBF_ALIAS
  Relate:JOBPAYMT_ALIAS &= Hide:Relate:JOBPAYMT_ALIAS
  Relate:JOBS_ALIAS &= Hide:Relate:JOBS_ALIAS

VCPIMBC5:FilesInit PROCEDURE
  CODE
  Hide:Relate:SUBCHRGE.Init
  Hide:Relate:DEFCHRGE.Init
  Hide:Relate:SUBTRACC.Init
  Hide:Relate:CHARTYPE.Init
  Hide:Relate:WAYBAWT.Init
  Hide:Relate:MANUFACT.Init
  Hide:Relate:USMASSIG.Init
  Hide:Relate:USUASSIG.Init
  Hide:Relate:JOBSOBF_ALIAS.Init
  Hide:Relate:JOBPAYMT_ALIAS.Init
  Hide:Relate:JOBS_ALIAS.Init


VCPIMBC5:DctKill PROCEDURE
  CODE
  Hide:Relate:SUBCHRGE.Kill
  Hide:Relate:DEFCHRGE.Kill
  Hide:Relate:SUBTRACC.Kill
  Hide:Relate:CHARTYPE.Kill
  Hide:Relate:WAYBAWT.Kill
  Hide:Relate:MANUFACT.Kill
  Hide:Relate:USMASSIG.Kill
  Hide:Relate:USUASSIG.Kill
  Hide:Relate:JOBSOBF_ALIAS.Kill
  Hide:Relate:JOBPAYMT_ALIAS.Kill
  Hide:Relate:JOBS_ALIAS.Kill


Hide:Access:SUBCHRGE.Init PROCEDURE
  CODE
  SELF.Init(SUBCHRGE,GlobalErrors)
  SELF.Buffer &= suc:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(suc:Model_Repair_Type_Key,'By Repair Type',0)
  SELF.AddKey(suc:Account_Charge_Key,'By Charge Type',0)
  SELF.AddKey(suc:Unit_Type_Key,'By Unit Type',0)
  SELF.AddKey(suc:Repair_Type_Key,'By Repair Type',0)
  SELF.AddKey(suc:Cost_Key,'By Cost',0)
  SELF.AddKey(suc:Charge_Type_Only_Key,'suc:Charge_Type_Only_Key',0)
  SELF.AddKey(suc:Repair_Type_Only_Key,'suc:Repair_Type_Only_Key',0)
  SELF.AddKey(suc:Unit_Type_Only_Key,'suc:Unit_Type_Only_Key',0)
  SELF.AddKey(suc:Model_Repair_Key,'suc:Model_Repair_Key',0)
  Access:SUBCHRGE &= SELF


Hide:Relate:SUBCHRGE.Init PROCEDURE
  CODE
  Hide:Access:SUBCHRGE.Init
  SELF.Init(Access:SUBCHRGE,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:CHARTYPE)
  SELF.AddRelation(Relate:UNITTYPE)
  SELF.AddRelation(Relate:REPAIRTY)
  SELF.AddRelation(Relate:SUBTRACC)


Hide:Access:SUBCHRGE.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:SUBCHRGE &= NULL


Hide:Relate:SUBCHRGE.Kill PROCEDURE

  CODE
  Hide:Access:SUBCHRGE.Kill
  PARENT.Kill
  Relate:SUBCHRGE &= NULL


Hide:Access:DEFCHRGE.Init PROCEDURE
  CODE
  SELF.Init(DEFCHRGE,GlobalErrors)
  SELF.Buffer &= dec:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(dec:Charge_Type_Key,'By Charge Type',0)
  SELF.AddKey(dec:Repair_Type_Only_Key,'dec:Repair_Type_Only_Key',0)
  SELF.AddKey(dec:Unit_Type_Only_Key,'dec:Unit_Type_Only_Key',0)
  Access:DEFCHRGE &= SELF


Hide:Relate:DEFCHRGE.Init PROCEDURE
  CODE
  Hide:Access:DEFCHRGE.Init
  SELF.Init(Access:DEFCHRGE,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:CHARTYPE)
  SELF.AddRelation(Relate:REPAIRTY)
  SELF.AddRelation(Relate:UNITTYPE)


Hide:Access:DEFCHRGE.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:DEFCHRGE &= NULL


Hide:Relate:DEFCHRGE.Kill PROCEDURE

  CODE
  Hide:Access:DEFCHRGE.Kill
  PARENT.Kill
  Relate:DEFCHRGE &= NULL


Hide:Access:SUBTRACC.Init PROCEDURE
  CODE
  SELF.Init(SUBTRACC,GlobalErrors)
  SELF.Buffer &= sub:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(sub:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(sub:Main_Account_Key,'By Account Number',0)
  SELF.AddKey(sub:Main_Name_Key,'By Company Name',0)
  SELF.AddKey(sub:Account_Number_Key,'By Account Number',0)
  SELF.AddKey(sub:Branch_Key,'By Branch',0)
  SELF.AddKey(sub:Main_Branch_Key,'By Branch',0)
  SELF.AddKey(sub:Company_Name_Key,'By Company Name',0)
  SELF.AddKey(sub:ReplicateFromKey,'By Account Number',0)
  SELF.AddKey(sub:GenericAccountKey,'sub:GenericAccountKey',0)
  SELF.AddKey(sub:GenericCompanyKey,'sub:GenericCompanyKey',0)
  SELF.AddKey(sub:GenericBranchKey,'sub:GenericBranchKey',0)
  SELF.AddKey(sub:RegionKey,'By Region',0)
  Access:SUBTRACC &= SELF


Hide:Relate:SUBTRACC.Init PROCEDURE
  CODE
  Hide:Access:SUBTRACC.Init
  SELF.Init(Access:SUBTRACC,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:SUBBUSHR,RI:CASCADE,RI:CASCADE,sbh:TypeDateKey)
  SELF.AddRelationLink(sub:RecordNumber,sbh:RefNumber)
  SELF.AddRelation(Relate:SUBEMAIL,RI:CASCADE,RI:CASCADE,sue:RecipientTypeKey)
  SELF.AddRelationLink(sub:RecordNumber,sue:RefNumber)
  SELF.AddRelation(Relate:SUBACCAD,RI:CASCADE,RI:CASCADE,sua:AccountNumberKey)
  SELF.AddRelationLink(sub:RecordNumber,sua:RefNumber)
  SELF.AddRelation(Relate:JOBS,RI:CASCADE,RI:None,job:AccountNumberKey)
  SELF.AddRelationLink(sub:Account_Number,job:Account_Number)
  SELF.AddRelation(Relate:SUBCHRGE,RI:CASCADE,RI:CASCADE,suc:Account_Charge_Key)
  SELF.AddRelationLink(sub:Account_Number,suc:Account_Number)
  SELF.AddRelation(Relate:TRADEACC)


Hide:Access:SUBTRACC.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:SUBTRACC &= NULL


Hide:Access:SUBTRACC.PrimeFields PROCEDURE

  CODE
  sub:UseCustDespAdd = 'NO'
  sub:PriceDespatchNotes = 0
  sub:Price_First_Copy_Only = 0
  sub:Print_Despatch_Complete = 'NO'
  sub:Print_Despatch_Despatch = 'NO'
  sub:Print_Retail_Despatch_Note = 'NO'
  sub:Print_Retail_Picking_Note = 'NO'
  sub:Despatch_Note_Per_Item = 'NO'
  sub:Summary_Despatch_Notes = 'NO'
  sub:Use_Customer_Address = 'NO'
  sub:Invoice_Customer_Address = 'YES'
  sub:ZeroChargeable = 'NO'
  sub:MultiInvoice = 'SIN'
  sub:EDIInvoice = 'NO'
  sub:HideDespAdd = 0
  sub:EuroApplies = 0
  sub:ForceCommonFault = 0
  sub:ForceOrderNumber = 0
  sub:InvoiceAtDespatch = 0
  sub:InvoiceType = 0
  sub:IndividualSummary = 0
  sub:UseTradeContactNo = 0
  sub:E1 = 1
  sub:E2 = 1
  sub:E3 = 1
  sub:UseDespatchDetails = 0
  sub:UseAlternativeAdd = 0
  sub:AutoSendStatusEmails = 0
  sub:EmailRecipientList = 0
  sub:EmailEndUser = 0
  sub:ForceEndUserName = 0
  sub:ChangeInvAddress = 1
  sub:ChangeCollAddress = 1
  sub:ChangeDelAddress = 1
  sub:AllowMaximumDiscount = 0
  sub:SetInvoicedJobStatus = 0
  sub:SetDespatchJobStatus = 0
  sub:FactorAccount = 0
  sub:ForceEstimate = 0
  sub:InvoiceAtCompletion = 0
  sub:InvoiceTypeComplete = 0
  sub:Generic_Account = 0
  sub:Force_Customer_Name = 0
  sub:ExcludeBouncer = 0
  sub:ExcludeFromTATReport = 0
  sub:OverrideHeadVATNo = 0
  sub:OverrideHeadMobile = 0
  sub:OverrideMobileDefault = 0
  sub:SIDJobBooking = 0
  sub:SIDJobEnquiry = 0
  sub:AllowVCPLoanUnits = 0
  sub:RefurbishmentAccount = 0
  sub:PrintOOWVCPFee = 0
  sub:PrintWarrantyVCPFee = 0
  PARENT.PrimeFields


Hide:Relate:SUBTRACC.Kill PROCEDURE

  CODE
  Hide:Access:SUBTRACC.Kill
  PARENT.Kill
  Relate:SUBTRACC &= NULL


Hide:Access:CHARTYPE.Init PROCEDURE
  CODE
  SELF.Init(CHARTYPE,GlobalErrors)
  SELF.Buffer &= cha:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(cha:Charge_Type_Key,'By Charge Type',0)
  SELF.AddKey(cha:Ref_Number_Key,'By Charge Type',0)
  SELF.AddKey(cha:Physical_Damage_Key,'By Charge Type',0)
  SELF.AddKey(cha:Warranty_Key,'By Charge Type',0)
  SELF.AddKey(cha:Warranty_Ref_Number_Key,'By Charge Type',0)
  Access:CHARTYPE &= SELF


Hide:Relate:CHARTYPE.Init PROCEDURE
  CODE
  Hide:Access:CHARTYPE.Init
  SELF.Init(Access:CHARTYPE,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:STDCHRGE,RI:CASCADE,RI:RESTRICT,sta:Charge_Type_Only_Key)
  SELF.AddRelationLink(cha:Charge_Type,sta:Charge_Type)
  SELF.AddRelation(Relate:DEFCHRGE,RI:CASCADE,RI:RESTRICT,dec:Charge_Type_Key)
  SELF.AddRelationLink(cha:Charge_Type,dec:Charge_Type)
  SELF.AddRelation(Relate:TRACHRGE,RI:CASCADE,RI:RESTRICT,trc:Charge_Type_Only_Key)
  SELF.AddRelationLink(cha:Charge_Type,trc:Charge_Type)
  SELF.AddRelation(Relate:SUBCHRGE,RI:CASCADE,RI:RESTRICT,suc:Charge_Type_Only_Key)
  SELF.AddRelationLink(cha:Charge_Type,suc:Charge_Type)


Hide:Access:CHARTYPE.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:CHARTYPE &= NULL


Hide:Access:CHARTYPE.PrimeFields PROCEDURE

  CODE
  cha:Force_Warranty = 'NO'
  cha:Allow_Physical_Damage = 'NO'
  cha:Allow_Estimate = 'NO'
  cha:Force_Estimate = 'NO'
  cha:Invoice_Customer = 'NO'
  cha:Invoice_Trade_Customer = 'NO'
  cha:Invoice_Manufacturer = 'NO'
  cha:No_Charge = 'NO'
  cha:Zero_Parts = 'NO'
  cha:Warranty = 'NO'
  cha:Exclude_EDI = 'NO'
  cha:ExcludeInvoice = 'NO'
  cha:ExcludeFromBouncer = 0
  cha:ForceAuthorisation = 0
  cha:Zero_Parts_ARC = 0
  cha:SecondYearWarranty = 0
  PARENT.PrimeFields


Hide:Relate:CHARTYPE.Kill PROCEDURE

  CODE
  Hide:Access:CHARTYPE.Kill
  PARENT.Kill
  Relate:CHARTYPE &= NULL


Hide:Access:WAYBAWT.Init PROCEDURE
  CODE
  SELF.Init(WAYBAWT,GlobalErrors)
  SELF.Buffer &= wya:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(wya:WAYBAWTIDKey,'wya:WAYBAWTIDKey',1)
  SELF.AddKey(wya:AccountJobNumberKey,'wya:AccountJobNumberKey',0)
  SELF.AddKey(wya:JobNumberKey,'wya:JobNumberKey',0)
  Access:WAYBAWT &= SELF


Hide:Relate:WAYBAWT.Init PROCEDURE
  CODE
  Hide:Access:WAYBAWT.Init
  SELF.Init(Access:WAYBAWT,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:WAYBAWT.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:WAYBAWT &= NULL


Hide:Relate:WAYBAWT.Kill PROCEDURE

  CODE
  Hide:Access:WAYBAWT.Kill
  PARENT.Kill
  Relate:WAYBAWT &= NULL


Hide:Access:MANUFACT.Init PROCEDURE
  CODE
  SELF.Init(MANUFACT,GlobalErrors)
  SELF.Buffer &= man:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(man:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(man:Manufacturer_Key,'By Manufacturer',0)
  SELF.AddKey(man:EDIFileTypeKey,'By Manufacturer',0)
  Access:MANUFACT &= SELF


Hide:Relate:MANUFACT.Init PROCEDURE
  CODE
  Hide:Access:MANUFACT.Init
  SELF.Init(Access:MANUFACT,1)
  DO AddRelations_1
  DO AddRelations_2

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:MANREJR,RI:CASCADE,RI:CASCADE,mar:CodeKey)
  SELF.AddRelationLink(man:RecordNumber,mar:MANRecordNumber)
  SELF.AddRelation(Relate:REPTYDEF,RI:CASCADE,RI:CASCADE,rtd:ManRepairTypeKey)
  SELF.AddRelationLink(man:Manufacturer,rtd:Manufacturer)
  SELF.AddRelation(Relate:NOTESFAU,RI:CASCADE,RI:CASCADE,nof:Notes_Key)
  SELF.AddRelationLink(man:Manufacturer,nof:Manufacturer)
  SELF.AddRelation(Relate:MODELNUM,RI:CASCADE,RI:CASCADE,mod:Manufacturer_Key)
  SELF.AddRelationLink(man:Manufacturer,mod:Manufacturer)
  SELF.AddRelation(Relate:REPAIRTY,RI:CASCADE,RI:CASCADE,rep:Manufacturer_Key)
  SELF.AddRelationLink(man:Manufacturer,rep:Manufacturer)
  SELF.AddRelation(Relate:JOBS,RI:CASCADE,RI:None,job:EDI_Key)
  SELF.AddRelationLink(man:Manufacturer,job:Manufacturer)
  SELF.AddRelation(Relate:MANFAULO,RI:CASCADE,RI:CASCADE,mfo:Field_Key)
  SELF.AddRelationLink(man:Manufacturer,mfo:Manufacturer)
  SELF.AddRelation(Relate:MANFAUPA,RI:CASCADE,RI:CASCADE,map:Field_Number_Key)
  SELF.AddRelationLink(man:Manufacturer,map:Manufacturer)
  SELF.AddRelation(Relate:MANFPALO,RI:CASCADE,RI:CASCADE,mfp:Field_Key)
  SELF.AddRelationLink(man:Manufacturer,mfp:Manufacturer)
  SELF.AddRelation(Relate:MANFAULT,RI:CASCADE,RI:CASCADE,maf:Field_Number_Key)
  SELF.AddRelationLink(man:Manufacturer,maf:Manufacturer)

AddRelations_2 ROUTINE
  SELF.AddRelation(Relate:STDCHRGE,RI:CASCADE,RI:CASCADE,sta:Model_Number_Charge_Key)
  SELF.AddRelationLink(man:Manufacturer,sta:Model_Number)


Hide:Access:MANUFACT.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:MANUFACT &= NULL


Hide:Access:MANUFACT.PrimeFields PROCEDURE

  CODE
  man:Use_MSN = 'NO'
  man:IncludeAdjustment = 'NO'
  man:AdjustPart = 0
  man:ForceParts = 0
  man:RemAccCosts = 0
  man:SiemensNewEDI = 0
  man:DOPCompulsory = 0
  man:UseQA = 0
  man:UseElectronicQA = 0
  man:QALoanExchange = 0
  man:QAAtCompletion = 0
  man:UseProductCode = 0
  man:ApplyMSNFormat = 0
  man:ValidateDateCode = 0
  man:ForceStatus = 0
  man:QAParts = 0
  man:QANetwork = 0
  man:POPRequired = 0
  man:UseInvTextForFaults = 0
  man:ForceAccessoryCode = 0
  man:UseInternetValidation = 0
  man:AutoRepairType = 0
  man:BillingConfirmation = 0
  man:CreateEDIReport = 0
  man:CreateEDIFile = 0
  man:QALoan = 0
  man:ForceCharFaultCodes = 0
  man:IncludeCharJobs = 0
  man:UseProdCodesForEXC = 0
  man:UseFaultCodesForOBF = 0
  man:KeyRepairRequired = 0
  man:UseReplenishmentProcess = 0
  man:UseResubmissionLimit = 0
  man:AutoTechnicalReports = 0
  man:LimitResubmissions = 0
  man:CopyDOPFromBouncer = 0
  man:ExcludeAutomaticRebookingProcess = 0
  PARENT.PrimeFields


Hide:Relate:MANUFACT.Kill PROCEDURE

  CODE
  Hide:Access:MANUFACT.Kill
  PARENT.Kill
  Relate:MANUFACT &= NULL


Hide:Access:USMASSIG.Init PROCEDURE
  CODE
  SELF.Init(USMASSIG,GlobalErrors)
  SELF.Buffer &= usm:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(usm:Model_Number_Key,'By Model Number',0)
  Access:USMASSIG &= SELF


Hide:Relate:USMASSIG.Init PROCEDURE
  CODE
  Hide:Access:USMASSIG.Init
  SELF.Init(Access:USMASSIG,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:USERS)


Hide:Access:USMASSIG.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:USMASSIG &= NULL


Hide:Relate:USMASSIG.Kill PROCEDURE

  CODE
  Hide:Access:USMASSIG.Kill
  PARENT.Kill
  Relate:USMASSIG &= NULL


Hide:Access:USUASSIG.Init PROCEDURE
  CODE
  SELF.Init(USUASSIG,GlobalErrors)
  SELF.Buffer &= usu:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(usu:Unit_Type_Key,'By Unit Type',0)
  SELF.AddKey(usu:Unit_Type_Only_Key,'By Unit Type',0)
  Access:USUASSIG &= SELF


Hide:Relate:USUASSIG.Init PROCEDURE
  CODE
  Hide:Access:USUASSIG.Init
  SELF.Init(Access:USUASSIG,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:USERS)
  SELF.AddRelation(Relate:UNITTYPE)


Hide:Access:USUASSIG.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:USUASSIG &= NULL


Hide:Relate:USUASSIG.Kill PROCEDURE

  CODE
  Hide:Access:USUASSIG.Kill
  PARENT.Kill
  Relate:USUASSIG &= NULL


Hide:Access:JOBSOBF_ALIAS.Init PROCEDURE
  CODE
  SELF.Init(JOBSOBF_ALIAS,GlobalErrors)
  SELF.Buffer &= jofali:Record
  SELF.AliasedFile &= Access:JOBSOBF                  !This is a File Alias, so assign aliased file
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jofali:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(jofali:RefNumberKey,'By Job Number',0)
  SELF.AddKey(jofali:StatusRefNumberKey,'By Job Number',0)
  SELF.AddKey(jofali:StatusIMEINumberKey,'By I.M.E.I. Number',0)
  SELF.AddKey(jofali:HeadAccountCompletedKey,'By Date Completed',0)
  SELF.AddKey(jofali:HeadAccountProcessedKey,'By Date Processed',0)
  Access:JOBSOBF_ALIAS &= SELF


Hide:Relate:JOBSOBF_ALIAS.Init PROCEDURE
  CODE
  Hide:Access:JOBSOBF_ALIAS.Init
  SELF.Init(Access:JOBSOBF_ALIAS,1)
  SELF.SetAlias(Relate:JOBSOBF)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS_ALIAS)


Hide:Access:JOBSOBF_ALIAS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBSOBF_ALIAS &= NULL


Hide:Relate:JOBSOBF_ALIAS.Kill PROCEDURE

  CODE
  Hide:Access:JOBSOBF_ALIAS.Kill
  PARENT.Kill
  Relate:JOBSOBF_ALIAS &= NULL


Hide:Access:JOBPAYMT_ALIAS.Init PROCEDURE
  CODE
  SELF.Init(JOBPAYMT_ALIAS,GlobalErrors)
  SELF.Buffer &= jpt_ali:Record
  SELF.AliasedFile &= Access:JOBPAYMT                 !This is a File Alias, so assign aliased file
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jpt_ali:Record_Number_Key,'By Record Number',1)
  SELF.AddKey(jpt_ali:All_Date_Key,'By Date',0)
  SELF.AddKey(jpt_ali:Loan_Deposit_Key,'jpt_ali:Loan_Deposit_Key',0)
  Access:JOBPAYMT_ALIAS &= SELF


Hide:Relate:JOBPAYMT_ALIAS.Init PROCEDURE
  CODE
  Hide:Access:JOBPAYMT_ALIAS.Init
  SELF.Init(Access:JOBPAYMT_ALIAS,1)
  SELF.SetAlias(Relate:JOBPAYMT)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:JOBPAYMT_ALIAS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBPAYMT_ALIAS &= NULL


Hide:Relate:JOBPAYMT_ALIAS.Kill PROCEDURE

  CODE
  Hide:Access:JOBPAYMT_ALIAS.Kill
  PARENT.Kill
  Relate:JOBPAYMT_ALIAS &= NULL


Hide:Access:JOBS_ALIAS.Init PROCEDURE
  CODE
  SELF.Init(JOBS_ALIAS,GlobalErrors)
  SELF.Buffer &= job_ali:Record
  SELF.AliasedFile &= Access:JOBS                     !This is a File Alias, so assign aliased file
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(job_ali:Ref_Number_Key,'By Job Number',1)
  SELF.AddKey(job_ali:Model_Unit_Key,'By Job Number',0)
  SELF.AddKey(job_ali:EngCompKey,'By Job Number',0)
  SELF.AddKey(job_ali:EngWorkKey,'By Job Number',0)
  SELF.AddKey(job_ali:Surname_Key,'By Surname',0)
  SELF.AddKey(job_ali:MobileNumberKey,'By Mobile Number',0)
  SELF.AddKey(job_ali:ESN_Key,'By E.S.N. / I.M.E.I.',0)
  SELF.AddKey(job_ali:MSN_Key,'By M.S.N.',0)
  SELF.AddKey(job_ali:AccountNumberKey,'By Account Number',0)
  SELF.AddKey(job_ali:AccOrdNoKey,'By Order Number',0)
  SELF.AddKey(job_ali:Model_Number_Key,'By Model Number',0)
  SELF.AddKey(job_ali:Engineer_Key,'By Engineer',0)
  SELF.AddKey(job_ali:Date_Booked_Key,'By Date Booked',0)
  SELF.AddKey(job_ali:DateCompletedKey,'By Date Completed',0)
  SELF.AddKey(job_ali:ModelCompKey,'By Completed Date',0)
  SELF.AddKey(job_ali:By_Status,'By Job Number',0)
  SELF.AddKey(job_ali:StatusLocKey,'By Job Number',0)
  SELF.AddKey(job_ali:Location_Key,'By Location',0)
  SELF.AddKey(job_ali:Third_Party_Key,'By Third Party',0)
  SELF.AddKey(job_ali:ThirdEsnKey,'By ESN',0)
  SELF.AddKey(job_ali:ThirdMsnKey,'By MSN',0)
  SELF.AddKey(job_ali:PriorityTypeKey,'By Priority',0)
  SELF.AddKey(job_ali:Unit_Type_Key,'By Unit Type',0)
  SELF.AddKey(job_ali:EDI_Key,'By Job Number',0)
  SELF.AddKey(job_ali:InvoiceNumberKey,'By Invoice_Number',0)
  SELF.AddKey(job_ali:WarInvoiceNoKey,'By Invoice Number',0)
  SELF.AddKey(job_ali:Batch_Number_Key,'By Job Number',0)
  SELF.AddKey(job_ali:Batch_Status_Key,'By Ref Number',0)
  SELF.AddKey(job_ali:BatchModelNoKey,'By Job Number',0)
  SELF.AddKey(job_ali:BatchInvoicedKey,'By Ref Number',0)
  SELF.AddKey(job_ali:BatchCompKey,'By Job Number',0)
  SELF.AddKey(job_ali:ChaInvoiceKey,'job_ali:ChaInvoiceKey',0)
  SELF.AddKey(job_ali:InvoiceExceptKey,'By Job Number',0)
  SELF.AddKey(job_ali:ConsignmentNoKey,'By Cosignment Number',0)
  SELF.AddKey(job_ali:InConsignKey,'By Consignment Number',0)
  SELF.AddKey(job_ali:ReadyToDespKey,'By Job Number',0)
  SELF.AddKey(job_ali:ReadyToTradeKey,'By Job Number',0)
  SELF.AddKey(job_ali:ReadyToCouKey,'By Job Number',0)
  SELF.AddKey(job_ali:ReadyToAllKey,'By Job Number',0)
  SELF.AddKey(job_ali:DespJobNumberKey,'By Job Number',0)
  SELF.AddKey(job_ali:DateDespatchKey,'By Job Number',0)
  SELF.AddKey(job_ali:DateDespLoaKey,'By Job Number',0)
  SELF.AddKey(job_ali:DateDespExcKey,'By Job Number',0)
  SELF.AddKey(job_ali:ChaRepTypeKey,'By Chargeable Repair Type',0)
  SELF.AddKey(job_ali:WarRepTypeKey,'By Warranty Repair Type',0)
  SELF.AddKey(job_ali:ChaTypeKey,'job_ali:ChaTypeKey',0)
  SELF.AddKey(job_ali:WarChaTypeKey,'job_ali:WarChaTypeKey',0)
  SELF.AddKey(job_ali:Bouncer_Key,'By Job Number',0)
  SELF.AddKey(job_ali:EngDateCompKey,'By Date Completed',0)
  SELF.AddKey(job_ali:ExcStatusKey,'By Job Number',0)
  SELF.AddKey(job_ali:LoanStatusKey,'By Job Number',0)
  SELF.AddKey(job_ali:ExchangeLocKey,'By Job Number',0)
  SELF.AddKey(job_ali:LoanLocKey,'By Job Number',0)
  SELF.AddKey(job_ali:BatchJobKey,'By Job Number',0)
  SELF.AddKey(job_ali:BatchStatusKey,'By Job Number',0)
  Access:JOBS_ALIAS &= SELF


Hide:Relate:JOBS_ALIAS.Init PROCEDURE
  CODE
  Hide:Access:JOBS_ALIAS.Init
  SELF.Init(Access:JOBS_ALIAS,1)
  SELF.SetAlias(Relate:JOBS)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBSOBF_ALIAS,RI:CASCADE,RI:CASCADE,jofali:RefNumberKey)
  SELF.AddRelationLink(job_ali:Ref_Number,jofali:RefNumber)
  SELF.AddRelation(Relate:BOUNCER,RI:CASCADE,RI:CASCADE,bou:Bouncer_Job_Only_Key)
  SELF.AddRelationLink(job_ali:Ref_Number,bou:Bouncer_Job_Number)
  SELF.AddRelation(Relate:REPAIRTY)


Hide:Access:JOBS_ALIAS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBS_ALIAS &= NULL


Hide:Relate:JOBS_ALIAS.Kill PROCEDURE

  CODE
  Hide:Access:JOBS_ALIAS.Kill
  PARENT.Kill
  Relate:JOBS_ALIAS &= NULL

