

   MEMBER('repbatch.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPRPDF.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('ABRPPSEL.INC'),ONCE

                     MAP
                       INCLUDE('REPBA004.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('REPBA005.INC'),ONCE        !Req'd for module callout resolution
                     END


GoodReceivedNote PROCEDURE (Long f:GRNNumber,Long f:Suffix) ! Generated from procedure template - Report

Progress:Thermometer BYTE                                  !
PartsQueue           QUEUE,PRE(parque)                     !
PartNumber           STRING(30)                            !Part Number
Description          STRING(30)                            !Description
ShelfLocation        STRING(30)                            !Shelf Location
QuantityOrdered      LONG                                  !Quantity Ordered
QuantityReceived     LONG                                  !Quantity Received
GRNValue             REAL                                  !GRN Value
                     END                                   !
tmp:TotalLines       LONG                                  !Total Lines
tmp:TotalItems       LONG                                  !Total Items
tmp:ExchangeRate     REAL                                  !Exchange Rate
tmp:TotalOrderValue  REAL                                  !Total Order Value
tmp:TotalForeignOrderValue REAL                            !Total Order Value (Foreign)
tmp:LineCost         REAL                                  !Line Cost
tmp:One              LONG(1)                               !
tmp:Ten              LONG(12)                              !
tmp:GRNNumber        LONG                                  !GRN Number
tmp:OrderNumber      STRING(13)                            !
tmp:CurrencyLineCost REAL                                  !Currency Line Cost
tmp:ReceivedDailyRate REAL                                 !Received Daily Rate
tmp:ReceivedCurrency STRING(30)                            !Received Currency
tmp:ReceivedDivideMultiply STRING(30)                      !Received Divide Multiply
Process:View         VIEW(GRNOTES)
                       PROJECT(grn:Goods_Received_Number)
                     END
ProgressWindow       WINDOW('Report GRNOTES'),AT(,,142,59),FONT('Tahoma',8,COLOR:Black,FONT:regular),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),FLAT,LEFT,MSG('Cancel Report'),TIP('Cancel Report'),ICON('WACANCEL.ICO')
                     END

Report               REPORT('GRNOTES Report'),AT(240,4365,7750,4323),PAPER(PAPER:A4),PRE(RPT),FONT('Tahoma',8,COLOR:Black,FONT:regular),THOUS
                       HEADER,AT(240,250,7750,3625)
                         STRING(@s30),AT(104,0),USE(def:OrderCompanyName),TRN,LEFT,FONT('Tahoma',16,,FONT:bold)
                         STRING('GOODS RECEIVED NOTE'),AT(5000,0),USE(?Text:GoodsReceivedNote),TRN,FONT(,16,,FONT:bold)
                         STRING(@s30),AT(104,260),USE(def:OrderAddressLine1),TRN,LEFT,FONT('Tahoma',8,,FONT:regular)
                         STRING(@s30),AT(104,417),USE(def:OrderAddressLine2),TRN,LEFT,FONT('Tahoma',8,,FONT:regular)
                         STRING('GRN No.:'),AT(5313,521),USE(?String44),TRN,FONT(,,,FONT:bold)
                         STRING(@s8),AT(6198,521),USE(grn:Goods_Received_Number),TRN
                         STRING(@s30),AT(104,573),USE(def:OrderAddressLine3),TRN,LEFT,FONT('Tahoma',8,,FONT:regular)
                         STRING(@s30),AT(104,729),USE(def:OrderPostcode),TRN,LEFT,FONT('Tahoma',8,,FONT:regular)
                         STRING('Date Printed:'),AT(5313,990),USE(?ReportDatePrompt),TRN,FONT(,,,FONT:bold)
                         STRING('<<-- Date Stamp -->'),AT(6198,990),USE(?ReportDateStamp),TRN
                         STRING('Tel:'),AT(104,885),USE(?String22),TRN
                         STRING(@s30),AT(469,885),USE(def:OrderTelephoneNumber),TRN,LEFT,FONT('Tahoma',8,,FONT:regular)
                         STRING('Fax:'),AT(104,1042),USE(?String22:2),TRN
                         STRING(@s30),AT(469,1042),USE(def:OrderFaxNumber),TRN,LEFT,FONT('Tahoma',8,,FONT:regular)
                         STRING('Email:'),AT(104,1198),USE(?String22:3),TRN
                         STRING(@s255),AT(469,1198),USE(def:OrderEmailAddress),TRN,LEFT
                         STRING(@s30),AT(156,1771),USE(sup:Company_Name),TRN
                         STRING(@s30),AT(156,1927),USE(sup:Address_Line1),TRN
                         STRING(@s30),AT(156,2083),USE(sup:Address_Line2),TRN
                         STRING(@s30),AT(4167,1771),USE(def:OrderCompanyName,,?def:OrderCompanyName:2),TRN
                         STRING(@s30),AT(156,2240),USE(sup:Address_Line3),TRN
                         STRING(@s30),AT(4167,1927),USE(def:OrderAddressLine1,,?def:OrderAddressLine1:2),TRN
                         STRING(@s10),AT(156,2396),USE(sup:Postcode),TRN
                         STRING(@s30),AT(4167,2083),USE(def:OrderAddressLine2,,?def:OrderAddressLine2:2),TRN
                         STRING('Tel:'),AT(156,2552),USE(?Text:SupplierTelephone),TRN
                         STRING(@s15),AT(417,2552),USE(sup:Telephone_Number),TRN
                         STRING(@s30),AT(4167,2240),USE(def:OrderAddressLine3,,?def:OrderAddressLine3:2),TRN
                         STRING('Fax:'),AT(156,2708),USE(?Text:SupplierFax),TRN
                         STRING(@s15),AT(417,2708),USE(sup:Fax_Number),TRN
                         STRING(@s30),AT(4167,2396),USE(def:OrderPostcode,,?def:OrderPostcode:2),TRN
                         STRING('Tel:'),AT(4167,2552),USE(?Text:DeliveryTelephone),TRN
                         STRING(@s30),AT(4427,2552),USE(def:OrderTelephoneNumber,,?def:OrderTelephoneNumber:2),TRN
                         STRING('Fax:'),AT(4167,2708),USE(?Text:DeliveryFax),TRN
                         STRING(@s30),AT(4427,2708),USE(def:OrderFaxNumber,,?def:OrderFaxNumber:2),TRN
                         STRING(@s15),AT(156,3438),USE(sup:Account_Number),TRN
                         STRING(@s13),AT(1667,3438),USE(tmp:OrderNumber),TRN
                         STRING(@d6),AT(3177,3438),USE(ord:Date),TRN,LEFT
                         STRING(@s60),AT(4688,3438,1406,156),USE(sup:Contact_Name),TRN,HIDE,FONT(,7,,)
                         STRING(@s30),AT(6198,3438),USE(def:OrderTelephoneNumber,,?def:OrderTelephoneNumber:3),TRN
                       END
Detail:PartsOnOrder    DETAIL,AT(,,,156),USE(?Detail:PartsOnOrder)
                         STRING(@s30),AT(1510,0,1354,156),USE(parque:PartNumber),TRN,LEFT,FONT('Tahoma',7,,FONT:regular)
                         STRING(@s8),AT(156,0),USE(parque:QuantityOrdered),RIGHT,FONT(,7,,)
                         STRING(@s8),AT(833,0),USE(parque:QuantityReceived),RIGHT,FONT(,7,,)
                         STRING(@s30),AT(2917,0,1875,156),USE(parque:Description),TRN,LEFT,FONT('Tahoma',7,,FONT:regular)
                         STRING(@s30),AT(4844,0,1406,156),USE(parque:ShelfLocation),TRN,LEFT,FONT('Tahoma',7,,FONT:regular)
                         STRING(@n14.2),AT(6698,0),USE(tmp:LineCost),RIGHT,FONT(,7,,)
                         STRING(@n14.2),AT(5854,0),USE(tmp:CurrencyLineCost),TRN,RIGHT,FONT(,7,,)
                       END
Footer:PartsOnOrder    DETAIL,PAGEAFTER(-1),AT(250,250,7750,11188),USE(?Footer:PartsOnOrder),FONT('Tahoma',8,,),ABSOLUTE
                         STRING('Exchange Rate (CUR):'),AT(4948,8802),USE(?Text:ExchangeRate),TRN,FONT(,8,,FONT:bold)
                         STRING('Total Items:'),AT(208,8802),USE(?Text:TotalItems),TRN,FONT(,8,,FONT:bold)
                         STRING(@s8),AT(1094,8802),USE(tmp:TotalItems),TRN,RIGHT,FONT(,8,,)
                         STRING(@n12.4),AT(6656,8802,,208),USE(tmp:ExchangeRate),TRN,RIGHT,FONT('Tahoma',8,,)
                         STRING(@s8),AT(1094,9271),USE(tmp:TotalLines),TRN,RIGHT,FONT(,8,,)
                         STRING('Total Order Value (CUR):'),AT(4948,9323),USE(?Text:TotalOrderValue:Currency),TRN,FONT(,8,,FONT:bold)
                         STRING(@n14.2),AT(6531,9323),USE(tmp:TotalForeignOrderValue),TRN,RIGHT,FONT('Tahoma',8,,)
                         STRING('Total Order Value (R):'),AT(4948,9063),USE(?Text:TotalOrderValue),TRN,FONT(,8,,FONT:bold)
                         STRING(@n14.2),AT(6531,9063),USE(tmp:TotalOrderValue),TRN,RIGHT,FONT(,8,,)
                         STRING('Total Lines:'),AT(208,9271),USE(?Text:TotalLines),TRN,FONT(,8,,FONT:bold)
                       END
Footer:Continue        DETAIL,PAGEAFTER(-1),AT(250,250,7750,11188),USE(?Footer:Continue),ABSOLUTE
                         STRING('Continued.....'),AT(6667,9063),USE(?Text:Continued),TRN,FONT(,10,,FONT:bold)
                       END
                       FOOTER,AT(250,10042,7750,1396),USE(?Footer)
                       END
                       FORM,AT(250,250,7750,11188),USE(?PageForm),FONT('Tahoma',8,,)
                         BOX,AT(5052,417,2646,1042),USE(?BoxGrey:TopDetails),ROUND,FILL(COLOR:Silver)
                         BOX,AT(156,1719,3604,1302),USE(?BoxGrey:Address1),ROUND,FILL(COLOR:Silver)
                         BOX,AT(4115,1719,3604,1302),USE(?BoxGrey:Address2),ROUND,FILL(COLOR:Silver)
                         BOX,AT(156,3177,7552,521),USE(?BoxGrey:Title),ROUND,FILL(COLOR:Silver)
                         BOX,AT(156,3854,7552,4740),USE(?BoxGrey:Detail),ROUND,FILL(COLOR:Silver)
                         BOX,AT(156,8750,2813,1042),USE(?BoxGrey:Total1),ROUND,FILL(COLOR:Silver)
                         BOX,AT(4896,8750,2813,1042),USE(?BoxGrey:Total2),ROUND,FILL(COLOR:Silver)
                         BOX,AT(5000,365,2646,1042),USE(?Box:TopDetails),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         BOX,AT(104,1667,3604,1302),USE(?Box:Address1),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(4063,1667,3604,1302),USE(?Box:Address2),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(104,3125,1510,260),USE(?Box:Title1),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         BOX,AT(1615,3125,1510,260),USE(?Box:Title2),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         BOX,AT(3125,3125,1510,260),USE(?Box:Title3),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         BOX,AT(4635,3125,1510,260),USE(?Box:Title4),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         BOX,AT(6146,3125,1510,260),USE(?Box:Title5),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         BOX,AT(104,3385,1510,260),USE(?Box:Title1a),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(1615,3385,1510,260),USE(?Box:Title2a),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(3125,3385,1510,260),USE(?Box:Title3a),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(4635,3385,1510,260),USE(?Box:Title4a),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(6146,3385,1510,260),USE(?Box:Title5a),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(104,3802,7552,260),USE(?Box:Heading),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         BOX,AT(104,4063,7552,4479),USE(?Box:Detail),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(104,8698,2813,1042),USE(?Box:Total1),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         BOX,AT(4844,8698,2813,1042),USE(?Box:Total2),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         STRING('SUPPLIER ADDRESS'),AT(156,1510),USE(?Text:SupplierAddress),TRN,FONT(,,,FONT:bold)
                         STRING('DELIVERY ADDRESS'),AT(4115,1510),USE(?Text:DeliveryAddress),TRN,FONT(,,,FONT:bold)
                         STRING('Account Number'),AT(156,3177),USE(?Text:AccountNumber),TRN,FONT(,,,FONT:bold)
                         STRING('Order Number'),AT(1667,3177),USE(?Text:OrderNumber),TRN,FONT(,,,FONT:bold)
                         STRING('Order Date'),AT(3177,3177),USE(?Text:OrderDate),TRN,FONT(,,,FONT:bold)
                         STRING('Contact'),AT(4688,3177),USE(?Text:Contact),TRN,HIDE,FONT(,,,FONT:bold)
                         STRING('Contact Number'),AT(6198,3177),USE(?Text:ContactNumber),TRN,FONT(,,,FONT:bold)
                         STRING('Qty Ordered'),AT(156,3854),USE(?Text:QtyOrdered),TRN,FONT(,7,,FONT:bold)
                         STRING('Qty Rcvd'),AT(938,3854),USE(?Text:QtyReceived:2),TRN,FONT(,7,,FONT:bold)
                         STRING('Part Number'),AT(1510,3854),USE(?Text:PartNumber),TRN,FONT(,7,,FONT:bold)
                         STRING('Description'),AT(2917,3854),USE(?Text:Description),TRN,FONT(,7,,FONT:bold)
                         STRING('Shelf Location'),AT(4844,3854),USE(?Text:ShelfLocation),TRN,FONT(,7,,FONT:bold)
                         STRING('GRN Value (R)'),AT(6875,3854),USE(?Text:GRNValue),TRN,FONT(,7,,FONT:bold)
                         STRING('GRN Value (CUR)'),AT(5885,3854),USE(?Text:CurrencyGRNValue),TRN,RIGHT,FONT(,7,,FONT:bold)
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeNoRecords          PROCEDURE(),DERIVED                 ! Method added to host embed code
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED                 ! Method added to host embed code
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('GoodReceivedNote')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open                                     ! File DEFAULTS used by this procedure, so make sure it's RelationManager is open
  Relate:GRNOTES.Open                                      ! File GRNOTES used by this procedure, so make sure it's RelationManager is open
  Relate:ORDERS.SetOpenRelated()
  Relate:ORDERS.Open                                       ! File ORDERS used by this procedure, so make sure it's RelationManager is open
  Relate:STANTEXT.Open                                     ! File STANTEXT used by this procedure, so make sure it's RelationManager is open
  Access:SUPPLIER.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ORDPARTS.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:STOCK.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  tmp:GRNNumber = f:GRNNumber
  
  
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  
  Access:STANTEXT.ClearKey(stt:Description_Key)
  stt:Description = 'PARTS ORDER'
  If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
      !Found
  
  Else ! Access:STANTEXT.TryFetch(stt:Description_Key)
      !Error
  End ! Access:STANTEXT.TryFetch(stt:Description_Key)
  
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('GoodReceivedNote',ProgressWindow)          ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.SetReportTarget(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ProgressWindow{Prop:Timer} = 10                          ! Assign timer interval
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:GRNOTES, ?Progress:PctText, Progress:Thermometer, ProgressMgr, grn:Goods_Received_Number)
  ThisReport.AddSortOrder(grn:Goods_Received_Number_Key)
  ThisReport.AddRange(grn:Goods_Received_Number,tmp:GRNNumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{Prop:Text}=''
  Relate:GRNOTES.SetQuickScan(1,Propagate:OneMany)
  SELF.SkipPreview = True
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom=True
  Previewer.Maximize=True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:GRNOTES.Close
    Relate:ORDERS.Close
    Relate:STANTEXT.Close
  END
  IF SELF.Opened
    INIMgr.Update('GoodReceivedNote',ProgressWindow)       ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text}=FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
      Return
  PARENT.TakeNoRecords


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  Access:ORDERS.ClearKey(ord:Order_Number_Key)
  ord:Order_Number = grn:Order_Number
  If Access:ORDERS.TryFetch(ord:Order_Number_Key) = Level:Benign
      !Found
      Access:SUPPLIER.ClearKey(sup:Company_Name_Key)
      sup:Company_Name = ord:Supplier
      If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
          !Found
  
      Else !If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
          !Error
      End !If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
  Else !If Access:ORDERS.TryFetch(ord:Order_Number_Key) = Level:Benign
      !Error
  End !If Access:ORDERS.TryFetch(ord:Order_Number_Key) = Level:Benign
  
  ! Show order number suffix - TrkBs: 6237 (DBH: 17-08-2005)
  tmp:OrderNumber = Format(ord:Order_Number,@n~SS~010) & '/' & Format(f:Suffix,@n02)
  ! End   - Show order number suffix - TrkBs: 6237 (DBH: 17-08-2005)
  
  SetTarget(Report)
  If grn:CurrencyCode <> ''
      ?Text:CurrencyGRNValue{prop:Text} = 'GRN Value (' & Clip(grn:CurrencyCode) & ')'
  Else ! If grn:CurrencyCode <> ''
      ?Text:CurrencyGRNValue{prop:Hide} = True
      ?tmp:CurrencyLineCost{prop:Hide} = True
  End ! If grn:CurrencyCode <> ''
  SetTarget()
  
  ReturnValue = PARENT.TakeRecord()
  Clear(PartsQueue)
  Free(PartsQueue)
  Access:ORDPARTS.ClearKey(orp:Part_Number_Key)
  orp:Order_Number = ord:Order_Number
  Set(orp:Part_Number_Key, orp:Part_Number_Key)
  Loop
      If Access:ORDPARTS.NEXT()
          Break
      End ! If
      If orp:Order_Number <> grn:Order_Number      |
          Then Break   ! End If
      End ! If
  !    If orp:GRN_Number <> ''
          If orp:GRN_Number <> grn:Goods_Received_Number
              Cycle
          End ! If orp:GRN:Number <> grn:Goods_Received_Number
  !    End ! If orp:GRN_Number <> ''
      If orp:Date_Received = ''
          Cycle
      End ! If orp:Date_Received = ''
  
      ! Inserting (DBH 11/25/2005) #6789 - Save the currency details recorded when the parts where received. They should be the same for all the parts on the grn
      ! Inserting (DBH 31/01/2006) #7106 - Do not lookup currency is none was setup at point of order
      If Clip(ord:OrderedCurrency) <> '' And ord:OrderedCurrency <> '0'
      ! End (DBH 31/01/2006) #7106
          tmp:ReceivedDailyRate = orp:ReceivedDailyRate
          tmp:ReceivedCurrency = orp:ReceivedCurrency
          tmp:ReceivedDivideMultiply = orp:ReceivedDivideMultiply
          ! End (DBH 11/25/2005) #6789
      Else ! If Clip(ord:OrderedCurrency) <> '' And ord:OrderedCurrency <> 0
          tmp:ReceivedDailyRate = ''
          tmp:ReceivedCurrency = ''
          tmp:ReceivedDivideMultiply = ''
      End ! If Clip(ord:OrderedCurrency) <> '' And ord:OrderedCurrency <> 0
  
  
      parque:PartNumber = orp:Part_Number
      parque:GRNValue   = orp:Purchase_Cost
      Get(PartsQueue, parque:PartNumber, parque:GRNValue)
      If Error()
          parque:PartNumber  = orp:Part_Number
          parque:Description = orp:Description
          Access:STOCK.Clearkey(sto:Ref_Number_Key)
          sto:Ref_Number  = orp:Part_Ref_Number
          If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
              ! Found
              parque:ShelfLocation    = sto:Shelf_Location
          Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
              ! Error
              parque:ShelfLocation    = ''
          End ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
          parque:QuantityOrdered  = orp:Quantity
          parque:QuantityReceived = orp:Number_Received
          parque:GRNValue         = orp:Purchase_Cost
          Add(PartsQueue)
      Else ! If Error()
          parque:QuantityOrdered  += orp:Quantity
          parque:QuantityReceived += orp:Number_Received
          Put(PartsQueue)
      End ! If Error()
  End ! Loop
  
  If Records(PartsQueue)
      tmp:TotalItems             = 0
      tmp:TotalLines             = 0
      tmp:TotalOrderValue        = 0
      tmp:TotalForeignOrderValue = 0
      Count#                     = 0
      Sort(PartsQueue, parque:PartNumber)
      Loop x# = 1 To Records(PartsQueue)
          Get(PartsQueue, x#)
          If Count# = > 27
              Count# = 0
              Print(rpt:Footer:Continue)
          End ! If Count# > 27If Count# > 27
          Count# += 1
          tmp:LineCost = parque:GRNValue * parque:QuantityReceived
  ! Changing (DBH 11/25/2005) #6789 - Use the rate at point of receipt, not order
  !         If grn:CurrencyCode
  !             Case grn:DivideMultiply
  !             Of '*'
  !                 tmp:CurrencyLineCost = (parque:GRNValue / grn:DailyRate) * parque:QuantityReceived
  !             Of '/'
  !                 tmp:CurrencyLineCost = (parque:GRNValue / grn:DailyRate) / parque:QuantityReceived
  !             End ! Case grn:DivideMultiply
  !             tmp:TotalForeignOrderValue += tmp:CurrencyLineCost
  !         End ! If grn:CurrencyCode
  ! to (DBH 11/25/2005) #6789
            If tmp:ReceivedCurrency <> ''
                Case tmp:ReceivedDivideMultiply
                Of '*'
                    tmp:CurrencyLineCost = (parque:GRNValue / tmp:ReceivedDailyRate) * parque:QuantityReceived
                Of '/'
                    tmp:CurrencyLineCost = (parque:GRNValue / tmp:ReceivedDailyRate) / parque:QuantityReceived
                End ! Case tmp:ReceivedDivideMultiply
                tmp:TotalForeignOrderValue += tmp:CurrencyLineCost
            End ! If tmp:ReceivedCurrency <> ''
  ! End (DBH 11/25/2005) #6789
  
          Print(rpt:Detail:PartsOnOrder)
          tmp:TotalItems      += parque:QuantityReceived
          tmp:TotalLines      += 1
          tmp:TotalOrderValue += tmp:LineCost
  
      End ! Loop x# = 1 To Records(PartsQueue)
  
      ! Show the foreign currency, if it was applied at order creation - TrkBs: 5110 (DBH: 24-05-2005)
      SetTarget(Report)
      ?tmp:ExchangeRate{PROP:Hide}           = True
      ?tmp:TotalForeignOrderValue{prop:Hide} = True
      ?Text:ExchangeRate{PROP:Hide}          = True
      ?Text:TotalOrderValue:Currency{prop:Hide} = True
      If tmp:ReceivedCurrency <> ''
          tmp:ExchangeRate             = tmp:ReceivedDailyRate
          ?Text:ExchangeRate{PROP:Text} = 'Exchange Rate (' & Clip(tmp:ReceivedCurrency) & '):'
          ?Text:TotalOrderValue:Currency{prop:Text} = 'Total Order Value (' & Clip(tmp:ReceivedCurrency) & '):'
          ?tmp:ExchangeRate{PROP:Hide}           = False
          ?tmp:TotalForeignOrderValue{prop:Hide} = False
          ?Text:ExchangeRate{PROP:Hide}          = False
          ?Text:TotalOrderValue:Currency{prop:Hide} = False
      End ! ord:OrderedCurrency <> ''
      SetTarget()
      ! End   - Show the foreign currency, if it was applied at order creation - TrkBs: 5110 (DBH: 24-05-2005)
      Print(rpt:Footer:PartsOnOrder)
  Else ! If Records(PartsQueue)
      ! There are no records for this GRN Number - TrkBs: 5110 (DBH: 01-06-2005)
  End ! If Records(PartsQueue)
  
  ! Start - Update GRN so that this is not is not picked up on the next batch run - TrkBs: 5110 (DBH: 10-06-2005)
  grn:BatchRunNotPrinted = False
  Access:GRNOTES.TryUpdate()
  ! End   - Update GRN so that this is not is not picked up on the next batch run - TrkBs: 5110 (DBH: 10-06-2005)
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName(glo:PDFExportPath)
  SELF.SetDocumentInfo('Goods Received Note','ServiceBase 3g','Batch Reporting','Batch Reporting','ServiceBase 3g','')
  SELF.SetPagesAsParentBookmark(False)
  SELF.CompressText   = True
  SELF.CompressImages = True

