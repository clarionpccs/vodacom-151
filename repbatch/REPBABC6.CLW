  MEMBER('repbatch.clw')

  INCLUDE('ABFILE.INC'),ONCE

  MAP
REPBABC6:DctInit    PROCEDURE
REPBABC6:DctKill    PROCEDURE
REPBABC6:FilesInit  PROCEDURE
  END

Hide:Access:TRDBATCH_ALIAS CLASS(FileManager),TYPE         ! FileManager for TRDBATCH_ALIAS
Init                   PROCEDURE(),DERIVED                 ! Method added to host embed code
Kill                   PROCEDURE(),DERIVED                 ! Method added to host embed code
UseFile                PROCEDURE(BYTE UseType = UseType:Uses),BYTE,PROC,DERIVED ! Method added to host embed code
                     END


Hide:Relate:TRDBATCH_ALIAS CLASS(RelationManager),TYPE     ! RelationManager for TRDBATCH_ALIAS
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED                 ! Method added to host embed code
                     END

Hide:Access:JOBPAYMT_ALIAS CLASS(FileManager),TYPE         ! FileManager for JOBPAYMT_ALIAS
Init                   PROCEDURE(),DERIVED                 ! Method added to host embed code
Kill                   PROCEDURE(),DERIVED                 ! Method added to host embed code
UseFile                PROCEDURE(BYTE UseType = UseType:Uses),BYTE,PROC,DERIVED ! Method added to host embed code
                     END


Hide:Relate:JOBPAYMT_ALIAS CLASS(RelationManager),TYPE     ! RelationManager for JOBPAYMT_ALIAS
Init                   PROCEDURE
DeferedAddRelations    PROCEDURE(),DERIVED                 ! Method added to host embed code
Kill                   PROCEDURE(),DERIVED                 ! Method added to host embed code
                     END

Hide:Access:STOCK_ALIAS CLASS(FileManager),TYPE            ! FileManager for STOCK_ALIAS
Init                   PROCEDURE(),DERIVED                 ! Method added to host embed code
Kill                   PROCEDURE(),DERIVED                 ! Method added to host embed code
UseFile                PROCEDURE(BYTE UseType = UseType:Uses),BYTE,PROC,DERIVED ! Method added to host embed code
                     END


Hide:Relate:STOCK_ALIAS CLASS(RelationManager),TYPE        ! RelationManager for STOCK_ALIAS
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED                 ! Method added to host embed code
                     END

Hide:Access:JOBS_ALIAS CLASS(FileManager),TYPE             ! FileManager for JOBS_ALIAS
Init                   PROCEDURE(),DERIVED                 ! Method added to host embed code
Kill                   PROCEDURE(),DERIVED                 ! Method added to host embed code
UseFile                PROCEDURE(BYTE UseType = UseType:Uses),BYTE,PROC,DERIVED ! Method added to host embed code
                     END


Hide:Relate:JOBS_ALIAS CLASS(RelationManager),TYPE         ! RelationManager for JOBS_ALIAS
Init                   PROCEDURE
DeferedAddRelations    PROCEDURE(),DERIVED                 ! Method added to host embed code
Kill                   PROCEDURE(),DERIVED                 ! Method added to host embed code
                     END


_Hide:Access:TRDBATCH_ALIAS &Hide:Access:TRDBATCH_ALIAS,AUTO,THREAD
_Hide:Relate:TRDBATCH_ALIAS &Hide:Relate:TRDBATCH_ALIAS,AUTO,THREAD
_Hide:Access:JOBPAYMT_ALIAS &Hide:Access:JOBPAYMT_ALIAS,AUTO,THREAD
_Hide:Relate:JOBPAYMT_ALIAS &Hide:Relate:JOBPAYMT_ALIAS,AUTO,THREAD
_Hide:Access:STOCK_ALIAS &Hide:Access:STOCK_ALIAS,AUTO,THREAD
_Hide:Relate:STOCK_ALIAS &Hide:Relate:STOCK_ALIAS,AUTO,THREAD
_Hide:Access:JOBS_ALIAS &Hide:Access:JOBS_ALIAS,AUTO,THREAD
_Hide:Relate:JOBS_ALIAS &Hide:Relate:JOBS_ALIAS,AUTO,THREAD


REPBABC6:DctInit PROCEDURE
  CODE
  _Hide:Access:TRDBATCH_ALIAS &= NEW(Hide:Access:TRDBATCH_ALIAS)
  _Hide:Relate:TRDBATCH_ALIAS &= NEW(Hide:Relate:TRDBATCH_ALIAS)
  _Hide:Access:JOBPAYMT_ALIAS &= NEW(Hide:Access:JOBPAYMT_ALIAS)
  _Hide:Relate:JOBPAYMT_ALIAS &= NEW(Hide:Relate:JOBPAYMT_ALIAS)
  _Hide:Access:STOCK_ALIAS &= NEW(Hide:Access:STOCK_ALIAS)
  _Hide:Relate:STOCK_ALIAS &= NEW(Hide:Relate:STOCK_ALIAS)
  _Hide:Access:JOBS_ALIAS &= NEW(Hide:Access:JOBS_ALIAS)
  _Hide:Relate:JOBS_ALIAS &= NEW(Hide:Relate:JOBS_ALIAS)
  Relate:TRDBATCH_ALIAS &= _Hide:Relate:TRDBATCH_ALIAS
  Relate:JOBPAYMT_ALIAS &= _Hide:Relate:JOBPAYMT_ALIAS
  Relate:STOCK_ALIAS &= _Hide:Relate:STOCK_ALIAS
  Relate:JOBS_ALIAS &= _Hide:Relate:JOBS_ALIAS


REPBABC6:FilesInit PROCEDURE
  CODE
  _Hide:Relate:TRDBATCH_ALIAS.Init
  _Hide:Relate:JOBPAYMT_ALIAS.Init
  _Hide:Relate:STOCK_ALIAS.Init
  _Hide:Relate:JOBS_ALIAS.Init


REPBABC6:DctKill PROCEDURE
  CODE
  _Hide:Relate:TRDBATCH_ALIAS.Kill
  DISPOSE(_Hide:Relate:TRDBATCH_ALIAS)
  _Hide:Relate:JOBPAYMT_ALIAS.Kill
  DISPOSE(_Hide:Relate:JOBPAYMT_ALIAS)
  _Hide:Relate:STOCK_ALIAS.Kill
  DISPOSE(_Hide:Relate:STOCK_ALIAS)
  _Hide:Relate:JOBS_ALIAS.Kill
  DISPOSE(_Hide:Relate:JOBS_ALIAS)


Hide:Relate:TRDBATCH_ALIAS.Init PROCEDURE
  CODE
  _Hide:Access:TRDBATCH_ALIAS.Init
  SELF.Init(Access:TRDBATCH_ALIAS,1)
  SELF.SetAlias(Relate:TRDBATCH)


Hide:Access:TRDBATCH_ALIAS.Init PROCEDURE

  CODE
  SELF.Initialized = False
  SELF.Buffer &= trb_ali:Record
  SELF.AliasedFile &= Access:TRDBATCH                      !This is a File Alias, so assign aliased file manager
  SELF.FileNameValue = 'TRDBATCH_ALIAS'
  SELF.SetErrors(GlobalErrors)
  SELF.File &= TRDBATCH_ALIAS
  PARENT.Init
  Access:TRDBATCH_ALIAS &= SELF


Hide:Access:TRDBATCH_ALIAS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:TRDBATCH_ALIAS &= NULL                            ! File manager has been destroyed to assign null, it is an error to reference this after this point


Hide:Access:TRDBATCH_ALIAS.UseFile PROCEDURE(BYTE UseType = UseType:Uses)

ReturnValue          BYTE,AUTO

  CODE
  IF UseType ~= UseType:Initialize
    SELF.UseFile(UseType:Initialize)                       !Recursive call to ensure initialization takes place
  END
  IF UseType = UseType:Initialize AND ~SELF.Initialized
    SELF.InUseFile = True
    SELF.Init(TRDBATCH_ALIAS,GlobalErrors)
    SELF.Create = 1
    SELF.LockRecover = 10
    SELF.AddKey(trb_ali:RecordNumberKey,'By Record Number',1)
    SELF.AddKey(trb_ali:Batch_Number_Key,'By Job Number',0)
    SELF.AddKey(trb_ali:Batch_Number_Status_Key,'By Batch Number',0)
    SELF.AddKey(trb_ali:BatchOnlyKey,'By Batch Number',0)
    SELF.AddKey(trb_ali:Batch_Company_Status_Key,'By Batch Number',0)
    SELF.AddKey(trb_ali:ESN_Only_Key,'By ESN',0)
    SELF.AddKey(trb_ali:ESN_Status_Key,'By ESN',0)
    SELF.AddKey(trb_ali:Company_Batch_ESN_Key,'By ESN',0)
    SELF.AddKey(trb_ali:AuthorisationKey,'By Authorisation Number',0)
    SELF.AddKey(trb_ali:StatusAuthKey,'By Status',0)
    SELF.AddKey(trb_ali:CompanyDateKey,'By Date',0)
    SELF.AddKey(trb_ali:JobStatusKey,'By Job Number',0)
    SELF.AddKey(trb_ali:JobNumberKey,'By Job Number',0)
    SELF.AddKey(trb_ali:CompanyDespatchedKey,'By Despatch Date',0)
    SELF.AddKey(trb_ali:ReturnDateKey,'By Return Date',0)
    SELF.AddKey(trb_ali:ReturnCompanyKey,'By Company Name',0)
    SELF.AddKey(trb_ali:NotPrintedManJobKey,'By Job Number',0)
    SELF.AddKey(trb_ali:PurchaseOrderKey,'By Job Number',0)
    SELF.AddKey(trb_ali:KeyEVO_Status,'trb_ali:KeyEVO_Status',0)
  END
                                                           !SELF.InUseFile will be set to False in PARENT
  ReturnValue = PARENT.UseFile(UseType)
  RETURN ReturnValue


Hide:Relate:TRDBATCH_ALIAS.Kill PROCEDURE

  CODE
  _Hide:Access:TRDBATCH_ALIAS.Kill                         ! Kill the file manager
  PARENT.Kill
  Relate:TRDBATCH_ALIAS &= NULL                            ! Assign NULL to the RelationManager's FileManager reference, it is an error to reference this after this point
  DISPOSE(_Hide:Access:TRDBATCH_ALIAS)                     ! destroy the file manager


Hide:Relate:JOBPAYMT_ALIAS.Init PROCEDURE
  CODE
  _Hide:Access:JOBPAYMT_ALIAS.Init
  SELF.Init(Access:JOBPAYMT_ALIAS,1)
  SELF.SetAlias(Relate:JOBPAYMT)


Hide:Access:JOBPAYMT_ALIAS.Init PROCEDURE

  CODE
  SELF.Initialized = False
  SELF.Buffer &= jpt_ali:Record
  SELF.AliasedFile &= Access:JOBPAYMT                      !This is a File Alias, so assign aliased file manager
  SELF.FileNameValue = 'JOBPAYMT_ALIAS'
  SELF.SetErrors(GlobalErrors)
  SELF.File &= JOBPAYMT_ALIAS
  PARENT.Init
  Access:JOBPAYMT_ALIAS &= SELF


Hide:Access:JOBPAYMT_ALIAS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBPAYMT_ALIAS &= NULL                            ! File manager has been destroyed to assign null, it is an error to reference this after this point


Hide:Access:JOBPAYMT_ALIAS.UseFile PROCEDURE(BYTE UseType = UseType:Uses)

ReturnValue          BYTE,AUTO

  CODE
  IF UseType ~= UseType:Initialize
    SELF.UseFile(UseType:Initialize)                       !Recursive call to ensure initialization takes place
  END
  IF UseType = UseType:Initialize AND ~SELF.Initialized
    SELF.InUseFile = True
    SELF.Init(JOBPAYMT_ALIAS,GlobalErrors)
    SELF.Create = 1
    SELF.LockRecover = 10
    SELF.AddKey(jpt_ali:Record_Number_Key,'By Record Number',1)
    SELF.AddKey(jpt_ali:All_Date_Key,'By Date',0)
    SELF.AddKey(jpt_ali:Loan_Deposit_Key,'jpt_ali:Loan_Deposit_Key',0)
  END
                                                           !SELF.InUseFile will be set to False in PARENT
  ReturnValue = PARENT.UseFile(UseType)
  RETURN ReturnValue


Hide:Relate:JOBPAYMT_ALIAS.DeferedAddRelations PROCEDURE

  CODE
  DO AddRelations_1
  
  PARENT.DeferedAddRelations

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Relate:JOBPAYMT_ALIAS.Kill PROCEDURE

  CODE
  _Hide:Access:JOBPAYMT_ALIAS.Kill                         ! Kill the file manager
  PARENT.Kill
  Relate:JOBPAYMT_ALIAS &= NULL                            ! Assign NULL to the RelationManager's FileManager reference, it is an error to reference this after this point
  DISPOSE(_Hide:Access:JOBPAYMT_ALIAS)                     ! destroy the file manager


Hide:Relate:STOCK_ALIAS.Init PROCEDURE
  CODE
  _Hide:Access:STOCK_ALIAS.Init
  SELF.Init(Access:STOCK_ALIAS,1)
  SELF.SetAlias(Relate:STOCK)


Hide:Access:STOCK_ALIAS.Init PROCEDURE

  CODE
  SELF.Initialized = False
  SELF.Buffer &= sto_ali:Record
  SELF.AliasedFile &= Access:STOCK                         !This is a File Alias, so assign aliased file manager
  SELF.FileNameValue = 'STOCK_ALIAS'
  SELF.SetErrors(GlobalErrors)
  SELF.File &= STOCK_ALIAS
  PARENT.Init
  Access:STOCK_ALIAS &= SELF


Hide:Access:STOCK_ALIAS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:STOCK_ALIAS &= NULL                               ! File manager has been destroyed to assign null, it is an error to reference this after this point


Hide:Access:STOCK_ALIAS.UseFile PROCEDURE(BYTE UseType = UseType:Uses)

ReturnValue          BYTE,AUTO

  CODE
  IF UseType ~= UseType:Initialize
    SELF.UseFile(UseType:Initialize)                       !Recursive call to ensure initialization takes place
  END
  IF UseType = UseType:Initialize AND ~SELF.Initialized
    SELF.InUseFile = True
    SELF.Init(STOCK_ALIAS,GlobalErrors)
    SELF.Create = 1
    SELF.LockRecover = 10
    SELF.AddKey(sto_ali:Ref_Number_Key,'By Ref Number',1)
    SELF.AddKey(sto_ali:Sundry_Item_Key,'By Sundry Item',0)
    SELF.AddKey(sto_ali:Description_Key,'By Description',0)
    SELF.AddKey(sto_ali:Description_Accessory_Key,'By Description',0)
    SELF.AddKey(sto_ali:Shelf_Location_Key,'By Shelf Location',0)
    SELF.AddKey(sto_ali:Shelf_Location_Accessory_Key,'By Shelf Location',0)
    SELF.AddKey(sto_ali:Supplier_Accessory_Key,'By Supplier',0)
    SELF.AddKey(sto_ali:Manufacturer_Key,'By Manufacturer',0)
    SELF.AddKey(sto_ali:Supplier_Key,'By Supplier',0)
    SELF.AddKey(sto_ali:Location_Key,'By Location',0)
    SELF.AddKey(sto_ali:Part_Number_Accessory_Key,'By Part Number',0)
    SELF.AddKey(sto_ali:Ref_Part_Description_Key,'sto_ali:Ref_Part_Description_Key',0)
    SELF.AddKey(sto_ali:Location_Manufacturer_Key,'By Manufacturer',0)
    SELF.AddKey(sto_ali:Manufacturer_Accessory_Key,'By Part Number',0)
    SELF.AddKey(sto_ali:Location_Part_Description_Key,'By Part Number',0)
    SELF.AddKey(sto_ali:Ref_Part_Description2_Key,'CASCADE Key',0)
    SELF.AddKey(sto_ali:Minimum_Part_Number_Key,'By Part Number',0)
    SELF.AddKey(sto_ali:Minimum_Description_Key,'sto_ali:Minimum_Description_Key',0)
    SELF.AddKey(sto_ali:SecondLocKey,'By Part Number',0)
    SELF.AddKey(sto_ali:RequestedKey,'By Part Number',0)
    SELF.AddKey(sto_ali:ExchangeAccPartKey,'By Part Number',0)
    SELF.AddKey(sto_ali:ExchangeAccDescKey,'By Description',0)
    SELF.AddKey(sto_ali:DateBookedKey,'By Date Booked',0)
    SELF.AddKey(sto_ali:Supplier_Only_Key,'sto_ali:Supplier_Only_Key',0)
    SELF.AddKey(sto_ali:LocPartSuspendKey,'By Part Number',0)
    SELF.AddKey(sto_ali:LocDescSuspendKey,'By Description',0)
    SELF.AddKey(sto_ali:LocShelfSuspendKey,'By Shelf Location',0)
    SELF.AddKey(sto_ali:LocManSuspendKey,'By Part Number',0)
    SELF.AddKey(sto_ali:ExchangeModelKey,'By Model Number',0)
    SELF.AddKey(sto_ali:LoanModelKey,'By Loan Model Number',0)
  END
                                                           !SELF.InUseFile will be set to False in PARENT
  ReturnValue = PARENT.UseFile(UseType)
  RETURN ReturnValue


Hide:Relate:STOCK_ALIAS.Kill PROCEDURE

  CODE
  _Hide:Access:STOCK_ALIAS.Kill                            ! Kill the file manager
  PARENT.Kill
  Relate:STOCK_ALIAS &= NULL                               ! Assign NULL to the RelationManager's FileManager reference, it is an error to reference this after this point
  DISPOSE(_Hide:Access:STOCK_ALIAS)                        ! destroy the file manager


Hide:Relate:JOBS_ALIAS.Init PROCEDURE
  CODE
  _Hide:Access:JOBS_ALIAS.Init
  SELF.Init(Access:JOBS_ALIAS,1)
  SELF.SetAlias(Relate:JOBS)


Hide:Access:JOBS_ALIAS.Init PROCEDURE

  CODE
  SELF.Initialized = False
  SELF.Buffer &= job_ali:Record
  SELF.AliasedFile &= Access:JOBS                          !This is a File Alias, so assign aliased file manager
  SELF.FileNameValue = 'JOBS_ALIAS'
  SELF.SetErrors(GlobalErrors)
  SELF.File &= JOBS_ALIAS
  PARENT.Init
  Access:JOBS_ALIAS &= SELF


Hide:Access:JOBS_ALIAS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBS_ALIAS &= NULL                                ! File manager has been destroyed to assign null, it is an error to reference this after this point


Hide:Access:JOBS_ALIAS.UseFile PROCEDURE(BYTE UseType = UseType:Uses)

ReturnValue          BYTE,AUTO

  CODE
  IF UseType ~= UseType:Initialize
    SELF.UseFile(UseType:Initialize)                       !Recursive call to ensure initialization takes place
  END
  IF UseType = UseType:Initialize AND ~SELF.Initialized
    SELF.InUseFile = True
    SELF.Init(JOBS_ALIAS,GlobalErrors)
    SELF.Create = 1
    SELF.LockRecover = 10
    SELF.AddKey(job_ali:Ref_Number_Key,'By Job Number',1)
    SELF.AddKey(job_ali:Model_Unit_Key,'By Job Number',0)
    SELF.AddKey(job_ali:EngCompKey,'By Job Number',0)
    SELF.AddKey(job_ali:EngWorkKey,'By Job Number',0)
    SELF.AddKey(job_ali:Surname_Key,'By Surname',0)
    SELF.AddKey(job_ali:MobileNumberKey,'By Mobile Number',0)
    SELF.AddKey(job_ali:ESN_Key,'By E.S.N. / I.M.E.I.',0)
    SELF.AddKey(job_ali:MSN_Key,'By M.S.N.',0)
    SELF.AddKey(job_ali:AccountNumberKey,'By Account Number',0)
    SELF.AddKey(job_ali:AccOrdNoKey,'By Order Number',0)
    SELF.AddKey(job_ali:Model_Number_Key,'By Model Number',0)
    SELF.AddKey(job_ali:Engineer_Key,'By Engineer',0)
    SELF.AddKey(job_ali:Date_Booked_Key,'By Date Booked',0)
    SELF.AddKey(job_ali:DateCompletedKey,'By Date Completed',0)
    SELF.AddKey(job_ali:ModelCompKey,'By Completed Date',0)
    SELF.AddKey(job_ali:By_Status,'By Job Number',0)
    SELF.AddKey(job_ali:StatusLocKey,'By Job Number',0)
    SELF.AddKey(job_ali:Location_Key,'By Location',0)
    SELF.AddKey(job_ali:Third_Party_Key,'By Third Party',0)
    SELF.AddKey(job_ali:ThirdEsnKey,'By ESN',0)
    SELF.AddKey(job_ali:ThirdMsnKey,'By MSN',0)
    SELF.AddKey(job_ali:PriorityTypeKey,'By Priority',0)
    SELF.AddKey(job_ali:Unit_Type_Key,'By Unit Type',0)
    SELF.AddKey(job_ali:EDI_Key,'By Job Number',0)
    SELF.AddKey(job_ali:InvoiceNumberKey,'By Invoice_Number',0)
    SELF.AddKey(job_ali:WarInvoiceNoKey,'By Invoice Number',0)
    SELF.AddKey(job_ali:Batch_Number_Key,'By Job Number',0)
    SELF.AddKey(job_ali:Batch_Status_Key,'By Ref Number',0)
    SELF.AddKey(job_ali:BatchModelNoKey,'By Job Number',0)
    SELF.AddKey(job_ali:BatchInvoicedKey,'By Ref Number',0)
    SELF.AddKey(job_ali:BatchCompKey,'By Job Number',0)
    SELF.AddKey(job_ali:ChaInvoiceKey,'job_ali:ChaInvoiceKey',0)
    SELF.AddKey(job_ali:InvoiceExceptKey,'By Job Number',0)
    SELF.AddKey(job_ali:ConsignmentNoKey,'By Cosignment Number',0)
    SELF.AddKey(job_ali:InConsignKey,'By Consignment Number',0)
    SELF.AddKey(job_ali:ReadyToDespKey,'By Job Number',0)
    SELF.AddKey(job_ali:ReadyToTradeKey,'By Job Number',0)
    SELF.AddKey(job_ali:ReadyToCouKey,'By Job Number',0)
    SELF.AddKey(job_ali:ReadyToAllKey,'By Job Number',0)
    SELF.AddKey(job_ali:DespJobNumberKey,'By Job Number',0)
    SELF.AddKey(job_ali:DateDespatchKey,'By Job Number',0)
    SELF.AddKey(job_ali:DateDespLoaKey,'By Job Number',0)
    SELF.AddKey(job_ali:DateDespExcKey,'By Job Number',0)
    SELF.AddKey(job_ali:ChaRepTypeKey,'By Chargeable Repair Type',0)
    SELF.AddKey(job_ali:WarRepTypeKey,'By Warranty Repair Type',0)
    SELF.AddKey(job_ali:ChaTypeKey,'job_ali:ChaTypeKey',0)
    SELF.AddKey(job_ali:WarChaTypeKey,'job_ali:WarChaTypeKey',0)
    SELF.AddKey(job_ali:Bouncer_Key,'By Job Number',0)
    SELF.AddKey(job_ali:EngDateCompKey,'By Date Completed',0)
    SELF.AddKey(job_ali:ExcStatusKey,'By Job Number',0)
    SELF.AddKey(job_ali:LoanStatusKey,'By Job Number',0)
    SELF.AddKey(job_ali:ExchangeLocKey,'By Job Number',0)
    SELF.AddKey(job_ali:LoanLocKey,'By Job Number',0)
    SELF.AddKey(job_ali:BatchJobKey,'By Job Number',0)
    SELF.AddKey(job_ali:BatchStatusKey,'By Job Number',0)
  END
                                                           !SELF.InUseFile will be set to False in PARENT
  ReturnValue = PARENT.UseFile(UseType)
  RETURN ReturnValue


Hide:Relate:JOBS_ALIAS.DeferedAddRelations PROCEDURE

  CODE
  DO AddRelations_1
  
  PARENT.DeferedAddRelations

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBSOBF_ALIAS,RI:CASCADE,RI:CASCADE,jofali:RefNumberKey)
  SELF.AddRelationLink(job_ali:Ref_Number,jofali:RefNumber)

  SELF.AddRelation(Relate:BOUNCER,RI:CASCADE,RI:CASCADE,bou:Bouncer_Job_Only_Key)
  SELF.AddRelationLink(job_ali:Ref_Number,bou:Bouncer_Job_Number)

  SELF.AddRelation(Relate:REPAIRTY)


Hide:Relate:JOBS_ALIAS.Kill PROCEDURE

  CODE
  _Hide:Access:JOBS_ALIAS.Kill                             ! Kill the file manager
  PARENT.Kill
  Relate:JOBS_ALIAS &= NULL                                ! Assign NULL to the RelationManager's FileManager reference, it is an error to reference this after this point
  DISPOSE(_Hide:Access:JOBS_ALIAS)                         ! destroy the file manager

