

   MEMBER('repbatch.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPRPDF.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE
   INCLUDE('ABRPPSEL.INC'),ONCE

                     MAP
                       INCLUDE('REPBA002.INC'),ONCE        !Local module procedure declarations
                     END


PartsOrder PROCEDURE                                       ! Generated from procedure template - Report

Progress:Thermometer BYTE                                  !
tmp:True             BYTE(1)                               !True
tmp:One              LONG(1)                               !One
tmp:Hundred          LONG(10)                              !Hundred
tmp:LineCost         REAL                                  !Line Cost
tmp:TotalOrderValue  REAL                                  !Total Order Value
tmp:TotalItems       LONG(0)                               !Total Items
tmp:TotalLines       LONG(-1)                              !Total Lines
tmp:ExchangeRate     STRING(30)                            !Exchange Rate
tmp:TotalForeignOrderValue STRING(30)                      !Total Foreign Order Value
tmp:LineCount        STRING(20)                            !Line Count
PartsQueue           QUEUE,PRE(parque)                     !
PartNumber           STRING(30)                            !Part Number
Description          STRING(30)                            !Description
QuantityOrdered      LONG                                  !Quantity Ordered
ItemCost             REAL                                  !Item Cost
CurrencyItemCost     REAL                                  !Currency Item Cost
Currency             STRING(3)                             !Currency
                     END                                   !
tmp:ShelfLocation    STRING(30)                            !Shelf Location
tmp:CurrencyLineCost STRING(30)                            !Currency Line Cost
tmp:CurrencyItemCost STRING(30)                            !Currency Item Cost
tmp:ItemCost         STRING(30)                            !Item Cost
Process:View         VIEW(ORDERS)
                       PROJECT(ord:BatchRunNotPrinted)
                       PROJECT(ord:Date)
                       PROJECT(ord:Order_Number)
                     END
ProgressWindow       WINDOW('Report ORDERS'),AT(,,142,59),FONT('Tahoma',,0190H,),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(46,42,49,15),USE(?Progress:Cancel),FLAT,LEFT,MSG('Cancel Report'),TIP('Cancel Report'),ICON('WACANCEL.ICO')
                     END

Report               REPORT('ORDERS Report'),AT(250,4323,7750,4458),PAPER(PAPER:A4),PRE(RPT),FONT('Tahoma',8,COLOR:Black,FONT:regular),THOUS
                       HEADER,AT(240,250,7750,2969)
                         STRING(@s30),AT(104,0),USE(def:OrderCompanyName),TRN,LEFT,FONT('Tahoma',16,,FONT:bold)
                         STRING(@s30),AT(104,260),USE(def:OrderAddressLine1),TRN,LEFT,FONT('Tahoma',8,,FONT:regular)
                         STRING(@s30),AT(104,417),USE(def:OrderAddressLine2),TRN,LEFT,FONT('Tahoma',8,,FONT:regular)
                         STRING(@s30),AT(104,573),USE(def:OrderAddressLine3),TRN,LEFT,FONT('Tahoma',8,,FONT:regular)
                         STRING(@s30),AT(104,729),USE(def:OrderPostcode),TRN,LEFT,FONT('Tahoma',8,,FONT:regular)
                         STRING('Date Printed:'),AT(5313,792),USE(?ReportDatePrompt),TRN,FONT(,,,FONT:bold)
                         STRING('<<-- Date Stamp -->'),AT(6198,792),USE(?ReportDateStamp),TRN
                         STRING('Tel:'),AT(104,885),USE(?String22),TRN
                         STRING(@s30),AT(469,885),USE(def:OrderTelephoneNumber),TRN,LEFT,FONT('Tahoma',8,,FONT:regular)
                         STRING('Fax:'),AT(104,1042),USE(?String22:2),TRN
                         STRING(@s30),AT(469,1042),USE(def:OrderFaxNumber),TRN,LEFT,FONT('Tahoma',8,,FONT:regular)
                         STRING('Email:'),AT(104,1198),USE(?String22:3),TRN
                         STRING(@s255),AT(469,1198),USE(def:OrderEmailAddress),TRN,LEFT
                         STRING(@s30),AT(156,1771),USE(sup:Company_Name),TRN
                         STRING(@s30),AT(156,1927),USE(sup:Address_Line1),TRN
                         STRING(@s30),AT(156,2083),USE(sup:Address_Line2),TRN
                         STRING(@s30),AT(4167,1771),USE(def:OrderCompanyName,,?def:OrderCompanyName:2),TRN
                         STRING(@s30),AT(156,2240),USE(sup:Address_Line3),TRN
                         STRING(@s30),AT(4167,1927),USE(def:OrderAddressLine1,,?def:OrderAddressLine1:2),TRN
                         STRING(@s10),AT(156,2396),USE(sup:Postcode),TRN
                         STRING(@s30),AT(4167,2083),USE(def:OrderAddressLine2,,?def:OrderAddressLine2:2),TRN
                         STRING('Tel:'),AT(156,2552),USE(?Text:SupplierTelephone),TRN
                         STRING(@s15),AT(417,2552),USE(sup:Telephone_Number),TRN
                         STRING(@s30),AT(4167,2240),USE(def:OrderAddressLine3,,?def:OrderAddressLine3:2),TRN
                         STRING('Fax:'),AT(156,2708),USE(?Text:SupplierFax),TRN
                         STRING(@s15),AT(417,2708),USE(sup:Fax_Number),TRN
                         STRING(@s30),AT(4167,2396),USE(def:OrderPostcode,,?def:OrderPostcode:2),TRN
                         STRING('Tel:'),AT(4167,2552),USE(?Text:DeliveryTelephone),TRN
                         STRING(@s30),AT(4427,2552),USE(def:OrderTelephoneNumber,,?def:OrderTelephoneNumber:2),TRN
                         STRING('Fax:'),AT(4167,2708),USE(?Text:DeliveryFax),TRN
                         STRING(@s30),AT(4427,2708),USE(def:OrderFaxNumber,,?def:OrderFaxNumber:2),TRN
                       END
Detail:PartsOnOrder    DETAIL,AT(,,,219),USE(?Detail:PartsOnOrder)
                         STRING(@s6),AT(125,-10),USE(parque:QuantityOrdered),TRN,RIGHT,FONT(,7,,)
                         STRING(@s15),AT(625,0),USE(parque:PartNumber),TRN,FONT(,7,,)
                         STRING(@s30),AT(1667,0,990,156),USE(tmp:ShelfLocation),TRN,LEFT,FONT('Tahoma',7,,)
                         STRING(@s30),AT(2760,0),USE(parque:Description),TRN,FONT(,7,,)
                         STRING(@n7.2),AT(5260,0,510,146),USE(tmp:ItemCost),TRN,RIGHT,FONT(,7,,)
                         STRING(@n14.2),AT(5833,0,,146),USE(tmp:CurrencyLineCost),TRN,RIGHT,FONT(,7,,)
                         STRING(@n14.2),AT(6677,0),USE(tmp:LineCost),TRN,RIGHT,FONT(,7,,)
                         STRING(@n7.2),AT(4479,0,510,146),USE(tmp:CurrencyItemCost),TRN,RIGHT,FONT(,7,,)
                       END
Footer:PartsOnOrder    DETAIL,PAGEAFTER(-1),AT(250,250,7750,11188),USE(?Footer:PartsOnOrder),ABSOLUTE
                         STRING('Exchange Rate (CUR):'),AT(4948,8802),USE(?Text:ExchangeRate),TRN,FONT(,8,,FONT:bold)
                         STRING('Total Items:'),AT(208,8854),USE(?Text:TotalItems),TRN,FONT(,8,,FONT:bold)
                         STRING(@s8),AT(1354,8854),USE(tmp:TotalItems),TRN,RIGHT,FONT(,8,,)
                         STRING(@n12.4),AT(6646,8802,,208),USE(tmp:ExchangeRate),TRN,RIGHT,FONT('Tahoma',8,,)
                         STRING(@s8),AT(1354,9323),USE(tmp:TotalLines),TRN,RIGHT,FONT(,8,,)
                         STRING('Total Order Value (CUR):'),AT(4948,9323),USE(?Text:TotalOrderValue:Currency),TRN,FONT(,8,,FONT:bold)
                         STRING(@n14.2),AT(6521,9323),USE(tmp:TotalForeignOrderValue),TRN,RIGHT,FONT('Tahoma',8,,)
                         STRING('Total Order Value (R):'),AT(4948,9063),USE(?Text:TotalOrderValue),TRN,FONT(,8,,FONT:bold)
                         STRING(@n14.2),AT(6521,9063),USE(tmp:TotalOrderValue),TRN,RIGHT,FONT(,8,,)
                         STRING('Total Lines:'),AT(208,9323),USE(?Text:TotalLines),TRN,FONT(,8,,FONT:bold)
                       END
Footer:Continue        DETAIL,PAGEAFTER(-1),AT(250,250,7750,11188),USE(?Footer:Continue),ABSOLUTE
                         STRING('Continued.....'),AT(6667,9063),USE(?Text:Continued),TRN,FONT(,10,,FONT:bold)
                       END
Header:Titles          DETAIL,AT(250,250,7750,),USE(?Header:Titles),ABSOLUTE
                         STRING(@s15),AT(156,3438),USE(sup:Account_Number),TRN
                         STRING(@n~SS~010),AT(1667,3438),USE(ord:Order_Number),TRN
                         STRING(@d6),AT(3177,3438),USE(ord:Date),TRN,LEFT
                         STRING(@s60),AT(4688,3438,1406,156),USE(sup:Contact_Name),TRN,FONT(,7,,)
                         STRING(@s15),AT(6198,3438),USE(sup:Telephone_Number,,?sup:Telephone_Number:2),TRN
                         STRING('Qty'),AT(188,3854),USE(?Text:Quantity),TRN,FONT(,7,,FONT:bold)
                         STRING('Part Number'),AT(625,3854),USE(?Text:PartNumber),TRN,FONT(,7,,FONT:bold)
                         STRING('Description'),AT(2760,3854),USE(?Text:Description),TRN,FONT(,7,,FONT:bold)
                         STRING('Item Cost (R)'),AT(5156,3854),USE(?Text:ItemCost),TRN,RIGHT,FONT(,7,,FONT:bold)
                         STRING('Line Cost (CUR)'),AT(5990,3854),USE(?Text:CurrencyLineCost),TRN,RIGHT,FONT(,7,,FONT:bold)
                         STRING('Line Cost (R)'),AT(6927,3854),USE(?Text:LineCost),TRN,RIGHT,FONT(,7,,FONT:bold)
                         STRING('Item Cost (CUR)'),AT(4219,3854),USE(?Text:CurrencyItemCost),TRN,RIGHT,FONT(,7,,FONT:bold)
                         STRING('Shelf Location'),AT(1667,3854),USE(?Text:ShelfLocation),TRN,FONT(,7,,FONT:bold)
                       END
                       FOOTER,AT(250,9958,7750,1490),USE(?OrderFooter)
                         TEXT,AT(104,52,7552,1250),USE(stt:Text),BOXED,TRN,FONT(,8,,),RESIZE
                       END
                       FORM,AT(250,250,7750,11188),USE(?Text:CurrencyItemCost:2),FONT('Tahoma',8,,FONT:regular)
                         BOX,AT(5052,417,2650,1042),USE(?BoxGrey:TopDetails),ROUND,FILL(COLOR:Silver)
                         BOX,AT(156,1719,3600,1300),USE(?BoxGrey:Address1),ROUND,FILL(COLOR:Silver)
                         BOX,AT(4115,1719,3600,1300),USE(?BoxGrey:Address2),ROUND,FILL(COLOR:Silver)
                         BOX,AT(156,3177,7552,521),USE(?BoxGrey:Title),ROUND,FILL(COLOR:Silver)
                         BOX,AT(156,3854,7552,4740),USE(?BoxGrey:Detail),ROUND,FILL(COLOR:Silver)
                         BOX,AT(156,8750,2813,1042),USE(?BoxGrey:Total1),ROUND,FILL(COLOR:Silver)
                         BOX,AT(4896,8750,2813,1042),USE(?BoxGrey:Total2),ROUND,FILL(COLOR:Silver)
                         BOX,AT(5000,365,2650,1042),USE(?Box:TopDetails),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         BOX,AT(104,1667,3600,1300),USE(?Box:Address1),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(4063,1667,3600,1300),USE(?Box:Address2),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(104,3125,1510,260),USE(?Box:Title1),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         BOX,AT(1615,3125,1510,260),USE(?Box:Title2),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         BOX,AT(3125,3125,1510,260),USE(?Box:Title3),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         BOX,AT(4635,3125,1510,260),USE(?Box:Title4),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         BOX,AT(6146,3125,1510,260),USE(?Box:Title5),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         BOX,AT(104,3385,1510,260),USE(?Box:Title1a),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(1615,3385,1510,260),USE(?Box:Title2a),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(3125,3385,1510,260),USE(?Box:Title3a),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(4635,3385,1510,260),USE(?Box:Title4a),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(6146,3385,1510,260),USE(?Box:Title5a),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(104,3802,7552,260),USE(?Text:Quantity:2),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         BOX,AT(104,4063,7552,4479),USE(?Box:Detail),ROUND,COLOR(COLOR:Black),FILL(COLOR:White)
                         BOX,AT(104,8698,2813,1042),USE(?Box:Total1),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         BOX,AT(4844,8698,2813,1042),USE(?Box:Total2),ROUND,COLOR(COLOR:Black),FILL(0C2E3F3H)
                         STRING('OFFICIAL PARTS ORDER'),AT(4948,52),USE(?Text:OfficialPartsOrder),TRN,FONT(,16,,FONT:bold)
                         STRING('SUPPLIER ADDRESS'),AT(156,1510),USE(?Text:SupplierAddress),TRN,FONT(,,,FONT:bold)
                         STRING('DELIVERY ADDRESS'),AT(4115,1510),USE(?Text:DeliveryAddress),TRN,FONT(,,,FONT:bold)
                         STRING('Account Number'),AT(156,3177),USE(?Text:AccountNumber),TRN,FONT(,,,FONT:bold)
                         STRING('Order Number'),AT(1667,3177),USE(?Text:OrderNumber),TRN,FONT(,,,FONT:bold)
                         STRING('Order Date'),AT(3177,3177),USE(?Text:OrderDate),TRN,FONT(,,,FONT:bold)
                         STRING('Contact'),AT(4688,3177),USE(?Text:Contact),TRN,FONT(,,,FONT:bold)
                         STRING('Contact Number'),AT(6198,3177),USE(?Text:ContactNumber),TRN,FONT(,,,FONT:bold)
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeNoRecords          PROCEDURE(),DERIVED                 ! Method added to host embed code
                     END

ThisReport           CLASS(ProcessClass)                   ! Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

ProgressMgr          StepLongClass                         ! Progress Manager
Previewer            PrintPreviewClass                     ! Print Previewer
TargetSelector       ReportTargetSelectorClass             ! Report Target Selector
PDFReporter          CLASS(PDFReportGenerator)             ! PDF
SetUp                  PROCEDURE(),DERIVED                 ! Method added to host embed code
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('PartsOrder')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:CURRENCY.Open                                     ! File CURRENCY used by this procedure, so make sure it's RelationManager is open
  Relate:DEFAULTS.Open                                     ! File DEFAULTS used by this procedure, so make sure it's RelationManager is open
  Relate:ORDERS.SetOpenRelated()
  Relate:ORDERS.Open                                       ! File ORDERS used by this procedure, so make sure it's RelationManager is open
  Relate:STANTEXT.Open                                     ! File STANTEXT used by this procedure, so make sure it's RelationManager is open
  Relate:STOCK_ALIAS.Open                                  ! File STOCK_ALIAS used by this procedure, so make sure it's RelationManager is open
  Access:SUPPLIER.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  
  Access:STANTEXT.ClearKey(stt:Description_Key)
  stt:Description = 'PARTS ORDER'
  If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
      !Found
  Else ! Access:STANTEXT.TryFetch(stt:Description_Key)
      !Error
  End ! Access:STANTEXT.TryFetch(stt:Description_Key)
  
  SELF.Open(ProgressWindow)                                ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('PartsOrder',ProgressWindow)                ! Restore window settings from non-volatile store
  TargetSelector.AddItem(PDFReporter.IReportGenerator)
  SELF.SetReportTarget(PDFReporter.IReportGenerator)
  SELF.AddItem(TargetSelector)
  ProgressWindow{Prop:Timer} = 10                          ! Assign timer interval
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:ORDERS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, ord:Order_Number)
  ThisReport.AddSortOrder(ord:NotPrintedOrderKey)
  ThisReport.AddRange(ord:BatchRunNotPrinted,tmp:True)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,Report,Previewer)
  ?Progress:UserString{Prop:Text}=''
  Relate:ORDERS.SetQuickScan(1,Propagate:OneMany)
  SELF.SkipPreview = True
  Previewer.SetINIManager(INIMgr)
  Previewer.AllowUserZoom=True
  Previewer.Maximize=True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CURRENCY.Close
    Relate:DEFAULTS.Close
    Relate:ORDERS.Close
    Relate:STANTEXT.Close
    Relate:STOCK_ALIAS.Close
  END
  IF SELF.Opened
    INIMgr.Update('PartsOrder',ProgressWindow)             ! Save window data to non-volatile store
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  SYSTEM{PROP:PrintMode} = 3
  ReturnValue = PARENT.OpenReport()
  IF ReturnValue = Level:Benign
    SELF.Report $ ?ReportDateStamp{PROP:Text}=FORMAT(TODAY(),@D17)
  END
  IF ReturnValue = Level:Benign
    SELF.Report{PROPPRINT:Extend}=True
  END
  RETURN ReturnValue


ThisWindow.TakeNoRecords PROCEDURE

  CODE
      Return
  PARENT.TakeNoRecords


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  tmp:LineCost = orp:Sale_Cost * orp:Quantity
  Access:SUPPLIER.ClearKey(sup:Company_Name_Key)
  sup:Company_Name = ord:Supplier
  If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
      !Found
  
  Else !If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
      !Error
  End !If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
  
  
  ReturnValue = PARENT.TakeRecord()
  Clear(PartsQueue)
  Free(PartsQueue)
  Access:ORDPARTS.ClearKey(orp:Part_Number_Key)
  orp:Order_Number = ord:Order_Number
  Set(orp:Part_Number_Key, orp:Part_Number_Key)
  Loop
      If Access:ORDPARTS.NEXT()
          Break
      End ! If
      If orp:Order_Number <> ord:Order_Number      |
          Then Break   ! End If
      End ! If
      parque:PartNumber = orp:Part_Number
      parque:ItemCost   = orp:Purchase_Cost
      parque:Currency   = orp:OrderedCurrency
      Get(PartsQueue, parque:PartNumber, parque:ItemCost, parque:Currency)
      If Error()
          parque:PartNumber      = orp:Part_Number
          parque:Description     = orp:Description
          parque:QuantityOrdered = orp:Quantity
          parque:ItemCost        = orp:Purchase_Cost
          If orp:OrderedCurrency <> ''
              Case orp:OrderedDivideMultiply
                  Of '*'
                      parque:CurrencyItemCost = orp:Purchase_Cost / orp:OrderedDailyRate
                  Of '/'
                      parque:CurrencyItemCost = orp:Purchase_Cost * orp:OrderedDailyRate
              End ! Case orp:OrderedDivideMultiply
              parque:Currency = orp:OrderedCurrency
          Else
              parque:CurrencyItemCost = 0
              parque:Currency = ''
          End ! If orp:OrderedCurrency <> ''
          Add(PartsQueue)
      Else ! If Error()
          parque:QuantityOrdered += orp:Quantity
          Put(PartsQueue)
      End ! If Error()
  End ! Loop
  
  Count# = 0
  If Records(PartsQueue)
      ! Show different title if currency used - TrkBs: 6441 (DBH: 03-10-2005)
      SetTarget(Report)
      If ord:OrderedCurrency <> ''
          ?Text:CurrencyItemCost{prop:Hide} = False
          ?Text:CurrencyLineCost{prop:Hide} = False
          ?tmp:CurrencyItemCost{prop:Hide} = False
          ?tmp:CurrencyLineCost{prop:Hide} = False
  
          ?Text:CurrencyItemCost{prop:Text} = 'Item Cost (' & Clip(ord:OrderedCurrency) & ')'
          ?Text:CurrencyLineCost{prop:Text} = 'Line Cost (' & Clip(ord:OrderedCurrency) & ')'
      Else ! If ord:OrderedCurrency <> ''
          ?Text:CurrencyItemCost{prop:Hide} = True
          ?Text:CurrencyLineCost{prop:Hide} = True
          ?tmp:CurrencyItemCost{prop:Hide} = True
          ?tmp:CurrencyLineCost{prop:Hide} = True
      End ! If ord:OrderedCurrency <> ''
      SetTarget()
      Print(rpt:Header:Titles)
  
      tmp:TotalOrderValue        = 0
      tmp:TotalItems             = 0
      tmp:TotalLines             = 0
      tmp:TotalForeignOrderValue = 0
      Sort(PartsQueue,parque:PartNumber)
      Loop x# = 1 To Records(PartsQueue)
          Get(PartsQueue, x#)
          Count#              += 1
  
          If Count# > 20
              Print(rpt:Footer:Continue)
              Print(rpt:Header:Titles)
              Count# = 0
          End ! If Count# >= 27If Count# >= 27
  
          tmp:LineCost         = parque:QuantityOrdered * parque:ItemCost
          tmp:CurrencyLineCost = parque:QuantityOrdered * parque:CurrencyItemCost
          tmp:CurrencyItemCost = parque:CurrencyItemCost
          tmp:ItemCost         = parque:ItemCost
          tmp:TotalOrderValue += tmp:LineCost
          tmp:TotalLines      += 1
          tmp:TotalItems      += orp:Quantity
          tmp:TotalForeignOrderValue += tmp:CurrencyLineCost
  
          ! Start - Show the Shelf Location - TrkBs: 6264 (DBH: 07-09-2005)
          Access:STOCK_ALIAS.ClearKey(sto_ali:Location_Key)
          sto_ali:Location    = 'MAIN STORE'
          sto_ali:Part_Number = parque:PartNumber
          If Access:STOCK_ALIAS.TryFetch(sto_ali:Location_Key) = Level:Benign
              !Found
              tmp:ShelfLocation = sto_ali:Shelf_Location
          Else !If Access:STOCK_ALIAS.TryFetch(sto_ali:Location_Key) = Level:Benign
              !Error
              tmp:ShelfLocation = ''
          End !If Access:STOCK_ALIAS.TryFetch(sto_ali:Location_Key) = Level:Benign
          ! End   - Show the Shelf Location - TrkBs: 6264 (DBH: 07-09-2005)
  
          Print(rpt:Detail:PartsOnOrder)
      End ! Loop x# = 1 To Records(PartsQueue)
      ! Show the foreign currency, if it was applied at order creation - TrkBs: 5110 (DBH: 24-05-2005)
      SetTarget(Report)
      ?tmp:ExchangeRate{PROP:Hide}           = True
      ?tmp:TotalForeignOrderValue{PROP:Hide} = True
      ?Text:ExchangeRate{PROP:Hide}          = True
      ?Text:TotalOrderValue:Currency{prop:Hide} = True
      If ord:OrderedCurrency <> ''
          tmp:ExchangeRate             = ord:OrderedDailyRate
          ?Text:ExchangeRate{PROP:Text} = 'Exchange Rate (' & Clip(ord:OrderedCurrency) & '):'
          ?Text:TotalOrderValue:Currency{prop:Text} = 'Total Order Value (' & Clip(ord:OrderedCurrency) & '):'
          ?tmp:ExchangeRate{PROP:Hide}           = False
          ?tmp:TotalForeignOrderValue{PROP:Hide} = False
          ?Text:ExchangeRate{PROP:Hide}          = False
          ?Text:TotalOrderValue:Currency{prop:Hide} = False
      End ! ord:OrderedCurrency <> ''
      SetTarget()
      ! End   - Show the foreign currency, if it was applied at order creation - TrkBs: 5110 (DBH: 24-05-2005)Q
      Print(rpt:Footer:PartsOnOrder)
  End ! If Records(PartsQueue)If Records(PartsQueue)
  
  ! Start - Update order so is not included on next batch run - TrkBs: 5110 (DBH: 10-06-2005)
  ord:BatchRunNotPrinted = False
  Access:ORDERS.TryUpdate()
  ! End   - Update order so is not included on next batch run - TrkBs: 5110 (DBH: 10-06-2005)
  RETURN ReturnValue


PDFReporter.SetUp PROCEDURE

  CODE
  PARENT.SetUp
  SELF.SetFileName(glo:PDFExportPath)
  SELF.SetDocumentInfo('Parts Orders','ServiceBase 3g','Batch Reporting','Batch Reporting','ServiceBase 3g','')
  SELF.SetPagesAsParentBookmark(True)
  SELF.CompressText   = True
  SELF.CompressImages = True

