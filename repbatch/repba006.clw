

   MEMBER('repbatch.clw')                                  ! This is a MEMBER module

                     MAP
                       INCLUDE('REPBA006.INC'),ONCE        !Local module procedure declarations
                     END


CreateOrdersFromARCRequest PROCEDURE                       ! Declare Procedure
tmp:VatRate          REAL                                  !
Count                LONG                                  !
LocalJobNo           LONG                                  !
LocalQuantity        LONG                                  !
  CODE
   Relate:RETDESNO.Open
   Relate:INVOICE.Open
   Relate:DEFAULTS.Open
   Relate:RETSTOCK.Open
   Relate:VATCODE.Open
   Relate:TRADEACC.Open
   Relate:STOCK.Open
   Relate:STOHISTE.Open
   Relate:RETSALES.Open
   Relate:STOHIST.Open
    !CreateOrdersFromARCRequest

    !ensure global queue is free
    free(glo:q_JobNumber)

    !get a list of which Stohiste we can use
    Access:STOHISTE.ClearKey(stoe:KeyArcStatus)
    stoe:ARC_Status = 'R'
    set(stoe:KeyArcStatus,stoe:KeyArcStatus)
    LOOP
        if access:STOHISTE.next() then break.
        if stoe:ARC_Status <> 'R' then break.

        !Remember the record number to be used in makesales
        glo:q_JobNumber.GLO:Job_Number_Pointer = stoe:RecordNumber
        ADD(glo:q_JobNumber)
    END

    if records(glo:Q_Jobnumber) THEN
        do MakeSales
    END

    free(glo:q_JobNumber)
   Relate:RETDESNO.Close
   Relate:INVOICE.Close
   Relate:DEFAULTS.Close
   Relate:RETSTOCK.Close
   Relate:VATCODE.Close
   Relate:TRADEACC.Close
   Relate:STOCK.Close
   Relate:STOHISTE.Close
   Relate:RETSALES.Close
   Relate:STOHIST.Close
MakeSales   routine

    !these sales can only be from AA20 - they are created when glo:RelocateStore is true
    Access:tradeacc.clearkey(tra:Account_Number_Key)
    tra:Account_Number = 'AA20'
    if access:Tradeacc.fetch(tra:Account_Number_Key) then exit.

    Set(Defaults)
    access:Defaults.next()

    !message('Got trade account AA20')
    !Ok got the trade account details - will use them several times in the retail sale
    Access:VATCODE.Clearkey(vat:Vat_Code_Key)
    vat:Vat_Code    = tra:Retail_Vat_Code
    If Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign
        !Found
        tmp:VatRate = vat:Vat_Rate
    ELSE
        tmp:VatRate = 0
    End! If Access:VATCODE.Tryfetch(vat:Vat_Code_Key) = Level:Benign

    LOOP Count = 1 to records(glo:q_JobNumber)

        get(glo:q_JobNumber,Count)

        Access:stohiste.clearkey(stoe:RecordNumberKey)
        stoe:RecordNumber = glo:q_JobNumber.GLO:Job_Number_Pointer
        if access:Stohiste.fetch(stoe:RecordNumberKey) then cycle.      !how can this error??

        !fetch the stohist record for more details
        Access:STOHIST.ClearKey(shi:record_number_key)
        shi:Record_Number = stoe:SHIRecordNumber
        if access:STOHIST.fetch(shi:record_number_key)=Level:Benign

            !found the stock hist - remember the job number and quanity for the new stock history later
            LocalJobNo = shi:Job_Number
            LocalQuantity = shi:Quantity

            Access:Stock.clearkey(sto:Ref_Number_Key)
            sto:Ref_Number = shi:Ref_Number
            if access:Stock.fetch(sto:Ref_Number_Key)=Level:Benign
                !now we have the trade account and the stock for this sale
                !message('Stock fetch worked OK')
                do GenerateTradeSale

                !now clear up the stohiste record - refetch this as we have generated another in the code
                Access:Stohiste.clearkey(stoe:RecordNumberKey)
                stoe:RecordNumber = glo:q_JobNumber.GLO:Job_Number_Pointer
                if access:StoHiste.fetch(stoe:RecordNumberKey) = Level:Benign
                    stoe:ARC_Status = 'S'
                    access:Stohiste.update()
                END !if stohiste found

            ELSE
                !message('fetch did not work for stock')
            END !if stock found
        ELSE
            !message('fetch did not work for stock history')
        END !if stohist found

    END !loop through glo:q_JobNumber


GenerateTradeSale   ROUTINE

    !message('About to try and generate trade sale')

    IF Access:RETSALES.PrimeRecord() = Level:Benign
        !message('prime record worked for retsales')
        ret:Who_Booked = 'XXX'      !use:User_Code
        ret:Account_Number = tra:StoresAccount
        ret:Contact_Name = ''
        ret:Purchase_Order_Number = ''
        ret:WebOrderNumber = ''
        ret:WebDateCreated = ''
        ret:WebCreatedUser = ''
        ret:PostCode = tra:Postcode
        ret:Company_Name = tra:Company_Name
        ret:Address_Line1 = tra:Address_Line1
        ret:Address_Line2 = tra:Address_Line2
        ret:Address_Line3 = tra:Address_Line3
        ret:Telephone_Number = tra:Telephone_Number
        ret:Fax_Number = tra:Fax_Number
        ret:Postcode_Delivery = tra:Postcode
        ret:Address_Line1_Delivery = tra:Address_Line1
        ret:Address_LIne2_Delivery = tra:Address_Line2
        ret:Address_Line3_Delivery = tra:Address_Line3
        ret:Telephone_Delivery = tra:Telephone_Number
        ret:Fax_Number_Delivery = tra:Fax_Number

        If tra:Retail_Payment_Type = 'ACC'
            ret:payment_method  = 'TRA'
        End!If SUB:Retail_Payment_Type = 'ACC'
        IF tra:retail_payment_type  = 'CAS'
            ret:payment_method = 'CAS'
        End!IF sub:retail_payment_type  = 'CAS'

        ret:Courier_Cost = tra:Courier_Cost
        ret:Courier = tra:Courier_Outgoing

        IF (Access:RETSALES.TryInsert())
            !message('Try insert did not work for retsales')
            Access:RETSALES.CancelAutoInc()
        ELSE
            !message('try insert did work for retsales - going to add the sale items')
            do AddPartToRetSale

        END !end if retsales.insert

        !now to create the invoice
        if access:invoice.primerecord() = level:benign
            inv:invoice_type       = 'RET'
            inv:job_number         = ret:ref_number
            inv:date_created       = Today()
            If tra:invoice_sub_accounts = 'YES' and tra:Use_Sub_accounts = 'YES'
                inv:AccountType = 'SUB'
            Else!If tra:invoice_sub_accounts = 'YES'
                inv:AccountType = 'MAI'
            End!If tra:invoice_sub_accounts = 'YES'
            inv:account_number     = ret:account_number
            inv:total              = ret:sub_total
            inv:vat_rate_labour    = ''
            inv:vat_rate_parts     = ''
            inv:vat_rate_retail    = '' !Round(tmp:VatRate,.01)
            inv:vat_number         = def:vat_number
            INV:Courier_Paid       = ret:courier_cost
            inv:parts_paid         = ret:parts_cost
            inv:labour_paid        = ''
            inv:invoice_vat_number = vat_number"
            invoice_error# = 0
            inv:UseAlternativeAddress   = 0
            inv:EuroExhangeRate = def:EuroRate

            If access:invoice.insert()
                invoice_error# = 1
                access:invoice.cancelautoinc()
            ELSE
                ret:Invoice_Number = inv:Invoice_Number
                ret:Invoice_Date   = today()
                Access:RetSales.update()

            End!If access:invoice.insert()

            !despatch number ...
            if access:retdesno.primerecord() = Level:Benign

                RDN:Consignment_Number  = ''
                RDN:Courier             = ret:courier
                rdn:sale_number         = ret:ref_number
                access:retdesno.insert()

                ret:despatched = 'PRO'
                ret:despatch_number = rdn:despatch_number
                access:retsales.update()

            End!if access:retdesno.primerecord() = Level:Benign
        END !if access:invoice.primerecord() = level:benign
    END !if primerecord worked




    EXIT

AddPartToRetSale    ROUTINE

        !Create the normal part
        If Access:RETSTOCK.PrimeRecord() = Level:Benign
            !message('primerecord worked for retstock')
            res:Ref_Number      = ret:Ref_Number
            res:Part_Number     = sto:Part_Number
            res:Description     = sto:Description
            res:Supplier        = sto:Supplier
            res:Purchase_Cost   = sto:Purchase_Cost
            res:Sale_Cost       = sto:Sale_Cost
            res:Retail_Cost     = sto:Retail_Cost
            res:AccessoryCost   = sto:AccessoryCost
            Case tra:Retail_Price_Structure
                Of 'RET'
                    res:Item_Cost   = res:Retail_Cost
                Of 'PUR'
                    res:Item_Cost   = res:Purchase_Cost
                Else
                    res:Item_Cost   = res:Sale_Cost
            End !Case tra:Retail_Price_Structure
            res:Quantity        = shi:Quantity
            res:Despatched      = 'YES'
            res:Part_Ref_Number = sto:Ref_Number
            res:Date_Received   = shi:Date
            If Access:RETSTOCK.TryInsert() = Level:Benign
                !OK
                do Create_Standard_History   !note that this will create a new stohiste record
                !message('Tryinsert worked for retstock')
            ELSE
                !message('try insert did not work for retstock')
                Access:RETSTOCK.CancelAutoInc()
            END !If Access:RETSTOCK.TryInsert() = Level:Benign
        END !If Access:RETSTOCK.PrimeRecord() = Level:Benign
    EXIT

Create_Standard_History       Routine

    If Access:STOHIST.PrimeRecord() = Level:Benign
        !message('Primerecord worked for stohist')
        shi:Ref_Number           = sto:Ref_Number          !f:RefNo
        shi:User                 = 'XXX'
        shi:Transaction_Type     = 'DEC'
        shi:Despatch_Note_Number = ''
        shi:Job_Number           = LocalJobNo
        shi:Sales_Number         = ret:Ref_Number
        shi:Quantity             = LocalQuantity
        shi:Date                 = Today()
        shi:Purchase_Cost        = Sto:Purchase_Cost
        shi:Sale_Cost            = sto:Sale_Cost
        shi:Retail_Cost          = Sto:Retail_Cost
        shi:Notes                = 'RETAIL ITEM SOLD'
        shi:Information          = 'ARC USEAGE<13,10>SALE AUTOMATICALLY CREATED FOLLOWING STOCK REQUEST<13,10>'&|
                                   '(STOCK WAS DECREMENTED AT STOCK REQUEST)'
        shi:StockOnHand          = sto:Quantity_Stock
        If Access:STOHIST.TryInsert() = Level:Benign
            ! Insert Successful
            !message('tryinsert worked for stohist')
            IF (Access:STOHISTE.PrimeRecord() = Level:Benign)
                stoe:SHIRecordNumber    = shi:Record_Number
                stoe:HistTime           = CLOCK()
                stoe:PreviousAveragePurchaseCost = sto:Purchase_Cost    !f:PAvPC
                stoe:PurchaseCost       = sto:Purchase_Cost
                stoe:SaleCost           = sto:Sale_Cost
                stoe:RetailCost         = sto:Retail_Cost
                stoe:ARC_Status         = ''
                IF (Access:STOHISTE.TryInsert())
                    Access:STOHISTE.CancelAutoInc()
                END
            END ! IF (Access:STOHISTE.PrimeRecord() = Level:Benign)
        Else ! If Access:STOHIST.TryInsert() = Level:Benign
            ! Insert Failed
            !message('tryinsert failed for stohist')
            Access:STOHIST.CancelAutoInc()
        End ! If Access:STOHIST.TryInsert() = Level:Benign
    End ! If Access:STOHIST.PrimeRecord() = Level:Benign

    EXIT

