  MEMBER('wm30enq.clw')

  INCLUDE('ABFILE.INC'),ONCE

  MAP
WM30EBC2:DctInit    PROCEDURE
WM30EBC2:DctKill    PROCEDURE
WM30EBC2:FilesInit  PROCEDURE
  END

Hide:Access:JOBSE    CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:JOBSE    CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:MANFAULT CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:MANFAULT CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:TRACHAR  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:TRACHAR  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:LOCVALUE CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:LOCVALUE CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:STOCKTYP CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:STOCKTYP CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:COURIER  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:COURIER  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:TRDPARTY CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:TRDPARTY CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBNOTES CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:JOBNOTES CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:MANFAURL CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:MANFAURL CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:MODELCCT CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:MODELCCT CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:MANFAUEX CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:MANFAUEX CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:MANFPARL CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:MANFPARL CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBPAYMT CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:JOBPAYMT CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:MANREJR  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:MANREJR  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:STOCK    CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:STOCK    CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:MODEXCHA CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:MODEXCHA CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:MANMARK  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:MANMARK  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:MODPROD  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:MODPROD  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:TRAFAULO CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:TRAFAULO CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:TRAFAULT CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
ValidateFieldServer    PROCEDURE(UNSIGNED Id,BYTE HandleErrors),BYTE,PROC,DERIVED
                     END


Hide:Relate:TRAFAULT CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

WM30EBC2:DctInit PROCEDURE
  CODE
  Relate:JOBSE &= Hide:Relate:JOBSE
  Relate:MANFAULT &= Hide:Relate:MANFAULT
  Relate:TRACHAR &= Hide:Relate:TRACHAR
  Relate:LOCVALUE &= Hide:Relate:LOCVALUE
  Relate:STOCKTYP &= Hide:Relate:STOCKTYP
  Relate:COURIER &= Hide:Relate:COURIER
  Relate:TRDPARTY &= Hide:Relate:TRDPARTY
  Relate:JOBNOTES &= Hide:Relate:JOBNOTES
  Relate:MANFAURL &= Hide:Relate:MANFAURL
  Relate:MODELCCT &= Hide:Relate:MODELCCT
  Relate:MANFAUEX &= Hide:Relate:MANFAUEX
  Relate:MANFPARL &= Hide:Relate:MANFPARL
  Relate:JOBPAYMT &= Hide:Relate:JOBPAYMT
  Relate:MANREJR &= Hide:Relate:MANREJR
  Relate:STOCK &= Hide:Relate:STOCK
  Relate:MODEXCHA &= Hide:Relate:MODEXCHA
  Relate:MANMARK &= Hide:Relate:MANMARK
  Relate:MODPROD &= Hide:Relate:MODPROD
  Relate:TRAFAULO &= Hide:Relate:TRAFAULO
  Relate:TRAFAULT &= Hide:Relate:TRAFAULT

WM30EBC2:FilesInit PROCEDURE
  CODE
  Hide:Relate:JOBSE.Init
  Hide:Relate:MANFAULT.Init
  Hide:Relate:TRACHAR.Init
  Hide:Relate:LOCVALUE.Init
  Hide:Relate:STOCKTYP.Init
  Hide:Relate:COURIER.Init
  Hide:Relate:TRDPARTY.Init
  Hide:Relate:JOBNOTES.Init
  Hide:Relate:MANFAURL.Init
  Hide:Relate:MODELCCT.Init
  Hide:Relate:MANFAUEX.Init
  Hide:Relate:MANFPARL.Init
  Hide:Relate:JOBPAYMT.Init
  Hide:Relate:MANREJR.Init
  Hide:Relate:STOCK.Init
  Hide:Relate:MODEXCHA.Init
  Hide:Relate:MANMARK.Init
  Hide:Relate:MODPROD.Init
  Hide:Relate:TRAFAULO.Init
  Hide:Relate:TRAFAULT.Init


WM30EBC2:DctKill PROCEDURE
  CODE
  Hide:Relate:JOBSE.Kill
  Hide:Relate:MANFAULT.Kill
  Hide:Relate:TRACHAR.Kill
  Hide:Relate:LOCVALUE.Kill
  Hide:Relate:STOCKTYP.Kill
  Hide:Relate:COURIER.Kill
  Hide:Relate:TRDPARTY.Kill
  Hide:Relate:JOBNOTES.Kill
  Hide:Relate:MANFAURL.Kill
  Hide:Relate:MODELCCT.Kill
  Hide:Relate:MANFAUEX.Kill
  Hide:Relate:MANFPARL.Kill
  Hide:Relate:JOBPAYMT.Kill
  Hide:Relate:MANREJR.Kill
  Hide:Relate:STOCK.Kill
  Hide:Relate:MODEXCHA.Kill
  Hide:Relate:MANMARK.Kill
  Hide:Relate:MODPROD.Kill
  Hide:Relate:TRAFAULO.Kill
  Hide:Relate:TRAFAULT.Kill


Hide:Access:JOBSE.Init PROCEDURE
  CODE
  SELF.Init(JOBSE,GlobalErrors)
  SELF.Buffer &= jobe:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jobe:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(jobe:RefNumberKey,'By Ref Number',0)
  SELF.AddKey(jobe:WarrStatusDateKey,'By Warranty Status Date',0)
  Access:JOBSE &= SELF


Hide:Relate:JOBSE.Init PROCEDURE
  CODE
  Hide:Access:JOBSE.Init
  SELF.Init(Access:JOBSE,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:JOBSE.Kill PROCEDURE

  CODE
  WebTableProperties:JOBSE.Kill
  PARENT.Kill
  Access:JOBSE &= NULL


Hide:Access:JOBSE.PrimeFields PROCEDURE

  CODE
  jobe:JobMark = 0
  jobe:JobReceived = 0
  jobe:FailedDelivery = 0
  jobe:CConfirmSecondEntry = 0
  jobe:HubRepair = 0
  jobe:POPConfirmed = 0
  jobe:ValidPOP = 'NO'
  jobe:OriginalPackaging = 0
  jobe:OriginalBattery = 0
  jobe:OriginalCharger = 0
  jobe:OriginalAntenna = 0
  jobe:OriginalManuals = 0
  jobe:PhysicalDamage = 0
  jobe:COverwriteRepairType = 0
  jobe:WOverwriteRepairType = 0
  jobe:IgnoreClaimCosts = 0
  jobe:WebJob = 0
  jobe:ExceedWarrantyRepairLimit = 0
  jobe:ConfirmClaimAdjustment = 0
  jobe:ExchangedATRRC = 0
  jobe:ClaimColour = 0
  jobe:Ignore3rdPartyCosts = 0
  jobe:OBFProcessed = 0
  jobe:PendingClaimColour = 0
  jobe:Booking48HourOption = 0
  jobe:Engineer48HourOption = 0
  jobe:ExcReplcamentCharge = 0
  jobe:VSACustomer = 0
  PARENT.PrimeFields


Hide:Relate:JOBSE.Kill PROCEDURE

  CODE
  Hide:Access:JOBSE.Kill
  PARENT.Kill
  Relate:JOBSE &= NULL


Hide:Access:MANFAULT.Init PROCEDURE
  CODE
  SELF.Init(MANFAULT,GlobalErrors)
  SELF.Buffer &= maf:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(maf:Field_Number_Key,'By Field Number',0)
  SELF.AddKey(maf:MainFaultKey,'By Main Fault',0)
  SELF.AddKey(maf:InFaultKey,'By In Fault',0)
  SELF.AddKey(maf:ScreenOrderKey,'By Screen Order',0)
  SELF.AddKey(maf:BouncerFaultKey,'By Bouncer Fault',0)
  Access:MANFAULT &= SELF


Hide:Relate:MANFAULT.Init PROCEDURE
  CODE
  Hide:Access:MANFAULT.Init
  SELF.Init(Access:MANFAULT,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:MANUFACT)
  SELF.AddRelation(Relate:MANFAULO,RI:CASCADE,RI:CASCADE,mfo:Field_Key)
  SELF.AddRelationLink(maf:Manufacturer,mfo:Manufacturer)
  SELF.AddRelationLink(maf:Field_Number,mfo:Field_Number)


Hide:Access:MANFAULT.Kill PROCEDURE

  CODE
  WebTableProperties:MANFAULT.Kill
  PARENT.Kill
  Access:MANFAULT &= NULL


Hide:Access:MANFAULT.PrimeFields PROCEDURE

  CODE
  maf:Compulsory = 'NO'
  maf:Compulsory_At_Booking = 'NO'
  maf:RestrictLength = 0
  maf:ForceFormat = 0
  maf:MainFault = 0
  maf:PromptForExchange = 0
  maf:InFault = 0
  maf:CompulsoryIfExchange = 0
  maf:NotAvailable = 0
  maf:CharCompulsory = 0
  maf:CharCompulsoryBooking = 0
  maf:RestrictAvailability = 0
  maf:RestrictServiceCentre = 0
  maf:GenericFault = 0
  maf:NotCompulsoryThirdParty = 0
  maf:HideThirdParty = 0
  maf:HideRelatedCodeIfBlank = 0
  maf:FillFromDOP = 0
  maf:CompulsoryForRepairType = 0
  PARENT.PrimeFields


Hide:Relate:MANFAULT.Kill PROCEDURE

  CODE
  Hide:Access:MANFAULT.Kill
  PARENT.Kill
  Relate:MANFAULT &= NULL


Hide:Access:TRACHAR.Init PROCEDURE
  CODE
  SELF.Init(TRACHAR,GlobalErrors)
  SELF.Buffer &= tch:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(tch:Account_Number_Key,'By Charge Type',0)
  Access:TRACHAR &= SELF


Hide:Relate:TRACHAR.Init PROCEDURE
  CODE
  Hide:Access:TRACHAR.Init
  SELF.Init(Access:TRACHAR,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:TRADEACC)


Hide:Access:TRACHAR.Kill PROCEDURE

  CODE
  WebTableProperties:TRACHAR.Kill
  PARENT.Kill
  Access:TRACHAR &= NULL


Hide:Relate:TRACHAR.Kill PROCEDURE

  CODE
  Hide:Access:TRACHAR.Kill
  PARENT.Kill
  Relate:TRACHAR &= NULL


Hide:Access:LOCVALUE.Init PROCEDURE
  CODE
  SELF.Init(LOCVALUE,GlobalErrors)
  SELF.Buffer &= lov:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(lov:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(lov:DateKey,'By Date',0)
  SELF.AddKey(lov:DateOnly,'lov:DateOnly',0)
  Access:LOCVALUE &= SELF


Hide:Relate:LOCVALUE.Init PROCEDURE
  CODE
  Hide:Access:LOCVALUE.Init
  SELF.Init(Access:LOCVALUE,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:LOCATION)


Hide:Access:LOCVALUE.Kill PROCEDURE

  CODE
  WebTableProperties:LOCVALUE.Kill
  PARENT.Kill
  Access:LOCVALUE &= NULL


Hide:Relate:LOCVALUE.Kill PROCEDURE

  CODE
  Hide:Access:LOCVALUE.Kill
  PARENT.Kill
  Relate:LOCVALUE &= NULL


Hide:Access:STOCKTYP.Init PROCEDURE
  CODE
  SELF.Init(STOCKTYP,GlobalErrors)
  SELF.Buffer &= stp:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(stp:Stock_Type_Key,'By Stock_Type',0)
  SELF.AddKey(stp:Use_Loan_Key,'By Stock Type',0)
  SELF.AddKey(stp:Use_Exchange_Key,'By Stock Type',0)
  Access:STOCKTYP &= SELF


Hide:Relate:STOCKTYP.Init PROCEDURE
  CODE
  Hide:Access:STOCKTYP.Init
  SELF.Init(Access:STOCKTYP,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:LOAN,RI:CASCADE,RI:RESTRICT,loa:Stock_Type_Key)
  SELF.AddRelationLink(stp:Stock_Type,loa:Stock_Type)
  SELF.AddRelation(Relate:EXCHANGE,RI:CASCADE,RI:RESTRICT,xch:Stock_Type_Key)
  SELF.AddRelationLink(stp:Stock_Type,xch:Stock_Type)


Hide:Access:STOCKTYP.Kill PROCEDURE

  CODE
  WebTableProperties:STOCKTYP.Kill
  PARENT.Kill
  Access:STOCKTYP &= NULL


Hide:Access:STOCKTYP.PrimeFields PROCEDURE

  CODE
  stp:Use_Loan = 'NO'
  stp:Use_Exchange = 'NO'
  stp:Available = 1
  stp:FranchiseViewOnly = 0
  PARENT.PrimeFields


Hide:Relate:STOCKTYP.Kill PROCEDURE

  CODE
  Hide:Access:STOCKTYP.Kill
  PARENT.Kill
  Relate:STOCKTYP &= NULL


Hide:Access:COURIER.Init PROCEDURE
  CODE
  SELF.Init(COURIER,GlobalErrors)
  SELF.Buffer &= cou:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(cou:Courier_Key,'By Courier',0)
  SELF.AddKey(cou:Courier_Type_Key,'By Courier_Type',0)
  Access:COURIER &= SELF


Hide:Relate:COURIER.Init PROCEDURE
  CODE
  Hide:Access:COURIER.Init
  SELF.Init(Access:COURIER,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:COUBUSHR,RI:CASCADE,RI:CASCADE,cbh:TypeDateKey)
  SELF.AddRelationLink(cou:Courier,cbh:Courier)
  SELF.AddRelation(Relate:TRDPARTY,RI:CASCADE,RI:RESTRICT,trd:Courier_Only_Key)
  SELF.AddRelationLink(cou:Courier,trd:Courier)


Hide:Access:COURIER.Kill PROCEDURE

  CODE
  WebTableProperties:COURIER.Kill
  PARENT.Kill
  Access:COURIER &= NULL


Hide:Access:COURIER.PrimeFields PROCEDURE

  CODE
  cou:Courier_Type = 'NA'
  cou:DespatchClose = 'NO'
  cou:CustomerCollection = 0
  cou:AutoConsignmentNo = 0
  cou:PrintLabel = 0
  cou:PrintWaybill = 0
  cou:IncludeSaturday = 0
  cou:IncludeSunday = 0
  PARENT.PrimeFields


Hide:Relate:COURIER.Kill PROCEDURE

  CODE
  Hide:Access:COURIER.Kill
  PARENT.Kill
  Relate:COURIER &= NULL


Hide:Access:TRDPARTY.Init PROCEDURE
  CODE
  SELF.Init(TRDPARTY,GlobalErrors)
  SELF.Buffer &= trd:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(trd:Company_Name_Key,'By Company Name',0)
  SELF.AddKey(trd:Account_Number_Key,'By Account_Number',0)
  SELF.AddKey(trd:Special_Instructions_Key,'By Special Instructions',0)
  SELF.AddKey(trd:Courier_Only_Key,'trd:Courier_Only_Key',0)
  SELF.AddKey(trd:DeactivateCompanyKey,'By Company Name',0)
  SELF.AddKey(trd:ASCIDKey,'By ASC ID',0)
  Access:TRDPARTY &= SELF


Hide:Relate:TRDPARTY.Init PROCEDURE
  CODE
  Hide:Access:TRDPARTY.Init
  SELF.Init(Access:TRDPARTY,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:TRDMAN,RI:CASCADE,RI:CASCADE,tdm:ManufacturerKey)
  SELF.AddRelationLink(trd:Company_Name,tdm:ThirdPartyCompanyName)
  SELF.AddRelation(Relate:TRDMODEL,RI:CASCADE,RI:CASCADE,trm:Model_Number_Key)
  SELF.AddRelationLink(trd:Company_Name,trm:Company_Name)
  SELF.AddRelation(Relate:JOBS,RI:CASCADE,RI:RESTRICT,job:Third_Party_Key)
  SELF.AddRelationLink(trd:Company_Name,job:Third_Party_Site)
  SELF.AddRelation(Relate:TRDSPEC)
  SELF.AddRelation(Relate:COURIER)


Hide:Access:TRDPARTY.Kill PROCEDURE

  CODE
  WebTableProperties:TRDPARTY.Kill
  PARENT.Kill
  Access:TRDPARTY &= NULL


Hide:Access:TRDPARTY.PrimeFields PROCEDURE

  CODE
  trd:Batch_Number = 0
  trd:Deactivate = 0
  trd:LHubAccount = 0
  trd:EVO_Excluded = 0
  PARENT.PrimeFields


Hide:Relate:TRDPARTY.Kill PROCEDURE

  CODE
  Hide:Access:TRDPARTY.Kill
  PARENT.Kill
  Relate:TRDPARTY &= NULL


Hide:Access:JOBNOTES.Init PROCEDURE
  CODE
  SELF.Init(JOBNOTES,GlobalErrors)
  SELF.Buffer &= jbn:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jbn:RefNumberKey,'By Job Number',0)
  Access:JOBNOTES &= SELF


Hide:Relate:JOBNOTES.Init PROCEDURE
  CODE
  Hide:Access:JOBNOTES.Init
  SELF.Init(Access:JOBNOTES,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:JOBNOTES.Kill PROCEDURE

  CODE
  WebTableProperties:JOBNOTES.Kill
  PARENT.Kill
  Access:JOBNOTES &= NULL


Hide:Relate:JOBNOTES.Kill PROCEDURE

  CODE
  Hide:Access:JOBNOTES.Kill
  PARENT.Kill
  Relate:JOBNOTES &= NULL


Hide:Access:MANFAURL.Init PROCEDURE
  CODE
  SELF.Init(MANFAURL,GlobalErrors)
  SELF.Buffer &= mnr:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(mnr:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(mnr:FieldKey,'By Field Number',0)
  SELF.AddKey(mnr:LinkedRecordNumberKey,'By Linked Record Number',0)
  Access:MANFAURL &= SELF


Hide:Relate:MANFAURL.Init PROCEDURE
  CODE
  Hide:Access:MANFAURL.Init
  SELF.Init(Access:MANFAURL,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:MANFAULO)


Hide:Access:MANFAURL.Kill PROCEDURE

  CODE
  WebTableProperties:MANFAURL.Kill
  PARENT.Kill
  Access:MANFAURL &= NULL


Hide:Access:MANFAURL.PrimeFields PROCEDURE

  CODE
  mnr:PartFaultCode = 0
  PARENT.PrimeFields


Hide:Relate:MANFAURL.Kill PROCEDURE

  CODE
  Hide:Access:MANFAURL.Kill
  PARENT.Kill
  Relate:MANFAURL &= NULL


Hide:Access:MODELCCT.Init PROCEDURE
  CODE
  SELF.Init(MODELCCT,GlobalErrors)
  SELF.Buffer &= mcc:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(mcc:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(mcc:CCTRefKey,'By CCT Reference Number',0)
  Access:MODELCCT &= SELF


Hide:Relate:MODELCCT.Init PROCEDURE
  CODE
  Hide:Access:MODELCCT.Init
  SELF.Init(Access:MODELCCT,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:MODELNUM)


Hide:Access:MODELCCT.Kill PROCEDURE

  CODE
  WebTableProperties:MODELCCT.Kill
  PARENT.Kill
  Access:MODELCCT &= NULL


Hide:Relate:MODELCCT.Kill PROCEDURE

  CODE
  Hide:Access:MODELCCT.Kill
  PARENT.Kill
  Relate:MODELCCT &= NULL


Hide:Access:MANFAUEX.Init PROCEDURE
  CODE
  SELF.Init(MANFAUEX,GlobalErrors)
  SELF.Buffer &= max:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(max:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(max:ModelNumberKey,'By Model Number..',0)
  Access:MANFAUEX &= SELF


Hide:Relate:MANFAUEX.Init PROCEDURE
  CODE
  Hide:Access:MANFAUEX.Init
  SELF.Init(Access:MANFAUEX,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:MANFAULO)


Hide:Access:MANFAUEX.Kill PROCEDURE

  CODE
  WebTableProperties:MANFAUEX.Kill
  PARENT.Kill
  Access:MANFAUEX &= NULL


Hide:Relate:MANFAUEX.Kill PROCEDURE

  CODE
  Hide:Access:MANFAUEX.Kill
  PARENT.Kill
  Relate:MANFAUEX &= NULL


Hide:Access:MANFPARL.Init PROCEDURE
  CODE
  SELF.Init(MANFPARL,GlobalErrors)
  SELF.Buffer &= mpr:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(mpr:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(mpr:FieldKey,'By Field Number',0)
  SELF.AddKey(mpr:LinkedRecordNumberKey,'By Linked Record Number',0)
  Access:MANFPARL &= SELF


Hide:Relate:MANFPARL.Init PROCEDURE
  CODE
  Hide:Access:MANFPARL.Init
  SELF.Init(Access:MANFPARL,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:MANFPALO)


Hide:Access:MANFPARL.Kill PROCEDURE

  CODE
  WebTableProperties:MANFPARL.Kill
  PARENT.Kill
  Access:MANFPARL &= NULL


Hide:Access:MANFPARL.PrimeFields PROCEDURE

  CODE
  mpr:JobFaultCode = 0
  PARENT.PrimeFields


Hide:Relate:MANFPARL.Kill PROCEDURE

  CODE
  Hide:Access:MANFPARL.Kill
  PARENT.Kill
  Relate:MANFPARL &= NULL


Hide:Access:JOBPAYMT.Init PROCEDURE
  CODE
  SELF.Init(JOBPAYMT,GlobalErrors)
  SELF.Buffer &= jpt:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jpt:Record_Number_Key,'By Record Number',1)
  SELF.AddKey(jpt:All_Date_Key,'By Date',0)
  SELF.AddKey(jpt:Loan_Deposit_Key,'jpt:Loan_Deposit_Key',0)
  Access:JOBPAYMT &= SELF


Hide:Relate:JOBPAYMT.Init PROCEDURE
  CODE
  Hide:Access:JOBPAYMT.Init
  SELF.Init(Access:JOBPAYMT,1)


Hide:Access:JOBPAYMT.Kill PROCEDURE

  CODE
  WebTableProperties:JOBPAYMT.Kill
  PARENT.Kill
  Access:JOBPAYMT &= NULL


Hide:Access:JOBPAYMT.PrimeFields PROCEDURE

  CODE
  jpt:Date = Today()
  jpt:Loan_Deposit = 0
  PARENT.PrimeFields


Hide:Relate:JOBPAYMT.Kill PROCEDURE

  CODE
  Hide:Access:JOBPAYMT.Kill
  PARENT.Kill
  Relate:JOBPAYMT &= NULL


Hide:Access:MANREJR.Init PROCEDURE
  CODE
  SELF.Init(MANREJR,GlobalErrors)
  SELF.Buffer &= mar:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(mar:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(mar:CodeKey,'By Code',0)
  Access:MANREJR &= SELF


Hide:Relate:MANREJR.Init PROCEDURE
  CODE
  Hide:Access:MANREJR.Init
  SELF.Init(Access:MANREJR,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:MANUFACT)


Hide:Access:MANREJR.Kill PROCEDURE

  CODE
  WebTableProperties:MANREJR.Kill
  PARENT.Kill
  Access:MANREJR &= NULL


Hide:Relate:MANREJR.Kill PROCEDURE

  CODE
  Hide:Access:MANREJR.Kill
  PARENT.Kill
  Relate:MANREJR &= NULL


Hide:Access:STOCK.Init PROCEDURE
  CODE
  SELF.Init(STOCK,GlobalErrors)
  SELF.Buffer &= sto:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(sto:Ref_Number_Key,'By Ref Number',1)
  SELF.AddKey(sto:Sundry_Item_Key,'By Sundry Item',0)
  SELF.AddKey(sto:Description_Key,'By Description',0)
  SELF.AddKey(sto:Description_Accessory_Key,'By Description',0)
  SELF.AddKey(sto:Shelf_Location_Key,'By Shelf Location',0)
  SELF.AddKey(sto:Shelf_Location_Accessory_Key,'By Shelf Location',0)
  SELF.AddKey(sto:Supplier_Accessory_Key,'By Supplier',0)
  SELF.AddKey(sto:Manufacturer_Key,'By Manufacturer',0)
  SELF.AddKey(sto:Supplier_Key,'By Supplier',0)
  SELF.AddKey(sto:Location_Key,'By Location',0)
  SELF.AddKey(sto:Part_Number_Accessory_Key,'By Part Number',0)
  SELF.AddKey(sto:Ref_Part_Description_Key,'sto:Ref_Part_Description_Key',0)
  SELF.AddKey(sto:Location_Manufacturer_Key,'By Manufacturer',0)
  SELF.AddKey(sto:Manufacturer_Accessory_Key,'By Part Number',0)
  SELF.AddKey(sto:Location_Part_Description_Key,'By Part Number',0)
  SELF.AddKey(sto:Ref_Part_Description2_Key,'CASCADE Key',0)
  SELF.AddKey(sto:Minimum_Part_Number_Key,'By Part Number',0)
  SELF.AddKey(sto:Minimum_Description_Key,'sto:Minimum_Description_Key',0)
  SELF.AddKey(sto:SecondLocKey,'By Part Number',0)
  SELF.AddKey(sto:RequestedKey,'By Part Number',0)
  SELF.AddKey(sto:ExchangeAccPartKey,'By Part Number',0)
  SELF.AddKey(sto:ExchangeAccDescKey,'By Description',0)
  SELF.AddKey(sto:DateBookedKey,'By Date Booked',0)
  SELF.AddKey(sto:Supplier_Only_Key,'sto:Supplier_Only_Key',0)
  SELF.AddKey(sto:LocPartSuspendKey,'By Part Number',0)
  SELF.AddKey(sto:LocDescSuspendKey,'By Description',0)
  SELF.AddKey(sto:LocShelfSuspendKey,'By Shelf Location',0)
  SELF.AddKey(sto:LocManSuspendKey,'By Part Number',0)
  SELF.AddKey(sto:ExchangeModelKey,'By Model Number',0)
  SELF.AddKey(sto:LoanModelKey,'By Loan Model Number',0)
  Access:STOCK &= SELF


Hide:Relate:STOCK.Init PROCEDURE
  CODE
  Hide:Access:STOCK.Init
  SELF.Init(Access:STOCK,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:STOPARTS,RI:CASCADE,RI:CASCADE,spt:DateChangedKey)
  SELF.AddRelationLink(sto:Ref_Number,spt:STOCKRefNumber)
  SELF.AddRelation(Relate:STOAUDIT,RI:CASCADE,RI:CASCADE,stoa:Stock_Ref_No_Key)
  SELF.AddRelationLink(sto:Ref_Number,stoa:Stock_Ref_No)
  SELF.AddRelation(Relate:STOHIST,RI:CASCADE,RI:CASCADE,shi:Ref_Number_Key)
  SELF.AddRelationLink(sto:Ref_Number,shi:Ref_Number)
  SELF.AddRelation(Relate:STOESN,RI:CASCADE,RI:CASCADE,ste:Sold_Key)
  SELF.AddRelationLink(sto:Ref_Number,ste:Ref_Number)
  SELF.AddRelation(Relate:ORDPARTS,RI:CASCADE,RI:None,orp:Ref_Number_Key)
  SELF.AddRelationLink(sto:Ref_Number,orp:Part_Ref_Number)
  SELF.AddRelationLink(sto:Part_Number,orp:Part_Number)
  SELF.AddRelationLink(sto:Description,orp:Description)
  SELF.AddRelation(Relate:STOMODEL,RI:CASCADE,RI:CASCADE,stm:Ref_Part_Description)
  SELF.AddRelationLink(sto:Ref_Number,stm:Ref_Number)
  SELF.AddRelationLink(sto:Part_Number,stm:Part_Number)
  SELF.AddRelationLink(sto:Location,stm:Location)
  SELF.AddRelationLink(sto:Description,stm:Description)
  SELF.AddRelation(Relate:LOCATION)
  SELF.AddRelation(Relate:LOCSHELF)


Hide:Access:STOCK.Kill PROCEDURE

  CODE
  WebTableProperties:STOCK.Kill
  PARENT.Kill
  Access:STOCK &= NULL


Hide:Access:STOCK.PrimeFields PROCEDURE

  CODE
  sto:Accessory = 'NO'
  sto:Minimum_Stock = 'NO'
  sto:Assign_Fault_Codes = 'NO'
  sto:Individual_Serial_Numbers = 'NO'
  sto:ExchangeUnit = 'NO'
  sto:Suspend = 0
  sto:E1 = 1
  sto:E2 = 1
  sto:E3 = 1
  sto:ReturnFaultySpare = 0
  sto:ChargeablePartOnly = 0
  sto:AttachBySolder = 0
  sto:AllowDuplicate = 0
  sto:DateBooked = Today()
  sto:ExcludeLevel12Repair = 0
  PARENT.PrimeFields


Hide:Relate:STOCK.Kill PROCEDURE

  CODE
  Hide:Access:STOCK.Kill
  PARENT.Kill
  Relate:STOCK &= NULL


Hide:Access:MODEXCHA.Init PROCEDURE
  CODE
  SELF.Init(MODEXCHA,GlobalErrors)
  SELF.Buffer &= moa:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(moa:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(moa:AccountNumberKey,'By Account Number',0)
  Access:MODEXCHA &= SELF


Hide:Relate:MODEXCHA.Init PROCEDURE
  CODE
  Hide:Access:MODEXCHA.Init
  SELF.Init(Access:MODEXCHA,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:MODELNUM)


Hide:Access:MODEXCHA.Kill PROCEDURE

  CODE
  WebTableProperties:MODEXCHA.Kill
  PARENT.Kill
  Access:MODEXCHA &= NULL


Hide:Relate:MODEXCHA.Kill PROCEDURE

  CODE
  Hide:Access:MODEXCHA.Kill
  PARENT.Kill
  Relate:MODEXCHA &= NULL


Hide:Access:MANMARK.Init PROCEDURE
  CODE
  SELF.Init(MANMARK,GlobalErrors)
  SELF.Buffer &= mak:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(mak:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(mak:SiteLocationKey,'By Site Location',0)
  SELF.AddKey(mak:SiteLocationOnlyKey,'By Site Location',0)
  Access:MANMARK &= SELF


Hide:Relate:MANMARK.Init PROCEDURE
  CODE
  Hide:Access:MANMARK.Init
  SELF.Init(Access:MANMARK,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:LOCATION)


Hide:Access:MANMARK.Kill PROCEDURE

  CODE
  WebTableProperties:MANMARK.Kill
  PARENT.Kill
  Access:MANMARK &= NULL


Hide:Relate:MANMARK.Kill PROCEDURE

  CODE
  Hide:Access:MANMARK.Kill
  PARENT.Kill
  Relate:MANMARK &= NULL


Hide:Access:MODPROD.Init PROCEDURE
  CODE
  SELF.Init(MODPROD,GlobalErrors)
  SELF.Buffer &= mop:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(mop:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(mop:ProductCodeKey,'By Model No/Product Code',0)
  Access:MODPROD &= SELF


Hide:Relate:MODPROD.Init PROCEDURE
  CODE
  Hide:Access:MODPROD.Init
  SELF.Init(Access:MODPROD,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:MODELNUM)


Hide:Access:MODPROD.Kill PROCEDURE

  CODE
  WebTableProperties:MODPROD.Kill
  PARENT.Kill
  Access:MODPROD &= NULL


Hide:Relate:MODPROD.Kill PROCEDURE

  CODE
  Hide:Access:MODPROD.Kill
  PARENT.Kill
  Relate:MODPROD &= NULL


Hide:Access:TRAFAULO.Init PROCEDURE
  CODE
  SELF.Init(TRAFAULO,GlobalErrors)
  SELF.Buffer &= tfo:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(tfo:Field_Key,'By Field',0)
  SELF.AddKey(tfo:DescriptionKey,'By Description',0)
  Access:TRAFAULO &= SELF


Hide:Relate:TRAFAULO.Init PROCEDURE
  CODE
  Hide:Access:TRAFAULO.Init
  SELF.Init(Access:TRAFAULO,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:TRAFAULT)


Hide:Access:TRAFAULO.Kill PROCEDURE

  CODE
  WebTableProperties:TRAFAULO.Kill
  PARENT.Kill
  Access:TRAFAULO &= NULL


Hide:Relate:TRAFAULO.Kill PROCEDURE

  CODE
  Hide:Access:TRAFAULO.Kill
  PARENT.Kill
  Relate:TRAFAULO &= NULL


Hide:Access:TRAFAULT.Init PROCEDURE
  CODE
  SELF.Init(TRAFAULT,GlobalErrors)
  SELF.Buffer &= taf:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(taf:RecordNumberKey,'taf:RecordNumberKey',1)
  SELF.AddKey(taf:Field_Number_Key,'By Field Number',0)
  Access:TRAFAULT &= SELF


Hide:Relate:TRAFAULT.Init PROCEDURE
  CODE
  Hide:Access:TRAFAULT.Init
  SELF.Init(Access:TRAFAULT,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:TRAFAULO,RI:CASCADE,RI:CASCADE,tfo:Field_Key)
  SELF.AddRelationLink(taf:AccountNumber,tfo:AccountNumber)
  SELF.AddRelationLink(taf:Field_Number,tfo:Field_Number)
  SELF.AddRelation(Relate:TRADEACC)


Hide:Access:TRAFAULT.Kill PROCEDURE

  CODE
  WebTableProperties:TRAFAULT.Kill
  PARENT.Kill
  Access:TRAFAULT &= NULL


Hide:Access:TRAFAULT.PrimeFields PROCEDURE

  CODE
  taf:Compulsory = 'NO'
  taf:Compulsory_At_Booking = 'NO'
  taf:RestrictLength = 0
  PARENT.PrimeFields


Hide:Access:TRAFAULT.ValidateFieldServer PROCEDURE(UNSIGNED Id,BYTE HandleErrors)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.ValidateFieldServer(Id,HandleErrors)
  CASE Id
  OF 3
    GlobalErrors.SetField('Fault Code Field Number')
    IF NOT INRANGE(taf:Field_Number,1,12)
      ReturnValue = Level:Notify
    END
    IF ReturnValue <> Level:Benign
      IF HandleErrors
        ReturnValue = GlobalErrors.ThrowMessage(Msg:FieldOutOfRange,'1 .. 12')
      END
    END
  END
  RETURN ReturnValue


Hide:Relate:TRAFAULT.Kill PROCEDURE

  CODE
  Hide:Access:TRAFAULT.Kill
  PARENT.Kill
  Relate:TRAFAULT &= NULL

