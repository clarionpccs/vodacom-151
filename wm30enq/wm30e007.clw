

   MEMBER('wm30enq.clw')                              ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABLWINR.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('ablwman.inc'),ONCE

                     MAP
                       INCLUDE('WM30E007.INC'),ONCE        !Local module procedure declarations
                     END


ShowStatusChanges PROCEDURE (SentJobNo)               !Generated from procedure template - Window

JobNo                LONG
LocalTime            TIME
BRW3::View:Browse    VIEW(AUDSTATS)
                       PROJECT(aus:DateChanged)
                       PROJECT(aus:OldStatus)
                       PROJECT(aus:NewStatus)
                       PROJECT(aus:RecordNumber)
                       PROJECT(aus:RefNumber)
                       PROJECT(aus:Type)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
aus:DateChanged        LIKE(aus:DateChanged)          !List box control field - type derived from field
LocalTime              LIKE(LocalTime)                !List box control field - type derived from local data
aus:OldStatus          LIKE(aus:OldStatus)            !List box control field - type derived from field
aus:NewStatus          LIKE(aus:NewStatus)            !List box control field - type derived from field
aus:RecordNumber       LIKE(aus:RecordNumber)         !Primary key field - type derived from field
aus:RefNumber          LIKE(aus:RefNumber)            !Browse key field - type derived from field
aus:Type               LIKE(aus:Type)                 !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Webmaster Enquiry - Status Changes'),AT(,,318,228),GRAY,DOUBLE
                       LIST,AT(10,10,293,184),USE(?List),IMM,MSG('Browsing Records'),FORMAT('50L|M~Date~L(2)@d6@35L|M~Time~L(2)@t7@120L|M~Old Status~L(2)@s30@120L|M~New Stat' &|
   'us~L(2)@s30@'),FROM(Queue:Browse)
                       BUTTON('Cancel'),AT(246,202,56,16),USE(?CancelButton)
                     END
Web:CurFrame         &WbFrameClass

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
WebWindowManager     WbWindowManagerClass
WebWindow            CLASS(WbWindowClass)
Init                   PROCEDURE()
                     END

BRW3                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW3::Sort0:Locator  StepLocatorClass                 !Default Locator
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ShowStatusChanges')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?List
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDSTATS.Open
  SELF.FilesOpened = True
  BRW3.Init(?List,Queue:Browse.ViewPosition,BRW3::View:Browse,Queue:Browse,Relate:AUDSTATS,SELF)
  OPEN(window)
  SELF.Opened=True
  JobNo = sentJobNo
  IF (WebServer.IsEnabled())
    WebWindow.Init()
    WebWindowManager.Init(WebServer, WebWindow.IPageCreator, WebWindow.IWebResponseProcessor, 0{PROP:text} & ' (ShowStatusChanges)')
    SELF.AddItem(WebWindow.WindowComponent)
    SELF.AddItem(WebWindowManager.WindowComponent)
  END
  BRW3.Q &= Queue:Browse
  BRW3.AddSortOrder(,aus:DateChangedKey)
  BRW3.AddRange(aus:RefNumber,JobNo)
  BRW3.AddLocator(BRW3::Sort0:Locator)
  BRW3::Sort0:Locator.Init(,aus:Type,1,BRW3)
  BRW3.SetFilter('(aus:type = ''JOB'')')
  BIND('LocalTime',LocalTime)
  BRW3.AddField(aus:DateChanged,BRW3.Q.aus:DateChanged)
  BRW3.AddField(LocalTime,BRW3.Q.LocalTime)
  BRW3.AddField(aus:OldStatus,BRW3.Q.aus:OldStatus)
  BRW3.AddField(aus:NewStatus,BRW3.Q.aus:NewStatus)
  BRW3.AddField(aus:RecordNumber,BRW3.Q.aus:RecordNumber)
  BRW3.AddField(aus:RefNumber,BRW3.Q.aus:RefNumber)
  BRW3.AddField(aus:Type,BRW3.Q.aus:Type)
  BRW3.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDSTATS.Close
  END
  IF (WebServer.IsEnabled())
    POST(EVENT:NewPage)
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CancelButton
      ThisWindow.Update
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


WebWindow.Init PROCEDURE

  CODE
  PARENT.Init
  SELF.SetFormatOptions(2, 2, 6, 13)


BRW3.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW3::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  LocalTime = aus:TimeChanged
  BRW3::RecordStatus=ReturnValue
  RETURN ReturnValue

