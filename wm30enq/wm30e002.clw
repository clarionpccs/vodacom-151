

   MEMBER('wm30enq.clw')                              ! This is a MEMBER module


   INCLUDE('ABLPROPR.INC'),ONCE
   INCLUDE('ABLWINR.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('ablwman.inc'),ONCE

                     MAP
                       INCLUDE('WM30E002.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WM30E004.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WM30E005.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WM30E006.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WM30E007.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WM30E008.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WM30E009.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('WM30E010.INC'),ONCE        !Req'd for module callout resolution
                     END


ShowDetails PROCEDURE (JobNo)                         !Generated from procedure template - Window

ModelString          STRING(30)
ExchangeString       STRING(3)
ExchangeDate         DATE
ExchangeDateString   STRING(12)
CustomerString       STRING(50)
ModelPrompt          STRING(20)
IMEIprompt           STRING(20)
MobileNumber         STRING(30)
CurrentStatus        STRING(30)
IMEIstring           STRING(20)
status_date          DATE
Status_time          TIME
ChargeString         STRING(30)
RetDatePrompt        STRING(30)
RetDate              STRING(30)
RetCourierPrompt     STRING(30)
RetCourier           STRING(30)
RetConsignPrompt     STRING(30)
RetConsignment       STRING(30)
window               WINDOW('ServiceBase 2000 -WebMaster Search Results'),AT(,,464,357),FONT('Arial',8,,FONT:regular,CHARSET:ANSI),GRAY,DOUBLE
                       IMAGE('10by25.gif'),AT(8,4,100,4),USE(?Image5)
                       STRING('Mobile Number'),AT(8,96),USE(?String7),FONT('Arial',8,,,CHARSET:ANSI)
                       STRING(@s30),AT(116,96,100,10),USE(MobileNumber)
                       PROMPT('Unit Exchanged'),AT(232,96),USE(?Prompt4)
                       STRING(@s3),AT(340,96),USE(ExchangeString)
                       STRING(@s20),AT(8,108),USE(IMEIprompt)
                       IMAGE('10by25.gif'),AT(8,240,100,4),USE(?Image1:6)
                       IMAGE('10by25.gif'),AT(8,320,100,4),USE(?Image1:3)
                       IMAGE('10by25.gif'),AT(8,28,100,4),USE(?Image1:9)
                       STRING(@s20),AT(116,108,100,10),USE(IMEIstring)
                       PROMPT('Date Exchanged'),AT(232,108),USE(?ExchangePrompt2)
                       STRING(@s12),AT(340,108),USE(ExchangeDateString)
                       BUTTON('Accessories'),AT(8,300,60,16),USE(?AccessButton)
                       BUTTON('Statuses'),AT(232,300,60,16),USE(?StatusButton)
                       STRING(@s20),AT(8,120),USE(ModelPrompt)
                       STRING(@s30),AT(116,120,100,10),USE(ModelString)
                       PROMPT('Account Number'),AT(232,120),USE(?AccNoPrompt)
                       STRING(@s15),AT(340,120,100,10),USE(job:Account_Number)
                       STRING('Charge Type'),AT(8,132),USE(?String13)
                       STRING(@s30),AT(116,132,100,10),USE(ChargeString)
                       PROMPT('Order No'),AT(232,132),USE(?OrdernumberPrompt)
                       STRING(@s30),AT(340,132,100,10),USE(job:Order_Number)
                       STRING(@s30),AT(340,172,100,10),USE(job:Address_Line1_Delivery)
                       STRING(@s30),AT(340,192,100,10),USE(job:Address_Line2_Delivery)
                       PROMPT('Customer Address'),AT(9,156),USE(?CustomerPrompt)
                       STRING(@s50),AT(116,156,100,10),USE(CustomerString)
                       PROMPT('Delivery Address'),AT(232,156),USE(?DelliveryPrompt)
                       STRING(@s30),AT(340,156,100,10),USE(job:Company_Name_Delivery)
                       STRING(@s30),AT(116,172,100,10),USE(job:Address_Line1)
                       STRING(@s30),AT(116,192,100,10),USE(job:Address_Line2)
                       STRING(@s30),AT(116,208,100,10),USE(job:Address_Line3)
                       STRING(@s30),AT(340,208,100,10),USE(job:Address_Line3_Delivery)
                       STRING(@s10),AT(117,224),USE(job:Postcode)
                       STRING(@s10),AT(340,224),USE(job:Postcode_Delivery)
                       IMAGE('10by25.gif'),AT(8,148,100,4),USE(?Image1:7)
                       STRING('Current Status'),AT(8,32),USE(?StatusPrompt)
                       PROMPT('Status Change Date'),AT(232,32),USE(?DatePrompt)
                       STRING(@s30),AT(116,32,100,10),USE(CurrentStatus)
                       STRING(@d17),AT(340,32),USE(status_date)
                       STRING(@t7),AT(396,32),USE(Status_time)
                       IMAGE('10by25.gif'),AT(8,51,100,4),USE(?Image1:10)
                       STRING('Current Location'),AT(8,63),USE(?String35)
                       STRING(@s30),AT(117,63,100,10),USE(job:Location)
                       PROMPT('at'),AT(384,32),USE(?Prompt10)
                       STRING(@s30),AT(8,252,100,10),USE(RetDatePrompt)
                       STRING(@s30),AT(116,252,100,10),USE(RetDate)
                       STRING(@s30),AT(232,268,100,10),USE(RetCourierPrompt)
                       STRING(@s30),AT(340,268,100,10),USE(RetCourier)
                       IMAGE('10by25.gif'),AT(8,292,100,4),USE(?Image1:8)
                       BUTTON('Repair Text'),AT(116,300,60,16),USE(?InvoiceButton)
                       STRING(@s30),AT(8,268,100,10),USE(RetConsignPrompt)
                       STRING(@s30),AT(116,268,100,10),USE(RetConsignment)
                       IMAGE('10by25.gif'),AT(116,4,100,4),USE(?Image1:5)
                       IMAGE('10by25.gif'),AT(8,88,100,4),USE(?Image1)
                       IMAGE('10by25.gif'),AT(340,4,100,4),USE(?Image2)
                       PROMPT('Job Number'),AT(8,16),USE(?Prompt8)
                       STRING(@s8),AT(116,16),USE(job:Ref_Number)
                       PROMPT('Date Booked'),AT(232,16),USE(?Prompt9)
                       STRING(@d6b),AT(340,16),USE(job:date_booked)
                       IMAGE('10by25.gif'),AT(8,348,100,4),USE(?Image1:2)
                       IMAGE('10by25.gif'),AT(232,4,100,4),USE(?Image1:4)
                       BUTTON('Cancel'),AT(340,328,60,16),USE(?CancelButton)
                       BUTTON('Contact'),AT(340,300,60,16),USE(?ContactButton)
                     END
Web:CurFrame         &WbFrameClass

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
WebWindowManager     WbWindowManagerClass
WebWindow            CLASS(WbWindowClass)
Init                   PROCEDURE()
                     END

Web:MobileNumber     CLASS(WbControlHtmlProperties)   !Web Control Manager for ?MobileNumber
GetEmbedText           PROCEDURE(ASTRING embed),STRING,DERIVED
                     END

Web:ExchangeString   CLASS(WbControlHtmlProperties)   !Web Control Manager for ?ExchangeString
GetEmbedText           PROCEDURE(ASTRING embed),STRING,DERIVED
                     END

Web:IMEIstring       CLASS(WbControlHtmlProperties)   !Web Control Manager for ?IMEIstring
GetEmbedText           PROCEDURE(ASTRING embed),STRING,DERIVED
                     END

Web:ExchangeDateString CLASS(WbControlHtmlProperties) !Web Control Manager for ?ExchangeDateString
GetEmbedText           PROCEDURE(ASTRING embed),STRING,DERIVED
                     END

Web:ModelString      CLASS(WbControlHtmlProperties)   !Web Control Manager for ?ModelString
GetEmbedText           PROCEDURE(ASTRING embed),STRING,DERIVED
                     END

Web:job:Account_Number CLASS(WbControlHtmlProperties) !Web Control Manager for ?job:Account_Number
GetEmbedText           PROCEDURE(ASTRING embed),STRING,DERIVED
                     END

Web:ChargeString     CLASS(WbControlHtmlProperties)   !Web Control Manager for ?ChargeString
GetEmbedText           PROCEDURE(ASTRING embed),STRING,DERIVED
                     END

Web:job:Order_Number CLASS(WbControlHtmlProperties)   !Web Control Manager for ?job:Order_Number
GetEmbedText           PROCEDURE(ASTRING embed),STRING,DERIVED
                     END

Web:job:Address_Line1_Delivery CLASS(WbControlHtmlProperties) !Web Control Manager for ?job:Address_Line1_Delivery
GetEmbedText           PROCEDURE(ASTRING embed),STRING,DERIVED
                     END

Web:job:Address_Line2_Delivery CLASS(WbControlHtmlProperties) !Web Control Manager for ?job:Address_Line2_Delivery
GetEmbedText           PROCEDURE(ASTRING embed),STRING,DERIVED
                     END

Web:CustomerString   CLASS(WbControlHtmlProperties)   !Web Control Manager for ?CustomerString
GetEmbedText           PROCEDURE(ASTRING embed),STRING,DERIVED
                     END

Web:job:Company_Name_Delivery CLASS(WbControlHtmlProperties) !Web Control Manager for ?job:Company_Name_Delivery
GetEmbedText           PROCEDURE(ASTRING embed),STRING,DERIVED
                     END

Web:job:Address_Line1 CLASS(WbControlHtmlProperties)  !Web Control Manager for ?job:Address_Line1
GetEmbedText           PROCEDURE(ASTRING embed),STRING,DERIVED
                     END

Web:job:Address_Line2 CLASS(WbControlHtmlProperties)  !Web Control Manager for ?job:Address_Line2
GetEmbedText           PROCEDURE(ASTRING embed),STRING,DERIVED
                     END

Web:job:Address_Line3 CLASS(WbControlHtmlProperties)  !Web Control Manager for ?job:Address_Line3
GetEmbedText           PROCEDURE(ASTRING embed),STRING,DERIVED
                     END

Web:job:Address_Line3_Delivery CLASS(WbControlHtmlProperties) !Web Control Manager for ?job:Address_Line3_Delivery
GetEmbedText           PROCEDURE(ASTRING embed),STRING,DERIVED
                     END

Web:job:Postcode     CLASS(WbControlHtmlProperties)   !Web Control Manager for ?job:Postcode
GetEmbedText           PROCEDURE(ASTRING embed),STRING,DERIVED
                     END

Web:job:Postcode_Delivery CLASS(WbControlHtmlProperties) !Web Control Manager for ?job:Postcode_Delivery
GetEmbedText           PROCEDURE(ASTRING embed),STRING,DERIVED
                     END

Web:CurrentStatus    CLASS(WbControlHtmlProperties)   !Web Control Manager for ?CurrentStatus
GetEmbedText           PROCEDURE(ASTRING embed),STRING,DERIVED
                     END

Web:status_date      CLASS(WbControlHtmlProperties)   !Web Control Manager for ?status_date
GetEmbedText           PROCEDURE(ASTRING embed),STRING,DERIVED
                     END

Web:Status_time      CLASS(WbControlHtmlProperties)   !Web Control Manager for ?Status_time
GetEmbedText           PROCEDURE(ASTRING embed),STRING,DERIVED
                     END

Web:job:Location     CLASS(WbControlHtmlProperties)   !Web Control Manager for ?job:Location
GetEmbedText           PROCEDURE(ASTRING embed),STRING,DERIVED
                     END

Web:RetDate          CLASS(WbControlHtmlProperties)   !Web Control Manager for ?RetDate
GetEmbedText           PROCEDURE(ASTRING embed),STRING,DERIVED
                     END

Web:RetCourier       CLASS(WbControlHtmlProperties)   !Web Control Manager for ?RetCourier
GetEmbedText           PROCEDURE(ASTRING embed),STRING,DERIVED
                     END

Web:RetConsignment   CLASS(WbControlHtmlProperties)   !Web Control Manager for ?RetConsignment
GetEmbedText           PROCEDURE(ASTRING embed),STRING,DERIVED
                     END

Web:job:Ref_Number   CLASS(WbControlHtmlProperties)   !Web Control Manager for ?job:Ref_Number
GetEmbedText           PROCEDURE(ASTRING embed),STRING,DERIVED
                     END

Web:job:date_booked  CLASS(WbControlHtmlProperties)   !Web Control Manager for ?job:date_booked
GetEmbedText           PROCEDURE(ASTRING embed),STRING,DERIVED
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ShowDetails')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image5
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?CancelButton,RequestCancelled)
  Relate:AUDSTATS.Open
  Relate:EXCHANGE.Open
  Access:JOBS.UseFile
  Access:LOAN.UseFile
  Access:JOBNOTES.UseFile
  SELF.FilesOpened = True
  !Find the right job  (Already know the number exists - previously checked)
     access:jobs.clearkey(job:Ref_Number_Key)
     job:Ref_Number = JobNo
     set(job:Ref_Number_Key,job:Ref_Number_Key)
     access:jobs.fetch(job:Ref_Number_Key)
  
  !set up the standards
  !   thiswindow{prop:text}='WEBMASTER SEARCH RESULTS        JOB NO:'&CLIP(JOBNO)&' BOOKED '& CLIP(FORMAT(job:date_booked,@D6))
     CustomerString = CLIP(job:Title) & ' ' & clip(job:Initial)&' '&clip(job:Surname)
  
  
  
  !Look for an exchange unit
  if job:Exchange_Unit_Number> 0 then
      ExchangeString  ='YES'
      if job:Exchange_Issued_Date = 0
          ExchangeDateString = 'N/A'  !format(
      ELSE
          ExchangeDateString = format(job:Exchange_Issued_Date,@d6)
      END
      currentStatus   = job:Exchange_Status
      RetDatePrompt   = 'Exchange Despatch Date'
      if job:Exchange_Issued_Date >0 then
          RetDate     = clip(format(job:Exchange_Issued_Date,@d6))
      ELSE
          Retdate     = 'Awaiting despatch'
      END
      RetCourierPrompt = 'Exchange Courier'
      Retcourier       = job:Exchange_Courier
      RetConsignPrompt = 'Exchange Consignment'
      IF job:Exchange_Consignment_Number = ''
        RetConsignment = 'N/A'
      Else
        RetConsignment = job:Exchange_Consignment_Number
      END!Exchange Consignment number
  
      access:Exchange.clearkey(xch:Ref_Number_Key)
      xch:Ref_Number = job:Exchange_Unit_Number
      if access:exchange.fetch(xch:Ref_Number_key) then
          ModelPrompt          = ''
          ModelString          = ''
          IMEIprompt           = ''
          IMEIstring           = ''
      ELSE
          !message('changing status')
          ModelPrompt          = 'Exchange Model'
          ModelString          = clip(xch:Manufacturer) & ' ' & clip(xch:Model_Number)
          IMEIprompt           = 'Exchange IMEI'
          IMEIstring           = xch:ESN
      END !Couldn't find exchange unit
  
  ELSE
      ExchangeString ='NO'
      IF job:Loan_Unit_Number > 0 !Look for a loan unit
  
          currentStatus       = job:Loan_Status     !'LOAN UNIT ISSUED' !job:Exchange_Status
          RetDatePrompt       = 'Loan Despatch Date'
          if job:Loan_Issued_Date >0 then
              RetDate         = clip(format(job:Loan_Issued_Date,@d6))
          ELSE
              Retdate         = 'Awaiting despatch'
          END
          RetCourierPrompt    = 'Loan Unit Courier'
          Retcourier          = job:Loan_Courier
          RetConsignPrompt    = 'Loan Unit Consignment'
          IF job:Loan_Consignment_Number = ''
            RetConsignment    = 'N/A'
          Else
            RetConsignment    = job:Loan_Consignment_Number
          END!Exchange Consignment number
  
          access:exchange.clearkey(loa:Ref_Number_Key)
          loa:Ref_Number = job:Loan_Unit_Number
          if access:loan.fetch(loa:Ref_Number_key) then
              ModelPrompt     = ''
              ModelString     = ''
              IMEIprompt      = ''
              IMEIstring      = ''
          ELSE
              !message('changing status')
              ModelPrompt     = 'Loan Model'
              ModelString     = clip(loa:Manufacturer) & ' ' & clip(loa:Model_Number)
              IMEIprompt      = 'Loan Unit IMEI'
              IMEIstring      = loa:ESN
          END !Couldn't find exchange unit
  
      ELSE
  
          !Still on the original job
          ModelPrompt   = 'Model'
          IMEIPrompt    = 'IMEI number'
          CurrentStatus = job:Current_Status
          IMEIString    = job:ESN
          !Hide the return details until they are valid
          if job:Date_Despatched = 0 then
            RetDatePrompt    = ''
            RetDate          = ''
            RetConsignment   = ''
            RetCourierPrompt = ''
            RetCourier       = ''
            RetConsignPrompt = ''
          ELSE
            RetDatePrompt    = 'Repair Despatch Date'
            RetCourierPrompt = 'Courier'
            RetConsignPrompt = 'Consignment No'
            if job:Date_Despatched = 0 then
              RetDate          = 'Awaiting Despatch'
            ELSE
              RetDate          = clip(format(job:Date_Despatched,@d6))
            END
            RetCourier       = clip(job:Courier)
            RetConsignment   = clip(job:Consignment_Number)
          END
  
          ModelString =  clip(job:Manufacturer) &' '& clip(job:Model_Number)
  
  
      End !IF Loan unit number exists
  
  End !IF exchange unit number exists
  
  MobileNumber  = Job:mobile_number
  
  
  if job:Warranty_Job = 'YES' then
      if job:Chargeable_Job = 'YES' then
         ChargeString = 'WARRANTY/CHARGEABLE'
      ELSE
         ChargeString = 'WARRANTY'
      END !IF Chargeable
  ELSE
      IF job:Chargeable_Job = 'YES' THEN
          if job:Insurance = 'YES' then
              ChargeString = 'INSURANCE/CHARGEABLE'
          ELSE
              ChargeString = 'CHARGEABLE'
          END
      else
          ChargeString     = 'INSURANCE'
      END !CHARGEABLE
  END !If warranty
  
  
  Access:Audstats.clearkey(aus:DateChangedKey)
  aus:Type = 'JOB'
  aus:RefNumber = Job:Ref_number
  set(aus:DateChangedKey,aus:DateChangedKey)
  loop
      IF Access:Audstats.next() THEN BREAK.
      TEMP# +=1
      if aus:RefNumber <> job:Ref_number then break.
      Status_date = aus:DateChanged
      Status_time = aus:TimeChanged
      IF TEMP#>100 THEN BREAK.
  END !Loop
  !Status_date now holds the date changed to the last status
  
  
  display
  thiswindow.update()
  
  
  
  OPEN(window)
  SELF.Opened=True
  IF (WebServer.IsEnabled())
    WebWindow.Init()
    WebWindowManager.Init(WebServer, WebWindow.IPageCreator, WebWindow.IWebResponseProcessor, 0{PROP:text} & ' (ShowDetails)')
    SELF.AddItem(WebWindow.WindowComponent)
    SELF.AddItem(WebWindowManager.WindowComponent)
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDSTATS.Close
    Relate:EXCHANGE.Close
  END
  IF (WebServer.IsEnabled())
    POST(EVENT:NewPage)
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?AccessButton
      ThisWindow.Update
      ShowAccessories(Job:Ref_Number)
      ThisWindow.Reset
    OF ?StatusButton
      ThisWindow.Update
      ShowStatusChanges(Job:Ref_Number)
      ThisWindow.Reset
    OF ?InvoiceButton
      ThisWindow.Update
      ShowNotes(job:ref_number)
      ThisWindow.Reset
    OF ?ContactButton
      ThisWindow.Update
      ContactHistory(JOB:Ref_number)
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Web:MobileNumber.GetEmbedText PROCEDURE(ASTRING embed)

ReturnValue          ANY

Target                  &IHtmlWriter,AUTO
  CODE
  Target &= IHtmlWriter:Create()
  CASE embed
  OF A:EmbedBeforeControl
    target.writeln('<<b>')
  OF A:EmbedAfterControl
    target.writeln('<</b>')
  END
  ReturnValue = PARENT.GetEmbedText(embed)
  ReturnValue = Target.GetText()
  Target.Release()
  RETURN ReturnValue


Web:ExchangeString.GetEmbedText PROCEDURE(ASTRING embed)

ReturnValue          ANY

Target                  &IHtmlWriter,AUTO
  CODE
  Target &= IHtmlWriter:Create()
  CASE embed
  OF A:EmbedBeforeControl
    target.writeln('<<b>')
  OF A:EmbedAfterControl
    target.writeln('<</b>')
  END
  ReturnValue = PARENT.GetEmbedText(embed)
  ReturnValue = Target.GetText()
  Target.Release()
  RETURN ReturnValue


Web:IMEIstring.GetEmbedText PROCEDURE(ASTRING embed)

ReturnValue          ANY

Target                  &IHtmlWriter,AUTO
  CODE
  Target &= IHtmlWriter:Create()
  CASE embed
  OF A:EmbedBeforeControl
    target.writeln('<<b>')
  OF A:EmbedAfterControl
    target.writeln('<</b>')
  END
  ReturnValue = PARENT.GetEmbedText(embed)
  ReturnValue = Target.GetText()
  Target.Release()
  RETURN ReturnValue


Web:ExchangeDateString.GetEmbedText PROCEDURE(ASTRING embed)

ReturnValue          ANY

Target                  &IHtmlWriter,AUTO
  CODE
  Target &= IHtmlWriter:Create()
  CASE embed
  OF A:EmbedBeforeControl
    target.writeln('<<b>')
  OF A:EmbedAfterControl
    target.writeln('<</b>')
  END
  ReturnValue = PARENT.GetEmbedText(embed)
  ReturnValue = Target.GetText()
  Target.Release()
  RETURN ReturnValue


Web:ModelString.GetEmbedText PROCEDURE(ASTRING embed)

ReturnValue          ANY

Target                  &IHtmlWriter,AUTO
  CODE
  Target &= IHtmlWriter:Create()
  CASE embed
  OF A:EmbedBeforeControl
    target.writeln('<<b>')
  OF A:EmbedAfterControl
    target.writeln('<</b>')
  END
  ReturnValue = PARENT.GetEmbedText(embed)
  ReturnValue = Target.GetText()
  Target.Release()
  RETURN ReturnValue


Web:job:Account_Number.GetEmbedText PROCEDURE(ASTRING embed)

ReturnValue          ANY

Target                  &IHtmlWriter,AUTO
  CODE
  Target &= IHtmlWriter:Create()
  CASE embed
  OF A:EmbedBeforeControl
    target.writeln('<<b>')
  OF A:EmbedAfterControl
    target.writeln('<</b>')
  END
  ReturnValue = PARENT.GetEmbedText(embed)
  ReturnValue = Target.GetText()
  Target.Release()
  RETURN ReturnValue


Web:ChargeString.GetEmbedText PROCEDURE(ASTRING embed)

ReturnValue          ANY

Target                  &IHtmlWriter,AUTO
  CODE
  Target &= IHtmlWriter:Create()
  CASE embed
  OF A:EmbedBeforeControl
    target.writeln('<<b>')
  OF A:EmbedAfterControl
    target.writeln('<</b>')
  END
  ReturnValue = PARENT.GetEmbedText(embed)
  ReturnValue = Target.GetText()
  Target.Release()
  RETURN ReturnValue


Web:job:Order_Number.GetEmbedText PROCEDURE(ASTRING embed)

ReturnValue          ANY

Target                  &IHtmlWriter,AUTO
  CODE
  Target &= IHtmlWriter:Create()
  CASE embed
  OF A:EmbedBeforeControl
    target.writeln('<<b>')
  OF A:EmbedAfterControl
    target.writeln('<</b>')
  END
  ReturnValue = PARENT.GetEmbedText(embed)
  ReturnValue = Target.GetText()
  Target.Release()
  RETURN ReturnValue


Web:job:Address_Line1_Delivery.GetEmbedText PROCEDURE(ASTRING embed)

ReturnValue          ANY

Target                  &IHtmlWriter,AUTO
  CODE
  Target &= IHtmlWriter:Create()
  CASE embed
  OF A:EmbedBeforeControl
    target.writeln('<<b>')
  OF A:EmbedAfterControl
    target.writeln('<</b>')
  END
  ReturnValue = PARENT.GetEmbedText(embed)
  ReturnValue = Target.GetText()
  Target.Release()
  RETURN ReturnValue


Web:job:Address_Line2_Delivery.GetEmbedText PROCEDURE(ASTRING embed)

ReturnValue          ANY

Target                  &IHtmlWriter,AUTO
  CODE
  Target &= IHtmlWriter:Create()
  CASE embed
  OF A:EmbedBeforeControl
    target.writeln('<<b>')
  OF A:EmbedAfterControl
    target.writeln('<</b>')
  END
  ReturnValue = PARENT.GetEmbedText(embed)
  ReturnValue = Target.GetText()
  Target.Release()
  RETURN ReturnValue


Web:CustomerString.GetEmbedText PROCEDURE(ASTRING embed)

ReturnValue          ANY

Target                  &IHtmlWriter,AUTO
  CODE
  Target &= IHtmlWriter:Create()
  CASE embed
  OF A:EmbedBeforeControl
    target.writeln('<<b>')
  OF A:EmbedAfterControl
    target.writeln('<</b>')
  END
  ReturnValue = PARENT.GetEmbedText(embed)
  ReturnValue = Target.GetText()
  Target.Release()
  RETURN ReturnValue


Web:job:Company_Name_Delivery.GetEmbedText PROCEDURE(ASTRING embed)

ReturnValue          ANY

Target                  &IHtmlWriter,AUTO
  CODE
  Target &= IHtmlWriter:Create()
  CASE embed
  OF A:EmbedBeforeControl
    target.writeln('<<b>')
  OF A:EmbedAfterControl
    target.writeln('<</b>')
  END
  ReturnValue = PARENT.GetEmbedText(embed)
  ReturnValue = Target.GetText()
  Target.Release()
  RETURN ReturnValue


Web:job:Address_Line1.GetEmbedText PROCEDURE(ASTRING embed)

ReturnValue          ANY

Target                  &IHtmlWriter,AUTO
  CODE
  Target &= IHtmlWriter:Create()
  CASE embed
  OF A:EmbedBeforeControl
    target.writeln('<<b>')
  OF A:EmbedAfterControl
    target.writeln('<</b>')
  END
  ReturnValue = PARENT.GetEmbedText(embed)
  ReturnValue = Target.GetText()
  Target.Release()
  RETURN ReturnValue


Web:job:Address_Line2.GetEmbedText PROCEDURE(ASTRING embed)

ReturnValue          ANY

Target                  &IHtmlWriter,AUTO
  CODE
  Target &= IHtmlWriter:Create()
  CASE embed
  OF A:EmbedBeforeControl
    target.writeln('<<b>')
  OF A:EmbedAfterControl
    target.writeln('<</b>')
  END
  ReturnValue = PARENT.GetEmbedText(embed)
  ReturnValue = Target.GetText()
  Target.Release()
  RETURN ReturnValue


Web:job:Address_Line3.GetEmbedText PROCEDURE(ASTRING embed)

ReturnValue          ANY

Target                  &IHtmlWriter,AUTO
  CODE
  Target &= IHtmlWriter:Create()
  CASE embed
  OF A:EmbedBeforeControl
    target.writeln('<<b>')
  OF A:EmbedAfterControl
    target.writeln('<</b>')
  END
  ReturnValue = PARENT.GetEmbedText(embed)
  ReturnValue = Target.GetText()
  Target.Release()
  RETURN ReturnValue


Web:job:Address_Line3_Delivery.GetEmbedText PROCEDURE(ASTRING embed)

ReturnValue          ANY

Target                  &IHtmlWriter,AUTO
  CODE
  Target &= IHtmlWriter:Create()
  CASE embed
  OF A:EmbedBeforeControl
    target.writeln('<<b>')
  OF A:EmbedAfterControl
    target.writeln('<</b>')
  END
  ReturnValue = PARENT.GetEmbedText(embed)
  ReturnValue = Target.GetText()
  Target.Release()
  RETURN ReturnValue


Web:job:Postcode.GetEmbedText PROCEDURE(ASTRING embed)

ReturnValue          ANY

Target                  &IHtmlWriter,AUTO
  CODE
  Target &= IHtmlWriter:Create()
  CASE embed
  OF A:EmbedBeforeControl
    target.writeln('<<b>')
  OF A:EmbedAfterControl
    target.writeln('<</b>')
  END
  ReturnValue = PARENT.GetEmbedText(embed)
  ReturnValue = Target.GetText()
  Target.Release()
  RETURN ReturnValue


Web:job:Postcode_Delivery.GetEmbedText PROCEDURE(ASTRING embed)

ReturnValue          ANY

Target                  &IHtmlWriter,AUTO
  CODE
  Target &= IHtmlWriter:Create()
  CASE embed
  OF A:EmbedBeforeControl
    target.writeln('<<b>')
  OF A:EmbedAfterControl
    target.writeln('<</b>')
  END
  ReturnValue = PARENT.GetEmbedText(embed)
  ReturnValue = Target.GetText()
  Target.Release()
  RETURN ReturnValue


Web:CurrentStatus.GetEmbedText PROCEDURE(ASTRING embed)

ReturnValue          ANY

Target                  &IHtmlWriter,AUTO
  CODE
  Target &= IHtmlWriter:Create()
  CASE embed
  OF A:EmbedBeforeControl
    target.writeln('<<b>')
  OF A:EmbedAfterControl
    target.writeln('<</b>')
  END
  ReturnValue = PARENT.GetEmbedText(embed)
  ReturnValue = Target.GetText()
  Target.Release()
  RETURN ReturnValue


Web:status_date.GetEmbedText PROCEDURE(ASTRING embed)

ReturnValue          ANY

Target                  &IHtmlWriter,AUTO
  CODE
  Target &= IHtmlWriter:Create()
  CASE embed
  OF A:EmbedBeforeControl
    target.writeln('<<b>')
  OF A:EmbedAfterControl
    target.writeln('<</b>')
  END
  ReturnValue = PARENT.GetEmbedText(embed)
  ReturnValue = Target.GetText()
  Target.Release()
  RETURN ReturnValue


Web:Status_time.GetEmbedText PROCEDURE(ASTRING embed)

ReturnValue          ANY

Target                  &IHtmlWriter,AUTO
  CODE
  Target &= IHtmlWriter:Create()
  CASE embed
  OF A:EmbedBeforeControl
    target.writeln('<<b>')
  OF A:EmbedAfterControl
    target.writeln('<</b>')
  END
  ReturnValue = PARENT.GetEmbedText(embed)
  ReturnValue = Target.GetText()
  Target.Release()
  RETURN ReturnValue


Web:job:Location.GetEmbedText PROCEDURE(ASTRING embed)

ReturnValue          ANY

Target                  &IHtmlWriter,AUTO
  CODE
  Target &= IHtmlWriter:Create()
  CASE embed
  OF A:EmbedBeforeControl
    target.writeln('<<b>')
  OF A:EmbedAfterControl
    target.writeln('<</b>')
  END
  ReturnValue = PARENT.GetEmbedText(embed)
  ReturnValue = Target.GetText()
  Target.Release()
  RETURN ReturnValue


Web:RetDate.GetEmbedText PROCEDURE(ASTRING embed)

ReturnValue          ANY

Target                  &IHtmlWriter,AUTO
  CODE
  Target &= IHtmlWriter:Create()
  CASE embed
  OF A:EmbedBeforeControl
    target.writeln('<<b>')
  OF A:EmbedAfterControl
    target.writeln('<</b>')
  END
  ReturnValue = PARENT.GetEmbedText(embed)
  ReturnValue = Target.GetText()
  Target.Release()
  RETURN ReturnValue


Web:RetCourier.GetEmbedText PROCEDURE(ASTRING embed)

ReturnValue          ANY

Target                  &IHtmlWriter,AUTO
  CODE
  Target &= IHtmlWriter:Create()
  CASE embed
  OF A:EmbedBeforeControl
    if job:Date_Despatched > 0  or  job:Exchange_Unit_Number> 0 or job:Loan_Unit_Number>0 then target.writeln('<<b>').
  OF A:EmbedAfterControl
    target.writeln('<</b>')
  END
  ReturnValue = PARENT.GetEmbedText(embed)
  ReturnValue = Target.GetText()
  Target.Release()
  RETURN ReturnValue


Web:RetConsignment.GetEmbedText PROCEDURE(ASTRING embed)

ReturnValue          ANY

Target                  &IHtmlWriter,AUTO
  CODE
  Target &= IHtmlWriter:Create()
  CASE embed
  OF A:EmbedBeforeControl
    target.writeln('<<b>')
  OF A:EmbedAfterControl
    target.writeln('<</b>')
  END
  ReturnValue = PARENT.GetEmbedText(embed)
  ReturnValue = Target.GetText()
  Target.Release()
  RETURN ReturnValue


Web:job:Ref_Number.GetEmbedText PROCEDURE(ASTRING embed)

ReturnValue          ANY

Target                  &IHtmlWriter,AUTO
  CODE
  Target &= IHtmlWriter:Create()
  CASE embed
  OF A:EmbedBeforeControl
    target.writeln('<<b>')
  OF A:EmbedAfterControl
    target.writeln('<</b>')
  END
  ReturnValue = PARENT.GetEmbedText(embed)
  ReturnValue = Target.GetText()
  Target.Release()
  RETURN ReturnValue


Web:job:date_booked.GetEmbedText PROCEDURE(ASTRING embed)

ReturnValue          ANY

Target                  &IHtmlWriter,AUTO
  CODE
  Target &= IHtmlWriter:Create()
  CASE embed
  OF A:EmbedBeforeControl
    target.writeln('<<b>')
  OF A:EmbedAfterControl
    target.writeln('<</b>')
  END
  ReturnValue = PARENT.GetEmbedText(embed)
  ReturnValue = Target.GetText()
  Target.Release()
  RETURN ReturnValue


WebWindow.Init PROCEDURE

  CODE
  PARENT.Init
  SELF.SetFormatOptions(2, 2, 6, 13)
  Web:MobileNumber.Init(?MobileNumber, FEQ:Unknown)
  SELF.AddControl(Web:MobileNumber.IControlToHtml)
  Web:ExchangeString.Init(?ExchangeString, FEQ:Unknown)
  SELF.AddControl(Web:ExchangeString.IControlToHtml)
  Web:IMEIstring.Init(?IMEIstring, FEQ:Unknown)
  SELF.AddControl(Web:IMEIstring.IControlToHtml)
  Web:ExchangeDateString.Init(?ExchangeDateString, FEQ:Unknown)
  SELF.AddControl(Web:ExchangeDateString.IControlToHtml)
  Web:ModelString.Init(?ModelString, FEQ:Unknown)
  SELF.AddControl(Web:ModelString.IControlToHtml)
  Web:job:Account_Number.Init(?job:Account_Number, FEQ:Unknown)
  SELF.AddControl(Web:job:Account_Number.IControlToHtml)
  Web:ChargeString.Init(?ChargeString, FEQ:Unknown)
  SELF.AddControl(Web:ChargeString.IControlToHtml)
  Web:job:Order_Number.Init(?job:Order_Number, FEQ:Unknown)
  SELF.AddControl(Web:job:Order_Number.IControlToHtml)
  Web:job:Address_Line1_Delivery.Init(?job:Address_Line1_Delivery, FEQ:Unknown)
  SELF.AddControl(Web:job:Address_Line1_Delivery.IControlToHtml)
  Web:job:Address_Line2_Delivery.Init(?job:Address_Line2_Delivery, FEQ:Unknown)
  SELF.AddControl(Web:job:Address_Line2_Delivery.IControlToHtml)
  Web:CustomerString.Init(?CustomerString, FEQ:Unknown)
  SELF.AddControl(Web:CustomerString.IControlToHtml)
  Web:job:Company_Name_Delivery.Init(?job:Company_Name_Delivery, FEQ:Unknown)
  SELF.AddControl(Web:job:Company_Name_Delivery.IControlToHtml)
  Web:job:Address_Line1.Init(?job:Address_Line1, FEQ:Unknown)
  SELF.AddControl(Web:job:Address_Line1.IControlToHtml)
  Web:job:Address_Line2.Init(?job:Address_Line2, FEQ:Unknown)
  SELF.AddControl(Web:job:Address_Line2.IControlToHtml)
  Web:job:Address_Line3.Init(?job:Address_Line3, FEQ:Unknown)
  SELF.AddControl(Web:job:Address_Line3.IControlToHtml)
  Web:job:Address_Line3_Delivery.Init(?job:Address_Line3_Delivery, FEQ:Unknown)
  SELF.AddControl(Web:job:Address_Line3_Delivery.IControlToHtml)
  Web:job:Postcode.Init(?job:Postcode, FEQ:Unknown)
  SELF.AddControl(Web:job:Postcode.IControlToHtml)
  Web:job:Postcode_Delivery.Init(?job:Postcode_Delivery, FEQ:Unknown)
  SELF.AddControl(Web:job:Postcode_Delivery.IControlToHtml)
  Web:CurrentStatus.Init(?CurrentStatus, FEQ:Unknown)
  SELF.AddControl(Web:CurrentStatus.IControlToHtml)
  Web:status_date.Init(?status_date, FEQ:Unknown)
  SELF.AddControl(Web:status_date.IControlToHtml)
  Web:Status_time.Init(?Status_time, FEQ:Unknown)
  SELF.AddControl(Web:Status_time.IControlToHtml)
  Web:job:Location.Init(?job:Location, FEQ:Unknown)
  SELF.AddControl(Web:job:Location.IControlToHtml)
  Web:RetDate.Init(?RetDate, FEQ:Unknown)
  SELF.AddControl(Web:RetDate.IControlToHtml)
  Web:RetCourier.Init(?RetCourier, FEQ:Unknown)
  SELF.AddControl(Web:RetCourier.IControlToHtml)
  Web:RetConsignment.Init(?RetConsignment, FEQ:Unknown)
  SELF.AddControl(Web:RetConsignment.IControlToHtml)
  Web:job:Ref_Number.Init(?job:Ref_Number, FEQ:Unknown)
  SELF.AddControl(Web:job:Ref_Number.IControlToHtml)
  Web:job:date_booked.Init(?job:date_booked, FEQ:Unknown)
  SELF.AddControl(Web:job:date_booked.IControlToHtml)

