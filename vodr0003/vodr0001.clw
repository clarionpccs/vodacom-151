

   MEMBER('vodr0003.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('VODR0001.INC'),ONCE        !Local module procedure declarations
                     END


BookingPerformanceReport PROCEDURE                         ! Generated from procedure template - Window

        MAP
ArrivedAtARCDate    PROCEDURE(LONG pCheckLocation=0),LONG,PROC
ClearClipboard  PROCEDURE(LONG fTimeOut=4)
Export  PROCEDURE()
FinalFormatting     PROCEDURE(STRING pLastSquare)
GetUsername PROCEDURE()
SetupVariables      PROCEDURE()
TagAccounts PROCEDURE()
WriteToExport       PROCEDURE()
UpdateProgressWindow    PROCEDURE(STRING fText)
        END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::3:TAGFLAG          BYTE(0)
DASBRW::3:TAGMOUSE         BYTE(0)
DASBRW::3:TAGDISPSTATUS    BYTE(0)
DASBRW::3:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
locStartDate         DATE                                  !
locEndDate           DATE                                  !
tmp:VersionNumber    STRING(30)                            !
locTag               STRING(1)                             !
Progress:Thermometer BYTE(0)                               !Moving Bar
StatusQueue          QUEUE,PRE(staque)                     !
StatusMessage        STRING(60)                            !Status Message
                     END                                   !
locHeadAccount       STRING(30)                            !
UserResultsQueue     QUEUE,PRE(uq)                         !
UserCode             STRING(3)                             !
Surname              STRING(30)                            !
Forename             STRING(30)                            !
JobsBooked           LONG                                  !
AccountNumber        STRING(15)                            !
                     END                                   !
RowNumber            LONG                                  !
Count                LONG                                  !
MaxUsers             LONG                                  !
CurrentAccountNumber STRING(15)                            !
BRW1::View:Browse    VIEW(TRADEACC)
                       PROJECT(tra:Account_Number)
                       PROJECT(tra:Company_Name)
                       PROJECT(tra:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
locTag                 LIKE(locTag)                   !List box control field - type derived from local data
locTag_Icon            LONG                           !Entry's icon ID
tra:Account_Number     LIKE(tra:Account_Number)       !List box control field - type derived from field
tra:Company_Name       LIKE(tra:Company_Name)         !List box control field - type derived from field
tra:RecordNumber       LIKE(tra:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('** Report Title **'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),ICON('Cellular3g.ico'),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE,IMM
                       PANEL,AT(64,38,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PROMPT('Booking Performance Report'),AT(68,39),USE(?WindowTitle),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('SRN:0000000'),AT(563,39),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,363,552,26),USE(?PanelButton),FILL(09A6A7CH)
                       ENTRY(@d17),AT(335,78,60,10),USE(locStartDate),RIGHT(2),FONT(,,,FONT:bold),COLOR(COLOR:White),REQ
                       PROMPT('Booked End Date'),AT(248,98),USE(?locEndDate:Prompt),TRN,CENTER,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                       PROMPT('Booked Start Date'),AT(248,76),USE(?locStartDate:Prompt),TRN,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                       ENTRY(@d17),AT(335,100,60,10),USE(locEndDate),RIGHT(2),FONT(,,,FONT:bold),COLOR(COLOR:White),REQ
                       LIST,AT(216,120,268,234),USE(?List),IMM,VSCROLL,COLOR(COLOR:White),MSG('Browsing Records'),FORMAT('11L(2)J@s1@71L(2)|M~Account Number~@s15@120L(2)|M~Company Name~@s30@'),FROM(Queue:Browse)
                       BUTTON,AT(492,268),USE(?DASTAG),TRN,FLAT,ICON('tagitemp.jpg')
                       BUTTON('&Rev tags'),AT(301,210,1,1),USE(?DASREVTAG),HIDE
                       BUTTON,AT(492,298),USE(?DASTAGAll),TRN,FLAT,ICON('tagallp.jpg')
                       BUTTON('sho&W tags'),AT(305,238,1,1),USE(?DASSHOWTAG),HIDE
                       BUTTON,AT(492,328),USE(?DASUNTAGALL),TRN,FLAT,ICON('untagalp.jpg')
                       PANEL,AT(64,54,552,305),USE(?PANEL1),FILL(09A6A7CH),BEVEL(1)
                       BUTTON,AT(400,74),USE(?Calendar),TRN,FLAT,ICON('lookupp.jpg')
                       BUTTON,AT(400,97),USE(?Calendar:2),TRN,FLAT,ICON('lookupp.jpg')
                       BUTTON,AT(549,363),USE(?Close),TRN,FLAT,ICON('closep.jpg')
                       PROMPT('Report Version'),AT(70,370),USE(?ReportVersion),TRN,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                       BUTTON,AT(479,363),USE(?buttonPrint),TRN,FLAT,ICON('printp.jpg')
                       BUTTON,AT(648,2),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                     END

!Progress Window
rejectrecord        long
recordstoprocess    long,auto
recordsprocessed    long,auto
recordspercycle     long,auto
recordsthiscycle    long,auto
percentprogress     byte
recordstatus        byte,auto
locCancel          byte

progresswindow WINDOW('Progress...'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH), |
         IMM,ICON('Cellular3g.ico'),WALLPAPER('sbback.jpg'),TILED,TIMER(1),GRAY,DOUBLE
       PANEL,AT(164,68,352,12),USE(?ProgPanel),FILL(09A6A7CH)
       LIST,AT(208,88,268,206),USE(?List1),VSCROLL,FONT(,,COLOR:Black,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:Black,COLOR:White), |
           FORMAT('20L(2)|M'),FROM(StatusQueue),GRID(COLOR:White)
       PANEL,AT(164,84,352,246),USE(?Panel4),FILL(09A6A7CH)
       PROGRESS,USE(progress:thermometer),SMOOTH,AT(208,314,268,12),RANGE(0,100)
       STRING(''),AT(259,300,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,COLOR:White,FONT:bold), |
           COLOR(09A6A7CH)
       STRING(''),AT(232,136,161,10),USE(?progress:pcttext),TRN,HIDE,CENTER,FONT('Arial',8,,)
       PANEL,AT(164,332,352,28),USE(?Panel2),FILL(09A6A7CH)
       PROMPT('Report Progress'),AT(168,70),USE(?WindowTitle2),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
       BUTTON,AT(444,332),USE(?ProgressCancel),TRN,FLAT,LEFT,ICON('cancelp.jpg')
       BUTTON,AT(376,332),USE(?Button:OpenReportFolder),TRN,FLAT,HIDE,ICON('openrepp.jpg')
       BUTTON,AT(444,332),USE(?Finish),TRN,FLAT,HIDE,ICON('finishp.jpg')
     END



Automatic       LONG(0)
locReportFolder     CSTRING(255)
locARCLocation      STRING(30)
locRepairCentreType STRING(30)
locARCDateBooked    DATE()
locARCReceivedBy    STRING(30)
locReportName EQUATE('Booking Performance Report')

exp SimpleExportClass

qExportFiles        QUEUE(),PRE(qExportFiles)
RecordNumber            LONG()
FileName                STRING(255)
TotalJobs               LONG()
                    END ! IF
                
locTempFileName STRING(255)
locUsername STRING(100)
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED                 ! Method added to host embed code
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
E1                   Class(oiExcel)
Init                   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0),byte,proc,virtual,name('INIT@F17OIEXCEL')
Kill                   PROCEDURE (byte pUnloadCOM=1),byte,proc,virtual,name('KILL@F17OIEXCEL')
TakeEvent              PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0),virtual,name('TAKEEVENT@F17OIEXCEL')
                     End

Calendar12           CalendarClass
Calendar13           CalendarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False) ! Method added to host embed code
                     END

BRW1                 CLASS(BrowseClass)                    ! Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Fetch                  PROCEDURE(BYTE Direction),DERIVED   ! Method added to host embed code
SetQueueRecord         PROCEDURE(),DERIVED                 ! Method added to host embed code
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeNewSelection       PROCEDURE(),DERIVED                 ! Method added to host embed code
ValidateRecord         PROCEDURE(),BYTE,DERIVED            ! Method added to host embed code
                     END

BRW1::Sort0:Locator  StepLocatorClass                      ! Default Locator
glo:ExportFile      STRING(255),STATIC
ExportFile          File,Driver('ASCII'),Pre(exp),Name(glo:ExportFile),Create,Bindable,Thread
Record                  Record
Line1                       String(2000)
                        End
                    End

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::3:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW1.UpdateBuffer
   glo:Queue.Pointer = tra:RecordNumber
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = tra:RecordNumber
     ADD(glo:Queue,glo:Queue.Pointer)
    locTag = '*'
  ELSE
    DELETE(glo:Queue)
    locTag = ''
  END
    Queue:Browse.locTag = locTag
  IF (locTag = '*')
    Queue:Browse.locTag_Icon = 2
  ELSE
    Queue:Browse.locTag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::3:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW1.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = tra:RecordNumber
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::3:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW1.Reset
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::3:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::3:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::3:QUEUE = glo:Queue
    ADD(DASBRW::3:QUEUE)
  END
  FREE(glo:Queue)
  BRW1.Reset
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::3:QUEUE.Pointer = tra:RecordNumber
     GET(DASBRW::3:QUEUE,DASBRW::3:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = tra:RecordNumber
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::3:DASSHOWTAG Routine
   CASE DASBRW::3:TAGDISPSTATUS
   OF 0
      DASBRW::3:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::3:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::3:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW1.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
TagAccounts2     Routine       !Called by automatic "tagall"

    !message('Calling tag accounts2')

        FREE(glo:Queue)
        Access:TRADEACC.ClearKey(tra:RecordNumberKey)
        tra:RecordNumber = 1
        SET(tra:RecordNumberKey,tra:RecordNumberKey)
        LOOP UNTIL Access:TRADEACC.Next() <> Level:Benign

            IF tra:RemoteRepairCentre = 1 then cycle.
            if tra:Account_Number = LocHeadAccount then cycle.
            if tra:Account_Number = 'XXXRRC' then cycle.
            if tra:Stop_Account ='YES' then cycle.

            glo:Pointer = tra:RecordNumber
            ADD(glo:Queue)

        END ! LOOP
SummaryStuff    Routine

    UpdateProgressWindow('Creating Summary')

    !created a new summary sheet at the start
    e1.InsertWorksheet()
    e1.RenameWorksheet('Summary')
    e1.SelectWorksheet('Summary')

    !titles
    e1.WriteToCell('Booking Performance Report ' & tra:Account_Number,'A1')
    e1.WriteToCell('Criteria','A3')
    e1.WriteToCell(tmp:VersionNumber,'D3')
    e1.WriteToCell('Start Date','A4')
    e1.WriteToCell(FORMAT(locStartDate,@d06b),'B4')
    e1.WriteToCell('End Date','A5')
    e1.WriteToCell(FORMAT(locEndDate,@d06b),'B5')
    e1.WriteToCell('Created By','A6')
    e1.WriteToCell(locUserName,'B6')
    e1.WriteToCell('Created Date','A7')
    e1.WriteToCell(FORMAT(TODAY(),@d06b),'B7')
    e1.WriteToCell('Summary','A9')

    !columns
    RowNumber = 10
    e1.WriteToCell('Code',       'A'&RowNumber)
    e1.WriteToCell('Forename',   'B'&RowNumber)
    e1.WriteToCell('Surname',    'C'&RowNumber)
    e1.WriteToCell('Jobs Booked','D'&RowNumber)
    e1.WriteToCell('Hit Rate%',  'E'&RowNumber)

    !users
    Sort(UserResultsQueue,uq:Surname)
    Loop Count = 1 to records(UserResultsQueue)
        get(UserResultsQueue,Count)

        !for the current export to trade account?
        if uq:AccountNumber <> tra:Account_Number then cycle.

        RowNumber += 1
        e1.WriteToCell(uq:UserCode,  'A'&RowNumber)
        e1.WriteToCell(uq:Forename,  'B'&RowNumber)
        e1.WriteToCell(uq:Surname,   'C'&RowNumber)
        e1.WriteToCell(uq:JobsBooked,'D'&RowNumber)
        !Added a bit here
        !e1.writeToCell(uq:AccountNumber,'G'&RowNumber)
    
    END !loop through UserResultsQueue

    MaxUsers = RowNumber +1

    If MaxUsers = 11
        !there are NO users on this page = no totals
        !adding in the totals gives excel errors on circular references
    ELSE
        !put totals at bottom of page
        e1.WriteToCell('Total',                  'C'&MaxUsers)
        e1.WriteToCell('=SUM(D11:D'&MaxUsers-1,  'D'&MaxUsers)
        e1.WriteToCell('=SUM(E11:E'&MaxUsers-1,  'E'&MaxUsers)

        !fill in the final %
        Loop Count = 11 to MaxUsers-1
            e1.WriteToCell('=IF(D'&MaxUsers&'=0, 0, D'&Count&'/D'&MaxUsers&')','E'&Count)
        END

    END !if Maxusers > 11

    !Do some tarting up
    e1.SetCellFontName('Tahoma','A1','E'&MaxUsers)
    e1.SetCellFontSize(12,'A1')
    e1.SetCellFontSize(8,'A2','E'&MaxUsers)
    e1.SetCellFontStyle('Bold','A1','E10')
    e1.SetCellFontStyle('Bold','A'&MaxUsers,'E'&MaxUsers)
    
    
    e1.SetColumnWidth('A','E'&MaxUsers)
    e1.SetColumnWidth('A',,'14')
    e1.SetColumnWidth('B',,'20')
    e1.SetColumnWidth('C',,'20')
    e1.SetColumnWidth('D',,'14')
    e1.SetColumnWidth('E',,'20')

    e1.SetCellBackgroundColor(color:Silver,'A1','E1')
    e1.SetCellBackgroundColor(color:Silver,'A3','E7')
    e1.SetCellBackgroundColor(color:Silver,'A9','E10')
    e1.SetCellBackgroundColor(color:Silver,'A'&MaxUsers,'E'&MaxUsers)

    e1.SetCellNumberFormat(oix:NumberFormatPercentage,,2,,'E11','E' & MaxUsers)

    e1.SelectCells('A1')

    exit
UpdateProgressWindow      routine !Progress Window Routines (DBH: 22-03-2004)
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
        percentprogress = (recordsprocessed / recordstoprocess)*100
        if percentprogress >= 100
            recordsprocessed        = 0
            percentprogress         = 0
            progress:thermometer    = 0
            recordsthiscycle        = 0
        end
        if percentprogress <> progress:thermometer then
            progress:thermometer = percentprogress
            ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
        end
    end
    !0{prop:Text} = ?progress:pcttext{prop:text}
    Display()

OpenProgressWindow  ROUTINE
    recordspercycle         = 25
    recordsprocessed        = 0
    percentprogress         = 0
    progress:thermometer    = 0
    recordstoprocess        = 500
    Open(ProgressWindow)

    ?progress:userstring{prop:text} = 'Running...'
    ?progress:pcttext{prop:text} = '0% Completed'

CancelCheck         routine
    cancel# = 0
    loccancel = 0
    accept
        Case Event()
        Of Event:Timer
            Break
        Of Event:CloseWindow
            cancel# = 1
            Break
        Of Event:accepted
            Yield()
            If Field() = ?ProgressCancel
                cancel# = 1
                Break
            End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Beep(Beep:SystemQuestion)  ;  Yield()
        Case Message('Do you want to finish off building the excel document with the data you have compile so far, or just quit now?','ServiceBase',|
            Icon:Question,'&Finish Report|&Quit Now|&Cancel',3)
        Of 1 ! &Finish Report Button
            locCancel = 2
        Of 2 ! &Quit Now Button
            locCancel = 1
        Of 3 ! &Cancel Button
        End!Case Message
    End!If cancel# = 1


EndProgress         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    display()
RecolourWindow      Routine


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          Access:SRNTExt.Clearkey(srn:KeySRN)
          srn:SRNumber = '020742'&'0'
          If Access:SRNText.Fetch(srn:KeySRN)
              !Error
              SRN:TipText = 'Press F1 to access detailed help'
          End !Access:SRNText.Fetch(stn:KeySRN) = Level:Benign
          ?SRNNumber{Prop:Text}= 'SRN:020742'&'0'
          if clip(SRN:TipText)='' then SRN:TipText = 'Press F1 to access detailed help'.
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BookingPerformanceReport')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('LocHeadAccount',LocHeadAccount)                    ! Added by: BrowseBox(ABC)
  BIND('locTag',locTag)                                    ! Added by: BrowseBox(ABC)
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:SRNText.Open()
  Relate:AUDIT.SetOpenRelated()
  Relate:AUDIT.Open                                        ! File AUDIT used by this procedure, so make sure it's RelationManager is open
  Relate:JOBSE3.Open                                       ! File JOBSE3 used by this procedure, so make sure it's RelationManager is open
  Relate:REPSCHAC.SetOpenRelated()
  Relate:REPSCHAC.Open                                     ! File REPSCHAC used by this procedure, so make sure it's RelationManager is open
  Relate:SRNTEXT.Open                                      ! File SRNTEXT used by this procedure, so make sure it's RelationManager is open
  Relate:WEBJOB.Open                                       ! File WEBJOB used by this procedure, so make sure it's RelationManager is open
  Access:JOBS.UseFile                                      ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:USERS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSE.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBSE2.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:LOCATLOG.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:REPSCHED.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:REPSCHCR.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:REPSCHLG.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
      SetupVariables()
  BRW1.Init(?List,Queue:Browse.ViewPosition,BRW1::View:Browse,Queue:Browse,Relate:TRADEACC,SELF) ! Initialize the browse manager
  SELF.Open(window)                                        ! Open window
  ! ================ Set Report Version =====================
  Include('..\ReportVersion.inc')
  tmp:VersionNumber = 'Version Number: ' & Clip(tmp:VersionNumber) & '5006'
  ?ReportVersion{prop:Text} = tmp:VersionNumber
      GetUserName()
  
      IF (COMMAND('/DAILY'))
          locUserName = 'AUTOMATED PROCEDURE'
          Automatic = TRUE
          locStartDate = TODAY()
          locEndDate = TODAY()
          TagAccounts()
      END
      IF COMMAND('/WEEKLY')
          locUserName = 'AUTOMATED PROCEDURE'
          Automatic = TRUE
          LOOP day# = TODAY() To TODAY()-7 By -1
              If day# %7 = 1
                  locStartDate = day#
                  Break
                  
              End !If day# %7 = 2
          END ! LOOP
          LOOP day# = TODAY() To TODAY() + 7
              IF day# %7 = 0
                  locEndDate = day#
                  Break
              End !IF day# %7 = 0
          END !
          TagAccounts()
      END 
      IF COMMAND('/MONTHLY')
          locUserName = 'AUTOMATED PROCEDURE'
          Automatic = TRUE
          locStartDate = Deformat('1/' & Month(Today()) & '/' & Year(Today()),@d6)
          locEndDate = Deformat('1/' & Month(Today()) + 1 & '/' & Year(Today()),@d6) - 1
          TagAccounts()
      END ! IF
  
      IF (Automatic = TRUE)
          Export()
          PUTINI('MAIN','InputDir',CLIP(locReportFolder),CLIP(PATH()) & '\AUTOEMAIL.INI')
          PUTINI('MAIN','ExportDir',CLIP(locReportFolder)&'EMAILED',CLIP(PATH()) & '\AUTOEMAIL.INI')
          RUN('EMAILDIR',1)
          POST(Event:CloseWindow)  
      END ! IF
  
  
      ! Inserting (DBH 31/08/2007) # 9125 - Is this an automatic report?
  
      !for testing look up report in repsched to find record number (2 in 13.2 test area)
      !use command line VODR0003.exe /SCHEDULE %2
  
      If Command('/SCHEDULE')
          x# = Instring('%',Command(),1,1)
          Access:REPSCHED.ClearKey(rpd:RecordNumberKey)
          rpd:RecordNumber = Clip(Sub(Command(),x# + 1,20))
          If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
              !Found
              if rpd:Active then
                  Access:REPSCHCR.ClearKey(rpc:ReportCriteriaKey)
                  rpc:ReportName = rpd:ReportName
                  rpc:ReportCriteriaType = rpd:ReportCriteriaType
                  If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
                      !Found
                      If rpc:AllAccounts
                          !DO DASBRW::3:DASTAGALL - THIS WAS TAGGING ALL TRADEACCS - AS FILTERS NOT APPLIED - FILES NOT OPENED
                          do TagAccounts2
                      Else ! If rpc:AllAccount
                          
                          Access:REPSCHAC.Clearkey(rpa:AccountNumberKey)
                          rpa:REPSCHCRRecordNumber = rpc:RecordNumber
                          Set(rpa:AccountNumberKey,rpa:AccountNumberKey)
                          Loop ! Begin Loop
                              If Access:REPSCHAC.Next()
                                  Break
                              End ! If Access:REPSCHAC.Next()
                              If rpa:REPSCHCRRecordNumber <> rpc:RecordNumber
                                  Break
                              End ! If rpa:REPSCHCRRecordNumber <> rpc:RecordNumber
  
                              Access:TRADEACC.ClearKey(tra:Account_Number_Key)
                              tra:Account_Number = rpa:AccountNumber
                              IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
                                  glo:Pointer = tra:RecordNumber
                                  Add(glo:Queue)
                              END ! IF
                          End ! Loop
                      End ! If rpc:AllAccount
          
                      Case rpc:DateRangeType
                      Of 1 ! Today Only
                          locStartDate = Today()
                          locEndDate = Today()
                      Of 2 ! 1st Of Month
                          locStartDate = Date(Month(Today()),1,Year(Today()))
                          locEndDate = Today()
                      Of 3 ! Whole Month
                          locEndDate = Date(Month(Today()),1,Year(Today())) - 1
                          locStartDate = Date(Month(locEndDate),1,Year(locEndDate))
                      End ! Case rpc:DateRangeType
          
                      Export()
  
                      If Access:REPSCHLG.PrimeRecord() = Level:Benign
                          rlg:REPSCHEDRecordNumber = rpd:RecordNumber
                          rlg:Information = 'Report Name: ' & Clip(rpd:ReportName) & ' - ' & Clip(rpd:ReportCriteriaType) & |
                                            '<13,10>Report Finished'
                          If Access:REPSCHLG.TryInsert() = Level:Benign
                              !Insert
                          Else ! If Access:REPSCHLG.TryInsert() = Level:Benign
                              Access:REPSCHLG.CancelAutoInc()
                          End ! If Access:REPSCHLG.TryInsert() = Level:Benign
                      End ! If Access.REPSCHLG.PrimeRecord() = Level:Benign
                      Post(Event:CloseWindow)
                  End ! If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
              END !if rpd:Active
          End ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
      End ! If Command('/SCHEDULE')
      ! End (DBH 31/08/2007) #9125
  ?List{prop:vcr} = TRUE
  Do DefineListboxStyle
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
      Alert(0078H) !F9
      Alert(0070H) !F1
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  BRW1.Q &= Queue:Browse
  BRW1.AddSortOrder(,tra:Account_Number_Key)               ! Add the sort order for tra:Account_Number_Key for sort order 1
  BRW1.AddLocator(BRW1::Sort0:Locator)                     ! Browse has a locator for sort order 1
  BRW1::Sort0:Locator.Init(,tra:Account_Number,1,BRW1)     ! Initialize the browse locator using  using key: tra:Account_Number_Key , tra:Account_Number
  BRW1.SetFilter('((tra:RemoteRepairCentre = 1 OR tra:Account_Number = LocHeadAccount) AND (tra:Account_Number <<> ''XXXRRC'') and (tra:Stop_Account<<>''YES''))') ! Apply filter expression to browse
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW1.AddField(locTag,BRW1.Q.locTag)                      ! Field locTag is a hot field or requires assignment from browse
  BRW1.AddField(tra:Account_Number,BRW1.Q.tra:Account_Number) ! Field tra:Account_Number is a hot field or requires assignment from browse
  BRW1.AddField(tra:Company_Name,BRW1.Q.tra:Company_Name)  ! Field tra:Company_Name is a hot field or requires assignment from browse
  BRW1.AddField(tra:RecordNumber,BRW1.Q.tra:RecordNumber)  ! Field tra:RecordNumber is a hot field or requires assignment from browse
  Do RecolourWindow
  BRW1.AddToolbarTarget(Toolbar)                           ! Browse accepts toolbar control
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List{Prop:Alrt,239} = SpaceKey
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  Relate:SRNText.Close()
    Relate:AUDIT.Close
    Relate:JOBSE3.Close
    Relate:REPSCHAC.Close
    Relate:SRNTEXT.Close
    Relate:WEBJOB.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::3:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::3:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::3:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::3:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::3:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Calendar
      ThisWindow.Update
      Calendar12.SelectOnClose = True
      Calendar12.Ask('Select a Date',locStartDate)
      IF Calendar12.Response = RequestCompleted THEN
      locStartDate=Calendar12.SelectedDate
      DISPLAY(?locStartDate)
      END
      ThisWindow.Reset(True)
    OF ?Calendar:2
      ThisWindow.Update
      Calendar13.SelectOnClose = True
      Calendar13.Ask('Select a Date',locEndDate)
      IF Calendar13.Response = RequestCompleted THEN
      locEndDate=Calendar13.SelectedDate
      DISPLAY(?locEndDate)
      END
      ThisWindow.Reset(True)
    OF ?buttonPrint
      ThisWindow.Update
          Export()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & '020742'&'0')
          Else !glo:WebJob Then
              HelpBrowser('020742'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020742'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeEvent()
    E1.TakeEvent ('', '')
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all field specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::3:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
  !Of Event:AlertKey
      If KeyCode() = 0078H
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & '020742'&'0')
          Else !glo:WebJob Then
              HelpBrowser('020742'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020742'&'0')
      ***
      End !If KeyCode() F9
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

ArrivedAtARCDate    PROCEDURE(LONG pCheckLocation=0)!,LONG,PROC
retValue    LONG(FALSE)
    CODE
        locARCDateBooked = ''
        locARCReceivedBy = ''

        IF (pCheckLocation = 1)
            ! Get the date/user when the job arrived at the ARC from RRC
            Access:LOCATLOG.ClearKey(lot:DateKey)
            lot:RefNumber = job:Ref_Number
            SET(lot:DateKey,lot:DateKey)
            LOOP UNTIL Access:LOCATLOG.Next() <> Level:Benign
                IF (lot:RefNumber <> job:Ref_Number)
                    BREAK
                END ! IF
                IF (lot:NewLocation = locARCLocation)
                    retValue = TRUE
                    locARCDateBooked = lot:TheDate
                    locARCReceivedBy = lot:UserCode
                    BREAK
                END ! IF
            END ! LOOP

        END ! IF
        
        IF (locARCDateBooked = '')
            ! Not arrived at ARC, fill in the job details as these will still be used
            locARCDateBooked = job:Date_Booked
            If job:Who_Booked = ''
                Access:AUDIT.Clearkey(aud:Action_Key)
                aud:Ref_Number    = job:Ref_Number
                aud:Action        = 'RAPID NEW JOB BOOKING INITIAL ENTRY'
                Set(aud:Action_Key,aud:Action_Key)
                Loop ! Begin Loop
                    If Access:AUDIT.Next()
                        Break
                    End ! If Access:AUDIT.Next()
                    If aud:Ref_Number <> job:Ref_Number
                        Break
                    End ! If aud:Ref_Number <> job:Ref_Number
                    If aud:Action <> 'RAPID NEW JOB BOOKING INITIAL ENTRY'
                        Break
                    End ! If aud:Action <> 'RAPID NEW JOB BOOKING INITIAL ENTRY'
                    job:Who_Booked = aud:User
                    Break
                End ! Loop
            End ! If job:Who_Booked = ''

            locARCReceivedBy = job:Who_Booked

        END ! IF
                
        RETURN retValue
ClearClipboard  PROCEDURE(LONG fTimeOut=4)
locTimeOut  LONG()
locCurrentTime  TIME()
    CODE
        e1.Copy('A1')
!        SETCURSOR(CURSOR:WAIT)
!        locCurrentTime = CLOCK()
!        SETCLIPBOARD('blank')
!        LOOP
!            IF (CLOCK() < locCurrentTime + (fTimeOut * 100))
!                CYCLE
!            END
!            IF (CLIPBOARD() = 'blank')
!                BREAK
!            END
!
!        END
!        SETCURSOR()
Export      PROCEDURE()
i   LONG(0)
kTempFolder CSTRING(255)

kReportStartDate    DATE()
kReportStartTime    TIME()

kFinalColumn EQUATE('X')        !tb13287 J - 08/01/15 - updated from V due to 2 new columns
kTotalJobs LONG(0)
locClipboard ANY

    CODE
        ! Create folder in My Documents
        SHGetSpecialFolderPathAA(GetDesktopWindow(),kTempFolder,001CH,0)

        IF (NOT Exists(kTempFolder))
            Beep(Beep:SystemHand)  ;  Yield()
            Case Message('An error has occurred creating the report.'&|
                '|Please quit and try again.','ServiceBase',|
                Icon:Hand,'&OK',1)
            Of 1 ! &OK Button
            End!Case Message
            RETURN
        END

        kReportStartDate = TODAY()
        kReportStartTime = CLOCK()

        DO OpenProgressWindow
        
        UpdateProgressWindow('Report Started: ' & FORMAT(kReportStartDate,@d6) & |
            ' ' & FORMAT(kReportStartTime,@t1))
        UpdateProgressWindow('')
        UpdateProgressWindow('Searching For Jobs...')
    
        LOOP i = 1 TO RECORDS(glo:Queue)
            GET(glo:Queue,i)
            
            Access:TRADEACC.ClearKey(tra:RecordNumberKey)
            tra:RecordNumber = glo:Pointer
            IF (Access:TRADEACC.TryFetch(tra:RecordNumberKey))
                CYCLE
            END ! IF
            
            UpdateProgressWindow('')
            UpdateProgressWindow('Account: ' & CLIP(tra:Account_Number) & ' ' & CLIP(tra:Company_Name))

            !remember what account this is - the trade buffer is changed as you read files
            CurrentAccountNumber = tra:Account_Number
            
            DO UpdateProgressWindow
            Do CancelCheck
            IF (locCancel)
                BREAK
            END 

            locTempFilename = CLIP(kTempFolder) & '\' & CLIP(tra:Account_Number) & '_' & CLOCK() & '.csv'
            
            IF (exp.Init(locTempFileName,,1) = Level:Benign)
                kTotalJobs = 0
                                                           
                IF (tra:Account_Number = locHeadAccount)
                    ! Loop through all jobs booked at ARC & jobs sent to ARC
                    Access:JOBS.ClearKey(job:Date_Booked_Key)
                    job:Date_Booked = locStartDate - 30 ! Add 30 days to account for RRC jobs sent to ARC
                    SET(job:Date_Booked_Key,job:Date_Booked_Key)
                    LOOP UNTIL Access:JOBS.Next() <> Level:Benign
                        IF (job:Date_Booked > locEndDate)
                            BREAK
                        END
                        
                        DO UpdateProgressWindow
                        DO CancelCheck
                        IF (locCancel)
                            BREAK
                        END
                        
                        Access:WEBJOB.ClearKey(wob:RefNumberKey)
                        wob:RefNumber = job:Ref_Number
                        IF (Access:WEBJOB.TryFetch(wob:RefNumberKey))
                            CYCLE
                        END ! IF
                        
                        Access:JOBSE.ClearKey(jobe:RefNumberKey)
                        jobe:RefNumber = job:Ref_Number
                        IF (Access:JOBSE.TryFetch(jobe:RefNumberKey))
                            CYCLE
                        END ! IF
                        
                        IF (job:Cancelled = 'YES')
                            CYCLE
                        END ! IF
                        IF (CLIP(job:ESN) = '' OR CLIP(job:Account_Number) = '')
                            CYCLE
                        END ! IF
                        
                        IF (jobe:WebJob = 1)
                            ! Booked At RRC
                            ! Only count if sent to ARC within the date range
                            IF (ArrivedAtARCDate(1) = FALSE)
                                CYCLE
                            END ! IF
                            
                            IF NOT (INRANGE(locARCDateBooked,locStartDate,locEndDate))
                                CYCLE
                            END ! IF
                            
                            ! IF GetUserDetails(LOC:ARCReceivedBy)
                                ! uq:RRCJobsReceived += 1
                                ! uq:JobsTotal       += 1
                                ! PUT(UserResultsQueue)
                                ! Print_Job# = 2
                            ! END !IF
                        ELSE
                            ArrivedAtARCDate()
                            ! ARC job, so only count jobs within date range
                            IF NOT (INRANGE(job:Date_Booked,locStartDate,locEndDate))
                                CYCLE
                            END ! IF

                        END ! IF
                        
                        WriteToExport()
                        kTotalJobs += 1
                    END ! LOOP
                    
                ELSE ! IF (tra:Account_Number = locHeadAccount)
                    ! Loop through all jobs booked at RRC
                    Access:WEBJOB.ClearKey(wob:DateBookedKey)
                    wob:HeadAccountNumber = tra:Account_Number
                    wob:DateBooked = locStartDate
                    SET(wob:DateBookedKey,wob:DateBookedKey)
                    LOOP UNTIL Access:WEBJOB.Next() <> Level:Benign
                        IF (wob:HeadACcountNumber <> tra:Account_Number OR |
                            wob:DateBooked > locEndDate)
                            BREAK
                        END ! IF
                        
                        DO UpdateProgressWindow
                        DO CancelCheck
                        IF (locCancel)
                            BREAK
                        END
                        
                        
                        Access:JOBS.ClearKey(job:Ref_Number_Key)
                        job:Ref_Number = wob:RefNumber
                        IF (Access:JOBS.Tryfetch(job:Ref_Number_Key))
                            CYCLE
                        END ! IF
                        
                        Access:JOBSE.ClearKey(jobe:RefNumberKey)
                        jobe:RefNumber = job:Ref_Number
                        IF (Access:JOBSE.TryFetch(jobe:RefNumberKey))
                            CYCLE
                        END ! IF
                        
                        IF (job:Cancelled = 'YES')
                            CYCLE
                        END ! IF
                        IF (CLIP(job:ESN) = '' OR CLIP(job:Account_Number) = '')
                            CYCLE
                        END ! IF
                        
                        ArrivedAtARCDate()
                        
                        WriteToExport()
                        kTotalJobs += 1
                        
                    END ! LOOP
                END ! IF (tra:Account_Number = locHeadAccount)
                
                IF (locCancel)
                    BREAK
                END ! IF
            ELSE
                CYCLE
            END ! IF

            exp.Kill()
!
!            IF (kTotalJobs = 0)
!                REMOVE(locTempFilename)
!                CYCLE
!            END ! IF

            qExportFiles.RecordNumber = glo:Pointer
            qExportFiles.FileName = locTempFileName
            qExportFiles.TotalJobs = kTotalJobs
            ADD(qExportFiles)

            UpdateProgressWindow('Jobs Found: ' & kTotalJobs)
        END ! LOOP i = 1 TO RECORDS(glo:Queue2)
        
        
        
        IF (locCancel = 0 OR locCancel = 2)
            ! Build Excel
            IF (e1.Init(0,0) = 0)
            END ! IF

            UpdateProgressWindow('')
            UpdateProgressWindow('Building Excel File(s)')

            LOOP i = 1 TO RECORDS(qExportFiles)
                GET(qExportFiles,i)

                Access:TRADEACC.ClearKey(tra:RecordNumberKey)
                tra:RecordNumber = qExportFiles.RecordNumber
                IF (Access:TRADEACC.TryFetch(tra:RecordNumberKey))
                END ! IF

                UpdateProgressWindow('')
                UpdateProgressWindow('Creating File: ' & CLIP(tra:Account_Number) & ' ' & CLIP(tra:Company_Name))

                Do UpdateProgressWindow

                ! Get data from temp csv file
                e1.OpenWorkBook(qExportFiles.Filename)
                e1.Copy('A1',kFinalColumn & qExportFiles.TotalJobs)

                locClipboard = CLIPBOARD()

                ClearClipBoard()

                e1.CloseWorkBook(2)

                REMOVE(qExportFiles.Filename)

                e1.NewWorkBook
                e1.RenameWorksheet(locReportName & ' ' & CLIP(tra:Account_Number))
                e1.SelectCells('A12')

                SETCLIPBOARD(locClipboard)

                e1.Paste()

                ClearClipBoard()

                FinalFormatting(kFinalColumn & (qExportFiles.ToTalJobs + 12))

                !now for the summary stuff
                do SummaryStuff

                REMOVE(CLIP(locReportFolder) & '\' & CLIP(tra:Account_Number) & ' ' & FORMAT(TODAY(),@d012) & '.xls')
                e1.SaveAs(CLIP(locReportFolder) & '\' & CLIP(tra:Account_Number) & ' ' & FORMAT(TODAY(),@d012))
                e1.CloseWorkBook(2)

            END ! LOOP




            e1.Kill()

            UpdateProgressWindow('===============')
            UpdateProgressWindow('Report Finished: ' & Format(Today(),@d6b) & ' ' & Format(Clock(),@t1b))

        ELSE
            UpdateProgressWindow('===============')
            UpdateProgressWindow('Report Cancelled: ' & FORMAT(Today(),@d6) & |
                ' ' & FORMAT(clock(),@t1))
        END

        !REMOVE(ExportFile)

        DO EndProgress

        ?progress:userstring{PROP:Text} = 'Finished...'
        display()
        ?ProgressCancel{PROP:Hide} = 1


        IF (Automatic = TRUE)
            CLOSE(ProgressWindow)
            POST(Event:CloseWindow)

        ELSE

            ?Finish{prop:Hide} = 0
            ?Button:OpenReportFolder{PROP:Hide} = 0
            ACCEPT
                CASE field()
                OF ?Finish
                    CASE EVENT()
                    OF EVENT:Accepted
                        BREAK
                    END
                OF ?Button:OpenReportFolder
                    CASE EVENT()
                    OF EVENT:Accepted
                        run('explorer.exe ' & locReportFolder)
                    END
                END
            END
            CLOSE(progresswindow)
            Post(EVENT:CloseWindow)
        END !
FinalFormatting     PROCEDURE(STRING pLastSquare)
    CODE
        e1.WriteToCell('Booking Performance Report ' & tra:Account_Number,'A1')
        e1.WriteToCell('Criteria','A3')
        e1.WriteToCell(tmp:VersionNumber,'D3')
        e1.WriteToCell('Start Date','A4')
        e1.WriteToCell(FORMAT(locStartDate,@d06b),'B4')
        e1.WriteToCell('End Date','A5')
        e1.WriteToCell(FORMAT(locEndDate,@d06b),'B5')
        e1.WriteToCell('Created By','A6')
        e1.WriteToCell(locUserName,'B6')
        e1.WriteToCell('Created Date','A7')
        e1.WriteToCell(FORMAT(TODAY(),@d06b),'B7')
        e1.WriteToCell('Section Name','A9')
        e1.WriteToCell('Booking Performace Report ' & tra:Account_Number,'B9')
        e1.WriteToCell('Total Records:','E9')
        e1.WriteToCell('=COUNT(D12:D' & qExportFiles.TotalJobs + 12 & ')','F9')
        e1.WriteToCell('Showing:','G9')
        e1.WriteToCell('=SUBTOTAL(2, D12:D' & qExportFiles.TotalJobs + 12 & ')','H9')
        e1.WriteToCell('User Code','A11')
        e1.WriteToCell('User Surname','B11')
        e1.WriteToCell('User Forename','C11')
        e1.WriteToCell('SB Job Number','D11')
        e1.WriteToCell('Franchise Branch Number','E11')
        e1.WriteToCell('Franchise Job Number','F11')
        e1.WriteToCell('Date Booked','G11')
        e1.WriteToCell('Head Account No','H11')
        e1.WriteToCell('Head Account Name','I11')
        e1.WriteToCell('Trade Account','J11')
        e1.WriteToCell('Trade Account Name','K11')
        e1.WriteToCell('Initial Transit Type','L11')
        e1.WriteToCell('Chargeable Charge Type','M11')
        e1.WriteToCell('Warranty Charge Type','N11')
        e1.WriteToCell('Incoming Courier','O11')
        e1.WriteToCell('Courier Waybill Number','P11')
        e1.WriteToCell('Consignment Number','Q11')
        e1.WriteToCell('Make','R11')
        e1.WriteToCell('Model Number','S11')
        e1.WriteToCell('Unit Type','T11')
        e1.WriteToCell('IMEI','U11')
        e1.WriteToCell('MSN','V11')

        !TB13287 - J - 08/01/15 - add title for SN
        e1.WriteToCell('Specifc Needs Device','W11')
        e1.WriteToCell('Type of SND','X11')

        e1.SetCellFontName('Tahoma','A1',pLastSquare)
        e1.SetCellFontSize(12,'A1')
        e1.SetCellFontSize(8,'A2',pLastSquare)
        e1.SetCellFontStyle('Bold','A1','X11')
        e1.SelectCells('A12')
        e1.FreezePanes()
        e1.SetColumnWidth('A',pLastSquare)
        e1.SetColumnWidth('A',,'14')
        e1.SetColumnWidth('B',,'20')
        e1.SetColumnWidth('D',,'14')

        e1.SetCellBackgroundColor(color:Silver,'A1','H1')
        e1.SetCellBackgroundColor(color:Silver,'A3','H7')
        e1.SetCellBackgroundColor(color:Silver,'A9','H9')
        e1.SetCellBackgroundColor(color:Silver,'A11','X11')

GetUserName     PROCEDURE() 
kCommandLine STRING(255)
pos LONG()
    CODE
        kCommandLine     = COMMAND('')
        
        Automatic = FALSE
        
        pos = INSTRING('%',kCommandLine,1,1)
        
        Access:USERS.ClearKey(use:Password_Key)
        use:Password = CLIP(SUB(kCommandLine,pos + 1,30))
        IF (Access:USERS.TryFetch(use:Password_Key))
            locUserName = 'AUTOMATED PROCEDURE'
            Automatic = TRUE
        ELSE

            locUserName = use:Forename
            
            IF (CLIP(locUsername) = '')
                locUsername = use:Surname
            ELSE
                locUsername = CLIP(use:Forename) & ' ' & CLIP(use:Surname)
            END
            
            IF (locUsername = '')
                locUsername = '<' & use:User_Code & '>'
            END ! IF
        END ! IF
        

       ! message('End of get user name - got '&clip(LocUserName))
SetupVariables      PROCEDURE()
    CODE
        locStartDate = TODAY()
        locEndDate = TODAY()
        locHeadAccount = GETINI('BOOKING','HEADACCOUNT','',CLIP(PATH()) & '\SB2KDEF.INI')
        locARCLocation = GETINI('RRC','ARCLocation', 'RECEIVED AT ARC', '.\SB2KDEF.INI')

        ! Create folder in My Documents
        SHGetSpecialFolderPathAA(GetDesktopWindow(),locReportFolder,5,0)
        locReportFolder = CLIP(locReportFolder) & '\ServiceBase Export'
        
        IF (NOT Exists(locReportFolder))
            IF (MKDir(locReportFolder))
                Beep(Beep:SystemHand)  ;  Yield()
                Case Message('An error has occurred finding, or creating the folder for the report.'&|
                    '|'&|
                    '|' & Clip(locReportFolder) & ''&|
                    '|'&|
                    '|Please quit and try again.','ServiceBase',|
                    Icon:Hand,'&OK',1)
                Of 1 ! &OK Button
                End!Case Message
                RETURN
            END
        END

        locReportFolder = Clip(locReportFolder) & '\' & clip(locReportName)

        IF (NOT Exists(locReportFolder))
            IF (MKDir(locReportFolder))
                Beep(Beep:SystemHand)  ;  Yield()
                Case Message('An error has occurred finding, or creating the folder for the report.'&|
                    '|'&|
                    '|' & Clip(locReportFolder) & ''&|
                    '|'&|
                    '|Please quit and try again.','ServiceBase',|
                    Icon:Hand,'&OK',1)
                Of 1 ! &OK Button
                End!Case Message
                RETURN
            END
        END  
TagAccounts     PROCEDURE()
    CODE

    !message('Calling tag accounts')

        FREE(glo:Queue)
        Access:TRADEACC.ClearKey(tra:RecordNumberKey)
        tra:RecordNumber = 1
        SET(tra:RecordNumberKey,tra:RecordNumberKey)
        LOOP UNTIL Access:TRADEACC.Next() <> Level:Benign
            IF GETINI('BOOKINGPERFORMANCE',tra:Account_Number,0,CLIP(Path()) & '\REPAUTO.INI') = 1
                glo:Pointer = tra:RecordNumber
                ADD(glo:Queue)
            END ! IF
        END ! LOOP




UpdateProgressWindow        PROCEDURE(STRING fText)
    CODE
        staque:StatusMessage = clip(fText)
        ADD(StatusQueue)
        select(?List1,RECORDS(StatusQueue))
        display()

WriteToExport       PROCEDURE()
    CODE
        ! User to booked (or received at ARC)
        Access:USERS.ClearKey(use:User_Code_Key)
        use:User_Code = locARCReceivedBy
        IF (Access:USERS.TryFetch(use:User_Code_Key) = Level:Benign)
            exp.AddField(use:User_Code,1)
            exp.AddField(use:Surname)
            exp.AddField(use:Forename)
        ELSE
            exp.AddField(,1)
            exp.AddField()
            exp.AddField()

            !to update the user results queue use this data
            use:User_Code = LocARCReceivedBy
            use:Forename  = 'UNKNOWN'
            use:Surname   = 'UNKNOWN'
        END ! IF
        
        ! Job Number
        exp.AddField(job:Ref_Number)
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = wob:HeadAccountNumber
        IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key))
        END ! IF
        exp.AddField(tra:BranchIdentification)
        exp.AddField(wob:JobNumber)
       
        ! Date Booked
        exp.AddField(FORMAT(job:Date_Booked,@d06b))
        
        ! Head Account
        exp.AddField(tra:Account_Number)
        exp.AddField(tra:Company_Name)
        ! Sub Account
        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = job:Account_Number
        IF (Access:SUBTRACC.TryFetch(sub:Account_Number_Key))
        END ! IF
        exp.AddField(sub:Account_Number)
        exp.AddField(sub:Company_Name)
        
        ! Transit Type
        exp.AddField(job:Transit_Type)
        
        ! Charge Types
        IF (job:Chargeable_Job = 'YES') 
            exp.AddField(job:Charge_Type)
        ELSE
            exp.AddField()
        END ! IF
        IF (job:Warranty_Job = 'YES')
            exp.AddField(job:Warranty_Charge_Type)
        ELSE
            exp.AddField()
        END ! IF
        
        ! Incoming Courier
        exp.AddField(job:Incoming_Courier)
        
        ! Courier Waybill Number
        Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
        jobe2:RefNumber = job:Ref_Number
        IF (Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign)
            exp.AddField(jobe2:CourierWaybillNumber)
        ELSE
            exp.AddField()
        END ! IF
        
        ! Consignment Number
        exp.AddField(job:Consignment_Number)
        
        ! Manufacturer
        exp.AddField(job:Manufacturer)
        ! Model Number
        exp.AddField(job:Model_Number)
        ! Unit Type
        exp.AddField(job:Unit_Type)
        ! IMEI
        IF (job:ESN <> '')
            exp.AddField('''' & job:ESN)
        ELSE
            exp.AddField()
        END ! IF
        ! MSN
        IF (job:MSN <> '')
            exp.AddField('''' & job:MSN)    !was exp.AddField('''' & job:MSN,,1) TB13287 means this is not end of line
        ELSE
            exp.AddField()                  !was exp.AddField(,,1) TB13287 means this is not end of line
        END ! IF

        !TB13287 - J - 08/01/15 - add the Specific needs detail
        Access:jobse3.clearkey(jobe3:KeyRefNumber)
        jobe3:RefNumber = Job:Ref_number
        if access:jobse3.fetch(jobe3:KeyRefNumber)
            !Add records for No and Blank
            exp.AddField('NO',,1)
        ELSE
            if jobe3:SpecificNeeds = false
                !add records for No and blank
                exp.AddField('NO')
                exp.AddField(,,1)
            ELSE
                !Add records for yes and jobe3:FaultCode21
                exp.AddField('YES')
                exp.AddField(clip(jobe3:FaultCode21),,1)
            END
        END !if jobse3.fetch


        !add detail to user result queue to produce summary
        CLEAR(UserResultsQueue)
        uq:UserCode = use:User_Code                  !J - TB13219 - need to add account number as well in the fetch to get right user right and  account match
        uq:AccountNumber = CurrentAccountNumber      !this line was missing
        GET(UserResultsQueue, +uq:UserCode, +uq:AccountNumber)    !this was GET(UserResultsQueue, +uq:UserCode)
        if ERRORCODE() then
            !add the detail
            uq:UserCode      = use:User_Code
            uq:Surname       = use:Surname
            uq:Forename      = use:Forename
            uq:AccountNumber = CurrentAccountNumber
            uq:JobsBooked    = 1
            Add(UserResultsQueue)
        ELSE
            !update
            uq:JobsBooked += 1
            put(UserResultsQueue)
        END !if errorcode()
!---------------------------------------------------------------------------------
E1.Init   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0)
ReturnValue   byte
  CODE
  ReturnValue = PARENT.Init (pStartVisible,pEnableEvents)
  self.TakeSnapShotOfWindowPos()
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.Kill   PROCEDURE (byte pUnloadCOM=1)
ReturnValue   byte
  CODE
  self.RestoreSnapShotOfWindowPos()
  ReturnValue = PARENT.Kill (pUnloadCOM)
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.TakeEvent   PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0)
  CODE
  PARENT.TakeEvent (pEventString1,pEventString2,pEventNumber,pEventType,pEventStatus)
  if pEventType = 0  ! Generated by CapeSoft Office Inside
    case event()
      of event:accepted
        case field()
      end
    end
  end

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window


BRW1.Fetch PROCEDURE(BYTE Direction)

GreenBarIndex   LONG
  CODE
  PARENT.Fetch(Direction)


BRW1.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = tra:RecordNumber
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      locTag = ''
    ELSE
      locTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  
  IF (locTag = '*')
    SELF.Q.locTag_Icon = 2                                 ! Set icon from icon list
  ELSE
    SELF.Q.locTag_Icon = 1                                 ! Set icon from icon list
  END


BRW1.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW1::RecordStatus=ReturnValue
  IF BRW1::RecordStatus NOT=Record:OK THEN RETURN BRW1::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = tra:RecordNumber
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::3:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW1::RecordStatus
  RETURN ReturnValue

XFiles PROCEDURE                                           ! Generated from procedure template - Window

QuickWindow          WINDOW('Window'),AT(,,260,160),FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:ANSI),CENTER,IMM,HLP('XFiles'),SYSTEM,GRAY,RESIZE
                       BUTTON('&OK'),AT(140,140,56,16),USE(?Ok),MSG('Accept operation'),TIP('Accept Operation')
                       BUTTON('&Cancel'),AT(200,140,56,16),USE(?Cancel),MSG('Cancel Operation'),TIP('Cancel Operation')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED                 ! Method added to host embed code
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False) ! Method added to host embed code
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
RecolourWindow      Routine


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('XFiles')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Ok
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Ok,RequestCancelled)                    ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Ok,RequestCompleted)                    ! Add the close control to the window manger
  END
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:SRNTEXT.Open                                      ! File SRNTEXT used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('XFiles',QuickWindow)                       ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  Do RecolourWindow
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:SRNTEXT.Close
  END
  IF SELF.Opened
    INIMgr.Update('XFiles',QuickWindow)                    ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

ds_Stop              PROCEDURE  (<string StopText>)        ! Declare Procedure
returned              long
TempTimeOut           long(0)
TempNoLogging         long(0)
Loc:StopText    string(4096)
  CODE
  if omitted(1) or StopText = '' then
    if ThisMessageBox.GetGlobalSetting('TranslationFile') <> ''
      Loc:StopText = getini('MessageBox_Text','StopDefault','Exit?',ThisMessageBox.GetGlobalSetting('TranslationFile'))
    else
      Loc:StopText = 'Exit?'
    end
  else
    Loc:StopText = StopText
  end
  if ThisMessageBox.GetGlobalSetting('TranslationFile') <> ''
    returned = ds_Message(Loc:StopText,getini('MessageBox_Text','StopHeader','Stop',ThisMessageBox.GetGlobalSetting('TranslationFile')),ICON:Hand,BUTTON:Abort+BUTTON:Ignore,BUTTON:Abort)
  else
    returned = ds_Message(Loc:StopText,'Stop',ICON:Hand,BUTTON:Abort+BUTTON:Ignore,BUTTON:Abort)
  end
  if returned = BUTTON:Abort then halt .
RecolourWindow      Routine

ds_Halt              PROCEDURE  (UNSIGNED Level=0,<STRING HaltText>) ! Declare Procedure
TempNoLogging         long(0)
  CODE
  if ~omitted(2) then
    if ThisMessageBox.GetGlobalSetting('TranslationFile') <> ''
      ds_Message(HaltText,getini('MessageBox_Text','HaltHeader','Halt',ThisMessageBox.GetGlobalSetting('TranslationFile')),ICON:Hand)
    else
      ds_Message(HaltText,'Halt',ICON:Hand)
    end
  end
  system{prop:HaltHook} = 0
  HALT(Level)
RecolourWindow      Routine

ds_Message PROCEDURE (STRING MessageTxt,<STRING HeadingTxt>,<STRING IconSent>,<STRING ButtonsPar>,UNSIGNED MsgDefaults=0,BOOL StylePar=FALSE) ! Generated from procedure template - Window

FilesOpened          BYTE                                  !
Loc:ButtonPressed    UNSIGNED                              !
EmailLink            CSTRING(4096)                         !
DontShowThisAgain       byte(0)         !CapeSoft MessageBox Data
LocalMessageBoxdata     group,pre(LMBD) !CapeSoft MessageBox Data
MessageText               cstring(4096)
HeadingText               cstring(1024)
UseIcon                   cstring(1024)
Buttons                   cstring(1024)
Defaults                  unsigned
                        end
window               WINDOW('Caption'),AT(,,404,108),FONT('Tahoma',8,,FONT:regular),COLOR(COLOR:White),GRAY
                       IMAGE,AT(11,18),USE(?Image1),HIDE
                       PROMPT(''''),AT(118,32),USE(?MainTextPrompt)
                       STRING('HyperActive Link'),AT(91,46),USE(?HALink),HIDE
                       STRING('Time Out:'),AT(103,54),USE(?TimerCounter),HIDE
                       CHECK('Dont Show This Again'),AT(85,65),USE(DontShowThisAgain),HIDE
                       BUTTON('Button 1'),AT(9,81,45,14),USE(?Button1),HIDE
                       BUTTON('Button 2'),AT(59,81,45,14),USE(?Button2),HIDE
                       BUTTON('Button 3'),AT(109,81,45,14),USE(?Button3),HIDE
                       BUTTON('Button 4'),AT(159,81,45,14),USE(?Button4),HIDE
                       BUTTON('Button 5'),AT(209,81,45,14),USE(?Button5),HIDE
                       BUTTON('Button 6'),AT(259,81,45,14),USE(?Button6),HIDE
                       BUTTON('Button 7'),AT(309,81,45,14),USE(?Button7),HIDE
                       BUTTON('Button 8'),AT(359,81,45,14),USE(?Button8),HIDE
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED                 ! Method added to host embed code
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Run                    PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop
  RETURN(Loc:ButtonPressed)

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
RecolourWindow      Routine

ThisMessageBoxLogInit          routine         !CapeSoft MessageBox Initialize properties routine
    if ~omitted(2) then LMBD:HeadingText = HeadingTxt .
    if ~omitted(3) then LMBD:UseIcon = IconSent .
    if ~omitted(4) then LMBD:Buttons = ButtonsPar .
    if ~omitted(5) then LMBD:Defaults = MsgDefaults .
    LMBD:MessageText = MessageTxt
    ThisMessageBox.FromExe = command(0)
    if instring('\',ThisMessageBox.FromExe,1,1)
      ThisMessageBox.FromExe = ThisMessageBox.FromExe[ (instring('\',ThisMessageBox.FromExe,-1,len(ThisMessageBox.FromExe)) + 1) : len(ThisMessageBox.FromExe) ]
    end
    ThisMessageBox.TrnStrings = 1
    ThisMessageBox.PromptControl = ?MainTextPrompt
    ThisMessageBox.IconControl = ?Image1
         !End of CapeSoft MessageBox Initialize properties routine

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
    ThisMessageBox.SavedResponse = GlobalResponse        !CapeSoft MessageBox Code - Preserves the GlobalResponse
  GlobalErrors.SetProcedureName('ds_Message')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
        GlobalRequest = ThisWindow.Request         !Keep GlobalRequest the correct value.
        if ThisMessageBox.MessagesUsed then
          return level:fatal
        end
        ThisMessageBox.MessagesUsed += 1
        do ThisMessageBoxLogInit                               !CapeSoft MessageBox Code
  SELF.Open(window)                                        ! Open window
  Do DefineListboxStyle
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  ThisMessageBox.open(LMBD:MessageText,LMBD:HeadingText,LMBD:UseIcon,LMBD:Buttons,LMBD:Defaults,StylePar)         !CapeSoft MessageBox Code
      alert(EscKey)
  Do RecolourWindow
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
              !CapeSoft MessageBox EndCode
    Loc:ButtonPressed = ThisMessageBox.ReturnValue
    if ThisMessageBox.MessagesUsed > 0 then ThisMessageBox.MessagesUsed -= 1 .
              !End of CapeSoft MessageBox EndCode
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
    ReturnValue = ThisMessageBox.SavedResponse           !CapeSoft MessageBox Code - Preserves the GlobalResponse
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ThisMessageBox.TakeEvent()                             !CapeSoft MessageBox Code
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

HelpBrowser PROCEDURE (func:Path)                          ! Generated from procedure template - Window

fepath               STRING(255)                           !
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
Window               WINDOW('Online Help'),AT(522,0,154,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(09A6A7CH),TILED,SYSTEM,GRAY,MAX,RESIZE,IMM
                       BUTTON,AT(0,0),USE(?Button1),TRN,FLAT,ICON('closep.jpg'),STD(STD:Close)
                       PROMPT('Close Online Help'),AT(72,8),USE(?Prompt1),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(-3,25,159,404),USE(?feControl)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED                 ! Method added to host embed code
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Open                   PROCEDURE(),DERIVED                 ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False) ! Method added to host embed code
Resize                 PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END


                     ! Start: FE Class Declaration
ThisViewer1          class(FeBrowser)
                     END
                     !End: FE Class Declaration


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
feGetFileFromUser_ThisViewer1  routine
  ThisViewer1.fe_AppPath = Path()           ! Used to restore current path after selection
  if ThisViewer1.fe_LastPath <> ''          ! Var set below
    SetPath (ThisViewer1.fe_LastPath)       ! Set path to location of last selected file
  end
  ThisViewer1.fe_RestorePath = ThisViewer1.fe_Path  ! current document's path
  ThisViewer1.fe_Path = ''
  if not FileDialog('Choose File to View', ThisViewer1.fe_Path, 'HTML Files|*.htm;*.html;*.mht|Text Files|*.txt|Image Files|*.gif;*.jpeg;*.jpg;*.png;*.art;*.au;*.aiff;*.xbm|XML Files|*.xml|EML Files|*.eml|All Files|*.*', 10000b)
    SetPath (ThisViewer1.fe_AppPath)        ! Restore path to app path if cancelled
    ThisViewer1.fe_Path = ThisViewer1.fe_RestorePath
  else
    ThisViewer1.fe_LastPath = Path()        ! Path of selected file
    SetPath (ThisViewer1.fe_AppPath)        ! Restore path
    ThisViewer1.Load (ThisViewer1.fe_Path)  ! Load the selected file
  end
 exit

fePathControlAccepted_ThisViewer1  routine
  ThisViewer1.fe_Path = ''
  ThisViewer1.Load (ThisViewer1.fe_Path)
  exit

feWindowClosing_ThisViewer1  routine
  ThisViewer1.Kill(2)  ! GJxxyy123 WIP test code, 5 Oct 2005
 exit

feRecoveryCode_ThisViewer1  routine
 ! This routine is obsolete.  Please read the docs or contact support@capesoft.com
 exit

feLoadInitialDocument_ThisViewer1  routine
  ! FileExplorer - Loading initial document as window opens
  ThisViewer1.fe_Path = fepath
  ThisViewer1.Load (ThisViewer1.fe_Path)
 exit

feWindowTakeFieldEvent_fePathControl_EventAlertKey_ThisViewer1  routine
  case keycode()
    of EnterKey
      post(event:accepted, )
    of CtrlEnter
      post(event:accepted, )
  end
 exit


RecolourWindow      Routine


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('HelpBrowser')
  fepath = Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & Clip(func:Path) & '.htm'
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  ThisViewer1.TempDirLocation = 0    ! temp files stored with the exe
  
   ! The feBrowser object uses "Early Binding" at this stage.  At some point we might
   ! make it optional to use "Late Binding" instead.  This switch sets which approach
   ! we use.  Don't change it for now...
   ! ThisViewer1.UseLateBinding = Glo:UseLateBinding
  
  
   ! Uncomment this line of code to override the above code where the property is set...
   ! For testing purposes...  Set the switch in the ProgramOptions screen (disabled by default).
   ! ThisViewer1.AutoDialDefaultInternetConnection = Glo:DUNAutoDialOption
   ! 0 - do not dial the default dial up connection
   ! 1 - dial the "Windows Default" connection
   ! 2 - establish the default connection, then dial it
  
   ThisViewer1.BetaCallbacksOn = 0  ! GJxxyy FILEEXPLORER TEST CODE
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Button1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.Open(Window)                                        ! Open window
  Do DefineListboxStyle
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Spread)                         ! Controls will spread out as the window gets bigger
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('HelpBrowser',Window)                       ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  Do RecolourWindow
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.Opened
    INIMgr.Update('HelpBrowser',Window)                    ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Open PROCEDURE

  CODE
  PARENT.Open
  ThisViewer1.Init (?feControl, , , , 1)


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
   ThisViewer1.TakeEvent()
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  
    OF EVENT:feCallback
      ! The File Explorer dll will post this event if you return fe:DivertNavigation from the
      ! (virtual) method ThisViewer::EventCallback.
  
    OF EVENT:feDelayedClose  ! GJxxyy123 WIP added 051005
      ThisViewer1.Kill()
  
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
        ThisViewer1._WindowClosing()  ! GJ added 280205
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:CloseWindow
      do feWindowClosing_ThisViewer1
    OF EVENT:OpenWindow
      do feLoadInitialDocument_ThisViewer1
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Button1, Resize:FixLeft+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Button1
  SELF.SetStrategy(?feControl, Resize:FixLeft+Resize:FixTop, Resize:ConstantRight+Resize:ConstantBottom) ! Override strategy for ?feControl
  SELF.SetStrategy(?Prompt1, Resize:FixLeft+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Prompt1


Resizer.Resize PROCEDURE

ReturnValue          BYTE,AUTO


  CODE
  ReturnValue = PARENT.Resize()
  ! Auto-Resizing the File Explorer COM Object
  ThisViewer1.Resize ()
  RETURN ReturnValue


