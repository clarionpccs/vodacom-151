  MEMBER('vodr0079.clw')

  INCLUDE('ABFILE.INC'),ONCE

  MAP
VODR0BC7:DctInit    PROCEDURE
VODR0BC7:DctKill    PROCEDURE
VODR0BC7:FilesInit  PROCEDURE
  END

Hide:Access:EXCHANGE_ALIAS CLASS(FileManager),TYPE         ! FileManager for EXCHANGE_ALIAS
Init                   PROCEDURE(),DERIVED                 ! Method added to host embed code
Kill                   PROCEDURE(),DERIVED                 ! Method added to host embed code
UseFile                PROCEDURE(BYTE UseType = UseType:Uses),BYTE,PROC,DERIVED ! Method added to host embed code
                     END


Hide:Relate:EXCHANGE_ALIAS CLASS(RelationManager),TYPE     ! RelationManager for EXCHANGE_ALIAS
Init                   PROCEDURE
DeferedAddRelations    PROCEDURE(),DERIVED                 ! Method added to host embed code
Kill                   PROCEDURE(),DERIVED                 ! Method added to host embed code
                     END

Hide:Access:JOBS_ALIAS CLASS(FileManager),TYPE             ! FileManager for JOBS_ALIAS
Init                   PROCEDURE(),DERIVED                 ! Method added to host embed code
Kill                   PROCEDURE(),DERIVED                 ! Method added to host embed code
UseFile                PROCEDURE(BYTE UseType = UseType:Uses),BYTE,PROC,DERIVED ! Method added to host embed code
                     END


Hide:Relate:JOBS_ALIAS CLASS(RelationManager),TYPE         ! RelationManager for JOBS_ALIAS
Init                   PROCEDURE
DeferedAddRelations    PROCEDURE(),DERIVED                 ! Method added to host embed code
Kill                   PROCEDURE(),DERIVED                 ! Method added to host embed code
                     END


_Hide:Access:EXCHANGE_ALIAS &Hide:Access:EXCHANGE_ALIAS,AUTO,THREAD
_Hide:Relate:EXCHANGE_ALIAS &Hide:Relate:EXCHANGE_ALIAS,AUTO,THREAD
_Hide:Access:JOBS_ALIAS &Hide:Access:JOBS_ALIAS,AUTO,THREAD
_Hide:Relate:JOBS_ALIAS &Hide:Relate:JOBS_ALIAS,AUTO,THREAD


VODR0BC7:DctInit PROCEDURE
  CODE
  _Hide:Access:EXCHANGE_ALIAS &= NEW(Hide:Access:EXCHANGE_ALIAS)
  _Hide:Relate:EXCHANGE_ALIAS &= NEW(Hide:Relate:EXCHANGE_ALIAS)
  _Hide:Access:JOBS_ALIAS &= NEW(Hide:Access:JOBS_ALIAS)
  _Hide:Relate:JOBS_ALIAS &= NEW(Hide:Relate:JOBS_ALIAS)
  Relate:EXCHANGE_ALIAS &= _Hide:Relate:EXCHANGE_ALIAS
  Relate:JOBS_ALIAS &= _Hide:Relate:JOBS_ALIAS


VODR0BC7:FilesInit PROCEDURE
  CODE
  _Hide:Relate:EXCHANGE_ALIAS.Init
  _Hide:Relate:JOBS_ALIAS.Init


VODR0BC7:DctKill PROCEDURE
  CODE
  _Hide:Relate:EXCHANGE_ALIAS.Kill
  DISPOSE(_Hide:Relate:EXCHANGE_ALIAS)
  _Hide:Relate:JOBS_ALIAS.Kill
  DISPOSE(_Hide:Relate:JOBS_ALIAS)


Hide:Relate:EXCHANGE_ALIAS.Init PROCEDURE
  CODE
  _Hide:Access:EXCHANGE_ALIAS.Init
  SELF.Init(Access:EXCHANGE_ALIAS,1)
  SELF.SetAlias(Relate:EXCHANGE)


Hide:Access:EXCHANGE_ALIAS.Init PROCEDURE

  CODE
  SELF.Initialized = False
  SELF.Buffer &= xch_ali:Record
  SELF.AliasedFile &= Access:EXCHANGE                      !This is a File Alias, so assign aliased file manager
  SELF.FileNameValue = 'EXCHANGE_ALIAS'
  SELF.SetErrors(GlobalErrors)
  SELF.File &= EXCHANGE_ALIAS
  PARENT.Init
  Access:EXCHANGE_ALIAS &= SELF


Hide:Access:EXCHANGE_ALIAS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:EXCHANGE_ALIAS &= NULL                            ! File manager has been destroyed to assign null, it is an error to reference this after this point


Hide:Access:EXCHANGE_ALIAS.UseFile PROCEDURE(BYTE UseType = UseType:Uses)

ReturnValue          BYTE,AUTO

  CODE
  IF UseType ~= UseType:Initialize
    SELF.UseFile(UseType:Initialize)                       !Recursive call to ensure initialization takes place
  END
  IF UseType = UseType:Initialize AND ~SELF.Initialized
    SELF.InUseFile = True
    SELF.Init(EXCHANGE_ALIAS,GlobalErrors)
    SELF.Create = 1
    SELF.LockRecover = 10
    SELF.AddKey(xch_ali:Ref_Number_Key,'By Loan Unit Number',1)
    SELF.AddKey(xch_ali:AvailLocIMEI,'xch_ali:AvailLocIMEI',0)
    SELF.AddKey(xch_ali:AvailLocMSN,'xch_ali:AvailLocMSN',0)
    SELF.AddKey(xch_ali:AvailLocRef,'xch_ali:AvailLocRef',0)
    SELF.AddKey(xch_ali:AvailLocModel,'xch_ali:AvailLocModel',0)
    SELF.AddKey(xch_ali:ESN_Only_Key,'xch_ali:ESN_Only_Key',0)
    SELF.AddKey(xch_ali:MSN_Only_Key,'xch_ali:MSN_Only_Key',0)
    SELF.AddKey(xch_ali:Ref_Number_Stock_Key,'By Ref Number',0)
    SELF.AddKey(xch_ali:Model_Number_Key,'By Model Number',0)
    SELF.AddKey(xch_ali:ESN_Key,'By E.S.N. / I.M.E.I. Number',0)
    SELF.AddKey(xch_ali:MSN_Key,'By M.S.N. Number',0)
    SELF.AddKey(xch_ali:ESN_Available_Key,'By E.S.N. / I.M.E.I.',0)
    SELF.AddKey(xch_ali:MSN_Available_Key,'By M.S.N.',0)
    SELF.AddKey(xch_ali:Ref_Available_Key,'By Exchange Unit Number',0)
    SELF.AddKey(xch_ali:Model_Available_Key,'By Model Number',0)
    SELF.AddKey(xch_ali:Stock_Type_Key,'By Stock Type',0)
    SELF.AddKey(xch_ali:ModelRefNoKey,'By Unit Number',0)
    SELF.AddKey(xch_ali:AvailIMEIOnlyKey,'By I.M.E.I. Number',0)
    SELF.AddKey(xch_ali:AvailMSNOnlyKey,'By M.S.N.',0)
    SELF.AddKey(xch_ali:AvailRefOnlyKey,'By Unit Number',0)
    SELF.AddKey(xch_ali:AvailModOnlyKey,'By Unit Number',0)
    SELF.AddKey(xch_ali:StockBookedKey,'By Unit Number',0)
    SELF.AddKey(xch_ali:AvailBookedKey,'By Unit Number',0)
    SELF.AddKey(xch_ali:DateBookedKey,'By Date Booked',0)
    SELF.AddKey(xch_ali:LocStockAvailIMEIKey,'By I.M.E.I. Number',0)
    SELF.AddKey(xch_ali:LocStockIMEIKey,'By I.M.E.I. Number',0)
    SELF.AddKey(xch_ali:LocIMEIKey,'By I.M.E.I. Number',0)
    SELF.AddKey(xch_ali:LocStockAvailRefKey,'By Exchange Unit Number',0)
    SELF.AddKey(xch_ali:LocStockRefKey,'By Exchange Unit Number',0)
    SELF.AddKey(xch_ali:LocRefKey,'By Exchange Unit Number',0)
    SELF.AddKey(xch_ali:LocStockAvailModelKey,'By Exchange Unit Number',0)
    SELF.AddKey(xch_ali:LocStockModelKey,'By Exchange Unit Number',0)
    SELF.AddKey(xch_ali:LocModelKey,'By Exchange Unit Number',0)
    SELF.AddKey(xch_ali:LocStockAvailMSNKey,'By M.S.N.',0)
    SELF.AddKey(xch_ali:LocStockMSNKey,'By Exchange Unit Number',0)
    SELF.AddKey(xch_ali:LocMSNKey,'By M.S.N.',0)
    SELF.AddKey(xch_ali:InTransitLocationKey,'By I.M.E.I. Number',0)
    SELF.AddKey(xch_ali:InTransitKey,'By I.M.E.I. Number',0)
    SELF.AddKey(xch_ali:StatusChangeDateKey,'By Status Change Date',0)
    SELF.AddKey(xch_ali:LocStatusChangeDatekey,'By Status Change Date',0)
  END
                                                           !SELF.InUseFile will be set to False in PARENT
  ReturnValue = PARENT.UseFile(UseType)
  RETURN ReturnValue


Hide:Relate:EXCHANGE_ALIAS.DeferedAddRelations PROCEDURE

  CODE
  DO AddRelations_1
  
  PARENT.DeferedAddRelations

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:EXCAUDIT,RI:CASCADE,RI:None,exa:ReplaceUnitNoKey)
  SELF.AddRelationLink(xch_ali:Ref_Number,exa:Replacement_Unit_Number)


Hide:Relate:EXCHANGE_ALIAS.Kill PROCEDURE

  CODE
  _Hide:Access:EXCHANGE_ALIAS.Kill                         ! Kill the file manager
  PARENT.Kill
  Relate:EXCHANGE_ALIAS &= NULL                            ! Assign NULL to the RelationManager's FileManager reference, it is an error to reference this after this point
  DISPOSE(_Hide:Access:EXCHANGE_ALIAS)                     ! destroy the file manager


Hide:Relate:JOBS_ALIAS.Init PROCEDURE
  CODE
  _Hide:Access:JOBS_ALIAS.Init
  SELF.Init(Access:JOBS_ALIAS,1)
  SELF.SetAlias(Relate:JOBS)


Hide:Access:JOBS_ALIAS.Init PROCEDURE

  CODE
  SELF.Initialized = False
  SELF.Buffer &= job_ali:Record
  SELF.AliasedFile &= Access:JOBS                          !This is a File Alias, so assign aliased file manager
  SELF.FileNameValue = 'JOBS_ALIAS'
  SELF.SetErrors(GlobalErrors)
  SELF.File &= JOBS_ALIAS
  PARENT.Init
  Access:JOBS_ALIAS &= SELF


Hide:Access:JOBS_ALIAS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBS_ALIAS &= NULL                                ! File manager has been destroyed to assign null, it is an error to reference this after this point


Hide:Access:JOBS_ALIAS.UseFile PROCEDURE(BYTE UseType = UseType:Uses)

ReturnValue          BYTE,AUTO

  CODE
  IF UseType ~= UseType:Initialize
    SELF.UseFile(UseType:Initialize)                       !Recursive call to ensure initialization takes place
  END
  IF UseType = UseType:Initialize AND ~SELF.Initialized
    SELF.InUseFile = True
    SELF.Init(JOBS_ALIAS,GlobalErrors)
    SELF.Create = 1
    SELF.LockRecover = 10
    SELF.AddKey(job_ali:Ref_Number_Key,'By Job Number',1)
    SELF.AddKey(job_ali:Model_Unit_Key,'By Job Number',0)
    SELF.AddKey(job_ali:EngCompKey,'By Job Number',0)
    SELF.AddKey(job_ali:EngWorkKey,'By Job Number',0)
    SELF.AddKey(job_ali:Surname_Key,'By Surname',0)
    SELF.AddKey(job_ali:MobileNumberKey,'By Mobile Number',0)
    SELF.AddKey(job_ali:ESN_Key,'By E.S.N. / I.M.E.I.',0)
    SELF.AddKey(job_ali:MSN_Key,'By M.S.N.',0)
    SELF.AddKey(job_ali:AccountNumberKey,'By Account Number',0)
    SELF.AddKey(job_ali:AccOrdNoKey,'By Order Number',0)
    SELF.AddKey(job_ali:Model_Number_Key,'By Model Number',0)
    SELF.AddKey(job_ali:Engineer_Key,'By Engineer',0)
    SELF.AddKey(job_ali:Date_Booked_Key,'By Date Booked',0)
    SELF.AddKey(job_ali:DateCompletedKey,'By Date Completed',0)
    SELF.AddKey(job_ali:ModelCompKey,'By Completed Date',0)
    SELF.AddKey(job_ali:By_Status,'By Job Number',0)
    SELF.AddKey(job_ali:StatusLocKey,'By Job Number',0)
    SELF.AddKey(job_ali:Location_Key,'By Location',0)
    SELF.AddKey(job_ali:Third_Party_Key,'By Third Party',0)
    SELF.AddKey(job_ali:ThirdEsnKey,'By ESN',0)
    SELF.AddKey(job_ali:ThirdMsnKey,'By MSN',0)
    SELF.AddKey(job_ali:PriorityTypeKey,'By Priority',0)
    SELF.AddKey(job_ali:Unit_Type_Key,'By Unit Type',0)
    SELF.AddKey(job_ali:EDI_Key,'By Job Number',0)
    SELF.AddKey(job_ali:InvoiceNumberKey,'By Invoice_Number',0)
    SELF.AddKey(job_ali:WarInvoiceNoKey,'By Invoice Number',0)
    SELF.AddKey(job_ali:Batch_Number_Key,'By Job Number',0)
    SELF.AddKey(job_ali:Batch_Status_Key,'By Ref Number',0)
    SELF.AddKey(job_ali:BatchModelNoKey,'By Job Number',0)
    SELF.AddKey(job_ali:BatchInvoicedKey,'By Ref Number',0)
    SELF.AddKey(job_ali:BatchCompKey,'By Job Number',0)
    SELF.AddKey(job_ali:ChaInvoiceKey,'job_ali:ChaInvoiceKey',0)
    SELF.AddKey(job_ali:InvoiceExceptKey,'By Job Number',0)
    SELF.AddKey(job_ali:ConsignmentNoKey,'By Cosignment Number',0)
    SELF.AddKey(job_ali:InConsignKey,'By Consignment Number',0)
    SELF.AddKey(job_ali:ReadyToDespKey,'By Job Number',0)
    SELF.AddKey(job_ali:ReadyToTradeKey,'By Job Number',0)
    SELF.AddKey(job_ali:ReadyToCouKey,'By Job Number',0)
    SELF.AddKey(job_ali:ReadyToAllKey,'By Job Number',0)
    SELF.AddKey(job_ali:DespJobNumberKey,'By Job Number',0)
    SELF.AddKey(job_ali:DateDespatchKey,'By Job Number',0)
    SELF.AddKey(job_ali:DateDespLoaKey,'By Job Number',0)
    SELF.AddKey(job_ali:DateDespExcKey,'By Job Number',0)
    SELF.AddKey(job_ali:ChaRepTypeKey,'By Chargeable Repair Type',0)
    SELF.AddKey(job_ali:WarRepTypeKey,'By Warranty Repair Type',0)
    SELF.AddKey(job_ali:ChaTypeKey,'job_ali:ChaTypeKey',0)
    SELF.AddKey(job_ali:WarChaTypeKey,'job_ali:WarChaTypeKey',0)
    SELF.AddKey(job_ali:Bouncer_Key,'By Job Number',0)
    SELF.AddKey(job_ali:EngDateCompKey,'By Date Completed',0)
    SELF.AddKey(job_ali:ExcStatusKey,'By Job Number',0)
    SELF.AddKey(job_ali:LoanStatusKey,'By Job Number',0)
    SELF.AddKey(job_ali:ExchangeLocKey,'By Job Number',0)
    SELF.AddKey(job_ali:LoanLocKey,'By Job Number',0)
    SELF.AddKey(job_ali:BatchJobKey,'By Job Number',0)
    SELF.AddKey(job_ali:BatchStatusKey,'By Job Number',0)
  END
                                                           !SELF.InUseFile will be set to False in PARENT
  ReturnValue = PARENT.UseFile(UseType)
  RETURN ReturnValue


Hide:Relate:JOBS_ALIAS.DeferedAddRelations PROCEDURE

  CODE
  DO AddRelations_1
  
  PARENT.DeferedAddRelations

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBSOBF_ALIAS,RI:CASCADE,RI:CASCADE,jofali:RefNumberKey)
  SELF.AddRelationLink(job_ali:Ref_Number,jofali:RefNumber)

  SELF.AddRelation(Relate:BOUNCER,RI:CASCADE,RI:CASCADE,bou:Bouncer_Job_Only_Key)
  SELF.AddRelationLink(job_ali:Ref_Number,bou:Bouncer_Job_Number)

  SELF.AddRelation(Relate:REPAIRTY)


Hide:Relate:JOBS_ALIAS.Kill PROCEDURE

  CODE
  _Hide:Access:JOBS_ALIAS.Kill                             ! Kill the file manager
  PARENT.Kill
  Relate:JOBS_ALIAS &= NULL                                ! Assign NULL to the RelationManager's FileManager reference, it is an error to reference this after this point
  DISPOSE(_Hide:Access:JOBS_ALIAS)                         ! destroy the file manager

