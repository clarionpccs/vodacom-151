!-----------------------------
! Prototype:
! (String f:Manufacturer, Date f:PreviousDOP, *Date f:DOP,  String f:IMEINumber, String f:Bouncer, *Byte f:Found)
!
! Used in sba01app
!
! Files:
! MANUFACT
! EXCHANGE
! JOBS_ALIAS
! JOBSE_ALIAS
!
!-----------------------------
Section('Local Data')
save_MANUFACT_ID    UShort,Auto
save_EXCHANGE_ID    UShort,Auto
save_JOBS_ALIAS_ID  UShort,Auto
save_JOBSE_ALIAS_ID UShort,Auto

!-----------------------------
Section('Processed Code')
    ! Insert --- Check if previous job, or 48 hour exchange, and copy dop is set (DBH: 05/03/2009) #10614
    f:Found = 0

    Relate:MANUFACT.Open()
    Relate:EXCHANGE.Open()
    Relate:JOBS_ALIAS.Open()
    Relate:JOBSE_ALIAS.Open()
    Relate:USERS.Open()
    Relate:ACCAREAS.Open()

    copyDOP# = 0
    save_MANUFACT_ID = Access:MANUFACT.SaveFile()
    Access:MANUFACT.Clearkey(man:Manufacturer_Key)
    man:Manufacturer     = f:Manufacturer
    if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
        ! Found
        if (man:CopyDOPFromBouncer)
            if (f:PreviousDOP = 0)
                ! A Bouncer hasn't already been found, check exchange for 48 Hour Exchange
                save_EXCHANGE_ID = Access:EXCHANGE.SaveFile()
                Access:EXCHANGE.Clearkey(xch:ESN_Only_Key)
                xch:ESN    = f:IMEINumber
                if (Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign)
                    ! Found
                    if (xch:Job_Number > 0)
                        save_JOBS_ALIAS_ID = Access:JOBS_ALIAS.SaveFile()
                        Access:JOBS_ALIAS.Clearkey(job_ali:Ref_Number_Key)
                        job_ali:Ref_Number    = xch:Job_Number
                        if (Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign)
                            ! Found
                            save_JOBSE_ALIAS_ID = Access:JOBSE_ALIAS.SaveFile()
                            Access:JOBSE_ALIAS.Clearkey(jobe_ali:RefNumberKey)
                            jobe_ali:RefNumber    = job_ali:Ref_Number
                            if (Access:JOBSE_ALIAS.TryFetch(jobe_ali:RefNumberKey) = Level:Benign)
                                ! Found
                                if (jobe_ali:Engineer48HourOption And job_ali:DOP > 0)
                                   f:Found = 1
                                   f:DOP = job_ali:DOP
                                end ! if (jobe_ali:Engineer48HourOption)
                            else ! if (Access:JOBSE_ALIAS.TryFetch(jobe_ali) = Level:Benign)
                                ! Error
                            end ! if (Access:JOBSE_ALIAS.TryFetch(jobe_ali) = Level:Benign)
                            Access:JOBSE_ALIAS.RestoreFile(save_JOBSE_ALIAS_ID)
                        else ! if (Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign)
                            ! Error
                        end ! if (Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign)
                        Access:JOBS_ALIAS.RestoreFile(save_JOBS_ALIAS_ID)
                    end ! if (xch:Job_Number > 0)
                else ! if (Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign)
                    ! Error
                end ! if (Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign)
                Access:EXCHANGE.RestoreFile(save_EXCHANGE_ID)
            else ! end ! if (f:PreviousDOP = 0)

                if (f:Bouncer > 0 Or f:Bouncer = 'B')
                    ! Job IS a bouncer (DBH: 05/03/2009) #10614
                    f:Found = 1
                    f:DOP = f:PreviousDOP
                end ! if (job:Bouncer = 'B')
            end ! if (f:PreviousDOP = 0)
        end ! if (man:CopyDOPFromBouncer)
    else ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
        ! Error
    end ! if (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
    Access:MANUFACT.RestoreFile(save_MANUFACT_ID)

    Relate:MANUFACT.Close()
    Relate:EXCHANGE.Close()
    Relate:JOBS_ALIAS.Close()
    Relate:JOBSE_ALIAS.Close()

    ! end --- (DBH: 05/03/2009) #10614
