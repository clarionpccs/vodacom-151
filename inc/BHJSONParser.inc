    OMIT('_EndOfInclude_',_BHJSONParser_)
_BHJSONParser_   EQUATE(1)

! ===========================================================================================
JSONParser   CLASS,TYPE,MODULE('BHJSONParser.clw'),LINK('BHJSONParser.clw',1),DLL(0)
! ===========================================================================================
AddStringToQueue        PROCEDURE(*QUEUE Q,STRING inputString,LONG pNullSkippedFields=0) ! Convert JSON String To Q
AddQueueToString  		PROCEDURE(*STRING outputString,QUEUE inQ) ! Convert Q Into JSON String
EncodeJSONString        PROCEDURE(STRING pJSONString),STRING ! Escape JSON String
DecodeJSONString        PROCEDURE(STRING pJSONString),STRING ! UnEscape JSON String
            END

    _EndOfInclude_
	