

   MEMBER('SBOMonitor.clw')                                ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('VSA_SYSU.INC'),ONCE

                     MAP
                       INCLUDE('SBOMONITOR001.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('SBOMONITOR003.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! Window
!!! </summary>
Main PROCEDURE 

QuickWindow          WINDOW('SBO Monitor'),AT(,,257,42),FONT('Tahoma',8,COLOR:Black,FONT:regular,CHARSET:DEFAULT), |
  DOUBLE,CENTER,ICON(ICON:Clarion),GRAY,IMM,HLP('Main'),SYSTEM,TIMER(500)
                       PROMPT('Data Path:'),AT(3,5),USE(?DataPath:Prompt)
                       ENTRY(@s255),AT(54,6,199,10),USE(DataPath)
                       BUTTON('Start'),AT(195,22,59),USE(?btnStart)
                       PROMPT('Monitor Period:'),AT(3,22),USE(?MonitorPeriod:Prompt)
                       ENTRY(@n_3),AT(54,23,33,10),USE(MonitorPeriod),RIGHT(1)
                       PROMPT('(secs)'),AT(91,22),USE(?MonitorPeriod:Prompt:2)
                     END

SI vsSystemUtilClass                                       ! ValuesSystemInformation (ABC Free)
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
        LocalPath = PATH()
        DataPath = GETINI('DEFAULTS','DataPath',,CLIP(LocalPath) & '\SBOMONITOR.INI')
        MonitorPeriod = GETINI('DEFAULTS','MonitorPeriod',5,CLIP(LocalPath) & '\SBOMONITOR.INI')
        ComputerName = SI.GetComputerName()    
  GlobalErrors.SetProcedureName('Main')
               
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?DataPath:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.Open(QuickWindow)                                   ! Open window
    0{PROP:Timer} = 0  
  Do DefineListboxStyle
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Main',QuickWindow)                         ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.Opened
    INIMgr.Update('Main',QuickWindow)                      ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?btnStart
      ThisWindow.Update
        IF (DataPath <> '' AND MonitorPeriod <> '')
            PUTINI('DEFAULTS','DataPath',DataPath,CLIP(LocalPath) & '\SBOMONITOR.INI')
            PUTINI('DEFAULTS','MonitorPeriod',MonitorPeriod,CLIP(LocalPath) & '\SBOMONITOR.INI')
      
            
            0{PROP:Timer} = MonitorPeriod * 100      
            ?DataPath{PROP:Disable} = 1
            ?MonitorPeriod{PROP:Disable} = 1
            ?btnStart{PROP:Disable} = 1
            ?btnStart{PROP:Text} = 'Running..'
            DISPLAY()
        END ! IF
        
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:Timer
        IF (DataPath <> '')
            SETPATH(DataPath)
            !AddToLog('Timer','')
            IF (Relate:CITYSERV.Open())
                AddToLog('OpenFile',ERRORCODE() & ' - ' & ERROR() & ' - ' & FILEERROR())
            ELSE
                Access:CITYSERV.ClearKey(cit:ServiceKey)
                cit:Service = 'X'
                IF (Access:CITYSERV.TryFetch(cit:ServiceKey) = Level:Benign)
                    IF (Access:CITYSERV.DeleteRecord(0))
                        AddToLog('DeleteRecord',ERRORCODE() & ' - ' & ERROR() & ' - ' & FILEERROR())
                    END ! IF
                ELSE ! IF
                    !IF (Access:CITYSERV.PrimeRecord() = Level:Benign)
                        Access:CITYSERV.PrimeRecord()
                        cit:Service = 'X'
                        cit:Description = 'TEST RECORD'
                        IF (Access:CITYSERV.TryInsert())
                            AddToLog('Insert',ERRORCODE() & ' - ' & ERROR() & ' - ' & FILEERROR())
                            Access:CITYSERV.CancelAutoInc()
                        END ! IF
      !                    ELSE ! IF
      !                        AddToLog('PrimeRecord',ERRORCODE() & ' - ' & ERROR() & ' - ' & FILEERROR())
      !                    END ! IF
                END ! IF   
                Relate:CITYSERV.Close()
            END ! IF
        END ! IF   
      
        
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

