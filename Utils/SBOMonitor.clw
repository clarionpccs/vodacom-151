   PROGRAM


CapesoftMessageBox:TemplateVersion equate('2.15')
NetTalk:TemplateVersion equate('4.57')
ActivateNetTalk   EQUATE(1)
  include('NetCrit.inc'),once
  include('NetMap.inc'),once
  include('NetAll.inc'),once
  include('NetTalk.inc'),once
  include('NetSimp.inc'),once
  include('NetWww.inc'),once
  include('NetWeb.inc'),once
  include('NetEmail.inc'),once
  include('NetFile.Inc'),once

   INCLUDE('ABERROR.INC'),ONCE
   INCLUDE('ABFILE.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ERRORS.CLW'),ONCE
   INCLUDE('KEYCODES.CLW'),ONCE
   INCLUDE('ABFUZZY.INC'),ONCE

!* * * * Line Print Template Generated Code * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

OFSTRUCT    GROUP,TYPE                                                                  
cBytes       BYTE                                                                       ! Specifies the length, in bytes, of the structure
cFixedDisk   BYTE                                                                       ! Specifies whether the file is on a hard (fixed) disk. This member is nonzero if the file is on a hard disk
nErrCode     SIGNED                                                                     ! Specifies the MS-DOS error code if the OpenFile function failed
Reserved1    SIGNED                                                                     ! Reserved, Don't use
Reserved2    SIGNED                                                                     ! Reserved, Don't use
szPathName   BYTE,DIM(128)                                                              ! Specifies the path and filename of the file
            END

LF                  CSTRING(CHR(10))                                                    ! Line Feed
FF                  CSTRING(CHR(12))                                                    ! Form Feed
CR                  CSTRING(CHR(13))                                                    ! Carriage Return

lpszFilename        CSTRING(144)                                                        ! File Name
fnAttribute         SIGNED                                                              ! Attribute
hf                  SIGNED                                                              ! File Handle
hpvBuffer           STRING(500)                                                        ! String To Write
cbBuffer            SIGNED                                                              ! Characters to Write
BytesWritten        SIGNED                                                              ! Characters Written

Succeeded           EQUATE(0)                                                           ! Function Succeeded
OpenError           EQUATE(1)                                                           ! Can Not Open File or Device
WriteError          EQUATE(2)                                                           ! Can not Write to File or Device
CloseError          EQUATE(3)                                                           ! Can not Close File or Device
SeekError           EQUATE(4)                                                           ! Can not Seek File or Device

ATTR_Normal         EQUATE(0)                                                           ! File Attribute Normal
ATTR_ReadOnly       EQUATE(1)                                                           ! File Attribute Read Only
ATTR_Hidden         EQUATE(2)                                                           ! File Attribute Hidden
ATTR_System         EQUATE(3)                                                           ! File Attribute System

OF_READ             EQUATE(0000h)                                                       ! Opens the file for reading only
OF_WRITE            EQUATE(0001h)                                                       ! Opens the file for writing only
OF_READWRITE        EQUATE(0002h)                                                       ! Opens the file for reading and writing
OF_SHARE_COMPAT     EQUATE(0000h)                                                       ! Opens the file with compatibility mode, allowing any program on a given machine to open the file any number of times.
OF_SHARE_EXCLUSIVE  EQUATE(0010h)                                                       ! Opens the file with exclusive mode, denying other programs both read and write access to the file
OF_SHARE_DENY_WRITE EQUATE(0020h)                                                       ! Opens the file and denies other programs write access to the file
OF_SHARE_DENY_READ  EQUATE(0030h)                                                       ! Opens the file and denies other programs read access to the file
OF_SHARE_DENY_NONE  EQUATE(0040h)                                                       ! Opens the file without denying other programs read or write access to the file
OF_PARSE            EQUATE(0100h)                                                       ! Fills the OFSTRUCT structure but carries out no other action
OF_DELETE           EQUATE(0200h)                                                       ! Deletes the file
OF_VERIFY           EQUATE(0400h)                                                       ! Compares the time and date in the OF_STRUCT with the time and date of the specified file
OF_SEARCH           EQUATE(0400h)                                                       ! Windows searches in directories even when the file name includes a full path
OF_CANCEL           EQUATE(0800h)                                                       ! Adds a Cancel button to the OF_PROMPT dialog box. Pressing the Cancel button directs OpenFile to return a file-not-found error message
OF_CREATE           EQUATE(1000h)                                                       ! Creates a new file. If the file already exists, it is truncated to zero length
OF_PROMPT           EQUATE(2000h)                                                       ! Displays a dialog box if the requested file does not exist.
OF_EXIST            EQUATE(4000h)                                                       ! Opens the file, and then closes it. This value is used to test for file existence
OF_REOPEN           EQUATE(8000h)                                                       ! Opens the file using information in the reopen buffer

OF_STRUCT           LIKE(OFSTRUCT)                                                      ! Will Be loaded with File information. See OFSTRUCT above

!* * * * Line Print Template Generated Code (End) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

  include('cwsynchc.inc'),once  ! added by NetTalk

   MAP
     MODULE('SBOMONITOR_BC.CLW')
DctInit     PROCEDURE                                      ! Initializes the dictionary definition module
DctKill     PROCEDURE                                      ! Kills the dictionary definition module
     END
!--- Application Global and Exported Procedure Definitions --------------------------------------------
     MODULE('SBOMONITOR001.CLW')
Main                   PROCEDURE   !Window
     END
     MODULE('SBOMONITOR004.CLW')
ds_Stop                PROCEDURE(<string StopText>)   !
     END
     MODULE('SBOMONITOR005.CLW')
ds_Halt                PROCEDURE(UNSIGNED Level=0,<STRING HaltText>)   !
     END
     MODULE('SBOMONITOR006.CLW')
ds_Message             FUNCTION(STRING MessageTxt,<STRING HeadingTxt>,<STRING IconSent>,<STRING ButtonsPar>,UNSIGNED MsgDefaults=0,BOOL StylePar=FALSE),UNSIGNED,PROC   !
     END
     
     !* * * * Line Print Template Generated Code (Start) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
     
          LinePrint(STRING StringToPrint,<STRING DeviceName>,<BYTE CRLF>),BYTE,PROC          ! Declare LinePrint Function
          DeleteFile(STRING FileToDelete),BYTE,PROC                                          ! Declare DeleteFile Function
            MODULE('')                                                                       ! MODULE Start
             OpenFile(*CSTRING,*OFSTRUCT,SIGNED),SIGNED,PASCAL,RAW                           ! Open/Create/Delete File
             _llseek(SIGNED,LONG,SIGNED),LONG,PASCAL                                         ! Control File Pointer
             _lwrite(SIGNED,*STRING,UNSIGNED),UNSIGNED,PASCAL,RAW                           ! Write to File
             _lclose(SIGNED),SIGNED,PASCAL                                                   ! Close File
            END                                                                              ! Terminate Module
     
     !* * * * Line Print Template Generated Code (End) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
     
   END

glo:ErrorText        STRING(1000)
glo:owner            STRING(20)
glo:TimeModify       TIME
glo:Notes_Global     STRING(1000)
glo:Q_Invoice        QUEUE,PRE(glo)
Invoice_Number         REAL
Account_Number         STRING(15)
                     END
glo:afe_path         STRING(255)
glo:DateModify       DATE
glo:Password         STRING(20)
glo:PassAccount      STRING(30)
glo:Preview          STRING('True')
glo:q_JobNumber      QUEUE,PRE(GLO)
Job_Number_Pointer     REAL
                     END
glo:Q_PartsOrder     QUEUE,PRE(GLO)
Q_Order_Number         REAL
Q_Part_Number          STRING(30)
Q_Description          STRING(30)
Q_Supplier             STRING(30)
Q_Quantity             LONG
Q_Purchase_Cost        REAL
Q_Sale_Cost            REAL
                     END
glo:G_Select1        GROUP,PRE(GLO)
Select1                STRING(30)
Select2                STRING(40)
Select3                STRING(40)
Select4                STRING(40)
Select5                STRING(40)
Select6                STRING(40)
Select7                STRING(40)
Select8                STRING(40)
Select9                STRING(40)
Select10               STRING(40)
Select11               STRING(40)
Select12               STRING(40)
Select13               STRING(40)
Select14               STRING(40)
Select15               STRING(40)
Select16               STRING(40)
Select17               STRING(40)
Select18               STRING(40)
Select19               STRING(40)
Select20               STRING(40)
Select21               STRING(40)
Select22               STRING(40)
Select23               STRING(40)
Select24               STRING(40)
Select25               STRING(40)
Select26               STRING(40)
Select27               STRING(40)
Select28               STRING(40)
Select29               STRING(40)
Select30               STRING(40)
Select31               STRING(40)
Select32               STRING(40)
Select33               STRING(40)
Select34               STRING(40)
Select35               STRING(40)
Select36               STRING(40)
Select37               STRING(40)
Select38               STRING(40)
Select39               STRING(40)
Select40               STRING(40)
Select41               STRING(40)
Select42               STRING(40)
Select43               STRING(40)
Select44               STRING(40)
Select45               STRING(40)
Select46               STRING(40)
Select47               STRING(40)
Select48               STRING(40)
Select49               STRING(40)
Select50               STRING(40)
                     END
glo:q_RepairType     QUEUE,PRE(GLO)
Repair_Type_Pointer    STRING(30)
                     END
glo:Q_UnitType       QUEUE,PRE(GLO)
Unit_Type_Pointer      STRING(30)
                     END
glo:Q_ChargeType     QUEUE,PRE(GLO)
Charge_Type_Pointer    STRING(30)
                     END
glo:Q_ModelNumber    QUEUE,PRE(GLO)
Model_Number_Pointer   STRING(30)
                     END
glo:Q_Accessory      QUEUE,PRE(GLO)
Accessory_Pointer      STRING(30)
                     END
glo:Q_AccessLevel    QUEUE,PRE(GLO)
Access_Level_Pointer   STRING(30)
                     END
glo:Q_AccessAreas    QUEUE,PRE(GLO)
Access_Areas_Pointer   STRING(30)
                     END
glo:Q_Notes          QUEUE,PRE(GLO)
notes_pointer          STRING(80)
                     END
glo:EnableFlag       STRING(3)
glo:TimeLogged       TIME
glo:GLO:ApplicationName STRING(8)
glo:GLO:ApplicationCreationDate LONG
glo:GLO:ApplicationCreationTime LONG
glo:GLO:ApplicationChangeDate LONG
glo:GLO:ApplicationChangeTime LONG
glo:GLO:Compiled32   BYTE
glo:GLO:AppINIFile   STRING(255)
glo:Queue            QUEUE,PRE(GLO)
Pointer                STRING(40)
                     END
glo:Queue2           QUEUE,PRE(GLO)
Pointer2               STRING(40)
                     END
glo:Queue3           QUEUE,PRE(GLO)
Pointer3               STRING(40)
                     END
glo:Queue4           QUEUE,PRE(GLO)
Pointer4               STRING(40)
                     END
glo:Queue5           QUEUE,PRE(GLO)
Pointer5               STRING(40)
                     END
glo:Queue6           QUEUE,PRE(GLO)
Pointer6               STRING(40)
                     END
glo:Queue7           QUEUE,PRE(GLO)
Pointer7               STRING(40)
                     END
glo:Queue8           QUEUE,PRE(GLO)
Pointer8               STRING(40)
                     END
glo:Queue9           QUEUE,PRE(GLO)
Pointer9               STRING(40)
                     END
glo:Queue10          QUEUE,PRE(GLO)
Pointer10              STRING(40)
                     END
glo:Queue11          QUEUE,PRE(GLO)
Pointer11              STRING(40)
                     END
glo:Queue12          QUEUE,PRE(GLO)
Pointer12              STRING(40)
                     END
glo:Queue13          QUEUE,PRE(GLO)
Pointer13              STRING(40)
                     END
glo:Queue14          QUEUE,PRE(GLO)
Pointer14              STRING(40)
                     END
glo:Queue15          QUEUE,PRE(GLO)
Pointer15              STRING(40)
                     END
glo:Queue16          QUEUE,PRE(GLO)
Pointer16              STRING(40)
                     END
glo:Queue17          QUEUE,PRE(GLO)
Pointer17              STRING(40)
                     END
glo:Queue18          QUEUE,PRE(GLO)
Pointer18              STRING(40)
                     END
glo:Queue19          QUEUE,PRE(GLO)
Pointer19              STRING(40)
                     END
glo:Queue20          QUEUE,PRE(GLO)
Pointer20              STRING(40)
                     END
glo:WebJob           BYTE(0)
glo:ARCAccountNumber STRING(30)
glo:ARCSiteLocation  STRING(30)
glo:FaultCodeGroup   GROUP,PRE(glo)
FaultCode1             STRING(30)
FaultCode2             STRING(30)
FaultCode3             STRING(30)
FaultCode4             STRING(30)
FaultCode5             STRING(30)
FaultCode6             STRING(30)
FaultCode7             STRING(30)
FaultCode8             STRING(30)
FaultCode9             STRING(30)
FaultCode10            STRING(255)
FaultCode11            STRING(255)
FaultCode12            STRING(255)
FaultCode13            STRING(30)
FaultCode14            STRING(30)
FaultCode15            STRING(30)
FaultCode16            STRING(30)
FaultCode17            STRING(30)
FaultCode18            STRING(30)
FaultCode19            STRING(30)
FaultCode20            STRING(30)
                     END
glo:IPAddress        STRING(30)
glo:HostName         STRING(60)
glo:ReportName       STRING(255)
glo:ExportReport     BYTE(0)
glo:EstimateReportName STRING(255)
glo:ReportCopies     BYTE
glo:Vetting          BYTE(0)
glo:TagFile          STRING(255)
glo:sbo_outfaultparts STRING(255)
glo:UserAccountNumber STRING(30)
glo:UserCode         STRING(3)
glo:Location         STRING(30)
glo:EDI_Reason       STRING(60)
glo:Insert_Global    STRING(3)
glo:sbo_outparts     STRING(255)
glo:StockReceiveTmp  STRING(255)
glo:SBO_GenericFile  STRING(255)
glo:RelocateStore    BYTE
glo:SBO_GenericTagFile STRING(255)
glo:SBO_DupCheck     STRING(255)
glo:EnableProcedureLogging LONG
SilentRunning        BYTE(0)                               ! Set true when application is running in 'silent mode'

!region File Declaration
CITYSERV             FILE,DRIVER('Btrieve'),OEM,NAME('CITYSERV.DAT'),PRE(cit),CREATE,BINDABLE,THREAD !City Link Services  
RefNumberKey             KEY(cit:RefNumber),NOCASE,PRIMARY !By Ref Number       
ServiceKey               KEY(cit:Service),NOCASE           !By Service          
Record                   RECORD,PRE()
RefNumber                   LONG                           !                    
Service                     STRING(1)                      !                    
Description                 STRING(30)                     !                    
                         END
                     END                       

!endregion

         include('MessageBox.inc'),once

ThisMessageBox       class(csEnhancedMessageClass) 
Init                     procedure  (long UseABCClasses=0,long UseDefaultFile=0)  ,virtual
PrimeLog                 procedure  (<string ExtraDetails>)  ,virtual
                      end
StartSearchEXEName          long(1)
EndSearchEXEName            long(0)
DataPath            CSTRING(256)
LocalPath           CSTRING(256)
MonitorPeriod       LONG
ComputerName        CSTRING(256)
Access:CITYSERV      &FileManager,THREAD                   ! FileManager for CITYSERV
Relate:CITYSERV      &RelationManager,THREAD               ! RelationManager for CITYSERV

FuzzyMatcher         FuzzyClass                            ! Global fuzzy matcher
GlobalErrorStatus    ErrorStatusClass,THREAD
GlobalErrors         ErrorClass                            ! Global error manager
INIMgr               INIClass                              ! Global non-volatile storage manager
GlobalRequest        BYTE(0),THREAD                        ! Set when a browse calls a form, to let it know action to perform
GlobalResponse       BYTE(0),THREAD                        ! Set to the response from the form
VCRRequest           LONG(0),THREAD                        ! Set to the request from the VCR buttons

Dictionary           CLASS,THREAD
Construct              PROCEDURE
Destruct               PROCEDURE
                     END


  CODE
  GlobalErrors.Init(GlobalErrorStatus)
  FuzzyMatcher.Init                                        ! Initilaize the browse 'fuzzy matcher'
  FuzzyMatcher.SetOption(MatchOption:NoCase, 1)            ! Configure case matching
  FuzzyMatcher.SetOption(MatchOption:WordOnly, 0)          ! Configure 'word only' matching
  INIMgr.Init('.\SBOMonitor.INI', NVD_INI)                 ! Configure INIManager to use INI file
  DctInit
                             ! Begin Generated by NetTalk Extension Template
    if ~command ('/netnolog') and (command ('/nettalklog') or command ('/nettalklogerrors') or command ('/neterrors') or command ('/netall'))
      NetDebugTrace ('[Nettalk Template] NetTalk Template version 4.57')
      NetDebugTrace ('[Nettalk Template] NetTalk Template using Clarion ' & sub (8000,1,1) & '.' & sub (8000,2,1))
      NetDebugTrace ('[Nettalk Template] NetTalk Object version ' & NETTALK:VERSION & ' in application ' & lower(clip(command(0))))
      NetDebugTrace ('[Nettalk Template] ABC Template Chain')
    end
                             ! End Generated by Extension Template
  SYSTEM{PROP:Icon} = 'clarion.ico'
                 !CapeSoft MessageBox init code
  ThisMessageBox.init(1,1)
                 !End of CapeSoft MessageBox init code
  Main
  INIMgr.Update
                             ! Begin Generated by NetTalk Extension Template
    NetCloseCallBackWindow() ! Tell NetTalk DLL to shutdown it's WinSock Call Back Window
  
    if ~command ('/netnolog') and (command ('/nettalklog') or command ('/nettalklogerrors') or command ('/neterrors') or command ('/netall'))
      NetDebugTrace ('[Nettalk Template] NetTalk Template version 4.57')
      NetDebugTrace ('[Nettalk Template] NetTalk Template using Clarion ' & sub (8000,1,1) & '.' & sub (8000,2,1))
      NetDebugTrace ('[Nettalk Template] Closing Down NetTalk (Object) version ' & NETTALK:VERSION & ' in application ' & lower(clip(command(0))))
    end
                             ! End Generated by Extension Template
      ThisMessageBox.Kill()                     !CapeSoft MessageBox template generated code
  INIMgr.Kill                                              ! Destroy INI manager
  FuzzyMatcher.Kill                                        ! Destroy fuzzy matcher
    

!* * * * Line Print Template Generated Code (Start) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

LinePrint FUNCTION(STRING StringToPrint,<STRING DeviceName>,<BYTE CRLF>)                ! LinePrint Function

   CODE                                                                                 ! Fuction Code Starts Here
    IF OMITTED(2)                                                                       ! If Device Name is Omitted
       IF SUB(PRINTER{07B29H},1,2) = '\\' 
         lpszFilename = SUB(PRINTER{07B29H},1,LEN(CLIP(PRINTER{07B29H})))               ! Use Default Device
       ELSE
         lpszFilename = SUB(PRINTER{07B29H},1,LEN(CLIP(PRINTER{07B29H})) - 1)           ! Use Default Device
       END 
    ELSE                                                                                ! Otherwise
       lpszFilename = CLIP(DeviceName)                                                  ! Use passed Device Name
    END                                                                                 ! Terminate IF

    IF (OMITTED(3) OR CRLF = True) AND CLIP(StringToPrint) <> FF                        ! If CRLF parameter is set or omitted and user did not pass FF
        hpvBuffer = StringToPrint & CR & LF                                             ! Print String with CR & LF
    ELSE                                                                                ! Otherwise
       hpvBuffer = StringToPrint                                                        ! Print Text As Is
    END                                                                                 ! Terminate IF

    cbBuffer = LEN(CLIP(hpvBuffer))                                                     ! Check Length of the Data to be Printed

     hf = OpenFile(lpszFilename,OF_STRUCT,OF_WRITE)                                     ! Open file and obtain file handle
     IF hf = -1                                                                         ! If File does not exist
      hf = OpenFile(lpszFilename,OF_STRUCT,OF_CREATE)                                   ! Create file and obtain file handle
      IF hf = -1 THEN RETURN(OpenError).                                                ! If Error then return OpenError
     END                                                                                ! Terminate IF

    IF SUB(lpszFilename,1,3) <> 'COM' AND |                                             ! If user prints to a file
       SUB(lpszFilename,1,3) <> 'LPT' AND |                                            
       SUB(lpszFilename,1,2) <> '\\'
       IF _llseek(hf,0,2) = -1 THEN RETURN(4).                                          ! Set file pointer to the end of the file (Append lines)
    END                                                                                 ! Terminate IF

    BytesWritten = _lwrite(hf,hpvBuffer,cbBuffer)                                       ! Write to the file
    IF BytesWritten < cbBuffer THEN RETURN(WriteError).                                 ! IF Writing to a device or file is not possible, return Write Error
    IF _lclose(hf) THEN RETURN(CloseError) ELSE RETURN(Succeeded).                      ! If error in closing device or a file, return CloseError otherwise return Succeeded


DeleteFile FUNCTION(STRING FileToDelete)                                                ! DeleteFile Function

    CODE                                                                                ! Function Code Starts Here
    lpszFilename = CLIP(FileToDelete)                                                   ! Put file name in the buffer
    hf = OpenFile(lpszFilename,OF_STRUCT,OF_DELETE)                                     ! and delete it
     IF hf = -1 THEN RETURN(OpenError) ELSE RETURN(Succeeded).                          ! If error, return OpenError otherwise Return Succeeded

!* * * * Line Print Template Generated Code (End) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
ThisMessageBox.Init                     procedure  (long UseABCClasses=0,long UseDefaultFile=0)   !CapeSoft MessageBox Object Procedure
TempVar         long,dim(2)
TMPLogFileName  string(252)

  Code
    parent.Init (UseABCClasses,UseDefaultFile)
    system{prop:MessageHook} = address(ds_Message)
    system{prop:StopHook} = address(ds_Stop)
    system{prop:HaltHook} = address(ds_Halt)
    self.StaticFeatures = Glo:Set_MsgLogging + Glo:Set_TimeOut
    self.SetGlobalSetting('TimeOut',200)
    self.SetGlobalSetting('ShowTimeOut',1)
    self.SetGlobalSetting('GPFHotKey', CtrlAltG)
    self.SetGlobalSetting('CopyKey', CtrlC)
    self.SetGlobalSetting('INIFile','CSMesBox.INI')
    self.SetGlobalSetting('INISection', 'CS_Messages')
    self.SetGlobalSetting('LogFileName','c:\MessageBox.Log')
    self.SetGlobalSetting('DateFormat', '@d17')

ThisMessageBox.PrimeLog                 procedure  (<string ExtraDetails>)   !CapeSoft MessageBox Object Procedure

  Code
              !CapeSoft MessageBox Template Generated code to prime the logging records
                 !Because there is no File selected, record priming is handled in the object and
                 !the default ASCII file is used to log the records.
    parent.PrimeLog (ExtraDetails)



Dictionary.Construct PROCEDURE

  CODE
  IF THREAD()<>1
     DctInit()
  END


Dictionary.Destruct PROCEDURE

  CODE
  DctKill()

