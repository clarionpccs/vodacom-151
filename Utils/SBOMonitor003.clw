

   MEMBER('SBOMonitor.clw')                                ! This is a MEMBER module

                     MAP
                       INCLUDE('SBOMONITOR003.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
AddToLog             PROCEDURE  (STRING pText, STRING pNotes) ! Declare Procedure

  CODE
        LinePrint('"' & FORMAT(TODAY(),@d06) & '",' & | 
            '"' & FORMAT(CLOCK(),@t04) & '",' & | 
            '"' & pText & '",' & | 
            ',' & | 
            '"' & ComputerName & '",' & |
            ',' & | 
            '"' & pNotes & '"',|
            LocalPath & '\' & FORMAT(TODAY(),@d012) & '.sblog')
