

   MEMBER('wipstatusdefaults.clw')                    ! This is a MEMBER module

                     MAP
                       INCLUDE('WIPST001.INC'),ONCE        !Local module procedure declarations
                     END


Main                 PROCEDURE                        ! Declare Procedure
ProgBar LONG
ProgressWindow WINDOW('Progress...'),AT(,,180,24),FONT('Tahoma',8,,FONT:regular),CENTER,WALLPAPER('sbback.jpg'), |
         TILED,GRAY,DOUBLE
       PROGRESS,USE(ProgBar),AT(4,4,172,14),RANGE(0,100)
     END



    MAP
PutiniEX   PROCEDURE(STRING fSection,STRING fEntry,STRING fValue,STRING fFile),LONG,PROC
    END
  CODE
! Processed Code
    BEEP(BEEP:SystemAsterisk)  ;  YIELD()
    CASE MESSAGE('Process to set the WIP Excluded Statuses for ALL Locations.'&|
        '|'&|
        '|Click ''OK'' to begin.','ServiceBase',|
                   ICON:Asterisk,'&OK|&Cancel',2) 
    OF 1 ! &OK Button
    OF 2 ! &Cancel Button
        RETURN
    END!CASE MESSAGE

    Relate:LOCATION.Open()
    Relate:WIPEXC.Open()
    Relate:STATUS.Open()
    Relate:TRADEACC.Open()

    OPEN(ProgressWindow)
    DISPLAY()
    ?ProgBar{prop:RangeHigh} = RECORDS(LOCATION)

    Access:LOCATION.ClearKey(loc:ActiveLocationKey)
    loc:Active = 1
    loc:Location = ''
    SET(loc:ActiveLocationKey,loc:ActiveLocationKey)
    LOOP UNTIL Access:LOCATION.Next() <> Level:Benign
        IF (loc:Active <> 1)
            BREAK
        END ! IF

        ?ProgBar{prop:Progress} = ?ProgBar{prop:Progress} + 1
        
        Access:TRADEACC.ClearKey(tra:SiteLocationKey)
        tra:SiteLocation = loc:Location
        IF (Access:TRADEACC.TryFetch(tra:SiteLocationKey) = Level:Benign)
            IF (tra:Stop_Account = 'YES')
                CYCLE
            END ! IF
        ELSE ! IF
            ! Not assigned to account?!
            CYCLE
        END ! IF
        
        ! Clear Exclusion List First
        Access:WIPEXC.ClearKey(wix:StatusTypeKey)
        wix:Location = loc:Location
        SET(wix:StatusTypeKey,wix:StatusTypeKey)
        LOOP UNTIL Access:WIPEXC.Next() <> Level:Benign
            IF (wix:Location <> loc:Location)
                BREAK
            END ! IF
            Access:WIPEXC.DeleteRecord(0)
        END ! LOOP
        
        putiniex(loc:Location,'StatusDays',180,clip(path())&'\WIPAudit.ini')
        
        Access:STATUS.ClearKey(sts:Ref_Number_Key)
        sts:Ref_Number = 0
        SET(sts:Ref_Number_Key,sts:Ref_Number_Key)
        LOOP UNTIL Access:STATUS.Next() <> Level:Benign
            IF (sts:Ref_Number <> 110 AND |
                sts:Ref_Number <> 305 AND |
                sts:Ref_Number <> 310 AND |
                sts:Ref_Number <> 315 AND |
                sts:Ref_Number <> 320 AND |
                sts:Ref_Number <> 330 AND |
                sts:Ref_Number <> 340 AND |
                sts:Ref_Number <> 345 AND |
                sts:Ref_Number <> 355 AND |
                sts:Ref_Number <> 450 AND |
                sts:Ref_Number <> 455 AND |
                sts:Ref_Number <> 457 AND |
                sts:Ref_Number <> 462 AND |
                sts:Ref_Number <> 463 AND |
                sts:Ref_Number <> 505 AND |
                sts:Ref_Number <> 510 AND |
                sts:Ref_Number <> 520 AND |
                sts:Ref_Number <> 535 AND |
                sts:Ref_Number <> 540 AND |
                sts:Ref_Number <> 605 AND |
                sts:Ref_Number <> 606 AND |
                sts:Ref_Number <> 610 AND |
                sts:Ref_Number <> 615 AND |
                sts:Ref_Number <> 620 AND |
                sts:Ref_Number <> 810 AND |
                sts:Ref_Number <> 811 AND |
                sts:Ref_Number <> 850 AND |
                sts:Ref_Number <> 916)
                
                IF (Access:WIPEXC.PrimeRecord() = Level:Benign)
                    wix:Location = loc:Location
                    wix:Status = sts:Status
                    IF (Access:WIPEXC.TryInsert())
                        Access:WIPEXC.CancelAutoInc()
                    END ! IF
                END ! IF
                
            END ! IF
                
        
        END ! LOOP
        
    END ! LOOP

    CLOSE(ProgressWindow)
    
    Relate:STATUS.Close()
    Relate:WIPEXC.Close()
    Relate:LOCATION.Close()
    Relate:TRADEACC.Close()

    BEEP(BEEP:SystemAsterisk)  ;  YIELD()
    CASE MESSAGE('Completed.','ServiceBase',|
                   ICON:Asterisk,'&OK',1) 
    OF 1 ! &OK Button
    END!CASE MESSAGE
    
PutiniEX   PROCEDURE(STRING fSection,STRING fEntry,STRING fValue,STRING fFile)        
retValue        BYTE(0)
Position        LONG
NameLength        LONG
TruncatedFile    STRING(100)
    CODE
    ! #13432 Naughty copy/paste, but this should be a one-off process (DBH: 16/01/2015)

    IF (fSection = '' OR fEntry = '')
        RETURN 0
    END
        
    retValue = 1
        
    Relate:INIDATA.Open()
    
    !now do the update for the ini file
    If fFile = ''
        !do the normal putini 
        Putini(fSection,fEntry,fValue)
        TruncatedFile = 'WIN'
    ELSE
        !do the normal putini 
        Putini(fSection,fEntry,fValue,fFile)
    
        !want to find the last '\' in the file name
        NameLength = len(clip(fFile))
        Position = NameLength
        Loop 
            Position -= 1
            If position = 0 then break.
            if fFile[Position] = '\' then break.
        END !Loop
        
        if Position = 0 then
            TruncatedFile = fFile
        ELSE            
            TruncatedFile = fFile[ Position+1 : NameLength-4 ]
        END !if Position = 0
    END !if ffile blank

    !does this one exist?
    access:IniData.clearkey(INI:KeyFileSectionEntry)
    INI:IniFile     = TruncatedFile
    INI:IniSection  = fSection
    INI:IniEntry    = fEntry
    If access:Inidata.fetch(INI:KeyFileSectionEntry) then

        !not found add it
        Access:IniData.primerecord()
        INI:IniFile    = TruncatedFile
        INI:IniSection = fSection
        INI:IniEntry   = fEntry
        INI:IniValue   = fValue
        Access:IniData.update()
        
    ELSE

        !If changed update it
        if clip(INI:IniValue) <> clip(fValue) then
            INI:IniValue = fValue
            Access:IniData.update()
        END !if changed

    END !if matching record not found
            
    Relate:INIDATA.CLOSE()
    
    RETURN retValue
