

   MEMBER('UpdateSMS.clw')                            ! This is a MEMBER module

                     MAP
                       INCLUDE('UPDAT001.INC'),ONCE        !Local module procedure declarations
                     END


Main                 PROCEDURE                        ! Declare Procedure
save_sms_id          USHORT,AUTO
pos                  STRING(255)
    MAP
AddToAudit PROCEDURE(LONG pJobNumber,STRING pType,STRING pAction,STRING pNotes)
    END ! MAP
  CODE
   Relate:SMSMAIL.Open
   Relate:AUDIT.Open
   Relate:CONTHIST.Open
    Save_sms_ID = Access:SMSMAIL.SaveFile()
    Access:SMSMAIL.Clearkey(sms:EmailSBUpdateKey)
    sms:EmailSent = 'T'
    sms:SBUpdated = 0
    Set(sms:EmailSBUpdateKey,sms:EmailSBUpdateKey)
    Loop ! Begin SMSMAIL Loop
        If Access:SMSMAIL.Next()
            Break
        End ! If !Access
        If sms:EmailSent <> 'T'
            Break
        End ! If
        If sms:SBUpdated <> 0
            Break
        End ! If

        If Access:CONTHIST.PrimeRecord() = Level:Benign
            cht:Ref_Number  = sms:RefNumber
            cht:Date        = sms:DateEmailSent
            cht:Time        = sms:TimeEmailSent
            cht:User        = '&SB'
            cht:Action      = 'EMAIL ALERT SENT'
            cht:Notes       = CLip(sms:EmailAddress) & '<13,10>' & Clip(sms:MSG)
            If Access:CONTHIST.TryInsert() = Level:Benign
                ! Insert Successful

            Else ! If Access:CONTHIST.TryInsert() = Level:Benign
                ! Insert Failed
                Access:CONTHIST.CancelAutoInc()
            End ! If Access:CONTHIST.TryInsert() = Level:Benign
        End !If Access:CONTHIST.PrimeRecord() = Level:Benign

        AddToAudit(sms:RefNumber,'JOB','EMAIL ALERT SENT',Clip(sms:EmailAddress))

        pos = Position(sms:EmailSBUpdateKey)
        sms:SBUpdated = 1
        If Access:SMSMAIL.TryUpdate() = Level:Benign
            Reset(sms:EmailSBUpdateKey,pos)
        End ! If Access:SMSMAIL.TryUpdate() = Level:Benign
    End ! End SMSMAIL Loop
    Access:SMSMAIL.RestoreFile(Save_sms_ID)

    Save_sms_ID = Access:SMSMAIL.SaveFile()
    Access:SMSMAIL.Clearkey(sms:SMSSBUpdateKey)
    sms:SMSSent = 'T'
    sms:SBUpdated = 0
    Set(sms:SMSSBUpdateKey,sms:SMSSBUpdateKey)
    Loop ! Begin SMSMAIL Loop
        If Access:SMSMAIL.Next()
            Break
        End ! If !Access
        If sms:SMSSent <> 'T'
            Break
        End ! If
        If sms:SBUpdated <> 0
            Break
        End ! If

        If Access:CONTHIST.PrimeRecord() = Level:Benign
            cht:Ref_Number  = sms:RefNumber
            cht:Date        = sms:DateSMSSent
            cht:Time        = sms:TimeSMSSent
            cht:User        = '&SB'
            cht:Action      = 'SMS ALERT SENT'
            cht:Notes       = CLip(sms:MSISDN) & '<13,10>' & Clip(sms:MSG)
            If Access:CONTHIST.TryInsert() = Level:Benign
                ! Insert Successful

            Else ! If Access:CONTHIST.TryInsert() = Level:Benign
                ! Insert Failed
                Access:CONTHIST.CancelAutoInc()
            End ! If Access:CONTHIST.TryInsert() = Level:Benign
        End !If Access:CONTHIST.PrimeRecord() = Level:Benign

        AddToAudit(sms:RefNumber,'JOB','SMS ALERT SENT',Clip(sms:MSISDN))

!        If Access:AUDIT.PrimeRecord() = Level:Benign
!            aud:Ref_Number  = sms:RefNumber
!            aud:Date        = sms:DateSMSSent
!            aud:Time        = sms:TimeSMSSent
!            aud:User        = '&SB'
!            aud:Action      = 'SMS ALERT SENT'
!            aud:Notes       = Clip(sms:MSISDN)
!            aud:Type        = 'JOB'
!            If Access:AUDIT.TryInsert() = Level:Benign
!                ! Insert Successful
!                pos = Position(sms:SMSSBUpdateKey)
!                sms:SBUpdated = 1
!                If Access:SMSMAIL.TryUpdate() = Level:Benign
!                    Reset(sms:SMSSBUpdateKey,pos)
!                End ! If Access:SMSMAIL.TryUpdate() = Level:Benign
!            Else ! If Access:AUDIT.TryInsert() = Level:Benign
!                ! Insert Failed
!                Access:AUDIT.CancelAutoInc()
!            End ! If Access:AUDIT.TryInsert() = Level:Benign
!        End !If Access:AUDIT.PrimeRecord() = Level:Benign
    End ! End SMSMAIL Loop
    Access:SMSMAIL.RestoreFile(Save_sms_ID)



   Relate:SMSMAIL.Close
   Relate:AUDIT.Close
   Relate:CONTHIST.Close
AddToAudit PROCEDURE(LONG pJobNumber,STRING pType,STRING pAction,STRING pNotes)
    CODE
        Relate:AUDIT.Open()
        Relate:AUDIT2.Open()
        Relate:AUDITE.Open()

        
        IF (Access:AUDIT.PrimeRecord() = Level:Benign)
            aud:Date        = TODAY()
            aud:Time        = CLOCK()
            aud:Ref_Number  = pJobNumber
            
            aud:User        = '&SB'
            
            aud:Action      = pAction
            aud:Type        = pType
            IF (Access:AUDIT.TryInsert() = Level:Benign)
                ! Insert Successful
                IF (Access:AUDITE.PrimeRecord() = Level:Benign)
                    aude:RefNumber  = aud:Record_Number
                    aude:IPAddress  = glo:IPAddress
                    aude:HostName   = glo:HostName
                    IF (Access:AUDITE.TryInsert())
                        Access:AUDITE.CancelAutoInc()
                    END ! IF
                END ! IF
            
                IF (Access:AUDIT2.PrimeRecord() = Level:Benign)
                    aud2:AUDRecordNumber    = aud:Record_Number
                    aud2:Notes              = pNotes
                    IF (Access:AUDIT2.TryInsert())
                        Access:AUDIT2.CancelAutoInc()
                    END ! IF
                END ! IF
                
            END ! IF
            
        END ! IF

        Relate:AUDIT.Open()
        Relate:AUDIT2.Open()
        Relate:AUDITE.Open()
