

   MEMBER('evobatch.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('EVOBA001.INC'),ONCE        !Local module procedure declarations
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

GRNQueue             QUEUE,PRE(GRQ)
GRN_RecordNo         LONG
GRN_OrderNumber      LONG
GRN_DespatchNote     STRING(20)
GRN_MinimumLineNo    LONG
GRN_DocumentAmount   DECIMAL(19,4)
                     END
OrderPartQueue       QUEUE,PRE(OPQ)
RecordNumber         LONG
OrderNumber          LONG
                     END
SB2KDEFINI           STRING(255)
EVO_CreateFiles      BYTE
EVO_Company_code     STRING(4)
EVO_XREF1_HD         STRING(240)
EVO_Tax_Code         STRING(2)
EVO_Tax_Code_Exempt  STRING(2)
EVO_Cost_Centre_Parts STRING(10)
EVO_Cost_Centre_3rdParty STRING(10)
EVO_Vendor_Line_No   STRING(10)
EVO_Tax_Line_No      STRING(10)
TempDespatchNoteNumber STRING(30)
TempMinLineNo        LONG
TempDocumentAmount   DECIMAL(19,4)
TempGRNSuffix        LONG
TempOrderNumber      STRING(30)
TempOrderedCurrency  STRING(30)
TempOrderedDailyRate REAL
QueueCount           LONG
OrderCount           LONG
ProcessCount         LONG
ErrorFound           BYTE
ErrorMessage         STRING(100)
ProcCall             STRING(20)
LocalFilename        STRING(500)
window               WINDOW('EvoBatch'),AT(0,0,100,44),FONT('Tahoma',8,,FONT:regular),COLOR(0A5E8F5H),TILED,GRAY,DOUBLE
                       BUTTON('Exit'),AT(16,8,65,16),USE(?ButtonExit),DEFAULT
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ButtonExit
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:EVO_AP.Open
  Relate:EVO_GL.Open
  Relate:EVO_HEAD.Open
  Relate:EVO_TAX.Open
  Relate:GRNOTES.Open
  Relate:JOBSE.Open
  Relate:VATCODE.Open
  Access:ORDERS.UseFile
  Access:ORDPARTS.UseFile
  Access:SUPPLIER.UseFile
  Access:TRDBATCH.UseFile
  Access:TRDPARTY.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Evo_Export('ALL','0','N')
  Post(event:closewindow)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EVO_AP.Close
    Relate:EVO_GL.Close
    Relate:EVO_HEAD.Close
    Relate:EVO_TAX.Close
    Relate:GRNOTES.Close
    Relate:JOBSE.Close
    Relate:VATCODE.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonExit
      ThisWindow.Update
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

