

   MEMBER('connect.clw')                              ! This is a MEMBER module

                     MAP
                       INCLUDE('CONNE004.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('CONNE005.INC'),ONCE        !Req'd for module callout resolution
                     END








PrintLabels PROCEDURE
RejectRecord         LONG,AUTO
LocalPrinter         STRING(100)
LocalJobType         STRING(20)
BarCode_JobNo        STRING(20)
BarCode_IMEI         STRING(20)
LocalLine            STRING(30),DIM(15)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
!-----------------------------------------------------------------------------
Process:View         VIEW(Labels)
                     END
Report               REPORT,AT(0,0,3000,2000),PAPER(PAPER:USER,3000,2000),PRE(RPT),FONT('Arial',10,,FONT:regular),THOUS
EndOfReportBreak       BREAK(EndOfReport)
Detail                   DETAIL,AT(146,,2750,1948),USE(?unnamed),FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           STRING(@s30),AT(52,0,2615,156),USE(Company),TRN,CENTER,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING(@s20),AT(1344,219,1302,156),USE(BarCode_JobNo),TRN,FONT('C128 High 12pt LJ3',14,,FONT:regular,CHARSET:ANSI)
                           STRING(@s10),AT(1344,375),USE(LocalLine[1]),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                           STRING('Customer'),AT(52,531),USE(?String8),TRN,FONT('Arial',7,,,CHARSET:ANSI)
                           STRING(@s20),AT(531,531),USE(LocalLine[4]),TRN,FONT(,7,,FONT:bold)
                           STRING('Company'),AT(52,667),USE(?String9),TRN,FONT(,7,,)
                           STRING(@s20),AT(531,667),USE(LocalLine[5]),TRN,FONT(,7,,FONT:bold)
                           STRING('Job Type'),AT(52,802),USE(?String10),TRN,FONT(,7,,)
                           STRING(@s20),AT(531,802),USE(LocalJobType),TRN,FONT(,7,,FONT:bold)
                           STRING('Location'),AT(52,917),USE(?String11),TRN,FONT(,7,,)
                           STRING(@s20),AT(531,917),USE(LocalLine[6]),TRN,FONT(,7,,FONT:bold)
                           STRING('Fault'),AT(52,1063),USE(?String12),TRN,FONT(,7,,)
                           TEXT,AT(521,1063,2135,260),USE(LocalLine[7]),FONT(,7,,FONT:bold)
                           STRING('Model'),AT(52,1313),USE(?String13),TRN,FONT(,7,,)
                           STRING(@s20),AT(531,1333),USE(LocalLine[8]),TRN,FONT(,7,,FONT:bold)
                           STRING('Make'),AT(52,1469),USE(?String14),TRN,FONT(,7,,)
                           STRING(@s10),AT(531,1469),USE(LocalLine[9]),TRN,FONT(,7,,FONT:bold)
                           STRING('ESN'),AT(1125,1469),USE(?String21),TRN,FONT(,7,,)
                           STRING(@s20),AT(52,1667),USE(BarCode_IMEI),TRN,CENTER,FONT('C128 High 12pt LJ3',14,,FONT:regular,CHARSET:ANSI)
                           STRING(@s16),AT(1344,1469),USE(LocalLine[10]),TRN,FONT(,7,,FONT:bold)
                           STRING(@s10),AT(531,375),USE(LocalLine[3]),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                           STRING('Booked'),AT(52,219),USE(?String5),TRN,FONT('Arial',7,,,CHARSET:ANSI)
                           STRING('Job'),AT(1125,219),USE(?String7),TRN,FONT('Arial',7,,,CHARSET:ANSI)
                           STRING(@s10),AT(531,219),USE(LocalLine[2]),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         END
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('PrintLabels')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  SETCURSOR
  PreviewReq = False
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:Labels.Open
  
  
  RecordsToProcess = BYTES(Labels)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  HIDE(?Progress:PctText)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  SEND(Labels,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      !Get Printer
      
      !Saves the old printer name
      LocalPrinter = Printer{PROPPRINT:Device}
      !replace with the label printer name
      !IF LabelPrinter <> '' THEN
      !  Printer{PROPPRINT:Device} = LabelPrinter
      !END !IF
      DO OpenReportRoutine
    OF Event:Timer
        !and this is where it starts
        set(Labels)
        x#=1
        Loop !for each label
            if access:Labels.next() then break.
            LocalLine[x#] = LAB:Textline
            x#+=1
            if x# = 15 then
                !Almost ready to print this label
                BarCode_JobNo = clip(LocalLine[1])
                BarCode_IMEI  = clip(LocalLine[10])
                SetBarCode(BarCode_JobNo)
                SetBarCode(BarCode_IMEI)
                if clip(LocalLine[11])='YES' and clip(LocalLine[12])='NO' then
                    LocalJobType  = 'WARRANTY'
                ELSE
                    LocalJobType  = 'CHARGEABLE'
                END
                print(RPT:Detail)
                x#=1
            END !I x# = 15
        END !
        
        LocalResponse = Requestcompleted
        Break
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(Labels,'QUICKSCAN=off').
  IF (ReportWasOpened AND Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
     (ReportWasOpened AND (AsciiOutputReq OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
    ENDPAGE(Report)
    IF ~RECORDS(PrintPreviewQueue)
        OMIT('**End Omit Nothing Message**')
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
        !**End Omit Nothing Message**
    ELSE
      IF CPCSPgOfPgOption = True
        AssgnPgOfPg(PrintPreviewQueue)
      END
      PRINTER{PROPPRINT:Copies} = Report{PROPPRINT:Copies}
      HandleCopies(PrintPreviewQueue,Report{PROPPRINT:Copies})
      Report{PROP:FlushPreview} = True
    END
  ELSIF ~ReportWasOpened
        OMIT('**End Omit Nothing Message**')
    MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
        !**End Omit Nothing Message**
  END
  CLOSE(Report)
  !reset to the previous printer
  Printer{PROPPRINT:Device} = LocalPrinter
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:Labels.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  IF ~ReportWasOpened
    OPEN(Report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> Report{PROPPRINT:Copies}
    Report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = Report{PROPPRINT:Copies}
  IF Report{PROPPRINT:COLLATE}=True
    Report{PROPPRINT:Copies} = 1
  END
  IF Report{PROP:TEXT}=''
    Report{PROP:TEXT}='PrintLabels'
  END
  IF (Report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR Report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    Report{Prop:Preview} = PrintPreviewImage
  END







