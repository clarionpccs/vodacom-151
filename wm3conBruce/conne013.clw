

   MEMBER('connect.clw')                              ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CONNE013.INC'),ONCE        !Local module procedure declarations
                     END


Missive PROCEDURE (SentMessage,SentTitle,SentIcon,SentButtons) !Generated from procedure template - Window

MissiveText          STRING(255)
IconName             STRING(20)
FullButtonText       STRING(60)
ButtonText           STRING(10),DIM(6)
ButtonCount          SHORT
ButtonBreak          SHORT
RetValue             BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Caption'),AT(,,408,95),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6E7EFH),CENTER,GRAY,DOUBLE
                       PANEL,AT(4,1,400,11),USE(?Panel3),FILL(09A6A7CH)
                       STRING('this string is changed on entry to the procedure - it is known as the stringtitl' &|
   'e.'),AT(12,2),USE(?StringTitle),TRN,FONT('Tahoma',,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(4,13,400,52),USE(?Panel1),FILL(09A6A7CH)
                       IMAGE,AT(12,15,48,44),USE(?Image1)
                       TEXT,AT(68,16,332,46),USE(MissiveText),SKIP,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),READONLY
                       PANEL,AT(4,69,400,24),USE(?Panel2),FILL(09A6A7CH)
                       BUTTON('Button 6'),AT(20,69,63,22),USE(?Button6),TRN,FLAT,FONT('Tahoma',10,COLOR:White,FONT:bold,CHARSET:ANSI)
                       BUTTON('Button 5'),AT(84,69,63,22),USE(?Button5),TRN,FLAT,FONT('Tahoma',10,COLOR:White,FONT:bold,CHARSET:ANSI)
                       BUTTON('Button 4'),AT(148,69,63,22),USE(?Button4),TRN,FLAT,FONT('Tahoma',10,COLOR:White,FONT:bold,CHARSET:ANSI)
                       BUTTON('Button 3'),AT(212,69,63,22),USE(?Button3),TRN,FLAT,FONT('Tahoma',10,COLOR:White,FONT:bold,CHARSET:ANSI)
                       BUTTON('Button 2'),AT(276,69,63,22),USE(?Button2),TRN,FLAT,FONT('Tahoma',10,COLOR:White,FONT:bold,CHARSET:ANSI)
                       BUTTON('Button 1'),AT(340,69,63,22),USE(?Button1),TRN,FLAT,FONT('Tahoma',10,COLOR:White,FONT:bold,CHARSET:ANSI)
                       IMAGE('MPurp.jpg'),AT(20,70,63,22),USE(?ButtonImage6)
                       IMAGE('MPurp.jpg'),AT(84,70,63,22),USE(?ButtonImage5)
                       IMAGE('MPurp.jpg'),AT(148,70,63,22),USE(?ButtonImage4)
                       IMAGE('MPurp.jpg'),AT(212,70,63,22),USE(?ButtonImage3)
                       IMAGE('MPurp.jpg'),AT(276,70,63,22),USE(?ButtonImage2)
                       IMAGE('MPurp.jpg'),AT(340,70,63,22),USE(?ButtonImage1)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(RetValue)


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Missive')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Panel3
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  !set up the window from sent variables
  ?StringTitle{Prop:Text}   = SentTitle
  IconName       = SentIcon
  if IconName    = '' then IconName = 'MBlank.jpg'.
  FullButtonText = SentButtons
  
  !sort out the Missive Text
  y# = 1
  loop x# = 1 to len(clip(SentMessage))
      if SentMessage[x#] = '|' then
          MissiveText[y#] = chr(13)
          y#+=1
          MissiveText[y#] = chr(10)
      ELSE
          MissiveText[y#]=SentMessage[x#]
      END
      y#+=1
  END !Loop through sent message
  
  if x# = y# then
      !there were no line breaks if they match
      !Add one to the start
      missiveText=chr(13)&chr(10)&MissiveText
  END !if x = y
  
  
  !Sort out the button texts
  ButtonCount = 0
  loop
      !Look for the divider
      ButtonBreak = instring('|',FullButtonText,1,1)
      if ButtonBreak = 0 then
          !No more buttons - just the last one to do
          ButtonCount += 1
          if ButtonCount > 6 then break.   !Ignore more than six buttons
          ButtonText[ButtonCount]=Clip(FullButtonText)
          Break  !From the loop
      ELSE
          !this button and another exists
          ButtonCount +=1
          if ButtonCount > 6 then break.   !Ignore more than six buttons
          ButtonText[ButtonCount] = FullButtonText[1:ButtonBreak-1]
          FullButtonText = FullButtonText[ButtonBreak+1:len(clip(FullButtonText))]
      END
  End!Loop through button text
  
  
  
  !Sort out the buttons
  !there must always be one!
  Case buttonText[1,1]
      of '/'
          ?ButtonImage1{prop:text} = 'Mgreen.jpg'
          ?Button1{Prop:text}=ButtonText[1,2:len(clip(ButtonText[1]))]
      of '\'
          ?ButtonImage1{prop:text} = 'Mred.jpg'
          ?Button1{Prop:text}=ButtonText[1,2:len(clip(ButtonText[1]))]
      else
          ?Button1{Prop:text}=ButtonText[1]
  END !Case
  
  if clip(buttonText[2]) = '' then
      hide(?Button2)
      ?ButtonImage2{prop:Text}='Blank.jpg'
  ELSE
      Case buttonText[2,1]
          of '/'
              ?ButtonImage2{prop:text} = 'Mgreen.jpg'
              ?Button2{Prop:text}=ButtonText[2,2:len(clip(ButtonText[2]))]
          of '\'
              ?ButtonImage2{prop:text} = 'Mred.jpg'
              ?Button2{Prop:text}=ButtonText[2,2:len(clip(ButtonText[2]))]
          else
              ?Button2{Prop:text}=ButtonText[2]
      END !Case
  END
  
  if clip(buttonText[3]) = '' then
      hide(?Button3)
      ?ButtonImage3{prop:text}='Blank.jpg'
  ELSE
      Case buttonText[3,1]
          of '/'
              ?ButtonImage3{prop:text} = 'Mgreen.jpg'
              ?Button3{Prop:text}=ButtonText[3,2:len(clip(ButtonText[3]))]
          of '\'
              ?ButtonImage3{prop:text} = 'Mred.jpg'
              ?Button3{Prop:text}=ButtonText[3,2:len(clip(ButtonText[3]))]
          else
              ?Button3{Prop:text}=ButtonText[3]
      END !Case
  END
  
  if clip(buttonText[4]) = '' then
      hide(?Button4)
      ?ButtonImage4{prop:text}='Blank.jpg'
  ELSE
      Case buttonText[4,1]
          of '/'
              ?ButtonImage4{prop:text} = 'Mgreen.jpg'
              ?Button4{Prop:text}=ButtonText[4,2:len(clip(ButtonText[4]))]
          of '\'
              ?ButtonImage4{prop:text} = 'Mred.jpg'
              ?Button4{Prop:text}=ButtonText[4,2:len(clip(ButtonText[4]))]
          else
              ?Button4{Prop:text}=ButtonText[4]
      END !Case
  END
  
  if clip(buttonText[5]) = '' then
      hide(?Button5)
      ?ButtonImage5{prop:text}='blank.jpg'
  ELSE
      Case buttonText[5,1]
          of '/'
              ?ButtonImage5{prop:text} = 'Mgreen.jpg'
              ?Button5{Prop:text}=ButtonText[5,2:len(clip(ButtonText[5]))]
          of '\'
              ?ButtonImage5{prop:text} = 'Mred.jpg'
              ?Button5{Prop:text}=ButtonText[5,2:len(clip(ButtonText[5]))]
          else
              ?Button5{Prop:text}=ButtonText[5]
      END !Case
  END
  
  if clip(buttonText[6]) = '' then
      hide(?Button6)
      ?ButtonImage6{prop:text}='Blank.jpg'
  ELSE
      Case buttonText[6,1]
          of '/'
              ?ButtonImage6{prop:text} = 'Mgreen.jpg'
              ?Button6{Prop:text}=ButtonText[6,2:len(clip(ButtonText[6]))]
          of '\'
              ?ButtonImage6{prop:text} = 'Mred.jpg'
              ?Button6{Prop:text}=ButtonText[6,2:len(clip(ButtonText[6]))]
          else
              ?Button6{Prop:text}=ButtonText[6]
      END !Case
  
  END
  ?image1{prop:text}= IconName
  select(?Button1)
  thiswindow.update()
  display()
  Bryan.CompFieldColour()
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button1
      RetValue = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button6
      ThisWindow.Update
      RetValue = 6
       POST(Event:CloseWindow)
    OF ?Button5
      ThisWindow.Update
      RetValue = 5
       POST(Event:CloseWindow)
    OF ?Button4
      ThisWindow.Update
      RetValue = 4
       POST(Event:CloseWindow)
    OF ?Button3
      ThisWindow.Update
      RetValue = 3
       POST(Event:CloseWindow)
    OF ?Button2
      ThisWindow.Update
      RetValue = 2
       POST(Event:CloseWindow)
    OF ?Button1
      ThisWindow.Update
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
