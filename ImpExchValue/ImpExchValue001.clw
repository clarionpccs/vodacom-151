

   MEMBER('ImpExchValue.clw')                              ! This is a MEMBER module

                     MAP
                       INCLUDE('IMPEXCHVALUE001.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Main                 PROCEDURE                             ! Declare Procedure
myProg PW ! ProgressBarLocal(BryanTemplate)

myProg:ProgressWindow      WINDOW('Progress...'),AT(,,210,64),CENTER,FONT('Tahoma', 8,, FONT:regular, CHARSET:ANSI),GRAY,TIMER(1),DOUBLE
                        PROGRESS, AT(17,15,175,12), USE(?myProg:ProgressThermometer), RANGE(0,100),SMOOTH
                        STRING(''), AT(0,3,211,10), USE(?myProg:UserString), CENTER
                        STRING(''), AT(0,30,208,10), USE(?myProg:PercentString), CENTER
                        BUTTON('Cancel'), AT(80,44,50,15), USE(?myProg:ProgressCancel)
                        END
                        
locManufacturer      STRING(30)                            !
locSavePath         CSTRING(255)
locImportFile       CSTRING(255),STATIC
ImportFile          File,DRIVER('BASIC'),PRE(impfil),Name(locImportFile),CREATE,BINDABLE,THREAD
RECORD                  RECORD
ModelNumber                 STRING(30)
SellingPrice                REAL()
                        END
                    END
locExportFile       CSTRING(255),STATIC
ExportFile          File,DRIVER('BASIC'),PRE(expfil),Name(locExportFile),CREATE,BINDABLE,THREAD
RECORD                  RECORD
ModelNumber                 STRING(30)
SellingPrice                String(30)
ExceptionReason             STRING(255)
                        END
                    END

FilesOpened     BYTE(0)

  CODE
  myProg.PWProgressWindow &= myProg:ProgressWindow
  myProg.CancelButtonFEQ = ?myProg:ProgressCancel
  myProg.UserStringFEQ = ?myProg:UserString
  myProg.ThermometerFEQ = ?myProg:ProgressThermometer
  myProg.PercentStringFEQ = ?myProg:PercentString
  
  
    ! Command Line Check
    ! Manufacturer will be passed as the command line
    
    man# = INSTRING('!',COMMAND(),1,1)
    
    IF (man# = 0)
        Beep(Beep:SystemHand)  ;  Yield()
        Case Message('This exe cannot be run from here.','ServiceBase',|
            Icon:Hand,'&OK',1) 
        Of 1 ! &OK Button
        End!Case Message
        RETURN
    ELSE
        locManufacturer = CLIP(SUB(COMMAND(),man# + 1,30))
    END
    
    
    ! Processing
    locSavePath = Path()
    locImportFile = ''
    if (fileDialog('Choose File',locImportFile,'CSV Files|*.csv|All Files|*.*',file:KeepDir + file:NoError + File:LongName))
        !Found
        setPath(locSavePath)
        
    else ! if (fileDialog)
        !Error
        setPath(locSavePath)
        RETURN
    end ! if (fileDialog)
        
    ! Set the exceptions file, if needed
    locExportFile = BHAddBackSlash(GETINI('EXCEPTION','ExchangeRate',,CLIP(PATH()) & '\SB2KDEF.INI')) & BHGetFileNoExtension(locImportFile) & FORMAT(TODAY(),@D012) & FORMAT(CLOCK(),@t05) & '.csv'
    REMOVE(locExportFile)
        
    DO OpenFiles
        
    OPEN(ImportFile)
    IF ERROR()
        STOP(ERROR())
        RETURN
    END
        
    ! Count Records
    SETCURSOR(CURSOR:Wait)
    countRec# = 0
    SET(ImportFile,0)
    LOOP
        NEXT(ImportFile)
        IF ERROR()
            BREAK
        END
        countRec# += 1
    END
    SETCURSOR()
    
    exceptionFound# = 0
    firstLine# = 1
        
    myProg.INIT(countRec#)
    SET(ImportFile,0)
    LOOP
        NEXT(ImportFile)
        IF (ERROR())
            BREAK
        END
        IF (myProg.UpdateProgressBar())
            BREAK
        END
        
        ! Ignore First Line
        IF (firstLine# = 1)
            firstLine# = 0
            CYCLE
        END
            
        Access:MODELNUM.Clearkey(mod:Manufacturer_Key)
        mod:Manufacturer = locManufacturer
        mod:Model_Number = UPPER(impfil:ModelNumber)
        IF (Access:MODELNUM.Tryfetch(mod:Manufacturer_Key) = Level:Benign)
            mod:ExchReplaceValue = impfil:SellingPrice
            Access:MODELNUM.TryUpdate()
        ELSE
            IF (exceptionFound# = 0)
                CREATE(ExportFile)
                OPEN(ExportFile)
                exceptionFound# = 1
            END
            expfil:ModelNumber = impfil:ModelNumber
            expfil:SellingPrice = impfil:SellingPrice
            expfil:ExceptionReason = 'Model Number Not Found'
            ADD(ExportFile)
        END
    END
        
    IF (exceptionFound#)
        CLOSE(ExportFile)
        Beep(Beep:SystemHand)  ;  Yield()
        Case Message('Some errors have occurred. The exceptions can be viewed in the following file:||' & CLIP(locExportFile),'ServiceBase',|
            Icon:Hand,'&OK',1) 
        Of 1 ! &OK Button
        End!Case Message
    END
    
    CLOSE(ImportFile)
    
    DO CloseFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:MODELNUM.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:MODELNUM.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:MODELNUM.Close
     FilesOpened = False
  END
