

   MEMBER('vodr0087.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('VODR0001.INC'),ONCE        !Local module procedure declarations
                     END


RepeatIMEIReport PROCEDURE                            !Generated from procedure template - Window

tmp:Line             STRING(2000)
Progress:Thermometer BYTE
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:BouncerLine      STRING(2000)
tmp:VersionNumber    STRING(30)
tmp:ExcelFileName    STRING(255)
StatusQueue          QUEUE,PRE(staque)
StatusMessage        STRING(60)
                     END
MainGroup       Group,Pre(m)
JobNumber      Long()
BranchCode     String(2)
CustomerName   String(30)
Manufacturer   String(30)
ModelNumber    String(30)
IMEINumber     String(30)
NumberOfTimes  Long()
InFault        String(30)
OutFault       String(30)
OutIMEINumber   String(30)
DateBooked      Date
                End ! MainGroup       Group,Pre(m)

BouncerQueue    Queue,Pre(b)
JobNumber      Long()
BranchCode     String(2)
CustomerName   String(30)
Manufacturer   String(30)
ModelNumber    String(30)
IMEINumber     String(30)
NumberOfTimes  Long()
InFault        String(30)
OutFault       String(30)
OutIMEINumber   String(30)
DateBooked      Date
                End ! BouncerGroup    Group,Pre(b)
ReportedJobsQueue    Queue,Pre(q)
JobNumber           Long()
                End !Queue
Excel                SIGNED !OLE Automation holder
excel:ProgramName    CString(255)
excel:ActiveWorkBook CString(20)
excel:Selected       CString(20)
excel:FileName       CString(255)
loc:Version          Cstring(30)
window               WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(240,146,200,144),USE(?PanelMain),FILL(0D6EAEFH)
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,164,192,44),USE(?PanelTip),FILL(0D6EAEFH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(364,258),USE(?Close),TRN,FLAT,ICON('cancelp.jpg')
                       BUTTON,AT(296,258),USE(?OK),TRN,FLAT,ICON('okp.jpg')
                       BUTTON,AT(396,172,36,32),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Help.jpg')
                       STRING(@s255),AT(252,184,140,),USE(SRN:TipText),TRN
                       PANEL,AT(244,212,192,42),USE(?Panel55),FILL(09A6A7CH)
                       PROMPT('Start Date'),AT(248,216),USE(?tmp:StartDate:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@d6),AT(312,216,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Start Date'),TIP('Start Date'),UPR
                       BUTTON,AT(380,212),USE(?PopCalendar),TRN,FLAT,ICON('lookupp.jpg')
                       PROMPT('End Date'),AT(248,232),USE(?tmp:EndDate:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@d6),AT(312,232,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('End Date'),TIP('End Date'),UPR
                       PROMPT('Report Verison:'),AT(248,244),USE(?ReportVersion),FONT(,7,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       BUTTON,AT(380,228),USE(?PopCalendar:2),TRN,FLAT,ICON('lookupp.jpg')
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Repeat IMEI Report'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       GROUP('Top Tip'),AT(248,168,184,36),USE(?GroupTip),BOXED,TRN
                       END
                     END

!Progress Window (DBH: 22-03-2004)
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progresswindow WINDOW('Progress...'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH), |
         CENTER,IMM,ICON('cellular3g.ico'),WALLPAPER('sbback.jpg'),TILED,TIMER(1),GRAY,DOUBLE
       PANEL,AT(160,64,360,300),USE(?Panel5),FILL(0D6E7EFH)
       PANEL,AT(164,68,352,12),USE(?Panel1),FILL(09A6A7CH)
       LIST,AT(208,88,268,206),USE(?List1),VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH), |
           FORMAT('20L(2)|M'),FROM(StatusQueue),GRID(COLOR:White)
       PANEL,AT(164,84,352,246),USE(?Panel4),FILL(09A6A7CH)
       PROGRESS,USE(progress:thermometer),AT(206,314,268,12),RANGE(0,100)
       STRING(''),AT(259,300,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,080FFFFH,FONT:bold), |
           COLOR(09A6A7CH)
       STRING(''),AT(232,136,161,10),USE(?progress:pcttext),TRN,HIDE,CENTER,FONT('Arial',8,,)
       PANEL,AT(164,332,352,28),USE(?Panel2),FILL(09A6A7CH)
       PROMPT('Report Progress'),AT(168,70),USE(?WindowTitle2),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
       BUTTON,AT(444,332),USE(?ProgressCancel),TRN,FLAT,LEFT,ICON('Cancelp.jpg')
       BUTTON,AT(376,332),USE(?Button:OpenReportFolder),TRN,FLAT,HIDE,ICON('openrepp.jpg')
       BUTTON,AT(444,332),USE(?Finish),TRN,FLAT,HIDE,ICON('Finishp.jpg')
     END
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
E1                   Class(oiExcel)
Init                   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0),byte,proc,virtual,name('INIT@F17OIEXCEL')
Kill                   PROCEDURE (byte pUnloadCOM=1),byte,proc,virtual,name('KILL@F17OIEXCEL')
TakeEvent              PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0),virtual,name('TAKEEVENT@F17OIEXCEL')
                     End

local       Class
UpdateProgressWindow        Procedure(String func:Text)
            End ! local       Class
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
    Map
ExcelSetup          Procedure(Byte      func:Visible)

ExcelMakeWorkBook   Procedure(String    func:Title,String   func:Author,String  func:AppName)

ExcelMakeSheet      Procedure()

ExcelSheetType      Procedure(String    func:Type)

ExcelHorizontal     Procedure(String    func:Direction)

ExcelVertical        Procedure(String    func:Direction)

ExcelCell   Procedure(String    func:Text,Byte  func:Bold)

ExcelFormatCell     Procedure(String    func:Format)

ExcelFormatRange    Procedure(String    func:Range,String   func:Format)

ExcelNewLine    Procedure(Long  func:Number)

ExcelMoveDown   Procedure()

ExcelColumnWidth        Procedure(String    func:Range,Long   func:Width)

ExcelCellWidth          Procedure(Long  func:Width)

ExcelAutoFit            Procedure(String    func:Range)

ExcelGrayBox            Procedure(String    func:Range)

ExcelGrid   Procedure(String    func:Range,Byte  func:Left,Byte  func:Top,Byte   func:Right,Byte func:Bottom,Byte func:Colour)

ExcelSelectRange        Procedure(String    func:Range)

ExcelFontSize           Procedure(Byte  func:Size)

ExcelSheetName          Procedure(String    func:Name)

ExcelSelectSheet    Procedure(String    func:SheetName)

ExcelAutoFilter         Procedure(String    func:Range)

ExcelDropAllSheets      Procedure()

ExcelDeleteSheet        Procedure(String    func:SheetName)

ExcelClose              Procedure()

ExcelSaveWorkBook       Procedure(String    func:Name)

ExcelFontColour         Procedure(String    func:Range,Long func:Colour)

ExcelWrapText           Procedure(String    func:Range,Byte func:True)

ExcelGetFilename        Procedure(Byte      func:DontAsk),Byte

ExcelGetDirectory       Procedure(),Byte

ExcelCurrentColumn      Procedure(),String

ExcelCurrentRow         Procedure(),String

ExcelPasteSpecial       Procedure(String    func:Range)

ExcelConvertFormula     Procedure(String    func:Formula),String

ExcelColumnLetter Procedure(Long  func:ColumnNumber),String

ExcelOpenDoc            Procedure(String    func:FileName)

ExcelFreeze             Procedure(String    func:Cell)
    End

  CODE
  GlobalResponse = ThisWindow.Run()

Reporting       Routine
Data
local:Desktop       Cstring(255)
tmp:LineNumber          Long()
local:ReportStartDate       Date()
local:ReportStartTime       Time()
Code
    excel:ProgramName = 'Repeat IMEI Report'
    ! Create Folder In My Documents
    SHGetSpecialFolderPath( GetDesktopWindow(), local:Desktop, 5, FALSE )
    local:Desktop = Clip(local:Desktop) & '\ServiceBase Export'

    !Does the Export Folder already Exists?
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    local:Desktop = Clip(local:Desktop) & '\' & Clip(excel:ProgramName)
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))


    excel:FileName = Clip(local:Desktop) & '\' & Clip(excel:ProgramName) & ' ' & Format(Today(),@d12)

    If Exists(excel:FileName & '.xls')
        Remove(excel:FileName & '.xls')
        If Error()
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('Cannot get access to the selected document:'&|
                '|' & Clip(excel:FileName) & ''&|
                '|'&|
                '|Ensure the file is not in use and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If Error()
    End !If Exists(excel:FileName)
    ! _____________________________________________________________________

    If E1.init(0,0) = False
        Case Missive('An error has occurred creating your excel document.'&|
          '|Please try again.','ServiceBase 3g',|
                       'mstop.jpg','/OK') 
            Of 1 ! OK Button
        End ! Case Missive
    End ! If E1.init(0,0) = False

    !Open progress window
    RecordsPerCycle = 25
    RecordsProcessed = 0
    PercentProgress = 0
    Progress:Thermometer = 0
    RecordsToProcess = 50
    Open(Progresswindow)

    local:ReportStartDate = Today()
    local:ReportStartTime = Clock()
    local.UpdateProgressWindow('Report Started: ' & Format(local:ReportStartDate,@d6) & ' ' & Format(local:ReportStartTime,@t1))
    local.UpdateProgressWindow('')

    E1.NewWorkBook()

    E1.RenameWorkSheet('Repeat IMEI Report')
    E1.WriteToCell('Repeat IMEI Number Report','A1')
    E1.WriteToCell('Start Date','A2')
    E1.WriteToCell('End Date','A3')
    E1.WriteToCell(Format(tmp:StartDate,@d06),'B2')
    E1.WriteToCell(Format(tmp:EndDate,@d06),'B3')
    E1.WriteToCell(tmp:VersionNumber,'D2')

    E1.WriteToCell('Job Number','A5')
    E1.WriteToCell('Franchise Branch Code','B5')
    E1.WriteToCell('Customer Name','C5')
    E1.WriteToCell('Manufacturer','D5')
    E1.WriteToCell('Model Number','E5')
    E1.WriteToCell('Incoming IMEI Number','F5')
    E1.WriteToCell('Number Of Times Booked In','G5')
    E1.WriteToCell('Incoming Fault Description','H5')
    E1.WriteToCell('Main Out Fault Description','I5')
    E1.WriteToCell('IMEI','J5')
    E1.WriteToCell('Date Booked','K5')

    tmp:LineNumber = 6
    
    Free(ReportedJobsQueue)
    Clear(ReportedJobsQueue)

    Access:JOBS.ClearKey(job:Date_Booked_Key)
    job:date_booked = tmp:StartDate
    Set(job:Date_Booked_Key,job:Date_Booked_Key)
    Loop
        If Access:JOBS.NEXT()
           Break
        End !If
        If job:date_booked > tmp:EndDate   |
            Then Break.  ! End If

        Do GetNextRecord2
        If tmp:Cancel
            Break
        End ! If tmp:Cancel


        If job:ESN = '' Or job:ESN = 'N/A'
            Cycle
        End ! If job:ESN = '' Or job:ESN = 'N/A'

        q:JobNumber = job:Ref_Number
        Get(ReportedJobsQueue,q:JobNumber)
        If ~Error()
            Cycle
        End ! If ~Error()

        Clear(MainGroup)
        Clear(BouncerQueue)
        Free(BouncerQueue)

        Count# = 0
        Access:JOBS_ALIAS.ClearKey(job_ali:ESN_Key)
        job_ali:ESN = job:ESN
        Set(job_ali:ESN_Key,job_ali:ESN_Key)
        Loop
            If Access:JOBS_ALIAS.NEXT()
               Break
            End !If
            If job_ali:ESN <> job:ESN      |
                Then Break.  ! End If
            If job_ali:Ref_Number = job:Ref_Number
                Cycle
            End ! If job_ali:Ref_Number = job:Ref_Number

            If job_ali:Date_Booked < job:Date_Booked - 90
                Cycle
            End ! If job_ali:Date_Booked < job:Date_Booked - 90

            If job_ali:Date_Booked > tmp:EndDate
                Cycle
            End ! If job_ali:Date_Booked > Deformat('30/4/2006',@d6)


            Do GetNextRecord2

            Count# += 1

            b:JobNumber = job_ali:Ref_Number

            Access:WEBJOB.Clearkey(wob:RefNumberKey)
            wob:RefNumber   = job_ali:Ref_Number
            If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                ! Found
                Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                tra:Account_Number  = wob:HeadAccountNumber
                If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                    ! Found
                    b:BranchCode = tra:BranchIdentification
                Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                    ! Error
                End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                ! Error
            End ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign

            b:CustomerName  = job_ali:Company_Name
            b:Manufacturer  = job_ali:Manufacturer
            b:ModelNumber   = job_ali:Model_Number

            IMEIError# = 0
            If job:Third_Party_Site <> ''
                Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
                jot:RefNumber = job_ali:Ref_Number
                Set(jot:RefNumberKey,jot:RefNumberKey)
                If Access:JOBTHIRD.NEXT()
                    IMEIError# = 1
                Else ! If Access:JOBTHIRD.NEXT()
                    If jot:RefNumber <> job_ali:Ref_Number
                        IMEIError# = 1
                    Else ! If jot:Ref_Number <> job:Ref_Number
                        IMEIError# = 0
                    End ! If jot:Ref_Number <> job:Ref_Number
                End ! If Access:JOBTHIRD.NEXT()
            Else ! If job:Third_Party_Site <> ''
                IMEIError# = 1
            End ! If job:Third_Party_Site <> ''

            If IMEIError# = 0
                b:IMEINumber = jot:OriginalIMEI
            Else ! If IMEIError# = 0
                b:IMEINumber = job_ali:ESN
            End ! If IMEIError# = 0

            mfo:Description = ''
            Access:MANFAULT.ClearKey(maf:InFaultKey)
            maf:Manufacturer = job_ali:Manufacturer
            maf:InFault      = True
            If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
                !Found
                Access:MANFAULO.ClearKey(mfo:Field_Key)
                mfo:Manufacturer = job_ali:Manufacturer
                mfo:Field_Number = maf:Field_Number
                mfo:Field        = Evaluate('job_ali:Fault_Code' & Clip(mfo:Field_Number))
                If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                    !Found

                Else !If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                    !Error
                End !If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

            Else !If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
                !Error
            End !If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
            b:InFault   = mfo:Description

            mfo:Description = ''
            Access:MANFAULT.ClearKey(maf:MainFaultKey)
            maf:Manufacturer = job_ali:Manufacturer
            maf:MainFault    = True
            If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                !Found
                Access:MANFAULO.Clearkey(mfo:Field_Key)
                mfo:Manufacturer    = job_ali:Manufacturer
                mfo:Field_Number    = maf:Field_Number
                mfo:Field           = Evaluate('job_ali:Fault_Code' & Clip(mfo:Field_Number))
                If Access:MANFAULO.Tryfetch(mfo:Field_Key) = Level:Benign
                    ! Found

                Else ! If Access:MANFAULO.Tryfetch(mfo:Field_Key) = Level:Benign
                    ! Error
                End ! If Access:MANFAULO.Tryfetch(mfo:Field_Key) = Level:Benign
            Else !If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
                !Error
            End !If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
            b:OutFault = mfo:Description
            b:OutIMEINumber = job_ali:ESN
            b:DateBooked = job_ali:Date_Booked
            Add(BouncerQueue)
        End !Loop

        If Count# = 0
            Cycle
        End ! If Count# = 0


        Access:WEBJOB.Clearkey(wob:RefNumberKey)
        wob:RefNumber   = job:Ref_Number
        If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
            ! Found

        Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
            ! Error
            Cycle
        End ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign

        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = wob:HeadAccountNumber
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            ! Found

        Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            ! Error
            Cycle
        End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign

        m:JobNumber = job:Ref_Number
        m:BranchCode = tra:BranchIdentification
        m:CustomerName = job:Company_Name
        m:Manufacturer = job:Manufacturer
        m:ModelNumber = job:Model_Number

        IMEIError# = 0
        If job:Third_Party_Site <> ''
            Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
            jot:RefNumber = job:Ref_Number
            Set(jot:RefNumberKey,jot:RefNumberKey)
            If Access:JOBTHIRD.NEXT()
                IMEIError# = 1
            Else ! If Access:JOBTHIRD.NEXT()
                If jot:RefNumber <> job:Ref_Number
                    IMEIError# = 1
                Else ! If jot:Ref_Number <> job:Ref_Number
                    IMEIError# = 0
                End ! If jot:Ref_Number <> job:Ref_Number
            End ! If Access:JOBTHIRD.NEXT()
        Else ! If job:Third_Party_Site <> ''
            IMEIError# = 1
        End ! If job:Third_Party_Site <> ''

        If IMEIError# = 0
            m:IMEINumber = jot:OriginalIMEI
        Else ! If IMEIError# = 0
            m:IMEINumber = job:ESN
        End ! If IMEIError# = 0

        m:NumberOfTimes = Count# + 1

        mfo:Description = ''
        Access:MANFAULT.ClearKey(maf:InFaultKey)
        maf:Manufacturer = job:Manufacturer
        maf:InFault      = True
        If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
            !Found
            Access:MANFAULO.ClearKey(mfo:Field_Key)
            mfo:Manufacturer = job:Manufacturer
            mfo:Field_Number = maf:Field_Number
            mfo:Field        = Evaluate('job:Fault_Code' & Clip(mfo:Field_Number))
            If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                !Found

            Else !If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
                !Error
            End !If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign

        Else !If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
            !Error
        End !If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign

        m:InFault   = mfo:Description

        mfo:Description = ''
        Access:MANFAULT.ClearKey(maf:MainFaultKey)
        maf:Manufacturer = job:Manufacturer
        maf:MainFault    = True
        If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
            !Found
            Access:MANFAULO.Clearkey(mfo:Field_Key)
            mfo:Manufacturer    = job:Manufacturer
            mfo:Field_Number    = maf:Field_Number
            mfo:Field           = Evaluate('job:Fault_Code' & Clip(mfo:Field_Number))
            If Access:MANFAULO.Tryfetch(mfo:Field_Key) = Level:Benign
                ! Found

            Else ! If Access:MANFAULO.Tryfetch(mfo:Field_Key) = Level:Benign
                ! Error
            End ! If Access:MANFAULO.Tryfetch(mfo:Field_Key) = Level:Benign
        Else !If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
            !Error
        End !If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign

        m:OutFault = mfo:Description
        m:OutIMEINumber = job:ESN
        m:DateBooked = job:Date_Booked

        Loop x# = 1 To Records(BouncerQueue)
            Get(BouncerQueue,x#)
            b:NumberOfTimes = m:NumberOfTimes
            E1.WriteToCell(b:JobNumber,'A' & TMP:LINENUMBER)
            E1.WriteToCell(b:BranchCode,'B' & TMP:LINENUMBER)
            E1.WriteToCell(b:CustomerName,'C' & TMP:LINENUMBER)
            E1.WriteToCell(b:Manufacturer,'D' & TMP:LINENUMBER)
            E1.WriteToCell(b:ModelNumber,'E' & TMP:LINENUMBER)
            E1.WriteToCell(b:IMEINumber,'F' & TMP:LINENUMBER)
            E1.WriteToCell(b:NumberOfTimes,'G' & TMP:LINENUMBER)
            E1.WriteToCell(b:InFault,'H' & TMP:LINENUMBER)
            E1.WriteToCell(b:Outfault,'I' & TMP:LINENUMBER)
            E1.WriteToCell(b:OutIMEINumber,'J' & TMP:LINENUMBER)
            E1.WriteToCell(Format(b:DateBooked,@d06),'K' & TMP:LINENUMBER)
            tmp:LineNumber += 1
            
            q:JobNumber = b:JobNumber
            Add(ReportedJobsQueue)
        End ! Loop x# = 1 To Records(BouncerQueue)
        E1.WriteToCell(m:JobNumber,'A' & TMP:LINENUMBER)
        E1.WriteToCell(m:BranchCode,'B' & TMP:LINENUMBER)
        E1.WriteToCell(m:CustomerName,'C' & TMP:LINENUMBER)
        E1.WriteToCell(m:Manufacturer,'D' & TMP:LINENUMBER)
        E1.WriteToCell(m:ModelNumber,'E' & TMP:LINENUMBER)
        E1.WriteToCell(m:IMEINumber,'F' & TMP:LINENUMBER)
        E1.WriteToCell(m:NumberOfTimes,'G' & TMP:LINENUMBER)
        E1.WriteToCell(m:InFault,'H' & TMP:LINENUMBER)
        E1.WriteToCell(m:Outfault,'I' & TMP:LINENUMBER)
        E1.WriteToCell(m:OutIMEINumber,'J' & TMP:LINENUMBER)
        E1.WriteToCell(Format(m:DateBooked,@d06),'K' & TMP:LINENUMBER)
        tmp:LineNumber += 1
        q:JobNumber = m:JobNumber
        Add(ReportedJobsQueue)
    End !Loop

    E1.SetCellFontName('Tahoma','A1','K' & tmp:LineNumber)
    E1.SetCellFontSize(10,'A1')
    E1.SetCellFontSize(8,'A2','K' & tmp:LineNumber)
    E1.SetCellFontStyle('Bold','A1','K5')
    E1.SetCellBackgroundColor(color:Silver,'A1','D3')
    E1.SetCellBackgroundColor(color:Silver,'A5','K5')
    E1.SetcellNumberFormat(oix:NumberFormatNumber,,0,,'F6','F' & tmp:LineNumber)
    E1.SetcellNumberFormat(oix:NumberFormatNumber,,0,,'J6','J' & tmp:LineNumber)
    E1.SetColumnWidth('A','K')

    E1.SelectCells('A5')
    E1.AutoFilter('A5','K5')
    E1.Selectcells('B6')
    E1.FreezePanes()

    E1.SaveAs(excel:Filename)
    E1.CloseWorkBook(2)

    E1.Kill()

    ! Inserting (DBH 27/06/2006) #7879 - Attempt to manually set the auto filter.
    ExcelSetup(0)
    ExcelOpenDoc(excel:Filename)
    ExcelAutoFilter('A5:K5')
    ExcelAutoFit('A:K')
    ExcelSelectRange('B6')
    ExcelSaveWorkBook(excel:Filename)
    ExcelClose()
    ! End (DBH 27/06/2006) #7879

    If tmp:cancel = 0 Or tmp:Cancel = 2
        local.UpdateProgressWindow('================')
        local.UpdateProgressWindow('Report Finished: ' & Format(Today(),@d6) & ' ' & Format(Clock(),@t1))
    Else ! If tmp:cancel = 0 Or tmp:Cancel = 2
        local.UpdateProgressWindow('================')
        local.UpdateProgressWindow('Report CANCELLED: ' & Format(TOday(),@d6) & ' ' & Format(Clock(),@t1))
    End ! If tmp:cancel = 0 Or tmp:Cancel = 2
    BHReturnDaysHoursMins(BHTimeDifference24Hr(local:ReportStartDate,Today(),local:ReportStarttime,Clock()),days#,hours#,mins#)
    local.UpdateProgressWindow('Time To Finish: ' & days# & ' Dys, ' & Format(hours#,@n02) & ':' & Format(mins#,@n02))

    Do EndPrintRun
    ?progress:userstring{prop:text} = 'Finished.'
    ?ProgressCancel{prop:Hide} = 1
    ?Finish{prop:Hide} = 0
    If Command('/SCHEDULE')
        If Access:REPSCHLG.PrimeRecord() = Level:Benign
            rlg:REPSCHEDRecordNumber = rpd:RecordNumber
            rlg:Information = 'Report Name: ' & Clip(rpd:ReportName) & ' - ' & Clip(rpd:ReportCriteriaType) & |
                              '<13,10>Report Finished'
            If Access:REPSCHLG.TryInsert() = Level:Benign
                !Insert
            Else ! If Access:REPSCHLG.TryInsert() = Level:Benign
                Access:REPSCHLG.CancelAutoInc()
            End ! If Access:REPSCHLG.TryInsert() = Level:Benign
        End ! If Access.REPSCHLG.PrimeRecord() = Level:Benign
    Else
        ?Button:OpenReportFolder{Prop:Hide} = 0
        Accept
            Case Field()
                Of ?Finish
                    Case Event()
                        Of Event:Accepted
                            Break

                    End !Case Event()
                Of ?Button:OpenReportFolder
                    Case Event()
                    Of Event:Accepted
                        RUN('EXPLORER.EXE ' & Clip(local:Desktop))
                    End ! Case Event()
            End !Case Field()
        End !Accept
    End
    Close(ProgressWindow)
    Post(Event:CloseWindow)



getnextrecord2      routine !Progress Window Routines (DBH: 22-03-2004)
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress >= 100
        recordsprocessed        = 0
        percentprogress         = 0
        progress:thermometer    = 0
        recordsthiscycle        = 0
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    !0{prop:Text} = ?progress:pcttext{prop:text}
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                Yield()
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case Missive('Do you want to finish off building the excel document with the data you have compiled so far, or just quit now?','ServiceBase 3g',|
                       'mquest.jpg','\Cancel|Quit|/Finish') 
            Of 3 ! Finish Button
                tmp:Cancel = 2
            Of 2 ! Quit Button
                tmp:Cancel = 1
            Of 1 ! Cancel Button
        End ! Case Missive
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    display()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g Webmaster'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020650'&'0'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('RepeatIMEIReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:JOBS.Open
  Relate:REPSCHCR.Open
  Relate:WEBJOB.Open
  Access:JOBS_ALIAS.UseFile
  Access:MANUFACT.UseFile
  Access:MANFAULT.UseFile
  Access:MANFAULO.UseFile
  Access:TRADEACC.UseFile
  Access:JOBTHIRD.UseFile
  Access:REPSCHED.UseFile
  Access:REPSCHLG.UseFile
  SELF.FilesOpened = True
  tmp:StartDate = Date(Month(Today()),1,Year(Today()))
  tmp:EndDate = Today()
  OPEN(window)
  SELF.Opened=True
  ! ========= Set Report Version =============
  Include('..\ReportVersion.inc')
  tmp:VersionNumber = 'Version Number: ' & Clip(tmp:VersionNumber) & '5003'
  ?ReportVersion{prop:Text} = tmp:VersionNumber
  Bryan.CompFieldColour()
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  SELF.SetAlerts()
  ! Inserting (DBH 31/08/2007) # 9125 - Is this an automatic report?
  If Command('/SCHEDULE')
      x# = Instring('%',Command(),1,1)
      Access:REPSCHED.ClearKey(rpd:RecordNumberKey)
      rpd:RecordNumber = Clip(Sub(Command(),x# + 1,20))
      If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
          !Found
          Access:REPSCHCR.ClearKey(rpc:ReportCriteriaKey)
          rpc:ReportName = rpd:ReportName
          rpc:ReportCriteriaType = rpd:ReportCriteriaType
          If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
              !Found
  
              Case rpc:DateRangeType
              Of 1 ! Today Only
                  tmp:StartDate = Today()
                  tmp:EndDate = Today()
              Of 2 ! 1st Of Month
                  tmp:StartDate = Date(Month(Today()),1,Year(Today()))
                  tmp:EndDate = Today()
              Of 3 ! Whole Month
  ! Changing (DBH 31/01/2008) # 9711 - Allow for change of year
  !                tmp:StartDate = Date(Month(Today()) - 1, 1, Year(Today()))
  !                tmp:EndDate = Date(Month(Today()),1,Year(Today())) - 1
  ! to (DBH 31/01/2008) # 9711
                  tmp:EndDate = Date(Month(Today()),1,Year(Today())) - 1
                  tmp:StartDate = Date(Month(tmp:EndDate),1,Year(tmp:EndDate))
  ! End (DBH 31/01/2008) #9711
              End ! Case rpc:DateRangeType
  
              Do Reporting
              Post(Event:CloseWindow)
          Else ! If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
              !Error
          End ! If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
  
      Else ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
          !Error
      End ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
  
  End ! If Command('/SCHEDULE')
  ! End (DBH 31/08/2007) #9125
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBS.Close
    Relate:REPSCHCR.Close
    Relate:WEBJOB.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      Do Reporting
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020650'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020650'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020650'&'0')
      ***
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeEvent()
    E1.TakeEvent ('', '')
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

local.UpdateProgressWindow      Procedure(String func:Text)
Code
    staque:StatusMessage    = Clip(func:Text)
    Add(StatusQueue)
    Select(?List1,Records(StatusQueue))
    Display()
!---------------------------------------------------------------------------------
E1.Init   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0)
ReturnValue   byte
  CODE
  ReturnValue = PARENT.Init (pStartVisible,pEnableEvents)
  self.TakeSnapShotOfWindowPos()
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.Kill   PROCEDURE (byte pUnloadCOM=1)
ReturnValue   byte
  CODE
  self.RestoreSnapShotOfWindowPos()
  ReturnValue = PARENT.Kill (pUnloadCOM)
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.TakeEvent   PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0)
  CODE
  PARENT.TakeEvent (pEventString1,pEventString2,pEventNumber,pEventType,pEventStatus)
  if pEventType = 0  ! Generated by CapeSoft Office Inside
    case event()
      of event:accepted
        case field()
      end
    end
  end
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
!Initialise
ExcelSetup          Procedure(Byte      func:Visible)
    Code
    Excel   = Create(0,Create:OLE)
    Excel{prop:Create} = 'Excel.Application'
    Excel{'ASYNC'}  = False
    Excel{'Application.DisplayAlerts'} = False

    Excel{'Application.ScreenUpdating'} = func:Visible
    Excel{'Application.Visible'} = func:Visible
    Excel{'Application.Calculation'} = 0FFFFEFD9h
    excel:ActiveWorkBook    = Excel{'ActiveWorkBook'}
    Yield()

!Make a WorkBook
ExcelMakeWorkBook   Procedure(String    func:Title,String   func:Author,String  func:AppName)
    Code
    Excel{prop:Release} = excel:ActiveWorkBook
    excel:ActiveWorkBook = Excel{'Application.Workbooks.Add()'}

    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Title")'} = func:Title
    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Author")'} = func:Author
    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Application Name")'} = func:AppName

    excel:Selected = Excel{'Sheets("Sheet2").Select'}
    Excel{prop:Release} = excel:Selected

    Excel{'ActiveWindow.SelectedSheets.Delete'}

    excel:Selected = Excel{'Sheets("Sheet1").Select'}
    Excel{prop:Release} = excel:Selected
    Yield()

ExcelMakeSheet      Procedure()
ActiveWorkBook  CString(20)
    Code
    ActiveWorkBook = Excel{'ActiveWorkBook'}

    Excel{ActiveWorkBook & '.Sheets("Sheet3").Select'}
    Excel{prop:Release} = ActiveWorkBook

    Excel{excel:ActiveWorkBook & '.Sheets.Add'}
    Yield()
!Select A Sheet
ExcelSelectSheet    Procedure(String    func:SheetName)
    Code
    Excel{'Sheets("' & Clip(func:SheetName) & '").Select'}
    Yield()
!Setup Sheet Type (P = Portrait, L = Lanscape)
ExcelSheetType      Procedure(String    func:Type)
    Code
    Case func:Type
        Of 'L'
            Excel{'ActiveSheet.PageSetup.Orientation'}  = 2
        Of 'P'
            Excel{'ActiveSheet.PageSetup.Orientation'}  = 1
    End !Case func:Type
    Excel{'ActiveSheet.PageSetup.FitToPagesWide'}  = 1
    Excel{'ActiveSheet.PageSetup.FitToPagesTall'}  = 9999
    Excel{'ActiveSheet.PageSetup.Order'}  = 2

    Yield()
ExcelHorizontal     Procedure(String    func:Direction)
Selection   Cstring(20)
    Code
    Selection = Excel{'Selection'}
    Case func:Direction
        Of 'Centre'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFF4h
        Of 'Left'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFDDh
        Of 'Right'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFC8h
    End !Case tmp:Direction
    Excel{prop:Release} = Selection
    Yield()
ExcelVertical   Procedure(String func:Direction)
Selection   CString(20)
    Code
    Selection = Excel{'Selection'}
    Case func:Direction
        Of 'Top'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFC0h
        Of 'Centre'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFF4h
        Of 'Bottom'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFF5h
    End ! Case func:Direction
    Excel{prop:Release} = Selection
    Yield()

ExcelCell   Procedure(String    func:Text,Byte    func:Bold)
Selection   Cstring(20)
    Code
    Selection = Excel{'Selection'}
    If func:Bold
        Excel{Selection & '.Font.Bold'} = True
    Else
        Excel{Selection & '.Font.Bold'} = False
    End !If func:Bold
    Excel{prop:Release} = Selection

    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.Formula'}  = func:Text
    Excel{excel:Selected & '.Offset(0, 1).Select'}
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelFormatCell     Procedure(String    func:Format)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.NumberFormat'} = func:Format
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelFormatRange    Procedure(String    func:Range,String   func:Format)
Selection       Cstring(20)
    Code

    ExcelSelectRange(func:Range)
    Selection   = Excel{'Selection'}
    Excel{Selection & '.NumberFormat'} = func:Format
    Excel{prop:Release} = Selection
    Yield()

ExcelNewLine    Procedure(Long func:Number)
    Code
    Loop excelloop# = 1 to func:Number
        ExcelSelectRange('A' & (ExcelCurrentRow() + 1))
    End !Loop excelloop# = 1 to func:Number
    !excel:Selected = Excel{'ActiveCell'}
    !Excel{excel:Selected & '.Offset(0, -' & Excel{excel:Selected & '.Column'} - 1 & ').Select'}
    !Excel{excel:Selected & '.Offset(1, 0).Select'}
    !Excel{prop:Release} = excel:Selected
    Yield()

ExcelMoveDown   Procedure()
    Code
    ExcelSelectRange(ExcelCurrentColumn() & (ExcelCurrentRow() + 1))
    Yield()
!Set Column Width

ExcelColumnWidth        Procedure(String    func:Range,Long   func:Width)
    Code
    Excel{'ActiveSheet.Columns("' & Clip(func:Range) & '").ColumnWidth'} = func:Width
    Yield()
ExcelCellWidth          Procedure(Long  func:Width)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.ColumnWidth'} = func:Width
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelAutoFit            Procedure(String func:Range)
    Code
    Excel{'ActiveSheet.Columns("' & Clip(func:Range) & '").Columns.AutoFit'}
    Yield()
!Set Gray Box

ExcelGrayBox            Procedure(String    func:Range)
Selection   CString(20)
    Code
    Selection = Excel{'Selection'}
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Excel{Selection & '.Interior.ColorIndex'} = 15
    Excel{Selection & '.Interior.Pattern'} = 1
    Excel{Selection & '.Interior.PatternColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(7).LineStyle'} = 1
    Excel{Selection & '.Borders(7).Weight'} = 2
    Excel{Selection & '.Borders(7).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(10).LineStyle'} = 1
    Excel{Selection & '.Borders(10).Weight'} = 2
    Excel{Selection & '.Borders(10).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(8).LineStyle'} = 1
    Excel{Selection & '.Borders(8).Weight'} = 2
    Excel{Selection & '.Borders(8).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(9).LineStyle'} = 1
    Excel{Selection & '.Borders(9).Weight'} = 2
    Excel{Selection & '.Borders(9).ColorIndex'} = 0FFFFEFF7h
    Excel{prop:Release} = Selection
    Yield()
ExcelGrid   Procedure(String    func:Range,Byte  func:Left,Byte  func:Top,Byte   func:Right,Byte func:Bottom,Byte func:Colour)
Selection   Cstring(20)
    Code
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Selection = Excel{'Selection'}
    If func:Colour
        Excel{Selection & '.Interior.ColorIndex'} = func:Colour
        Excel{Selection & '.Interior.Pattern'} = 1
        Excel{Selection & '.Interior.PatternColorIndex'} = 0FFFFEFF7h
    End !If func:Colour
    If func:Left
        Excel{Selection & '.Borders(7).LineStyle'} = 1
        Excel{Selection & '.Borders(7).Weight'} = 2
        Excel{Selection & '.Borders(7).ColorIndex'} = 0FFFFEFF7h
    End !If func:Left

    If func:Right
        Excel{Selection & '.Borders(10).LineStyle'} = 1
        Excel{Selection & '.Borders(10).Weight'} = 2
        Excel{Selection & '.Borders(10).ColorIndex'} = 0FFFFEFF7h
    End !If func:Top

    If func:Top
        Excel{Selection & '.Borders(8).LineStyle'} = 1
        Excel{Selection & '.Borders(8).Weight'} = 2
        Excel{Selection & '.Borders(8).ColorIndex'} = 0FFFFEFF7h
    End !If func:Right

    If func:Bottom
        Excel{Selection & '.Borders(9).LineStyle'} = 1
        Excel{Selection & '.Borders(9).Weight'} = 2
        Excel{Selection & '.Borders(9).ColorIndex'} = 0FFFFEFF7h
    End !If func:Bottom
    Excel{prop:Release} = Selection
    Yield()
!Select a range of cells
ExcelSelectRange        Procedure(String    func:Range)
    Code
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Yield()
!Change font size
ExcelFontSize           Procedure(Byte  func:Size)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.Font.Size'}   = func:Size
    Excel{prop:Release} = excel:Selected
    Yield()
!Sheet Name
ExcelSheetName          Procedure(String    func:Name)
    Code
    Excel{'ActiveSheet.Name'} = func:Name
    Yield()
ExcelAutoFilter         Procedure(String    func:Range)
Selection   Cstring(20)
    Code
    ExcelSelectRange(func:Range)
    Selection = Excel{'Selection'}
    Excel{Selection & '.AutoFilter'}
    Excel{prop:Release} = Selection
    Yield()
ExcelDropAllSheets      Procedure()
    Code
    Excel{prop:Release} = excel:ActiveWorkBook
    Loop While Excel{'WorkBooks.Count'} > 0
        excel:ActiveWorkBook = Excel{'ActiveWorkBook'}
        Excel{'ActiveWorkBook.Close(1)'}
        Excel{prop:Release} = excel:ActiveWorkBook
    End !Loop While Excel{'WorkBooks.Count'} > 0
    Yield()
ExcelClose              Procedure()
!xlCalculationAutomatic
    Code
    Excel{'Application.Calculation'}= 0FFFFEFF7h
    Excel{'Application.Quit'}
    Excel{prop:Deactivate}
    Destroy(Excel)
    Yield()
ExcelDeleteSheet        Procedure(String    func:SheetName)
    Code
    ExcelSelectSheet(func:SheetName)
    Excel{'ActiveWindow.SelectedSheets.Delete'}
    Yield()
ExcelSaveWorkBook       Procedure(String    func:Name)
    Code
    Excel{'Application.ActiveWorkBook.SaveAs("' & LEFT(CLIP(func:Name)) & '")'}
    Excel{'Application.ActiveWorkBook.Close()'}
   Excel{'Application.Calculation'} = 0FFFFEFF7h
    Excel{'Application.Quit'}

    Excel{PROP:DEACTIVATE}
    YIELD()
ExcelFontColour         Procedure(String    func:Range,Long func:Colour)
    Code
    !16 = Gray
    ExcelSelectRange(func:Range)
    Excel{'Selection.Font.ColorIndex'} = func:Colour
    Yield()
ExcelWrapText           Procedure(String    func:Range,Byte func:True)
Selection   Cstring(20)
    Code
    ExcelSelectRange(func:Range)
    Selection = Excel{'Selection'}
    Excel{Selection & '.WrapText'} = func:True
    Excel{prop:Release} = Selection
    Yield()
ExcelCurrentColumn      Procedure()
CurrentColumn   String(20)
    Code
    excel:Selected = Excel{'ActiveCell'}
    CurrentColumn = Excel{excel:Selected & '.Column'}
    Excel{prop:Release} = excel:Selected
    Yield()
    Return CurrentColumn

ExcelCurrentRow         Procedure()
CurrentRow      String(20)
    Code
    excel:Selected = Excel{'ActiveCell'}
    CurrentRow = Excel{excel:Selected & '.Row'}
    Excel{prop:Release} = excel:Selected
    Yield()
    Return CurrentRow

ExcelPasteSpecial       Procedure(String    func:Range)
Selection       CString(20)
    Code
    ExcelSelectRange(func:Range)
    Selection   = Excel{'ActiveCell'}
    Excel{Selection & '.PasteSpecial'}
    Excel{prop:Release} = Selection
    Yield()

ExcelConvertFormula     Procedure(String    func:Formula)
    Code
    Return Excel{'Application.ConvertFormula("' & Clip(func:Formula) & '",' & 0FFFFEFCAh & ',' & 1 & ')'}

ExcelGetFilename        Procedure(Byte  func:DontAsk)
sav:Path        CString(255)
func:Desktop     CString(255)
    Code

        SHGetSpecialFolderPath( GetDesktopWindow(), func:Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
        func:Desktop = Clip(func:Desktop) & '\ServiceBase Export'

        !Does the Export Folder already Exists?
        If ~Exists(Clip(func:Desktop))
            If ~MkDir(func:Desktop)
                Return Level:Fatal

            End !If MkDir(func:Desktop)
        End !If Exists(Clip(tmp:Desktop))

        Error# = 0
        sav:Path = Path()
        SetPath(func:Desktop)

        func:Desktop = Clip(func:Desktop) & '\' & CLIP(Excel:ProgramName) & ' ' & FORMAT(TODAY(), @D12) & '.xls'

        If func:DontAsk = False
            IF NOT FILEDIALOG('Save Spreadsheet', func:Desktop, 'Microsoft Excel Workbook|*.XLS', |
                FILE:KeepDir + FILE:Save + FILE:NoError + FILE:LongName)
                Error# = 1
            End!IF NOT FILEDIALOG('Save Spreadsheet', tmp:Desktop, 'Microsoft Excel Workbook|*.XLS', |
        End !If func:DontAsk = True

        SetPath(sav:Path)

        If Error#
            Return Level:Fatal
        End !If Error#
        excel:FileName    = func:Desktop
        Return Level:Benign

ExcelGetDirectory       Procedure()
sav:Path        CString(255)
func:Desktop    CString(255)
    Code
        SHGetSpecialFolderPath( GetDesktopWindow(), func:Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
        func:Desktop = Clip(func:Desktop) & '\ServiceBase Export\'
        !Does the Export Folder already Exists?
        Error# = 0
        If ~Exists(Clip(func:Desktop))
            If ~MkDir(func:Desktop)
                If Not FileDialog('Save Spreadsheet To Folder', func:Desktop, ,FILE:KeepDir+ File:Save + File:NoError + File:LongName + File:Directory)
                    Return Level:Fatal
                End !+ File:LongName + File:Directory)
            End !If MkDir(func:Desktop)
        End !If Exists(Clip(tmp:Desktop))

        excel:FileName  = func:Desktop
        Return Level:Benign

ExcelColumnLetter     Procedure(Long func:ColumnNumber)
local:Over26        Long()
Code
    local:Over26 = 0
    If func:ColumnNumber > 26
        Loop Until func:ColumnNumber <= 26
            local:Over26 += 1
            func:ColumnNumber -= 26
        End !Loop Until ColumnNumber <= 26.
    End !If func:ColumnNumber > 26

    If local:Over26 > 26
        Stop('ExcelColumnLetter Procedure Out Of Range!')
    End !If local:Over26 > 26
    If local:Over26 > 0
        Return Clip(CHR(local:Over26 + 64)) & Clip(CHR(func:ColumnNumber + 64))
    Else !If local:Over26 > 0
        Return Clip(CHR(func:ColumnNumber + 64))
    End !If local:Over26 > 0
ExcelOpenDoc        Procedure(String func:FileName)
Code
    Excel{'Workbooks.Open("' & Clip(func:FileName) & '")'}
ExcelFreeze         Procedure(String func:Cell)
Code
    Excel{'Range("' & Clip(func:Cell) & '").Select'}
    Excel{'ActiveWindow.FreezePanes'} = True
