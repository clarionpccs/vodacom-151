

   MEMBER('vodr0046.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('VODR0002.INC'),ONCE        !Local module procedure declarations
                     END


IncomeReportRetailSales PROCEDURE                     !Generated from procedure template - Window

tmp:VersionNumber    STRING(30)
Automatic            BYTE
Parameter_Group      GROUP,PRE()
LOC:EndDate          DATE
LOC:StartDate        DATE
                     END
Local_Group          GROUP,PRE(LOC)
Account_Name         STRING(100)
AccountNumber        STRING(15)
ApplicationName      STRING('ServiceBase')
CompanyName          STRING(30)
Courier              STRING(30)
DayLeawayCount       LONG
DesktopPath          STRING(255)
FileName             STRING(255)
JobNumber            LONG
JobsExtendedInSync   BYTE
MaxTabNumber         BYTE(2)
PartNumber           STRING(30)
PartType             STRING(10)
Path                 STRING(255)
ProgramName          STRING(100)
SalesType            STRING(10)
SectionName          STRING(100)
TabNumber            BYTE(1)
Text                 STRING(255)
UserCode             STRING(3)
UserName             STRING(100)
Version              STRING('3.1.0000')
                     END
Misc_Group           GROUP,PRE()
Result               BYTE
CountOrders          LONG
CountLines           LONG
CountParts           LONG
CountDelivered       LONG
                     END
DASTAG_Group         GROUP,PRE()
AccountValue         STRING(15)
AccountTag           STRING(1)
AccountCount         LONG
AccountChange        BYTE
                     END
Progress_Group       GROUP,PRE()
RecordCount          LONG
Progress:Text        STRING(100)
                     END
Excel_Group          GROUP,PRE()
Excel                CSTRING(20)
excel:ColumnName     STRING(50)
excel:ColumnWidth    REAL
excel:CommentText    STRING(255)
excel:DateFormat     STRING('dd mmm yyyy')
excel:OperatingSystem REAL
excel:Visible        BYTE
                     END
Clipboard_Group      GROUP,PRE(clip)
OriginalValue        STRING(255)
Saved                BYTE
Value                CSTRING(1024)
                     END
HeaderQueue          QUEUE,PRE(head)
ColumnName           STRING(50)
ColumnWidth          REAL
NumberFormat         STRING(20)
                     END
Sheet_Group          GROUP,PRE(sheet)
TempLastCol          STRING(1)
HeadLastCol          STRING('J')
DataLastCol          STRING('O')
HeadSummaryRow       LONG(9)
DataSectionRow       LONG(9)
DataHeaderRow        LONG(11)
                     END
SubAccount_Queue     QUEUE,PRE(saQ)
AccountNumber        STRING(15)
AccountName          STRING(30)
HeadAccountNumber    STRING(15)
HeadAccountName      STRING(30)
BranchIdentification STRING(2)
PartsVATCode         STRING(2)
LabourVATCode        STRING(2)
RetailVATCode        STRING(2)
PartsVATRate         REAL
LabourVATRate        REAL
RetailVATRate        REAL
CountInvoices        LONG
CountParts           LONG
CountLines           LONG
Courier              REAL
Labour               REAL
Parts                REAL
Total                REAL
                     END
HeadAccount_Queue    QUEUE,PRE(haQ)
AccountNumber        STRING(15)
AccountName          STRING(30)
BranchIdentification STRING(2)
InvoiceSubAccounts   BYTE
PartsVATCode         STRING(2)
LabourVATCode        STRING(2)
RetailVATCode        STRING(2)
PartsVATRate         REAL
LabourVATRate        REAL
RetailVATRate        REAL
                     END
Debug_Group          GROUP,PRE(debug)
Active               LONG
Count                LONG
                     END
Calc_Group           GROUP,PRE(calc)
Courier              REAL
Labour               REAL
Parts                REAL
Paid                 REAL
VATRate              REAL
VAT                  REAL
Parts_COS            REAL
Total                REAL
TotalVolume          REAL
CountLines           LONG
CountParts           REAL
CountLoans           LONG
                     END
Account_Queue        QUEUE,PRE(acQ)
AccountNumber        STRING(15)
AccountName          STRING(30)
CountInvoices        LONG
CountLines           LONG
CountParts           LONG
CountLoans           LONG
Courier              REAL
Labour               REAL
Parts                REAL
Total                REAL
                     END
MainWindow           WINDOW('Report'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(240,146,200,144),USE(?PanelMain),FILL(0D6EAEFH)
                       PANEL,AT(244,148,192,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(244,164,192,44),USE(?PanelTip),FILL(0D6EAEFH)
                       PANEL,AT(244,258,192,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(396,172,36,32),USE(?ButtonHelp),TRN,FLAT,ICON('F1Help.jpg')
                       STRING(@s255),AT(252,184,140,),USE(SRN:TipText),TRN
                       PROMPT('SRN:0000000'),AT(384,150),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Income Report (Retail) Criteria'),AT(248,150),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       GROUP('Top Tip'),AT(248,168,184,36),USE(?GroupTip),BOXED,TRN
                         SHEET,AT(244,212,192,42),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                           TAB('Criteria'),USE(?Tab1)
                             STRING('Invoiced Date From'),AT(248,218),USE(?InvoicedDateFrom),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             ENTRY(@D6b),AT(340,218,64,10),USE(LOC:StartDate),IMM,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),TIP('Enter the earliest Invoice date to show on the report'),REQ
                             BUTTON,AT(404,214),USE(?StartPopCalendar),IMM,SKIP,TRN,FLAT,LEFT,ICON('lookupp.jpg')
                             STRING('Invoiced Date To'),AT(248,234,68,8),USE(?String2:2),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                             ENTRY(@D6b),AT(340,234,64,10),USE(LOC:EndDate),IMM,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),TIP('Enter the latest Invoice date'),REQ
                             PROMPT('Report Version'),AT(248,244),USE(?ReportVersion),TRN,FONT(,7,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                             BUTTON,AT(404,230),USE(?EndPopCalendar),IMM,SKIP,TRN,FLAT,LEFT,ICON('lookupp.jpg')
                             CHECK('Show Excel'),AT(248,266),USE(excel:Visible),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),TIP('Tick To Show Excel During Report Creation.<13,10>Only Tick if you are having problems' &|
   ' <13,10>with Excel reports.'),VALUE('1','0')
                           END
                           TAB('Repair Categories'),USE(?Tab2)
                           END
                         END
                         BUTTON,AT(304,258),USE(?OkButton),TRN,FLAT,LEFT,TIP('Click to print report'),ICON('printp.jpg'),DEFAULT
                         BUTTON,AT(368,258),USE(?CancelButton),TRN,FLAT,LEFT,TIP('Click to close this form'),ICON('cancelp.jpg')
                       END
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
    Map
DateToString        PROCEDURE( DATE ), STRING
GetHeadAccount PROCEDURE( STRING )
GetInvoiceType      PROCEDURE( STRING ), STRING
GetSubAccount PROCEDURE( STRING )
GetVATRate      PROCEDURE( STRING ), REAL
LoadINVOICE     PROCEDURE( DATE, DATE, *LONG ), LONG ! BOOL
LoadRETPAY                   PROCEDURE( LONG, *LONG ), LONG ! BOOL
LoadRETSALES                   PROCEDURE( LONG, *LONG ), LONG ! BOOL
LoadRETSTOCK            PROCEDURE( LONG, *LONG ), LONG ! BOOL
LoadSTOCK       PROCEDURE( LONG ), LONG ! BOOL
LoadSUBTRACC   PROCEDURE( STRING ), LONG ! BOOL
LoadTRADEACC   PROCEDURE( STRING ), LONG ! BOOL
LoadVATCODE     PROCEDURE( STRING ), LONG ! BOOL
LoadWEBJOB      PROCEDURE( LONG ), LONG ! BOOL
LocalUseEuro            Procedure(String    func:AccountNumber),Byte
NumberToColumn PROCEDURE( LONG ), STRING
WriteColumn PROCEDURE( STRING, LONG=False )
WriteDebug      PROCEDURE( STRING )
    END !MAP
! Dimensions for the Accumulators(3, 3, 3)

OrderDimension     EQUATE(1)
BackorderDimension EQUATE(2)
InvoiceDimension   EQUATE(3)

PartsDimension     EQUATE(1)
AccessoryDimension EQUATE(2)
TotalDimension     EQUATE(3)

CostDimension      EQUATE(1)
TradeDimension     EQUATE(2)
RetailDimension    EQUATE(3)

!---   ProgressBar Declarations   --------------------------------------------------
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte
 
progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY,DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
!-----------------------------------------------------------------------------------
!---   Excel EQUATES   -------------------------------------------------------------
xlCalculationManual    EQUATE(0FFFFEFD9h) ! XlCalculation
xlCalculationAutomatic EQUATE(0FFFFEFF7h) ! XlCalculation
!----------------------------------------------------

!----------------------------------------------------
xlNone       EQUATE(0FFFFEFD2h)
xlContinuous EQUATE(        1 ) ! xlFillStyle
xlThin       EQUATE(        2 ) ! XlBorderWeight
!----------------------------------------------------

!----------------------------------------------------
xlAutomatic   EQUATE(0FFFFEFF7h) ! Constants.
xlSolid       EQUATE(        1 ) ! Constants.
xlLeft        EQUATE(0FFFFEFDDh) ! Constants.
xlRight       EQUATE(0FFFFEFC8h) ! Constants.
xlCenter      EQUATE(0FFFFEFF4h) ! Constants.
xlLastCell    EQUATE(       11 ) ! Constants.
xlTop         EQUATE(0FFFFEFC0h) ! Constants.
xlBottom      EQUATE(0FFFFEFF5h) ! Constants.

xlTopToBottom EQUATE(        1 ) ! Constants.
!----------------------------------------------------

!----------------------------------------------------
xlDiagonalDown     EQUATE( 5) ! XlBordersIndex
xlDiagonalUp       EQUATE( 6) ! XlBordersIndex
xlEdgeLeft         EQUATE( 7) ! XlBordersIndex
xlEdgeTop          EQUATE( 8) ! XlBordersIndex
xlEdgeBottom       EQUATE( 9) ! XlBordersIndex
xlEdgeRight        EQUATE(10) ! XlBordersIndex
xlInsideVertical   EQUATE(11) ! XlBordersIndex
xlInsideHorizontal EQUATE(12) ! XlBordersIndex
!----------------------------------------------------

!----------------------------------------------------
xlA1         EQUATE(        1 ) ! XlReferenceStyle
xlR1C1       EQUATE(0FFFFEFCAh) ! XlReferenceStyle
!----------------------------------------------------

!----------------------------------------------------
xlFilterCopy    EQUATE(2) ! XlFilterAction
xlFilterInPlace EQUATE(1) ! XlFilterAction
!----------------------------------------------------

!----------------------------------------------------
xlGuess EQUATE(0) ! XlYesNoGuess
xlYes   EQUATE(1) ! XlYesNoGuess
xlNo    EQUATE(2) ! XlYesNoGuess
!----------------------------------------------------

!----------------------------------------------------
xlAscending  EQUATE(1) ! XlSortOrder
xlDescending EQUATE(2) ! XlSortOrder
!----------------------------------------------------

!----------------------------------------------------
xlLandscape EQUATE(2) ! XlPageOrientation
xlPortrait  EQUATE(1) ! XlPageOrientation
!----------------------------------------------------

!----------------------------------------------------
xlDownThenOver EQUATE( 1 ) ! XlOrder
xlOverThenDown EQUATE( 2 ) ! XlOrder
!-----------------------------------------------------------------------------------
!! Excel EQUATES
!
!!----------------------------------------------------
!xlCalculationManual    EQUATE(0FFFFEFD9h) ! XlCalculation
!xlCalculationAutomatic EQUATE(0FFFFEFF7h) ! XlCalculation
!!----------------------------------------------------
!
!!----------------------------------------------------
!xlNone       EQUATE(0FFFFEFD2h)
!xlContinuous EQUATE(        1 ) ! xlFillStyle
!xlThin       EQUATE(        2 ) ! XlBorderWeight
!!----------------------------------------------------
!
!!----------------------------------------------------
!xlAutomatic  EQUATE(0FFFFEFF7h) ! Constants.
!xlSolid      EQUATE(        1 ) ! Constants.
!xlLeft       EQUATE(0FFFFEFDDh) ! Constants.
!xlRight      EQUATE(0FFFFEFC8h) ! Constants.
!xlLastCell   EQUATE(       11 ) ! Constants.
!
!xlTopToBottom EQUATE(        1 ) ! Constants.
!
!xlCenter     EQUATE(0FFFFEFF4h) ! Excel.Constants
!xlBottom     EQUATE(0FFFFEFF5h) ! Excel.Constants
!!----------------------------------------------------
!
!!----------------------------------------------------
!xlDiagonalDown     EQUATE( 5) ! XlBordersIndex
!xlDiagonalUp       EQUATE( 6) ! XlBordersIndex
!xlEdgeLeft         EQUATE( 7) ! XlBordersIndex
!xlEdgeTop          EQUATE( 8) ! XlBordersIndex
!xlEdgeBottom       EQUATE( 9) ! XlBordersIndex
!xlEdgeRight        EQUATE(10) ! XlBordersIndex
!xlInsideVertical   EQUATE(11) ! XlBordersIndex
!xlInsideHorizontal EQUATE(12) ! XlBordersIndex
!!----------------------------------------------------
!
!!----------------------------------------------------
!xlA1         EQUATE(        1 ) ! XlReferenceStyle
!xlR1C1       EQUATE(0FFFFEFCAh) ! XlReferenceStyle
!!----------------------------------------------------
!
!!----------------------------------------------------
!xlFilterCopy    EQUATE(2) ! XlFilterAction
!xlFilterInPlace EQUATE(1) ! XlFilterAction
!!----------------------------------------------------
!
!!----------------------------------------------------
!xlGuess EQUATE(0) ! XlYesNoGuess
!xlYes   EQUATE(1) ! XlYesNoGuess
!xlNo    EQUATE(2) ! XlYesNoGuess
!!----------------------------------------------------
!
!!----------------------------------------------------
!xlAscending  EQUATE(1) ! XlSortOrder
!xlDescending EQUATE(2) ! XlSortOrder
!!----------------------------------------------------
!
!!----------------------------------------------------
!xlLandscape EQUATE(2) ! XlPageOrientation
!xlPortrait  EQUATE(1) ! XlPageOrientation
!!----------------------------------------------------
!
!!----------------------------------------------------
!xlDownThenOver EQUATE( 1 ) ! XlOrder
!xlOverThenDown EQUATE( 2 ) ! XlOrder
!!----------------------------------------------------
!
!---   MS Office EQUATES   ---------------------------------------------------------
!----------------------------------------------------
msoCTrue          EQUATE(        1 ) ! MsoTriState
msoFalse          EQUATE(        0 ) ! MsoTriState
msoTriStateMixed  EQUATE(0FFFFFFFEh) ! MsoTriState
msoTriStateToggle EQUATE(0FFFFFFFDh) ! MsoTriState
msoTrue           EQUATE(0FFFFFFFFh) ! MsoTriState
!----------------------------------------------------

!----------------------------------------------------
msoScaleFromBottomRight EQUATE(2) ! MsoScaleFrom
msoScaleFromMiddle      EQUATE(1) ! MsoScaleFrom
msoScaleFromTopLeft     EQUATE(0) ! MsoScaleFrom
!----------------------------------------------------

!----------------------------------------------------
msoPropertyTypeBoolean EQUATE(2) ! MsoDocProperties
msoPropertyTypeDate    EQUATE(3) ! MsoDocProperties
msoPropertyTypeFloat   EQUATE(5) ! MsoDocProperties
msoPropertyTypeNumber  EQUATE(1) ! MsoDocProperties
msoPropertyTypeString  EQUATE(4) ! MsoDocProperties
!----------------------------------------------------
!-----------------------------------------------------------------------------------
CommandLine STRING(255)
tmpPos      LONG
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!-----------------------------------------------
OKButton_Pressed            ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        CancelPressed = False
        debug:Active  = False

        !-----------------------------------------------
        ! 23 Oct 2002 John
        ! R003, berrjo :Insert the report exe name EG VODR56.V01
        ! Do not attach the EXE extenstion but show the version number.  Increment the version number each time changed.
        ! Make this standard on all reports sent for correction.
        ! LOC:Version STRING(8) 3.1.0000 3.1 to match sevicebase version (and dictionary) .0000 increment before each compile
        !
        !-----------------------------------------------

        DO ExportSetup
        DO ExportBody
        DO ExportFinalize

        If Command('/SCHEDULE')
            If Access:REPSCHLG.PrimeRecord() = Level:Benign
                rlg:REPSCHEDRecordNumber = rpd:RecordNumber
                rlg:Information = 'Report Name: ' & Clip(rpd:ReportName) & ' - ' & Clip(rpd:ReportCriteriaType) & |
                                  '<13,10>Report Finished'
                If Access:REPSCHLG.TryInsert() = Level:Benign
                    !Insert
                Else ! If Access:REPSCHLG.TryInsert() = Level:Benign
                    Access:REPSCHLG.CancelAutoInc()
                End ! If Access:REPSCHLG.TryInsert() = Level:Benign
            End ! If Access.REPSCHLG.PrimeRecord() = Level:Benign
        ENd


        POST(Event:CloseWindow)
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
ExportSetup                             ROUTINE
    DATA
temp DATE
    CODE
        !-----------------------------------------------------------------
        Do GetFileName
        IF CLIP(LOC:FileName) = ''
          DO LoadFileDialog
        END !IF

        IF LOC:StartDate > LOC:EndDate
            temp          = LOC:EndDate
            LOC:EndDate   = LOC:StartDate
            LOC:StartDate = temp
        END !IF

        IF LOC:FileName = ''
            Case Missive('Invalid Filename.','ServiceBase 3g',|
                           'mstop.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive

            CancelPressed = True

            EXIT
        END !IF LOC:FileName = ''

        IF LOWER(RIGHT(LOC:FileName, 4)) <> '.xls'
            LOC:FileName = CLIP(LOC:FileName) & '.xls'
        END !IF
        !-----------------------------------------------------------------
        SETCURSOR(CURSOR:Wait)

        DO ProgressBar_Setup
        !-----------------------------------------------------------------
        DO XL_Setup

        DO XL_AddWorkbook
        !-----------------------------------------------------------------
    EXIT
!ExportSetup                             ROUTINE
!    DATA
!temp DATE
!    CODE
!        !-----------------------------------------------------------------
!        SETCURSOR(CURSOR:Wait)
!        CancelPressed = False
!
!        IF LOC:StartDate > LOC:EndDate
!            temp          = LOC:EndDate
!            LOC:EndDate   = LOC:StartDate
!            LOC:StartDate = temp
!        END !IF
!        !-----------------------------------------------------------------
!        IF CLIP(LOC:FileName) = ''
!            DO LoadFileDialog
!        END !IF
!
!        IF LOC:FileName = ''
!            Case MessageEx('No filename chosen.<10,13>   Enter a filename then try again','ServiceBase 2000',|
!                           'Styles\warn.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
!                Of 1 ! &OK Button
!
!            End!Case MessageEx
!
!            SETCURSOR()
!
!            EXIT
!        END !IF LOC:FileName = ''
!
!        IF LOWER(RIGHT(LOC:FileName, 4)) <> '.xls'
!            LOC:FileName = CLIP(LOC:FileName) & '.xls'
!        END !IF
!        !-----------------------------------------------------------------
!        DO ProgressBar_Setup
!        !-----------------------------------------------------------------
!        Excel = Create(0, Create:OLE)
!        Excel{PROP:Create} = 'Excel.Application'
!
!        Excel{'Application.Calculation'}    = xlCalculationManual
!        Excel{'Application.ScreenUpdating'} = tmp:ShowExcel
!        Excel{'Application.Visible'}        = tmp:ShowExcel
!
!        DO XL_AddWorkbook
!        !-----------------------------------------------------------------
!    EXIT
ExportBody                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        DO CreateTitleSheet
        DO CreateDataSheet
        DO WriteHeadSummary
        !-----------------------------------------------------------------
    EXIT
ExportFinalize                          ROUTINE
    DATA
FilenameLength LONG
StartAt        LONG
SUBLength      LONG
    CODE
        !-----------------------------------------------------------------
        DO ResetClipboard

        IF CancelPressed
            DO XL_DropAllWorksheets
            DO XL_Finalize

            EXIT
        END !IF
        !-----------------------------------------------------------------
        Excel{'Sheets("Sheet3").Select'}
        Excel{'ActiveWindow.SelectedSheets.Delete'}

        Excel{'Sheets("Summary").Select'}
        Excel{'Range("A1").Select'}

        Excel{'Application.ActiveWorkBook.SaveAs("' & LEFT(CLIP(LOC:FileName)) & '")'}
        Excel{'Application.ActiveWorkBook.Close()'}
        !-----------------------------------------------------------------
        DO XL_Finalize

        DO ProgressBar_Finalise
        !-----------------------------------------------------------------
        SETCURSOR()
        IF Automatic = FALSE
          IF MATCH(LOC:Filename, CLIP(LOC:DesktopPath) & '*')
              FilenameLength = LEN(CLIP(LOC:Filename   ))
              StartAt        = LEN(CLIP(LOC:DesktopPath)) + 1
              SUBLength      = FilenameLength - StartAt + 1

          ELSE
          END !IF
        END
        !-----------------------------------------------------------------
    EXIT
!ExportFinalize                          ROUTINE
!    DATA
!    CODE
!        !-----------------------------------------------------------------
!        IF CancelPressed
!            Excel{'Application.ActiveWorkBook.Close()'}
!            SETCURSOR()
!            Excel{PROP:DEACTIVATE}
!
!            EXIT
!        END !IF
!        !-----------------------------------------------------------------
!        Excel{'Range("A1").Select'}
!
!        Excel{'Application.Calculation'}    = xlCalculationAutomatic
!        Excel{'Application.ScreenUpdating'} = False
!        Excel{'Application.Visible'}        = False
!
!        DO XL_SetWorksheetLandscape
!        !-----------------------------------------------------------------
!        ! 4 Dec 2001 John
!        ! Delete empty sheets
!        ! Show summary on entry
!        !
!        Excel{'Application.DisplayAlerts'} = False  ! no alerts to the user (do you want to save the file?) ! MOVED 10 BDec 2001 John
!
!        Excel{'Sheets("Sheet2").Select'}
!        Excel{'ActiveWindow.SelectedSheets.Delete'}
!        Excel{'Sheets("Sheet3").Select'}
!        Excel{'ActiveWindow.SelectedSheets.Delete'}
!        Excel{'Sheets("Summary").Select'}
!        !-----------------------------------------------------------------
!        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Title")'}            = CLIP(LOC:ProgramName)
!        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Author")'}           = CLIP(LOC:UserName)
!        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Application Name")'} = 'ServiceBase 2000'
!
!        Excel{'Application.ActiveWorkBook.SaveAs("' & LEFT(CLIP(LOC:FileName)) & '")'}
!        Excel{'Application.ActiveWorkBook.Close()'}
!
!        Excel{PROP:DEACTIVATE}
!        !-----------------------------------------------------------------
!        DO ProgressBar_Finalise
!
!        SETCURSOR()
!
!        Case MessageEx('Export Completed.', |
!                     'ServiceBase 2000',    |
!                     'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
!        Of 1 ! &OK Button
!        End!Case MessageEx
!        !-----------------------------------------------------------------
!    EXIT
!-----------------------------------------------
CreateTitleSheet                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        Excel{'ActiveWorkBook.Sheets("Sheet1").Select'}
        !-----------------------------------------------------------------
        LOC:Text          = 'Summary'
        sheet:TempLastCol = sheet:HeadLastCol

        DO CreateWorksheetHeader
        !-----------------------------------------------------------------
        DO XL_SetWorksheetPortrait

        Excel{'ActiveSheet.Columns("A:A").ColumnWidth'}                         = 20
        Excel{'ActiveSheet.Columns("B:B").ColumnWidth'}                         = 20
        Excel{'ActiveSheet.Columns("C:' & sheet:HeadLastCol & '").ColumnWidth'} = 12
        !-----------------------------------------------------------------
        Excel{'Range("A' & sheet:HeadSummaryRow & '").Select'}
            DO XL_SetBold
            Excel{'ActiveCell.Formula'} = 'Summary'

        Excel{'Range("A' & sheet:HeadSummaryRow & ':' & sheet:HeadLastCol & sheet:HeadSummaryRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------------------------
        ! Prepare For summary details
        !
        Excel{'Range("A' & sheet:HeadSummaryRow+1 & '").Select'}
        !-----------------------------------------------------------------
    EXIT
CreateDataSheet                                             ROUTINE
    DATA
First LONG(True)
    CODE
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        DO XL_AddSheet
            DO XL_SetWorksheetLandscape
        !-----------------------------------------------------------------
        DO InitColumns

        LOC:SectionName   = 'Detailed'
        LOC:Text = CLIP(LOC:SectionName)

        excel:CommentText = ''
        sheet:TempLastCol = sheet:DataLastCol

        DO CreateSectionHeader
        !-----------------------------------------------------------------
        CountOrders    = 0
        CountLines     = 0
        CountParts     = 0
        CountDelivered = 0
        !------------------------------------------
        Progress:Text    = CLIP(LOC:SectionName)
        RecordsToProcess = 1000 * (LOC:EndDate - LOC:StartDate + 1)

        DO ProgressBar_LoopPre
        !-----------------------------------------------------------------
        WriteDebug('CreateDataSheet')

        LOOP WHILE LoadINVOICE(LOC:StartDate, LOC:EndDate, First)
            !-------------------------------------------------------------
            DO ProgressBar_Loop

            IF CancelPressed
                BREAK
            END !IF
            !-------------------------------------------------------------
            CountOrders += 1

            DO WriteColumns
            !-------------------------------------------------------------
        END !LOOP

        DO ProgressBar_LoopPost
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        DO WriteDataSummary
        !-----------------------------------------------------------------
    EXIT

!-----------------------------------------------
CreateSectionHeader                                        ROUTINE
    DATA
LastColumn STRING(1)
    CODE
        !-----------------------------------------------       
        DO CreateWorksheetHeader
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataSectionRow & ':' & sheet:DataLastCol & sheet:DataSectionRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A' & sheet:DataSectionRow & '").Select'}
            Excel{'ActiveCell.Formula'}         = 'Section Name:'
            DO XL_ColRight
                Excel{'ActiveCell.Formula'}     = CLIP(LOC:SectionName)
                DO XL_SetBold
                DO XL_HorizontalAlignmentLeft

        Excel{'Range("E' & sheet:DataSectionRow & '").Select'}
            Excel{'ActiveCell.Formula'}         = 'Total Records:'
            DO XL_HorizontalAlignmentRight
            DO XL_ColRight
                Excel{'ActiveCell.Formula'}     = 0
                DO XL_HorizontalAlignmentLeft
        !-----------------------------------------------
        DO SetColumns

        Excel{'Range("A' & sheet:DataHeaderRow+1 & '").Select'}
        !-----------------------------------------------
    EXIT
CreateWorksheetHeader                                                               ROUTINE
    DATA
CurrentRow LONG(3)
    CODE
        !-----------------------------------------------       
        Excel{'ActiveSheet.Name'}                      = LEFT(CLIP(LOC:Text), 30)
        !-----------------------------------------------       
        Excel{'Range("A1").Select'}
            Excel{'ActiveCell.Formula'}                = CLIP(LOC:ProgramName)
            DO XL_SetBold
            Excel{'ActiveCell.Font.Size'}              = 16

        Excel{'Range("D3").Select'}
            Excel{'ActiveCell.Formula'}   = Clip(tmp:VersionNumber)
            DO XL_SetBold
            Excel{'ActiveCell.Font.Size'} = 8

        Excel{'Range("A1:' & sheet:TempLastCol & '1").Select'}
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------
        Excel{'Range("A' & CurrentRow & ':' & sheet:TempLastCol & CurrentRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A' & CurrentRow & '").Select'}
            Excel{'ActiveCell.Formula'}                = 'Criteria'
            DO XL_SetBold
        !-----------------------------------------------
        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.FormulaR1C1'}        = 'Invoice Date From:' ! 25 Sep 2002 John from "Ordered Date To:"
                DO XL_ColRight
                    DO XL_FormatDate
                    DO XL_HorizontalAlignmentLeft
                    Excel{'ActiveCell.Formula'}        = DateToString(LOC:StartDate)

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.FormulaR1C1'}        = 'Invoice Date To:' ! 25 Sep 2002 John from "Ordered Date To:"
                DO XL_ColRight
                    DO XL_FormatDate
                    DO XL_HorizontalAlignmentLeft
                    Excel{'ActiveCell.Formula'}        = DateToString(LOC:EndDate)

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.Formula'}            = 'Created By:'
                DO XL_ColRight
                    Excel{'ActiveCell.Formula'}        = LOC:UserName

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.Formula'}            = 'Date Created:'
                DO XL_ColRight
                    DO XL_FormatDate
                    DO XL_HorizontalAlignmentLeft
                    Excel{'ActiveCell.Formula'}        = DateToString(TODAY())
        !-----------------------------------------------
        Excel{'Range("A4:' & sheet:TempLastCol & CurrentRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
InitColumns                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        FREE(HeaderQueue)

        head:ColumnName       = 'Invoice Number'
            head:ColumnWidth  = 15.00
            head:NumberFormat = '###0'
            ADD(HeaderQueue)

        head:ColumnName       = 'Invoice Date'
            head:ColumnWidth  = 13.00
            head:NumberFormat = excel:DateFormat
            ADD(HeaderQueue)

        head:ColumnName       = 'Invoice Type'
            head:ColumnWidth  = 15.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        !-----------------------------------------------
        ! 25 Sep 2002 John, Add column
        head:ColumnName       = 'Sale Number'
            head:ColumnWidth  = 12.00
            head:NumberFormat = '###0'
            ADD(HeaderQueue)
        !-----------------------------------------------

        !-----------------------------------------------
        ! 23 Sep 2002 John, Display all account details
        head:ColumnName       = 'Account Number'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Account Name'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)
        !-----------------------------------------------

        !-----------------------------------------------
        ! 23 Sep 2002 John, Remove column
!        head:ColumnName       = 'Courier'
!            head:ColumnWidth  = 20.00
!            head:NumberFormat = '#,##0.00'
!            ADD(HeaderQueue)
        !-----------------------------------------------

        !-----------------------------------------------
        ! 23 Sep 2002 John, Remove column
!        head:ColumnName       = 'Labour'
!            head:ColumnWidth  = 11.00
!            head:NumberFormat = '#,##0.00'
!            ADD(HeaderQueue)
        !-----------------------------------------------

        !-----------------------------------------------
        ! 25 Sep 2002 John, Rename column "Parts - COS"
        head:ColumnName       = 'Parts Cost'
            head:ColumnWidth  = 11.00
            head:NumberFormat = '#,##0.00'
            ADD(HeaderQueue)
        !-----------------------------------------------

        !-----------------------------------------------
        ! 25 Sep 2002 John, Rename column "Parts - Sales"
        head:ColumnName       = 'Parts Sales'
            head:ColumnWidth  = 11.00
            head:NumberFormat = '#,##0.00'
            ADD(HeaderQueue)
        !-----------------------------------------------

        head:ColumnName       = 'VAT'
            head:ColumnWidth  = 11.00
            head:NumberFormat = '#,##0.00'
            ADD(HeaderQueue)

        head:ColumnName       = 'Total'
            head:ColumnWidth  = 11.00
            head:NumberFormat = '#,##0.00'
            ADD(HeaderQueue)

        !-----------------------------------------------
        ! 25 Sep 2002 John, Add column "Paid"
        head:ColumnName       = 'Paid'
            head:ColumnWidth  = 11.00
            head:NumberFormat = '#,##0.00'
            ADD(HeaderQueue)
        !-----------------------------------------------

        !-----------------------------------------------
        ! 25 Sep 2002 John, Add column "Outstanding"
        head:ColumnName       = 'Outstanding'
            head:ColumnWidth  = 11.00
            head:NumberFormat = '#,##0.00'
            ADD(HeaderQueue)
        !-----------------------------------------------

        !-----------------------------------------------
        sheet:DataLastCol = NumberToColumn(RECORDS(HeaderQueue))
        !-----------------------------------------------
    EXIT

!
!        head:ColumnName       = 'Account Number'
!            head:ColumnWidth  = 20.00
!            head:NumberFormat = chr(64) ! Text (AT) Sign
!            ADD(HeaderQueue)
!
!        head:ColumnName       = 'Account Name'
!            head:ColumnWidth  = 20.00
!            head:NumberFormat = chr(64) ! Text (AT) Sign
!            ADD(HeaderQueue)
!
!        head:ColumnName       = 'Part Number'
!            head:ColumnWidth  = 11.00
!            head:NumberFormat = chr(64) ! Text (AT) Sign
!            ADD(HeaderQueue)
!
!        head:ColumnName       = 'Description'
!            head:ColumnWidth  = 20.00
!            head:NumberFormat = chr(64) ! Text (AT) Sign
!            ADD(HeaderQueue)
!
!        head:ColumnName       = 'Quanitity'
!            head:ColumnWidth  = 11.00
!            head:NumberFormat = '###0'
!            ADD(HeaderQueue)
!
!        head:ColumnName       = 'Spares Sales Value'
!            head:ColumnWidth  = 11.00
!            head:NumberFormat = '#,##0.00'
!            ADD(HeaderQueue)
!
!        head:ColumnName       = 'Spares Sales At Cost'
!            head:ColumnWidth  = 11.00
!            head:NumberFormat = '#,##0.00'
!            ADD(HeaderQueue)
!
!        head:ColumnName       = 'Accessory Sales Value'
!            head:ColumnWidth  = 11.00
!            head:NumberFormat = '#,##0.00'
!            ADD(HeaderQueue)
!
!        head:ColumnName       = 'Accessory Sales At Cost'
!            head:ColumnWidth  = 11.00
!            head:NumberFormat = '#,##0.00'
!            ADD(HeaderQueue)
!
!        head:ColumnName       = 'Accessory Exchange Value'
!            head:ColumnWidth  = 11.00
!            head:NumberFormat = '#,##0.00'
!            ADD(HeaderQueue)
!
!        head:ColumnName       = 'Spare Parts Backorder Value'
!            head:ColumnWidth  = 11.00
!            head:NumberFormat = '#,##0.00'
!            ADD(HeaderQueue)
!
!        head:ColumnName       = 'Accessory Backorder Value'
!            head:ColumnWidth  = 11.00
!            head:NumberFormat = '#,##0.00'
!            ADD(HeaderQueue)
!
!        head:ColumnName       = 'Accessory Exchange Backorder Value'
!            head:ColumnWidth  = 11.00
!            head:NumberFormat = '#,##0.00'
!            ADD(HeaderQueue)
!
!        head:ColumnName       = 'Use Euro'
!            head:ColumnWidth  = 11.00
!            head:NumberFormat = '###0'
!            head:NumberFormat = chr(64) ! Text (AT) Sign
!            ADD(HeaderQueue)
FormatColumns                                                   ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF RecordCount < 1
            EXIT
        END !IF
        !-----------------------------------------------
        LOOP x# = 1 TO RECORDS(HeaderQueue)
            !-------------------------------------------
            GET(HeaderQueue, x#)
            IF ERRORCODE()
                CYCLE
            END !IF

            Excel{'Range("' & CHR(64+x#) & sheet:DataHeaderRow+1 & ':' & CHR(64+x#) & sheet:DataHeaderRow+RecordCount & '").Select'}
                Excel{'Selection.NumberFormat'} = head:NumberFormat
                !DO XL_HighLight ! DEBUG
                DO XL_HorizontalAlignmentRight
            !-------------------------------------------
        END !LOOP
        !-----------------------------------------------
    EXIT
SetColumns                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataHeaderRow & ':' & sheet:DataLastCol & sheet:DataHeaderRow & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & sheet:DataHeaderRow  & '").Select'}
        !-----------------------------------------------
        LOOP x# = 1 TO RECORDS(HeaderQueue)
            GET(HeaderQueue, x#)

            Excel{'ActiveCell.Formula'}         = head:ColumnName
                Excel{'ActiveCell.ColumnWidth'} = head:ColumnWidth
                DO XL_ColRight
        END !LOOP
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataHeaderRow+1  & '").Select'}
        Excel{'ActiveWindow.FreezePanes'} = True
        !-----------------------------------------------
    EXIT
WriteColumns                                                       ROUTINE
    DATA
Paid REAL
    CODE
        !-----------------------------------------------
        WriteDebug('WriteColumns')

        IF inv:Invoice_Type <> 'RET' ! Retail
            EXIT
        END !IF
        !===============================================
        DO CalculateSalesValues

        RecordCount += 1

        WriteColumn( inv:Invoice_Number, True            ) ! Invoice Number
        WriteColumn( DateToString(inv:Date_Created)      ) ! Date Issued
        WriteColumn( GetInvoiceType(inv:Invoice_Type)    ) ! Invoice Type
        WriteColumn( ret:Ref_Number                      ) ! Sale Number

        !-----------------------------------------------
        ! 23 Sep 2002 John, Display all account details
        WriteColumn( acQ:AccountNumber ) ! Client
        WriteColumn( acQ:AccountName   ) ! Client
        !-----------------------------------------------

        WriteColumn( calc:Parts_COS                      ) ! Parts Cost
        WriteColumn( calc:Parts                          ) ! Parts Sales

        WriteColumn( calc:VAT                            ) ! VAT
        WriteColumn( calc:Total                          ) ! Total

        WriteColumn( calc:Paid                           ) ! Paid
        WriteColumn( calc:Total - calc:Paid              ) ! Outstanding
        !-----------------------------------------------

        IF excel:OperatingSystem < 5
            DO PassViaClipboard
        END !IF
        !-----------------------------------------------
        DO XL_ColFirst
        DO XL_RowDown
        !-----------------------------------------------
    EXIT


                                                           ! inv:Currency
!        WriteColumn( calc:Courier                        ) ! Courier
!        WriteColumn( calc:Labour                         ) ! Labour
!        WriteColumn( calc:Parts_COS                      ) ! Parts - COS
!        WriteColumn( calc:Parts / calc:TotalVolume       ) ! Parts - Sales (Weighted Average)


!        DO WriteColumn_SaleNumber                      ! Sale Number
!        DO WriteColumn_AccountNumber                   ! Account Number
!        DO WriteColumn_AccountName                     ! Account Name
!        DO WriteColumn_PartNumber                      ! Part Number
!        DO WriteColumn_Description                     ! Description
!        DO WriteColumn_Quantity                        ! Quanitity
!        DO WriteColumn_SparesSalesValue                ! Spares Sales Value
!        DO WriteColumn_SparesSalesAtCost               ! Spares Sales At Cost
!        DO WriteColumn_AccessorySalesValue             ! Accessory Sales Value
!        DO WriteColumn_AccessorySalesCost              ! Accessory Sales At Cost
!        DO WriteColumn_AccessoryExchangeValue          ! Accesory Excel Value
!        DO WriteColumn_SparePartsBackorderValue        ! Spare Parts Backorder Value
!        DO WriteColumn_AccessoryBackOrderValue         ! Accessory Backorder Value
!        DO WriteColumn_AccessoryExchangeBackorderValue ! Accessory Exchange Backorder V
!        DO WriteColumn_UseEuro                         ! Use Euro
!-----------------------------------------------
CalculateSalesValues                                ROUTINE !
    DATA
RetSales_First LONG(True)
RetStock_First LONG(True)
    CODE
        !-----------------------------------------------
        WriteDebug('CalculateSalesValues(' & inv:Invoice_Number &  ')')

        DO SetupAccount
        !-----------------------------------------------
        Clear(Calc_Group)

        calc:VATRate              =  (inv:Vat_Rate_Retail / 100)
        WriteDebug('CalculateSalesValues(inv:Vat_Rate_Retail="' & inv:Vat_Rate_Retail & '", calc:VATRate="' & calc:VATRate & '")')

        IF LoadRETSALES(inv:Invoice_Number, RetSales_First)
            WriteDebug('CalculateSalesValues LOOP LoadRETSALES(Ref_Number="' & ret:Ref_Number & '", ' & RetSales_First & ')')
            DO CalcPaid

            calc:Courier         += ret:Invoice_Courier_Cost
            calc:CountLines      += 1

            RetStock_First        = True
            LOOP WHILE LoadRETSTOCK(ret:Ref_Number, RetStock_First)
                RetStock_First    = False
                WriteDebug('CalculateSalesValues LOOP LoadRETSTOCK(Ref_Number="' & res:Ref_Number & '", ' & RetStock_First & ')')

                IF res:Despatched <> 'YES'
                    WriteDebug('CalculateSalesValues LOOP LoadRETSTOCK(Ref_Number="' & res:Ref_Number & '", ' & RetStock_First & ') CYCLE="' & CLIP(res:Despatched) & '"')

                    CYCLE
                END !IF

                calc:Parts_COS   += res:Purchase_Cost * res:Quantity
                calc:Parts       += res:Item_Cost     * res:Quantity
                
                calc:VAT         += (res:Item_Cost    * res:Quantity) * calc:VATRate

                calc:TotalVolume += res:Quantity
                calc:CountParts  += 1
                IF (res:LoanRefNumber <> '') ! #12341 Count loan units if this is a loan order sale (DBH: 31/01/2012)
                    calc:CountLoans += 1
                END
            END !LOOP

        END !IF

        calc:Labour               = 0 ! no labour on retail invoices

        calc:Total                = calc:Courier + calc:Labour + calc:Parts + calc:VAT
        !-----------------------------------------------
        DO UpdateAccount
        !-----------------------------------------------
    EXIT

!CalculateSalesValues                                ROUTINE !
!    DATA
!    CODE
!        !-----------------------------------------------
!        WriteDebug('CalculateSalesValues(' & inv:Invoice_Number &  ')')
!
!        DO SetupAccount
!            !-------------------------------------------
!            Clear(Calc_Group)
!
!            calc:Courier = job:WInvoice_Courier_Cost
!            calc:Parts   = job:WInvoice_Parts_Cost
!            calc:Labour  = job:WInvoice_Labour_Cost
!
!            DO CalcParts ! calc:Parts_COS
!
!            calc:VAT += calc:Courier * (inv:Vat_Rate_Labour / 100)
!            calc:VAT += calc:Parts   * (inv:Vat_Rate_Parts  / 100)
!            calc:VAT += calc:Labour  * (inv:Vat_Rate_Labour / 100)
!
!            calc:Total = calc:Courier + calc:Labour + calc:Parts + calc:VAT
!            !-------------------------------------------
!        DO UpdateAccount
!        !-----------------------------------------------
!    EXIT

CalcParts                                                ROUTINE
    DATA
RetSales_First LONG(True)
RetStock_First LONG(True)
    CODE
        !-----------------------------------------------
        WriteDebug('CalcParts')
        !-----------------------------------------------
        RetSales_First = True
        LOOP WHILE LoadRETSALES(inv:Invoice_Number, RetSales_First)
            WriteDebug('CalculateSalesValues LOOP LoadRETSALES(Ref_Number="' & ret:Ref_Number & '", ' & RetSales_First & ')')

            RetSales_First        = False

            RetStock_First        = True
            LOOP WHILE LoadRETSTOCK(ret:Ref_Number, RetStock_First)
                WriteDebug('CalculateSalesValues LOOP LoadRETSTOCK(Ref_Number="' & res:Ref_Number & '", ' & RetStock_First & ')')

                RetStock_First    = False

                calc:Parts_COS   += res:Purchase_Cost * res:Quantity
            END !LOOP

        END !LOOP
        !-----------------------------------------------
    EXIT
CalcPaid                                                  ROUTINE
    DATA
RetPay_First LONG(True)
    CODE
        !-----------------------------------------------
        WriteDebug('CalcPaid')
        !-----------------------------------------------
        calc:Paid        = 0

        LOOP WHILE LoadRETPAY(ret:Ref_Number, RetPay_First)
            WriteDebug('CalcPaid(Ref_Number="' & ret:Ref_Number & '", ' & RetPay_First & ')')

            RetPay_First = False

            calc:Paid   += rtp:amount

        END !LOOP
        !-----------------------------------------------
    EXIT
SetupAccount            ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------  
        WriteDebug('SetupAccount(AccountType="' & CLIP(inv:AccountType) & '", AccountNumber="' & CLIP(inv:Account_Number) & '")')
        SORT(Account_Queue, +acQ:AccountNumber)
        acQ:AccountNumber  = inv:Account_Number
        GET(Account_Queue, +acQ:AccountNumber)

        CASE ERRORCODE()
        OF 00 ! FOUND
            WriteDebug('SetupAccount(Found)')

        OF 30 ! NOT Found
            ! Not in queue - ADD
            WriteDebug('SetupAccount(ADD="' & CLIP(inv:Account_Number) & '")')

            CLEAR(Account_Queue)
                acQ:AccountNumber       = inv:Account_Number
                IF inv:AccountType = 'SUB'
                    IF LoadSUBTRACC(inv:Account_Number)
                        acQ:AccountName = sub:Company_Name
                    ELSE
                        acQ:AccountName = acQ:AccountNumber
                    END !IF
                ELSE
                    IF LoadTRADEACC(inv:Account_Number)
                        acQ:AccountName = tra:Company_Name
                    ELSE
                        acQ:AccountName = acQ:AccountNumber
                    END !IF
                END !IF

            ADD(Account_Queue, +acQ:AccountNumber)
        ELSE
            CancelPressed = True

            WriteDebug('SetupAccount(ERROR)')
        END !IF
        !-----------------------------------------------------------------
UpdateAccount                                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        WriteDebug('UpdateAccount(acQ:CountInvoices="' & |
                        acQ:CountInvoices & '", "' & |
                        acQ:CountLines    & '", "' & |
                        calc:CountParts   & '", "' & |
                        calc:Courier      & '", "' & |
                        calc:Labour       & '", "' & |
                        calc:Parts        & '", "' & |
                        calc:Total        & ')')

        acQ:CountInvoices += 1
        acQ:CountLines    += calc:CountLines
        acQ:CountParts    += calc:CountParts
        acQ:CountLoans         += calc:CountLoans  ! #12341 Count loan units if this is a loan order sale (DBH: 31/01/2012)

        acQ:Courier       += calc:Courier
        acQ:Labour        += calc:Labour
        acQ:Parts         += calc:Parts
        
        acQ:Total         += calc:Total

        PUT(Account_Queue)
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
PassViaClipboard                                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF clip:Saved = False
            clip:OriginalValue = CLIPBOARD()
            clip:Saved         = True
        END !IF
        !-----------------------------------------------
        SETCLIPBOARD(CLIP(clip:Value))
            Excel{'ActiveSheet.Paste()'}
        clip:Value = ''
        !-----------------------------------------------
    EXIT
ResetClipboard                                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF clip:Saved = False
            EXIT
        END !IF

        SETCLIPBOARD(clip:OriginalValue)
        
        clip:Saved = False
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
WriteDataSummary                                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        ! Summary details
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        IF RecordCount < 1
            Excel{'ActiveCell.Formula'} = 'None Found'

            EXIT
        END !IF
        !-----------------------------------------------------------------
        DO FormatColumns

        Excel{'Range("F' & sheet:DataSectionRow & '").Select'}
            DO XL_HorizontalAlignmentLeft
            Excel{'ActiveCell.Formula'}         = RecordCount

        Excel{'Range("G' & sheet:DataSectionRow & '").Select'}
            DO XL_HorizontalAlignmentRight
            Excel{'ActiveCell.Formula'}         = 'Showing'

        Excel{'Range("H' & sheet:DataSectionRow & '").Select'}
            DO XL_HorizontalAlignmentLeft
            Excel{'ActiveCell.Formula'}         = '=SUBTOTAL(2, A' & sheet:DataHeaderRow+1 & ':a' & sheet:DataHeaderRow+RecordCount & ')'
        !-----------------------------------------------------------------
        Excel{'Range("A' & sheet:DataHeaderRow+1 & '").Select'}
            DO XL_DataAutoFilter
        !-----------------------------------------------------------------
    EXIT
WriteHeadSummary                                              ROUTINE
    DATA
QueueIndex   LONG
ResultsCount LONG

TitleRow     LONG
FirstRow     LONG
LastRow      LONG
    CODE
        !-----------------------------------------------------------------
        ! summary sheet details
        !
        Excel{'ActiveWorkBook.Sheets("Summary").Select'}
        !-----------------------------------------------------------------
        ! Totals
        !
        !-----------------------------------------------
        ResultsCount = RECORDS(Account_Queue)
        TitleRow     = Excel{'ActiveCell.Row'}  ! first blank line
        FirstRow     = TitleRow + 1
        LastRow      = TitleRow + ResultsCount

        Excel{'Range("A' & TitleRow & ':' & sheet:HeadLastCol & TitleRow & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & TitleRow & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'} = 'Account Number'
            Excel{'ActiveCell.Offset(0, 1).Formula'} = 'Account Name'

            Excel{'ActiveCell.Offset(0, 2).Formula'} = 'Number Of Invoices'
            Excel{'ActiveCell.Offset(0, 3).Formula'} = 'Number Of Lines'
            Excel{'ActiveCell.Offset(0, 4).Formula'} = 'Number Of Parts'
            Excel{'ActiveCell.Offset(0, 5).Formula'} = 'Number Of Loans'

            Excel{'ActiveCell.Offset(0, 6).Formula'} = 'Total Courier'
            Excel{'ActiveCell.Offset(0, 7).Formula'} = 'Total Labour'
            Excel{'ActiveCell.Offset(0, 8).Formula'} = 'Total Parts'

            Excel{'ActiveCell.Offset(0, 9).Formula'} = 'Total'
        !-----------------------------------------------
        DO XL_RowDown
        !-----------------------------------------------------------------
        IF ResultsCount < 1
            Excel{'ActiveCell.Formula'} = 'No Accounts Found'

            EXIT
        END !IF
        !-----------------------------------------------
        SORT(Account_Queue, +acQ:AccountNumber)

        LOOP E# = 1 TO ResultsCount
            !-------------------------------------------------------------
            GET(Account_Queue, E#)

                Excel{'ActiveCell.Offset(0, 0).Formula'}             = acQ:AccountNumber
                Excel{'ActiveCell.Offset(0, 0).HorizontalAlignment'} = xlRight

                Excel{'ActiveCell.Offset(0, 1).Formula'}             = acQ:AccountName
                Excel{'ActiveCell.Offset(0, 1).HorizontalAlignment'} = xlRight

                Excel{'ActiveCell.Offset(0, 2).NumberFormat'}        = '#,##0'
                Excel{'ActiveCell.Offset(0, 2).Formula'}             = acQ:CountInvoices

                Excel{'ActiveCell.Offset(0, 3).NumberFormat'}        = '#,##0'
                Excel{'ActiveCell.Offset(0, 3).Formula'}             = acQ:CountLines

                Excel{'ActiveCell.Offset(0, 4).NumberFormat'}        = '#,##0'
                Excel{'ActiveCell.Offset(0, 4).Formula'}             = acQ:CountParts

                Excel{'ActiveCell.Offset(0, 5).NumberFormat'}        = '#,##0'
                Excel{'ActiveCell.Offset(0, 5).Formula'}             = acQ:CountLoans

                Excel{'ActiveCell.Offset(0, 6).NumberFormat'}        = '#,##0.00'
                Excel{'ActiveCell.Offset(0, 6).Formula'}             = acQ:Courier

                Excel{'ActiveCell.Offset(0, 7).NumberFormat'}        = '#,##0.00'
                Excel{'ActiveCell.Offset(0, 7).Formula'}             = acQ:Labour

                Excel{'ActiveCell.Offset(0, 8).NumberFormat'}        = '#,##0.00'
                Excel{'ActiveCell.Offset(0, 8).Formula'}             = acQ:Parts

                Excel{'ActiveCell.Offset(0, 9).NumberFormat'}        = '#,##0.00'
                Excel{'ActiveCell.Offset(0, 9).Formula'}             = acQ:Total
            !-------------------------------------------------------------
            DO XL_RowDown
            !-------------------------------------------------------------
        END !LOOP

        Excel{'Range("A' & TitleRow & ':' & sheet:HeadLastCol & LastRow & '").Select'}
            Excel{'Selection.AutoFilter'}

        Excel{'Range("A' & LastRow+2 & '").Select'}
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
GetUserName                            ROUTINE
    DATA
CommandLine STRING(255)
tmpPos      LONG
    CODE
        !-----------------------------------------------
        CommandLine = CLIP(COMMAND(''))

!        LOC:UserName = LOC:ApplicationName
!
!        Case MessageEx('Fail User Check For "' & CLIP(CommandLine) & '"?', LOC:ApplicationName & ' DEBUG',|
!                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
!            Of 1 ! &Yes Button
!                POST(Event:CloseWindow)
!
!                EXIT
!            Of 2 ! &No Button
!                EXIT
!        End!Case MessageEx
        !-----------------------------------------------
        tmpPos = INSTRING('%', CommandLine)
        IF NOT tmpPos
            Case Missive('You must run this report from ServiceBase''s "Custom Reports".','ServiceBase 3g',|
                           'mstop.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive

            POST(Event:CloseWindow)

           EXIT
        END !IF tmpPos
        !-----------------------------------------------
        SET(USERS)
        Access:USERS.Clearkey(use:Password_Key)
        glo:Password = CLIP(SUB(CommandLine, tmpPos + 2, 30))
      
        Access:USERS.Clearkey(use:Password_Key)
        use:Password = glo:Password
        If Access:USERS.Tryfetch(use:Password_Key)
            Case Missive('Error! Cannot find the user''s details.','ServiceBase 3g',|
                           'mstop.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive

            POST(Event:CloseWindow)

           EXIT
        End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        !-----------------------------------------------
        LOC:UserName = use:Forename

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = use:Surname
        ELSE
            LOC:UserName = CLIP(use:Forename) & ' ' & use:Surname 
        END !IF

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = '<' & use:User_Code & '>'
        END !IF
        !-----------------------------------------------
    EXIT
GetUserName2                            ROUTINE
    DATA
CommandLine STRING(255)
tmpPos      LONG
    CODE
        !-----------------------------------------------
        If Command('/SCHEDULE')
            Automatic = False
            Exit
        End ! If Command('/SCHEDULE')
        CommandLine = CLIP(COMMAND(''))

!        LOC:UserName = LOC:ApplicationName
!
!        Case MessageEx('Fail User Check For "' & CLIP(CommandLine) & '"?', LOC:ApplicationName & ' DEBUG',|
!                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
!            Of 1 ! &Yes Button
!                POST(Event:CloseWindow)
!
!                EXIT
!            Of 2 ! &No Button
!                EXIT
!        End!Case MessageEx
        !-----------------------------------------------
        Automatic = FALSE

!        IF NOT tmpPos
!            Case MessageEx('Attempting to use Workshop Performance Report<10,13>'            & |
!                '   without using ' & LOC:ApplicationName & '.<10,13>'                       & |
!                '   Start ' & LOC:ApplicationName & ' and run the report from there.<10,13>',  |
!                LOC:ApplicationName,                                                           |
!                'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,LocalTimeOut)
!            Of 1 ! &OK Button
!            END!Case MessageEx
!
!           POST(Event:CloseWindow)
!
!           EXIT
!        END !IF tmpPos
        !-----------------------------------------------
        skip# = 0
        SET(USERS)
        Access:USERS.Clearkey(use:Password_Key)
        glo:Password = CLIP(SUB(CommandLine, tmpPos + 2, 30))

      
        Access:USERS.Clearkey(use:Password_Key)
        use:Password = glo:Password
        If Access:USERS.Tryfetch(use:Password_Key)
            !Assert(0,'<13,10>Fetch Error<13,10>')
!            Case MessageEx('Unable to find your logged in user details.', |
!                    LOC:ApplicationName,                                  |
!                    'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,LocalTimeOut)
!                Of 1 ! &OK Button
!            End!Case MessageEx
!
!            POST(Event:CloseWindow)
!
!           EXIT
            LOC:UserName = 'AUTOMATED PROCEDURE'
            Automatic = TRUE
            skip# = 1
        End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        !-----------------------------------------------
        IF skip# = 0
            LOC:UserName = use:Forename

            IF CLIP(LOC:UserName) = ''
                LOC:UserName = use:Surname
            ELSE
                LOC:UserName = CLIP(use:Forename) & ' ' & use:Surname 
            END !IF

            IF CLIP(LOC:UserName) = ''
                LOC:UserName = '<' & use:User_Code & '>'
            END !IF
            !-----------------------------------------------
        END
    EXIT
GetFileName                             ROUTINE ! Generate default file name
    DATA
local:Desktop         CString(255)
DeskTopExists   BYTE
ApplicationPath STRING(255)
excel:ProgramName       Cstring(255)
    CODE
    excel:ProgramName = 'Retail Sales Income Report'
    SHGetSpecialFolderPath( GetDesktopWindow(), local:Desktop, 5, FALSE )
    local:Desktop = Clip(local:Desktop) & '\ServiceBase Export'

    !Does the Export Folder already Exists?
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    local:Desktop = Clip(local:Desktop) & '\' & Clip(excel:ProgramName)
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    loc:Path = local:Desktop
    loc:FileName = Clip(local:Desktop) & '\' & Clip(excel:ProgramName) & ' ' & Format(Today(),@d12)
!
!
!GetFileName                             ROUTINE ! Generate default file name
!    DATA
!Desktop         CString(255)
!DeskTopExists   BYTE
!ApplicationPath STRING(255)
!    CODE
!        !-----------------------------------------------
!        ! Generate default file name
!        !
!        !-----------------------------------------------
!        ! 4 Dec 2001 John Stop using def:Export_Path as previously directed
!        ! 4 Dec 2001 John Use generated date to create file name not parameters LOC:StartDate,LOC:EndDate
!        !
!        IF CLIP(LOC:FileName) <> ''
!            ! Already generated 
!            EXIT
!        END !IF
!
!        DeskTopExists = False
!
!        SHGetSpecialFolderPath( GetDesktopWindow(), Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
!        LOC:DesktopPath = Desktop
!
!        ApplicationPath = Desktop & '\' & LOC:ApplicationName & ' Export'
!        IF EXISTS(CLIP(ApplicationPath))
!            LOC:Path      = CLIP(ApplicationPath) & '\'
!            DeskTopExists = True
!
!        ELSE
!            Desktop  = CLIP(ApplicationPath)
!
!            IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!!                MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!!            ELSE
!!                MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
!            END !IF
!
!            IF EXISTS(CLIP(ApplicationPath))
!                LOC:Path      = CLIP(ApplicationPath) & '\'
!                DeskTopExists = True
!            END !IF
!
!        END !IF
!
!        IF DeskTopExists
!            ApplicationPath = CLIP(LOC:Path) & CLIP(LOC:ProgramName)
!            Desktop         = CLIP(ApplicationPath)
!
!            IF NOT EXISTS(CLIP(ApplicationPath))
!                IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!!                    MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!!                ELSE
!!                    MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
!                END !IF
!            END !IF
!
!            IF EXISTS(CLIP(ApplicationPath))
!                LOC:Path = CLIP(ApplicationPath) & '\'
!            END !IF
!        END !IF
!
!        !-----------------------------------------------
!        ! 23 Oct 2002 John
!        ! R003, berrjo :Insert the report exe name EG VODR56.V01
!        ! Do not attach the EXE extenstion but show the version number.  Increment the version number each time changed.
!        ! Make this standard on all reports sent for correction.
!        ! LOC:Version STRING(8) 3.1.0000 3.1 to match sevicebase version (and dictionary) .0000 increment before each compile
!        !
!        !LOC:FileName = CLIP(SHORTPATH(LOC:Path)) & CLIP(LOC:ProgramName) & ' ' & FORMAT(TODAY(), @D12) & '.xls'
!        LOC:FileName = SHORTPATH(CLIP(LOC:Path)) & CLIP(LOC:ProgramName) & ' VODR0046 ' & FORMAT(TODAY(), @D12) & '.xls'
!        !-----------------------------------------------
!        
!        !-----------------------------------------------
!    EXIT
LoadFileDialog                          ROUTINE ! Ask user if file name is empty
    DATA
OriginalPath STRING(255)
    CODE
        !-----------------------------------------------
        IF CLIP(LOC:Filename) = ''
            DO GetFileName 
        END !IF

        OriginalPath = PATH()
        SETPATH(LOC:Path) ! Required for Win95/98
            IF NOT FILEDIALOG('Save Spreadsheet', LOC:Filename, 'Microsoft Excel Workbook|*.XLS', |
                FILE:KeepDir + FILE:Save + FILE:NoError + FILE:LongName)

                LOC:Filename = ''
            END !IF
        SETPATH(OriginalPath)

        UPDATE()
        DISPLAY()
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
LoadUSERS                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Result = False

        Access:USERS.ClearKey(use:User_Code_Key)
        use:User_Code = LOC:UserCode
        SET(use:User_Code_Key, use:User_Code_Key)

        IF Access:USERS.NEXT()
            EXIT
        END !IF

        IF use:User_Code <> LOC:UserCode
            EXIT
        END !IF

        Result = True
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
ProgressBar_Setup                       ROUTINE
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
            RecordsPerCycle      = 25
            RecordsProcessed     = 0
            RecordsToProcess     = 10 !***The Number Of Records, or at least a guess***
            PercentProgress      = 0
            Progress:thermometer = 0

            thiswindow.reset(1) !This may error, if so, remove this line.
            open(progresswindow)
            progresswindow{PROP:Timer} = 1
         
            ?progress:userstring{prop:text} = 'Running...'
            ?progress:pcttext{prop:text}    = '0% Completed'
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***

ProgressBar_LoopPre          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***

        RecordsProcessed = 0
        RecordCount      = 0

        ?Progress:UserString{PROP:Text} = CLIP(Progress:Text)

        !RecordsToProcess = RECORDS(JOBS) !***The Number Of Records, or at least a guess***
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
        !-----------------------------------------------
    EXIT

ProgressBar_Loop             ROUTINE
    DATA
newPctText STRING(255)
    CODE
        !-----------------------------------------------
        !***Place the following code inside the loop. The first line updates the progress bar, and the rest allows the user to cancel the routine. This can be removed if necessary. ****

        Do ProgressBar_GetNextRecord2 !Necessary

        Do ProgressBar_CancelCheck
        If CancelPressed
            CLOSE(ProgressWindow)
        End!If 
        !-----------------------------------------------
        newPctText = 'Checked(' & RecordsProcessed & '), Found(' & RecordCount & ')'

        IF ?progress:pcttext{prop:text} <> newPctText
            ?progress:pcttext{prop:text} = newPctText

            Display()
        END !IF

        !***Place the following code inside the loop. The first line updates the progress bar, and the rest allows the user to cancel the routine. This can be removed if necessary. ****
        !-----------------------------------------------
    EXIT

ProgressBar_LoopPost         ROUTINE
    DATA
newPctText STRING(255)
    CODE
        !-----------------------------------------------
        ?Progress:UserString{PROP:Text} = CLIP(Progress:Text) & ' Done(' & RecordsProcessed & '/' & RecordCount & ')'

        newPctText = 'Checked(' & RecordsProcessed & '), Found(' & RecordCount & ')'

        IF ?progress:pcttext{prop:text} <> newPctText
            ?progress:pcttext{prop:text} = newPctText
        END !IF
        !-----------------------------------------------
    EXIT

ProgressBar_Finalise                        ROUTINE
        !**** End Of Loop ***
            Do ProgressBar_EndPrintRun
            close(progresswindow)
        !**** End Of Loop ***
        ! when in report procedure->LocalResponse = RequestCompleted

ProgressBar_GetNextRecord2                  routine
    recordsprocessed += 1
    recordsthiscycle += 1

    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        !?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end

ProgressBar_CancelCheck                     routine
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept

    If cancel# = 1
        BEEP(BEEP:SystemAsterisk)  ;  YIELD()
        Case Missive('Are you sure you want to cancel?','ServiceBase 3g',|
                       'mstop.jpg','\No|/Yes') 
            Of 2 ! Yes Button
                CancelPressed = True
            Of 1 ! No Button
        End ! Case Missive
    End!If cancel# = 1

ProgressBar_EndPrintRun                     routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()

RefreshWindow                          ROUTINE
    !|
    !| This routine is used to keep all displays and control templates current.
    !|
    !IF window{Prop:AcceptAll} THEN EXIT.
    IF MainWindow{Prop:AcceptAll} THEN EXIT.
    
    DISPLAY()
    !ForceRefresh = False
XL_Setup                                                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel = Create(0, Create:OLE)
        Excel{PROP:Create} = 'Excel.Application'

        Excel{'Application.DisplayAlerts'}  = False         ! no alerts to the user (do you want to save the file?) ! MOVED 10 BDec 2001 John
        Excel{'Application.ScreenUpdating'} = excel:Visible ! False
        Excel{'Application.Visible'}        = excel:Visible ! False

        Excel{'Application.Calculation'}    = xlCalculationManual
        
        DO XL_GetOperatingSystem
        !-----------------------------------------------
    EXIT
XL_Finalize                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Application.Calculation'}    = xlCalculationAutomatic

        Excel{PROP:DEACTIVATE}
        !-----------------------------------------------
    EXIT
XL_AddSheet                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveWorkBook.Sheets("Sheet3").Select'}

        Excel{'ActiveWorkBook.Sheets.Add'}
        !-----------------------------------------------
    EXIT
XL_AddWorkbook               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Application.Workbooks.Add()'}

        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Title")'}            = CLIP(LOC:ProgramName)
        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Author")'}           = CLIP(LOC:UserName)
        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Application Name")'} = LOC:ApplicationName
        !-----------------------------------------------
        ! 4 Dec 2001 John
        ! Delete empty sheets

        Excel{'Sheets("Sheet2").Select'}
        Excel{'ActiveWindow.SelectedSheets.Delete'}

        Excel{'Sheets("Sheet1").Select'}
        !-----------------------------------------------
    EXIT
XL_SaveAllWorksheets                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOOP WHILE Excel{'WorkBooks.Count'} > 0
            Excel{'ActiveWorkBook.Save()'}
            Excel{'ActiveWorkBook.Close()'}
        END !LOOP
        !-----------------------------------------------
    EXIT
XL_DropAllWorksheets                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOOP WHILE Excel{'WorkBooks.Count'} > 0
            Excel{'ActiveWorkBook.Close(' & FALSE & ')'}
        END !LOOP
        !-----------------------------------------------
    EXIT
XL_AddComment                ROUTINE
    DATA
xlComment CSTRING(20)
    CODE
        !-----------------------------------------------
        Excel{'Selection.AddComment("' & CLIP(excel:CommentText) & '")'}

!        xlComment{'Shape.IncrementLeft'} = 127.50
!        xlComment{'Shape.IncrementTop'}  =   8.25
!        xlComment{'Shape.ScaleWidth'}    =   1.76
!        xlComment{'Shape.ScaleHeight'}   =   0.69
        !-----------------------------------------------
    EXIT
XL_RowDown                   ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(1, 0).Select'}
        !-----------------------------------------------
    EXIT
XL_ColLeft                   ROUTINE
    DATA
CurrentCol LONG
    CODE
        !-----------------------------------------------
        CurrentCol = Excel{'ActiveCell.Col'}
        IF CurrentCol = 1
            EXIT
        END !IF

        Excel{'ActiveCell.Offset(0, -1).Select'}
        !-----------------------------------------------
    EXIT
XL_ColRight                  ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(0, 1).Select'}
        !-----------------------------------------------
    EXIT
XL_ColFirstSelect                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, Cells(' & Excel{'ActiveCell.Row'} & ', 1)).Select'}
        !Excel{'ActiveCell.Offset(0, -' & Excel{'ActiveCell.Column'} - 1 & ').Select'}
        !-----------------------------------------------
    EXIT
XL_ColFirst                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(0, -' & Excel{'ActiveCell.Column'} - 1 & ').Select'}
        !-----------------------------------------------
    EXIT
XL_SetLastCell                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, ActiveCell.SpecialCells(' & xlLastCell & ')).Select'}
        !-----------------------------------------------
    EXIT
XL_SetGrid                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlDiagonalDown & ').LineStyle'} = xlNone
        Excel{'Selection.Borders(' & xlDiagonalUp   & ').LineStyle'} = xlNone

        DO XL_SetBorder

        Excel{'Selection.Borders(' & xlInsideVertical & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlInsideVertical & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlInsideVertical & ').ColorIndex'} = xlAutomatic

        Excel{'Selection.Borders(' & xlInsideHorizontal & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlInsideHorizontal & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlInsideHorizontal & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorder                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_SetBorderLeft
        DO XL_SetBorderRight
        DO XL_SetBorderBottom
        DO XL_SetBorderTop
        !-----------------------------------------------
    EXIT
XL_SetBorderLeft                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeLeft & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeLeft & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeLeft & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderRight                           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeRight & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeRight & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeRight & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderBottom                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeBottom & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeBottom & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeBottom & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderTop                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeTop & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeTop & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeTop & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_HighLight                           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Interior.ColorIndex'}        = 6 ! YELLOW
        Excel{'Selection.Interior.Pattern'}           = xlSolid
        Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic
        !-----------------------------------------------
XL_SetTitle                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Interior.ColorIndex'}        = 15 ! GREY
        Excel{'Selection.Interior.Pattern'}           = xlSolid
        Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetWrapText                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.WrapText'}    = True
        !-----------------------------------------------
    EXIT
XL_SetGreyText                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Font.ColorIndex'}    = 16 ! grey
        !-----------------------------------------------
    EXIT
XL_SetBold                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Font.Bold'} = True
        !-----------------------------------------------
    EXIT
XL_FormatDate                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.NumberFormat'} = excel:DateFormat
        !-----------------------------------------------
    EXIT
XL_FormatNumber                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.NumberFormat'} = '#,##0.00'
        !-----------------------------------------------
    EXIT
XL_FormatTextBox                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.ShapeRange.IncrementLeft'} = 127.5
        Excel{'Selection.ShapeRange.IncrementTop'}  =   8.25
!        Excel{'Selection.ShapeRange.ScaleWidth'}    =  '1.76, ' & msoFalse & ', ' & msoScaleFromBottomRight
!        Excel{'Selection.ShapeRange.ScaleHeight'}   =  '0.69, ' & msoFalse & ', ' & msoScaleFromTopLeft
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentCentre            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlCenter
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentLeft              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlLeft
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentRight             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlRight
        !-----------------------------------------------
    EXIT
XL_VerticalAlignmentTop                 ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.VerticalAlignment'} = xlTop
        !-----------------------------------------------
    EXIT
XL_VerticalAlignmentCentre            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.VerticalAlignment'} = xlCenter
        !-----------------------------------------------
    EXIT
XL_SetColumnHeader                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_SetTitle
        DO XL_SetGrid
        DO XL_SetWrapText
        DO XL_SetBold
        !-----------------------------------------------
    EXIT
XL_SetColumn_Date                       ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = 'dd Mmm yyyy'

!        !Excel{'ActiveCell.FormulaR1C1'}  = LEFT(FORMAT(ret:Invoice_Date, @D8-))
        !-----------------------------------------------
    EXIT
XL_SetColumn_Number                     ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = '#,##0.00'
        !-----------------------------------------------
    EXIT
XL_SetColumn_Percent                    ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = '0.00%'
!        Excel{'ActiveCell.NumberFormat'} = CHR(64)       ! Text (AT) Sign
        !-----------------------------------------------
    EXIT
XL_SetColumn_Text                       ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'} = CHR(64) ! Text (AT) Sign
        !-----------------------------------------------
    EXIT
XL_DataSelectRange                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, ActiveCell.SpecialCells(' & xlLastCell & ')).Select'}
        !-----------------------------------------------
    EXIT
XL_DataHideDuplicates                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.AdvancedFilter Action:=' & xlFilterInPlace & ', Unique:=' & True }
        !-----------------------------------------------
    EXIT
XL_DataSortRange                            ROUTINE
    DATA
    CODE                     
        !-----------------------------------------------
        DO XL_DataSelectRange
        Excel{'ActiveWindow.ScrollColumn'} = 1
        Excel{'Selection.Sort Key1:=Range("' & LOC:Text & '"), Order1:=' & xlAscending & ', Header:=' & xlGuess & |
            ', OrderCustom:=1, MatchCase:=' & False & ', Orientation:=' & xlTopToBottom}
        !-----------------------------------------------
    EXIT
XL_DataAutoFilter                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.AutoFilter'}
        !-----------------------------------------------
    EXIT
XL_SetWorksheetLandscape                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.Orientation'}     = xlLandscape
!        Excel{'ActiveSheet.PageSetup.FitToPagesWide'} = 1
!        Excel{'ActiveSheet.PageSetup.FitToPagesTall'} = 9999
!        Excel{'ActiveSheet.PageSetup.Order'}          = xlOverThenDown
        !-----------------------------------------------
    EXIT
XL_SetWorksheetPortrait                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.Orientation'} = xlPortrait
        !-----------------------------------------------
    EXIT
XL_PrintTitleRows                               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.PrintTitleRows'} = CLIP(LOC:Text)
        !-----------------------------------------------
    EXIT
XL_GetCurrentCell                               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOC:Text = Excel{'Application.ConvertFormula( "RC", ' & xlR1C1 & ', ' & xlA1 & ')'}
        !-----------------------------------------------
    EXIT

XL_MergeCells                                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlCenter
        Excel{'Selection.VerticalAlignment'}   = xlBottom
        Excel{'Selection.MergeCells'}          = True
        !-----------------------------------------------
    EXIT

XL_GetOperatingSystem                                                          ROUTINE
    DATA
OperatingSystem     STRING(50)
tmpLen              LONG
LEN_OperatingSystem LONG
    CODE
        !-----------------------------------------------
        excel:OperatingSystem = 0.0    ! Default/error value
        OperatingSystem       = Excel{'Application.OperatingSystem'}
        LEN_OperatingSystem   = LEN(CLIP(OperatingSystem))

        LOOP x# = LEN_OperatingSystem TO 1 BY -1

            CASE SUB(OperatingSystem, x#, 1)
            OF '0' OROF'1' OROF '2' OROF '3' OROF '4' OROF '5' OROF '6' OROF '7' OROF '8' OROF '9' OROF '.'
                ! NULL
            ELSE
                tmpLen                = LEN_OperatingSystem-x#+1
                excel:OperatingSystem = CLIP(SUB(OperatingSystem, x#+1, tmpLen))

                EXIT
            END !CASE
        END !LOOP
        !-----------------------------------------------
    EXIT
!-----------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020607'&'0'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('IncomeReportRetailSales')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:INVOICE.Open
  Relate:REPSCHCR.Open
  Relate:RETPAY.Open
  Relate:VATCODE.Open
  Relate:WEBJOB.Open
  Access:USERS.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:TRDBATCH.UseFile
  Access:RETSALES.UseFile
  Access:RETSTOCK.UseFile
  Access:STOCK.UseFile
  Access:REPSCHED.UseFile
  Access:REPSCHLG.UseFile
  SELF.FilesOpened = True
  OPEN(MainWindow)
  SELF.Opened=True
  ! ========= Set Report Version =============
  Include('..\ReportVersion.inc')
  tmp:VersionNumber = 'Version Number: ' & Clip(tmp:VersionNumber) & '5004'
  ?ReportVersion{prop:Text} = tmp:VersionNumber
      LOC:ProgramName         = 'Income Report - Retail Sales'   !           Job=2060         Cust=BOUNCER
  
      LOC:StartDate      = DATE( MONTH(TODAY()), 1, YEAR(TODAY()) )
      LOC:EndDate        = TODAY() !
      LOC:DayLeawayCount = 90
  
      excel:Visible      = False
      debug:Active       = False
  
      DO GetUserName2
  
      IF Automatic = TRUE
        CommandLine = CLIP(COMMAND(''))
        tmpPos = INSTRING('&', CommandLine)
        LOC:StartDate      = DATE( MONTH(CLIP(SUB(CommandLine, tmpPos + 1, 30))), 1, YEAR(CLIP(SUB(CommandLine, tmpPos + 1, 30))) )
        LOC:EndDate        = CLIP(SUB(CommandLine, tmpPos + 1, 30)) !
      END
  
  Bryan.CompFieldColour()
  ?LOC:EndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
      IF Automatic = TRUE
        !LOC:StartDate = TODAY()
        !LOC:EndDate = TODAY()
        !DoAll = 'Y'
        DO OKButton_Pressed
        !POST(Event:Closewindow)
        !Call email program!
        !PUTINI('MAIN','InputDir',CLIP(Loc:Path),CLIP(PATH()) & '\AUTOEMAIL.INI')
        !PUTINI('MAIN','ExportDir',CLIP(Loc:Path)&'EMAILED',CLIP(PATH()) & '\AUTOEMAIL.INI')
        !RUN('EMAILDIR',1)
        POST(Event:CloseWindow)
      END
  SELF.SetAlerts()
  ! Inserting (DBH 31/08/2007) # 9125 - Is this an automatic report?
  If Command('/SCHEDULE')
      x# = Instring('%',Command(),1,1)
      Access:REPSCHED.ClearKey(rpd:RecordNumberKey)
      rpd:RecordNumber = Clip(Sub(Command(),x# + 1,20))
      If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
          !Found
          Access:REPSCHCR.ClearKey(rpc:ReportCriteriaKey)
          rpc:ReportName = rpd:ReportName
          rpc:ReportCriteriaType = rpd:ReportCriteriaType
          If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
              !Found
  
              Case rpc:DateRangeType
              Of 1 ! Today Only
                  loc:StartDate = Today()
                  loc:EndDate = Today()
              Of 2 ! 1st Of Month
                  loc:StartDate = Date(Month(Today()),1,Year(Today()))
                  loc:EndDate = Today()
              Of 3 ! Whole Month
  ! Changing (DBH 31/01/2008) # 9711 - Does not account for a change of year
  !                loc:StartDate = Date(Month(Today()) - 1, 1, Year(Today()))
  ! to (DBH 31/01/2008) # 9711
                  loc:EndDate = Date(Month(Today()),1,Year(Today())) - 1
                  loc:StartDate = Date(Month(loc:EndDate),1,Year(loc:EndDate))
  ! End (DBH 31/01/2008) #9711
  
              End ! Case rpc:DateRangeType
  
              Do OKButton_Pressed
              Post(Event:CloseWindow)
          Else ! If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
              !Error
          End ! If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
  
      Else ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
          !Error
      End ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
  
  End ! If Command('/SCHEDULE')
  ! End (DBH 31/08/2007) #9125
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:INVOICE.Close
    Relate:REPSCHCR.Close
    Relate:RETPAY.Close
    Relate:VATCODE.Close
    Relate:WEBJOB.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OkButton
          DO OKButton_Pressed
    OF ?CancelButton
       POST(Event:CloseWindow)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020607'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020607'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020607'&'0')
      ***
    OF ?StartPopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          LOC:StartDate = TINCALENDARStyle1(LOC:StartDate)
          Display(?LOC:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?EndPopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          LOC:EndDate = TINCALENDARStyle1(LOC:EndDate)
          Display(?LOC:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?LOC:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?EndPopCalendar)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

DateToString        PROCEDURE( IN:Date )! STRING
    CODE
        !-----------------------------------------------------------------
        RETURN LEFT(FORMAT(IN:Date, @D8))
        !-----------------------------------------------------------------
GetHeadAccount PROCEDURE( IN:AccountNumber )
    CODE
        !-----------------------------------------------------------------  
        WriteDebug('GetHeadAccount(' & CLIP(IN:AccountNumber) & ')')

        haq:AccountNumber  = IN:AccountNumber
        GET(HeadAccount_Queue, +haq:AccountNumber)

        CASE ERRORCODE()
        OF 00 ! FOUND
            WriteDebug('GetHeadAccount(Found)')

        OF 30 ! NOT Found
            ! Not in queue - ADD
            WriteDebug('GetHeadAccount(ADD="' & CLIP(IN:AccountNumber) & '")')

            haq:AccountNumber            = IN:AccountNumber

            IF NOT LoadTRADEACC(IN:AccountNumber)
                haq:AccountName          = '*T'
                haq:BranchIdentification = '*T'

                haQ:PartsVATCode         = ''
                haQ:LabourVATCode        = ''
                haQ:RetailVATCode        = ''

                haQ:PartsVATRate         = 0.00
                haQ:LabourVATRate        = 0.00
                haQ:RetailVATRate        = 0.00

                ADD(HeadAccount_Queue, +haq:AccountNumber)

                RETURN
            END !IF

            haq:AccountName          = tra:Company_Name
            haq:BranchIdentification = tra:BranchIdentification

            IF tra:Invoice_Sub_Accounts = 'YES'
                haQ:InvoiceSubAccounts = True
            ELSE
                haQ:InvoiceSubAccounts = False
            END !IF

            haQ:PartsVATCode         = tra:Parts_VAT_Code
            haQ:LabourVATCode        = tra:Labour_VAT_Code
            haQ:RetailVATCode        = tra:Retail_Discount_Code

            haQ:PartsVATRate         = GetVATRate(tra:Parts_VAT_Code)
            haQ:LabourVATRate        = GetVATRate(tra:Labour_VAT_Code)
            haQ:RetailVATRate        = GetVATRate(tra:Retail_Discount_Code)

            ADD(HeadAccount_Queue, +haq:AccountNumber)
        ELSE
            CancelPressed = True

            WriteDebug('GetHeadAccount(ERROR)')
        END !IF
        !-----------------------------------------------------------------
GetInvoiceType      PROCEDURE( IN:InvoiceType )! STRING
    CODE
        !------------------------------------------
        ! 25 Sep 2002 John, Although the system does not allow credits at the moment make provission for it here
        RETURN 'Invoice'
!
!        CASE IN:InvoiceType
!        OF 'CHA'
!            RETURN 'Multiple Chargeable'
!        OF 'WAR'
!            RETURN 'Warranty'
!        OF 'RET'
!            RETURN 'Retail'
!        OF 'SIN'
!            RETURN 'Single Chargeable'
!        ELSE
!            RETURN 'Undefined'
!        END !CASE
        !------------------------------------------
GetSubAccount PROCEDURE( IN:AccountNumber )
    CODE
        !-----------------------------------------------------------------  
        WriteDebug('GetSubAccount(' & CLIP(IN:AccountNumber) & ')')

        saq:AccountNumber  = IN:AccountNumber
        GET(SubAccount_Queue, +saq:AccountNumber)

        CASE ERRORCODE()
        OF 00 ! FOUND
            WriteDebug('GetSubAccount(Found)')

        OF 30 ! NOT Found
            ! Not in queue - ADD
            WriteDebug('GetSubAccount(ADD)')

            saq:AccountNumber            = IN:AccountNumber

            IF NOT LoadSUBTRACC(IN:AccountNumber)
                saq:AccountName          = ''
                saq:HeadAccountNumber    = ''
                saQ:HeadAccountName      = ''
                saQ:BranchIdentification = ''

                saQ:PartsVATCode         = ''
                saQ:LabourVATCode        = ''
                saQ:RetailVATCode        = ''

                saQ:PartsVATRate         = 1.00
                saQ:LabourVATRate        = 1.00
                saQ:RetailVATRate        = 1.00

                ADD(SubAccount_Queue, +saq:AccountNumber)

                RETURN
            END !IF

            GetHeadAccount(sub:Main_Account_Number)

            saq:AccountName          = sub:Company_Name
            saQ:HeadAccountNumber    = sub:Main_Account_Number
            saQ:HeadAccountName      = haQ:AccountName
            saQ:BranchIdentification = haQ:BranchIdentification

            If haQ:InvoiceSubAccounts = True
                saQ:PartsVATCode         = sub:Parts_VAT_Code
                saQ:LabourVATCode        = sub:Labour_VAT_Code
                saQ:RetailVATCode        = sub:Retail_Discount_Code

                saQ:PartsVATRate         = GetVATRate(sub:Parts_VAT_Code)
                saQ:LabourVATRate        = GetVATRate(sub:Labour_VAT_Code)
                saQ:RetailVATRate        = GetVATRate(sub:Retail_Discount_Code)
            Else
                saQ:PartsVATCode         = haQ:PartsVATCode
                saQ:LabourVATCode        = haQ:LabourVATCode
                saQ:RetailVATCode        = haQ:RetailVATCode

                saQ:PartsVATRate         = haQ:PartsVATRate
                saQ:LabourVATRate        = haQ:LabourVATRate
                saQ:RetailVATRate        = haQ:RetailVATRate
            End!If tra:use_sub_accounts = 'YES'

            ADD(SubAccount_Queue, +saq:AccountNumber)

        ELSE
            WriteDebug('GetSubAccount(ERROR)')

            CancelPressed = True
        END !CASE
        !-----------------------------------------------------------------
GetVATRate      PROCEDURE( IN:VATCode )! REAL
    CODE
        !-----------------------------------------------------------------  
        IF NOT LoadVATCODE(IN:VATCode)
            RETURN 0.00
        END ! IF

        RETURN (vat:VAT_Rate / 100)
        !-----------------------------------------------------------------  
LoadINVOICE             PROCEDURE( IN:StartDate, IN:EndDate, INOUT:First )! LONG ! BOOL
    CODE
        !------------------------------------------
        WriteDebug('LoadINVOICE(IN:StartDate="' & FORMAT(IN:StartDate, @d8) & '", ' & INOUT:First & ')')

        IF INOUT:First = True
            WriteDebug('LoadINVOICE(First)')
            INOUT:First = False

            Access:INVOICE.ClearKey(inv:Date_Created_Key)
                inv:Date_Created = IN:StartDate
            SET(inv:Date_Created_Key, inv:Date_Created_Key)
        END !IF

        IF Access:INVOICE.Next() <> Level:Benign
            WriteDebug('LoadINVOICE(NOT Found)')

            RETURN False
        END !IF

        IF inv:Date_Created > IN:EndDate
            WriteDebug('LoadINVOICE(END="' & FORMAT(IN:EndDate, @d8) & '")')

            RETURN False
        END !IF

        WriteDebug('LoadINVOICE(Found)')
        RETURN True
        !-----------------------------------------------------------------
LoadRETPAY                   PROCEDURE( IN:SalesNumber, INOUT:Init )! LONG ! BOOL
    CODE
        !------------------------------------------------------------------
        WriteDebug('LoadRETPAY(IN:SalesNumber="' & IN:SalesNumber & '", ' & INOUT:Init & ')')

        IF INOUT:Init = True
            WriteDebug('LoadRETPAY(IN:SalesNumber="' & IN:SalesNumber & '", First)')

            INOUT:Init = False
            !
            Access:RETPAY.ClearKey(rtp:Date_Key)
                rtp:Ref_Number = IN:SalesNumber
            SET(rtp:Date_Key, rtp:Date_Key)
        END !IF

        IF Access:RETPAY.Next() <> Level:Benign
            WriteDebug('LoadRETPAY(EndOfIndex)')

            RETURN False
        END !IF

        IF NOT rtp:Ref_Number = IN:SalesNumber
            WriteDebug('LoadRETPAY(Passed Last Sale RetPay Entry)')

            RETURN False
        END !IF

        WriteDebug('LoadRETPAY(Found)')
        RETURN True
        !------------------------------------------------------------------
LoadRETSALES                   PROCEDURE( IN:InvoiceNumber, INOUT:Init )! LONG ! BOOL
    CODE
        !------------------------------------------------------------------
        WriteDebug('LoadRETSALES(IN:InvoiceNumber="' & IN:InvoiceNumber & '", ' & INOUT:Init & ')')

        IF INOUT:Init = True
            WriteDebug('LoadRETSALES(IN:InvoiceNumber="' & IN:InvoiceNumber & '", First)')

            INOUT:Init = False
            ! Invoice_Number_Key       KEY(ret:Invoice_Number),DUP,NOCASE
            !
            Access:RETSALES.ClearKey(ret:Invoice_Number_Key)
                ret:Invoice_Number = IN:InvoiceNumber
            SET(ret:Invoice_Number_Key, ret:Invoice_Number_Key)
        END !IF

        IF Access:RETSALES.Next() <> Level:Benign
            WriteDebug('LoadRETSALES(EndOfIndex)')

            ret:Ref_Number = ''
            RETURN False
        END !IF

        IF NOT ret:Invoice_Number = IN:InvoiceNumber
            WriteDebug('LoadRETSALES(Passed Last Invoice RetSales Entry)')

            ret:Ref_Number = ''
            RETURN False
        END !IF

        WriteDebug('LoadRETSALES(Found)')
        RETURN True
        !------------------------------------------------------------------
LoadRETSTOCK            PROCEDURE( IN:RetSalesID, INOUT:Init )! LONG ! BOOL
    CODE
        !-----------------------------------------------------------------
        WriteDebug('LoadRETSTOCK(' & IN:RetSalesID & ', ' & INOUT:Init & ')')

        IF INOUT:Init = True
            WriteDebug('LoadRETSTOCK(First)')
            INOUT:Init = False
            !
            ! Part_Number_Key KEY( res:Ref_Number, res:Part_Number ),DUP,NOCASE
            !
            Access:RETSTOCK.ClearKey(res:Part_Number_Key)
                res:Ref_Number = IN:RetSalesID
            SET(res:Part_Number_Key, res:Part_Number_Key)
        END !IF

        LOOP WHILE Access:RETSTOCK.Next() = Level:Benign
            IF NOT res:Ref_Number = IN:RetSalesID
                WriteDebug('LoadRETSTOCK(NOT Found)')

                RETURN False
            END !IF

            IF res:Despatched <> 'YES'
                WriteDebug('LoadRETSTOCK(CYCLE="' & CLIP(res:Despatched) & '")')

                CYCLE
            END !IF

            WriteDebug('LoadRETSTOCK(Found)')
            RETURN True
        END !IF

        WriteDebug('LoadRETSTOCK(EndOfIndex)')

        RETURN False
        !-----------------------------------------------------------------
LoadSTOCK       PROCEDURE( IN:PartRefNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:STOCK.ClearKey(sto:Ref_Number_Key)
            sto:Ref_Number = IN:PartRefNumber
        SET(sto:Ref_Number_Key, sto:Ref_Number_Key)

        IF Access:STOCK.NEXT() <> Level:Benign
            RETURN False
        END !IF

        IF sto:Ref_Number <> IN:PartRefNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadSUBTRACC   PROCEDURE( IN:AccountNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = IN:AccountNumber

        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key) <> Level:Benign
            RETURN False
        END !IF

        IF sub:Account_Number <> IN:AccountNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadTRADEACC   PROCEDURE( IN:AccountNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = IN:AccountNumber

        IF Access:TRADEACC.TryFetch(tra:Account_Number_Key) <> Level:Benign
            RETURN False
        END !IF

        IF tra:Account_Number <> IN:AccountNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadVATCODE     PROCEDURE( IN:VATCode )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:VATCODE.ClearKey(vat:Vat_code_Key)
        vat:VAT_Code = IN:VATCode

        IF Access:VATCODE.TryFetch(vat:Vat_code_Key) <> Level:Benign
            RETURN False
        END !IF

        IF NOT vat:VAT_Code = IN:VATCode
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadWEBJOB      PROCEDURE( IN:JobNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:WEBJOB.ClearKey(wob:RefNumberKey)
        wob:JobNumber = IN:JobNumber

        IF Access:WEBJOB.TryFetch(wob:RefNumberKey) <> Level:Benign
            RETURN False
        END !IF

        IF NOT wob:JobNumber = IN:JobNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LocalUseEuro        Procedure(String    func:AccountNumber)
    Code
        WriteDebug('LocalUseEuro(' & CLIP(func:AccountNumber) & ')')

    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
    sub:Account_Number  = func:AccountNumber
    If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Found
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = sub:Main_Account_Number
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
            If tra:Use_Sub_Accounts = 'YES' and tra:Invoice_Sub_Accounts = 'YES'
                !Check whether Sub Account uses the Euro.
                !This will change the screen displays to reflect this
                If sub:EuroApplies
                    Return Level:Benign
                End !If sub:EuroApplied = 1
            Else !If tra:Use_Sub_Accounts = 'YES' and tra:Invoice_Sub_Accounts = 'YES'
                IF tra:EuroApplies
                    Return Level:Benign
                End !IF tra:EuroApplies
            End !If tra:Use_Sub_Accounts = 'YES' and tra:Invoice_Sub_Accounts = 'YES'
        Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        
    Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign

    Return Level:Fatal
NumberToColumn PROCEDURE(IN:Long)! STRING
FirstCol STRING(1)
Temp     LONG
    CODE
        !-----------------------------------------------
        Temp = IN:Long - 1

        FirstCol = Temp / 26
        IF FirstCol = 0
            RETURN CHR(Temp+65)
        END !IF

        RETURN CHR(FirstCol+64) & CHR( (Temp % 26)+65 )
        !-----------------------------------------------
WriteColumn PROCEDURE( IN:String, IN:StartNewRow )
Temp STRING(255)
    CODE
        !-----------------------------------------------
        Temp = IN:String
        IF CLIP(Temp) = ''
            Temp = ''' '
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            IF IN:StartNewRow
                clip:Value = Temp
            ELSE
                clip:Value = CLIP(clip:Value) & '<09>' & Temp
            END !IF

            RETURN
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
WriteDebug      PROCEDURE( IN:Message )
    CODE
        !-----------------------------------------------
        IF NOT debug:Active
            RETURN
        END !IF

        debug:Count += 1

        PUTINI(CLIP(LOC:ProgramName), debug:Count, IN:Message, 'C:\Debug.ini')
        !-----------------------------------------------
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
