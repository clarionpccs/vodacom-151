

   MEMBER('vodr0059.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('VODR0002.INC'),ONCE        !Local module procedure declarations
                     END


ARCIncomeReport PROCEDURE                             !Generated from procedure template - Window

Automatic            BYTE
ReportCancelled      BYTE
CommandLine          STRING(255)
tmpPos               LONG
ActiveRow            LONG
WarActiveRow         LONG
FinalExportPath      CSTRING(255)
tmp:VersionNumber    STRING(30)
ParameterStuff       GROUP,PRE()
LOC:EndDate          DATE
LOC:StartDate        DATE
                     END
RRC_Group            GROUP,PRE(rrc)
ARCLocation          STRING(30)
DespatchToCustomer   STRING(30)
SentToARC            STRING(30)
RRCLocation          STRING(30)
StatusReceivedAtRRC  STRING(30)
                     END
Local_Stuff          GROUP,PRE(LOC)
Site_Location        STRING(30)
Account_Name         STRING(100)
DateTitle            STRING(20)
AccountNumber        STRING(15)
ApplicationName      STRING('ServiceBase')
CommentText          STRING(100)
CompanyName          STRING(30)
Courier              STRING(30)
DesktopPath          STRING(255)
FileName             STRING(255)
FranchiseJobNumber   STRING(20)
JobNumber            LONG
LastColumn           STRING('L')
LineCost             DECIMAL(7,2)
Path                 STRING('C:\ {252}')
ProgramName          STRING(100)
RepairCentreType     STRING(3)
SectionName          STRING(100)
Text                 STRING(255)
UserCode             STRING(3)
UserName             STRING(100)
VATCode              STRING(2)
Version              STRING('3.1.0000')
                     END
ProgressBar__Stuff   GROUP,PRE()
Progress:Text        STRING(100)
RecordCount          LONG
                     END
Misc_Group           GROUP,PRE()
AccountChange        BYTE
InvoiceOK            BYTE
OPTION1              SHORT
Result               BYTE
StockOK              BYTE
tmp:FirstModel       STRING(30)
WebJOB_OK            BYTE
                     END
Excel_Group          GROUP,PRE()
Excel                SIGNED
excel:DateFormat     STRING('dd mmm yyyy')
excel:ColumnName     STRING(50)
excel:ColumnWidth    REAL
excel:CommentText    STRING(255)
excel:OperatingSystem REAL
excel:Visible        BYTE
                     END
Sheet_Group          GROUP,PRE(sheet)
TempLastCol          STRING(2)
HeadLastCol          STRING('o')
DataLastCol          STRING('M {1}')
HeadSummaryRow       LONG(9)
DataSectionRow       LONG(9)
DataHeaderRow        LONG(11)
                     END
Clipboard_Group      GROUP,PRE(clip)
OriginalValue        STRING(255)
Saved                BYTE
Value                CSTRING(1024)
                     END
WarParts_Group       GROUP,PRE(wp)
NonWAccessoriesCost  DECIMAL(7,2)
NonWProfit           DECIMAL(7,2)
NonWRevenue          DECIMAL(7,2)
NonWSparesCost       DECIMAL(7,2)
WarrAccessoriesCost  DECIMAL(7,2)
WarrProfit           DECIMAL(7,2)
WarrRevenue          DECIMAL(7,2)
WarrSparesCost       DECIMAL(7,2)
                     END
Stuart_Group         GROUP,PRE()
DoAll                STRING(1)
GUIMode              BYTE
LocalHeadAccount     STRING(30)
LocalTag             STRING(1)
LocalTimeOut         LONG
                     END
Debug_Group          GROUP,PRE(debug)
Active               LONG
Count                LONG
                     END
Calc_Group           GROUP,PRE(calc)
x3rdPartyRepairCosts DECIMAL(10,2)
x3rdPartyRepairVAT   DECIMAL(10,2)
ARCDateBooked        DATE
ARCLocation          STRING(30)
CurrentStatus        STRING(20)
DeliveryDateAtARC    DATE
InvoiceToAccountNumber STRING(15)
InvoiceToAccountName STRING(30)
InvoiceToSubAccountNumber STRING(15)
InvoiceToSubAccountName STRING(30)
OldStatus            STRING(30)
SentToARC            STRING(3)
                     END
ChaWar_ARCRRC_Group  GROUP,PRE(calc),DIM(4)
SparesCostPrice      DECIMAL(10,2)
SparesSellingPrice   DECIMAL(10,2)
LabourRate           DECIMAL(10,2)
LostLoan             REAL
HandlingFee          DECIMAL(10,2)
ExchangeFee          DECIMAL(10,2)
VAT                  DECIMAL(10,2)
TotalJobValue        DECIMAL(10,2)
AmountPaid           DECIMAL(10,2)
                     END
HeaderQueue          QUEUE,PRE(head)
ColumnName           STRING(30)
ColumnWidth          REAL
NumberFormat         STRING(20)
                     END
EngineerQueue        QUEUE,PRE(eq)
Engineer             STRING(3)
Name                 STRING(100)
                     END
RepairCentre_Queue   QUEUE,PRE(repQ)
AccountNumber        STRING(15)
AccountName          STRING(30)
SheetName            STRING(30)
RepairCentreType     STRING(3)
RecordCount          LONG
                     END
HeadAccount_Queue    QUEUE,PRE(haQ)
AccountNumber        STRING(15)
AccountName          STRING(30)
BranchIdentification STRING(2)
InvoiceSubAccounts   BYTE
PartsVATCode         STRING(2)
LabourVATCode        STRING(2)
RetailVATCode        STRING(2)
PartsVATRate         REAL
LabourVATRate        REAL
RetailVATRate        REAL
                     END
SubAccount_Queue     QUEUE,PRE(saQ)
AccountNumber        STRING(15)
AccountName          STRING(30)
HeadAccountNumber    STRING(15)
HeadAccountName      STRING(30)
BranchIdentification STRING(2)
PartsVATCode         STRING(2)
LabourVATCode        STRING(2)
RetailVATCode        STRING(2)
PartsVATRate         REAL
LabourVATRate        REAL
RetailVATRate        REAL
VATCode              STRING(2)
VATRate              REAL
Generic              BYTE
                     END
SummaryQueue         QUEUE,PRE(sq)
Manufacturer         STRING(30)
TotalUnitsRepaired   LONG
WarrantyCount        LONG
WarrRevenueCount     LONG
WarrNonRevenueCount  LONG
WarrSparesCost       DECIMAL(7,2)
WarrAccessoriesCost  DECIMAL(7,2)
WarrRevenue          DECIMAL(7,2)
WarrProfit           DECIMAL(7,2)
NonWarrantyCount     LONG
NonWRevenueCount     LONG
NonWNonRevenueCount  LONG
NonWSparesCost       DECIMAL(7,2)
NonWAccessoriesCost  DECIMAL(7,2)
NonWRevenue          DECIMAL(7,2)
NonWProfit           DECIMAL(7,2)
                     END
Excel                SIGNED !OLE Automation holder
excel:ProgramName    CString(255)
excel:ActiveWorkBook CString(20)
excel:Selected       CString(20)
excel:FileName       CString(255)
loc:Version          Cstring(30)
MainWindow           WINDOW('Window Title'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(160,64,360,300),USE(?PanelMain),FILL(0D6EAEFH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       BUTTON,AT(636,8),USE(?ButtonHelp),SKIP,TRN,FLAT,ICON('F1Helpsw.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,90,348,238),USE(?Panel4),FILL(09A6A7CH)
                       PROMPT('Income Report (ARC) Criteria'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,347,28),USE(?PanelButton),FILL(09A6A7CH)
                       PROMPT('Report Version: 5.2.5001'),AT(168,342),USE(?ReportVersion),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PROMPT('Export Date Rage:'),AT(264,166),USE(?Prompt4),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       STRING('Start Date'),AT(264,196,40,8),USE(?String2),TRN,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                       ENTRY(@d6),AT(344,196,64,10),USE(LOC:StartDate),IMM,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),TIP('Enter the earliest booking date'),REQ
                       BUTTON,AT(416,192),USE(?StartPopCalendar),IMM,TRN,FLAT,LEFT,TIP('Click to show calendar'),ICON('lookupp.jpg')
                       STRING('End Date'),AT(264,224,40,8),USE(?String2:2),TRN,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                       ENTRY(@d6),AT(344,224,64,10),USE(LOC:EndDate),IMM,FONT(,,010101H,FONT:bold),COLOR(COLOR:White),TIP('Enter the latest booking date'),REQ
                       BUTTON,AT(416,220),USE(?EndPopCalendar),IMM,TRN,FLAT,LEFT,TIP('Click to show calendar'),ICON('lookupp.jpg')
                       BUTTON,AT(380,334),USE(?OkButton),TRN,FLAT,LEFT,TIP('Click to print report'),ICON('exportp.jpg'),DEFAULT
                       BUTTON,AT(448,334),USE(?CancelButton),TRN,FLAT,LEFT,TIP('Click to close this form'),ICON('cancelp.jpg')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
E1                   Class(oiExcel)
Init                   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0),byte,proc,virtual,name('INIT@F17OIEXCEL')
Kill                   PROCEDURE (byte pUnloadCOM=1),byte,proc,virtual,name('KILL@F17OIEXCEL')
TakeEvent              PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0),virtual,name('TAKEEVENT@F17OIEXCEL')
                     End

    MAP

GetInvoiceAccount   PROCEDURE()
GetSubAccount       PROCEDURE( STRING )
GetHeadAccount      PROCEDURE( STRING )
GetVATRate          PROCEDURE( STRING ), REAL
GetEngineer         PROCEDURE( string )
LoadSUBTRACC        PROCEDURE( string ), BOOL
LoadTRDPARTY        PROCEDURE( string ), BOOL
LoadTRADEACC        PROCEDURE( string ), BOOL

!GetDeliveryDateAtARC    PROCEDURE( LONG ), BOOL
!LoadJOBPAYMT            PROCEDURE( long, BOOL ), BOOL
!GetOldStatus            PROCEDURE( long, string )
!LoadWARPARTS            PROCEDURE( LONG , BOOL), BOOL


    END !MAP
Expo    MyExportClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End
    Map
ExcelSetup          Procedure(Byte      func:Visible)

ExcelMakeWorkBook   Procedure(String    func:Title,String   func:Author,String  func:AppName)

ExcelMakeSheet      Procedure()

ExcelSheetType      Procedure(String    func:Type)

ExcelHorizontal     Procedure(String    func:Direction)

ExcelVertical        Procedure(String    func:Direction)

ExcelCell   Procedure(String    func:Text,Byte  func:Bold)

ExcelFormatCell     Procedure(String    func:Format)

ExcelFormatRange    Procedure(String    func:Range,String   func:Format)

ExcelNewLine    Procedure(Long  func:Number)

ExcelMoveDown   Procedure()

ExcelColumnWidth        Procedure(String    func:Range,Long   func:Width)

ExcelCellWidth          Procedure(Long  func:Width)

ExcelAutoFit            Procedure(String    func:Range)

ExcelGrayBox            Procedure(String    func:Range)

ExcelGrid   Procedure(String    func:Range,Byte  func:Left,Byte  func:Top,Byte   func:Right,Byte func:Bottom,Byte func:Colour)

ExcelSelectRange        Procedure(String    func:Range)

ExcelFontSize           Procedure(Byte  func:Size)

ExcelSheetName          Procedure(String    func:Name)

ExcelSelectSheet    Procedure(String    func:SheetName)

ExcelAutoFilter         Procedure(String    func:Range)

ExcelDropAllSheets      Procedure()

ExcelDeleteSheet        Procedure(String    func:SheetName)

ExcelClose              Procedure()

ExcelSaveWorkBook       Procedure(String    func:Name)

ExcelFontColour         Procedure(String    func:Range,Long func:Colour)

ExcelWrapText           Procedure(String    func:Range,Byte func:True)

ExcelGetFilename        Procedure(Byte      func:DontAsk),Byte

ExcelGetDirectory       Procedure(),Byte

ExcelCurrentColumn      Procedure(),String

ExcelCurrentRow         Procedure(),String

ExcelPasteSpecial       Procedure(String    func:Range)

ExcelConvertFormula     Procedure(String    func:Formula),String

ExcelColumnLetter Procedure(Long  func:ColumnNumber),String

ExcelOpenDoc            Procedure(String    func:FileName)

ExcelFreeze             Procedure(String    func:Cell)
    End

  CODE
  GlobalResponse = ThisWindow.Run()

OKButton_Pressed                   Routine

    !Read defaults
    rrc:ARCLocation         = GETINI('RRC',         'ARCLocation',     'RECEIVED AT ARC', '.\SB2KDEF.INI') ! [RRC]ARCLocation=RECEIVED AT ARC
    !rrc:RRCLocation         = GETINI('RRC',         'RRCLocation',        'AT FRANCHISE', '.\SB2KDEF.INI') ! [RRC]RRCLocation=AT FRANCHISE
    !rrc:DespatchToCustomer  = GETINI('RRC',  'DespatchToCustomer',          'DESPATCHED', '.\SB2KDEF.INI') ! [RRC]DespatchToCustomer=DESPATCHED
    !rrc:SentToARC           = GETINI('RRC',           'InTransit',   'IN-TRANSIT TO ARC', '.\SB2KDEF.INI') ! [RRC]InTransit=IN-TRANSIT TO ARC
    !rrc:StatusReceivedAtRRC = GETINI('RRC', 'StatusReceivedAtRRC', '455 RECEIVED AT RRC', '.\SB2KDEF.INI') ! [RRC]StatusReceivedAtRRC=455 RECEIVED AT RRC


    !Guess how many records to be processed   - use tmpPos, Activerow as available variables (long)
    Access:INVOICE.ClearKey(inv:Date_Created_Key)
    inv:Date_Created   = LOC:StartDate
    Set(inv:Date_Created_Key,inv:Date_Created_Key)
    if access:invoice.next()
        !?
        tmpPos = 0
    ELSE
        tmpPos = inv:Invoice_Number
    END
    Access:INVOICE.ClearKey(inv:Date_Created_Key)
    inv:Date_Created   = LOC:EndDate
    set(inv:Date_Created_Key,inv:Date_Created_Key)
    if access:invoice.previous()
        !?
        ActiveRow = records(Invoice) - TmpPos +25
    ELSE
        ActiveRow = inv:Invoice_Number - TmpPos + 25
    END
    !add on the jobs total
    Access:jobs.clearkey(job:Date_Booked_Key)
    job:date_booked =Loc:startDate
    set(job:Date_Booked_Key,job:Date_Booked_Key)
    if access:jobs.next() then
        tmpPos = 0
    ELSE
        tmpPos = job:Ref_Number
    END
    Access:jobs.clearkey(job:Date_Booked_Key)
    job:date_booked =Loc:EndDate
    set(job:Date_Booked_Key,job:Date_Booked_Key)
    if access:jobs.previous() then
        WarActiveRow = records(jobs) - TmpPos +25
    ELSE
        WarActiveRow = job:ref_Number - tmpPos + 25
    END
    !add 'em together
    ActiveRow += WarActiveRow
    WarActiveRow = ActiveRow / 30    !because only being updated every 25 records
    !End of guess how many records


    !progress window only being used here
    if not Automatic

        Expo.OpenProgressWindow(0,WarActiveRow)

        Expo.UpdateProgressText()          !blank line
        
        Expo.UpdateProgressText('Report Started: ' & FORMAT(Expo.StartDate,@d06) & |
            ' ' & FORMAT(Expo.StartTime,@t04))
        
        Expo.UpdateProgressText()
        Expo.UpdateProgressText('Records to process: (Approximately) '&clip(ActiveRow))
        Expo.UpdateProgressText()

    END  !of not automatic

    FinalExportPath = SetReportsFolder('ServiceBase Export','ARC Income Report',0)
    IF (FinalExportPath = '')

        If not automatic
            Expo.UpdateProgressText('Unable to trace export path for export. Please try again')
            Expo.UpdateProgressText('===============')
            Expo.UpdateProgressText('Report Abandoned: ' & FORMAT(Today(),@d06)& |
                    ' ' & FORMAT(clock(),@t04))
            Expo.FinishProgress()
        END

        EXIT
    END

    !start up the excel interface  "E1"
    If E1.Init(0,0) = 0

        If not automatic
            Expo.UpdateProgressText('===============')
            Expo.UpdateProgressText('Unable to create connection to Excel for export. Please try again')
            Expo.UpdateProgressText('===============')
            Expo.UpdateProgressText('Report Abandoned: ' & FORMAT(Today(),@d06) & |
                    ' ' & FORMAT(clock(),@t04))
            Expo.FinishProgress()
        END

        EXIT

    End ! If E1.Init(0,0) = False

    !start creating stuff in excel
    E1.NewWorkBook()

    !column headings
    Do ColumnHeadings

    !turning of the calculations should seriously improve performance "Provides a dramatic performance increase."
    E1.SetCalculations(oix:CalculationManual)

    ! Screen updating can be turned off to provide a small performance boost
    ! in cases where the window is hidden or the user does not need to see the 
    ! document being updated
    E1.SetScreenUpdating(false)

    ! Write the required data to the worksheet here...
    ReportCancelled = false


    !--------------------------------------------------
    !RUN THE MAIN LOOPS OF THE EXPORT
    Do InvoiceLoop      !for the chargeable sheet
    Do JobsLoop         !for the warranty sheet
    !--------------------------------------------------


    if not Automatic
        
        Expo.UpdateProgressText('===============')
        Expo.UpdateProgressText('Closing Operatioins ')
        Expo.UpdateProgressText()

    END

    if ReportCancelled = true then
        E1.Kill()
        exit
    END !if report has been cancelled

    !After all the data entry update the sheets
    ! Manually calculate (if necessary)
    E1.Calculate()

    !reset the scrren
    E1.SetScreenUpdating(true)
    
    ! Reset the Automatic calculation
    E1.SetCalculations(oix:CalculationAutomatic)


    !save and exit - warranty sheet formatting first
    !=============================================================
    E1.SelectWorkSheet('AA20_Warranty')

    e1.WriteToCell(clip(WarActiveRow-12),'F9')
    e1.WriteToCell('=SUBTOTAL(2, A12:A'&clip(WarActiveRow-1),'H9')

    e1.SetCellNumberFormat(oix:NumberFormatCurrency, oix:CurrencySymbolNone, 2, , 'I12','R'&clip(WarActiveRow-1))

    e1.SelectRows ('12')   !this works at last, in this position or after the autofilter
    e1.FreezePanes ()


    e1.SetCellFontStyle('Bold','A11','AH11')    !column headings
    e1.SetCellAlignment(oix:WrapText,true,'A11','AH11')


    !e1.SelectRows ('11')
    e1.SelectCells('B18')   !somewhere in the middle
    e1.AutoFilter('A','AF')    !According to  Capesoft this should be the format
    !e1.AutoFilter('A11','AF11') !this works elsewhere, but not here

    E1.SelectCells('A1')        !put cursor back to the first cell


    !=============================================================
    !Move back onto the chargeable worksheet
    e1.SelectWorkSheet('AA20')
    e1.WriteToCell(clip(ActiveRow-12),'F9')
    e1.WriteToCell('=SUBTOTAL(2, A12:A'&clip(ActiveRow-1),'H9')

    e1.SetCellNumberFormat(oix:NumberFormatCurrency, oix:CurrencySymbolNone, 2, , 'I12','U'&clip(ActiveRow-1))

    !freeze works at last
    e1.SelectRows ('12')
    e1.FreezePanes ()


    e1.SetCellFontStyle('Bold','A11','AH11')    !column headings
    e1.SetCellAlignment(oix:WrapText,true,'A11','AH11')

    !e1.SelectRows ('11')
    e1.SelectCells('B18')
    !e1.AutoFilter ('A','AH')
    e1.AutoFilter('A11','AH11')

    E1.SelectCells('A1')        !put cursor back to the first cell

    E1.SaveAs(clip(FinalExportPath)&'ARC Income Report_'&format(Today(),@d12)&'_'&format(clock(),@t5)&'.xlsx',oix:xlWorkbookDefault )
    E1.CloseWorkBook(2)
    E1.Kill

    !say goodbye nicely
    if not AutoMatic

        Expo.UpdateProgressText('===============')
        Expo.UpdateProgressText('Report Completed: ' & FORMAT(Today(),@d06) & |
            ' ' & FORMAT(clock(),@t04))
        Expo.FinishProgress(FinalExportPath)

    END

    exit
!--------------------------------------------------
InvoiceLoop                 Routine

    !Main loop for the chargeable stuff
    !progress window only being used here
    if not Automatic
        
        Expo.UpdateProgressText('===============')
        Expo.UpdateProgressText('Invoices Report Started For Chargeable Sheet ')
        Expo.UpdateProgressText()

    END

    Access:INVOICE.ClearKey(inv:Date_Created_Key)
    inv:Date_Created   = LOC:StartDate
    SET(inv:Date_Created_Key, inv:Date_Created_Key)
    LoopCount# = 30
    
    LOOP    !Main loop

        if Access:INVOICE.Next() then break.
        IF inv:Date_Created > LOC:EndDate then break.

        if Automatic = false
            if loopCount# > 25 then
                if Expo.UpdateProgressWindow()
                    Expo.UpdateProgressText('===============')
                    Expo.UpdateProgressText('Report interrupted ' & FORMAT(Today(),@d6) & |
                        ' ' & FORMAT(clock(),@t1))
                    Expo.FinishProgress()
                    !ReportCancelled = true
                    break
                END

                IF (Expo.CancelPressed = 2)
                    Expo.UpdateProgressText('===============')
                    Expo.UpdateProgressText('Report Cancelled: ' & FORMAT(Today(),@d6) & |
                        ' ' & FORMAT(clock(),@t1))
                    Expo.FinishProgress()
                    !ReportCancelled = true
                    break
                END

                !expo.UpdateProgresstext('Working on invoice number '&clip(inv:Invoice_Number))
                loopcount# = 0
            END !if loopcount# > 25
        END !if not automatic

        loopcount# += 1

!CHA, WAR and SIN are identical, so we just need to exclude anything else
!        CASE inv:Invoice_Type
!        OF 'CHA'
!            DO Invoice_CHA ! Multiple Chargeable
!        OF 'WAR'
!            DO Invoice_WAR ! Warranty
!        OF 'RET'
!            !DO Invoice_RET ! Retail
!        OF 'SIN'
!            DO Invoice_SIN ! Single Chargeable
!        ELSE
!            !DO Invoice_CHA
!        END !CASE
!        !-------------------------------------------------------------

        
        Case Inv:invoice_Type
            of 'CHA' orof 'WAR' orof 'SIN'

            ! Start - Only include invoices where the ARC has actually created an invoice - TrkBs: 6369 (DBH: 15-09-2005)
            If inv:ARCInvoiceDate <> ''

                Access:JOBS.ClearKey(job:Ref_Number_Key)
                job:Ref_Number = inv:Job_Number
                if access:jobs.fetch(job:Ref_number_key) then cycle.
                
                !get the matching webjob record
                Access:Webjob.clearkey(wob:RefNumberKey)
                wob:RefNumber = job:Ref_Number
                IF Access:WEBJOB.Fetch(wob:RefNumberKey) then cycle.

                !reset the defaults
                calc:SentToARC         = 'NO'
                calc:ARCDateBooked     = ''
                calc:DeliveryDateAtARC = ''

                !write the data if this is booked by ARC or delivered to ARC
                IF wob:HeadAccountNumber = LocalHeadAccount
                    
                    DO WriteColumns_Chargeable
                    
                ELSE

                    !find if this was ever sent to the ARC
                    Access:LOCATLOG.ClearKey(lot:DateKey)
                    lot:RefNumber = job:Ref_Number
                    lot:TheDate   = 0                !was LOC:StartDate - needs to be much earlier the job would be sent before the invoice
                    SET(lot:DateKey, lot:DateKey)
                    Loop

                        IF Access:LOCATLOG.NEXT() then break.
                        if lot:RefNumber <> job:Ref_Number then break.

                        IF CLIP(lot:NewLocation) = CLIP(rrc:ARCLocation)
                            calc:SentToARC         = 'YES'
                            calc:ARCDateBooked     = lot:TheDate
                            calc:DeliveryDateAtARC = lot:TheDate
                            Break
                        END !IF

                    END !loop through Locatlog

                    if calc:SentToARC  = 'YES' then
                        Do WriteColumns_Chargeable
                    END

                END !IF wob:HeadAccountNumber = LocalHeadAccount

            !------------------------------------------------------------------
            End ! If inv:ARCInvoiceDate <> ''
            ! End   - Only include invoices where the ARC has actually created an invoice - TrkBs: 6369 (DBH: 15-09-2005)

        END !Case invoice_type

    END !LOOP
WriteColumns_Chargeable                                                          ROUTINE
    DATA
Cha_RRC LONG(1)
Cha_ARC LONG(2)
War_RRC LONG(3)
War_ARC LONG(4)
    CODE

    !will need jobse
    Access:JOBSE.ClearKey(jobe:RefNumberKey)
    jobe:RefNumber = job:Ref_Number
    IF Access:JOBSE.Fetch(jobe:RefNumberKey) then exit.

    !order of these three was wrong
    !GetSubAccount(job:Account_Number)     called as part of getInvoiceAccount
    !GetHeadAccount(wob:HeadAccountNumber) called as part of getInvoiceaccount

    GetInvoiceAccount()
    !Expo.UpdateProgressText('Got Invoice Accounts '&' Ref_number is '&clip(job:Ref_Number))

    GetEngineer(job:Engineer)
    !Expo.UpdateProgressText('Got Engineer '&' Ref_number is '&clip(job:Ref_Number))

    DO UpdateSummaryQueue
    !Expo.UpdateProgressText('Got Summary Queue '&' Ref_number is '&clip(job:Ref_Number))

    !DO Calculate_ChaWar_ARCRRC_Costs    !part of UpdateSummaryQueue
    !Expo.UpdateProgressText('Got Calculations ')

    RecordCount += 1

    e1.WriteToCell( inv:Invoice_Number,'A'&clip(ActiveRow)) ! Invoice Number
    e1.WriteToCell( format(inv:Date_Created,@d08),'B'&clip(ActiveRow)       ) ! Invoice Date
    e1.WriteToCell( job:Ref_Number,'C'&clip(ActiveRow)                       ) ! SB Job Number

    !Add column to show Engineer Option - 3788 (DBH: 15-04-2004)
    Case jobe:Engineer48HourOption
        Of 1
            e1.WriteToCell('48H','D'&clip(ActiveRow))
        Of 2
            e1.WriteToCell('ARC','D'&clip(ActiveRow))
        Of 3
            e1.WriteToCell('7DT','D'&clip(ActiveRow))
        Of 4
            e1.WriteToCell('STD','D'&clip(ActiveRow))
        Else
            e1.WriteToCell('   ','D'&clip(ActiveRow))
    End !Case jobe:Engineer48HourOption

    e1.WriteToCell( haQ:BranchIdentification,'E'&clip(ActiveRow)             ) ! Franchise Branch Number
    e1.WriteToCell( wob:JobNumber,'F'&clip(ActiveRow)                        ) ! Franchise Job Number

    IF calc:SentToArc = ''
      calc:SentToArc = 'NO'
    END

    e1.WriteToCell( calc:SentToARC,'G'&clip(ActiveRow)                       ) ! Sent To ARC
    e1.WriteToCell( format(calc:DeliveryDateAtARC,@d08),'H'&clip(ActiveRow) ) ! Date Received At ARC   calc:ARCDateBooked   calc:DeliveryDateAtARC

    if job:Exchange_Unit_Number = 0
        e1.WriteToCell('NO','I'&clip(ActiveRow)            )
    else
        if jobe:ExchangedATRRC
            e1.WriteToCell('RRC','I'&clip(ActiveRow)            )
        else
            e1.WriteToCell('ARC','I'&clip(ActiveRow)            )
        end
    end

    IF LoanAttachedToJob(job:ref_number) = 1
      e1.WriteToCell('YES','J'&clip(ActiveRow)            )
    ELSE
      !No Loan ever
      e1.WriteToCell('NO','J'&clip(ActiveRow)            )
    END

    e1.WriteToCell( calc:x3rdPartyRepairCosts,'K'&clip(ActiveRow)            )   ! 3rd Party Repair Cost
    e1.WriteToCell( calc:x3rdPartyRepairVAT,'L'&clip(ActiveRow)              )   ! 3rd Party Repair VAT

    !Expo.UpdateProgressText('Middle of Excel writing '&' Ref_number is '&clip(job:Ref_Number))


    IF job:Chargeable_Job = 'YES'
        e1.WriteToCell( calc:SparesCostPrice[Cha_ARC],'M'&clip(ActiveRow)                       ) ! Spares Cost Price
        e1.WriteToCell( calc:SparesSellingPrice[Cha_ARC],'N'&clip(ActiveRow)                    ) ! Spares Selling Price
        e1.WriteToCell( calc:LabourRate[Cha_ARC],'O'&clip(ActiveRow)                            ) ! Labour Rate
        e1.WriteToCell( calc:LostLoan[Cha_ARC],'P'&clip(ActiveRow)                              ) ! Labour Rate
        e1.WriteToCell( job:Courier_Cost_Warranty,'Q'&clip(ActiveRow))
        e1.WriteToCell( calc:VAT[Cha_ARC],'R'&clip(ActiveRow)                                   ) ! VAT
        e1.WriteToCell( calc:TotalJobValue[Cha_ARC],'S'&clip(ActiveRow)                         ) ! Total Job Value
        e1.WriteToCell( calc:AmountPaid[Cha_ARC],'T'&clip(ActiveRow)                            ) ! Amount Paid
        e1.WriteToCell( calc:HandlingFee[Cha_ARC]+calc:ExchangeFee[Cha_ARC],'U'&clip(ActiveRow) ) ! Handling / Exchange Fee
    ELSE ! IF job:Warranty_Job = 'YES'
        e1.WriteToCell( calc:SparesCostPrice[War_ARC],'M'&clip(ActiveRow)                       ) ! Spares Cost Price
        e1.WriteToCell( calc:SparesSellingPrice[War_ARC],'N'&clip(ActiveRow)                    ) ! Spares Selling Price
        e1.WriteToCell( calc:LabourRate[War_ARC],'O'&clip(ActiveRow)                            ) ! Labour Rate
        e1.WriteToCell( calc:LostLoan[War_ARC],'P'&clip(ActiveRow)                              ) ! Labour Rate
        e1.WriteToCell( job:Courier_Cost_Warranty,'Q'&clip(ActiveRow)                          )
        e1.WriteToCell( calc:VAT[War_ARC],'R'&clip(ActiveRow)                                   ) ! VAT
        e1.WriteToCell( calc:TotalJobValue[War_ARC],'S'&clip(ActiveRow)                         ) ! Total Job Value
        e1.WriteToCell( calc:AmountPaid[War_ARC],'T'&clip(ActiveRow)                            ) ! Amount Paid
        e1.WriteToCell( calc:HandlingFee[War_ARC]+calc:ExchangeFee[War_ARC],'U'&clip(ActiveRow) ) ! Handling / Exchange Fee
    END !IF

    IF job:Warranty_Job = 'YES'
        IF job:Chargeable_Job = 'YES'
            e1.writeToCell( 'Chargeable/Warranty','V'&clip(ActiveRow))
        ELSE
            !Temp = 'Warranty'
            e1.writeToCell( 'Warranty','V'&clip(ActiveRow))
        END !IF
    ELSIF job:Chargeable_Job = 'YES'
        !Temp = 'Chargeable'
        e1.writeToCell( 'Chargeable','V'&clip(ActiveRow))
    ELSE
        !Temp = 'Unspecfied'
        e1.writeToCell( 'Unspecified','V'&clip(ActiveRow))
    END !IF

    IF job:Chargeable_Job = 'YES'
        e1.WriteToCell( job:Charge_Type,'W'&clip(ActiveRow)                  ) ! Chargeable Charge Type
        e1.WriteToCell( job:Repair_Type,'X'&clip(ActiveRow)                  ) ! Chargeable Repair Type
    END !IF

    e1.WriteToCell( calc:InvoiceToAccountNumber,'Y'&clip(ActiveRow)          ) ! Invoice To Account Number
    e1.WriteToCell( calc:InvoiceToAccountName,'Z'&clip(ActiveRow)            ) ! Invoice To Account Name


    IF LoadTRDPARTY(job:Third_Party_Site)
        e1.WriteToCell( trd:Account_Number,'AA'&clip(ActiveRow)               ) ! Third Party Account Number
        e1.WriteToCell( trd:Company_Name,'AB'&clip(ActiveRow)                 ) ! Third Party Account Name
        e1.WriteToCell( jobe:ARC3rdPartyInvoiceNumber,'AC'&clip(ActiveRow)    )
    ELSE
        !e1.WriteToCell( ''                               ) ! Third Party Account Number
        !e1.WriteToCell( ''                               ) ! Third Party Account Name
        !e1.WriteToCell( ''                               )
    END !IF

    e1.WriteToCell( Format(job:date_booked, @D08),'AD'&clip(ActiveRow)        ) ! Date Booked
    e1.WriteToCell( format(job:Date_Completed,@d08),'AE'&clip(ActiveRow)      ) ! Date Completed
    e1.WriteToCell( job:Manufacturer,'AF'&clip(ActiveRow)                     ) ! Manufacturer
    e1.WriteToCell( job:Model_Number,'AG'&clip(ActiveRow)                     ) ! Model
    !set next cell as text
    e1.SetCellNumberFormat(oix:NumberFormatText,,0,,'AH'&ActiveRow)
    e1.WriteToCell( job:ESN,'AH'&clip(ActiveRow)                              ) ! IMEI Number
    !e1.WriteToCell( ''''& job:ESN,'AH'&clip(ActiveRow)                              ) ! IMEI Number

    ActiveRow += 1
    !Expo.UpdateProgressText('Completed writing to excel '&' Ref_number is '&clip(job:Ref_Number))

    EXIT
Calculate_ChaWar_ARCRRC_Costs                      ROUTINE
    DATA
ParFirst LONG(True)
WprFirst LONG(True)
JptFirst LONG(True)

Cha_RRC LONG(1)
Cha_ARC LONG(2)
War_RRC LONG(3)
War_ARC LONG(4)
    CODE
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        CLEAR(ChaWar_ARCRRC_Group[Cha_RRC])
        CLEAR(ChaWar_ARCRRC_Group[Cha_ARC])
        CLEAR(ChaWar_ARCRRC_Group[War_RRC])
        CLEAR(ChaWar_ARCRRC_Group[War_ARC])
        !-----------------------------------------------------------------
        wp:NonWAccessoriesCost = 0
        wp:NonWSparesCost      = 0
        wp:NonWProfit          = 0
        wp:NonWRevenue         = 0

!TB13235 - Income Report not responding
!tracked to here - get 'In Costs - Chargeable Ref_Number is xxxxxxxx' and nothing more
!something is going wrong, occasionally, below here

        IF job:Chargeable_Job = 'YES'
            !Expo.UpdateProgressText('In Costs - Chargeable '&' Ref_number is '&clip(job:Ref_Number))
            Access:PARTS.ClearKey(par:Part_Number_Key)      !key is ref_number asc, Part_number asc
            par:Ref_Number = job:Ref_Number
            SET(par:Part_Number_Key, par:Part_Number_Key)
            LOOP
            
                if access:parts.next() then break.
                if par:Ref_Number <> job:Ref_Number then break.

                calc:SparesCostPrice[Cha_RRC] += par:quantity * par:RRCAveragePurchaseCost
                calc:SparesCostPrice[Cha_ARC] += par:quantity * par:AveragePurchaseCost

                !Is this an accessory?
                Access:STOCK.ClearKey(sto:Ref_Number_Key)
                sto:Ref_Number = Par:Part_ref_Number
                IF Access:STOCK.Fetch(sto:Ref_Number_Key) = level:benign then
                    IF (sto:accessory = 'YES')
                        wp:NonWAccessoriesCost += par:quantity * par:purchase_cost
                    ELSE
                        wp:NonWSparesCost      += par:quantity * par:purchase_cost
                    END !IF
                END !IF
            END !LOOP

            wp:NonWRevenue = job:Sub_Total
            wp:NonWProfit  = wp:NonWRevenue - wp:NonWAccessoriesCost - wp:NonWSparesCost
            !-----------------------------------------------------------------
            !Expo.UpdateProgressText('In Costs - Done Parts '&' Ref_number is '&clip(job:Ref_Number))

            !LOOP WHILE LoadJOBPAYMT(job:Ref_Number, JptFirst)
            !replace loadJobPayMt with
            Access:JOBPAYMT.ClearKey(jpt:All_Date_Key)
            jpt:Ref_Number = job:Ref_Number
            SET(jpt:All_Date_Key, jpt:All_Date_Key)
            Loop

                If access:jobPaymt.next() then break.
                if jpt:Ref_Number <> job:Ref_Number then break.

                !Is it valid!
                ltmp# = 0
                Access:Users.ClearKey(use:User_Code_Key)
                use:User_Code = jpt:User_Code
                IF Access:Users.Fetch(use:User_Code_Key)
                  !Error
                ELSE
                  IF LOC:Site_Location = use:Location
                    ltmp# = 1

                  END
                END
                calc:AmountPaid[Cha_RRC]      += jpt:Amount
                IF ltmp# = 1
                  calc:AmountPaid[Cha_ARC]    += jpt:Amount
                END
            END !LOOP

!something is going wrong above here
!TB13235 - Income Report not responding
!tracked to here - get 'In Costs - Chargeable Ref_Number is xxxxxxxx' and nothing more

            !Expo.UpdateProgressText('In Costs - Got payments '&' Ref_number is '&clip(job:Ref_Number))
            !-----------------------------------------------------------------
            calc:SparesSellingPrice[Cha_RRC]   = jobe:InvRRCCPartsCost
            calc:LabourRate[Cha_RRC]           = jobe:InvRRCCLabourCost
            calc:LostLoan[Cha_RRC]             = job:Invoice_Courier_Cost

            !Rules for Handling fees!
            handling# = 0
            exchange# = 0
            !Better find out what the charge should be!
            JobPricingRoutine(0)
            !Expo.UpdateProgressText('Chargeable - got pricing '&' Ref_number is '&clip(job:Ref_Number))
            !
! Using "JobPricingRoutine" should negate these checks (DBH: 06-05-2005)
!            IF SentToHub(job:ref_number) AND wob:HeadAccountNumber <>  LocalHeadAccount!True = sent to hub!!True = sent to hub!
!              !Allocate Handling Fee!
!              handling# = 1
!            END
!            IF jobe:ExchangedATRRC = TRUE
!              exchange#=1
!              !Exchanged at RRC!
!            END
!
!            IF handling# = 0
!              jobe:InvoiceHandlingFee = 0
!              jobe:HandlingFee = 0
!!              Access:Jobse.Update()
!            ELSE
!              jobe:InvoiceHandlingFee = handling"
!              jobe:HandlingFee = handling"
!!              Access:Jobse.Update()
!            END
!            IF exchange# = 0
!              jobe:InvoiceExchangeRate = 0
!              jobe:ExchangeRate = 0
!!              Access:Jobse.Update()
!            ELSE
!              jobe:InvoiceExchangeRate = exchange"
!              jobe:ExchangeRate = exchange"
!!              Access:Jobse.Update()
!            END
! End   - Using "JobPricingRoutine" should negate these checks (DBH: 06-05-2005)

!            IF SentToHub(job:ref_number) AND wob:HeadAccountNumber <>  LocalHeadAccount!True = sent to hub!
!              !Allocate Handling Fee!
!              handling# = 1
!              jobe:InvoiceHandlingFee = jobe:HandlingFee
!              Access:Jobse.Update()
!            END
!            IF jobe:ExchangedATRRC = TRUE
!              !Exchanged at RRC!
!              exchange#=1
!              jobe:InvoiceExchangeRate = jobe:ExchangeRate
!              Access:Jobse.Update()
!            END
!
!            IF handling# = 0
!              jobe:InvoiceHandlingFee = 0
!              jobe:HandlingFee = 0
!              Access:Jobse.Update()
!            END
!            IF exchange# = 0
!              jobe:InvoiceExchangeRate = 0
!              jobe:ExchangeRate = 0
!              Access:Jobse.Update()
!            END

            calc:HandlingFee[Cha_RRC]          = jobe:InvoiceHandlingFee
            calc:ExchangeFee[Cha_RRC]          = jobe:InvoiceExchangeRate

            calc:VAT[Cha_RRC]                  = (jobe:InvRRCCLabourCost   * (inv:RRCVatRateLabour / 100))
            calc:VAT[Cha_RRC]                 += (jobe:InvRRCCPartsCost    * (inv:RRCVatRateParts  / 100))
            calc:VAT[Cha_RRC]                 += (job:Invoice_Courier_Cost * (inv:RRCVatRateParts  / 100))

            calc:TotalJobValue[Cha_RRC]        = jobe:InvRRCCLabourCost + jobe:InvRRCCPartsCost + job:Invoice_Courier_Cost + calc:VAT[Cha_RRC]
            !-----------------------------------------------------------------
            calc:SparesSellingPrice[Cha_ARC]   = job:Invoice_Parts_Cost
            calc:LabourRate[Cha_ARC]           = job:Invoice_Labour_Cost
            calc:LostLoan[Cha_ARC]             = job:Invoice_Courier_Cost
            calc:HandlingFee[Cha_ARC]          = jobe:InvoiceHandlingFee
            calc:ExchangeFee[Cha_ARC]          = jobe:InvoiceExchangeRate

            calc:VAT[Cha_ARC]                  = (job:Invoice_Labour_Cost  * (inv:Vat_Rate_Labour / 100))
            calc:VAT[Cha_ARC]                 += (job:Invoice_Parts_Cost   * (inv:Vat_Rate_Parts  / 100))
            calc:VAT[Cha_ARC]                 += (job:Invoice_Courier_Cost * (inv:Vat_Rate_Parts  / 100))

            calc:TotalJobValue[Cha_ARC]        = job:Invoice_Labour_Cost + job:Invoice_Parts_Cost + job:Invoice_Courier_Cost + calc:VAT[Cha_ARC]
            !-------------------------------------------------------------
        END !IF job:ChargeableJob = YES
        !-----------------------------------------------------------------
        wp:WarrAccessoriesCost = 0
        wp:WarrSparesCost      = 0
        wp:WarrProfit          = 0
        wp:WarrRevenue         = 0

        IF job:Warranty_Job = 'YES'
            !Expo.UpdateProgressText('In Costs - Warranty '&' Ref_number is '&clip(job:Ref_Number))

            Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
            wpr:Ref_Number = job:Ref_Number         !IN:JobNumber
            SET(wpr:Part_Number_Key, wpr:Part_Number_Key)
            LOOP                !WHILE LoadWARPARTS(job:Ref_Number, WprFirst)

                if access:Warparts.next() then break.
                if wpr:Ref_Number <> job:Ref_Number then break.

                calc:SparesSellingPrice[War_RRC]   + = wpr:quantity * wpr:RRCSaleCost
                calc:SparesSellingPrice[War_ARC]   + = wpr:quantity * wpr:Sale_Cost

                !IF LoadSTOCK(wpr:Part_Ref_Number)
                Access:STOCK.ClearKey(sto:Ref_Number_Key)
                sto:Ref_Number = wpr:Part_Ref_Number
                IF Access:STOCK.TryFetch(sto:Ref_Number_Key)
                    IF (sto:accessory = 'YES')
                        wp:WarrAccessoriesCost += wpr:quantity * wpr:purchase_cost
                    ELSE
                        wp:WarrSparesCost      += wpr:quantity * wpr:purchase_cost
                    END !IF
                END !IF
            END !LOOP

            wp:WarrRevenue = job:Sub_Total_Warranty
            wp:WarrProfit  = wp:WarrRevenue - wp:WarrAccessoriesCost - wp:WarrSparesCost
            !-----------------------------------------------------------------
            calc:AmountPaid[War_RRC]           = 0 ! No payment taken for warranty jobs.

            calc:SparesCostPrice[War_RRC]      = jobe:InvRRCWPartsCost
            calc:LabourRate[War_RRC]           = jobe:InvRRCWLabourCost
            calc:LostLoan[War_RRC]             = job:WInvoice_Courier_Cost
            !Rules for Handling fees!
            handling# = 0
            exchange# = 0
            !Better find out what the charge should be!
            JobPricingRoutine(0)
            !Expo.UpdateProgressText('Warranty - Done job pricing '&' Ref_number is '&clip(job:Ref_Number))
            !
! Using "JobPricingRoutine" should negate these checks (DBH: 06-05-2005)
!            IF SentToHub(job:ref_number) !True = sent to hub!
!              !Allocate Handling Fee!
!              handling# = 1
!            END
!            IF jobe:ExchangedATRRC = TRUE
!              exchange#=1
!              !Exchanged at RRC!
!            END
!
!            IF handling# = 0
!              jobe:InvoiceHandlingFee = 0
!              jobe:HandlingFee = 0
!!              Access:Jobse.Update()
!            ELSE
!              jobe:InvoiceHandlingFee = handling"
!              jobe:HandlingFee = handling"
!!              Access:Jobse.Update()
!            END
!            IF exchange# = 0
!              jobe:InvoiceExchangeRate = 0
!              jobe:ExchangeRate = 0
!!              Access:Jobse.Update()
!            ELSE
!              jobe:InvoiceExchangeRate = exchange"
!              jobe:ExchangeRate = exchange"
!!              Access:Jobse.Update()
!            END

! End   - Using "JobPricingRoutine" should negate these checks (DBH: 06-05-2005)

            calc:HandlingFee[War_RRC]          = jobe:InvoiceHandlingFee
            calc:ExchangeFee[War_RRC]          = jobe:InvoiceExchangeRate

            calc:VAT[War_RRC]                  = (jobe:InvRRCWLabourCost    * (inv:RRCVatRateLabour / 100))
            calc:VAT[War_RRC]                 += (jobe:InvRRCWPartsCost     * (inv:RRCVatRateParts  / 100))
            calc:VAT[War_RRC]                 += (job:WInvoice_Courier_Cost * (inv:RRCVatRateRetail / 100))

            calc:TotalJobValue[War_RRC]        = jobe:InvRRCWLabourCost + jobe:InvRRCWPartsCost + job:WInvoice_Courier_Cost + calc:VAT[War_RRC]
            !-------------------------------------------------------------
            calc:AmountPaid[War_ARC]           = 0 ! No payment taken for warranty jobs.

            calc:SparesCostPrice[War_ARC]      = job:WInvoice_Parts_Cost
            calc:LabourRate[War_ARC]           = job:WInvoice_Labour_Cost
            calc:LostLoan[War_ARC]             = job:WInvoice_Courier_Cost
            calc:HandlingFee[War_ARC]          = jobe:InvoiceHandlingFee
            calc:ExchangeFee[War_ARC]          = jobe:InvoiceExchangeRate

            calc:VAT[War_ARC]                  = (job:WInvoice_Labour_Cost  * (inv:Vat_Rate_Labour / 100))
            calc:VAT[War_ARC]                 += (job:WInvoice_Parts_Cost   * (inv:Vat_Rate_Parts  / 100))
            calc:VAT[War_ARC]                 += (job:WInvoice_Courier_Cost * (inv:Vat_Rate_Parts  / 100))

            calc:TotalJobValue[War_ARC]        = job:WInvoice_Labour_Cost + job:WInvoice_Parts_Cost + job:WInvoice_Courier_Cost + calc:VAT[War_ARC]
            !-------------------------------------------------------------
        END !IF job:WarrantyJob = YES
        !-----------------------------------------------------------------
    EXIT
!--------------------------------------------------
JobsLoop                    Routine

    if ReportCancelled = true then exit.

    !progress window only being used here
    if not Automatic
        
        Expo.UpdateProgressText('===============')
        Expo.UpdateProgressText('Jobs Export Started For Warranty Sheet ')
        Expo.UpdateProgressText()
        loopcount# = 30
    END

    e1.SelectWorkSheet('AA20_Warranty')        !right sheet
    WarActiveRow = 12                          !right starting line

    Access:JOBS.ClearKey(job:DateCompletedKey)
    job:Date_Completed   = LOC:StartDate
    SET(job:DateCompletedKey, job:DateCompletedKey)
    !------------------------------------------
    LOOP WHILE Access:JOBS.Next() = Level:Benign
        !-------------------------------------------------------------

        if not Automatic
            if loopCount# > 25 then
                if Expo.UpdateProgressWindow()
                    Expo.UpdateProgressText('===============')
                    Expo.UpdateProgressText('Report interrupted ' & FORMAT(Today(),@d6) & |
                        ' ' & FORMAT(clock(),@t1))
                    Expo.FinishProgress()
                    ReportCancelled = true
                    break

                END


                IF (Expo.CancelPressed = 2)
                    Expo.UpdateProgressText('===============')
                    Expo.UpdateProgressText('Report Cancelled: ' & FORMAT(Today(),@d6) & |
                        ' ' & FORMAT(clock(),@t1))
                    Expo.FinishProgress()
                    ReportCancelled = true
                    break
                END

                !expo.UpdateProgresstext('Working on Job number '&clip(job:Ref_Number))
                loopcount# = 0
            END !if loopcount# > 25
        END

        loopcount# += 1

        IF job:Date_Completed > LOC:EndDate
            BREAK
        END !IF

        IF job:Warranty_Job <> 'YES'
            cycle
        END !IF


       !get the matching webjob record
        Access:Webjob.clearkey(wob:RefNumberKey)
        wob:RefNumber = job:Ref_Number
        IF Access:WEBJOB.Fetch(wob:RefNumberKey) then cycle.

        !reset the defaults
        calc:SentToARC         = 'NO'
        calc:ARCDateBooked     = ''
        calc:DeliveryDateAtARC = ''

        !write the data if this is booked by ARC or delivered to ARC
        IF wob:HeadAccountNumber = LocalHeadAccount
            
            DO WriteColumns_Warranty
            
        ELSE

            !find if this was ever sent to the ARC
            Access:LOCATLOG.ClearKey(lot:DateKey)
            lot:RefNumber = job:Ref_Number
            lot:TheDate   = 0
            SET(lot:DateKey, lot:DateKey)
            Loop

                IF Access:LOCATLOG.NEXT() then break.
                if lot:RefNumber <> job:Ref_Number then break.

                IF CLIP(lot:NewLocation) = CLIP(rrc:ARCLocation)
                    calc:SentToARC         = 'YES'
                    calc:ARCDateBooked     = lot:TheDate
                    calc:DeliveryDateAtARC = lot:TheDate
                    Break
                END !IF

            END !loop through Locatlog

            if calc:SentToARC  = 'YES' then
                Do WriteColumns_Warranty
            END

        END !IF wob:HeadAccountNumber = LocalHeadAccount


    END !LOOP through jobs

    EXIT
WriteColumns_Warranty                               ROUTINE
    DATA
Cha_RRC LONG(1)
Cha_ARC LONG(2)
War_RRC LONG(3)
War_ARC LONG(4)
    CODE

        !will need jobse
        Access:JOBSE.ClearKey(jobe:RefNumberKey)
        jobe:RefNumber = job:Ref_Number
        IF Access:JOBSE.Fetch(jobe:RefNumberKey) then exit.

        !-----------------------------------------------------------------
        GetSubAccount(job:Account_Number)
        !GetHeadAccount(wob:HeadAccountNumber)  ! saQ:HeadAccountNumber ! wob:HeadAccountNumber called as part of get sub
        GetEngineer(job:Engineer)

        !-----------------------------------------------------------------
        calc:x3rdPartyRepairCosts = jobe:ARC3rdPartyCost ! jobe:InvARC3rdPartCost ! jobe:ARC3rdPartyCost
        calc:x3rdPartyRepairVAT   = jobe:ARC3rdPartyVAT
        !Expo.UpdateProgressText('Before calculating costs')
        DO Calculate_ChaWar_ARCRRC_Costs_WarrantyProvision
        !Expo.UpdateProgressText('Got costs')

        !LOC:FranchiseJobNumber = GetWebJobNumber(job:Ref_Number)
        !=================================================================
        !WriteDebug('WriteColumns(OK)')
        !RecordCount += 1
            !-------------------------------------------------------------
            e1.WriteToCell( job:Ref_Number,'A'&clip(WarActiveRow)                       ) ! SB Job Number
           !Add column to show Engineer Option - 3788 (DBH: 15-04-2004)
            Case jobe:Engineer48HourOption
                Of 1
                    e1.WriteToCell('48H','B'&clip(WarActiveRow))
                Of 2
                    e1.WriteToCell('ARC','B'&clip(WarActiveRow))
                Of 3
                    e1.WriteToCell('7DT','B'&clip(WarActiveRow))
                Of 4
                    e1.WriteToCell('STD','B'&clip(WarActiveRow))
!                Else
!                    e1.WriteToCell('')
            End !Case jobe:Engineer48HourOption

            e1.WriteToCell( HaQ:BranchIdentification,'C'&clip(WarActiveRow)             ) ! Franchise Branch Number
            e1.WriteToCell( wob:JobNumber,'D'&clip(WarActiveRow)                        ) ! Franchise Job Number
            !IF LOC:RepairCentreType = 'ARC'
            !  e1.WriteToCell( 'NO'                       ) ! Sent To ARC
           !   e1.WriteToCell( '' ) ! Date Received At ARC   calc:ARCDateBooked   calc:DeliveryDateAtARC
           ! ELSE
              e1.WriteToCell( calc:SentToARC,'E'&clip(WarActiveRow)                       ) ! Sent To ARC
              e1.WriteToCell( Format(calc:DeliveryDateAtARC,@d08),'F'&clip(WarActiveRow) ) ! Date Received At ARC   calc:ARCDateBooked   calc:DeliveryDateAtARC
           ! END
!            e1.WriteToCell( calc:SentToARC                       ) ! Sent To ARC
!            e1.WriteToCell( Format(calc:DeliveryDateAtARC) ) ! Date Received At ARC   calc:ARCDateBooked   calc:DeliveryDateAtARC

           if job:Exchange_Unit_Number = 0
                e1.WriteToCell('NO','G'&clip(WarActiveRow)            )
            else
                if jobe:ExchangedATRRC
                    e1.WriteToCell('RRC','G'&clip(WarActiveRow)            )
                else
                    e1.WriteToCell('ARC','G'&clip(WarActiveRow)            )
                end
            end

            IF LoanAttachedToJob(job:ref_number) = 1
              e1.WriteToCell('YES','H'&clip(WarActiveRow)            )
            ELSE
              !No Loan ever
              e1.WriteToCell('NO','H'&clip(WarActiveRow)            )
            END

            e1.WriteToCell( calc:x3rdPartyRepairCosts,'I'&clip(WarActiveRow)            )   ! 3rd Party Repair Cost
            e1.WriteToCell( calc:x3rdPartyRepairVAT,'J'&clip(WarActiveRow)              )   ! 3rd Party Repair VAT

            !IF LOC:RepairCentreType = 'ARC'
                e1.WriteToCell( calc:SparesCostPrice[War_ARC],'K'&clip(WarActiveRow)    ) ! Spares Cost Price
                e1.WriteToCell( calc:SparesSellingPrice[War_ARC],'L'&clip(WarActiveRow)                    ) ! Spares Selling Price
                e1.WriteToCell( calc:LabourRate[War_ARC] , 'M'&clip(WarActiveRow)                          )
                e1.WriteToCell( calc:LostLoan[War_ARC] , 'N'&clip(WarActiveRow)                            ) ! Labour Rate
                e1.WriteToCell( job:Courier_Cost_Warranty , 'O'&clip(WarActiveRow)                        )
                e1.WriteToCell( calc:VAT[War_ARC] , 'P'&clip(WarActiveRow)                                 ) ! VAT
                e1.WriteToCell( calc:TotalJobValue[War_ARC],'Q'&clip(WarActiveRow)                         ) ! Total Job Value
                e1.WriteToCell( calc:HandlingFee[War_ARC]+calc:ExchangeFee[War_ARC],'R'&clip(WarActiveRow) ) ! Handling / Exchange Fee
!            ELSE
!                e1.WriteToCell( calc:SparesCostPrice[War_RRC],'K'&clip(WarActiveRow)                       ) ! Spares Cost Price
!!                IF calc:x3rdPartyRepairCosts > 0
!!                  calc:SparesSellingPrice[War_RRC] = 0
!!                END
!                e1.WriteToCell( calc:SparesSellingPrice[War_RRC],'L'&clip(WarActiveRow)                    ) ! Spares Selling Price
!                e1.WriteToCell( calc:LabourRate[War_RRC],'M'&clip(WarActiveRow)                            ) ! Labour Rate
!                e1.WriteToCell( calc:LostLoan[War_RRC],'N'&clip(WarActiveRow)                              ) ! Labour Rate
!                e1.WriteToCell( job:Courier_Cost_Warranty,'O'&clip(WarActiveRow)                          )
!                e1.WriteToCell( calc:VAT[War_RRC],'P'&clip(WarActiveRow)                                   ) ! VAT
!                e1.WriteToCell( calc:TotalJobValue[War_RRC],'Q'&clip(WarActiveRow)                         ) ! Total Job Value
!                e1.WriteToCell( calc:HandlingFee[War_RRC]+calc:ExchangeFee[War_RRC],'R'&clip(WarActiveRow) ) ! Handling / Exchange Fee
!            END !IF

! Changing (DBH 07/09/2007) # 8708 - Show the status based on the webjob
!            e1.WriteToCell( GetClaimStatus(job:EDI)              ) ! Claim Status
! to (DBH 07/09/2007) # 8708
            If wob:EDI = 'XXX'
                e1.WriteToCell('EXCLUDED','S'&clip(WarActiveRow))
            Else ! If wob:EDI = 'XXX'
                e1.WriteToCell(WOBEDIStatus(wob:EDI),'S'&clip(WarActiveRow))
            End ! If wob:EDI = 'XXX'
! End (DBH 07/09/2007) #8708
            e1.WriteToCell( job:EDI_Batch_Number,'T'&clip(WarActiveRow)                 ) ! Warranty Batch Number


            e1.WriteToCell( job:Warranty_Charge_Type,'U'&clip(WarActiveRow)             ) ! Warranty Charge Type
            e1.WriteToCell( job:Repair_Type_Warranty ,'V'&clip(WarActiveRow)            ) ! Warranty Repair Type
            !Access:The Manufact File!
            Access:Manufact.ClearKey(man:Manufacturer_Key)
            man:Manufacturer = job:manufacturer
            Access:Manufact.Fetch(man:Manufacturer_Key)
            IF job:Warranty_Charge_Type <> 'WARRANTY (2ND YR)'
              !
              e1.WriteToCell( man:EDI_Account_Number,'W'&clip(WarActiveRow)                   ) ! Invoice To Account Number
              e1.WriteToCell( man:Manufacturer,'X'&clip(WarActiveRow)                      ) ! Invoice To Account Name
            ELSE
              e1.WriteToCell( 'AA40-80696','W'&clip(WarActiveRow)  ) ! Invoice To Account Number
              e1.WriteToCell( 'NOVA RISK C/O CELLSURE','X'&clip(WarActiveRow)                     ) ! Invoice To Account Name
            END
            IF LoadTRDPARTY(job:Third_Party_Site)
                e1.WriteToCell( trd:Account_Number,'Y'&clip(WarActiveRow)               ) ! Third Party Account Number
                e1.WriteToCell( trd:Company_Name ,'Z'&clip(WarActiveRow)                ) ! Third Party Account Name
                e1.WriteToCell( jobe:ARC3rdPartyInvoiceNumber,'AA'&clip(WarActiveRow)    )
            ELSE
                !e1.WriteToCell( ''                               ) ! Third Party Account Number
                !e1.WriteToCell( ''                               ) ! Third Party Account Name
                !e1.WriteToCell( ''                               )
            END !IF
            e1.WriteToCell( Format(job:date_booked,@d08),'AB'&clip(WarActiveRow)        ) ! Date Booked
            e1.WriteToCell( Format(job:Date_Completed,@d08),'AC'&clip(WarActiveRow)     ) ! Date Completed
            e1.WriteToCell( job:Manufacturer,'AD'&clip(WarActiveRow)                     ) ! Manufacturer
            e1.WriteToCell( job:Model_Number,'AE'&clip(WarActiveRow)                     ) ! Model
            !set next cell as text
            e1.SetCellNumberFormat(oix:NumberFormatText,,0,,'AF'&WarActiveRow)
            e1.WriteToCell( job:ESN ,'AF'&clip(WarActiveRow)                      ) ! IMEI Number
            !-------------------------------------------------------------
        !-----------------------------------------------------------------
        WarActiveRow += 1
        !Expo.UpdateProgressText('Written record to excel')

    EXIT
Calculate_ChaWar_ARCRRC_Costs_WarrantyProvision                      ROUTINE
    DATA
ParFirst LONG(True)
WprFirst LONG(True)
JptFirst LONG(True)

Cha_RRC LONG(1)
Cha_ARC LONG(2)
War_RRC LONG(3)
War_ARC LONG(4)
    CODE
        !-----------------------------------------------------------------

        !-----------------------------------------------------------------
        CLEAR(ChaWar_ARCRRC_Group[Cha_RRC])
        CLEAR(ChaWar_ARCRRC_Group[Cha_ARC])
        CLEAR(ChaWar_ARCRRC_Group[War_RRC])
        CLEAR(ChaWar_ARCRRC_Group[War_ARC])
        !-----------------------------------------------------------------
        wp:NonWAccessoriesCost = 0
        wp:NonWSparesCost      = 0
        wp:NonWProfit          = 0
        wp:NonWRevenue         = 0
        !-----------------------------------------------------------------
        wp:WarrAccessoriesCost = 0
        wp:WarrSparesCost      = 0
        wp:WarrProfit          = 0
        wp:WarrRevenue         = 0

        IF job:Warranty_Job = 'YES'

            !LOOP WHILE LoadWARPARTS(job:Ref_Number, WprFirst)
            Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
            wpr:Ref_Number = job:Ref_Number
            SET(wpr:Part_Number_Key, wpr:Part_Number_Key)
            Loop
                IF Access:WARPARTS.NEXT() then break.
                if wpr:Ref_Number <> job:Ref_Number then break.

                calc:SparesCostPrice[War_RRC]    += wpr:quantity * wpr:RRCAveragePurchaseCost
                calc:SparesCostPrice[War_ARC]    += wpr:quantity * wpr:AveragePurchaseCost

                
                Access:STOCK.ClearKey(sto:Ref_Number_Key)
                sto:Ref_Number = wpr:Part_Ref_Number ! wpr:Part_Ref_Number
                IF Access:STOCK.Fetch(sto:Ref_Number_Key) = level:Benign
                    IF (sto:accessory = 'YES')
                        wp:WarrAccessoriesCost += wpr:quantity * wpr:purchase_cost
                    ELSE
                        wp:WarrSparesCost      += wpr:quantity * wpr:purchase_cost
                    END !IF
                END !IF

            END !LOOP
            !Expo.UpdateProgressText('Got WarParts')

            wp:WarrRevenue = job:Sub_Total_Warranty
            wp:WarrProfit  = wp:WarrRevenue - wp:WarrAccessoriesCost - wp:WarrSparesCost
            !-----------------------------------------------------------------
            calc:SparesSellingPrice[War_RRC]      = jobe:RRCWPartsCost
            calc:LabourRate[War_RRC]           = jobe:RRCWLabourCost
            calc:LostLoan[War_RRC]             = job:WInvoice_Courier_Cost
            !Better Check!

            JobPricingRoutine(0)
            !Expo.UpdateProgressText('Got JobPricingRoutine')
! Start - Using "JobPricingRoutine" should negate these cheks (DBH: 06-05-2005)
!            !Pricing_Routine('W',labour",parts",pass",claim",handling",exchange",RRCRate",RRCParts")
!            !Rules for Handling fees!
!            handling# = 0
!            exchange# = 0
!            IF SentToHub(job:ref_number) AND wob:HeadAccountNumber <>  LocalHeadAccount!True = sent to hub!
!              !Allocate Handling Fee!
!              handling# = 1
!            END
!            IF jobe:ExchangedATRRC = TRUE
!              exchange#=1
!              !Exchanged at RRC!
!            END
!            IF handling# = 0
!              jobe:InvoiceHandlingFee = 0
!              jobe:HandlingFee = 0
!!              Access:Jobse.Update()
!            ELSE
!              jobe:InvoiceHandlingFee = handling"
!              jobe:HandlingFee = handling"
!!              Access:Jobse.Update()
!            END
!            IF exchange# = 0
!              jobe:InvoiceExchangeRate = 0
!              jobe:ExchangeRate = 0
!!              Access:Jobse.Update()
!            ELSE
!              jobe:InvoiceExchangeRate = exchange"
!              jobe:ExchangeRate = exchange"
!!              Access:Jobse.Update()
!            END

! End   - Using "JobPricingRoutine" should negate these cheks (DBH: 06-05-2005)
            calc:HandlingFee[War_RRC]          = jobe:HandlingFee
            calc:ExchangeFee[War_RRC]          = jobe:ExchangeRate

            calc:VAT[War_RRC]                  = (jobe:RRCWLabourCost    * saQ:LabourVATRate)
            calc:VAT[War_RRC]                 += (calc:SparesSellingPrice[War_RRC]     * saQ:PartsVATRate)
            calc:VAT[War_RRC]                 += (job:Courier_Cost_Warranty * saQ:PartsVATRate)

            calc:TotalJobValue[War_RRC]        = jobe:RRCWLabourCost + calc:SparesSellingPrice[War_RRC] + job:Courier_Cost_Warranty + calc:VAT[War_RRC]
            calc:AmountPaid[War_RRC]           = 0 ! No payment taken for warranty jobs.
            !-------------------------------------------------------------
            calc:SparesSellingPrice[War_ARC]      = job:Parts_Cost_Warranty
            calc:LabourRate[War_ARC]           = job:Labour_Cost_Warranty
            calc:LostLoan[War_RRC]             = job:WInvoice_Courier_Cost
            calc:HandlingFee[War_ARC]          = jobe:HandlingFee
            calc:ExchangeFee[War_ARC]          = jobe:ExchangeRate

            calc:VAT[War_ARC]                  = (job:Labour_Cost_Warranty * saQ:LabourVATRate)
            calc:VAT[War_ARC]                 += (calc:SparesSellingPrice[War_ARC]   * saQ:PartsVATRate)
            calc:VAT[War_ARC]                 += (job:Courier_Cost_Warranty * saQ:PartsVATRate)

            calc:TotalJobValue[War_ARC]        = job:Labour_Cost_Warranty + calc:SparesSellingPrice[War_ARC] + job:Courier_Cost_Warranty + calc:VAT[War_ARC]
            calc:AmountPaid[War_ARC]           = 0 ! No payment taken for warranty jobs.
            !-------------------------------------------------------------
        END !IF Warranty job
        !-----------------------------------------------------------------
        !Expo.UpdateProgressText('finished pricing bit')
    EXIT
!--------------------------------------------------
ColumnHeadings                      Routine               !NEW

    !Warranty sheet
    E1.RenameWorkSheet('AA20_Warranty')

    !sheet titles
    e1.writeToCell('ARC Income Report AA20','A1')
    e1.writeToCell('Criteria',             'A3')
    e1.writeToCell(tmp:VersionNumber,      'B3')
    e1.writeToCell('Completed Date From:', 'A4')
    e1.writeToCell(format(LOC:StartDate,@d08),'B4')
    e1.writeToCell('Completed Date To:',   'A5')
    e1.writeToCell(format(LOC:EndDate,@d08),'B5')
    e1.writeToCell('Created By:',          'A6')
    e1.writeToCell(LOC:UserName,           'B6')
    e1.writeToCell('Created Date:',        'A7')
    e1.writeToCell(format(today(),@d08),   'B7')
    e1.writeToCell('Section Name',         'A9')
    e1.writeToCell('ARC Income Report AA20 Warranty', 'B9')
    e1.writeToCell('Total Records',        'E9')
    e1.writeToCell('Showing',              'G9')

    !Colour rows 1,3,4-7,9,11
    e1.SetCellBackgroundColor(color:silver,'A1','AF1')
    e1.SetCellBorders('A1','AF1',oix:BorderEdgeBottom,oix:LineStyleContinuous)

    e1.SetCellBackgroundColor(color:silver,'A3','AF3')
    e1.SetCellBorders('A3','AF3',oix:BorderEdgeTop   ,oix:LineStyleContinuous)
    e1.SetCellBorders('A3','AF3',oix:BorderEdgeBottom,oix:LineStyleContinuous)

    e1.SetCellBackgroundColor(color:silver,'A4','AF7')
    e1.SetCellBorders('A7','AF7',oix:BorderEdgeBottom,oix:LineStyleContinuous)

    e1.SetCellBackgroundColor(color:silver,'A9','AF9')
    e1.SetCellBorders('A9','AF9',oix:BorderEdgeBottom,oix:LineStyleContinuous)
    e1.SetCellBorders('A9','AF9',oix:BorderEdgeTop,oix:LineStyleContinuous)

    e1.SetCellBackgroundColor(color:silver,'A11','AF11')
    e1.SetCellBorders('A11','AF11',oix:BorderEdgeBottom ,oix:LineStyleContinuous)
    e1.SetCellBorders('A11','AF11',oix:BorderEdgeTop    ,oix:LineStyleContinuous)
    e1.SetCellBorders('A11','AF11',oix:BorderEdgeRight  ,oix:LineStyleContinuous)

    !set some font sizes and boldness
    e1.SetCellFontSize(14,'A1')             !bold font and size for A1
    e1.SetCellFontStyle('Bold','A1')
    e1.SetCellFontStyle('Bold','A3')
    e1.SetCellFontSize(8,'B3')
    e1.SetCellFontStyle('Bold','B9')


    !column headings
    e1.writeToCell('SB Job Number','A11')
    e1.writeToCell('Engineer Option','B11')
    e1.writeToCell('Franchise Branch Number','C11')
    e1.writeToCell('Franchise Job Number','D11')
    e1.writeToCell('Sent to ARC','E11')
    e1.writeToCell('Date Received at ARC','F11')
    e1.writeToCell('Exchanged','G11')
    e1.writeToCell('Loan','H11')
    e1.writeToCell('3rd Party Repair Cost','I11')
    e1.writeToCell('3rd Party Repair VAT','J11')
    e1.writeToCell('Spares Cost Price','K11')
    e1.writeToCell('Spares Selling Price','L11')
    e1.writeToCell('Labour Rate','M11')
    e1.writeToCell('Lost Loan Fee','N11')
    e1.writeToCell('Manufacturer Exchange Fee','O11')
    e1.writeToCell('VAT','P11')
    e1.writeToCell('Total Job Value','Q11')
    e1.writeToCell('Handling / Exchange Fee','R11')
    e1.writeToCell('Claim Status','S11')
    e1.writeToCell('Warranty Batch Number','T11')
    e1.writeToCell('Warranty Charge Type','U11')
    e1.writeToCell('Warranty Repair Type','V11')
    e1.writeToCell('Invoice to Account Number','W11')
    e1.writeToCell('Invoice to Account Name','X11')
    e1.writeToCell('Third Party Account Number','Y11')
    e1.writeToCell('Third Party Account Name','Z11')
    e1.writeToCell('Third Party Invoice Number','AA11')
    e1.writeToCell('Date Booked','AB11')
    e1.writeToCell('Date Completed','AC11')
    e1.writeToCell('Manufacturer','AD11')
    e1.writeToCell('Model ','AE11')
    e1.writeToCell('IMEI Number ','AF11')


    e1.SetColumnWidth('A','','20')
    e1.SetColumnWidth('B','','13')
    e1.SetColumnWidth('C','','12')
    e1.SetColumnWidth('D','','10')
    e1.SetColumnWidth('E','','8')
    e1.SetColumnWidth('F','','13')
    e1.SetColumnWidth('G','','8')
    e1.SetColumnWidth('H','','10')
    e1.SetColumnWidth('I','','12')
    e1.SetColumnWidth('J','','12')
    e1.SetColumnWidth('K','','12')
    e1.SetColumnWidth('L','','12')
    e1.SetColumnWidth('M','','12')
    e1.SetColumnWidth('N','','12')
    e1.SetColumnWidth('O','','14')
    e1.SetColumnWidth('P','','12')
    e1.SetColumnWidth('Q','','12')
    e1.SetColumnWidth('R','','12')
    e1.SetColumnWidth('S','','12')
    e1.SetColumnWidth('T','','10')
    e1.SetColumnWidth('U','','20')
    e1.SetColumnWidth('V','','20')
    e1.SetColumnWidth('W','','20')
    e1.SetColumnWidth('X','','20')
    e1.SetColumnWidth('Y','','20')
    e1.SetColumnWidth('Z','','20')
    e1.SetColumnWidth('AA','','20')
    e1.SetColumnWidth('AB','','13')
    e1.SetColumnWidth('AC','','13')
    e1.SetColumnWidth('AD','','20')
    e1.SetColumnWidth('AE','','20')
    e1.SetColumnWidth('AF','','20')

    !Align the Total Records right
    e1.SetCellAlignment(oix:HorizontalAlignment, oix:xlRight,'E9')

!This does not work here - try after the cells have been filled in - OK button pressed
!    e1.SelectCells ('A11')
!    e1.AutoFilter ('A11','AH11')

    !Start a new sheet in the excel file - the non-warrantry stuff
    e1.InsertWorksheet()
    e1.RenameWorkSheet('AA20')
    e1.SelectWorkSheet('AA20')



    !sheet titles
    e1.writeToCell('ARC Income Report AA20','A1')
    e1.writeToCell('Criteria',             'A3')
    e1.writeToCell(tmp:VersionNumber,      'B3')
    e1.writeToCell('Completed Date From:', 'A4')
    e1.writeToCell(format(LOC:StartDate,@d08),'B4')
    e1.writeToCell('Completed Date To:',   'A5')
    e1.writeToCell(format(LOC:EndDate,@d08),'B5')
    e1.writeToCell('Created By:',          'A6')
    e1.writeToCell(LOC:UserName,           'B6')
    e1.writeToCell('Created Date:',        'A7')
    e1.writeToCell(format(today(),@d08),   'B7')
    e1.writeToCell('Section Name',         'A9')
    e1.writeToCell('ARC Income Report AA20', 'B9')
    e1.writeToCell('Total Records',        'E9')
    e1.writeToCell('Showing',              'G9')

    !Colour rows 1,3,4-7,9,11
    e1.SetCellBackgroundColor(color:silver,'A1','AH1')
    e1.SetCellBorders('A1','AH1',oix:BorderEdgeBottom,oix:LineStyleContinuous)

    e1.SetCellBackgroundColor(color:silver,'A3','AH3')
    e1.SetCellBorders('A3','AH3',oix:BorderEdgeTop   ,oix:LineStyleContinuous)
    e1.SetCellBorders('A3','AH3',oix:BorderEdgeBottom,oix:LineStyleContinuous)

    e1.SetCellBackgroundColor(color:silver,'A4','AH7')
    e1.SetCellBorders('A7','AH7',oix:BorderEdgeBottom,oix:LineStyleContinuous)

    e1.SetCellBackgroundColor(color:silver,'A9','AH9')
    e1.SetCellBorders('A9','AH9',oix:BorderEdgeBottom,oix:LineStyleContinuous)
    e1.SetCellBorders('A9','AH9',oix:BorderEdgeTop,oix:LineStyleContinuous)

    e1.SetCellBackgroundColor(color:silver,'A11','AH11')
    e1.SetCellBorders('A11','AH11',oix:BorderEdgeBottom ,oix:LineStyleContinuous)
    e1.SetCellBorders('A11','AH11',oix:BorderEdgeTop    ,oix:LineStyleContinuous)
    e1.SetCellBorders('A11','AH11',oix:BorderEdgeRight  ,oix:LineStyleContinuous)

    !set some font sizes and boldness
    e1.SetCellFontSize(14,'A1')             !bold font and size for A1
    e1.SetCellFontStyle('Bold','A1')
    e1.SetCellFontStyle('Bold','A3')
    e1.SetCellFontSize(8,'B3')
    e1.SetCellFontStyle('Bold','B9')


    !column headings
    e1.writeToCell('Invoice Number','A11')
    e1.writeToCell('Invoice Date','B11')
    e1.writeToCell('SB Job Number','C11')
    e1.writeToCell('Engineer Option','D11')
    e1.writeToCell('Franchise Branch Number','E11')
    e1.writeToCell('Franchise Job Number','F11')
    e1.writeToCell('Sent to ARC','G11')
    e1.writeToCell('Date Received At ARC','H11')
    e1.writeToCell('Exchanged','I11')
    e1.writeToCell('Loan','J11')
    e1.writeToCell('3rd Party Repair Cost','K11')
    e1.writeToCell('3rd Party Repair VAT','L11')
    e1.writeToCell('Spares Cost Price','M11')
    e1.writeToCell('Spares Selling Price','N11')
    e1.writeToCell('Labour Rate','O11')
    e1.writeToCell('Lost Loan Fee','P11')
    e1.writeToCell('Manufacturer Exchange Fee','Q11')
    e1.writeToCell('VAT','R11')
    e1.writeToCell('Total Job Value','S11')
    e1.writeToCell('Amount Paid','T11')
    e1.writeToCell('Handling / Exchange Fee','U11')
    e1.writeToCell('Job Type','V11')
    e1.writeToCell('Chargeable Charge Type','W11')
    e1.writeToCell('Chargeable Repair Type','X11')
    e1.writeToCell('Invoice To Account Number','Y11')
    e1.writeToCell('Invoice To Account Name','Z11')
    e1.writeToCell('Third Party Account Number','AA11')
    e1.writeToCell('Third Party Account Name','AB11')
    e1.writeToCell('Third Party Invoice Number','AC11')
    e1.writeToCell('Date Booked','AD11')
    e1.writeToCell('Date Completed','AE11')
    e1.writeToCell('Manufacturer','AF11')
    e1.writeToCell('Model','AG11')
    e1.writeToCell('IMEI Number','AH11')

    e1.SetColumnWidth('A','','20')
    e1.SetColumnWidth('B','','13')
    e1.SetColumnWidth('C','','10')
    e1.SetColumnWidth('D','','13')
    e1.SetColumnWidth('E','','10')
    e1.SetColumnWidth('F','','10')
    e1.SetColumnWidth('G','','8')
    e1.SetColumnWidth('H','','10')
    e1.SetColumnWidth('I','','13')
    e1.SetColumnWidth('J','','8')
    e1.SetColumnWidth('K','','13')
    e1.SetColumnWidth('L','','12')
    e1.SetColumnWidth('M','','12')
    e1.SetColumnWidth('N','','12')
    e1.SetColumnWidth('O','','12')
    e1.SetColumnWidth('P','','12')
    e1.SetColumnWidth('Q','','14')
    e1.SetColumnWidth('R','','12')
    e1.SetColumnWidth('S','','12')
    e1.SetColumnWidth('T','','10')
    e1.SetColumnWidth('U','','20')
    e1.SetColumnWidth('V','','10')
    e1.SetColumnWidth('W','','20')
    e1.SetColumnWidth('X','','20')
    e1.SetColumnWidth('Y','','20')
    e1.SetColumnWidth('Z','','20')
    e1.SetColumnWidth('AA','','20')
    e1.SetColumnWidth('AB','','20')
    e1.SetColumnWidth('AC','','20')
    e1.SetColumnWidth('AD','','13')
    e1.SetColumnWidth('AE','','13')
    e1.SetColumnWidth('AF','','20')
    e1.SetColumnWidth('AG','','20')
    e1.SetColumnWidth('AH','','20')

    !Align the Total Records right
    e1.SetCellAlignment(oix:HorizontalAlignment, oix:xlRight,'E9')


    !start adding data at row 12
    ActiveRow = 12
    WarActiveRow = 12


    Exit
GetUserName2                        ROUTINE           !Original
    DATA
CommandLine STRING(255)
    CODE
        !-----------------------------------------------
        If Command('/SCHEDULE')
            Automatic = False
            Exit
        End ! If Command('/SCHEDULE')
        CommandLine = CLIP(COMMAND(''))

!        LOC:UserName = LOC:ApplicationName
!
!        Case MessageEx('Fail User Check For "' & CLIP(CommandLine) & '"?', LOC:ApplicationName & ' DEBUG',|
!                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
!            Of 1 ! &Yes Button
!                POST(Event:CloseWindow)
!
!                EXIT
!            Of 2 ! &No Button
!                EXIT
!        End!Case MessageEx
        !-----------------------------------------------
        Automatic = FALSE
        tmpPos = INSTRING('%', CommandLine)
!        IF NOT tmpPos
!            Case MessageEx('Attempting to use Workshop Performance Report<10,13>'            & |
!                '   without using ' & LOC:ApplicationName & '.<10,13>'                       & |
!                '   Start ' & LOC:ApplicationName & ' and run the report from there.<10,13>',  |
!                LOC:ApplicationName,                                                           |
!                'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,LocalTimeOut)
!            Of 1 ! &OK Button
!            END!Case MessageEx
!
!           POST(Event:CloseWindow)
!
!           EXIT
!        END !IF tmpPos
        !-----------------------------------------------
        skip# = 0
        SET(USERS)
        Access:USERS.Clearkey(use:Password_Key)
        glo:Password = CLIP(SUB(CommandLine, tmpPos + 1, 30))
      
        Access:USERS.Clearkey(use:Password_Key)
        use:Password = glo:Password
        If Access:USERS.Tryfetch(use:Password_Key)
            !Assert(0,'<13,10>Fetch Error<13,10>')
!            Case MessageEx('Unable to find your logged in user details.', |
!                    LOC:ApplicationName,                                  |
!                    'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,LocalTimeOut)
!                Of 1 ! &OK Button
!            End!Case MessageEx
!
!            POST(Event:CloseWindow)
!
!           EXIT
            LOC:UserName = 'AUTOMATED PROCEDURE'
            Automatic = TRUE
            skip# = 1
        End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        !-----------------------------------------------
        IF skip# = 0
            LOC:UserName = use:Forename

            IF CLIP(LOC:UserName) = ''
                LOC:UserName = use:Surname
            ELSE
                LOC:UserName = CLIP(use:Forename) & ' ' & use:Surname 
            END !IF

            IF CLIP(LOC:UserName) = ''
                LOC:UserName = '<' & use:User_Code & '>'
            END !IF
            !-----------------------------------------------
        END
    EXIT
UpdateSummaryQueue                        ROUTINE
    DATA
Dummy LONG(False)
    CODE
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        ! CLEAR(Calc_Group) ! do not clear as GetInvoiceAccount() sets invoice head/sub accounts
        calc:x3rdPartyRepairCosts = jobe:ARC3rdPartyCost ! jobe:InvARC3rdPartCost ! jobe:ARC3rdPartyCost
        calc:x3rdPartyRepairVAT   = jobe:ARC3rdPartyVAT
        calc:ARCDateBooked        = ''
        calc:ARCLocation          = ''
        calc:CurrentStatus        = ''
        !calc:DeliveryDateAtARC    = ''
        !calc:InvoiceToAccountNumber    ! set by GetInvoiceAccount() 
        !calc:InvoiceToAccountName      ! set by GetInvoiceAccount() 
        !calc:InvoiceToSubAccountNumber ! set by GetInvoiceAccount() 
        !calc:InvoiceToSubAccountName   ! set by GetInvoiceAccount() 
        !calc:OldStatus            = '' !never used for anthing
        !calc:SentToARC            = ''
        !---
        !IF wob:HeadAccountNumber <> LocalHeadAccount
        !  Dummy = GetDeliveryDateAtARC(job:Ref_Number)
        !END
        !GetOldStatus(job:Ref_Number, job:Current_Status)


        !-----------------------------------------------------------------
        sq:Manufacturer  = job:Manufacturer
        GET(SummaryQueue, +sq:Manufacturer)
        IF ERRORCODE()
            ! Not in queue - ADD
            CLEAR(SummaryQueue)
                sq:Manufacturer        = job:Manufacturer

                sq:WarrantyCount       = 0
                sq:WarrRevenueCount    = 0
                sq:WarrNonRevenueCount = 0
                sq:WarrSparesCost      = 0
                sq:WarrAccessoriesCost = 0
                sq:WarrRevenue         = 0
                sq:WarrProfit          = 0

                sq:NonWarrantyCount    = 0
                sq:NonWRevenueCount    = 0
                sq:NonWNonRevenueCount = 0
                sq:NonWSparesCost      = 0
                sq:NonWAccessoriesCost = 0
                sq:NonWRevenue         = 0
                sq:NonWProfit          = 0
            ADD(SummaryQueue, +sq:Manufacturer)
        END !IF
        !-----------------------------------------------------------------
        !Expo.UpdateProgressText('Got Manufacturer '&' Ref_number is '&clip(job:Ref_Number))
        DO Calculate_ChaWar_ARCRRC_Costs
        !Expo.UpdateProgressText('Got Costs '&' Ref_number is '&clip(job:Ref_Number))

        sq:TotalUnitsRepaired  += 1
        !-----------------------------------------------------------------
        IF    (job:Warranty_Job   = 'YES')
            sq:WarrantyCount           += 1

            IF (job:Sub_Total_Warranty <= 0)
                sq:WarrNonRevenueCount += 1
            ELSE
                sq:WarrRevenueCount    += 1
            END !IF

            sq:WarrSparesCost          += wp:WarrSparesCost
            sq:WarrAccessoriesCost     += wp:WarrAccessoriesCost
            sq:WarrRevenue             += wp:WarrRevenue
            sq:WarrProfit              += wp:WarrProfit
            !Expo.UpdateProgressText('Got Warranty Job '&' Ref_number is '&clip(job:Ref_Number))
        END !IF
        !-----------------------------------------------------------------
        IF (job:Chargeable_Job = 'YES')
            sq:NonWarrantyCount        += 1

            IF (job:Sub_Total <= 0)
                sq:NonWNonRevenueCount += 1
            ELSE
                sq:NonWRevenueCount    += 1
            END !IF

            sq:NonWSparesCost          += wp:NonWSparesCost
            sq:NonWAccessoriesCost     += wp:NonWAccessoriesCost
            sq:NonWRevenue             += wp:NonWRevenue
            sq:NonWProfit              += wp:NonWProfit
            !Expo.UpdateProgressText('Got Chargeable Job '&' Ref_number is '&clip(job:Ref_Number))
        END !IF
        !-----------------------------------------------------------------
        PUT(SummaryQueue, +sq:Manufacturer)
        !-----------------------------------------------------------------
    EXIT

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020616'&'0'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ARCIncomeReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:CHARTYPE.Open
  Relate:DISCOUNT.Open
  Relate:JOBPAYMT.Open
  Relate:REPSCHCR.Open
  Relate:TRADEACC_ALIAS.Open
  Relate:VATCODE.Open
  Relate:WEBJOB.Open
  Access:USERS.UseFile
  Access:STOCK.UseFile
  Access:STOMODEL.UseFile
  Access:JOBS.UseFile
  Access:INVOICE.UseFile
  Access:WARPARTS.UseFile
  Access:TRADEACC.UseFile
  Access:PARTS.UseFile
  Access:SUBCHRGE.UseFile
  Access:TRACHRGE.UseFile
  Access:STDCHRGE.UseFile
  Access:TRACHAR.UseFile
  Access:SUBTRACC.UseFile
  Access:JOBSE.UseFile
  Access:TRDPARTY.UseFile
  Access:MANUFACT.UseFile
  Access:REPSCHED.UseFile
  Access:REPSCHLG.UseFile
  Access:LOCATLOG.UseFile
  SELF.FilesOpened = True
  OPEN(MainWindow)
  SELF.Opened=True
  ! ========= Set Report Version =============
  Include('..\ReportVersion.inc')
  tmp:VersionNumber = 'Version Number: ' & Clip(tmp:VersionNumber) & '5006'
  ?ReportVersion{prop:Text} = tmp:VersionNumber
      LOC:ProgramName         = 'ARC Income Report'
  
      LOC:StartDate      = DATE( MONTH(TODAY()), 1, YEAR(TODAY()) )
      LOC:EndDate        = TODAY() !
  
  
  !    SET(defaults)
  !    access:defaults.next()
  
      DO GetUserName2
  
      IF GUIMode = 1 THEN
         MainWindow{PROP:ICONIZE} = TRUE
         LocalTimeOut = 500
      
      END !IF
  
      LocalHeadAccount = GETINI('BOOKING','HEADACCOUNT','',CLIP(PATH()) & '\SB2KDEF.INI')
  
      IF Automatic = TRUE
        CommandLine = CLIP(COMMAND(''))
        tmpPos = INSTRING('&', CommandLine)
        LOC:StartDate      = DATE( MONTH(CLIP(SUB(CommandLine, tmpPos + 1, 30))), 1, YEAR(CLIP(SUB(CommandLine, tmpPos + 1, 30))) )
        LOC:EndDate        = CLIP(SUB(CommandLine, tmpPos + 1, 30)) !
      END
  Bryan.CompFieldColour()
  ?LOC:StartDate{Prop:Alrt,255} = MouseLeft2
  ?LOC:EndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
      IF Automatic = TRUE
  
        DO OKButton_Pressed
  
        POST(Event:CloseWindow)
      END
  SELF.SetAlerts()
  ! Inserting (DBH 31/08/2007) # 9125 - Is this an automatic report?   (Third type)
  If Command('/SCHEDULE')
      x# = Instring('%',Command(),1,1)
      Access:REPSCHED.ClearKey(rpd:RecordNumberKey)
      rpd:RecordNumber = Clip(Sub(Command(),x# + 1,20))
      If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
          !Found
          Access:REPSCHCR.ClearKey(rpc:ReportCriteriaKey)
          rpc:ReportName = rpd:ReportName
          rpc:ReportCriteriaType = rpd:ReportCriteriaType
          If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
              !Found
  
              Case rpc:DateRangeType
              Of 1 ! Today Only
                  loc:StartDate = Today()
                  loc:EndDate = Today()
              Of 2 ! 1st Of Month
                  loc:StartDate = Date(Month(Today()),1,Year(Today()))
                  loc:EndDate = Today()
              Of 3 ! Whole Month
  ! Changing (DBH 31/01/2008) # 9711 - Allow for change of year
  !                tmp:StartDate = Date(Month(Today()) - 1, 1, Year(Today()))
  !                tmp:EndDate = Date(Month(Today()),1,Year(Today())) - 1
  ! to (DBH 31/01/2008) # 9711
                  loc:EndDate = Date(Month(Today()),1,Year(Today())) - 1
                  loc:StartDate = Date(Month(loc:EndDate),1,Year(loc:EndDate))
  ! End (DBH 31/01/2008) #9711
              End ! Case rpc:DateRangeType
  
              Do OKButton_Pressed
              Post(Event:CloseWindow)
          Else ! If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
              !Error
          End ! If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
  
      Else ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
          !Error
      End ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
  
  End ! If Command('/SCHEDULE')
  ! End (DBH 31/08/2007) #9125
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
    Relate:DISCOUNT.Close
    Relate:JOBPAYMT.Close
    Relate:REPSCHCR.Close
    Relate:TRADEACC_ALIAS.Close
    Relate:VATCODE.Close
    Relate:WEBJOB.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OkButton
          DO OKButton_Pressed
    OF ?CancelButton
       POST(Event:CloseWindow)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020616'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020616'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020616'&'0')
      ***
    OF ?StartPopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          LOC:StartDate = TINCALENDARStyle1(LOC:StartDate)
          Display(?LOC:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?EndPopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          LOC:EndDate = TINCALENDARStyle1(LOC:EndDate)
          Display(?LOC:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeEvent()
    E1.TakeEvent ('', '')
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?LOC:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?StartPopCalendar)
      CYCLE
    END
  OF ?LOC:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?EndPopCalendar)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

GetEngineer                 PROCEDURE( IN:UserCode )
DoADD LONG(False)
Temp  STRING(100)
    CODE
        !-----------------------------------------------------------------
        !WriteDebug('GetEngineer(' & CLIP(IN:UserCode) & ') START')
        ! on entry IN:UserCode = User code to look up and
        !   store the details in the queue
        !-----------------------------------------------------------------
        eq:Engineer = IN:UserCode
        GET(EngineerQueue, +eq:Engineer)
        CASE ERRORCODE()
        OF 00 ! FOUND
            IF eq:Engineer = IN:UserCode
                !WriteDebug('GetEngineer(Found)')
                ! NULL
            ELSE
                !WriteDebug('GetEngineer(Partial Match - ADD)')
                DoADD = True
            END !IF
            
        OF 30
            ! Not in queue - ADD
            !WriteDebug('GetEngineer(Not in queue - ADD)')
            DoADD = True

        END !CASE
        !-----------------------------------------------------------------
        IF DoADD = True
            !WriteDebug('GetEngineer(ADD)')
            CLEAR(EngineerQueue)
                eq:Engineer      = IN:UserCode

                !IF LoadUSERS(IN:UserCode)
                access:users.clearkey(use:User_Code_Key)
                use:User_Code = in:userCode
                if access:users.fetch(use:User_Code_Key)

                    !WriteDebug('GetEngineer(LoadUSERS OK)')

                    eq:Name = clip(use:Surname)&', '&clip(use:Forename)

                ELSE
                    !WriteDebug('GetEngineer(LoadUSERS FAIL)')

                    eq:Name = job:Engineer

                END ! IF
            ADD(EngineerQueue, +eq:Engineer)
        END !IF
        !-----------------------------------------------------------------
        !WriteDebug('GetEngineer END')
        !-----------------------------------------------------------------
!LoadJOBPAYMT   PROCEDURE( IN:JobNumber, IN:First )! LONG ! BOOL
!    CODE
!        !-----------------------------------------------
!        !WriteDebug('LoadJOBPAYMT(' & IN:JobNumber & ', ' & IN:First & ')')
!
!        IF IN:First
!            IN:First = False
!            !WriteDebug('LoadJOBPAYMT(First)')
!            !-------------------------------------------
!            ! All_Date_Key             KEY(jpt:Ref_Number,                   jpt:Date ),DUP,NOCASE
!            ! Loan_Deposit_Key         KEY(jpt:Ref_Number, jpt:Loan_Deposit, jpt:Date ),DUP,NOCASE
!            !
!            Access:JOBPAYMT.ClearKey(jpt:All_Date_Key)
!                jpt:Ref_Number = IN:JobNumber
!            SET(jpt:All_Date_Key, jpt:All_Date_Key)
!            !-------------------------------------------
!        END !IF
!
!        IF NOT Access:JOBPAYMT.NEXT() = Level:Benign
!            !WriteDebug('LoadJOBPAYMT(NEXT=Fail)')
!
!            RETURN False
!        END !IF
!
!        IF NOT jpt:Ref_Number = IN:JobNumber
!            !WriteDebug('LoadJOBPAYMT(NOT Found)')
!
!            RETURN False
!        END !IF
!
!        !WriteDebug('LoadJOBPAYMT(OK)')
!
!        RETURN True
!        !-----------------------------------------------
!GetDeliveryDateAtARC        PROCEDURE( IN:JobNumber )! LONG ! BOOL
!First LONG(True)
!    CODE
!        !-----------------------------------------------------------------
!        !WriteDebug('GetDeliveryDateAtARC(' & IN:JobNumber & ')')
!
!        calc:SentToARC         = 'NO'
!        calc:ARCDateBooked     = ''
!        calc:DeliveryDateAtARC = ''
!
!        
!        Access:LOCATLOG.ClearKey(lot:DateKey)
!        lot:RefNumber = IN:JobNumber
!        SET(lot:DateKey, lot:DateKey)
!
!        LOOP
!
!            IF Access:LOCATLOG.NEXT() <> Level:Benign
!                RETURN False
!            END !IF
!
!            IF NOT lot:RefNumber = IN:JobNumber
!                RETURN False
!            END !IF
!
!            First = False
!
!            IF CLIP(lot:NewLocation) = CLIP(rrc:ARCLocation)
!                calc:SentToARC         = 'YES'
!                calc:ARCDateBooked     = lot:TheDate
!                calc:DeliveryDateAtARC = lot:TheDate
!
!                RETURN True
!            END !IF
!            !-------------------------------------------------------------
!        END !LOOP
!
!
!
!        RETURN False
!        !-----------------------------------------------------------------
LoadTRDPARTY        PROCEDURE( IN:CompanyName )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        IF CLIP(IN:CompanyName) = ''
            !WriteDebug('LoadTRDPARTY("") FAIL BLANK')

            RETURN False
        END !IF
        !-----------------------------------------------
        ! Account_Number_Key       KEY( trd:Account_Number       ),NOCASE         !By Account_Number
        ! Company_Name_Key         KEY( trd:Company_Name         ),NOCASE,PRIMARY !By Company Name
        !
        Access:TRDPARTY.ClearKey(trd:Company_Name_Key)
        trd:Company_Name = IN:CompanyName
        SET(trd:Company_Name_Key, trd:Company_Name_Key)

        IF Access:TRDPARTY.fetch(trd:Company_Name_Key) <> Level:Benign
            !WriteDebug('LoadTRDPARTY("' & CLIP(IN:CompanyName) & '") FAIL EOF')

            RETURN False
        END !IF

        IF NOT trd:Company_Name = IN:CompanyName
            !WriteDebug('LoadTRDPARTY("' & CLIP(IN:CompanyName) & '") FAIL EOI')

            RETURN False
        END !IF

        !WriteDebug('LoadTRDPARTY("' & CLIP(IN:CompanyName) & '") OK')

        RETURN True
        !-----------------------------------------------
LoadTRADEACC   PROCEDURE( IN:AccountNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        !WriteDebug('GetSubAccount("' & CLIP(IN:AccountNumber) & '")')

        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = IN:AccountNumber

        IF Access:TRADEACC.TryFetch(tra:Account_Number_Key) <> Level:Benign
            !WriteDebug('GetSubAccount(EOF)')
            RETURN False
        END !IF

        IF tra:Account_Number <> IN:AccountNumber
            !WriteDebug('GetSubAccount(EOI)')
            RETURN False
        END !IF

        !WriteDebug('GetSubAccount(OK)')
        RETURN True
        !-----------------------------------------------
LoadSUBTRACC   PROCEDURE( IN:AccountNumber )! string ! BOOL
    CODE
        !-----------------------------------------------
        !WriteDebug('GetSubAccount("' & CLIP(IN:AccountNumber) & '")')
        !Expo.UpdateProgressText('try to load SUB account for '&CLIP(IN:AccountNumber))

        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = IN:AccountNumber
        IF Access:SUBTRACC.Fetch(sub:Account_Number_Key) <> Level:Benign
            !WriteDebug('GetSubAccount(EOF)')
            !Expo.UpdateProgressText('fail not found')
            RETURN False
        END !IF

        IF sub:Account_Number <> IN:AccountNumber
            !WriteDebug('GetSubAccount(EOI)')
            !Expo.UpdateProgressText('fail wrong account')
            RETURN False
        END !IF

        !WriteDebug('GetSubAccount(OK)')
        !Expo.UpdateProgressText('worked OK')

        RETURN True
        !-----------------------------------------------
!LoadWARPARTS   PROCEDURE( IN:JobNumber, IN:First )! LONG ! BOOL
!    CODE
!        !-----------------------------------------------
!        !WriteDebug('LoadWARPARTS(' & IN:JobNumber & ', ' & IN:First & ')')
!
!        IF IN:First
!            IN:First = False
!            !WriteDebug('LoadWARPARTS(First)')
!
!            Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
!            wpr:Ref_Number = IN:JobNumber
!            SET(wpr:Part_Number_Key, wpr:Part_Number_Key)
!        END !IF
!
!        IF Access:WARPARTS.NEXT() <> Level:Benign
!            !WriteDebug('LoadWARPARTS(NEXT=Fail)')
!
!            RETURN False
!        END !IF
!
!        IF NOT wpr:Ref_Number = IN:JobNumber
!            !WriteDebug('LoadWARPARTS(NOT Found)')
!
!            RETURN False
!        END !IF
!
!        !WriteDebug('LoadWARPARTS(OK)')
!
!        RETURN True
!        !-----------------------------------------------
GetVATRate      PROCEDURE( IN:VATCode )! REAL
    CODE
        !-----------------------------------------------------------------
        Access:Vatcode.clearkey(vat:Vat_code_Key)
        vat:VAT_Code = in:VatCode
        if access:Vatcode.fetch(vat:Vat_code_Key)
            RETURN 0.00
        END ! IF

        RETURN (vat:VAT_Rate / 100)
        !-----------------------------------------------------------------  
GetInvoiceAccount       PROCEDURE()
    CODE
        !-----------------------------------------------------------------
        !WriteDebug('GetInvoiceAccount("' & inv:AccountType & '")')

        IF (inv:AccountType = 'HEA') OR (inv:AccountType = 'MAI')
            !WriteDebug('GetInvoiceAccount("HEA/MAI")')

            GetHeadAccount(inv:Account_Number)
                calc:InvoiceToAccountNumber    = haQ:AccountNumber
                calc:InvoiceToAccountName      = haQ:AccountName
                calc:InvoiceToSubAccountNumber = ''
                calc:InvoiceToSubAccountName   = ''
        ELSE
            !WriteDebug('GetInvoiceAccount(ELSE=SUB)')

            GetSubAccount(inv:Account_Number)

            GetHeadAccount(saQ:HeadAccountNumber)

            IF saQ:Generic = TRUE
              IF haQ:InvoiceSubAccounts = FALSE
                calc:InvoiceToAccountNumber    = saQ:HeadAccountNumber
                calc:InvoiceToAccountName      = saQ:HeadAccountName
              ELSE
                calc:InvoiceToAccountNumber    = saQ:AccountNumber
                calc:InvoiceToAccountName      = saQ:AccountName
              END
            ELSE
              IF calc:SentToARC = 'YES'
                  calc:InvoiceToAccountNumber    = saQ:HeadAccountNumber
                  calc:InvoiceToAccountName      = saQ:HeadAccountName
              ELSE
                  calc:InvoiceToAccountNumber    = saQ:AccountNumber
                  calc:InvoiceToAccountName      = saQ:AccountName
              END
            END
            !GetHeadAccount(saQ:HeadAccountNumber)
            !    calc:InvoiceToAccountNumber    = saQ:AccountNumber
            !    calc:InvoiceToAccountName      = saQ:AccountName
        END !IF
!-----------------------------------------------------------------
GetSubAccount PROCEDURE( IN:AccountNumber )           !job:Accountnumber
Temp          LONG
VAT_Rate_temp LIKE(vat:vat_rate)
    CODE
        !-----------------------------------------------------------------
        !WriteDebug('GetSubAccount("' & CLIP(IN:AccountNumber) & '")')
        !Expo.UpdateProgressText('Get SUB account for '&CLIP(IN:AccountNumber)&' Ref_number is '&clip(job:Ref_Number))

        saq:AccountNumber = IN:AccountNumber
        GET(SubAccount_Queue, +saq:AccountNumber)

        CASE ERRORCODE()
        OF 00 ! FOUND
            !-------------------------------------------------------------
            IF saq:AccountNumber = IN:AccountNumber
                RETURN
            END !IF
            !-------------------------------------------------------------
            !WriteDebug('GetSubAccount(Partial Match - ADD)')
            !-------------------------------------------------------------
        OF 30
            !-------------------------------------------------------------
            !WriteDebug('GetSubAccount(NOT Found - ADD)')
            !-------------------------------------------------------------
        ELSE
            !WriteDebug('GetSubAccount(Error ' & ERRORCODE() & '"' & ERROR() & '")')

            CancelPressed = True

            RETURN
        END !CASE
        !-----------------------------------------------------------------
        !WriteDebug('GetSubAccount(ADD)')

        IF NOT LoadSUBTRACC(IN:AccountNumber)
            !WriteDebug('GetSubAccount(NOT LoadSUBTRACC(job:Account_Number))')
            !Expo.UpdateProgressText('Not found subaccount details for '&clip(IN:AccountNumber))

            CLEAR(SubAccount_Queue)
                saq:AccountNumber        = IN:AccountNumber
                saq:AccountName          = 'Not Found'
                saq:HeadAccountNumber    = ''
                saQ:HeadAccountName      = ''
                saQ:BranchIdentification = ''
                saQ:VATCode              = ''
                saQ:VATRate              = 0.00
            ADD(SubAccount_Queue, +saq:AccountNumber)

            RETURN
        END !IF
        !-----------------------------------------------------------------
        GetHeadAccount(sub:Main_Account_Number)

        CLEAR(SubAccount_Queue)
            saq:AccountNumber        = IN:AccountNumber
            saq:AccountName          = sub:Company_Name
            saQ:HeadAccountNumber    = sub:Main_Account_Number
            saQ:HeadAccountName      = haQ:AccountName
            saQ:BranchIdentification = haQ:BranchIdentification
            saQ:Generic              = sub:Generic_Account

            If haQ:InvoiceSubAccounts = True
                saQ:PartsVATCode         = sub:Parts_VAT_Code
                saQ:LabourVATCode        = sub:Labour_VAT_Code
                saQ:RetailVATCode        = sub:Retail_Discount_Code

                saQ:PartsVATRate         = GetVATRate(sub:Parts_VAT_Code)
                saQ:LabourVATRate        = GetVATRate(sub:Labour_VAT_Code)
                saQ:RetailVATRate        = GetVATRate(sub:Retail_Discount_Code)

                LOC:VATCode              = saQ:PartsVATCode
            Else
                saQ:PartsVATCode         = haQ:PartsVATCode
                saQ:LabourVATCode        = haQ:LabourVATCode
                saQ:RetailVATCode        = haQ:RetailVATCode

                saQ:PartsVATRate         = haQ:PartsVATRate
                saQ:LabourVATRate        = haQ:LabourVATRate
                saQ:RetailVATRate        = haQ:RetailVATRate

                LOC:VATCode              = haQ:PartsVATCode
            End !If tra:use_sub_accounts = 'YES'

            saQ:VATCode      = LOC:VATCode
            saQ:VATRate      = GetVATRate(LOC:VatCode) * 100

!            IF LoadVATCODE(LOC:VATCode)
!                saQ:VATRate  = vat:vat_rate
!            ELSE
!                saQ:VATRate  = 0.00
!            END ! IF
        ADD(SubAccount_Queue, +saq:AccountNumber)

        !WriteDebug('GetSubAccount(OK)')
!-----------------------------------------------------------------
GetHeadAccount PROCEDURE( IN:AccountNumber )
    CODE
        !-----------------------------------------------------------------
        !WriteDebug('GetHeadAccount("' & CLIP(IN:AccountNumber) & '")')
        !Expo.UpdateProgressText('Get Head account for '&CLIP(IN:AccountNumber)&' Ref_number is '&clip(job:Ref_Number))


        haq:AccountNumber  = IN:AccountNumber
        GET(HeadAccount_Queue, +haq:AccountNumber)

        CASE ERRORCODE()
        OF 00 ! FOUND
            !-------------------------------------------------------------
            IF haq:AccountNumber  = IN:AccountNumber
                !WriteDebug('GetHeadAccount(Found)')

                RETURN
            END !IF
            !-------------------------------------------------------------
            !WriteDebug('GetHeadAccount(Partial Match - ADD)')
            !-------------------------------------------------------------
        OF 30
            !-------------------------------------------------------------
            !WriteDebug('GetHeadAccount(NOT Found - ADD)')
            !-------------------------------------------------------------
        ELSE
            !-------------------------------------------------------------
            !WriteDebug('GetHeadAccount(Error ' & ERRORCODE() & '"' & ERROR() & '")')

            CancelPressed = True

            RETURN
            !-------------------------------------------------------------
        END !IF
        !-----------------------------------------------------------------
        !WriteDebug('GetHeadAccount(ADD)')
        ! Not in queue - ADD
        CLEAR(HeadAccount_Queue)
            haq:AccountNumber            = IN:AccountNumber
            IF LoadTRADEACC(IN:AccountNumber)
                haq:AccountName          = tra:Company_Name
                haq:BranchIdentification = tra:BranchIdentification

                IF tra:Invoice_Sub_Accounts = 'YES'
                    haQ:InvoiceSubAccounts = True
                ELSE
                    haQ:InvoiceSubAccounts = False
                END !IF
                haQ:PartsVATCode         = tra:Parts_VAT_Code
                haQ:LabourVATCode        = tra:Labour_VAT_Code
                haQ:RetailVATCode        = tra:Retail_Discount_Code

                haQ:PartsVATRate         = GetVATRate(tra:Parts_VAT_Code)
                haQ:LabourVATRate        = GetVATRate(tra:Labour_VAT_Code)
                haQ:RetailVATRate        = GetVATRate(tra:Retail_Discount_Code)

            ELSE
                haq:AccountName          = '*T'
                haq:BranchIdentification = '*T'

            END !IF
        ADD(HeadAccount_Queue, +haq:AccountNumber)
!-----------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.Init   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0)
ReturnValue   byte
  CODE
  ReturnValue = PARENT.Init (pStartVisible,pEnableEvents)
  self.TakeSnapShotOfWindowPos()
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.Kill   PROCEDURE (byte pUnloadCOM=1)
ReturnValue   byte
  CODE
  self.RestoreSnapShotOfWindowPos()
  ReturnValue = PARENT.Kill (pUnloadCOM)
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.TakeEvent   PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0)
  CODE
  PARENT.TakeEvent (pEventString1,pEventString2,pEventNumber,pEventType,pEventStatus)
  if pEventType = 0  ! Generated by CapeSoft Office Inside
    case event()
      of event:accepted
        case field()
      end
    end
  end
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
!Initialise
ExcelSetup          Procedure(Byte      func:Visible)
    Code
    Excel   = Create(0,Create:OLE)
    Excel{prop:Create} = 'Excel.Application'
    Excel{'ASYNC'}  = False
    Excel{'Application.DisplayAlerts'} = False

    Excel{'Application.ScreenUpdating'} = func:Visible
    Excel{'Application.Visible'} = func:Visible
    Excel{'Application.Calculation'} = 0FFFFEFD9h
    excel:ActiveWorkBook    = Excel{'ActiveWorkBook'}
    Yield()

!Make a WorkBook
ExcelMakeWorkBook   Procedure(String    func:Title,String   func:Author,String  func:AppName)
    Code
    Excel{prop:Release} = excel:ActiveWorkBook
    excel:ActiveWorkBook = Excel{'Application.Workbooks.Add()'}

    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Title")'} = func:Title
    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Author")'} = func:Author
    Excel{excel:ActiveWorkBook & '.BuiltinDocumentProperties("Application Name")'} = func:AppName

    excel:Selected = Excel{'Sheets("Sheet2").Select'}
    Excel{prop:Release} = excel:Selected

    Excel{'ActiveWindow.SelectedSheets.Delete'}

    excel:Selected = Excel{'Sheets("Sheet1").Select'}
    Excel{prop:Release} = excel:Selected
    Yield()

ExcelMakeSheet      Procedure()
ActiveWorkBook  CString(20)
    Code
    ActiveWorkBook = Excel{'ActiveWorkBook'}

    Excel{ActiveWorkBook & '.Sheets("Sheet3").Select'}
    Excel{prop:Release} = ActiveWorkBook

    Excel{excel:ActiveWorkBook & '.Sheets.Add'}
    Yield()
!Select A Sheet
ExcelSelectSheet    Procedure(String    func:SheetName)
    Code
    Excel{'Sheets("' & Clip(func:SheetName) & '").Select'}
    Yield()
!Setup Sheet Type (P = Portrait, L = Lanscape)
ExcelSheetType      Procedure(String    func:Type)
    Code
    Case func:Type
        Of 'L'
            Excel{'ActiveSheet.PageSetup.Orientation'}  = 2
        Of 'P'
            Excel{'ActiveSheet.PageSetup.Orientation'}  = 1
    End !Case func:Type
    Excel{'ActiveSheet.PageSetup.FitToPagesWide'}  = 1
    Excel{'ActiveSheet.PageSetup.FitToPagesTall'}  = 9999
    Excel{'ActiveSheet.PageSetup.Order'}  = 2

    Yield()
ExcelHorizontal     Procedure(String    func:Direction)
Selection   Cstring(20)
    Code
    Selection = Excel{'Selection'}
    Case func:Direction
        Of 'Centre'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFF4h
        Of 'Left'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFDDh
        Of 'Right'
            Excel{Selection & '.HorizontalAlignment'} = 0FFFFEFC8h
    End !Case tmp:Direction
    Excel{prop:Release} = Selection
    Yield()
ExcelVertical   Procedure(String func:Direction)
Selection   CString(20)
    Code
    Selection = Excel{'Selection'}
    Case func:Direction
        Of 'Top'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFC0h
        Of 'Centre'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFF4h
        Of 'Bottom'
            Excel{Selection & '.VerticalAlignment'} = 0FFFFEFF5h
    End ! Case func:Direction
    Excel{prop:Release} = Selection
    Yield()

ExcelCell   Procedure(String    func:Text,Byte    func:Bold)
Selection   Cstring(20)
    Code
    Selection = Excel{'Selection'}
    If func:Bold
        Excel{Selection & '.Font.Bold'} = True
    Else
        Excel{Selection & '.Font.Bold'} = False
    End !If func:Bold
    Excel{prop:Release} = Selection

    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.Formula'}  = func:Text
    Excel{excel:Selected & '.Offset(0, 1).Select'}
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelFormatCell     Procedure(String    func:Format)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.NumberFormat'} = func:Format
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelFormatRange    Procedure(String    func:Range,String   func:Format)
Selection       Cstring(20)
    Code

    ExcelSelectRange(func:Range)
    Selection   = Excel{'Selection'}
    Excel{Selection & '.NumberFormat'} = func:Format
    Excel{prop:Release} = Selection
    Yield()

ExcelNewLine    Procedure(Long func:Number)
    Code
    Loop excelloop# = 1 to func:Number
        ExcelSelectRange('A' & (ExcelCurrentRow() + 1))
    End !Loop excelloop# = 1 to func:Number
    !excel:Selected = Excel{'ActiveCell'}
    !Excel{excel:Selected & '.Offset(0, -' & Excel{excel:Selected & '.Column'} - 1 & ').Select'}
    !Excel{excel:Selected & '.Offset(1, 0).Select'}
    !Excel{prop:Release} = excel:Selected
    Yield()

ExcelMoveDown   Procedure()
    Code
    ExcelSelectRange(ExcelCurrentColumn() & (ExcelCurrentRow() + 1))
    Yield()
!Set Column Width

ExcelColumnWidth        Procedure(String    func:Range,Long   func:Width)
    Code
    Excel{'ActiveSheet.Columns("' & Clip(func:Range) & '").ColumnWidth'} = func:Width
    Yield()
ExcelCellWidth          Procedure(Long  func:Width)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.ColumnWidth'} = func:Width
    Excel{prop:Release} = excel:Selected
    Yield()
ExcelAutoFit            Procedure(String func:Range)
    Code
    Excel{'ActiveSheet.Columns("' & Clip(func:Range) & '").Columns.AutoFit'}
    Yield()
!Set Gray Box

ExcelGrayBox            Procedure(String    func:Range)
Selection   CString(20)
    Code
    Selection = Excel{'Selection'}
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Excel{Selection & '.Interior.ColorIndex'} = 15
    Excel{Selection & '.Interior.Pattern'} = 1
    Excel{Selection & '.Interior.PatternColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(7).LineStyle'} = 1
    Excel{Selection & '.Borders(7).Weight'} = 2
    Excel{Selection & '.Borders(7).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(10).LineStyle'} = 1
    Excel{Selection & '.Borders(10).Weight'} = 2
    Excel{Selection & '.Borders(10).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(8).LineStyle'} = 1
    Excel{Selection & '.Borders(8).Weight'} = 2
    Excel{Selection & '.Borders(8).ColorIndex'} = 0FFFFEFF7h
    Excel{Selection & '.Borders(9).LineStyle'} = 1
    Excel{Selection & '.Borders(9).Weight'} = 2
    Excel{Selection & '.Borders(9).ColorIndex'} = 0FFFFEFF7h
    Excel{prop:Release} = Selection
    Yield()
ExcelGrid   Procedure(String    func:Range,Byte  func:Left,Byte  func:Top,Byte   func:Right,Byte func:Bottom,Byte func:Colour)
Selection   Cstring(20)
    Code
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Selection = Excel{'Selection'}
    If func:Colour
        Excel{Selection & '.Interior.ColorIndex'} = func:Colour
        Excel{Selection & '.Interior.Pattern'} = 1
        Excel{Selection & '.Interior.PatternColorIndex'} = 0FFFFEFF7h
    End !If func:Colour
    If func:Left
        Excel{Selection & '.Borders(7).LineStyle'} = 1
        Excel{Selection & '.Borders(7).Weight'} = 2
        Excel{Selection & '.Borders(7).ColorIndex'} = 0FFFFEFF7h
    End !If func:Left

    If func:Right
        Excel{Selection & '.Borders(10).LineStyle'} = 1
        Excel{Selection & '.Borders(10).Weight'} = 2
        Excel{Selection & '.Borders(10).ColorIndex'} = 0FFFFEFF7h
    End !If func:Top

    If func:Top
        Excel{Selection & '.Borders(8).LineStyle'} = 1
        Excel{Selection & '.Borders(8).Weight'} = 2
        Excel{Selection & '.Borders(8).ColorIndex'} = 0FFFFEFF7h
    End !If func:Right

    If func:Bottom
        Excel{Selection & '.Borders(9).LineStyle'} = 1
        Excel{Selection & '.Borders(9).Weight'} = 2
        Excel{Selection & '.Borders(9).ColorIndex'} = 0FFFFEFF7h
    End !If func:Bottom
    Excel{prop:Release} = Selection
    Yield()
!Select a range of cells
ExcelSelectRange        Procedure(String    func:Range)
    Code
    Excel{'Range("' & Clip(func:Range) & '").Select'}
    Yield()
!Change font size
ExcelFontSize           Procedure(Byte  func:Size)
    Code
    excel:Selected = Excel{'ActiveCell'}
    Excel{excel:Selected & '.Font.Size'}   = func:Size
    Excel{prop:Release} = excel:Selected
    Yield()
!Sheet Name
ExcelSheetName          Procedure(String    func:Name)
    Code
    Excel{'ActiveSheet.Name'} = func:Name
    Yield()
ExcelAutoFilter         Procedure(String    func:Range)
Selection   Cstring(20)
    Code
    ExcelSelectRange(func:Range)
    Selection = Excel{'Selection'}
    Excel{Selection & '.AutoFilter'}
    Excel{prop:Release} = Selection
    Yield()
ExcelDropAllSheets      Procedure()
    Code
    Excel{prop:Release} = excel:ActiveWorkBook
    Loop While Excel{'WorkBooks.Count'} > 0
        excel:ActiveWorkBook = Excel{'ActiveWorkBook'}
        Excel{'ActiveWorkBook.Close(1)'}
        Excel{prop:Release} = excel:ActiveWorkBook
    End !Loop While Excel{'WorkBooks.Count'} > 0
    Yield()
ExcelClose              Procedure()
!xlCalculationAutomatic
    Code
    Excel{'Application.Calculation'}= 0FFFFEFF7h
    Excel{'Application.Quit'}
    Excel{prop:Deactivate}
    Destroy(Excel)
    Yield()
ExcelDeleteSheet        Procedure(String    func:SheetName)
    Code
    ExcelSelectSheet(func:SheetName)
    Excel{'ActiveWindow.SelectedSheets.Delete'}
    Yield()
ExcelSaveWorkBook       Procedure(String    func:Name)
    Code
    Excel{'Application.ActiveWorkBook.SaveAs("' & LEFT(CLIP(func:Name)) & '")'}
    Excel{'Application.ActiveWorkBook.Close()'}
   Excel{'Application.Calculation'} = 0FFFFEFF7h
    Excel{'Application.Quit'}

    Excel{PROP:DEACTIVATE}
    YIELD()
ExcelFontColour         Procedure(String    func:Range,Long func:Colour)
    Code
    !16 = Gray
    ExcelSelectRange(func:Range)
    Excel{'Selection.Font.ColorIndex'} = func:Colour
    Yield()
ExcelWrapText           Procedure(String    func:Range,Byte func:True)
Selection   Cstring(20)
    Code
    ExcelSelectRange(func:Range)
    Selection = Excel{'Selection'}
    Excel{Selection & '.WrapText'} = func:True
    Excel{prop:Release} = Selection
    Yield()
ExcelCurrentColumn      Procedure()
CurrentColumn   String(20)
    Code
    excel:Selected = Excel{'ActiveCell'}
    CurrentColumn = Excel{excel:Selected & '.Column'}
    Excel{prop:Release} = excel:Selected
    Yield()
    Return CurrentColumn

ExcelCurrentRow         Procedure()
CurrentRow      String(20)
    Code
    excel:Selected = Excel{'ActiveCell'}
    CurrentRow = Excel{excel:Selected & '.Row'}
    Excel{prop:Release} = excel:Selected
    Yield()
    Return CurrentRow

ExcelPasteSpecial       Procedure(String    func:Range)
Selection       CString(20)
    Code
    ExcelSelectRange(func:Range)
    Selection   = Excel{'ActiveCell'}
    Excel{Selection & '.PasteSpecial'}
    Excel{prop:Release} = Selection
    Yield()

ExcelConvertFormula     Procedure(String    func:Formula)
    Code
    Return Excel{'Application.ConvertFormula("' & Clip(func:Formula) & '",' & 0FFFFEFCAh & ',' & 1 & ')'}

ExcelGetFilename        Procedure(Byte  func:DontAsk)
sav:Path        CString(255)
func:Desktop     CString(255)
    Code

        SHGetSpecialFolderPath( GetDesktopWindow(), func:Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
        func:Desktop = Clip(func:Desktop) & '\ServiceBase Export'

        !Does the Export Folder already Exists?
        If ~Exists(Clip(func:Desktop))
            If ~MkDir(func:Desktop)
                Return Level:Fatal

            End !If MkDir(func:Desktop)
        End !If Exists(Clip(tmp:Desktop))

        Error# = 0
        sav:Path = Path()
        SetPath(func:Desktop)

        func:Desktop = Clip(func:Desktop) & '\' & CLIP(Excel:ProgramName) & ' ' & FORMAT(TODAY(), @D12) & '.xls'

        If func:DontAsk = False
            IF NOT FILEDIALOG('Save Spreadsheet', func:Desktop, 'Microsoft Excel Workbook|*.XLS', |
                FILE:KeepDir + FILE:Save + FILE:NoError + FILE:LongName)
                Error# = 1
            End!IF NOT FILEDIALOG('Save Spreadsheet', tmp:Desktop, 'Microsoft Excel Workbook|*.XLS', |
        End !If func:DontAsk = True

        SetPath(sav:Path)

        If Error#
            Return Level:Fatal
        End !If Error#
        excel:FileName    = func:Desktop
        Return Level:Benign

ExcelGetDirectory       Procedure()
sav:Path        CString(255)
func:Desktop    CString(255)
    Code
        SHGetSpecialFolderPath( GetDesktopWindow(), func:Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
        func:Desktop = Clip(func:Desktop) & '\ServiceBase Export\'
        !Does the Export Folder already Exists?
        Error# = 0
        If ~Exists(Clip(func:Desktop))
            If ~MkDir(func:Desktop)
                If Not FileDialog('Save Spreadsheet To Folder', func:Desktop, ,FILE:KeepDir+ File:Save + File:NoError + File:LongName + File:Directory)
                    Return Level:Fatal
                End !+ File:LongName + File:Directory)
            End !If MkDir(func:Desktop)
        End !If Exists(Clip(tmp:Desktop))

        excel:FileName  = func:Desktop
        Return Level:Benign

ExcelColumnLetter     Procedure(Long func:ColumnNumber)
local:Over26        Long()
Code
    local:Over26 = 0
    If func:ColumnNumber > 26
        Loop Until func:ColumnNumber <= 26
            local:Over26 += 1
            func:ColumnNumber -= 26
        End !Loop Until ColumnNumber <= 26.
    End !If func:ColumnNumber > 26

    If local:Over26 > 26
        Stop('ExcelColumnLetter Procedure Out Of Range!')
    End !If local:Over26 > 26
    If local:Over26 > 0
        Return Clip(CHR(local:Over26 + 64)) & Clip(CHR(func:ColumnNumber + 64))
    Else !If local:Over26 > 0
        Return Clip(CHR(func:ColumnNumber + 64))
    End !If local:Over26 > 0
ExcelOpenDoc        Procedure(String func:FileName)
Code
    Excel{'Workbooks.Open("' & Clip(func:FileName) & '")'}
ExcelFreeze         Procedure(String func:Cell)
Code
    Excel{'Range("' & Clip(func:Cell) & '").Select'}
    Excel{'ActiveWindow.FreezePanes'} = True
