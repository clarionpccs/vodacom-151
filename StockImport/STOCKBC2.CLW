  MEMBER('StockHistAdj.clw')

  INCLUDE('ABFILE.INC'),ONCE

  MAP
STOCKBC2:DctInit    PROCEDURE
STOCKBC2:DctKill    PROCEDURE
STOCKBC2:FilesInit  PROCEDURE
  END

Hide:Access:JOBNOTES CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:JOBNOTES CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:MANFAURL CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:MANFAURL CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:MODELCCT CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:MODELCCT CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:MANFAUEX CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:MANFAUEX CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:MANFPARL CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:MANFPARL CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBPAYMT CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:JOBPAYMT CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:MANREJR  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:MANREJR  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:STOCK    CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:STOCK    CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:MODEXCHA CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:MODEXCHA CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:MANMARK  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:MANMARK  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:MODPROD  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:MODPROD  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:TRAFAULO CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:TRAFAULO CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:TRAFAULT CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
ValidateFieldServer    PROCEDURE(UNSIGNED Id,BYTE HandleErrors),BYTE,PROC,DERIVED
                     END


Hide:Relate:TRAFAULT CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBLOHIS CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:JOBLOHIS CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:ACCESSOR CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:ACCESSOR CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:SUBACCAD CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:SUBACCAD CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:WARPARTS CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:WARPARTS CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:PARTS    CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:PARTS    CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBACC   CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:JOBACC   CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:AUDIT    CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:AUDIT    CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

STOCKBC2:DctInit PROCEDURE
  CODE
  Relate:JOBNOTES &= Hide:Relate:JOBNOTES
  Relate:MANFAURL &= Hide:Relate:MANFAURL
  Relate:MODELCCT &= Hide:Relate:MODELCCT
  Relate:MANFAUEX &= Hide:Relate:MANFAUEX
  Relate:MANFPARL &= Hide:Relate:MANFPARL
  Relate:JOBPAYMT &= Hide:Relate:JOBPAYMT
  Relate:MANREJR &= Hide:Relate:MANREJR
  Relate:STOCK &= Hide:Relate:STOCK
  Relate:MODEXCHA &= Hide:Relate:MODEXCHA
  Relate:MANMARK &= Hide:Relate:MANMARK
  Relate:MODPROD &= Hide:Relate:MODPROD
  Relate:TRAFAULO &= Hide:Relate:TRAFAULO
  Relate:TRAFAULT &= Hide:Relate:TRAFAULT
  Relate:JOBLOHIS &= Hide:Relate:JOBLOHIS
  Relate:ACCESSOR &= Hide:Relate:ACCESSOR
  Relate:SUBACCAD &= Hide:Relate:SUBACCAD
  Relate:WARPARTS &= Hide:Relate:WARPARTS
  Relate:PARTS &= Hide:Relate:PARTS
  Relate:JOBACC &= Hide:Relate:JOBACC
  Relate:AUDIT &= Hide:Relate:AUDIT

STOCKBC2:FilesInit PROCEDURE
  CODE
  Hide:Relate:JOBNOTES.Init
  Hide:Relate:MANFAURL.Init
  Hide:Relate:MODELCCT.Init
  Hide:Relate:MANFAUEX.Init
  Hide:Relate:MANFPARL.Init
  Hide:Relate:JOBPAYMT.Init
  Hide:Relate:MANREJR.Init
  Hide:Relate:STOCK.Init
  Hide:Relate:MODEXCHA.Init
  Hide:Relate:MANMARK.Init
  Hide:Relate:MODPROD.Init
  Hide:Relate:TRAFAULO.Init
  Hide:Relate:TRAFAULT.Init
  Hide:Relate:JOBLOHIS.Init
  Hide:Relate:ACCESSOR.Init
  Hide:Relate:SUBACCAD.Init
  Hide:Relate:WARPARTS.Init
  Hide:Relate:PARTS.Init
  Hide:Relate:JOBACC.Init
  Hide:Relate:AUDIT.Init


STOCKBC2:DctKill PROCEDURE
  CODE
  Hide:Relate:JOBNOTES.Kill
  Hide:Relate:MANFAURL.Kill
  Hide:Relate:MODELCCT.Kill
  Hide:Relate:MANFAUEX.Kill
  Hide:Relate:MANFPARL.Kill
  Hide:Relate:JOBPAYMT.Kill
  Hide:Relate:MANREJR.Kill
  Hide:Relate:STOCK.Kill
  Hide:Relate:MODEXCHA.Kill
  Hide:Relate:MANMARK.Kill
  Hide:Relate:MODPROD.Kill
  Hide:Relate:TRAFAULO.Kill
  Hide:Relate:TRAFAULT.Kill
  Hide:Relate:JOBLOHIS.Kill
  Hide:Relate:ACCESSOR.Kill
  Hide:Relate:SUBACCAD.Kill
  Hide:Relate:WARPARTS.Kill
  Hide:Relate:PARTS.Kill
  Hide:Relate:JOBACC.Kill
  Hide:Relate:AUDIT.Kill


Hide:Access:JOBNOTES.Init PROCEDURE
  CODE
  SELF.Init(JOBNOTES,GlobalErrors)
  SELF.Buffer &= jbn:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jbn:RefNumberKey,'By Job Number',0)
  Access:JOBNOTES &= SELF


Hide:Relate:JOBNOTES.Init PROCEDURE
  CODE
  Hide:Access:JOBNOTES.Init
  SELF.Init(Access:JOBNOTES,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:JOBNOTES.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBNOTES &= NULL


Hide:Relate:JOBNOTES.Kill PROCEDURE

  CODE
  Hide:Access:JOBNOTES.Kill
  PARENT.Kill
  Relate:JOBNOTES &= NULL


Hide:Access:MANFAURL.Init PROCEDURE
  CODE
  SELF.Init(MANFAURL,GlobalErrors)
  SELF.Buffer &= mnr:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(mnr:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(mnr:FieldKey,'By Field Number',0)
  SELF.AddKey(mnr:LinkedRecordNumberKey,'By Linked Record Number',0)
  Access:MANFAURL &= SELF


Hide:Relate:MANFAURL.Init PROCEDURE
  CODE
  Hide:Access:MANFAURL.Init
  SELF.Init(Access:MANFAURL,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:MANFAULO)


Hide:Access:MANFAURL.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:MANFAURL &= NULL


Hide:Access:MANFAURL.PrimeFields PROCEDURE

  CODE
  mnr:PartFaultCode = 0
  PARENT.PrimeFields


Hide:Relate:MANFAURL.Kill PROCEDURE

  CODE
  Hide:Access:MANFAURL.Kill
  PARENT.Kill
  Relate:MANFAURL &= NULL


Hide:Access:MODELCCT.Init PROCEDURE
  CODE
  SELF.Init(MODELCCT,GlobalErrors)
  SELF.Buffer &= mcc:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(mcc:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(mcc:CCTRefKey,'By CCT Reference Number',0)
  Access:MODELCCT &= SELF


Hide:Relate:MODELCCT.Init PROCEDURE
  CODE
  Hide:Access:MODELCCT.Init
  SELF.Init(Access:MODELCCT,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:MODELNUM)


Hide:Access:MODELCCT.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:MODELCCT &= NULL


Hide:Relate:MODELCCT.Kill PROCEDURE

  CODE
  Hide:Access:MODELCCT.Kill
  PARENT.Kill
  Relate:MODELCCT &= NULL


Hide:Access:MANFAUEX.Init PROCEDURE
  CODE
  SELF.Init(MANFAUEX,GlobalErrors)
  SELF.Buffer &= max:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(max:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(max:ModelNumberKey,'By Model Number..',0)
  Access:MANFAUEX &= SELF


Hide:Relate:MANFAUEX.Init PROCEDURE
  CODE
  Hide:Access:MANFAUEX.Init
  SELF.Init(Access:MANFAUEX,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:MANFAULO)


Hide:Access:MANFAUEX.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:MANFAUEX &= NULL


Hide:Relate:MANFAUEX.Kill PROCEDURE

  CODE
  Hide:Access:MANFAUEX.Kill
  PARENT.Kill
  Relate:MANFAUEX &= NULL


Hide:Access:MANFPARL.Init PROCEDURE
  CODE
  SELF.Init(MANFPARL,GlobalErrors)
  SELF.Buffer &= mpr:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(mpr:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(mpr:FieldKey,'By Field Number',0)
  SELF.AddKey(mpr:LinkedRecordNumberKey,'By Linked Record Number',0)
  Access:MANFPARL &= SELF


Hide:Relate:MANFPARL.Init PROCEDURE
  CODE
  Hide:Access:MANFPARL.Init
  SELF.Init(Access:MANFPARL,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:MANFPALO)


Hide:Access:MANFPARL.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:MANFPARL &= NULL


Hide:Access:MANFPARL.PrimeFields PROCEDURE

  CODE
  mpr:JobFaultCode = 0
  PARENT.PrimeFields


Hide:Relate:MANFPARL.Kill PROCEDURE

  CODE
  Hide:Access:MANFPARL.Kill
  PARENT.Kill
  Relate:MANFPARL &= NULL


Hide:Access:JOBPAYMT.Init PROCEDURE
  CODE
  SELF.Init(JOBPAYMT,GlobalErrors)
  SELF.Buffer &= jpt:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jpt:Record_Number_Key,'By Record Number',1)
  SELF.AddKey(jpt:All_Date_Key,'By Date',0)
  SELF.AddKey(jpt:Loan_Deposit_Key,'jpt:Loan_Deposit_Key',0)
  Access:JOBPAYMT &= SELF


Hide:Relate:JOBPAYMT.Init PROCEDURE
  CODE
  Hide:Access:JOBPAYMT.Init
  SELF.Init(Access:JOBPAYMT,1)


Hide:Access:JOBPAYMT.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBPAYMT &= NULL


Hide:Access:JOBPAYMT.PrimeFields PROCEDURE

  CODE
  jpt:Date = Today()
  jpt:Loan_Deposit = 0
  PARENT.PrimeFields


Hide:Relate:JOBPAYMT.Kill PROCEDURE

  CODE
  Hide:Access:JOBPAYMT.Kill
  PARENT.Kill
  Relate:JOBPAYMT &= NULL


Hide:Access:MANREJR.Init PROCEDURE
  CODE
  SELF.Init(MANREJR,GlobalErrors)
  SELF.Buffer &= mar:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(mar:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(mar:CodeKey,'By Code',0)
  Access:MANREJR &= SELF


Hide:Relate:MANREJR.Init PROCEDURE
  CODE
  Hide:Access:MANREJR.Init
  SELF.Init(Access:MANREJR,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:MANUFACT)


Hide:Access:MANREJR.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:MANREJR &= NULL


Hide:Relate:MANREJR.Kill PROCEDURE

  CODE
  Hide:Access:MANREJR.Kill
  PARENT.Kill
  Relate:MANREJR &= NULL


Hide:Access:STOCK.Init PROCEDURE
  CODE
  SELF.Init(STOCK,GlobalErrors)
  SELF.Buffer &= sto:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(sto:Ref_Number_Key,'By Ref Number',1)
  SELF.AddKey(sto:Sundry_Item_Key,'By Sundry Item',0)
  SELF.AddKey(sto:Description_Key,'By Description',0)
  SELF.AddKey(sto:Description_Accessory_Key,'By Description',0)
  SELF.AddKey(sto:Shelf_Location_Key,'By Shelf Location',0)
  SELF.AddKey(sto:Shelf_Location_Accessory_Key,'By Shelf Location',0)
  SELF.AddKey(sto:Supplier_Accessory_Key,'By Supplier',0)
  SELF.AddKey(sto:Manufacturer_Key,'By Manufacturer',0)
  SELF.AddKey(sto:Supplier_Key,'By Supplier',0)
  SELF.AddKey(sto:Location_Key,'By Location',0)
  SELF.AddKey(sto:Part_Number_Accessory_Key,'By Part Number',0)
  SELF.AddKey(sto:Ref_Part_Description_Key,'sto:Ref_Part_Description_Key',0)
  SELF.AddKey(sto:Location_Manufacturer_Key,'By Manufacturer',0)
  SELF.AddKey(sto:Manufacturer_Accessory_Key,'By Part Number',0)
  SELF.AddKey(sto:Location_Part_Description_Key,'By Part Number',0)
  SELF.AddKey(sto:Ref_Part_Description2_Key,'CASCADE Key',0)
  SELF.AddKey(sto:Minimum_Part_Number_Key,'By Part Number',0)
  SELF.AddKey(sto:Minimum_Description_Key,'sto:Minimum_Description_Key',0)
  SELF.AddKey(sto:SecondLocKey,'By Part Number',0)
  SELF.AddKey(sto:RequestedKey,'By Part Number',0)
  SELF.AddKey(sto:ExchangeAccPartKey,'By Part Number',0)
  SELF.AddKey(sto:ExchangeAccDescKey,'By Description',0)
  SELF.AddKey(sto:DateBookedKey,'By Date Booked',0)
  SELF.AddKey(sto:Supplier_Only_Key,'sto:Supplier_Only_Key',0)
  SELF.AddKey(sto:LocPartSuspendKey,'By Part Number',0)
  SELF.AddKey(sto:LocDescSuspendKey,'By Description',0)
  SELF.AddKey(sto:LocShelfSuspendKey,'By Shelf Location',0)
  SELF.AddKey(sto:LocManSuspendKey,'By Part Number',0)
  SELF.AddKey(sto:ExchangeModelKey,'By Model Number',0)
  SELF.AddKey(sto:LoanModelKey,'By Loan Model Number',0)
  Access:STOCK &= SELF


Hide:Relate:STOCK.Init PROCEDURE
  CODE
  Hide:Access:STOCK.Init
  SELF.Init(Access:STOCK,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:STOPARTS,RI:CASCADE,RI:CASCADE,spt:DateChangedKey)
  SELF.AddRelationLink(sto:Ref_Number,spt:STOCKRefNumber)
  SELF.AddRelation(Relate:STOAUDIT,RI:CASCADE,RI:CASCADE,stoa:Stock_Ref_No_Key)
  SELF.AddRelationLink(sto:Ref_Number,stoa:Stock_Ref_No)
  SELF.AddRelation(Relate:STOHIST,RI:CASCADE,RI:CASCADE,shi:Ref_Number_Key)
  SELF.AddRelationLink(sto:Ref_Number,shi:Ref_Number)
  SELF.AddRelation(Relate:STOESN,RI:CASCADE,RI:CASCADE,ste:Sold_Key)
  SELF.AddRelationLink(sto:Ref_Number,ste:Ref_Number)
  SELF.AddRelation(Relate:ORDPARTS,RI:CASCADE,RI:None,orp:Ref_Number_Key)
  SELF.AddRelationLink(sto:Ref_Number,orp:Part_Ref_Number)
  SELF.AddRelationLink(sto:Part_Number,orp:Part_Number)
  SELF.AddRelationLink(sto:Description,orp:Description)
  SELF.AddRelation(Relate:STOMODEL,RI:CASCADE,RI:CASCADE,stm:Ref_Part_Description)
  SELF.AddRelationLink(sto:Ref_Number,stm:Ref_Number)
  SELF.AddRelationLink(sto:Part_Number,stm:Part_Number)
  SELF.AddRelationLink(sto:Location,stm:Location)
  SELF.AddRelationLink(sto:Description,stm:Description)
  SELF.AddRelation(Relate:LOCATION)
  SELF.AddRelation(Relate:LOCSHELF)


Hide:Access:STOCK.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:STOCK &= NULL


Hide:Access:STOCK.PrimeFields PROCEDURE

  CODE
  sto:Accessory = 'NO'
  sto:Minimum_Stock = 'NO'
  sto:Assign_Fault_Codes = 'NO'
  sto:Individual_Serial_Numbers = 'NO'
  sto:ExchangeUnit = 'NO'
  sto:Suspend = 0
  sto:E1 = 1
  sto:E2 = 1
  sto:E3 = 1
  sto:ReturnFaultySpare = 0
  sto:ChargeablePartOnly = 0
  sto:AttachBySolder = 0
  sto:AllowDuplicate = 0
  sto:DateBooked = Today()
  sto:ExcludeLevel12Repair = 0
  PARENT.PrimeFields


Hide:Relate:STOCK.Kill PROCEDURE

  CODE
  Hide:Access:STOCK.Kill
  PARENT.Kill
  Relate:STOCK &= NULL


Hide:Access:MODEXCHA.Init PROCEDURE
  CODE
  SELF.Init(MODEXCHA,GlobalErrors)
  SELF.Buffer &= moa:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(moa:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(moa:AccountNumberKey,'By Account Number',0)
  Access:MODEXCHA &= SELF


Hide:Relate:MODEXCHA.Init PROCEDURE
  CODE
  Hide:Access:MODEXCHA.Init
  SELF.Init(Access:MODEXCHA,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:MODELNUM)


Hide:Access:MODEXCHA.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:MODEXCHA &= NULL


Hide:Relate:MODEXCHA.Kill PROCEDURE

  CODE
  Hide:Access:MODEXCHA.Kill
  PARENT.Kill
  Relate:MODEXCHA &= NULL


Hide:Access:MANMARK.Init PROCEDURE
  CODE
  SELF.Init(MANMARK,GlobalErrors)
  SELF.Buffer &= mak:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(mak:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(mak:SiteLocationKey,'By Site Location',0)
  SELF.AddKey(mak:SiteLocationOnlyKey,'By Site Location',0)
  Access:MANMARK &= SELF


Hide:Relate:MANMARK.Init PROCEDURE
  CODE
  Hide:Access:MANMARK.Init
  SELF.Init(Access:MANMARK,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:LOCATION)


Hide:Access:MANMARK.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:MANMARK &= NULL


Hide:Relate:MANMARK.Kill PROCEDURE

  CODE
  Hide:Access:MANMARK.Kill
  PARENT.Kill
  Relate:MANMARK &= NULL


Hide:Access:MODPROD.Init PROCEDURE
  CODE
  SELF.Init(MODPROD,GlobalErrors)
  SELF.Buffer &= mop:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(mop:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(mop:ProductCodeKey,'By Model No/Product Code',0)
  Access:MODPROD &= SELF


Hide:Relate:MODPROD.Init PROCEDURE
  CODE
  Hide:Access:MODPROD.Init
  SELF.Init(Access:MODPROD,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:MODELNUM)


Hide:Access:MODPROD.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:MODPROD &= NULL


Hide:Relate:MODPROD.Kill PROCEDURE

  CODE
  Hide:Access:MODPROD.Kill
  PARENT.Kill
  Relate:MODPROD &= NULL


Hide:Access:TRAFAULO.Init PROCEDURE
  CODE
  SELF.Init(TRAFAULO,GlobalErrors)
  SELF.Buffer &= tfo:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(tfo:Field_Key,'By Field',0)
  SELF.AddKey(tfo:DescriptionKey,'By Description',0)
  Access:TRAFAULO &= SELF


Hide:Relate:TRAFAULO.Init PROCEDURE
  CODE
  Hide:Access:TRAFAULO.Init
  SELF.Init(Access:TRAFAULO,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:TRAFAULT)


Hide:Access:TRAFAULO.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:TRAFAULO &= NULL


Hide:Relate:TRAFAULO.Kill PROCEDURE

  CODE
  Hide:Access:TRAFAULO.Kill
  PARENT.Kill
  Relate:TRAFAULO &= NULL


Hide:Access:TRAFAULT.Init PROCEDURE
  CODE
  SELF.Init(TRAFAULT,GlobalErrors)
  SELF.Buffer &= taf:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(taf:RecordNumberKey,'taf:RecordNumberKey',1)
  SELF.AddKey(taf:Field_Number_Key,'By Field Number',0)
  Access:TRAFAULT &= SELF


Hide:Relate:TRAFAULT.Init PROCEDURE
  CODE
  Hide:Access:TRAFAULT.Init
  SELF.Init(Access:TRAFAULT,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:TRAFAULO,RI:CASCADE,RI:CASCADE,tfo:Field_Key)
  SELF.AddRelationLink(taf:AccountNumber,tfo:AccountNumber)
  SELF.AddRelationLink(taf:Field_Number,tfo:Field_Number)
  SELF.AddRelation(Relate:TRADEACC)


Hide:Access:TRAFAULT.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:TRAFAULT &= NULL


Hide:Access:TRAFAULT.PrimeFields PROCEDURE

  CODE
  taf:Compulsory = 'NO'
  taf:Compulsory_At_Booking = 'NO'
  taf:RestrictLength = 0
  PARENT.PrimeFields


Hide:Access:TRAFAULT.ValidateFieldServer PROCEDURE(UNSIGNED Id,BYTE HandleErrors)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.ValidateFieldServer(Id,HandleErrors)
  CASE Id
  OF 3
    GlobalErrors.SetField('Fault Code Field Number')
    IF NOT INRANGE(taf:Field_Number,1,12)
      ReturnValue = Level:Notify
    END
    IF ReturnValue <> Level:Benign
      IF HandleErrors
        ReturnValue = GlobalErrors.ThrowMessage(Msg:FieldOutOfRange,'1 .. 12')
      END
    END
  END
  RETURN ReturnValue


Hide:Relate:TRAFAULT.Kill PROCEDURE

  CODE
  Hide:Access:TRAFAULT.Kill
  PARENT.Kill
  Relate:TRAFAULT &= NULL


Hide:Access:JOBLOHIS.Init PROCEDURE
  CODE
  SELF.Init(JOBLOHIS,GlobalErrors)
  SELF.Buffer &= jlh:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jlh:Ref_Number_Key,'By Date',0)
  SELF.AddKey(jlh:record_number_key,'jlh:record_number_key',1)
  Access:JOBLOHIS &= SELF


Hide:Relate:JOBLOHIS.Init PROCEDURE
  CODE
  Hide:Access:JOBLOHIS.Init
  SELF.Init(Access:JOBLOHIS,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:JOBLOHIS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBLOHIS &= NULL


Hide:Relate:JOBLOHIS.Kill PROCEDURE

  CODE
  Hide:Access:JOBLOHIS.Kill
  PARENT.Kill
  Relate:JOBLOHIS &= NULL


Hide:Access:ACCESSOR.Init PROCEDURE
  CODE
  SELF.Init(ACCESSOR,GlobalErrors)
  SELF.Buffer &= acr:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(acr:Accesory_Key,'By Accessory',0)
  SELF.AddKey(acr:Model_Number_Key,'By Model Number',0)
  SELF.AddKey(acr:AccessOnlyKey,'By Accessory',0)
  Access:ACCESSOR &= SELF


Hide:Relate:ACCESSOR.Init PROCEDURE
  CODE
  Hide:Access:ACCESSOR.Init
  SELF.Init(Access:ACCESSOR,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:ACCESDEF)
  SELF.AddRelation(Relate:MODELNUM)


Hide:Access:ACCESSOR.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:ACCESSOR &= NULL


Hide:Relate:ACCESSOR.Kill PROCEDURE

  CODE
  Hide:Access:ACCESSOR.Kill
  PARENT.Kill
  Relate:ACCESSOR &= NULL


Hide:Access:SUBACCAD.Init PROCEDURE
  CODE
  SELF.Init(SUBACCAD,GlobalErrors)
  SELF.Buffer &= sua:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(sua:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(sua:AccountNumberKey,'By Account Number',0)
  SELF.AddKey(sua:CompanyNameKey,'By Company Name',0)
  SELF.AddKey(sua:AccountNumberOnlyKey,'By Account Number',0)
  Access:SUBACCAD &= SELF


Hide:Relate:SUBACCAD.Init PROCEDURE
  CODE
  Hide:Access:SUBACCAD.Init
  SELF.Init(Access:SUBACCAD,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:SUBTRACC)


Hide:Access:SUBACCAD.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:SUBACCAD &= NULL


Hide:Relate:SUBACCAD.Kill PROCEDURE

  CODE
  Hide:Access:SUBACCAD.Kill
  PARENT.Kill
  Relate:SUBACCAD &= NULL


Hide:Access:WARPARTS.Init PROCEDURE
  CODE
  SELF.Init(WARPARTS,GlobalErrors)
  SELF.Buffer &= wpr:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(wpr:Part_Number_Key,'By Part Number',0)
  SELF.AddKey(wpr:RecordNumberKey,'wpr:RecordNumberKey',1)
  SELF.AddKey(wpr:PartRefNoKey,'By Part Ref Number',0)
  SELF.AddKey(wpr:Date_Ordered_Key,'By Date Ordered',0)
  SELF.AddKey(wpr:RefPartRefNoKey,'By Part Ref Number',0)
  SELF.AddKey(wpr:PendingRefNoKey,'wpr:PendingRefNoKey',0)
  SELF.AddKey(wpr:Order_Number_Key,'wpr:Order_Number_Key',0)
  SELF.AddKey(wpr:Supplier_Key,'By Supplier',0)
  SELF.AddKey(wpr:Order_Part_Key,'By Order Part Number',0)
  SELF.AddKey(wpr:SuppDateOrdKey,'By Date Ordered',0)
  SELF.AddKey(wpr:SuppDateRecKey,'By Date Received',0)
  SELF.AddKey(wpr:RequestedKey,'By Part Number',0)
  SELF.AddKey(wpr:WebOrderKey,'By Part Number',0)
  SELF.AddKey(wpr:PartAllocatedKey,'By Part Number',0)
  SELF.AddKey(wpr:StatusKey,'By Part Number',0)
  SELF.AddKey(wpr:AllocatedStatusKey,'By Part Number',0)
  Access:WARPARTS &= SELF


Hide:Relate:WARPARTS.Init PROCEDURE
  CODE
  Hide:Access:WARPARTS.Init
  SELF.Init(Access:WARPARTS,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:SUPPLIER)
  SELF.AddRelation(Relate:JOBS)


Hide:Access:WARPARTS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:WARPARTS &= NULL


Hide:Access:WARPARTS.PrimeFields PROCEDURE

  CODE
  wpr:Main_Part = 'NO'
  wpr:Fault_Codes_Checked = 'NO'
  wpr:Credit = 'NO'
  wpr:Requested = 0
  wpr:WebOrder = 0
  wpr:PartAllocated = 0
  wpr:ExchangeUnit = 0
  wpr:SecondExchangeUnit = 0
  wpr:Correction = 0
  PARENT.PrimeFields


Hide:Relate:WARPARTS.Kill PROCEDURE

  CODE
  Hide:Access:WARPARTS.Kill
  PARENT.Kill
  Relate:WARPARTS &= NULL


Hide:Access:PARTS.Init PROCEDURE
  CODE
  SELF.Init(PARTS,GlobalErrors)
  SELF.Buffer &= par:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(par:Part_Number_Key,'By Part Number',0)
  SELF.AddKey(par:recordnumberkey,'par:recordnumberkey',1)
  SELF.AddKey(par:PartRefNoKey,'By Part_Ref_Number',0)
  SELF.AddKey(par:Date_Ordered_Key,'By Date Ordered',0)
  SELF.AddKey(par:RefPartRefNoKey,'By Part Ref Number',0)
  SELF.AddKey(par:PendingRefNoKey,'par:PendingRefNoKey',0)
  SELF.AddKey(par:Order_Number_Key,'par:Order_Number_Key',0)
  SELF.AddKey(par:Supplier_Key,'By Supplier',0)
  SELF.AddKey(par:Order_Part_Key,'By Order Part Number',0)
  SELF.AddKey(par:SuppDateOrdKey,'By Date Ordered',0)
  SELF.AddKey(par:SuppDateRecKey,'By Date Received',0)
  SELF.AddKey(par:RequestedKey,'By Part Number',0)
  SELF.AddKey(par:WebOrderKey,'By Part Number',0)
  SELF.AddKey(par:PartAllocatedKey,'By Part Number',0)
  SELF.AddKey(par:StatusKey,'By Part Number',0)
  SELF.AddKey(par:AllocatedStatusKey,'By Part Number',0)
  Access:PARTS &= SELF


Hide:Relate:PARTS.Init PROCEDURE
  CODE
  Hide:Access:PARTS.Init
  SELF.Init(Access:PARTS,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:SUPPLIER)
  SELF.AddRelation(Relate:JOBS)


Hide:Access:PARTS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:PARTS &= NULL


Hide:Access:PARTS.PrimeFields PROCEDURE

  CODE
  par:Fault_Codes_Checked = 'NO'
  par:Credit = 'NO'
  par:Requested = 0
  par:WebOrder = 0
  par:PartAllocated = 0
  par:ExchangeUnit = 0
  par:SecondExchangeUnit = 0
  par:Correction = 0
  PARENT.PrimeFields


Hide:Relate:PARTS.Kill PROCEDURE

  CODE
  Hide:Access:PARTS.Kill
  PARENT.Kill
  Relate:PARTS &= NULL


Hide:Access:JOBACC.Init PROCEDURE
  CODE
  SELF.Init(JOBACC,GlobalErrors)
  SELF.Buffer &= jac:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jac:Ref_Number_Key,'By Accessory',0)
  SELF.AddKey(jac:DamagedKey,'By Accessory',0)
  SELF.AddKey(jac:DamagedPirateKey,'By Accessory',0)
  Access:JOBACC &= SELF


Hide:Relate:JOBACC.Init PROCEDURE
  CODE
  Hide:Access:JOBACC.Init
  SELF.Init(Access:JOBACC,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:JOBACC.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBACC &= NULL


Hide:Access:JOBACC.PrimeFields PROCEDURE

  CODE
  jac:Damaged = 0
  jac:Pirate = 0
  jac:Attached = 0
  PARENT.PrimeFields


Hide:Relate:JOBACC.Kill PROCEDURE

  CODE
  Hide:Access:JOBACC.Kill
  PARENT.Kill
  Relate:JOBACC &= NULL


Hide:Access:AUDIT.Init PROCEDURE
  CODE
  SELF.Init(AUDIT,GlobalErrors)
  SELF.Buffer &= aud:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(aud:Ref_Number_Key,'By Date',0)
  SELF.AddKey(aud:Action_Key,'By Action',0)
  SELF.AddKey(aud:User_Key,'By User',0)
  SELF.AddKey(aud:Record_Number_Key,'aud:Record_Number_Key',1)
  SELF.AddKey(aud:ActionOnlyKey,'By Action',0)
  SELF.AddKey(aud:TypeRefKey,'By Date',0)
  SELF.AddKey(aud:TypeActionKey,'By Action',0)
  SELF.AddKey(aud:TypeUserKey,'By User',0)
  SELF.AddKey(aud:DateActionJobKey,'By Job Number',0)
  SELF.AddKey(aud:DateJobKey,'By Job Number',0)
  SELF.AddKey(aud:DateTypeJobKey,'By Job Number',0)
  SELF.AddKey(aud:DateTypeActionKey,'By Job Number',0)
  Access:AUDIT &= SELF


Hide:Relate:AUDIT.Init PROCEDURE
  CODE
  Hide:Access:AUDIT.Init
  SELF.Init(Access:AUDIT,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:AUDIT2,RI:CASCADE,RI:CASCADE,aud2:AUDRecordNumberKey)
  SELF.AddRelationLink(aud:record_number,aud2:AUDRecordNumber)
  SELF.AddRelation(Relate:AUDITE,RI:CASCADE,RI:CASCADE,aude:RefNumberKey)
  SELF.AddRelationLink(aud:record_number,aude:RefNumber)
  SELF.AddRelation(Relate:JOBS)


Hide:Access:AUDIT.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:AUDIT &= NULL


Hide:Access:AUDIT.PrimeFields PROCEDURE

  CODE
  aud:Type = 'JOB'
  PARENT.PrimeFields


Hide:Relate:AUDIT.Kill PROCEDURE

  CODE
  Hide:Access:AUDIT.Kill
  PARENT.Kill
  Relate:AUDIT &= NULL

