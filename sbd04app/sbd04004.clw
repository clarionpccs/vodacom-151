

   MEMBER('sbd04app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD04004.INC'),ONCE        !Local module procedure declarations
                     END


SDS_Thermal_Label    PROCEDURE  (Func:JobNo)          ! Declare Procedure
tmp:OS               STRING(20)
tmp:High             BYTE
tmp:Low              BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
    tmp:OS  = GetVersion()
    tmp:High    = BShift(tmp:OS,-8)
    tmp:Low     = tmp:OS

    If tmp:High = 0
        SDSLabelInd_Thermal_NT(func:JobNo)
    Else!If tmp:High = 0
        SDSLabelInd_Thermal_9X(func:JobNo)
    End!If tmp:High = 0
