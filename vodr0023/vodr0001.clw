

   MEMBER('vodr0023.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('VODR0001.INC'),ONCE        !Local module procedure declarations
                     END


XFiles PROCEDURE                                      !Generated from procedure template - Window

QuickWindow          WINDOW('Window'),AT(,,260,160),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,HLP('XFiles'),SYSTEM,GRAY,RESIZE
                       BUTTON('&OK'),AT(140,140,56,16),USE(?Ok),MSG('Accept operation'),TIP('Accept Operation')
                       BUTTON('&Cancel'),AT(200,140,56,16),USE(?Cancel),MSG('Cancel Operation'),TIP('Cancel Operation')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('XFiles')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Ok
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Ok,RequestCancelled)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:SRNTEXT.Open
  SELF.FilesOpened = True
  OPEN(QuickWindow)
  SELF.Opened=True
  Bryan.CompFieldColour()
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:SRNTEXT.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

CompletedJobsReport PROCEDURE                         !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::6:TAGFLAG          BYTE(0)
DASBRW::6:TAGMOUSE         BYTE(0)
DASBRW::6:TAGDISPSTATUS    BYTE(0)
DASBRW::6:QUEUE           QUEUE
Pointer2                      LIKE(glo:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
tmp:VersionNumber    STRING(30)
Parameter_Group      GROUP,PRE(LOC)
Courier              STRING(30)
EndDate              DATE
StartDate            DATE
                     END
Debug_Group          GROUP,PRE(debug)
Active               LONG
Count                LONG
                     END
Local_Group          GROUP,PRE(LOC)
Account_Name         STRING(100)
AccountNumber        STRING(15)
ApplicationName      STRING('ServiceBase')
DesktopPath          STRING(255)
FileName             STRING(255)
Path                 STRING(255)
ProgramName          STRING(100)
RepairCentreType     STRING(3)
SectionName          STRING(100)
Text                 STRING(255)
UserCode             STRING(3)
UserName             STRING(100)
Version              STRING('3.1.0000')
                     END
Misc_Group           GROUP,PRE()
AccountValue         STRING(15)
AccountTag           STRING(1)
AccountCount         LONG
AccountChange        BYTE
ExchangeOK           BYTE
OPTION1              SHORT
RecordCount          LONG
Result               BYTE
                     END
UserResultsQueue     QUEUE,PRE(uq)
UserCode             STRING(3)
Forename             STRING(30)
Surname              STRING(30)
SiteLocation         STRING(30)
JobsBooked           LONG
                     END
Progress_Group       GROUP,PRE()
progress:Text        STRING(100)
progress:NextCancelCheck TIME
                     END
Sheet_Group          GROUP,PRE(sheet)
TempLastCol          STRING(1)
HeadLastCol          STRING('E')
DataLastCol          STRING('N')
HeadSummaryRow       LONG
DataSectionRow       LONG
DataHeaderRow        LONG
                     END
Clipboard_Group      GROUP,PRE(clip)
OriginalValue        STRING(255)
Saved                BYTE
Value                CSTRING(200)
                     END
Excel_Group          GROUP,PRE()
Excel                CSTRING(20)
excel:ColumnName     STRING(50)
excel:ColumnWidth    REAL
excel:CommentText    STRING(255)
excel:DateFormat     STRING('dd mmm yyyy')
excel:OperatingSystem REAL
excel:Visible        BYTE
                     END
HeaderQueue          QUEUE,PRE(head)
ColumnName           STRING(30)
ColumnWidth          REAL
NumberFormat         STRING(20)
                     END
StuartGroup          QUEUE,PRE()
GUIMode              BYTE
LocalTag             STRING(1)
DoAll                STRING(1)
LocalTimeOut         LONG
LocalHeadAccount     STRING(30)
                     END
HeadAccount_Queue    QUEUE,PRE(haQ)
AccountNumber        STRING(15)
AccountName          STRING(30)
BranchIdentification STRING(2)
SiteLocation         STRING(30)
JobsBooked           LONG
                     END
SubAccount_Queue     QUEUE,PRE(saQ)
AccountNumber        STRING(15)
AccountName          STRING(30)
HeadAccountNumber    STRING(15)
HeadAccountName      STRING(30)
BranchIdentification STRING(2)
JobsBooked           LONG
                     END
BRW5::View:Browse    VIEW(TRADEACC)
                       PROJECT(tra:Account_Number)
                       PROJECT(tra:Company_Name)
                       PROJECT(tra:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
LocalTag               LIKE(LocalTag)                 !List box control field - type derived from local data
LocalTag_Icon          LONG                           !Entry's icon ID
tra:Account_Number     LIKE(tra:Account_Number)       !List box control field - type derived from field
tra:Company_Name       LIKE(tra:Company_Name)         !List box control field - type derived from field
tra:RecordNumber       LIKE(tra:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
MainWindow           WINDOW('Report'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(60,38,560,360),USE(?PanelMain),FILL(0D6EAEFH)
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(64,56,552,44),USE(?PanelTip),FILL(0D6EAEFH)
                       GROUP('Top Tip'),AT(68,60,544,36),USE(?GroupTip),BOXED,TRN
                       END
                       STRING(@s255),AT(72,76,496,),USE(SRN:TipText),TRN
                       BUTTON,AT(576,64,36,32),USE(?ButtonHelp),TRN,FLAT,ICON('F1Help.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Completed Jobs Report Criteria'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,104,552,258),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Criteria'),USE(?Tab1)
                           STRING('Completion Start Date '),AT(247,116),USE(?String4),TRN,LEFT,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           ENTRY(@D6),AT(342,116,64,10),USE(LOC:StartDate),IMM,FONT('Arial',8,010101H,FONT:bold),COLOR(COLOR:White),TIP('Enter the earliest despatched date to show on report'),REQ
                           BUTTON,AT(410,112),USE(?StartDateCalendar),TRN,FLAT,LEFT,ICON('lookupp.jpg')
                           BUTTON('sho&W tags'),AT(244,224,70,13),USE(?DASSHOWTAG),DISABLE,HIDE
                           STRING('Completion End Date '),AT(247,136),USE(?String5),TRN,LEFT,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                           ENTRY(@D6),AT(342,136,64,10),USE(LOC:EndDate),IMM,FONT('Arial',8,010101H,FONT:bold),COLOR(COLOR:White),TIP('Enter the latest despatched date to show on report'),REQ
                           BUTTON,AT(410,134),USE(?EndDateCalendar),TRN,FLAT,LEFT,ICON('lookupp.jpg')
                           BUTTON('&Rev tags'),AT(240,240,50,13),USE(?DASREVTAG),DISABLE,HIDE
                           BUTTON,AT(496,242),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(496,274),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(496,306),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                           CHECK('All Repair Centres'),AT(192,164),USE(DoAll),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('Y','N')
                           CHECK('Show Excel'),AT(432,162,56,12),USE(excel:Visible),TRN,LEFT,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH),TIP('Tick To Show Excel During Report Creation.<13,10>Only Tick if you are experiencing pr' &|
   'oblems <13,10>with Excel reports.'),VALUE('1','0')
                           LIST,AT(192,178,296,180),USE(?List),IMM,VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)FJ@s1@60L(2)|F~Account Number~@s15@120L(2)|F~Company Name~@s30@'),FROM(Queue:Browse)
                         END
                       END
                       BUTTON,AT(480,366),USE(?OkButton),TRN,FLAT,LEFT,TIP('Click to print report'),ICON('printp.jpg'),DEFAULT
                       BUTTON,AT(548,366),USE(?CancelButton),TRN,FLAT,LEFT,TIP('Click to close this form'),ICON('cancelp.jpg')
                       PROMPT('Report Verison'),AT(68,376),USE(?ReportVersion),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW5                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW5::Sort0:Locator  StepLocatorClass                 !Default Locator
    MAP
GetHeadAccount PROCEDURE( STRING )
GetSubAccount                       PROCEDURE( STRING )
GetWebJobNumber                 PROCEDURE( LONG ), STRING
LoadSUBTRACC                    PROCEDURE( STRING ), LONG ! BOOL
LoadTRADEACC                    PROCEDURE( STRING ), LONG ! BOOL
LoadWEBJOB                      PROCEDURE( LONG ), LONG ! BOOL
NumberToColumn                  PROCEDURE( LONG ), STRING
WriteColumn                     PROCEDURE( STRING, LONG=False )
WriteDebug                      PROCEDURE( STRING )
    END !MAP
! ProgressWindow Declarations
 
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte
 
progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY,DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
 
!---   Excel EQUATES   -------------------------------------------------------------
xlCalculationManual    EQUATE(0FFFFEFD9h) ! XlCalculation
xlCalculationAutomatic EQUATE(0FFFFEFF7h) ! XlCalculation
!----------------------------------------------------

!----------------------------------------------------
xlNone       EQUATE(0FFFFEFD2h)
xlContinuous EQUATE(        1 ) ! xlFillStyle
xlThin       EQUATE(        2 ) ! XlBorderWeight
!----------------------------------------------------

!----------------------------------------------------
xlAutomatic   EQUATE(0FFFFEFF7h) ! Constants.
xlSolid       EQUATE(        1 ) ! Constants.
xlLeft        EQUATE(0FFFFEFDDh) ! Constants.
xlRight       EQUATE(0FFFFEFC8h) ! Constants.
xlCenter      EQUATE(0FFFFEFF4h) ! Constants.
xlLastCell    EQUATE(       11 ) ! Constants.
xlTop         EQUATE(0FFFFEFC0h) ! Constants.
xlBottom      EQUATE(0FFFFEFF5h) ! Constants.

xlTopToBottom EQUATE(        1 ) ! Constants.
!----------------------------------------------------

!----------------------------------------------------
xlDiagonalDown     EQUATE( 5) ! XlBordersIndex
xlDiagonalUp       EQUATE( 6) ! XlBordersIndex
xlEdgeLeft         EQUATE( 7) ! XlBordersIndex
xlEdgeTop          EQUATE( 8) ! XlBordersIndex
xlEdgeBottom       EQUATE( 9) ! XlBordersIndex
xlEdgeRight        EQUATE(10) ! XlBordersIndex
xlInsideVertical   EQUATE(11) ! XlBordersIndex
xlInsideHorizontal EQUATE(12) ! XlBordersIndex
!----------------------------------------------------

!----------------------------------------------------
xlA1         EQUATE(        1 ) ! XlReferenceStyle
xlR1C1       EQUATE(0FFFFEFCAh) ! XlReferenceStyle
!----------------------------------------------------

!----------------------------------------------------
xlFilterCopy    EQUATE(2) ! XlFilterAction
xlFilterInPlace EQUATE(1) ! XlFilterAction
!----------------------------------------------------

!----------------------------------------------------
xlGuess EQUATE(0) ! XlYesNoGuess
xlYes   EQUATE(1) ! XlYesNoGuess
xlNo    EQUATE(2) ! XlYesNoGuess
!----------------------------------------------------

!----------------------------------------------------
xlAscending  EQUATE(1) ! XlSortOrder
xlDescending EQUATE(2) ! XlSortOrder
!----------------------------------------------------

!----------------------------------------------------
xlLandscape EQUATE(2) ! XlPageOrientation
xlPortrait  EQUATE(1) ! XlPageOrientation
!----------------------------------------------------

!----------------------------------------------------
xlDownThenOver EQUATE( 1 ) ! XlOrder
xlOverThenDown EQUATE( 2 ) ! XlOrder
!-----------------------------------------------------------------------------------
! Office EQUATES

!----------------------------------------------------
msoCTrue          EQUATE(        1 ) ! MsoTriState
msoFalse          EQUATE(        0 ) ! MsoTriState
msoTriStateMixed  EQUATE(0FFFFFFFEh) ! MsoTriState
msoTriStateToggle EQUATE(0FFFFFFFDh) ! MsoTriState
msoTrue           EQUATE(0FFFFFFFFh) ! MsoTriState
!----------------------------------------------------

!----------------------------------------------------
msoScaleFromBottomRight EQUATE(2) ! MsoScaleFrom
msoScaleFromMiddle      EQUATE(1) ! MsoScaleFrom
msoScaleFromTopLeft     EQUATE(0) ! MsoScaleFrom
!----------------------------------------------------

!----------------------------------------------------
msoPropertyTypeBoolean EQUATE(2) ! MsoDocProperties
msoPropertyTypeDate    EQUATE(3) ! MsoDocProperties
msoPropertyTypeFloat   EQUATE(5) ! MsoDocProperties
msoPropertyTypeNumber  EQUATE(1) ! MsoDocProperties
msoPropertyTypeString  EQUATE(4) ! MsoDocProperties
!----------------------------------------------------
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::6:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW5.UpdateBuffer
   glo:Queue2.Pointer2 = tra:RecordNumber
   GET(glo:Queue2,glo:Queue2.Pointer2)
  IF ERRORCODE()
     glo:Queue2.Pointer2 = tra:RecordNumber
     ADD(glo:Queue2,glo:Queue2.Pointer2)
    LocalTag = 'Y'
  ELSE
    DELETE(glo:Queue2)
    LocalTag = ''
  END
    Queue:Browse.LocalTag = LocalTag
  IF (LocalTag = 'Y')
    Queue:Browse.LocalTag_Icon = 2
  ELSE
    Queue:Browse.LocalTag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW5.Reset
  FREE(glo:Queue2)
  LOOP
    NEXT(BRW5::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue2.Pointer2 = tra:RecordNumber
     ADD(glo:Queue2,glo:Queue2.Pointer2)
  END
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue2)
  BRW5.Reset
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::6:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue2)
    GET(glo:Queue2,QR#)
    DASBRW::6:QUEUE = glo:Queue2
    ADD(DASBRW::6:QUEUE)
  END
  FREE(glo:Queue2)
  BRW5.Reset
  LOOP
    NEXT(BRW5::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::6:QUEUE.Pointer2 = tra:RecordNumber
     GET(DASBRW::6:QUEUE,DASBRW::6:QUEUE.Pointer2)
    IF ERRORCODE()
       glo:Queue2.Pointer2 = tra:RecordNumber
       ADD(glo:Queue2,glo:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASSHOWTAG Routine
   CASE DASBRW::6:TAGDISPSTATUS
   OF 0
      DASBRW::6:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::6:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::6:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW5.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!-----------------------------------------------
OKButtonPressed         ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        WriteDebug('OKButtonPressed')

        CancelPressed = False
        !-----------------------------------------------------------------
        DO InitColumns
        DO ProgressBar_Setup
        DO XL_Setup
        !-----------------------------------------------------------------
        Access:TRADEACC_ALIAS.CLEARKEY(tra_ali:Account_Number_Key)
        !    tra_ali:Account_Number = LocalHeadAccount
        SET(tra_ali:Account_Number_Key, tra_ali:Account_Number_Key)

        LOOP WHILE Access:TRADEACC_ALIAS.NEXT() = Level:Benign
            WriteDebug('OKButtonPressed LOOP')
            IF tra_ali:Account_Number = LocalHeadAccount THEN
                LOC:RepairCentreType = 'ARC'
            ELSE
               IF tra_ali:RemoteRepairCentre =  0  THEN CYCLE.

                LOC:RepairCentreType = 'RRC'
            END !IF

            IF tra_ali:Account_Number = 'XXXRRC' THEN CYCLE.
            IF DoAll <> 'Y' THEN
                WriteDebug('OKButtonPressed IF DoAll <> "Y" THEN')

                glo:Queue2.Pointer2 = tra_ali:RecordNumber
                GET(glo:Queue2,glo:Queue2.Pointer2)

                CASE ERRORCODE()
                OF 00 ! Found
                    ! null
                OF 30 ! NOT Found
                    CYCLE
                ELSE
                    CancelPressed = True

                    BREAK
                END !CASE
            END !IF

            LOC:FileName = ''
            DO ExportSetup
            IF LOC:FileName = '' THEN CYCLE.

            DO ExportBody
            DO ExportFinalize

            IF CancelPressed = True
                BREAK
            END !IF
        END !LOOP
        !-----------------------------------------------------------------
        DO XL_Finalize
        DO ProgressBar_Finalise

        WriteDebug('OKButtonPressed END')

        POST(Event:CloseWindow)
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
ExportSetup                             ROUTINE
    DATA
temp DATE
    CODE
        !-----------------------------------------------------------------
        WriteDebug('ExportSetup')

        IF CancelPressed
            LOC:FileName = 'exit'

            EXIT
        END !IF
        !-----------------------------------------------------------------
        CancelPressed = False


        !-----------------------------------------------
        ! 23 Oct 2002 John
        ! R003, berrjo :Insert the report exe name EG VODR56.V01
        ! Do not attach the EXE extenstion but show the version number.  Increment the version number each time changed.
        ! Make this standard on all reports sent for correction.
        ! LOC:Version STRING(8) 3.1.0000 3.1 to match sevicebase version (and dictionary) .0000 increment before each compile
        !
        !-----------------------------------------------


        !-----------------------------------------------------------------
        IF CLIP(LOC:FileName) = ''
            DO LoadFileDialog
        END !IF
!        IF LOC:FileName = ''
!            Case MessageEx('No filename chosen.<10,13>   Enter a filename then try again', LOC:ApplicationName,|
!                           'Styles\warn.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
!                Of 1 ! &OK Button
!
!            End!Case MessageEx
!
!            CancelPressed = True
!
!            EXIT
!        END !IF LOC:FileName = ''
        IF LOC:FileName <> '' THEN
           IF LOWER(RIGHT(LOC:FileName, 4)) <> '.xls'
              LOC:FileName = CLIP(LOC:FileName) & '.xls'
           END !IF
           !-----------------------------------------------------------------
           SETCURSOR(CURSOR:Wait)

           DO XL_AddWorkbook
           !-----------------------------------------------------------------
        END !IF
    EXIT
ExportBody                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        WriteDebug('ExportBody')

        DO CreateTitleSheet
        DO CreateDataSheet
        DO WriteHeadSummary
        !-----------------------------------------------------------------
    EXIT
ExportFinalize                          ROUTINE
    DATA
FilenameLength LONG
StartAt        LONG
SUBLength      LONG
    CODE
        !-----------------------------------------------------------------
        WriteDebug('ExportFinalize')

        IF CancelPressed
            DO XL_DropAllWorksheets
            DO XL_Finalize

            EXIT
        END !IF
        !-----------------------------------------------------------------
        !
        Excel{'Sheets("Sheet3").Select'}
        Excel{'ActiveWindow.SelectedSheets.Delete'}

        Excel{'Sheets("Summary").Select'}
        Excel{'Range("A1").Select'}

        Excel{'Application.ActiveWorkBook.SaveAs("' & LEFT(CLIP(LOC:FileName)) & '")'}
        Excel{'Application.ActiveWorkBook.Close()'}
        !-----------------------------------------------------------------
        SETCURSOR()

!        IF MATCH(LOC:Filename, CLIP(LOC:DesktopPath) & '*')
!            FilenameLength = LEN(CLIP(LOC:Filename   ))
!            StartAt        = LEN(CLIP(LOC:DesktopPath)) + 1
!            SUBLength      = FilenameLength - StartAt + 1
!
!            Case MessageEx('Export Completed.'                                & |
!                           '<13,10>'                                          & |
!                           '<13,10>Spreadsheet saved to your Desktop'         & |
!                           '<13,10>'                                          & |
!                           '<13,10>' & SUB(LOC:Filename, StartAt, SUBLength),   |
!                         LOC:ApplicationName,                                   |
!                         'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
!            Of 1 ! &OK Button
!            End!Case MessageEx
!        ELSE
!            Case MessageEx('Export Completed.'                        & |
!                           '<13,10>'                                  & |
!                           '<13,10>Spreadsheet saved to '             & |
!                           '<13,10>'                                  & |
!                           '<13,10>' & CLIP(LOC:Filename),              |
!                         LOC:ApplicationName,                           |
!                         'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
!            Of 1 ! &OK Button
!            End!Case MessageEx
!        END !IF
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
CreateTitleSheet                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        WriteDebug('CreateTitleSheet')

        Excel{'ActiveWorkBook.Sheets("Sheet1").Select'}
        DO XL_SetWorksheetLandscape

        Excel{'ActiveSheet.Columns("A:A").ColumnWidth'} = 28.43
        Excel{'ActiveSheet.Columns("B:B").ColumnWidth'} = 34.00
        Excel{'ActiveSheet.Columns("C:' & sheet:HeadLastCol & '").ColumnWidth'} = 10.29
        !-----------------------------------------------------------------
        LOC:Text = 'Summary'
        sheet:TempLastCol = sheet:HeadLastCol

        DO CreateWorksheetHeader
        !-----------------------------------------------------------------
        ! Prepare For summary details
        !
        Excel{'Range("A' & sheet:HeadSummaryRow & '").Select'}
            Excel{'ActiveCell.Formula'} = 'Summary'
            DO XL_SetBold

        Excel{'Range("A' & sheet:HeadSummaryRow & ':' & sheet:HeadLastCol & sheet:HeadSummaryRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A' & sheet:HeadSummaryRow+1 & '").Select'}
        !-----------------------------------------------------------------
    EXIT
CreateDataSheet                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        WriteDebug('CreateDataSheet')
        IF CancelPressed
            EXIT
        END !IF

        FREE(UserResultsQueue)
        CLEAR(UserResultsQueue)

        !-----------------------------------------------------------------
        !FREE(HeadAccountQueue)
        !CLEAR(HeadAccountQueue)
        !
        !FREE(SubAccountQueue)
        !CLEAR(SubAccountQueue)
        !-----------------------------------------------------------------
        DO XL_AddSheet

        DO XL_SetWorksheetLandscape
        DO InitColumns
        !-----------------------------------------------------------------
        LOC:Text          = CLIP(LOC:ProgramName)
        LOC:SectionName   = CLIP(LOC:ProgramName) & ' ' & CLIP(tra_ali:Account_Number)
        excel:CommentText = 'Complete Jobs'

        DO CreateSectionHeader
        !------------------------------------------
        ! Date_Booked_Key          KEY(job:date_booked),DUP,NOCASE
        ! DateCompletedKey         KEY(job:Date_Completed),DUP,NOCASE
        !
        Access:JOBS.ClearKey(job:DateCompletedKey)
        job:Date_Completed = LOC:StartDate
        SET(job:DateCompletedKey, job:DateCompletedKey)
        !------------------------------------------
        Progress:Text    = LOC:SectionName
        RecordsToProcess = RECORDS(JOBS)

        DO ProgressBar_LoopPre
        !-----------------------------------------------------------------
        LOOP UNTIL Access:JOBS.Next()
            !-------------------------------------------------------------
            WriteDebug('CreateDataSheet LOOP(' & job:Ref_Number & ')')

            DO ProgressBar_Loop

            IF CancelPressed
                BREAK
            END !IF
            !-------------------------------------------------------------
            IF job:Date_Completed > LOC:EndDate
                BREAK
            END !IF
            !-------------------------------------------------------------
            DO WriteColumns
            !-------------------------------------------------------------
        END !LOOP

        DO ProgressBar_LoopPost
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        DO WriteDataSummary
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
CreateSectionHeader                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        WriteDebug('CreateSectionHeader')

        sheet:TempLastCol = sheet:DataLastCol

        DO CreateWorksheetHeader
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataSectionRow & '").Select'}
            Excel{'ActiveCell.Formula'}                     = 'Section Name:'
                Excel{'Range("B' & sheet:DataSectionRow & '").Select'}
                Excel{'ActiveCell.Formula'}                 = CLIP(LOC:SectionName)
                DO XL_SetBold
                DO XL_HorizontalAlignmentLeft

        Excel{'Range("D' & sheet:DataSectionRow & '").Select'}
            Excel{'ActiveCell.Formula'}                     = 'Total Records:'
            DO XL_HorizontalAlignmentRight
                Excel{'Range("F' & sheet:DataSectionRow & '").Select'}
                Excel{'ActiveCell.Formula'}                 = 0
                DO XL_HorizontalAlignmentLeft
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataSectionRow & ':' & sheet:DataLastCol & sheet:DataSectionRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------
        DO SetColumns
        !-----------------------------------------------
    EXIT
CreateWorksheetHeader                                                               ROUTINE
    DATA
CurrentRow LONG(3)
    CODE
        !-----------------------------------------------       
        WriteDebug('CreateWorksheetHeader')

        Excel{'ActiveSheet.Name'} = CLIP(LOC:Text)
        !-----------------------------------------------       
        Excel{'Range("A1:' & sheet:TempLastCol & '1").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A1").Select'}
            Excel{'ActiveCell.Font.Size'}          = 14
            DO XL_SetBold
            Excel{'ActiveCell.Formula'}            = CLIP(LOC:ProgramName) & ' ' & CLIP(tra_ali:Account_Number)

        Excel{'Range("D3").Select'}
            Excel{'ActiveCell.Formula'}   = Clip(tmp:VersionNumber)
            DO XL_SetBold
            Excel{'ActiveCell.Font.Size'} = 8


        !-----------------------------------------------
        Excel{'Range("A' & CurrentRow & ':' & sheet:TempLastCol & CurrentRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A' & CurrentRow & '").Select'}
            Excel{'ActiveCell.Formula'}            = 'Criteria'
            DO XL_SetBold
        !-----------------------------------------------
        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.Formula'}        = 'Completion Start Date:'
                DO XL_ColRight
                    DO XL_FormatDate
                    DO XL_HorizontalAlignmentLeft
                    Excel{'ActiveCell.Formula'}    = LEFT(FORMAT(LOC:StartDate, @D8))

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.Formula'}        = 'Completion End Date:'
                DO XL_ColRight
                    DO XL_FormatDate
                    DO XL_HorizontalAlignmentLeft
                    Excel{'ActiveCell.Formula'}    = LEFT(FORMAT(LOC:EndDate, @D8))

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.Formula'}        = 'Created By:'
                DO XL_ColRight
                    Excel{'ActiveCell.Formula'}    = LOC:UserName
                    DO XL_HorizontalAlignmentLeft

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.Formula'}        = 'Created Date:'
                DO XL_ColRight
                    DO XL_FormatDate
                    DO XL_HorizontalAlignmentLeft
                    Excel{'ActiveCell.Formula'}    = LEFT(FORMAT(TODAY(), @D8))

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.Formula'}        = 'Report Reference Number:'
                DO XL_ColRight
                    Excel{'ActiveCell.Formula'}    = 'VODR0023'
                    DO XL_HorizontalAlignmentLeft
        !-----------------------------------------------
        Excel{'Range("A4:' & sheet:TempLastCol & CurrentRow &'").Select'}
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------
        sheet:DataSectionRow = CurrentRow           + 2
        sheet:DataHeaderRow  = sheet:DataSectionRow + 2

        sheet:HeadSummaryRow = sheet:DataSectionRow 
        !-----------------------------------------------
    EXIT

!-----------------------------------------------
FormatColumns                                                   ROUTINE
    DATA
ColumnPos LONG(1)
    CODE
        !-----------------------------------------------
        WriteDebug('FormatColumns')

        LOOP x# = 1 TO RECORDS(HeaderQueue)
            !-------------------------------------------
            GET(HeaderQueue, x#)
            IF ERRORCODE()
                CYCLE
            END !IF

            Excel{'Range("' & CHR(64+x#) & sheet:DataHeaderRow+1 & ':' & CHR(64+x#) & sheet:DataHeaderRow+RecordCount & '").Select'}
                Excel{'Selection.NumberFormat'} = head:NumberFormat
                DO XL_HorizontalAlignmentRight
            !-------------------------------------------
        END !LOOP
        !-----------------------------------------------
    EXIT
InitColumns                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        WriteDebug('InitColumns')

        FREE(HeaderQueue)

        !-----------------------------------------------

        !O WriteColumn_JobNumber         ! *
        head:ColumnName       = 'SB Job Number' ! 13 Sep 2002 John from "Job Number"
            head:ColumnWidth  = 22.00
            head:NumberFormat = '###0'
            ADD(HeaderQueue)

        head:ColumnName       = 'Franchise Branch Number'
            head:ColumnWidth  = 14.00
            head:NumberFormat = '00' ! 24 Oct 2002 John, Changed from "###0"
            ADD(HeaderQueue)

        !-----------------------------------------------
        ! 19 Sep 2002 John, Franchise Job Number missing
        ! 24 Oct 2002 John, MICHALAN: This column [Franchise Job Number] to be inserted after Franchise Branch Number column.
        !                   Franchise job number only to appear ie. Take out the SB job number and branch number in
        !                       front of the Franchise Job Number eg. "12-0112" should read "12".
        head:ColumnName       = 'Franchise Job Number'
            head:ColumnWidth  = 14.00
            head:NumberFormat = '###0'
            ADD(HeaderQueue)
        !-----------------------------------------------

        !O WriteColumn_EngineerSurname   ! 17 May 2002 John
        head:ColumnName       = 'Engineer Surname' ! 13 Sep 2002 John from "Surname"
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        !O WriteColumn_EngineerForename  ! 17 May 2002 John
        head:ColumnName       = 'Engineer Forename'  ! 13 Sep 2002 John from "Forename"
            head:ColumnWidth  = 15.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        !O WriteColumn_Engineer          ! 17 May 2002 John
        head:ColumnName       = 'Engineer'
            head:ColumnWidth  = 10.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)


        !-----------------------------------------------
        ! 24 Oct 2002 John
        ! Neil IRC, okay.....so they have been allocated to a RRC engineer, but not worked on by ARC.
        !   Add another colum to indicate fact that orginal RRC engineer still on job.
!        head:ColumnName       = 'Engineer''s Site'
!            head:ColumnWidth  = 15.00
!            head:NumberFormat = chr(64) ! Text (AT) Sign
!            ADD(HeaderQueue)
        !-----------------------------------------------


        !-----------------------------------------------
        !O WriteColumn_AccountNumber     ! Account No.
        head:ColumnName       = 'Head Account Number' ! 13 Sep 2002 John from "Account Number"
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        !O WriteColumn_AccountName       ! Account Name
        head:ColumnName       = 'Head Account Name' ! 13 Sep 2002 John from "Account Name"
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)
        !-----------------------------------------------

        !-----------------------------------------------
        ! Add two new columns next to those above called 'Sub Account Name' and 'Sub Account Number'.
        ! Populate these fields with the relevant infomation
        !
        head:ColumnName       = 'Sub Account Number' ! 13 Sep 2002 John from "Account Number"
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Sub Account Name' ! 13 Sep 2002 John from "Account Name"
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)
        !-----------------------------------------------

        !O WriteColumn_OrderNumber       ! Order Number
        head:ColumnName       = 'Order Number'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        !O WriteColumn_ModelNumber       !
        head:ColumnName       = 'Model Number'
            head:ColumnWidth  = 25.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        !-----------------------------------------------
        ! 13 Sep 2002 John swap the "Exchange IMEI" and the "Original IMEI"
        !
        !O WriteColumn_OriginalIMEI      ! Original IMEI
        head:ColumnName       = 'Original IMEI'
            head:ColumnWidth  = 20.00
            head:NumberFormat = '###############'
            ADD(HeaderQueue)

        !O WriteColumn_ExchangeEMEI      ! Exchange EMEI
        head:ColumnName       = 'Exchange IMEI'
            head:ColumnWidth  = 20.00
            head:NumberFormat = '###############'
            ADD(HeaderQueue)
        !-----------------------------------------------

        !-----------------------------------------------
        ! 13 Sep 2002 John
        ! 12. Remove the "Accessory" Column
        !head:ColumnName       = 'Accessories'
        !    head:ColumnWidth  = 20.00
        !    head:NumberFormat = chr(64) ! Text (AT) Sign
        !    ADD(HeaderQueue)
        !-----------------------------------------------

        !-----------------------------------------------
        ! 13 Sep 2002 John
        ! 13. Amend the "Job Type" and "Repair Type" to read "Chargeable Job Type" and "Chargeable Repair Type"
        head:ColumnName       = 'Chargeable Charge Type' ! 13 Sep 2002 John from "Job Type"
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Chargeable Repair Type' ! 13 Sep 2002 John from "Repair Type"
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)
        ! 13 Sep 2002 John
        ! 14. Add two new columns called  "Warranty Charge Type" and "Warranty Repair Type"
        head:ColumnName       = 'Warranty Charge Type' ! 13 Sep 2002 John from "Job Type"
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Warranty Repair Type' ! 13 Sep 2002 John from "Repair Type"
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Repair Type' ! 13 Sep 2002 John from "Repair Type"
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)
        !-----------------------------------------------

        !O WriteColumn_ConsignmentNumber ! Consignment Number
        head:ColumnName       = 'Consignment Number'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)
        !-----------------------------------------------
        sheet:DataLastCol = NumberToColumn(RECORDS(HeaderQueue))
        !-----------------------------------------------
    EXIT
SetColumns                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        WriteDebug('SetColumns')

        Excel{'Range("A' & sheet:DataHeaderRow & ':' & sheet:DataLastCol & sheet:DataHeaderRow & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & sheet:DataHeaderRow  & '").Select'}
        !-----------------------------------------------
        LOOP x# = 1 TO RECORDS(HeaderQueue)
            GET(HeaderQueue, x#)

            Excel{'ActiveCell.Formula'}         = head:ColumnName
                Excel{'ActiveCell.ColumnWidth'} = head:ColumnWidth
                DO XL_ColRight
        END !LOOP
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataHeaderRow+1  & '").Select'}
        Excel{'ActiveWindow.FreezePanes'} = True
        !-----------------------------------------------
    EXIT
WriteColumns                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        WriteDebug('WriteColumns')
!        LOADSUBTRACC(job:Account_Number)
!        LOADTRADEACC(sub:Main_Account_Number)
!        IF tra:Account_Number <> tra_ali:Account_Number
!          EXIT
!        END


        GetSubAccount(job:Account_Number)
        IF saQ:AccountNumber <> job:Account_Number
            EXIT
        ELSIF saQ:HeadAccountNumber <> tra_ali:Account_Number THEN
            EXIT
        END !IF
!        !---
        DO SyncExchange
        !---
        GetHeadAccount(saQ:HeadAccountNumber)
        !===============================================
        WriteDebug('FormatColumns OK')

        RecordCount += 1
        !-----------------------------------------------
        LOC:UserCode = job:Engineer
        DO GetUserDetailsFromQueue

        uq:JobsBooked += 1

        PUT(UserResultsQueue, +uq:UserCode)
        !-----------------------------------------------
        haQ:JobsBooked += 1
        PUT(HeadAccount_Queue)

        saQ:JobsBooked += 1
        PUT(SubAccount_Queue)
        !-----------------------------------------------

        !-----------------------------------------------
        ! 19 Sep 2002 John, Franchise Job Number missing
        ! 24 Oct 2002 John, MICHALAN: This column [Franchise Job Number] to be inserted after Franchise Branch Number column.
        !                   Franchise job number only to appear ie. Take out the SB job number and branch number in
        !                       front of the Franchise Job Number eg. "12-0112" should read "12".
        WriteColumn( job:Ref_Number ) ! Job Number
        DO WriteColumn_FranchiseBranchNumber
        WriteColumn( GetWebJobNumber(job:Ref_Number) )
        !-----------------------------------------------

        DO WriteColumn_EngineerSurname   ! 17 May 2002 John
        DO WriteColumn_EngineerForename  ! 17 May 2002 John
        DO WriteColumn_Engineer          ! 17 May 2002 John


        !-----------------------------------------------
        ! 24 Oct 2002 John
        ! Neil IRC, okay.....so they have been allocated to a RRC engineer, but not worked on by ARC.
        !   Add another colum to indicate fact that orginal RRC engineer still on job.
!        WriteColumn( uq:SiteLocation )
        !-----------------------------------------------


        !-----------------------------------------------
        ! 13 Sep 2002 John Add Head Account/Sub Account
        DO WriteColumn_HeadAccountNumber ! Account No.
        DO WriteColumn_HeadAccountName   ! Account Name

        DO WriteColumn_SubAccountNumber  ! Account No.
        DO WriteColumn_SubAccountName    ! Account Name
        !-----------------------------------------------

        DO WriteColumn_OrderNumber       ! Order Number
        DO WriteColumn_ModelNumber       !

        !-----------------------------------------------
        ! 13 Sep 2002 John swap the "Exchange IMEI" and the "Original IMEI"
        DO WriteColumn_OriginalIMEI      ! Original IMEI
        DO WriteColumn_ExchangeEMEI      ! Exchange EMEI
        !-----------------------------------------------

        !-----------------------------------------------
        ! 13 Sep 2002 John
        ! 12. Remove the "Accessory" Column
        !DO WriteColumn_Accessories       ! Accessories
        !-----------------------------------------------

        !-----------------------------------------------
        ! 13 Sep 2002
        ! 13. Amend the "Job Type" and "Repair Type" column to read "Chargeable Job Type" and "Chargeable repair Type"
        !
        !DO WriteColumn_JobType           ! Job Type
        DO WriteColumn_ChargeableChargeType
        DO WriteColumn_ChargeableRepairType
        ! 14. Add two new columns called "Warranty Charge Type" and "Warranty Repair Type"
        !     Put these nezt to the columns in (23.) above
        DO WriteColumn_WarrantyChargeType
        DO WriteColumn_WarrantyRepairType
        !-----------------------------------------------

        DO WriteColumn_RepairType        ! Repair Type
        DO WriteColumn_ConsignmentNumber ! Consignment Number

        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            DO PassViaClipboard
        END !IF
        !-----------------------------------------------
        DO XL_ColFirst
        DO XL_RowDown
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
WriteColumn_JobNumber                   ROUTINE ! Write value to data cell
    DATA
    CODE
        !-----------------------------------------------
        ! Job Number
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = job:Ref_Number

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Ref_Number

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_FranchiseBranchNumber  ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = haq:BranchIdentification

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = haq:BranchIdentification

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_EngineerSurname                     ROUTINE ! 28 May 2002 John
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & uq:Surname

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = uq:Surname

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_EngineerForename                     ROUTINE ! 28 May 2002 John
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & uq:Forename

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = uq:Forename

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_Engineer                     ROUTINE ! 28 May 2002 John
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Engineer

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Engineer

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_HeadAccountNumber                                     ROUTINE !
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & saQ:HeadAccountNumber

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = saQ:HeadAccountNumber

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_HeadAccountName                                     ROUTINE !
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & saQ:HeadAccountName

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = saQ:HeadAccountName

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_SubAccountNumber                                     ROUTINE !
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & saQ:AccountNumber

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = saQ:AccountNumber

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_SubAccountName                                     ROUTINE !
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & saQ:AccountName

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = saQ:AccountName

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_OrderNumber                                                         ROUTINE ! Order Number
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Order_Number

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Order_Number

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_ModelNumber                 ROUTINE  ! Model
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Model_Number

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}      = job:Model_Number

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_ExchangeEMEI                                    ROUTINE ! Exchange EMEI
    DATA
    CODE
        !-----------------------------------------------
        ! Request from Harvey on site in Ireland 17 May 2002
        !   1. Show existing IMEI if no Exchange IMEI
        !   2. Show Exchange IMEI as "NONE" if no Exchange IMEI
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            IF ExchangeOK
                clip:Value = CLIP(clip:Value) & '<09>' & xch:ESN
            ELSE
                clip:Value = CLIP(clip:Value) & '<09>' & 'NONE'
            END !IF

            EXIT
        END !IF
        !-----------------------------------------------
        IF ExchangeOK
            Excel{'ActiveCell.Formula'}  = xch:ESN
        ELSE
            Excel{'ActiveCell.Formula'}  = 'NONE'
        END !IF

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_OriginalIMEI                                    ROUTINE ! Original IMEI
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:ESN

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:ESN

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
!-------------------------------------------------------
! Request from Harvey on site in Irland 17 May 2002
!   1. Show existing IMEI if no Exchange IMEI
!   2. Show Exchange IMEI as "NONE" if no Exchange IMEI
!WriteColumn_OriginalIMEI                                    ROUTINE ! Original IMEI
!    DATA
!    CODE
!        !-----------------------------------------------
!        IF excel:OperatingSystem < 5
!            IF ExchangeOK
!                clip:Value = CLIP(clip:Value) & '<09>' & job:ESN
!            ELSE
!                clip:Value = CLIP(clip:Value) & '<09>'
!            END !IF
!
!            EXIT
!        END !IF
!        !-----------------------------------------------
!        IF ExchangeOK
!            Excel{'ActiveCell.Formula'}  = '''' & job:ESN
!        END !IF
!
!        DO XL_ColRight
!        !-----------------------------------------------
!    EXIT
!-------------------------------------------------------
WriteColumn_Accessories                                     ROUTINE ! Accessories
    DATA
    CODE
        !-----------------------------------------------
        DO CreateAccessoriesList
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & LOC:Text

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  =  LOC:Text

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_JobType                                         ROUTINE ! Job Type
    DATA
Temp STRING('Chargable/Warranty')
    CODE
        !-----------------------------------------------
        IF job:Chargeable_Job = 'YES'
            IF job:Warranty_Job = 'YES'
                Temp = 'Chargable/Warranty'
            ELSE
                Temp = 'Chargable'
            END !IF

        ELSIF job:Warranty_Job = 'YES'
            Temp = 'Warranty'

        ELSE
            Temp = ''
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & Temp

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_ChargeableChargeType                                         ROUTINE ! Job Type
    DATA
Temp LIKE(job:Charge_Type)
    CODE
        !-----------------------------------------------
        IF job:Chargeable_Job = 'YES'
            Temp = job:Charge_Type
        ELSE
            Temp = ''
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & Temp

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_ChargeableRepairType                                         ROUTINE ! Job Type
    DATA
Temp LIKE(job:Repair_Type)
    CODE
        !-----------------------------------------------
        IF job:Chargeable_Job = 'YES'
            Temp = job:Repair_Type
        ELSE
            Temp = '' ! 'Chargable'
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & Temp

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_WarrantyChargeType                                         ROUTINE ! Job Type
    DATA
Temp LIKE(job:Warranty_Charge_Type)
    CODE
        !-----------------------------------------------
        IF job:Warranty_Job = 'YES'
            Temp = job:Warranty_Charge_Type
        ELSE
            Temp = ''
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & Temp

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_WarrantyRepairType                                         ROUTINE ! Job Type
    DATA
Temp LIKE(job:Repair_Type_Warranty)
    CODE
        !-----------------------------------------------
        IF job:Warranty_Job = 'YES'
            Temp = job:Repair_Type_Warranty
        ELSE
            Temp = '' ! 'Chargable'
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & Temp

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_RepairType                                      ROUTINE ! Repair Type
    DATA
Temp LIKE(job:Repair_Type)
AddComment LONG ! BOOL
    CODE
        !-----------------------------------------------
        IF job:Chargeable_Job = 'YES'
            IF job:Warranty_Job = 'YES'
                AddComment = True
            END !IF
            Temp = job:Repair_Type

        ELSIF job:Warranty_Job = 'YES'
            Temp = job:Repair_Type_Warranty

        ELSE
            Temp = 'Unspecified'

        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & Temp
            IF AddComment
                clip:Value = CLIP(clip:Value) & '/' & job:Repair_Type_Warranty
            END !IF

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp
        IF AddComment
            excel:CommentText = job:Repair_Type_Warranty

            DO XL_AddComment
        END !IF

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_ConsignmentNumber           ROUTINE ! Consignment Number
    DATA
Temp LIKE(job:Consignment_Number)
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Consignment_Number

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Consignment_Number

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
WriteDataSummary                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        ! summary details
        !
        DO FormatColumns

        Excel{'Range("E' & sheet:DataSectionRow & '").Select'}
            DO XL_HorizontalAlignmentLeft
            Excel{'ActiveCell.Formula'}         = RecordCount

        Excel{'Range("F' & sheet:DataSectionRow & '").Select'}
            DO XL_HorizontalAlignmentRight
            Excel{'ActiveCell.Formula'}         = 'Showing'

        Excel{'Range("G' & sheet:DataSectionRow & '").Select'}
            DO XL_HorizontalAlignmentLeft
            Excel{'ActiveCell.Formula'}         = '=SUBTOTAL(2, A' & sheet:DataHeaderRow+1 & ':A' & sheet:DataHeaderRow+RecordCount & ')'

        Excel{'Range("A' & sheet:DataHeaderRow & ':' & CLIP(sheet:DataLastCol) & sheet:DataHeaderRow+RecordCount & '").Select'}
            DO XL_DataAutoFilter
        !-----------------------------------------------------------------
    EXIT
WriteHeadSummary                             ROUTINE
    DATA
QueueIndex   LONG
ResultsCount LONG
Offset       LONG

HeaderRow    LONG
FirstRow     LONG
LastRow      LONG
TotalsRow    LONG

CurrentRow   LONG
    CODE
        !-----------------------------------------------------------------
        ! summary sheet details
        !
        WriteDebug('WriteHeadSummary')

        Excel{'ActiveWorkBook.Sheets("Summary").Select'}
        !-----------------------------------------------------------------
        ResultsCount = RECORDS(UserResultsQueue)
        Offset       = ResultsCount

        HeaderRow = sheet:HeadSummaryRow+1
        FirstRow  = HeaderRow + 1
        LastRow   = HeaderRow + ResultsCount
        TotalsRow = LastRow   + 1

        Excel{'Range("A' & HeaderRow & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'} = 'Engineer Surname' ! 13 Sep 2002 John Changed from "Surname"
            Excel{'ActiveCell.Offset(0, 1).Formula'} = 'Engineer Forename' ! 13 Sep 2002 John Changed from "Forename"
            Excel{'ActiveCell.Offset(0, 2).Formula'} = 'User Code' ! 13 Sep 2002 John Changed from "User"
            !-----------------------------------------------------------------
            ! Neil IRC, okay.....so they have been allocated to a RRC engineer, but not worked on by ARC.
            !   Add another colum to indicate fact that orginal RRC engineer still on job.
            !Excel{'ActiveCell.Offset(0, 3).Formula'} = 'Location' ! 24 Oct 2002 John, Add Engineers Location
            !-----------------------------------------------------------------
            Excel{'ActiveCell.Offset(0, 3).Formula'} = 'Jobs Completed' ! 13 Sep 2002 John Changed from "Jobs Complete"
            Excel{'ActiveCell.Offset(0, 4).Formula'} = 'Hit Rate %'
                    
        Excel{'Range("A' & HeaderRow & ':' & sheet:HeadLastCol & HeaderRow & '").Select'}
            DO XL_HorizontalAlignmentRight
            !DO XL_HorizontalAlignmentLeft
            DO XL_SetBold
            DO XL_SetWrapText
            DO XL_SetTitle
            DO XL_SetGrid
        !-----------------------------------------------------------------
        Excel{'Range("A' & FirstRow & '").Select'}

        IF ResultsCount = 0
            Excel{'ActiveCell.Formula'} = 'No Jobs Found'

            EXIT
        END !IF

        SORT(UserResultsQueue, +uq:Surname, +uq:Forename)
        LOOP QueueIndex = 1 TO ResultsCount
            GET(UserResultsQueue, QueueIndex)
            Excel{'ActiveCell.Offset(0, 0).Formula'}      = uq:Surname
            Excel{'ActiveCell.Offset(0, 1).Formula'}      = uq:Forename
            Excel{'ActiveCell.Offset(0, 2).Formula'}      = uq:UserCode

            !-------------------------------------------------------------
            ! 24 Oct 2002 John
            ! Neil IRC, okay.....so they have been allocated to a RRC engineer, but not worked on by ARC.
            !   Add another colum to indicate fact that orginal RRC engineer still on job.
!            IF (LOC:RepairCentreType = 'ARC') AND (tra_ali:SiteLocation = uq:SiteLocation)
!                Excel{'ActiveCell.Offset(0, 3).Formula'}  = 'ARC'
!            ELSE
!                Excel{'ActiveCell.Offset(0, 3).Formula'}  = 'RRC'
!            END !IF
            !-------------------------------------------------------------

            Excel{'ActiveCell.Offset(0, 3).NumberFormat'} = '#,##0'
            Excel{'ActiveCell.Offset(0, 3).Formula'}      = uq:JobsBooked

            Excel{'ActiveCell.Offset(0, 4).NumberFormat'} = '0.00%'
            Excel{'ActiveCell.Offset(0, 4).FormulaR1C1'}  = '=IF(R[+' & Offset & ']C[-1]=0, 0, RC[-1]/R[+' & Offset & ']C[-1])'

            DO XL_RowDown
            Offset -= 1
        END !LOOP
        !-----------------------------------------------------------------
        Excel{'Range("A' & TotalsRow & ':B' & TotalsRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("C' & TotalsRow & ':' & sheet:HeadLastCol & TotalsRow & '").Select'}
            DO XL_SetBold
            DO XL_SetTitle
            DO XL_SetGrid

        Excel{'Range("C' & TotalsRow & '").Select'}
            Excel{'ActiveCell.Formula'}           = 'Total Jobs'

        Excel{'Range("D' & TotalsRow & '").Select'}
            Excel{'ActiveCell.NumberFormat'}      = '#,##0'
            Excel{'ActiveCell.FormulaR1C1'}       = '=SUM(R[-1]C:R[-' & ResultsCount & ']C)'

        Excel{'Range("E' & TotalsRow & '").Select'}
            Excel{'ActiveCell.NumberFormat'}      = '0.00%'
            Excel{'ActiveCell.FormulaR1C1'}       = '=SUM(R[-1]C:R[-' & ResultsCount & ']C)'
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
GetUserName                            ROUTINE
    DATA
CommandLine STRING(255)
tmpPos      LONG
    CODE
        !-----------------------------------------------
        CommandLine = CLIP(COMMAND(''))

!        LOC:UserName = LOC:ApplicationName
!
!        Case MessageEx('Fail User Check For "' & CLIP(CommandLine) & '"?', LOC:ApplicationName & ' DEBUG',|
!                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
!            Of 1 ! &Yes Button
!                POST(Event:CloseWindow)
!
!                EXIT
!            Of 2 ! &No Button
!                EXIT
!        End!Case MessageEx
        !-----------------------------------------------
        tmpPos = INSTRING('%', CommandLine)
        IF NOT tmpPos
            Case Missive('You must run this report from ServiceBase''s "Custom Reports".','ServiceBase 3g',|
                           'mstop.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive

            POST(Event:CloseWindow)

           EXIT
        END !IF tmpPos
        !-----------------------------------------------
        SET(USERS)
        Access:USERS.Clearkey(use:Password_Key)
        glo:Password = CLIP(SUB(CommandLine, tmpPos + 1, 30))
      
        Access:USERS.Clearkey(use:Password_Key)
        use:Password = glo:Password
        If Access:USERS.Tryfetch(use:Password_Key)
            Case Missive('Error! Cannot find the user''s details.','ServiceBase 3g',|
                           'mstop.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missivex

            POST(Event:CloseWindow)

           EXIT
        End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        !-----------------------------------------------
        LOC:UserName = use:Forename

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = use:Surname
        ELSE
            LOC:UserName = CLIP(use:Forename) & ' ' & use:Surname 
        END !IF

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = '<' & use:User_Code & '>'
        END !IF
        !-----------------------------------------------
    EXIT
GetFileName                             ROUTINE ! Generate default file name
    DATA
local:Desktop         CString(255)
DeskTopExists   BYTE
ApplicationPath STRING(255)
excel:ProgramName       Cstring(255)
    CODE
    excel:ProgramName = 'Completed Jobs Report'
    SHGetSpecialFolderPath( GetDesktopWindow(), local:Desktop, 5, FALSE )
    local:Desktop = Clip(local:Desktop) & '\ServiceBase Export'

    !Does the Export Folder already Exists?
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    local:Desktop = Clip(local:Desktop) & '\' & Clip(excel:ProgramName)
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    loc:Path = local:Desktop
    loc:FileName = Clip(local:Desktop) & '\' & Clip(tra_ali:Account_Number) & ' ' & Format(Today(),@d12)
!
!
!GetFileName                             ROUTINE ! Generate default file name
!    DATA
!Desktop         CString(255)
!DeskTopExists   BYTE
!ApplicationPath STRING(255)
!    CODE
!        !-----------------------------------------------
!        ! Generate default file name
!        !
!        WriteDebug('GetFileName')
!        !-----------------------------------------------
!        ! 4 Dec 2001 John Stop using def:Export_Path as previously directed
!        ! 4 Dec 2001 John Use generated date to create file name not parameters LOC:StartDate,LOC:EndDate
!        !
!        IF CLIP(LOC:FileName) <> ''
!            ! Already generated 
!            EXIT
!        END !IF
!
!        DeskTopExists = False
!
!        SHGetSpecialFolderPath( GetDesktopWindow(), Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
!        LOC:DesktopPath = Desktop
!
!        ApplicationPath = Desktop & '\' & LOC:ApplicationName & ' Export'
!        IF EXISTS(CLIP(ApplicationPath))
!            LOC:Path      = CLIP(ApplicationPath) & '\'
!            DeskTopExists = True
!
!        ELSE
!            Desktop  = CLIP(ApplicationPath)
!
!            IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!!                MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!!            ELSE
!!                MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
!            END !IF
!
!            IF EXISTS(CLIP(ApplicationPath))
!                LOC:Path      = CLIP(ApplicationPath) & '\'
!                DeskTopExists = True
!            END !IF
!
!        END !IF
!
!        IF DeskTopExists
!            ApplicationPath = CLIP(LOC:Path) & CLIP(LOC:ProgramName)
!            Desktop         = CLIP(ApplicationPath)
!
!            IF NOT EXISTS(CLIP(ApplicationPath))
!                IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!!                    MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!!                ELSE
!!                    MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
!                END !IF
!            END !IF
!
!            IF EXISTS(CLIP(ApplicationPath))
!                LOC:Path = CLIP(ApplicationPath) & '\'
!            END !IF
!        END !IF
!
!        !-----------------------------------------------
!        ! 23 Oct 2002 John
!        ! R003, berrjo :Insert the report exe name EG VODR56.V01
!        ! Do not attach the EXE extenstion but show the version number.  Increment the version number each time changed.
!        ! Make this standard on all reports sent for correction.
!        ! LOC:Version STRING(8) 3.1.0000 3.1 to match sevicebase version (and dictionary) .0000 increment before each compile
!        !
!        !LOC:FileName = SHORTPATH(CLIP(LOC:Path)) & CLIP(tra_ali:Account_Number) & ' VODR0023 ' & FORMAT(TODAY(), @D12) & '.xls'
!        LOC:FileName = SHORTPATH(CLIP(LOC:Path)) & CLIP(tra_ali:Account_Number) & ' VODR0023 ' & FORMAT(TODAY(), @D12) & '.xls'
!        !-----------------------------------------------
!        
!
!        WriteDebug('GetFileName(' & CLIP(LOC:FileName) & ')')
!        !-----------------------------------------------
!    EXIT
LoadFileDialog                          ROUTINE ! Ask user if file name is empty
    DATA
OriginalPath STRING(255)
    CODE
        !-----------------------------------------------
        WriteDebug('LoadFileDialog')

        IF CLIP(LOC:Filename) = ''
            DO GetFileName 
        END !IF

!        OriginalPath = PATH()
!            IF NOT FILEDIALOG('Save Spreadsheet', LOC:Filename, 'Microsoft Excel Workbook|*.XLS', |
!                FILE:KeepDir + FILE:Save + FILE:NoError + FILE:LongName)
!
!                LOC:Filename = ''
!            END !IF
!        SETPATH(OriginalPath)

        UPDATE()
        DISPLAY()
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
PassViaClipboard                                            ROUTINE
    DATA
ActiveWorkSheet CSTRING(20)
ActiveCell      CSTRING(20)
    CODE
        !-----------------------------------------------
        IF clip:Saved = False
            clip:OriginalValue = CLIPBOARD()
            clip:Saved         = True
        END !IF
        !-----------------------------------------------
        SETCLIPBOARD(CLIP(clip:Value))

        ActiveWorkSheet = Excel{'ActiveSheet'}
            Excel{ActiveWorkSheet & '.Paste()'}
        Excel{prop:Release} = ActiveWorkSheet
        !-----------------------------------------------
    EXIT
RefreshWindow                          ROUTINE
    !|
    !| This routine is used to keep all displays and control templates current.
    !|
    IF MainWindow{Prop:AcceptAll} THEN EXIT.
    DISPLAY()
    !ForceRefresh = False
!-----------------------------------------------
ResetClipboard                                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF clip:Saved = False
            EXIT
        END !IF

        SETCLIPBOARD(clip:OriginalValue)
        !-----------------------------------------------
    EXIT
GetUserDetailsFromQueue                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        ! on entry LOC:UserCode = User code to look up and
        !   store the details in the queue
        !-----------------------------------------------------------------
        WriteDebug('GetUserDetailsFromQueue="' & LOC:UserCode & '"')

        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        uq:UserCode = LOC:UserCode
        GET(UserResultsQueue, +uq:UserCode)
        IF ERRORCODE()
            ! Not in queue - ADD
            CLEAR(UserResultsQueue)
                DO LoadUSERS
                IF Result
                    uq:UserCode     = LOC:UserCode
                    uq:Forename     = use:Forename
                    uq:Surname      = use:Surname
                    !---------------------------------------------------------
                    ! 24 Oct 2002 John
                    ! Neil IRC, okay.....so they have been allocated to a RRC engineer, but not worked on by ARC.
                    !   Add another colum to indicate fact that orginal RRC engineer still on job.
                    uq:SiteLocation = use:Location ! Link to TRADEACC, use:Location = tra:SiteLocation
                    !---------------------------------------------------------
                ELSE
                    uq:UserCode   = LOC:UserCode
                END ! IF


            ADD(UserResultsQueue, +uq:UserCode)
        END !IF
        !-----------------------------------------------------------------
    EXIT
CreateAccessoriesList                                       ROUTINE
    DATA
Count LONG
    CODE
        !-----------------------------------------------
        WriteDebug('CreateAccessoriesList')

        DO XL_HorizontalAlignmentRight

        LOC:Text = ''

        Access:JOBACC.ClearKey(jac:Ref_Number_Key)
        jac:Ref_Number = job:Ref_Number
        SET(jac:Ref_Number_Key, jac:Ref_Number_Key)

        LOOP UNTIL Access:JOBACC.NEXT()
            Count += 1

            IF Count = 1
                LOC:Text = CLIP(jac:Accessory)
            ELSE
                LOC:Text = CLIP(LOC:Text) & ', ' & CLIP(jac:Accessory)
            END !IF

            IF LEN(LOC:Text) > 250
                BREAK
            END !IF

        END !LOOP

        WriteDebug('CreateAccessoriesList="' & CLIP(LOC:Text) & '"')
        !-----------------------------------------------
    EXIT
LoadUSERS                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        WriteDebug('LoadUSERS')

        Result = False
        !-----------------------------------------------
!        User_Code_Key            KEY(use:User_Code),NOCASE,PRIMARY
!        Surname_Active_Key       KEY(use:Active,use:Surname),DUP,NOCASE
!        User_Code_Active_Key     KEY(use:Active,use:User_Code),DUP,NOCASE
!        Logged_In_Key            KEY(use:Logged_In,use:Surname),DUP,NOCASE
!        password_key             KEY(use:Password),NOCASE
!        surname_key              KEY(use:Surname),DUP,NOCASE
!        Active_Team_Surname      KEY(use:Team,use:Active,use:Surname),DUP,NOCASE
!        Team_Surname             KEY(use:Team,use:Surname),DUP,NOCASE
!        User_Type_Active_Key     KEY(use:User_Type,use:Active,use:Surname),DUP,NOCASE
!        User_Type_Key            KEY(use:User_Type,use:Surname),DUP,NOCASE
        !
        Access:USERS.ClearKey(use:User_Code_Key)
        use:User_Code = LOC:UserCode
        SET(use:User_Code_Key, use:User_Code_Key)

        IF Access:USERS.NEXT()
            EXIT
        END !IF
        !-----------------------------------------------
        IF use:User_Code <> LOC:UserCode
            EXIT
        END !IF

        Result = True
        !-----------------------------------------------
    EXIT
SyncExchange                                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        ! xch:Ref_Number_Key           KEY(xch:Ref_Number),NOCASE,PRIMARY
        !
        Result     = False
        ExchangeOK = Result

        IF job:Exchange_Unit_Number = 0
            EXIT
        END !IF

        Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
        xch:Ref_Number = job:Exchange_Unit_Number
        SET(xch:Ref_Number_Key, xch:Ref_Number_Key)

        IF Access:EXCHANGE.NEXT()
            EXIT
        END !IF

        IF xch:Ref_Number <> job:Exchange_Unit_Number
            EXIT
        END !IF

        Result = True
        ExchangeOK = Result
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
ProgressBar_Setup                       ROUTINE
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
            recordspercycle      = 25
            recordsprocessed     = 0
            recordstoprocess     = 10 !***The Number Of Records, or at least a guess***
            percentprogress      = 0
            progress:thermometer = 0

            thiswindow.reset(1) !This may error, if so, remove this line.
            open(progresswindow)
            progresswindow{PROP:Timer} = 1
         
            ?progress:userstring{prop:text} = 'Running...'
            ?progress:pcttext{prop:text}    = '0% Completed'
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
ProgressBar_LoopPre          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***

        ?Progress:UserString{PROP:Text} = CLIP(Progress:Text)

        RecordsProcessed = 0
        RecordCount      = 0

        !RecordsToProcess = RECORDS(JOBS) !***The Number Of Records, or at least a guess***
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
        !-----------------------------------------------
    EXIT
ProgressBar_Loop             ROUTINE
    DATA
newPctText STRING(255)
    CODE
        !-----------------------------------------------
        !***Place the following code inside the loop.
        !The first line updates the progress bar,
        !   and the rest allows the user to cancel the routine.
        !This can be removed if necessary. ****
        !RecordsProcessed += 1

        Do ProgressBar_GetNextRecord2 !Necessary

        Do ProgressBar_CancelCheck
        If CancelPressed
            CLOSE(ProgressWindow)
        End!If 
        !-----------------------------------------------
        newPctText = 'Checked(' & RecordsProcessed & '), Found(' & RecordCount & ')'

        IF ?progress:pcttext{prop:text} <> newPctText
            ?progress:pcttext{prop:text} = newPctText

            Display()
        END !IF
        !***Place the following code inside the loop. The first line updates the progress bar, and the rest allows the user to cancel the routine. This can be removed if necessary. ****
        !-----------------------------------------------
    EXIT
ProgressBar_LoopPost         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        ?Progress:UserString{PROP:Text} = CLIP(Progress:Text) & ' Done(' & RecordsProcessed & '/' & RecordCount & ')'
        !-----------------------------------------------
    EXIT
ProgressBar_Finalise                        ROUTINE
        !**** End Of Loop ***
            Do ProgressBar_EndPrintRun
            close(progresswindow)
        !**** End Of Loop ***
        ! when in report procedure->LocalResponse = RequestCompleted
ProgressBar_GetNextRecord2                  routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        !?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end

    ?progress:pcttext{prop:text} = 'Checked(' & RecordsProcessed & '), Found(' & RecordCount & ')'

    Display()
 
ProgressBar_CancelCheck                     routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        BEEP(BEEP:SystemAsterisk)  ;  YIELD()

        Case Missive('Are you sure you want to cancel?','ServiceBase 3g',|
                       'mstop.jpg','\No|/Yes') 
            Of 2 ! Yes Button
                tmp:cancel = 1
                CancelPressed = True
            Of 1 ! No Button
        End ! Case Missive
    End!If cancel# = 1

ProgressBar_EndPrintRun                     routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()
XL_Setup                                                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel = Create(0, Create:OLE)
        Excel{PROP:Create} = 'Excel.Application'

        Excel{'Application.DisplayAlerts'}  = False         ! no alerts to the user (do you want to save the file?) ! MOVED 10 BDec 2001 John
        Excel{'Application.ScreenUpdating'} = excel:Visible ! False
        Excel{'Application.Visible'}        = excel:Visible ! False

        Excel{'Application.Calculation'}    = xlCalculationManual

        DO XL_GetOperatingSystem
        !-----------------------------------------------
    EXIT
XL_Finalize                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Application.Calculation'}    = xlCalculationAutomatic

        Excel{PROP:DEACTIVATE}
        !-----------------------------------------------
    EXIT
XL_AddSheet                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveWorkBook.Sheets("Sheet3").Select'}

        Excel{'ActiveWorkBook.Sheets.Add'}
        !-----------------------------------------------
    EXIT
XL_AddWorkbook               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Application.Workbooks.Add()'}
        YIELD()

        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Title")'}            = CLIP(LOC:ProgramName) & ' ' & CLIP(tra_ali:Account_Number)
        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Author")'}           = CLIP(LOC:UserName)
        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Application Name")'} = LOC:ApplicationName
        !-----------------------------------------------
        ! 4 Dec 2001 John
        ! Delete empty sheets

        Excel{'Sheets("Sheet2").Select'}
        Excel{'ActiveWindow.SelectedSheets.Delete'}

        Excel{'Sheets("Sheet1").Select'}
        !-----------------------------------------------
    EXIT
XL_SaveAllWorksheets                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOOP WHILE Excel{'WorkBooks.Count'} > 0
            Excel{'ActiveWorkBook.Save()'}
            Excel{'ActiveWorkBook.Close()'}
        END !LOOP
        !-----------------------------------------------
    EXIT
XL_DropAllWorksheets                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOOP WHILE Excel{'WorkBooks.Count'} > 0
            Excel{'ActiveWorkBook.Close(' & FALSE & ')'}
            YIELD()
        END !LOOP
        !-----------------------------------------------
    EXIT
XL_AddComment                ROUTINE
    DATA
!xlComment SIGNED
    CODE
        !-----------------------------------------------
!        xlComment = Excel{'Selection.AddComment("' & CLIP(excel:CommentText) & '")'}
        Excel{'Selection.AddComment("' & CLIP(excel:CommentText) & '")'}

!        xlComment{'Shape.IncrementLeft'} = 127.50
!        xlComment{'Shape.IncrementTop'}  =   8.25
!        xlComment{'Shape.ScaleWidth'}    =   1.76
!        xlComment{'Shape.ScaleHeight'}   =   0.69
        !-----------------------------------------------
    EXIT
XL_RowDown                   ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(1, 0).Select'}
        !-----------------------------------------------
    EXIT
XL_ColLeft                   ROUTINE
    DATA
CurrentCol LONG
    CODE
        !-----------------------------------------------
        CurrentCol = Excel{'ActiveCell.Col'}
        IF CurrentCol = 1
            EXIT
        END !IF

        Excel{'ActiveCell.Offset(0, -1).Select'}
        !-----------------------------------------------
    EXIT
XL_ColRight                  ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(0, 1).Select'}
        !-----------------------------------------------
    EXIT
XL_ColFirstSelect                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, Cells(' & Excel{'ActiveCell.Row'} & ', 1)).Select'}
        !Excel{'ActiveCell.Offset(0, -' & Excel{'ActiveCell.Column'} - 1 & ').Select'}
        !-----------------------------------------------
    EXIT
XL_ColFirst                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(0, -' & Excel{'ActiveCell.Column'} - 1 & ').Select'}
        !-----------------------------------------------
    EXIT
XL_SetLastCell                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, ActiveCell.SpecialCells(' & xlLastCell & ')).Select'}
        !-----------------------------------------------
    EXIT
XL_SetGrid                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlDiagonalDown & ').LineStyle'} = xlNone
        Excel{'Selection.Borders(' & xlDiagonalUp   & ').LineStyle'} = xlNone

        DO XL_SetBorder

        Excel{'Selection.Borders(' & xlInsideVertical & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlInsideVertical & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlInsideVertical & ').ColorIndex'} = xlAutomatic

        Excel{'Selection.Borders(' & xlInsideHorizontal & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlInsideHorizontal & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlInsideHorizontal & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorder                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_SetBorderLeft
        DO XL_SetBorderRight
        DO XL_SetBorderBottom
        DO XL_SetBorderTop
        !-----------------------------------------------
    EXIT
XL_SetBorderLeft                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeLeft & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeLeft & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeLeft & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderRight                           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeRight & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeRight & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeRight & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderBottom                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeBottom & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeBottom & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeBottom & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderTop                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeTop & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeTop & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeTop & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_HighLight                           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Interior.ColorIndex'}        = 6 ! YELLOW
        Excel{'Selection.Interior.Pattern'}           = xlSolid
        Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic
        !-----------------------------------------------
XL_SetTitle                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Interior.ColorIndex'}        = 15 ! GREY
        Excel{'Selection.Interior.Pattern'}           = xlSolid
        Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetWrapText                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.WrapText'}    = True
        !-----------------------------------------------
    EXIT
XL_SetGreyText                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Font.ColorIndex'}    = 16 ! grey
        !-----------------------------------------------
    EXIT
XL_SetBold                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Font.Bold'} = True
        !-----------------------------------------------
    EXIT
XL_FormatDate                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.NumberFormat'} = excel:DateFormat
        !-----------------------------------------------
    EXIT
XL_FormatNumber                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.NumberFormat'} = '#,##0.00'
        !-----------------------------------------------
    EXIT
XL_FormatTextBox                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.ShapeRange.IncrementLeft'} = 127.5
        Excel{'Selection.ShapeRange.IncrementTop'}  =   8.25
!        Excel{'Selection.ShapeRange.ScaleWidth'}    =  '1.76, ' & msoFalse & ', ' & msoScaleFromBottomRight
!        Excel{'Selection.ShapeRange.ScaleHeight'}   =  '0.69, ' & msoFalse & ', ' & msoScaleFromTopLeft
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentLeft              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlLeft
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentRight             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlRight
        !-----------------------------------------------
    EXIT
XL_SetColumnHeader                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_SetTitle
        DO XL_SetGrid
        DO XL_SetWrapText
        DO XL_SetBold
        !-----------------------------------------------
    EXIT
XL_SetColumn_Date                       ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = 'dd Mmm yyyy'

!        !Excel{'ActiveCell.FormulaR1C1'}  = LEFT(FORMAT(ret:Invoice_Date, @D8-))
        !-----------------------------------------------
    EXIT
XL_SetColumn_Number                     ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = '#,##0.00'
        !-----------------------------------------------
    EXIT
XL_SetColumn_Percent                    ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = '0.00%'
!        Excel{'ActiveCell.NumberFormat'} = CHR(64)       ! Text (AT) Sign
        !-----------------------------------------------
    EXIT
XL_SetColumn_Text                       ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'} = CHR(64) ! Text (AT) Sign
        !-----------------------------------------------
    EXIT
XL_DataSelectRange                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range("A10").Select'}
        Excel{'Range(Selection, ActiveCell.SpecialCells(' & xlLastCell & ')).Select'}
        !-----------------------------------------------
    EXIT
XL_DataHideDuplicates                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.AdvancedFilter Action:=' & xlFilterInPlace & ', Unique:=' & True }
        !-----------------------------------------------
    EXIT
XL_DataSortRange                            ROUTINE
    DATA
    CODE                     
        !-----------------------------------------------
        DO XL_DataSelectRange
        Excel{'ActiveWindow.ScrollColumn'} = 1
        Excel{'Selection.Sort Key1:=Range("A11"), Order1:=' & xlAscending & ', Header:=' & xlGuess & |
            ', OrderCustom:=1, MatchCase:=' & False & ', Orientation:=' & xlTopToBottom}
        !-----------------------------------------------
    EXIT
XL_DataAutoFilter                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.AutoFilter'}
        !-----------------------------------------------
    EXIT
XL_SetWorksheetLandscape                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.Orientation'}     = xlLandscape
!        Excel{'ActiveSheet.PageSetup.FitToPagesWide'} = 1
!        Excel{'ActiveSheet.PageSetup.FitToPagesTall'} = 9999
!        Excel{'ActiveSheet.PageSetup.Order'}          = xlOverThenDown
        !-----------------------------------------------
    EXIT
XL_SetWorksheetPortrait                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.Orientation'} = xlPortrait
        !-----------------------------------------------
    EXIT
XL_GetCurrentCell                               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOC:Text = Excel{'Application.ConvertFormula( "RC", ' & xlR1C1 & ', ' & xlA1 & ')'}
        !-----------------------------------------------
    EXIT

XL_MergeCells                                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlCenter
        Excel{'Selection.VerticalAlignment'}   = xlBottom
        Excel{'Selection.MergeCells'}          = True
        !-----------------------------------------------
    EXIT

XL_GetOperatingSystem                                                          ROUTINE
    DATA
OperatingSystem     STRING(50)
tmpLen              LONG
LEN_OperatingSystem LONG
    CODE
        !-----------------------------------------------
        excel:OperatingSystem = 0.0 ! Default/error value
        OperatingSystem     = Excel{'Application.OperatingSystem'}
        LEN_OperatingSystem = LEN(CLIP(OperatingSystem))

        LOOP x# = LEN_OperatingSystem TO 1 BY -1

            CASE SUB(OperatingSystem, x#, 1)
            OF '0' OROF'1' OROF '2' OROF '3' OROF '4' OROF '5' OROF '6' OROF '7' OROF '8' OROF '9' OROF '.'
                ! NULL
            ELSE
                tmpLen                = LEN_OperatingSystem-x#+1
                excel:OperatingSystem = CLIP(SUB(OperatingSystem, x#+1, tmpLen))

                EXIT
            END !CASE
        END !LOOP
        !excel:OperatingSystem = 1.0
        !-----------------------------------------------
    EXIT
!-----------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020602'&'0'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('CompletedJobsReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:EXCHANGE.Open
  Relate:JOBACC.Open
  Relate:TRADEACC_ALIAS.Open
  Relate:WEBJOB.Open
  Access:USERS.UseFile
  Access:JOBS.UseFile
  Access:SUBTRACC.UseFile
  SELF.FilesOpened = True
  BRW5.Init(?List,Queue:Browse.ViewPosition,BRW5::View:Browse,Queue:Browse,Relate:TRADEACC,SELF)
  OPEN(MainWindow)
  SELF.Opened=True
  ! ========= Set Report Version =============
  Include('..\ReportVersion.inc')
  tmp:VersionNumber = 'Version Number: ' & Clip(tmp:VersionNumber) & '5001'
  ?ReportVersion{prop:Text} = tmp:VersionNumber
      LOC:ProgramName       = 'Completed Jobs Report'                   ! Job=1622  Cust=N NOT FOUND new Job number created 17 May 2002 John
                                                                        ! SBCR0023 Folder Created Wednesday, April 17, 2002, 12:05:08 PM
      MainWindow{PROP:Text} = LOC:ProgramName
      ?Tab1{PROP:Text}      = CLIP(LOC:ProgramName) & ' Criteria'
  
      excel:Visible = False ! INTEC = ON
      debug:Active  = True
  
      LOC:StartDate = TODAY() !
      LOC:EndDate   = TODAY() !
  
      SET(defaults)
      access:defaults.next()
  
      DO GetUserName
      IF GUIMode = 1 THEN
         DOAll = 'Y'
         LocalTimeOut = 500
         MainWindow{PROP:ICONIZE} = TRUE
      END !IF
  
      LocalHeadAccount = GETINI('BOOKING','HEADACCOUNT','',CLIP(PATH()) & '\SB2KDEF.INI')
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?LOC:StartDate{Prop:Alrt,255} = MouseLeft2
  ?LOC:EndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW5.Q &= Queue:Browse
  BRW5.AddSortOrder(,tra:Account_Number_Key)
  BRW5.AddLocator(BRW5::Sort0:Locator)
  BRW5::Sort0:Locator.Init(,tra:Account_Number,1,BRW5)
  BRW5.SetFilter('((tra:RemoteRepairCentre = 1 OR tra:Account_Number = LocalHeadAccount) AND (tra:Account_Number <<> ''XXXRRC''))')
  BIND('LocalTag',LocalTag)
  BIND('LocalHeadAccount',LocalHeadAccount)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW5.AddField(LocalTag,BRW5.Q.LocalTag)
  BRW5.AddField(tra:Account_Number,BRW5.Q.tra:Account_Number)
  BRW5.AddField(tra:Company_Name,BRW5.Q.tra:Company_Name)
  BRW5.AddField(tra:RecordNumber,BRW5.Q.tra:RecordNumber)
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List{Prop:Alrt,239} = SpaceKey
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:JOBACC.Close
    Relate:TRADEACC_ALIAS.Close
    Relate:WEBJOB.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?DoAll
      IF DoAll = 'Y' THEN
         HIDE(?List)
         HIDE(?DASTAG)
         HIDE(?DASTAGALL)
         HIDE(?DASUNTAGALL)
      ELSE
          UNHIDE(?List)
          UNHIDE(?DASTAG)
          UNHIDE(?DASTAGALL)
          UNHIDE(?DASUNTAGALL)
      END !IF
    OF ?OkButton
          DO OKButtonPressed
    OF ?CancelButton
       POST(Event:CloseWindow)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020602'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020602'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020602'&'0')
      ***
    OF ?StartDateCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          LOC:StartDate = TINCALENDARStyle1(LOC:StartDate)
          Display(?LOC:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?EndDateCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          LOC:EndDate = TINCALENDARStyle1(LOC:EndDate)
          Display(?LOC:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?LOC:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?StartDateCalendar)
      CYCLE
    END
  OF ?LOC:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?EndDateCalendar)
      CYCLE
    END
  OF ?List
    CASE EVENT()
    OF EVENT:AlertKey
      IF RECORDS(BRW5) <> 0 AND DoAll <> 'Y' AND KeyCode() = MouseLeft2 THEN
         POST(Event:Accepted,?DASTAG)
      END !IF
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::6:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

GetHeadAccount PROCEDURE( IN:AccountNumber )
    CODE
        !-----------------------------------------------------------------  
        WriteDebug('GetHeadAccount(' & CLIP(IN:AccountNumber) & ')')

        haq:AccountNumber  = IN:AccountNumber
        GET(HeadAccount_Queue, +haq:AccountNumber)

        CASE ERRORCODE()
        OF 00 ! FOUND
            !
        OF 30 ! NOT Found
            ! Not in queue - ADD
            haq:AccountNumber            = IN:AccountNumber

            IF LoadTRADEACC(IN:AccountNumber)
                haq:AccountName          = tra:Company_Name
                haq:BranchIdentification = tra:BranchIdentification
                haQ:JobsBooked           = 0
                !---------------------------------------------------------
                ! 24 Oct 2002 John
                ! Neil IRC, okay.....so they have been allocated to a RRC engineer, but not worked on by ARC.
                !   Add another colum to indicate fact that orginal RRC engineer still on job.
                haQ:SiteLocation         = tra:SiteLocation ! Link to TRADEACC, use:Location = tra:SiteLocation
                !---------------------------------------------------------

!                IF tra:Invoice_Sub_Accounts = 'YES'
!                    haQ:InvoiceSubAccounts = True
!                ELSE
!                    haQ:InvoiceSubAccounts = False
!                END !IF
!
!                haQ:PartsVATCode         = tra:Parts_VAT_Code
!                haQ:LabourVATCode        = tra:Labour_VAT_Code
!                haQ:RetailVATCode        = tra:Retail_Discount_Code
!
!                haQ:PartsVATRate         = GetVATRate(tra:Parts_VAT_Code)
!                haQ:LabourVATRate        = GetVATRate(tra:Labour_VAT_Code)
!                haQ:RetailVATRate        = GetVATRate(tra:Retail_Discount_Code)
            ELSE
                haq:AccountName          = '*T'
                haq:BranchIdentification = '*T'
                haQ:JobsBooked           = 0

!                haQ:PartsVATCode         = ''
!                haQ:LabourVATCode        = ''
!                haQ:RetailVATCode        = ''
!
!                haQ:PartsVATRate         = 0.00
!                haQ:LabourVATRate        = 0.00
!                haQ:RetailVATRate        = 0.00
            END !IF

            ADD(HeadAccount_Queue, +haq:AccountNumber)
        ELSE
            CancelPressed = True
        END !IF
        !-----------------------------------------------------------------
GetSubAccount PROCEDURE( IN:AccountNumber )
    CODE
        !-----------------------------------------------------------------  
        WriteDebug('GetSubAccount(' & CLIP(IN:AccountNumber) & ')')

        saq:AccountNumber  = IN:AccountNumber
        GET(SubAccount_Queue, +saq:AccountNumber)

        CASE ERRORCODE()
        OF 00 ! FOUND
            !
        OF 30 ! NOT Found
            ! Not in queue - ADD
            saq:AccountNumber            = IN:AccountNumber

            IF LoadSUBTRACC(job:Account_Number)
                GetHeadAccount(sub:Main_Account_Number)

                saq:AccountName          = sub:Company_Name
                saQ:HeadAccountNumber    = sub:Main_Account_Number
                saQ:HeadAccountName      = haQ:AccountName
                saQ:BranchIdentification = haQ:BranchIdentification
                saQ:JobsBooked           = 0

!                If haQ:InvoiceSubAccounts = True
!                    saQ:PartsVATCode         = sub:Parts_VAT_Code
!                    saQ:LabourVATCode        = sub:Labour_VAT_Code
!                    saQ:RetailVATCode        = sub:Retail_Discount_Code
!
!                    saQ:PartsVATRate         = GetVATRate(sub:Parts_VAT_Code)
!                    saQ:LabourVATRate        = GetVATRate(sub:Labour_VAT_Code)
!                    saQ:RetailVATRate        = GetVATRate(sub:Retail_Discount_Code)
!                Else
!                    saQ:PartsVATCode         = saQ:PartsVATCode
!                    saQ:LabourVATCode        = saQ:LabourVATCode
!                    saQ:RetailVATCode        = saQ:RetailVATCode
!
!                    saQ:PartsVATRate         = haQ:PartsVATRate
!                    saQ:LabourVATRate        = haQ:LabourVATRate
!                    saQ:RetailVATRate        = haQ:RetailVATRate
!                End!If tra:use_sub_accounts = 'YES'

            ELSE
                saq:AccountName          = ''
                saq:HeadAccountNumber    = ''
                saQ:HeadAccountName      = ''
                saQ:BranchIdentification = ''
                saQ:JobsBooked           = 0

!                haQ:PartsVATCode         = ''
!                haQ:LabourVATCode        = ''
!                haQ:RetailVATCode        = ''
!
!                haQ:PartsVATRate         = 0.00
!                haQ:LabourVATRate        = 0.00
!                haQ:RetailVATRate        = 0.00
            END !IF

            ADD(SubAccount_Queue, +saq:AccountNumber)
        ELSE
            CancelPressed = True
        END !CASE
        !-----------------------------------------------------------------
GetWebJobNumber                 PROCEDURE( IN:JobNumber )! STRING
    CODE
        !-----------------------------------------------
        IF NOT LoadWEBJOB( job:Ref_Number)
            RETURN ''
        END !IF

        !-----------------------------------------------
        ! 24 Oct 2002 John, MICHALAN: This column [Franchise Job Number] to be inserted after Franchise Branch Number column.
        !                   Franchise job number only to appear ie. Take out the SB job number and branch number in
        !                       front of the Franchise Job Number eg. "12-0112" should read "12".
        !RETURN IN:JobNumber & '-' & saQ:BranchIdentification & wob:JobNumber
        RETURN wob:JobNumber
        !-----------------------------------------------
LoadSUBTRACC                PROCEDURE(IN:SubAccountNumber)! LONG ! BOOL
    CODE
        !-----------------------------------------------
        WriteDebug('LoadSUBTRACC')

        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = IN:SubAccountNumber

        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key)
            RETURN False
        END !IF

        IF sub:Account_Number <> IN:SubAccountNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadTRADEACC                    PROCEDURE(IN:HeadAccountNumber)! LONG ! BOOL
    CODE
        !-----------------------------------------------
        WriteDebug('LoadTRADEACC(' & CLIP(IN:HeadAccountNumber) & ')')

        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = IN:HeadAccountNumber

        IF Access:TRADEACC.TryFetch(tra:Account_Number_Key) <> Level:Benign
            RETURN False
        END !IF


        IF NOT tra:Account_Number = IN:HeadAccountNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadWEBJOB      PROCEDURE( IN:JobNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:WEBJOB.ClearKey(wob:RefNumberKey)
        wob:RefNumber = IN:JobNumber

        IF Access:WEBJOB.TryFetch(wob:RefNumberKey) <> Level:Benign
            RETURN False
        END !IF

        IF NOT wob:RefNumber = IN:JobNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
NumberToColumn PROCEDURE(IN:Long)! STRING
FirstCol STRING(1)
Temp     LONG
    CODE
        !-----------------------------------------------
        Temp = IN:Long - 1

        FirstCol = Temp / 26
        IF FirstCol = 0
            RETURN CHR(Temp+65)
        END !IF

        RETURN CHR(FirstCol+64) & CHR( (Temp % 26)+65 )
        !-----------------------------------------------
WriteColumn PROCEDURE( IN:String, IN:StartNewRow )
Temp STRING(255)
    CODE
        !-----------------------------------------------
        Temp = IN:String
        IF CLIP(Temp) = ''
            Temp = ''' '
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            IF IN:StartNewRow
                clip:Value = Temp
            ELSE
                clip:Value = CLIP(clip:Value) & '<09>' & Temp
            END !IF

            RETURN
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
WriteDebug      PROCEDURE( IN:Message )
    CODE
        !-----------------------------------------------
        !IF debug:Active = False
            RETURN
        !END ! IF

        debug:Count += 1

        PUTINI( CLIP(LOC:ProgramName), debug:Count, IN:Message, 'C:\DEBUG.ini')
        !-----------------------------------------------
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW5.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = tra:RecordNumber
     GET(glo:Queue2,glo:Queue2.Pointer2)
    IF ERRORCODE()
      LocalTag = ''
    ELSE
      LocalTag = 'Y'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (LocalTag = 'Y')
    SELF.Q.LocalTag_Icon = 2
  ELSE
    SELF.Q.LocalTag_Icon = 1
  END


BRW5.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW5.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW5.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW5::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW5::RecordStatus=ReturnValue
  IF BRW5::RecordStatus NOT=Record:OK THEN RETURN BRW5::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = tra:RecordNumber
     GET(glo:Queue2,glo:Queue2.Pointer2)
    EXECUTE DASBRW::6:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW5::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW5::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW5::RecordStatus
  RETURN ReturnValue

