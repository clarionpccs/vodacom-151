

   MEMBER('vodr0082.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('VODR0001.INC'),ONCE        !Local module procedure declarations
                     END


ReturnedThirdPartyDespatchReport PROCEDURE            !Generated from procedure template - Window

Local                CLASS
UpdateProgressWindow Procedure(String func:Text)
                     END
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:AllThirdParty    BYTE(1)
tmp:ThirdPartySite   STRING(30)
Progress:Thermometer BYTE
StatusQueue          QUEUE,PRE(staque)
StatusMessage        STRING(60)
                     END
tmp:VersionNumber    STRING(30)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?tmp:ThirdPartySite
trd:Company_Name       LIKE(trd:Company_Name)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB11::View:FileDropCombo VIEW(TRDPARTY)
                       PROJECT(trd:Company_Name)
                     END
QuickWindow          WINDOW('Window'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),ICON('Cellular3g.ico'),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE,IMM
                       PANEL,AT(160,64,360,300),USE(?PanelMain),FILL(0D6EAEFH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(164,84,352,44),USE(?PanelTip),FILL(0D6EAEFH)
                       GROUP('Top Tip'),AT(168,88,344,36),USE(?GroupTip),BOXED,TRN
                       END
                       STRING(@s255),AT(172,104,300,),USE(SRN:TipText),TRN
                       PANEL,AT(164,132,352,198),USE(?Panel55),FILL(09A6A7CH)
                       PROMPT('Returned Start Date'),AT(249,176),USE(?tmp:StartDate:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@d6),AT(336,176,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('Start Date'),TIP('Start Date'),UPR
                       BUTTON,AT(405,172),USE(?PopCalendar),TRN,FLAT,ICON('lookupp.jpg')
                       PROMPT('Returned End Date'),AT(249,200),USE(?tmp:EndDate:Prompt),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@d6),AT(336,200,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,010101H),MSG('End Date'),TIP('End Date'),UPR
                       CHECK('All Third Party Repairers'),AT(336,222),USE(tmp:AllThirdParty),FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('All Third Party Repairers'),TIP('All Third Party Repairers'),VALUE('1','0')
                       COMBO(@s30),AT(335,242,124,10),USE(tmp:ThirdPartySite),IMM,VSCROLL,FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                       PROMPT('Third Party Repairer'),AT(249,242),USE(?tmp:ThirdPartySite:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       BUTTON,AT(405,196),USE(?PopCalendar:2),TRN,FLAT,ICON('lookupp.jpg')
                       BUTTON,AT(476,92,36,32),USE(?ButtonHelp),TRN,FLAT,ICON('F1Help.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PROMPT('Returned Third Party Despatch Report'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(376,332),USE(?Ok),TRN,FLAT,MSG('Accept operation'),TIP('Accept Operation'),ICON('printp.jpg')
                       BUTTON,AT(444,332),USE(?Cancel),TRN,FLAT,MSG('Cancel Operation'),TIP('Cancel Operation'),ICON('cancelp.jpg')
                       PROMPT('Report Version'),AT(168,342),USE(?ReportVersion),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
E1                   Class(oiExcel)
Init                   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0),byte,proc,virtual,name('INIT@F17OIEXCEL')
Kill                   PROCEDURE (byte pUnloadCOM=1),byte,proc,virtual,name('KILL@F17OIEXCEL')
TakeEvent              PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0),virtual,name('TAKEEVENT@F17OIEXCEL')
                     End

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB11               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

!Progress Window (DBH: 22-03-2004)
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progresswindow WINDOW('Progress...'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH), |
         CENTER,IMM,ICON('cellular3g.ico'),WALLPAPER('sbback.jpg'),TILED,TIMER(1),GRAY,DOUBLE
       PANEL,AT(160,64,360,300),USE(?Panel5),FILL(0D6E7EFH)
       PANEL,AT(164,68,352,12),USE(?Panel1),FILL(09A6A7CH)
       LIST,AT(208,88,268,206),USE(?List1),VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH), |
           FORMAT('20L(2)|M'),FROM(StatusQueue),GRID(COLOR:White)
       PANEL,AT(164,84,352,246),USE(?Panel4),FILL(09A6A7CH)
       PROGRESS,USE(progress:thermometer),AT(206,314,268,12),RANGE(0,100)
       STRING(''),AT(259,300,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,080FFFFH,FONT:bold), |
           COLOR(09A6A7CH)
       STRING(''),AT(232,136,161,10),USE(?progress:pcttext),TRN,HIDE,CENTER,FONT('Arial',8,,)
       PANEL,AT(164,332,352,28),USE(?Panel2),FILL(09A6A7CH)
       PROMPT('Report Progress'),AT(168,70),USE(?WindowTitle2),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
       BUTTON,AT(444,332),USE(?ProgressCancel),TRN,FLAT,LEFT,ICON('Cancelp.jpg')
       BUTTON,AT(376,332),USE(?Button:OpenReportFolder),TRN,FLAT,HIDE,ICON('openrepp.jpg')
       BUTTON,AT(444,332),USE(?Finish),TRN,FLAT,HIDE,ICON('Finishp.jpg')
     END
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

Reporting       Routine
Data
tmp:Desktop         CString(255)
excel:FileName   CString(255)
excel:PRogramName   Cstring(255)
tmp:CurrentRow      Long()
local:ReportStartDate   Date()
local:ReportStartTime   Time()
local:Desktop           CString(255)
Code
    excel:ProgramName = 'Returned 3rd Party Report'
    SHGetSpecialFolderPath( GetDesktopWindow(), local:Desktop, 5, FALSE )
    local:Desktop = Clip(local:Desktop) & '\ServiceBase Export'

    !Does the Export Folder already Exists?
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    local:Desktop = Clip(local:Desktop) & '\' & Clip(excel:ProgramName)
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    excel:FileName = Clip(local:Desktop) & '\' & Clip(excel:ProgramName) & ' ' & Format(Today(),@d12)

    If Exists(excel:FileName & '.xls')
        Remove(excel:FileName & '.xls')
        If Error()
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('Cannot get access to the selected document:'&|
                '|' & Clip(excel:FileName) & ''&|
                '|'&|
                '|Ensure the file is not in use and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If Error()
    End !If Exists(excel:FileName)

    !Open Program Window (DBH: 10-03-2004)
    recordspercycle         = 25
    recordsprocessed        = 0
    percentprogress         = 0
    progress:thermometer    = 0
    recordstoprocess        = 50
    Open(ProgressWindow)

    ?progress:userstring{prop:text} = 'Running...'
    ?progress:pcttext{prop:text} = '0% Completed'

    local:ReportStartDate = Today()
    local:ReportStartTime = Clock()

    local.UpdateProgressWindow('Report Started: ' & Format(local:ReportStartDate,@d6b) & ' ' & Format(local:ReportStartTime,@t1b))
    local.UpdateProgressWindow('')

    ! _____________________________________________________________________

    If E1.Init(0,0) = False
        Case Missive('An error has occured creating your excel document. '&|
          '|Please try again.','ServiceBase 3g',|
                       'mstop.jpg','/OK') 
            Of 1 ! OK Button
        End ! Case Missive
        Exit
    End ! If E1.Init(0,0) = False

    E1.NewWorkbook()
    E1.RenameWorksheet('Returned Third Party Report')
    E1.SetCellFontName('Tahoma','A1','R1')
    E1.SetCellFontSize(8,'A1','R1')
    E1.WriteToCell('Username'       ,'A1')
    E1.WriteToCell('WaybillNumber'  ,'B1')
    E1.WriteToCell('Date Received'  ,'C1')
    E1.WriteToCell('Batch Number',  'D1')
    E1.WriteToCell('Supplier Name'  ,'E1')
    E1.WriteToCell('Supplier Account No'    ,'F1')
    E1.WriteToCell('SB Job Number'  ,'G1')
    E1.WriteToCell('Repair Type'    ,'H1')
    E1.WriteToCell('Manufacturer'   ,'I1')
    E1.WriteToCell('Model Number'   ,'J1')
    E1.WriteToCell('I.M.E.I. No'    ,'K1')
    E1.WriteToCell('M.S.N. No'      ,'L1')
    E1.WriteToCell('3rd Party Invoice No'   ,'M1')
    E1.WriteToCell('3rd Party Invoice Date' ,'N1')
    E1.WriteToCell('Invoice Amount' ,'O1')
    E1.WriteToCell('V.A.T.'         ,'P1')
    E1.WriteToCell('Total'          ,'Q1')
    E1.WriteToCell('Rejected Amount','R1')
    E1.WriteToCell('Rejected Reason','S1')
    E1.SetCellFontStyle('Bold','A1','S1')
    E1.SetCellBackgroundColor(color:silver,'A1','S1')

    local.UpdateProgressWindow('Building Report.....')
    local.UpdateProgressWindow('========')
    local.UpdateProgressWindow('')

    tmp:CurrentRow = 2

    Access:TRDBATCH.ClearKey(trb:ReturnDateKey)
    trb:DateReturn = tmp:StartDate
    Set(trb:ReturnDateKey,trb:ReturnDateKey)
    Loop
        If Access:TRDBATCH.NEXT()
           Break
        End !If
        If trb:DateReturn > tmp:EndDate       |
            Then Break.  ! End If
        Do GetNextRecord2
        Do CancelCheck
        If tmp:Cancel
            Break
        End !If tmp:Cancel

        ! Start - Only include the selected Third Party Site - TrkBs: 5110 (DBH: 23-05-2005)
        If tmp:AllThirdParty <> True
            If trb:Company_Name <> tmp:ThirdPartySite
                Cycle
            End !If trb:Company_Name <> tmp:ThirdPartySite
        End ! If tmp:AllThirdParty <> True
        ! End   - Only include the selected Third Party Site - TrkBs: 5110 (DBH: 23-05-2005)

        Access:JOBS.Clearkey(job:Ref_Number_Key)
        job:Ref_Number  = trb:Ref_Number
        If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
            ! Found

        Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
            ! Error
        End ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign

        ! Inserting (DBH 17/02/2006) #7190 - Get the jobse file
        Access:JOBSE.Clearkey(jobe:RefNumberKey)
        jobe:RefNumber  = job:Ref_Number
        If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            ! Found

        Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            ! Error
        End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        ! End (DBH 17/02/2006) #7190

        Access:TRDPARTY.ClearKey(trd:Company_Name_Key)
        trd:Company_Name = trb:Company_Name
        If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
            !Found

        Else !If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign
            !Error
        End !If Access:TRDPARTY.TryFetch(trd:Company_Name_Key) = Level:Benign

        E1.WriteToCell(trb:ReturnUser               ,'A' & tmp:CurrentRow)
        E1.WriteToCell(trb:ReturnWaybillNo          ,'B' & tmp:CurrentRow)
        E1.WriteToCell(Format(trb:DateReturn,@d17)  ,'C' & tmp:CurrentRow)
        ! Display the Oracle PO Number - TrkBs: 6264 (DBH: 09-09-2005)
        If trb:PurchaseOrderNumber > 0
            E1.WriteToCell('SS3' & Format(trb:PurchaseOrderNumber,@n07) & '/01' ,'D' & tmp:CurrentRow)
        Else ! If trb:PurchaseOrderNumber > 0
            E1.WriteToCell('' ,'D' & tmp:CurrentRow)
        End ! If trb:PurchaseOrderNumber > 0

        E1.WriteToCell(trb:Company_Name             ,'E' & tmp:CurrentRow)
        E1.WriteToCell(trd:Account_Number           ,'F' & tmp:CurrentRow)
        E1.WriteToCell(trb:Ref_Number               ,'G' & tmp:CurrentRow)
        E1.WriteToCell(trb:ReturnRepairType         ,'H' & tmp:CurrentRow)
        E1.WriteToCell(job:Manufacturer             ,'I' & tmp:CurrentRow)
        E1.WriteToCell(job:Model_Number             ,'J' & tmp:CurrentRow)
        E1.WriteToCell(trb:ESN                      ,'K' & tmp:CurrentRow)
        E1.WriteToCell(trb:MSN                      ,'L' & tmp:CurrentRow)
        E1.WriteToCell(trb:ThirdPartyInvoiceNo      ,'M' & tmp:CurrentRow)
        E1.WriteToCell(Format(trb:ThirdPartyInvoiceDate,@d17b)       ,'N' & tmp:CurrentRow)
        E1.WriteToCell(Format(trb:ThirdPartyInvoiceCharge,@n_14.2)  ,'O' & tmp:CurrentRow)
        E1.WriteToCell(Format(trb:ThirdPartyVAT,@n_14.2)            ,'P' & tmp:CurrentRow)
        E1.WriteToCell(Format(trb:ThirdPartyInvoiceCharge + trb:ThirdPartyVAT,@n_14.2)  ,'Q' & tmp:CurrentRow)

        E1.WriteToCell(Format(trb:ReturnRejectedAmount,@n_14.2)     ,'R' & tmp:CurrentRow)
        E1.WriteToCell(Clip(trb:ReturnRejectedReason)               ,'S' & tmp:CurrentRow)

        tmp:CurrentRow += 1

        Found# = False
        Loop x# = 1 To Records(StatusQueue)
            Get(StatusQueue,x#)
            If Instring(Clip(trb:Company_Name),Clip(staque:StatusMessage),1,1)
                CurrentCount# = Sub(staque:StatusMessage,Len(Clip(trb:Company_Name)) + 3,5)
                CurrentCount# += 1
                staque:StatusMessage = Clip(trb:Company_Name) & ': ' & CurrentCount#
                Put(StatusQueue)
                Found# = True
                Break
            End ! If Instring(trb:Company_Name,staque:StatusMessage,1,1)
        End ! Loop x# = 1 To Records(StatusQueue)
        If Found# = True
        Else ! Found# = True
            staque:StatusMessage = Clip(trb:Company_Name) & ': 1'
            Add(StatusQueue)
        End ! Found# = True
        Select(?List1,Records(StatusQueue))
        Display()


    End !Loop
    E1.SetCellFontName('Tahoma','A1','S' & tmp:CurrentRow)
    E1.SetCellFontSize(8,'A1','S' & tmp:CurrentRow)
    x# = E1.SetCellAlignment(oix:WrapText,True,'R1','S' & tmp:CurrentRow)
    E1.SetCellNumberFormat(oix:NumberFormatNumber,,0,,'K2','L' & tmp:CurrentRow)
    E1.SetCellNumberFormat(oix:NumberFormatGeneral,,,,'M2','M' & tmp:CurrentRow)
    E1.SetCellNumberFormat(oix:NumberFormatNumber,,2,,'O2','R' & tmp:CurrentRow)
    E1.SetColumnWidth('A','R')
    E1.SetColumnWidth('S','S','60')
    E1.SetRowHeight('1',tmp:CurrentRow)
    E1.SelectCells('A2')
    E1.FreezePanes()

    E1.SaveAs(excel:FileName)
    E1.CloseWorkbook(2)
    If tmp:Cancel = 0 Or tmp:Cancel = 2
        local.UpdateProgressWindow('================')
        local.UpdateProgressWindow('Report Finished: ' & Format(Today(),@d6b) & ' ' & Format(Clock(),@t1b))
    Else!If tmp:Cancel = False
        staque:StatusMessage = '=========='
        Add(StatusQueue)
        Select(?List1,Records(StatusQueue))

        staque:StatusMessage = 'Report CANCELLED: ' & Format(Today(),@d6b) & ' ' & Format(Clock(),@t1b)
        Add(StatusQueue)
        Select(?List1,Records(StatusQueue))

    End !If tmp:Cancel = False

    BHReturnDaysHoursMins(BHTimeDifference24Hr(local:ReportStartDate,Today(),local:ReportStartTime,Clock()),Days#,Hours#,Mins#)

    local.UpdateProgressWindow('Time To Finish: ' & Days# & ' Dys, ' & Format(Hours#,@n02) & ':' & Format(Mins#,@n02))
    Add(StatusQueue)
    Select(?List1,Records(StatusQueue))

    Do EndPrintRun
    ?progress:userstring{prop:text} = 'Finished...'

    Display()
    ?ProgressCancel{prop:Hide} = 1
    ?Finish{prop:Hide} = 0
    If Command('/SCHEDULE')
        If Access:REPSCHLG.PrimeRecord() = Level:Benign
            rlg:REPSCHEDRecordNumber = rpd:RecordNumber
            rlg:Information = 'Report Name: ' & Clip(rpd:ReportName) & ' - ' & Clip(rpd:ReportCriteriaType) & |
                              '<13,10>Report Finished'
            If Access:REPSCHLG.TryInsert() = Level:Benign
                !Insert
            Else ! If Access:REPSCHLG.TryInsert() = Level:Benign
                Access:REPSCHLG.CancelAutoInc()
            End ! If Access:REPSCHLG.TryInsert() = Level:Benign
        End ! If Access.REPSCHLG.PrimeRecord() = Level:Benign
    ELse
        ?Button:OpenReportFolder{Prop:Hide} = 0
        Accept
            Case Field()
                Of ?Finish
                    Case Event()
                        Of Event:Accepted
                            Break

                    End !Case Event()
                Of ?Button:OpenReportFolder
                    Case Event()
                    Of Event:Accepted
                        RUN('EXPLORER.EXE ' & Clip(local:Desktop))
                    End ! Case Event()
            End !Case Field()
        End !Accept
    End
    Close(ProgressWindow)
    Post(Event:CloseWindow)


getnextrecord2      routine !Progress Window Routines (DBH: 22-03-2004)
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress >= 100
        recordsprocessed        = 0
        percentprogress         = 0
        progress:thermometer    = 0
        recordsthiscycle        = 0
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    !0{prop:Text} = ?progress:pcttext{prop:text}
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                Yield()
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case Missive('Do you want to finish off building the excel document with the data you have compiled so far, or just quit now?','ServiceBase 3g',|
                       'mquest.jpg','\Cancel|Quit|/Finish') 
            Of 3 ! Finish Button
                tmp:Cancel = 2
            Of 2 ! Quit Button
                tmp:Cancel = 1
            Of 1 ! Cancel Button
        End ! Case Missive
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    display()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020632'&'0'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ReturnedThirdPartyDespatchReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Ok,RequestCancelled)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:JOBS.Open
  Relate:REPSCHCR.Open
  Access:TRDBATCH.UseFile
  Access:USERS.UseFile
  Access:JOBSE.UseFile
  Access:REPSCHED.UseFile
  Access:REPSCHLG.UseFile
  SELF.FilesOpened = True
  tmp:StartDate = Date(Month(Today()),1,Year(Today()))
  tmp:EndDate = Today()
  OPEN(QuickWindow)
  SELF.Opened=True
  !============== Version Number =====================
  Include('..\ReportVersion.inc')
  tmp:VersionNumber = 'Version Number: ' & Clip(tmp:VersionNumber) & '5004'
  ?ReportVersion{prop:Text} = tmp:VersionNumber
  Bryan.CompFieldColour()
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  IF ?tmp:AllThirdParty{Prop:Checked} = True
    DISABLE(?tmp:ThirdPartySite)
  END
  IF ?tmp:AllThirdParty{Prop:Checked} = False
    ENABLE(?tmp:ThirdPartySite)
  END
  FDCB11.Init(tmp:ThirdPartySite,?tmp:ThirdPartySite,Queue:FileDropCombo.ViewPosition,FDCB11::View:FileDropCombo,Queue:FileDropCombo,Relate:TRDPARTY,ThisWindow,GlobalErrors,0,1,0)
  FDCB11.Q &= Queue:FileDropCombo
  FDCB11.AddSortOrder()
  FDCB11.AddField(trd:Company_Name,FDCB11.Q.trd:Company_Name)
  ThisWindow.AddItem(FDCB11.WindowComponent)
  FDCB11.DefaultFill = 0
  SELF.SetAlerts()
  ! Inserting (DBH 31/08/2007) # 9125 - Is this an automatic report?
  If Command('/SCHEDULE')
      x# = Instring('%',Command(),1,1)
      Access:REPSCHED.ClearKey(rpd:RecordNumberKey)
      rpd:RecordNumber = Clip(Sub(Command(),x# + 1,20))
      If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
          !Found
          Access:REPSCHCR.ClearKey(rpc:ReportCriteriaKey)
          rpc:ReportName = rpd:ReportName
          rpc:ReportCriteriaType = rpd:ReportCriteriaType
          If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
              !Found
              tmp:AllThirdParty = rpc:AllThirdPartySites
              tmp:ThirdPartySite = rpc:ThirdPartySite
  
              Case rpc:DateRangeType
              Of 1 ! Today Only
                  tmp:StartDate = Today()
                  tmp:EndDate = Today()
              Of 2 ! 1st Of Month
                  tmp:StartDate = Date(Month(Today()),1,Year(Today()))
                  tmp:EndDate = Today()
              Of 3 ! Whole Month
  ! Changing (DBH 31/01/2008) # 9711 - Allow for change of year
  !                tmp:StartDate = Date(Month(Today()) - 1, 1, Year(Today()))
  !                tmp:EndDate = Date(Month(Today()),1,Year(Today())) - 1
  ! to (DBH 31/01/2008) # 9711
                  tmp:EndDate = Date(Month(Today()),1,Year(Today())) - 1
                  tmp:StartDate = Date(Month(tmp:EndDate),1,Year(tmp:EndDate))
  ! End (DBH 31/01/2008) #9711
              End ! Case rpc:DateRangeType
  
              Do Reporting
              Post(Event:CloseWindow)
          Else ! If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
              !Error
          End ! If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
  
      Else ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
          !Error
      End ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
  
  End ! If Command('/SCHEDULE')
  ! End (DBH 31/08/2007) #9125
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBS.Close
    Relate:REPSCHCR.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tmp:AllThirdParty
      IF ?tmp:AllThirdParty{Prop:Checked} = True
        DISABLE(?tmp:ThirdPartySite)
      END
      IF ?tmp:AllThirdParty{Prop:Checked} = False
        ENABLE(?tmp:ThirdPartySite)
      END
      ThisWindow.Reset
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020632'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020632'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020632'&'0')
      ***
    OF ?Ok
      ThisWindow.Update
      Do Reporting
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeEvent()
    E1.TakeEvent ('', '')
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:CloseWindow
        E1.Kill()
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Local.UpdateProgressWindow      Procedure(String    func:Text)
Code
    staque:StatusMessage = Clip(func:Text)
    Add(StatusQueue)
    Select(?List1,Records(StatusQueue))
    Display()
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
!---------------------------------------------------------------------------------
E1.Init   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0)
ReturnValue   byte
  CODE
  ReturnValue = PARENT.Init (pStartVisible,pEnableEvents)
  self.TakeSnapShotOfWindowPos()
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.Kill   PROCEDURE (byte pUnloadCOM=1)
ReturnValue   byte
  CODE
  self.RestoreSnapShotOfWindowPos()
  ReturnValue = PARENT.Kill (pUnloadCOM)
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.TakeEvent   PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0)
  CODE
  PARENT.TakeEvent (pEventString1,pEventString2,pEventNumber,pEventType,pEventStatus)
  if pEventType = 0  ! Generated by CapeSoft Office Inside
    case event()
      of event:accepted
        case field()
      end
    end
  end

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

