

   MEMBER('repriceutility.clw')                       ! This is a MEMBER module

                     MAP
                       INCLUDE('REPRI001.INC'),ONCE        !Local module procedure declarations
                     END


Main                 PROCEDURE                        ! Declare Procedure
save_job_id          USHORT,AUTO
tmp:Path             STRING(255)
tmp:ExclusionFile    STRING(255),STATIC
ExclusionFile    File,Driver('BASIC'),Pre(excfil),Name(tmp:ExclusionFile),Create,Bindable,Thread
Record                  Record
JobNumber                   Long
                        End
                    End
Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

  CODE
   Relate:JOBS.Open
   Relate:WEBJOB.Open
   Relate:JOBSE.Open
   Relate:TRADEACC.Open
   Relate:SUBTRACC.Open
   Relate:SUBCHRGE.Open
   Relate:TRACHRGE.Open
   Relate:STDCHRGE.Open
   Relate:REPTYDEF.Open
    tmp:Path = Clip(GETINI('REPRICE','DefaultPath',,CLIP(PATH())&'\SB2KDEF.INI'))
    If tmp:Path = ''
        Case MessageEx('This appears to be the first time you have run the Reprice Routine. You need to setup a default path to save the Excluded Jobs Export File to.'&|
          '<13,10>'&|
          '<13,10>Please select a path now.','ServiceBase 2000',|
                       'Styles\idea.ico','|&OK|&Cancel',2,2,'',,'Arial',8,0,700,CHARSET:ANSI,8421631,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
            Of 1 ! &OK Button
                filedialog ('Choose Directory',tmp:Path,'All Directories|*.*', |
                            file:save+file:keepdir + file:noerror + file:longname + file:directory)
                tmp:Path = upper(tmp:Path)
                PUTINI('REPRICE','DefaultPath',tmp:Path,CLIP(PATH()) & '\SB2KDEF.INI')
            Of 2 ! &Cancel Button
        End!Case MessageEx
    End !If tmp:Path = ''

    If Sub(Clip(tmp:Path),-1,1) <> '\'
        tmp:Path = Clip(tmp:Path) & '\'
    End !If Sub(Clip(tmp:Path),-1,1) <> '\'
    tmp:ExclusionFile = Clip(tmp:Path) & 'Exclusions ' & Format(Today(),@d12) & Format(Clock(),@t2) & '.csv'

    Create(ExclusionFile)
    Open(ExclusionFile)
    If Error()
        Stop(Error())
    Else !If Error()
    
        Count# = 0
        CountExclusions# = 0

        Prog.ProgressSetup(Records(JOBS))
        Save_job_ID = Access:JOBS.SaveFile()
        Access:JOBS.ClearKey(job:Date_Booked_Key)
        job:date_booked = Today() - 9
        Set(job:Date_Booked_Key,job:Date_Booked_Key)
        Loop
            If Access:JOBS.NEXT()
               Break
            End !If
            If job:date_booked > Today()       |
                Then Break.  ! End If
           Count# += 1
           

           Prog.ProgressText('Updated: ' & Count# & '  Excluded: ' & CountExclusions#)
           If Prog.InsideLoop()
               Break
           End !If Prog.InsideLoop()

           If JobInUse(job:Ref_Number,0)
               Clear(excfil:Record)
               excfil:JobNumber    = job:Ref_Number
               Add(ExclusionFile)
               CountExclusions# += 1
               Cycle
           End !If JobInUse(job:Ref_Number,0)
           Access:MANUFACT.Clearkey(man:Manufacturer_Key)
           man:Manufacturer    = job:Manufacturer
           If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
               !Found

           Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
               !Error
           End !If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
           Access:JOBSE.Clearkey(jobe:RefNumberKey)
           jobe:RefNumber  = job:Ref_Number
           If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
               !Found

           Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
               !Error
           End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

           Access:WEBJOB.Clearkey(wob:RefNumberKey)
           wob:RefNumber   = job:Ref_Number
           If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
               !Found

           Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
               !Error
           End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign

            !Add new prototype - L945 (DBH: 04-09-2003)
           JobPricingRoutine(0)

           job:EDI = PendingJob(job:Manufacturer)

           If job:Warranty_job = 'YES'
               If jobe:WarrantyClaimStatus = '' and job:EDI <> 'XXX'
                   jobe:WarrantyClaimStatus = 'PENDING'
               End !If jobe:WarrantyStatus = ''
               If job:EDI = 'XXX'
                   jobe:WarrantyCLaimStatus = ''
               End !If job:EDI = 'XXX'
           Else
               jobe:WarrantyClaimStatus = ''
           End !If job:Warranty_job = 'YES'

           Access:JOBSE.TryUpdate()
           Access:JOBS.TryUpdate()

       end !loop
       access:jobs.restorefile(save_job_id)
       setcursor()
       Close(ExclusionFile)
       Prog.ProgressFinish()
    End !Else !If Error()

   Relate:JOBS.Close
   Relate:WEBJOB.Close
   Relate:JOBSE.Close
   Relate:TRADEACC.Close
   Relate:SUBTRACC.Close
   Relate:SUBCHRGE.Close
   Relate:TRACHRGE.Close
   Relate:STDCHRGE.Close
   Relate:REPTYDEF.Close
Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Source
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Beep(Beep:SystemQuestion)  ;  Yield()
        Case Message('Are you sure you want to cancel?', |
                'Cancel Pressed', Icon:Question, |
                 Button:Yes+Button:No, Button:No, 0)
        Of Button:Yes
            return 1
        Of Button:No
        End !CASE
    End!If cancel# = 1

    return 0
