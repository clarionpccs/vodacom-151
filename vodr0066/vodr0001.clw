

   MEMBER('vodr0066.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('VODR0001.INC'),ONCE        !Local module procedure declarations
                     END


GoodsReceivedFranchisesReport PROCEDURE               !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::7:TAGFLAG          BYTE(0)
DASBRW::7:TAGMOUSE         BYTE(0)
DASBRW::7:TAGDISPSTATUS    BYTE(0)
DASBRW::7:QUEUE           QUEUE
AccountNumberValue            LIKE(glo:AccountNumberValue)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
tmp:VersionNumber    STRING(30)
Automatic            BYTE
Parameter_Group      GROUP,PRE()
LOC:EndDate          DATE
LOC:StartDate        DATE
                     END
Local_Group          GROUP,PRE(LOC)
Account_Name         STRING(100)
AccountNumber        STRING(15)
ApplicationName      STRING('ServiceBase')
CompanyName          STRING(30)
Courier              STRING(30)
DayLeawayCount       LONG
DesktopPath          STRING(255)
FileName             STRING(255)
JobNumber            LONG
JobsExtendedInSync   BYTE(false)
HeadAccount          STRING(15)
MaxTabNumber         BYTE(2)
PartNumber           STRING(30)
PartType             STRING(10)
Path                 STRING(255)
ProgramName          STRING(100)
SalesType            STRING(10)
SectionName          STRING(100)
TabNumber            BYTE(1)
Text                 STRING(255)
UserCode             STRING(3)
UserName             STRING(100)
Version              STRING('3.1.0000')
                     END
Misc_Group           GROUP,PRE()
Result               BYTE
CountOrders          LONG
CountLines           LONG
CountParts           LONG
CountDelivered       LONG
                     END
DASTAG_Group         GROUP,PRE()
AccountValue         STRING(15)
AccountTag           STRING(1)
AccountCount         LONG
AccountChange        BYTE
                     END
Progress_Group       GROUP,PRE()
RecordCount          LONG
Progress:Text        STRING(100)
                     END
AccountQueue         QUEUE,PRE(aq)
AccountNumber        STRING(15),NAME('aq:AccountNumber')
AccountName          STRING(30)
NumberOfOrders       LONG
NumberOfLines        LONG
NumberOfParts        LONG
SparesSalesValue     REAL
SparesCostValue      REAL
AccessorySalesValue  REAL
AccessoryCostValue   REAL
AccessoryExchangeValue REAL
SparesBackOrderValue REAL
AccessoryBackOrderValue REAL
AccessoryExchangeBackOrderValue REAL
UseEuro              STRING(3),NAME('aq:UseEuro')
                     END
Tally                GROUP,PRE(tally)
Quantity             LONG
QuantityInvoiced     LONG
QuantityBackOrdered  LONG
QuantityOrder        LONG
QuantityAccessory    LONG
QuantitySpareParts   LONG
ValueOfAccessories   DECIMAL(7,2),DIM(2)
ValueOfSpares        DECIMAL(7,2),DIM(3)
                     END
TempGroup            GROUP,PRE(tmp)
NumberOfParts        LONG
SparesSalesValue     REAL
SparesCostValue      REAL
AccessorySalesValue  REAL
AccessoryCostValue   REAL
AccessoryExchangeValue REAL
SparesBackorderValue REAL
AccessoryBackorderValue REAL
AccessoryExchangeBackorderValue REAL
                     END
Excel_Group          GROUP,PRE()
Excel                CSTRING(20)
excel:ColumnName     STRING(50)
excel:ColumnWidth    REAL
excel:CommentText    STRING(255)
excel:DateFormat     STRING('dd mmm yyyy')
excel:OperatingSystem REAL
excel:Visible        BYTE
                     END
Clipboard_Group      GROUP,PRE(clip)
OriginalValue        STRING(255)
Saved                BYTE
Value                CSTRING(1024)
                     END
HeaderQueue          QUEUE,PRE(head)
ColumnName           STRING(50)
ColumnWidth          REAL
NumberFormat         STRING(20)
                     END
Sheet_Group          GROUP,PRE(sheet)
TempLastCol          STRING(1)
HeadLastCol          STRING('G')
DataLastCol          STRING('O')
HeadSummaryRow       LONG(9)
DataSectionRow       LONG(9)
DataHeaderRow        LONG(11)
                     END
AccountNumber_Group  GROUP,PRE()
AccountNumberValue2  STRING(15)
AccountNumberTag     STRING(1)
AccountNumberCount   LONG
AccountNumberChange  BYTE
                     END
Debug_Group          GROUP,PRE(debug)
Active               LONG
Count                LONG
                     END
SupplierQueue        QUEUE,PRE(supQ)
AccountNumber        STRING(15)
Part_Number          STRING(30)
Location             STRING(30)
CompanyName          STRING(30)
Description          STRING(30)
Quantity             LONG
Amount               REAL
                     END
Location_Queue       QUEUE,PRE(locQ)
Location             STRING(30)
                     END
BRW6::View:Browse    VIEW(TRADEACC)
                       PROJECT(tra:Account_Number)
                       PROJECT(tra:SiteLocation)
                       PROJECT(tra:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
AccountNumberTag       LIKE(AccountNumberTag)         !List box control field - type derived from local data
AccountNumberTag_Icon  LONG                           !Entry's icon ID
tra:Account_Number     LIKE(tra:Account_Number)       !List box control field - type derived from field
tra:SiteLocation       LIKE(tra:SiteLocation)         !List box control field - type derived from field
tra:RecordNumber       LIKE(tra:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
MainWindow           WINDOW('Report'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(60,38,560,360),USE(?PanelMain),FILL(0D6EAEFH)
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(64,56,552,44),USE(?PanelTip),FILL(0D6EAEFH)
                       GROUP('Top Tip'),AT(68,60,544,36),USE(?GroupTip),BOXED,TRN
                       END
                       STRING(@s255),AT(72,76,496,),USE(SRN:TipText),TRN
                       BUTTON,AT(576,64,36,32),USE(?ButtonHelp),TRN,FLAT,ICON('F1Help.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Goods Received Franchises Report Criteria'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,104,552,258),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Criteria'),USE(?Tab1)
                           STRING('GRN Date From'),AT(257,114,72,8),USE(?InvoicedDateFrom),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@D6),AT(333,114,64,10),USE(LOC:StartDate),IMM,FONT(,,,FONT:bold),COLOR(COLOR:White),TIP('Enter the earliest Order date'),REQ
                           BUTTON,AT(401,110),USE(?StartPopCalendar),IMM,SKIP,FLAT,LEFT,ICON('lookupp.jpg')
                           STRING('GRN Date To'),AT(257,138,68,8),USE(?String2:2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@D6),AT(333,136,64,10),USE(LOC:EndDate),IMM,FONT(,,,FONT:bold),COLOR(COLOR:White),TIP('Enter the latest Order date'),REQ
                           BUTTON,AT(401,132),USE(?EndPopCalendar),IMM,SKIP,FLAT,LEFT,ICON('lookupp.jpg')
                           BUTTON,AT(468,228),USE(?DASTAG),FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(468,260),USE(?DASTAGAll),FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(468,292),USE(?DASUNTAGALL),FLAT,LEFT,ICON('untagalp.jpg')
                           BUTTON('&Rev tags'),AT(352,244,56,16),USE(?DASREVTAG),HIDE,LEFT
                           BUTTON('sho&W tags'),AT(352,264,56,16),USE(?DASSHOWTAG),HIDE
                           LIST,AT(212,170,248,188),USE(?List),IMM,SCROLL,VSCROLL,COLOR(COLOR:White),MSG('Browsing Records'),FORMAT('11L(2)I@s1@66L(2)|M~Account~@s15@120L(2)|M~Location~@s30@'),FROM(Queue:Browse)
                           CHECK('Show Excel'),AT(404,154,56,12),USE(excel:Visible),LEFT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),TIP('Tick To Show Excel During Report Creation.<13,10>Only Tick if you are having problems' &|
   ' <13,10>with Excel reports.'),VALUE('1','0')
                         END
                         TAB('Repair Categories'),USE(?Tab2)
                         END
                       END
                       BUTTON,AT(480,366),USE(?OkButton),FLAT,LEFT,TIP('Click to print report'),ICON('printp.jpg'),DEFAULT
                       BUTTON,AT(548,366),USE(?CancelButton),FLAT,LEFT,TIP('Click to close this form'),ICON('cancelp.jpg')
                       PROMPT('Report Version'),AT(68,376),USE(?ReportVersion),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRWTradeAcc          CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW6::Sort0:Locator  StepLocatorClass                 !Default Locator
    MAP
DateToString PROCEDURE( DATE ), STRING
FillLocation         PROCEDURE(), LONG ! BOOL
GetHeadAccount      PROCEDURE(), STRING
GetLocation         PROCEDURE( STRING ), LONG ! BOOL
LoadGRNOTES             PROCEDURE( DATE, DATE, *LONG ), LONG ! BOOL
LoadORDERS      PROCEDURE( LONG ), LONG ! BOOL
LoadORDPARTS        PROCEDURE( LONG, DATE, *LONG ), LONG ! BOOL


LocalUseEuro            Procedure(String    func:AccountNumber),Byte
NumberToColumn PROCEDURE( LONG ), STRING
UpdateSupplierQueue         PROCEDURE( STRING, STRING, STRING )
WriteColumn PROCEDURE( STRING, LONG=False )
WriteDebug      PROCEDURE( STRING )
    END !MAP
! Dimensions for the Accumulators(3, 3, 3)

OrderDimension     EQUATE(1)
BackorderDimension EQUATE(2)
InvoiceDimension   EQUATE(3)

PartsDimension     EQUATE(1)
AccessoryDimension EQUATE(2)
TotalDimension     EQUATE(3)

CostDimension      EQUATE(1)
TradeDimension     EQUATE(2)
RetailDimension    EQUATE(3)

!---   ProgressBar Declarations   --------------------------------------------------
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte
 
progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY,DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
!-----------------------------------------------------------------------------------
!---   Excel EQUATES   -------------------------------------------------------------
xlCalculationManual    EQUATE(0FFFFEFD9h) ! XlCalculation
xlCalculationAutomatic EQUATE(0FFFFEFF7h) ! XlCalculation
!----------------------------------------------------

!----------------------------------------------------
xlNone       EQUATE(0FFFFEFD2h)
xlContinuous EQUATE(        1 ) ! xlFillStyle
xlThin       EQUATE(        2 ) ! XlBorderWeight
!----------------------------------------------------

!----------------------------------------------------
xlAutomatic   EQUATE(0FFFFEFF7h) ! Constants.
xlSolid       EQUATE(        1 ) ! Constants.
xlLeft        EQUATE(0FFFFEFDDh) ! Constants.
xlRight       EQUATE(0FFFFEFC8h) ! Constants.
xlCenter      EQUATE(0FFFFEFF4h) ! Constants.
xlLastCell    EQUATE(       11 ) ! Constants.
xlTop         EQUATE(0FFFFEFC0h) ! Constants.
xlBottom      EQUATE(0FFFFEFF5h) ! Constants.

xlTopToBottom EQUATE(        1 ) ! Constants.
!----------------------------------------------------

!----------------------------------------------------
xlDiagonalDown     EQUATE( 5) ! XlBordersIndex
xlDiagonalUp       EQUATE( 6) ! XlBordersIndex
xlEdgeLeft         EQUATE( 7) ! XlBordersIndex
xlEdgeTop          EQUATE( 8) ! XlBordersIndex
xlEdgeBottom       EQUATE( 9) ! XlBordersIndex
xlEdgeRight        EQUATE(10) ! XlBordersIndex
xlInsideVertical   EQUATE(11) ! XlBordersIndex
xlInsideHorizontal EQUATE(12) ! XlBordersIndex
!----------------------------------------------------

!----------------------------------------------------
xlA1         EQUATE(        1 ) ! XlReferenceStyle
xlR1C1       EQUATE(0FFFFEFCAh) ! XlReferenceStyle
!----------------------------------------------------

!----------------------------------------------------
xlFilterCopy    EQUATE(2) ! XlFilterAction
xlFilterInPlace EQUATE(1) ! XlFilterAction
!----------------------------------------------------

!----------------------------------------------------
xlGuess EQUATE(0) ! XlYesNoGuess
xlYes   EQUATE(1) ! XlYesNoGuess
xlNo    EQUATE(2) ! XlYesNoGuess
!----------------------------------------------------

!----------------------------------------------------
xlAscending  EQUATE(1) ! XlSortOrder
xlDescending EQUATE(2) ! XlSortOrder
!----------------------------------------------------

!----------------------------------------------------
xlLandscape EQUATE(2) ! XlPageOrientation
xlPortrait  EQUATE(1) ! XlPageOrientation
!----------------------------------------------------

!----------------------------------------------------
xlDownThenOver EQUATE( 1 ) ! XlOrder
xlOverThenDown EQUATE( 2 ) ! XlOrder
!-----------------------------------------------------------------------------------
!! Excel EQUATES
!
!!----------------------------------------------------
!xlCalculationManual    EQUATE(0FFFFEFD9h) ! XlCalculation
!xlCalculationAutomatic EQUATE(0FFFFEFF7h) ! XlCalculation
!!----------------------------------------------------
!
!!----------------------------------------------------
!xlNone       EQUATE(0FFFFEFD2h)
!xlContinuous EQUATE(        1 ) ! xlFillStyle
!xlThin       EQUATE(        2 ) ! XlBorderWeight
!!----------------------------------------------------
!
!!----------------------------------------------------
!xlAutomatic  EQUATE(0FFFFEFF7h) ! Constants.
!xlSolid      EQUATE(        1 ) ! Constants.
!xlLeft       EQUATE(0FFFFEFDDh) ! Constants.
!xlRight      EQUATE(0FFFFEFC8h) ! Constants.
!xlLastCell   EQUATE(       11 ) ! Constants.
!
!xlTopToBottom EQUATE(        1 ) ! Constants.
!
!xlCenter     EQUATE(0FFFFEFF4h) ! Excel.Constants
!xlBottom     EQUATE(0FFFFEFF5h) ! Excel.Constants
!!----------------------------------------------------
!
!!----------------------------------------------------
!xlDiagonalDown     EQUATE( 5) ! XlBordersIndex
!xlDiagonalUp       EQUATE( 6) ! XlBordersIndex
!xlEdgeLeft         EQUATE( 7) ! XlBordersIndex
!xlEdgeTop          EQUATE( 8) ! XlBordersIndex
!xlEdgeBottom       EQUATE( 9) ! XlBordersIndex
!xlEdgeRight        EQUATE(10) ! XlBordersIndex
!xlInsideVertical   EQUATE(11) ! XlBordersIndex
!xlInsideHorizontal EQUATE(12) ! XlBordersIndex
!!----------------------------------------------------
!
!!----------------------------------------------------
!xlA1         EQUATE(        1 ) ! XlReferenceStyle
!xlR1C1       EQUATE(0FFFFEFCAh) ! XlReferenceStyle
!!----------------------------------------------------
!
!!----------------------------------------------------
!xlFilterCopy    EQUATE(2) ! XlFilterAction
!xlFilterInPlace EQUATE(1) ! XlFilterAction
!!----------------------------------------------------
!
!!----------------------------------------------------
!xlGuess EQUATE(0) ! XlYesNoGuess
!xlYes   EQUATE(1) ! XlYesNoGuess
!xlNo    EQUATE(2) ! XlYesNoGuess
!!----------------------------------------------------
!
!!----------------------------------------------------
!xlAscending  EQUATE(1) ! XlSortOrder
!xlDescending EQUATE(2) ! XlSortOrder
!!----------------------------------------------------
!
!!----------------------------------------------------
!xlLandscape EQUATE(2) ! XlPageOrientation
!xlPortrait  EQUATE(1) ! XlPageOrientation
!!----------------------------------------------------
!
!!----------------------------------------------------
!xlDownThenOver EQUATE( 1 ) ! XlOrder
!xlOverThenDown EQUATE( 2 ) ! XlOrder
!!----------------------------------------------------
!
!---   MS Office EQUATES   ---------------------------------------------------------
!----------------------------------------------------
msoCTrue          EQUATE(        1 ) ! MsoTriState
msoFalse          EQUATE(        0 ) ! MsoTriState
msoTriStateMixed  EQUATE(0FFFFFFFEh) ! MsoTriState
msoTriStateToggle EQUATE(0FFFFFFFDh) ! MsoTriState
msoTrue           EQUATE(0FFFFFFFFh) ! MsoTriState
!----------------------------------------------------

!----------------------------------------------------
msoScaleFromBottomRight EQUATE(2) ! MsoScaleFrom
msoScaleFromMiddle      EQUATE(1) ! MsoScaleFrom
msoScaleFromTopLeft     EQUATE(0) ! MsoScaleFrom
!----------------------------------------------------

!----------------------------------------------------
msoPropertyTypeBoolean EQUATE(2) ! MsoDocProperties
msoPropertyTypeDate    EQUATE(3) ! MsoDocProperties
msoPropertyTypeFloat   EQUATE(5) ! MsoDocProperties
msoPropertyTypeNumber  EQUATE(1) ! MsoDocProperties
msoPropertyTypeString  EQUATE(4) ! MsoDocProperties
!----------------------------------------------------
!-----------------------------------------------------------------------------------
CommandLine STRING(255)
tmpPos      LONG
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::7:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRWTradeAcc.UpdateBuffer
   AccountNumber_QUEUE.AccountNumberValue = tra:Account_Number
   GET(AccountNumber_QUEUE,AccountNumber_QUEUE.AccountNumberValue)
  IF ERRORCODE()
     AccountNumber_QUEUE.AccountNumberValue = tra:Account_Number
     ADD(AccountNumber_QUEUE,AccountNumber_QUEUE.AccountNumberValue)
    AccountNumberTag = '*'
  ELSE
    DELETE(AccountNumber_QUEUE)
    AccountNumberTag = ''
  END
    Queue:Browse.AccountNumberTag = AccountNumberTag
  IF (AccountNumberTag = '*')
    Queue:Browse.AccountNumberTag_Icon = 2
  ELSE
    Queue:Browse.AccountNumberTag_Icon = 1
  END
  PUT(Queue:Browse)
  ThisWindow.Reset(1)
  SELECT(?List,CHOICE(?List))
  AccountNumberCount = RECORDS(AccountNumber_QUEUE) !DAS Taging
  AccountNumberChange = 1  !DAS Taging
  !neil
DASBRW::7:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRWTradeAcc.Reset
  FREE(AccountNumber_QUEUE)
  LOOP
    NEXT(BRW6::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     AccountNumber_QUEUE.AccountNumberValue = tra:Account_Number
     ADD(AccountNumber_QUEUE,AccountNumber_QUEUE.AccountNumberValue)
  END
  SETCURSOR
  BRWTradeAcc.ResetSort(1)
  ThisWindow.Reset(1)
  SELECT(?List,CHOICE(?List))
  AccountNumberCount = RECORDS(AccountNumber_QUEUE) !DAS Taging
  AccountNumberChange = 1  !DAS Taging
DASBRW::7:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(AccountNumber_QUEUE)
  BRWTradeAcc.Reset
  SETCURSOR
  BRWTradeAcc.ResetSort(1)
  ThisWindow.Reset(1)
  SELECT(?List,CHOICE(?List))
  AccountNumberCount = RECORDS(AccountNumber_QUEUE) !DAS Taging
  AccountNumberChange = 1  !DAS Taging
DASBRW::7:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::7:QUEUE)
  LOOP QR# = 1 TO RECORDS(AccountNumber_QUEUE)
    GET(AccountNumber_QUEUE,QR#)
    DASBRW::7:QUEUE = AccountNumber_QUEUE
    ADD(DASBRW::7:QUEUE)
  END
  FREE(AccountNumber_QUEUE)
  BRWTradeAcc.Reset
  LOOP
    NEXT(BRW6::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::7:QUEUE.AccountNumberValue = tra:Account_Number
     GET(DASBRW::7:QUEUE,DASBRW::7:QUEUE.AccountNumberValue)
    IF ERRORCODE()
       AccountNumber_QUEUE.AccountNumberValue = tra:Account_Number
       ADD(AccountNumber_QUEUE,AccountNumber_QUEUE.AccountNumberValue)
    END
  END
  SETCURSOR
  BRWTradeAcc.ResetSort(1)
  ThisWindow.Reset(1)
  SELECT(?List,CHOICE(?List))
  AccountNumberCount = RECORDS(AccountNumber_QUEUE) !DAS Taging
  AccountNumberChange = 1  !DAS Taging
DASBRW::7:DASSHOWTAG Routine
   CASE DASBRW::7:TAGDISPSTATUS
   OF 0
      DASBRW::7:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::7:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::7:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRWTradeAcc.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!-----------------------------------------------
OKButton_Pressed            ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        CancelPressed = False
        !debug:Active  = True

        IF NOT FillLocation()
            Case Missive('You must select at least one location.','ServiceBase 3g',|
                           'mstop.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive

            EXIT
        END !IF
        !-----------------------------------------------------------------

        !-----------------------------------------------
        ! 23 Oct 2002 John
        ! R003, berrjo :Insert the report exe name EG VODR56.V01
        ! Do not attach the EXE extenstion but show the version number.  Increment the version number each time changed.
        ! Make this standard on all reports sent for correction.
        ! LOC:Version STRING(8) 3.1.0000 3.1 to match sevicebase version (and dictionary) .0000 increment before each compile
        !
        !LOC:Version = '3.1.0002'
        !-----------------------------------------------

        DO ExportSetup
        DO ExportBody
        DO ExportFinalize

        If Command('/SCHEDULE')
            If Access:REPSCHLG.PrimeRecord() = Level:Benign
                rlg:REPSCHEDRecordNumber = rpd:RecordNumber
                rlg:Information = 'Report Name: ' & Clip(rpd:ReportName) & ' - ' & Clip(rpd:ReportCriteriaType) & |
                                  '<13,10>Report Finished'
                If Access:REPSCHLG.TryInsert() = Level:Benign
                    !Insert
                Else ! If Access:REPSCHLG.TryInsert() = Level:Benign
                    Access:REPSCHLG.CancelAutoInc()
                End ! If Access:REPSCHLG.TryInsert() = Level:Benign
            End ! If Access.REPSCHLG.PrimeRecord() = Level:Benign
        End


        POST(Event:CloseWindow)
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
ExportSetup                             ROUTINE
    DATA
temp DATE
    CODE
        !-----------------------------------------------------------------
        DO GetFileName
        IF CLIP(LOC:FileName) = ''
            DO LoadFileDialog
        END !IF

        IF LOC:StartDate > LOC:EndDate
            temp          = LOC:EndDate
            LOC:EndDate   = LOC:StartDate
            LOC:StartDate = temp
        END !IF

        IF LOC:FileName = ''

            CancelPressed = True

            EXIT
        END !IF LOC:FileName = ''

        IF LOWER(RIGHT(LOC:FileName, 4)) <> '.xls'
            LOC:FileName = CLIP(LOC:FileName) & '.xls'
        END !IF
        !-----------------------------------------------------------------
        SETCURSOR(CURSOR:Wait)

        DO ProgressBar_Setup
        !-----------------------------------------------------------------
        DO XL_Setup

        DO XL_AddWorkbook
        !-----------------------------------------------------------------
    EXIT
ExportBody                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        WriteDebug('ExportBody')

        IF CancelPressed = True
            EXIT
        END !IF
        !-----------------------------------------------------------------
        DO CreateTitleSheet
        DO CreateDataSheet
        DO WriteHeadSummary
        !-----------------------------------------------------------------
    EXIT
ExportFinalize                          ROUTINE
    DATA
FilenameLength LONG
StartAt        LONG
SUBLength      LONG
    CODE
        !-----------------------------------------------------------------
        DO ResetClipboard

        IF CancelPressed
            DO XL_DropAllWorksheets
            DO XL_Finalize

            EXIT
        END !IF
        !-----------------------------------------------------------------
        Excel{'Sheets("Sheet3").Select'}
        Excel{'ActiveWindow.SelectedSheets.Delete'}

        Excel{'Sheets("Summary").Select'}
        Excel{'Range("A1").Select'}

        Excel{'Application.ActiveWorkBook.SaveAs("' & LEFT(CLIP(LOC:FileName)) & '")'}
        Excel{'Application.ActiveWorkBook.Close()'}
        !-----------------------------------------------------------------
        DO XL_Finalize

        DO ProgressBar_Finalise
        !-----------------------------------------------------------------
        SETCURSOR()
        IF Automatic = FALSE
            IF MATCH(LOC:Filename, CLIP(LOC:DesktopPath) & '*')
                FilenameLength = LEN(CLIP(LOC:Filename   ))
                StartAt        = LEN(CLIP(LOC:DesktopPath)) + 1
                SUBLength      = FilenameLength - StartAt + 1

            ELSE
            END !IF
        END
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
CreateTitleSheet                            ROUTINE
    DATA
    CODE
!        !-----------------------------------------------------------------
!        WriteDebug('CreateTitleSheet')
!
!        Excel{'ActiveWorkBook.Sheets("Sheet1").Select'}
!        !-----------------------------------------------------------------
!        LOC:Text          = 'Summary'
!        sheet:TempLastCol = sheet:HeadLastCol
!
!        DO CreateWorksheetHeader
!        !-----------------------------------------------------------------
!        DO XL_SetWorksheetPortrait
!
!        Excel{'ActiveSheet.Columns("A:E").ColumnWidth'}                         = 20
!        Excel{'ActiveSheet.Columns("F:' & sheet:HeadLastCol & '").ColumnWidth'} = 12
!        !-----------------------------------------------------------------
!        Excel{'Range("A' & sheet:HeadSummaryRow & '").Select'}
!            DO XL_SetBold
!            Excel{'ActiveCell.Formula'} = 'Summary'
!
!        Excel{'Range("A' & sheet:HeadSummaryRow & ':' & sheet:HeadLastCol & sheet:HeadSummaryRow & '").Select'}
!            DO XL_SetTitle
!            DO XL_SetBorder
!        !-----------------------------------------------------------------
!        ! Prepare For summary details
!        !
!        Excel{'Range("A' & sheet:HeadSummaryRow+1 & '").Select'}
!        !-----------------------------------------------------------------
    EXIT
CreateDataSheet                                             ROUTINE
    DATA
!GRN_First LONG(True)
!ORP_First LONG(True)
    CODE
        !-----------------------------------------------------------------
        WriteDebug('CreateDataSheet')

        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
!        DO XL_AddSheet
            DO XL_SetWorksheetLandscape
        !-----------------------------------------------------------------
        DO InitColumns

        LOC:SectionName   = 'Detailed'
        excel:CommentText = ''
        sheet:TempLastCol = sheet:DataLastCol

        DO CreateSectionHeader
        !-----------------------------------------------------------------
        Progress:Text    = CLIP(LOC:SectionName)
        RecordsToProcess = 1000 * (LOC:EndDate - LOC:StartDate + 1)

        DO ProgressBar_LoopPre
        !-----------------------------------------------------------------
!        GRN_First = True
!        LOOP WHILE LoadGRNOTES(LOC:StartDate, LOC:EndDate, GRN_First)
!            !-------------------------------------------------------------
!            WriteDebug('CreateDataSheet GRN_First')
!            GRN_First = False
!
!            ORP_First = True
!            LOOP WHILE LoadORDPARTS(grn:Order_Number, grn:Goods_Received_Date, ORP_First)
!                !---------------------------------------------------------
!                WriteDebug('CreateDataSheet(' & grn:Order_Number & '), ORP_First="' & ORP_First & '"')
!                DO ProgressBar_Loop
!
!                IF CancelPressed
!                    BREAK
!                END !IF
!                !---------------------------------------------------------
!                ORP_First = False
!
!                DO WriteColumns
!                !---------------------------------------------------------
!            END !LOOP
!            !-------------------------------------------------------------
!        END !LOOP

        If Automatic = True
            !Get account criteria for automated ini  (DBH: 06-05-2004)
            Free(AccountNumber_QUEUE)
            Set(tra:Account_Number_Key)
            Loop
                If Access:TRADEACC.NEXT()
                   Break
                End !If
                If tra:BranchIdentification = ''
                    Cycle
                End !If tra:BranchIdentification = ''

                If GETINI('GOODSRECEIVED',tra:Account_Number,,CLIP(Path()) & '\REPAUTO.INI') = 1
                    AccountNumberValue = tra:Account_Number
                    Add(AccountNumber_QUEUE)
                End !If GETINI('TAT','tra:Account_Number',,CLIP(Path()) & '\REPAUTO.INI') = 1
            End !Loop

        End !If Automatic = True


        Access:GRNOTESR.ClearKey(grr:Date_Recd_Key)
        grr:Goods_Received_Date = LOC:StartDate
        Set(grr:Date_Recd_Key,grr:Date_Recd_Key)
        Loop
            If Access:GRNOTESR.NEXT()
               Break
            End !If
            If grr:Goods_Received_Date > Loc:EndDate      |
                Then Break.  ! End If

            Access:RETSALES.ClearKey(ret:Invoice_Number_Key)
            ret:Invoice_Number = grr:Order_Number
            If Access:RETSALES.TryFetch(ret:Invoice_Number_Key) = Level:Benign
                !Found

                !Only pick up sales where the account number matches the "stores account"
                !of the tagges trade accounts
                FoundAccount# = 0
                Loop x# = 1 To Records(AccountNumber_QUEUE)
                    Get(AccountNumber_QUEUE,x#)

                    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                    tra:Account_Number  = AccountNumberValue
                    If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                        !Found
                        
                        If tra:StoresAccount = ret:Account_Number
                            FoundAccount# = 1
                            Break
                        End !If tra:StoresAccount = ret:Account_Number
                    Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                        !Error
                    End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                End !Loop x# = 1 To Records(AccountNumber_QUEUE)

                If FoundAccount# = 0
                    Cycle
                End !If FoundAccount# = 0

                Access:RETSTOCK.ClearKey(res:Part_Number_Key)
                res:Ref_Number  = ret:Ref_Number
                Set(res:Part_Number_Key,res:Part_Number_Key)
                Loop
                    If Access:RETSTOCK.NEXT()
                       Break
                    End !If
                    If res:Ref_Number  <> ret:Ref_Number      |
                        Then Break.  ! End If
                    If res:GRNNumber <> grr:Goods_Received_Number
                        Cycle
                    End !If res:GRNNumber <> grr:Goods_Received_Number

                    DO ProgressBar_Loop

                    IF CancelPressed
                       BREAK
                    END !IF

!                    IF orp:Order_Number <> grn:Order_Number
!                        WriteDebug('CreateDataSheet(EOI orp:Order_Number="' & orp:Order_Number & '")')
!                      BREAK
!                    END
!                    IF orp:GRN_Number <> grn:Goods_Received_Number
!                        WriteDebug('CreateDataSheet(EOI orp:GRN_Number="' & orp:GRN_Number & '")')
!                      CYCLE
!                    END
                    DO WriteColumns

                End !Loop
            Else!If Access:RETSALES.TryFetch(ret:Invoice_Number_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:RETSALES.TryFetch(ret:Invoice_Number_Key) = Level:Benign

        END !LOOP 1


        DO ProgressBar_LoopPost
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        DO WriteDataSummary
        !-----------------------------------------------------------------
    EXIT

!-----------------------------------------------
CreateSectionHeader                                        ROUTINE
    DATA
LastColumn STRING(1)
    CODE
        !-----------------------------------------------       
        LOC:Text = CLIP(LOC:SectionName)

        DO CreateWorksheetHeader
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataSectionRow & ':' & sheet:DataLastCol & sheet:DataSectionRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A' & sheet:DataSectionRow & '").Select'}
            Excel{'ActiveCell.Formula'}         = 'Section Name:'
            DO XL_ColRight
                Excel{'ActiveCell.Formula'}     = CLIP(LOC:SectionName)
                DO XL_SetBold
                DO XL_HorizontalAlignmentLeft

        Excel{'Range("E' & sheet:DataSectionRow & '").Select'}
            Excel{'ActiveCell.Formula'}         = 'Total Records:'
            DO XL_HorizontalAlignmentRight
            DO XL_ColRight
                Excel{'ActiveCell.Formula'}     = 0
                DO XL_HorizontalAlignmentLeft
        !-----------------------------------------------
        DO SetColumns

        Excel{'Range("A' & sheet:DataHeaderRow+1 & '").Select'}
        !-----------------------------------------------
    EXIT
CreateWorksheetHeader                                                               ROUTINE
    DATA
CurrentRow LONG(3)
    CODE
        !-----------------------------------------------       
        Excel{'ActiveSheet.Name'}                    = CLIP(LOC:Text)
        !-----------------------------------------------       
        Excel{'Range("A1").Select'}
            Excel{'ActiveCell.Formula'}              = CLIP(LOC:ProgramName)
            DO XL_SetBold
            Excel{'ActiveCell.Font.Size'} = 16

        Excel{'Range("D1").Select'}
            Excel{'ActiveCell.Formula'}              = Clip(tmp:VersionNumber)
            DO XL_SetBold
            Excel{'ActiveCell.Font.Size'} = 16


        Excel{'Range("A1:' & sheet:TempLastCol & '1").Select'}
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------
        Excel{'Range("A' & CurrentRow & ':' & sheet:TempLastCol & CurrentRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A' & CurrentRow & '").Select'}
            Excel{'ActiveCell.Formula'}              = 'Criteria'
            DO XL_SetBold
        !-----------------------------------------------
        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.FormulaR1C1'}      = 'Ordered Date To:'
                DO XL_ColRight
                    DO XL_FormatDate
                    DO XL_HorizontalAlignmentLeft
                    Excel{'ActiveCell.Formula'}      = DateToString(LOC:StartDate)

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.FormulaR1C1'}      = 'Ordered Date To:'
                DO XL_ColRight
                    DO XL_FormatDate
                    DO XL_HorizontalAlignmentLeft
                    Excel{'ActiveCell.Formula'}      = DateToString(LOC:EndDate)

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.Formula'}          = 'Created By:'
                DO XL_ColRight
                    Excel{'ActiveCell.Formula'}      = LOC:UserName

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.Formula'}          = 'Date Created:'
                DO XL_ColRight
                    DO XL_FormatDate
                    DO XL_HorizontalAlignmentLeft
                    Excel{'ActiveCell.Formula'}      = DateToString(TODAY())
        !-----------------------------------------------
        Excel{'Range("A4:' & sheet:TempLastCol & CurrentRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------
        sheet:DataSectionRow = CurrentRow + 2
        sheet:DataHeaderRow  = CurrentRow + 4

        sheet:HeadSummaryRow = CurrentRow + 2
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
InitColumns                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        FREE(HeaderQueue)

        head:ColumnName       = 'GRN Number'
            head:ColumnWidth  = 15.00
            head:NumberFormat = '###0'
            ADD(HeaderQueue)

        head:ColumnName       = 'GRN Date'
            head:ColumnWidth  = 12.75
            head:NumberFormat = excel:DateFormat
            ADD(HeaderQueue)

!        head:ColumnName       = 'Supplier Number'
!            head:ColumnWidth  = 20.00
!            head:NumberFormat = chr(64) ! Text (AT) Sign
!            ADD(HeaderQueue)
!
!        head:ColumnName       = 'Supplier Name'
!            head:ColumnWidth  = 20.00
!            head:NumberFormat = chr(64) ! Text (AT) Sign
!            ADD(HeaderQueue)
!
        head:ColumnName       = 'Account Name'
            head:ColumnWidth  = 35.00
            head:NumberFormat = CHR(64)
            ADD(HeaderQueue)

        head:ColumnName       = 'Delivery Note Number'
            head:ColumnWidth  = 10.00
            head:NumberFormat = '###0'
            ADD(HeaderQueue)

        head:ColumnName       = 'Part Number'
            head:ColumnWidth  = 11.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Description'
            head:ColumnWidth  = 35.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        head:ColumnName       = 'Quantity'
            head:ColumnWidth  = 11.00
            head:NumberFormat = '#,##0'
            ADD(HeaderQueue)

        head:ColumnName       = 'GRN Amount'
            head:ColumnWidth  = 11.00
            head:NumberFormat = '#,##0.00'
            ADD(HeaderQueue)

        head:ColumnName       = 'Line Amount'
            head:ColumnWidth  = 11.00
            head:NumberFormat = '#,##0.00'
            ADD(HeaderQueue)
        !-----------------------------------------------
        sheet:DataLastCol = NumberToColumn(RECORDS(HeaderQueue))
        !-----------------------------------------------
    EXIT
FormatColumns                                                   ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF RecordCount < 1
            EXIT
        END !IF
        !-----------------------------------------------
        LOOP x# = 1 TO RECORDS(HeaderQueue)
            !-------------------------------------------
            GET(HeaderQueue, x#)
            IF ERRORCODE()
                CYCLE
            END !IF

            Excel{'Range("' & CHR(64+x#) & sheet:DataHeaderRow+1 & ':' & CHR(64+x#) & sheet:DataHeaderRow+RecordCount & '").Select'}
                Excel{'Selection.NumberFormat'} = head:NumberFormat
                !DO XL_HighLight ! DEBUG
                DO XL_HorizontalAlignmentRight
            !-------------------------------------------
        END !LOOP
        !-----------------------------------------------
    EXIT
SetColumns                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataHeaderRow & ':' & sheet:DataLastCol & sheet:DataHeaderRow & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & sheet:DataHeaderRow  & '").Select'}
        !-----------------------------------------------
        LOOP x# = 1 TO RECORDS(HeaderQueue)
            GET(HeaderQueue, x#)

            Excel{'ActiveCell.Formula'}         = head:ColumnName
                Excel{'ActiveCell.ColumnWidth'} = head:ColumnWidth
                DO XL_ColRight
        END !LOOP
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataHeaderRow+1  & '").Select'}
        Excel{'ActiveWindow.FreezePanes'} = True
        !-----------------------------------------------
    EXIT
WriteColumns                                                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        WriteDebug('WriteColumns CHECKING')

!        IF NOT LoadORDERS(grn:Order_Number)
!            WriteDebug('WriteColumns CHECKING FAIL grn:Order_Number="' & CLIP(grn:Order_Number) & '"')
!
!            EXIT
!        ELSIF NOT LoadSUPPLIER(ord:Supplier)
!            WriteDebug('WriteColumns CHECKING FAIL ord:Supplier="' & CLIP(ord:Supplier) & '"')
!
!            EXIT
!        END !IF
!
!        IF NOT LoadSTOCK(orp:Part_Ref_Number)
!            WriteDebug('WriteColumns CHECKING FAIL orp:Part_Ref_Number="' & CLIP(orp:Part_Ref_Number) & '"')
!
!            EXIT
!        ELSIF NOT GetLocation(sto:Location)
!            WriteDebug('WriteColumns CHECKING FAIL sto:Location="' & CLIP(sto:Location) & '"')
!
!            EXIT
!        END !IF
        !===============================================
        WriteDebug('WriteColumns CHECK OK')
        UpdateSupplierQueue(sup:Account_Number, orp:Part_Number, sto:Location)

        RecordCount += 1

        WriteColumn( grr:Goods_Received_Number               ) ! GRN Number
        WriteColumn( DateToString(grr:Goods_Received_Date)   ) ! GRN Date
        !WriteColumn( sup:Account_Number                      ) ! Supplier Number
        !WriteColumn( sup:Company_Name                        ) ! Supplier Name
        WriteColumn( tra:Account_Number)                      !Stores Account Number For Retail Sale Account Number
        WriteColumn( ret:Invoice_Number                      ) ! Delivery Note Number
        WriteColumn( res:Part_Number                         ) ! Part Number
        WriteColumn( res:Description                         ) ! Description
        WriteColumn( res:Quantity                     ) ! Quantity
        WriteColumn( Round(res:Item_Cost * res:QuantityReceived,.01) ) ! GRN Amount
        WriteColumn( Round(res:Item_Cost * res:Quantity,.01)        ) ! Line Amount

        IF excel:OperatingSystem < 5
            DO PassViaClipboard
        END !IF
        !-----------------------------------------------
        DO XL_ColFirst
        DO XL_RowDown
        !-----------------------------------------------
        WriteDebug('WriteColumns EXIT')
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
PassViaClipboard                                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF clip:Saved = False
            clip:OriginalValue = CLIPBOARD()
            clip:Saved         = True
        END !IF
        !-----------------------------------------------
        SETCLIPBOARD(CLIP(clip:Value))
            Excel{'ActiveSheet.Paste()'}
        clip:Value = ''
        !-----------------------------------------------
    EXIT
ResetClipboard                                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF clip:Saved = False
            EXIT
        END !IF

        SETCLIPBOARD(clip:OriginalValue)
        
        clip:Saved = False
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
WriteDataSummary                                              ROUTINE
    DATA
    CODE
!        !-----------------------------------------------------------------
!        ! Summary details
!        !-----------------------------------------------------------------
!        IF CancelPressed
!            EXIT
!        END !IF
!        !-----------------------------------------------------------------
!        IF RecordCount < 1
!            Excel{'ActiveCell.Formula'} = 'None Found'
!
!            EXIT
!        END !IF
!        !-----------------------------------------------------------------
!        DO FormatColumns
!
!        Excel{'Range("F' & sheet:DataSectionRow & '").Select'}
!            DO XL_HorizontalAlignmentLeft
!            Excel{'ActiveCell.Formula'}         = RecordCount
!
!        Excel{'Range("G' & sheet:DataSectionRow & '").Select'}
!            DO XL_HorizontalAlignmentRight
!            Excel{'ActiveCell.Formula'}         = 'Showing'
!
!        Excel{'Range("H' & sheet:DataSectionRow & '").Select'}
!            DO XL_HorizontalAlignmentLeft
!            Excel{'ActiveCell.Formula'}         = '=SUBTOTAL(2, A' & sheet:DataHeaderRow+1 & ':a' & sheet:DataHeaderRow+RecordCount & ')'
!        !-----------------------------------------------------------------
!        Excel{'Range("A' & sheet:DataHeaderRow & '").Select'}
!            DO XL_DataAutoFilter
!        !-----------------------------------------------------------------
    EXIT
WriteHeadSummary                                              ROUTINE
    DATA
QueueIndex   LONG
ResultsCount LONG
TitleRow     LONG
FirstRow     LONG
LastRow      LONG
    CODE
!        !-----------------------------------------------------------------
!        ! summary sheet details
!        !
!        Excel{'ActiveWorkBook.Sheets("Summary").Select'}
!
!        ResultsCount = RECORDS(SupplierQueue)
!        TitleRow     = sheet:HeadSummaryRow + 1 !Excel{'ActiveCell.Row'}  ! first blank line
!        FirstRow     = TitleRow             + 1
!        LastRow      = TitleRow             + ResultsCount
!        !-----------------------------------------------------------------
!        Excel{'Range("A' & TitleRow & ':' & sheet:HeadLastCol & TitleRow & '").Select'}
!            DO XL_SetColumnHeader
!
!        Excel{'Range("A' & TitleRow & '").Select'}
!            Excel{'ActiveCell.Offset(0, 0).Formula'} = 'Part Number'
!            Excel{'ActiveCell.Offset(0, 1).Formula'} = 'Description'
!            Excel{'ActiveCell.Offset(0, 2).Formula'} = 'Quantity Received'
!            Excel{'ActiveCell.Offset(0, 3).Formula'} = 'Amount'
!        !-----------------------------------------------
!        Excel{'Range("A' & FirstRow & '").Select'}
!
!        IF ResultsCount < 1
!            Excel{'ActiveCell.Formula'} = 'None Found'
!
!            EXIT
!        END !IF
!        !-----------------------------------------------------------------
!        ! Totals
!        !
!        SORT(SupplierQueue, +supQ:AccountNumber, +supQ:Part_Number, +supQ:Location)
!
!        LOOP QueueIndex = 1 TO RECORDS(SupplierQueue)
!            !-------------------------------------------------------------
!            GET(SupplierQueue, QueueIndex)
!
!!            Excel{'ActiveCell.Formula'}          = '''' & supQ:AccountNumber
!!                DO XL_HorizontalAlignmentRight
!!
!!            DO XL_ColRight
!!                DO XL_HorizontalAlignmentRight
!!                Excel{'ActiveCell.Formula'}      =        supQ:CompanyName
!
!!            DO XL_ColRight
!                Excel{'ActiveCell.Formula'}      = '''' & supQ:Part_Number
!
!            DO XL_ColRight
!                Excel{'ActiveCell.Formula'}      =        supQ:Description
!
!            DO XL_ColRight
!!                Excel{'ActiveCell.Formula'}      = '''' & supQ:Location
!!
!!            DO XL_ColRight
!                Excel{'ActiveCell.NumberFormat'} =        '#,##0'
!                Excel{'ActiveCell.Formula'}      =        supQ:Quantity
!
!            DO XL_ColRight
!                Excel{'ActiveCell.NumberFormat'} =        '#,##0.00'
!                Excel{'ActiveCell.Formula'}      =        supQ:Amount
!            !-------------------------------------------------------------
!            DO XL_ColFirst
!            DO XL_RowDown
!            !-------------------------------------------------------------
!        END !LOOP
!
!        Excel{'Range("A' & TitleRow & ':' & sheet:HeadLastCol & LastRow & '").Select'}
!            Excel{'Selection.AutoFilter'}
!
!        Excel{'Range("A' & LastRow + 2 & '").Select'}
!        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
GetUserName                            ROUTINE
    DATA
CommandLine STRING(255)
tmpPos      LONG
    CODE
        !-----------------------------------------------
        CommandLine = CLIP(COMMAND(''))

!        LOC:UserName = LOC:ApplicationName
!
!        Case MessageEx('Fail User Check For "' & CLIP(CommandLine) & '"?', LOC:ApplicationName & ' DEBUG',|
!                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
!            Of 1 ! &Yes Button
!                POST(Event:CloseWindow)
!
!                EXIT
!            Of 2 ! &No Button
!                EXIT
!        End!Case MessageEx
        !-----------------------------------------------
        tmpPos = INSTRING('%', CommandLine)
        IF NOT tmpPos
            Case Missive('You must run this report from ServiceBase''s "Custom Reports".','ServiceBase 3g',|
                           'mstop.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive

            POST(Event:CloseWindow)

           EXIT
        END !IF tmpPos
        !-----------------------------------------------
        SET(USERS)
        Access:USERS.Clearkey(use:Password_Key)
        glo:Password = CLIP(SUB(CommandLine, tmpPos + 1, 30))
      
        Access:USERS.Clearkey(use:Password_Key)
        use:Password = glo:Password
        If Access:USERS.Tryfetch(use:Password_Key)
            Case Missive('Error! Unable to find the user''s details.','ServiceBase 3g',|
                           'mstop.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive

            POST(Event:CloseWindow)

           EXIT
        End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        !-----------------------------------------------
        LOC:UserName = use:Forename

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = use:Surname
        ELSE
            LOC:UserName = CLIP(use:Forename) & ' ' & use:Surname 
        END !IF

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = '<' & use:User_Code & '>'
        END !IF
        !-----------------------------------------------
    EXIT
GetUserName2                            ROUTINE
    DATA
CommandLine STRING(255)
tmpPos      LONG
    CODE
        !-----------------------------------------------
        If Command('/SCHEDULE')
            Automatic = False
            Exit
        End ! If Command('/SCHEDULE')
        CommandLine = CLIP(COMMAND(''))

!        LOC:UserName = LOC:ApplicationName
!
!        Case MessageEx('Fail User Check For "' & CLIP(CommandLine) & '"?', LOC:ApplicationName & ' DEBUG',|
!                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
!            Of 1 ! &Yes Button
!                POST(Event:CloseWindow)
!
!                EXIT
!            Of 2 ! &No Button
!                EXIT
!        End!Case MessageEx
        !-----------------------------------------------
        Automatic = FALSE
        tmpPos = INSTRING('%', CommandLine)
!        IF NOT tmpPos
!            Case MessageEx('Attempting to use Workshop Performance Report<10,13>'            & |
!                '   without using ' & LOC:ApplicationName & '.<10,13>'                       & |
!                '   Start ' & LOC:ApplicationName & ' and run the report from there.<10,13>',  |
!                LOC:ApplicationName,                                                           |
!                'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,LocalTimeOut)
!            Of 1 ! &OK Button
!            END!Case MessageEx
!
!           POST(Event:CloseWindow)
!
!           EXIT
!        END !IF tmpPos
        !-----------------------------------------------
        skip# = 0
        SET(USERS)
        Access:USERS.Clearkey(use:Password_Key)
        glo:Password = CLIP(SUB(CommandLine, tmpPos + 1, 30))
      
        Access:USERS.Clearkey(use:Password_Key)
        use:Password = glo:Password
        If Access:USERS.Tryfetch(use:Password_Key)
            !Assert(0,'<13,10>Fetch Error<13,10>')
!            Case MessageEx('Unable to find your logged in user details.', |
!                    LOC:ApplicationName,                                  |
!                    'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,LocalTimeOut)
!                Of 1 ! &OK Button
!            End!Case MessageEx
!
!            POST(Event:CloseWindow)
!
!           EXIT
            LOC:UserName = 'AUTOMATED PROCEDURE'
            Automatic = TRUE
            skip# = 1
        End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        !-----------------------------------------------
        IF skip# = 0
            LOC:UserName = use:Forename

            IF CLIP(LOC:UserName) = ''
                LOC:UserName = use:Surname
            ELSE
                LOC:UserName = CLIP(use:Forename) & ' ' & use:Surname 
            END !IF

            IF CLIP(LOC:UserName) = ''
                LOC:UserName = '<' & use:User_Code & '>'
            END !IF
            !-----------------------------------------------
        END
    EXIT
GetFileName                             ROUTINE ! Generate default file name
    DATA
local:Desktop         CString(255)
DeskTopExists   BYTE
ApplicationPath STRING(255)
excel:ProgramName       Cstring(255)
    CODE

    excel:ProgramName = 'Goods Received Franchises Report'
    SHGetSpecialFolderPath( GetDesktopWindow(), local:Desktop, 5, FALSE )
    local:Desktop = Clip(local:Desktop) & '\ServiceBase Export'

    !Does the Export Folder already Exists?
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    local:Desktop = Clip(local:Desktop) & '\' & Clip(excel:ProgramName)
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    loc:Path = local:Desktop
    loc:FileName = Clip(local:Desktop) & '\' & Clip(excel:ProgramName) & ' ' & Format(Today(),@d12)
!
!    If Exists(excel:FileName & '.xls')
!        Remove(excel:FileName & '.xls')
!        If Error()
!            Beep(Beep:SystemHand)  ;  Yield()
!            Case Missive('Cannot get access to the selected document:'&|
!                '|' & Clip(excel:FileName) & ''&|
!                '|'&|
!                '|Ensure the file is not in use and try again.','ServiceBase',|
!                           'mstop.jpg','/&OK')
!                Of 1 ! &OK Button
!            End!Case Message
!            Exit
!        End !If Error()
!    End !If Exists(excel:FileName)
!        !-----------------------------------------------
!        ! Generate default file name
!        !
!        !-----------------------------------------------
!        ! 4 Dec 2001 John Stop using def:Export_Path as previously directed
!        ! 4 Dec 2001 John Use generated date to create file name not parameters LOC:StartDate,LOC:EndDate
!        !
!        IF CLIP(LOC:FileName) <> ''
!            ! Already generated 
!            EXIT
!        END !IF
!
!        DeskTopExists = False
!
!        SHGetSpecialFolderPath( GetDesktopWindow(), Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
!        LOC:DesktopPath = Desktop
!
!        ApplicationPath = Desktop & '\' & LOC:ApplicationName & ' Export'
!        IF EXISTS(CLIP(ApplicationPath))
!            LOC:Path      = CLIP(ApplicationPath) & '\'
!            DeskTopExists = True
!
!        ELSE
!            Desktop  = CLIP(ApplicationPath)
!
!            IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!!                MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!!            ELSE
!!                MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
!            END !IF
!
!            IF EXISTS(CLIP(ApplicationPath))
!                LOC:Path      = CLIP(ApplicationPath) & '\'
!                DeskTopExists = True
!            END !IF
!
!        END !IF
!
!        IF DeskTopExists
!            ApplicationPath = CLIP(LOC:Path) & CLIP(LOC:ProgramName)
!            Desktop         = CLIP(ApplicationPath)
!
!            IF NOT EXISTS(CLIP(ApplicationPath))
!                IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!!                    MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!!                ELSE
!!                    MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
!                END !IF
!            END !IF
!
!            IF EXISTS(CLIP(ApplicationPath))
!                LOC:Path = CLIP(ApplicationPath) & '\'
!            END !IF
!        END !IF
!
!        !-----------------------------------------------
!        ! 23 Oct 2002 John
!        ! R003, berrjo :Insert the report exe name EG VODR56.V01
!        ! Do not attach the EXE extenstion but show the version number.  Increment the version number each time changed.
!        ! Make this standard on all reports sent for correction.
!        ! LOC:Version STRING(8) 3.1.0000 3.1 to match sevicebase version (and dictionary) .0000 increment before each compile
!        !
!        !LOC:FileName = CLIP(SHORTPATH(LOC:Path)) & CLIP(LOC:ProgramName) & ' ' & FORMAT(TODAY(), @D12) & '.xls'
!        LOC:FileName = SHORTPATH(CLIP(LOC:Path)) &  CLIP(LOC:ProgramName) & ' VODR0066 ' & FORMAT(TODAY(), @D12) & '.xls'
!        !-----------------------------------------------
!        
!        !-----------------------------------------------
!    EXIT
LoadFileDialog                          ROUTINE ! Ask user if file name is empty
    DATA
OriginalPath STRING(255)
    CODE
        !-----------------------------------------------
        IF CLIP(LOC:Filename) = ''
            DO GetFileName 
        END !IF

        OriginalPath = PATH()
        SETPATH(LOC:Path) ! Required for Win95/98
            IF NOT FILEDIALOG('Save Spreadsheet', LOC:Filename, 'Microsoft Excel Workbook|*.XLS', |
                FILE:KeepDir + FILE:Save + FILE:NoError + FILE:LongName)

                LOC:Filename = ''
            END !IF
        SETPATH(OriginalPath)

        UPDATE()
        DISPLAY()
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
LoadSubAccount                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !
        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = LOC:AccountNumber
        !--------------------------------------------------------------------------------------
        IF Access:SUBTRACC.Fetch(sub:Account_Number_Key)
            LOC:Account_Name = '<Not Found>'
        ELSE
            LOC:Account_Name = CLIP(sub:Company_Name)
        END !IF
        !-----------------------------------------------
    EXIT
LoadUSERS                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Result = False

        Access:USERS.ClearKey(use:User_Code_Key)
        use:User_Code = LOC:UserCode
        SET(use:User_Code_Key, use:User_Code_Key)

        IF Access:USERS.NEXT()
            EXIT
        END !IF

        IF use:User_Code <> LOC:UserCode
            EXIT
        END !IF

        Result = True
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
ProgressBar_Setup                       ROUTINE
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
            RecordsPerCycle      = 25
            RecordsProcessed     = 0
            RecordsToProcess     = 10 !***The Number Of Records, or at least a guess***
            PercentProgress      = 0
            Progress:thermometer = 0

            thiswindow.reset(1) !This may error, if so, remove this line.
            open(progresswindow)
            progresswindow{PROP:Timer} = 1
         
            ?progress:userstring{prop:text} = 'Running...'
            ?progress:pcttext{prop:text}    = '0% Completed'
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***

ProgressBar_LoopPre          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***

        RecordsProcessed = 0
        RecordCount      = 0

        ?Progress:UserString{PROP:Text} = CLIP(Progress:Text)

        !RecordsToProcess = RECORDS(JOBS) !***The Number Of Records, or at least a guess***
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
        !-----------------------------------------------
    EXIT

ProgressBar_Loop             ROUTINE
    DATA
newPctText STRING(255)
    CODE
        !-----------------------------------------------
        !***Place the following code inside the loop. The first line updates the progress bar, and the rest allows the user to cancel the routine. This can be removed if necessary. ****

        Do ProgressBar_GetNextRecord2 !Necessary

        Do ProgressBar_CancelCheck
        If CancelPressed
            CLOSE(ProgressWindow)
        End!If 
        !-----------------------------------------------
        newPctText = 'Checked(' & RecordsProcessed & '), Found(' & RecordCount & ')'

        IF ?progress:pcttext{prop:text} <> newPctText
            ?progress:pcttext{prop:text} = newPctText

            Display()
        END !IF

        !***Place the following code inside the loop. The first line updates the progress bar, and the rest allows the user to cancel the routine. This can be removed if necessary. ****
        !-----------------------------------------------
    EXIT

ProgressBar_LoopPost         ROUTINE
    DATA
newPctText STRING(255)
    CODE
        !-----------------------------------------------
        ?Progress:UserString{PROP:Text} = CLIP(Progress:Text) & ' Done(' & RecordsProcessed & '/' & RecordCount & ')'

        newPctText = 'Checked(' & RecordsProcessed & '), Found(' & RecordCount & ')'

        IF ?progress:pcttext{prop:text} <> newPctText
            ?progress:pcttext{prop:text} = newPctText
        END !IF
        !-----------------------------------------------
    EXIT

ProgressBar_Finalise                        ROUTINE
        !**** End Of Loop ***
            Do ProgressBar_EndPrintRun
            close(progresswindow)
        !**** End Of Loop ***
        ! when in report procedure->LocalResponse = RequestCompleted

ProgressBar_GetNextRecord2                  routine
    recordsprocessed += 1
    recordsthiscycle += 1

    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        !?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end

ProgressBar_CancelCheck                     routine
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept

    If cancel# = 1
        BEEP(BEEP:SystemAsterisk)  ;  YIELD()

        Case Missive('Are you sure you want to cancel?','ServiceBase 3g',|
                       'mquest.jpg','\No|/Yes') 
            Of 2 ! Yes Button
                CancelPressed = True
            Of 1 ! No Button
        End ! Case Missive
    End!If cancel# = 1

ProgressBar_EndPrintRun                     routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()

RefreshWindow                          ROUTINE
    !|
    !| This routine is used to keep all displays and control templates current.
    !|
    !IF window{Prop:AcceptAll} THEN EXIT.
    IF MainWindow{Prop:AcceptAll} THEN EXIT.
    
    DISPLAY()
    !ForceRefresh = False
XL_Setup                                                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel = Create(0, Create:OLE)
        Excel{PROP:Create} = 'Excel.Application'

        Excel{'Application.DisplayAlerts'}  = False         ! no alerts to the user (do you want to save the file?) ! MOVED 10 BDec 2001 John
        Excel{'Application.ScreenUpdating'} = excel:Visible ! False
        Excel{'Application.Visible'}        = excel:Visible ! False

        Excel{'Application.Calculation'}    = xlCalculationManual
        
        DO XL_GetOperatingSystem
        !-----------------------------------------------
    EXIT
XL_Finalize                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Application.Calculation'}    = xlCalculationAutomatic

        Excel{PROP:DEACTIVATE}
        !-----------------------------------------------
    EXIT
XL_AddSheet                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveWorkBook.Sheets("Sheet3").Select'}

        Excel{'ActiveWorkBook.Sheets.Add'}
        !-----------------------------------------------
    EXIT
XL_AddWorkbook               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Application.Workbooks.Add()'}

        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Title")'}            = CLIP(LOC:ProgramName)
        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Author")'}           = CLIP(LOC:UserName)
        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Application Name")'} = LOC:ApplicationName
        !-----------------------------------------------
        ! 4 Dec 2001 John
        ! Delete empty sheets

        Excel{'Sheets("Sheet2").Select'}
        Excel{'ActiveWindow.SelectedSheets.Delete'}

        Excel{'Sheets("Sheet1").Select'}
        !-----------------------------------------------
    EXIT
XL_SaveAllWorksheets                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOOP WHILE Excel{'WorkBooks.Count'} > 0
            Excel{'ActiveWorkBook.Save()'}
            Excel{'ActiveWorkBook.Close()'}
        END !LOOP
        !-----------------------------------------------
    EXIT
XL_DropAllWorksheets                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOOP WHILE Excel{'WorkBooks.Count'} > 0
            Excel{'ActiveWorkBook.Close(' & FALSE & ')'}
        END !LOOP
        !-----------------------------------------------
    EXIT
XL_AddComment                ROUTINE
    DATA
xlComment CSTRING(20)
    CODE
        !-----------------------------------------------
        Excel{'Selection.AddComment("' & CLIP(excel:CommentText) & '")'}

!        xlComment{'Shape.IncrementLeft'} = 127.50
!        xlComment{'Shape.IncrementTop'}  =   8.25
!        xlComment{'Shape.ScaleWidth'}    =   1.76
!        xlComment{'Shape.ScaleHeight'}   =   0.69
        !-----------------------------------------------
    EXIT
XL_RowDown                   ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(1, 0).Select'}
        !-----------------------------------------------
    EXIT
XL_ColLeft                   ROUTINE
    DATA
CurrentCol LONG
    CODE
        !-----------------------------------------------
        CurrentCol = Excel{'ActiveCell.Col'}
        IF CurrentCol = 1
            EXIT
        END !IF

        Excel{'ActiveCell.Offset(0, -1).Select'}
        !-----------------------------------------------
    EXIT
XL_ColRight                  ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(0, 1).Select'}
        !-----------------------------------------------
    EXIT
XL_ColFirstSelect                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, Cells(' & Excel{'ActiveCell.Row'} & ', 1)).Select'}
        !Excel{'ActiveCell.Offset(0, -' & Excel{'ActiveCell.Column'} - 1 & ').Select'}
        !-----------------------------------------------
    EXIT
XL_ColFirst                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(0, -' & Excel{'ActiveCell.Column'} - 1 & ').Select'}
        !-----------------------------------------------
    EXIT
XL_SetLastCell                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, ActiveCell.SpecialCells(' & xlLastCell & ')).Select'}
        !-----------------------------------------------
    EXIT
XL_SetGrid                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlDiagonalDown & ').LineStyle'} = xlNone
        Excel{'Selection.Borders(' & xlDiagonalUp   & ').LineStyle'} = xlNone

        DO XL_SetBorder

        Excel{'Selection.Borders(' & xlInsideVertical & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlInsideVertical & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlInsideVertical & ').ColorIndex'} = xlAutomatic

        Excel{'Selection.Borders(' & xlInsideHorizontal & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlInsideHorizontal & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlInsideHorizontal & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorder                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_SetBorderLeft
        DO XL_SetBorderRight
        DO XL_SetBorderBottom
        DO XL_SetBorderTop
        !-----------------------------------------------
    EXIT
XL_SetBorderLeft                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeLeft & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeLeft & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeLeft & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderRight                           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeRight & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeRight & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeRight & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderBottom                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeBottom & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeBottom & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeBottom & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderTop                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeTop & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeTop & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeTop & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_HighLight                           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Interior.ColorIndex'}        = 6 ! YELLOW
        Excel{'Selection.Interior.Pattern'}           = xlSolid
        Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic
        !-----------------------------------------------
XL_SetTitle                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Interior.ColorIndex'}        = 15 ! GREY
        Excel{'Selection.Interior.Pattern'}           = xlSolid
        Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetWrapText                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.WrapText'}    = True
        !-----------------------------------------------
    EXIT
XL_SetGreyText                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Font.ColorIndex'}    = 16 ! grey
        !-----------------------------------------------
    EXIT
XL_SetBold                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Font.Bold'} = True
        !-----------------------------------------------
    EXIT
XL_FormatDate                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.NumberFormat'} = excel:DateFormat
        !-----------------------------------------------
    EXIT
XL_FormatNumber                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.NumberFormat'} = '#,##0.00'
        !-----------------------------------------------
    EXIT
XL_FormatTextBox                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.ShapeRange.IncrementLeft'} = 127.5
        Excel{'Selection.ShapeRange.IncrementTop'}  =   8.25
!        Excel{'Selection.ShapeRange.ScaleWidth'}    =  '1.76, ' & msoFalse & ', ' & msoScaleFromBottomRight
!        Excel{'Selection.ShapeRange.ScaleHeight'}   =  '0.69, ' & msoFalse & ', ' & msoScaleFromTopLeft
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentCentre            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlCenter
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentLeft              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlLeft
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentRight             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlRight
        !-----------------------------------------------
    EXIT
XL_VerticalAlignmentTop                 ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.VerticalAlignment'} = xlTop
        !-----------------------------------------------
    EXIT
XL_VerticalAlignmentCentre            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.VerticalAlignment'} = xlCenter
        !-----------------------------------------------
    EXIT
XL_SetColumnHeader                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_SetTitle
        DO XL_SetGrid
        DO XL_SetWrapText
        DO XL_SetBold
        !-----------------------------------------------
    EXIT
XL_SetColumn_Date                       ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = 'dd Mmm yyyy'

!        !Excel{'ActiveCell.FormulaR1C1'}  = LEFT(FORMAT(ret:Invoice_Date, @D8-))
        !-----------------------------------------------
    EXIT
XL_SetColumn_Number                     ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = '#,##0.00'
        !-----------------------------------------------
    EXIT
XL_SetColumn_Percent                    ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = '0.00%'
!        Excel{'ActiveCell.NumberFormat'} = CHR(64)       ! Text (AT) Sign
        !-----------------------------------------------
    EXIT
XL_SetColumn_Text                       ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'} = CHR(64) ! Text (AT) Sign
        !-----------------------------------------------
    EXIT
XL_DataSelectRange                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, ActiveCell.SpecialCells(' & xlLastCell & ')).Select'}
        !-----------------------------------------------
    EXIT
XL_DataHideDuplicates                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.AdvancedFilter Action:=' & xlFilterInPlace & ', Unique:=' & True }
        !-----------------------------------------------
    EXIT
XL_DataSortRange                            ROUTINE
    DATA
    CODE                     
        !-----------------------------------------------
        DO XL_DataSelectRange
        Excel{'ActiveWindow.ScrollColumn'} = 1
        Excel{'Selection.Sort Key1:=Range("' & LOC:Text & '"), Order1:=' & xlAscending & ', Header:=' & xlGuess & |
            ', OrderCustom:=1, MatchCase:=' & False & ', Orientation:=' & xlTopToBottom}
        !-----------------------------------------------
    EXIT
XL_DataAutoFilter                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.AutoFilter'}
        !-----------------------------------------------
    EXIT
XL_SetWorksheetLandscape                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.Orientation'}     = xlLandscape
!        Excel{'ActiveSheet.PageSetup.FitToPagesWide'} = 1
!        Excel{'ActiveSheet.PageSetup.FitToPagesTall'} = 9999
!        Excel{'ActiveSheet.PageSetup.Order'}          = xlOverThenDown
        !-----------------------------------------------
    EXIT
XL_SetWorksheetPortrait                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.Orientation'} = xlPortrait
        !-----------------------------------------------
    EXIT
XL_PrintTitleRows                               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.PrintTitleRows'} = CLIP(LOC:Text)
        !-----------------------------------------------
    EXIT
XL_GetCurrentCell                               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOC:Text = Excel{'Application.ConvertFormula( "RC", ' & xlR1C1 & ', ' & xlA1 & ')'}
        !-----------------------------------------------
    EXIT

XL_MergeCells                                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlCenter
        Excel{'Selection.VerticalAlignment'}   = xlBottom
        Excel{'Selection.MergeCells'}          = True
        !-----------------------------------------------
    EXIT

XL_GetOperatingSystem                                                          ROUTINE
    DATA
OperatingSystem     STRING(50)
tmpLen              LONG
LEN_OperatingSystem LONG
    CODE
        !-----------------------------------------------
        excel:OperatingSystem = 0.0    ! Default/error value
        OperatingSystem       = Excel{'Application.OperatingSystem'}
        LEN_OperatingSystem   = LEN(CLIP(OperatingSystem))

        LOOP x# = LEN_OperatingSystem TO 1 BY -1

            CASE SUB(OperatingSystem, x#, 1)
            OF '0' OROF'1' OROF '2' OROF '3' OROF '4' OROF '5' OROF '6' OROF '7' OROF '8' OROF '9' OROF '.'
                ! NULL
            ELSE
                tmpLen                = LEN_OperatingSystem-x#+1
                excel:OperatingSystem = CLIP(SUB(OperatingSystem, x#+1, tmpLen))

                EXIT
            END !CASE
        END !LOOP
        !-----------------------------------------------
    EXIT
!-----------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020617'&'0'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('GoodsReceivedFranchisesReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:GRNOTES.Open
  Relate:GRNOTESR.Open
  Relate:LOCATION.Open
  Relate:REPSCHAC.Open
  Relate:RETSALES.Open
  Access:USERS.UseFile
  Access:STOCK.UseFile
  Access:ORDERS.UseFile
  Access:ORDPARTS.UseFile
  Access:SUPPLIER.UseFile
  Access:RETSTOCK.UseFile
  Access:REPSCHCR.UseFile
  Access:REPSCHED.UseFile
  Access:REPSCHLG.UseFile
  SELF.FilesOpened = True
  BRWTradeAcc.Init(?List,Queue:Browse.ViewPosition,BRW6::View:Browse,Queue:Browse,Relate:TRADEACC,SELF)
  OPEN(MainWindow)
  SELF.Opened=True
  ! ========= Set Report Version =============
  Include('..\ReportVersion.inc')
  tmp:VersionNumber = 'Version Number: ' & Clip(tmp:VersionNumber) & '5003'
  ?ReportVersion{prop:Text} = tmp:VersionNumber
      LOC:ProgramName         = 'Goods Received Report'   !           Job=         Cust=
  
      LOC:StartDate      = DATE( MONTH(TODAY()), 1, YEAR(TODAY()) )
      LOC:EndDate        = TODAY() !
      LOC:DayLeawayCount = 90
  
      LOC:HeadAccount    = GetHeadAccount()
  
      excel:Visible      = False
      debug:Active       = False
  
      DO GetUserName2
  
      !Check to see if the report has been run with one of the auto switches  (DBH: 07-05-2004)
      If Command('/DAILY') Or Command('/WEEKLY') Or Command('/MONTHLY')
          LOC:UserName = 'AUTOMATED PROCEDURE'
          Automatic = True
      End !If Command('/DAILY') Or Command('/WEEKLY') Or Command('/MONTHLY')
  
      IF Automatic = TRUE
        CommandLine = CLIP(COMMAND(''))
        tmpPos = INSTRING('&', CommandLine)
        LOC:StartDate      = DATE( MONTH(CLIP(SUB(CommandLine, tmpPos + 1, 30))), 1, YEAR(CLIP(SUB(CommandLine, tmpPos + 1, 30))) )
        LOC:EndDate        = CLIP(SUB(CommandLine, tmpPos + 1, 30)) !
      END
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?LOC:EndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRWTradeAcc.Q &= Queue:Browse
  BRWTradeAcc.AddSortOrder(,tra:Account_Number_Key)
  BRWTradeAcc.AddLocator(BRW6::Sort0:Locator)
  BRW6::Sort0:Locator.Init(,tra:Account_Number,1,BRWTradeAcc)
  BRWTradeAcc.SetFilter('(tra:Account_Number <<> ''XXXRRC'' AND tra:SiteLocation <<> '''')')
  BIND('AccountNumberTag',AccountNumberTag)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRWTradeAcc.AddField(AccountNumberTag,BRWTradeAcc.Q.AccountNumberTag)
  BRWTradeAcc.AddField(tra:Account_Number,BRWTradeAcc.Q.tra:Account_Number)
  BRWTradeAcc.AddField(tra:SiteLocation,BRWTradeAcc.Q.tra:SiteLocation)
  BRWTradeAcc.AddField(tra:RecordNumber,BRWTradeAcc.Q.tra:RecordNumber)
      IF Automatic = TRUE
      !      LOC:StartDate = TODAY()
      !      LOC:EndDate = TODAY()
      !      DoAll = 'Y'
          DO DASBRW::7:DASTAGALL
  
          !Daily Report is not specified  (DBH: 07-05-2004)
          If Command('/DAILY')
              loc:StartDate = Today()
              loc:EndDate = Today()
          End !If Command('/DAILY')
  
          !Weekly Report from begning of week to end of week  (DBH: 07-05-2004)
  
          If Command('/WEEKLY')
              Loop day# = Today() To Today()-7 By -1
                  If day# %7 = 1
                      loc:StartDate = day#
                      Break
                  End !If day# %7 = 2
              End !Loop day# = Today() To Today()-7 By -1
              Loop day# = Today() To Today() + 7
                  IF day# %7 = 0
                      loc:EndDate = day#
                      Break
                  End !IF day# %7 = 0
              End !Loop day# = Today() To Today() + 7 By -1
          End !If Command('/WEEKLY')
          !Monthly - Start Date is 1st of Month, End Date is last of month  (DBH: 07-05-2004)
          If Command('/MONTHLY')
              loc:StartDate = Deformat('1/' & Month(Today()) & '/' & Year(Today()),@d6)
              loc:EndDate = Deformat('1/' & Month(Today()) + 1 & '/' & Year(Today()),@d6) - 1
          End !If Command('/MONTHLY')
  
  
          DO OKButton_Pressed
          !POST(Event:Closewindow)
          !Call email program!
    !      PUTINI('MAIN','InputDir',CLIP(Loc:Path),CLIP(PATH()) & '\AUTOEMAIL.INI')
    !      PUTINI('MAIN','ExportDir',CLIP(Loc:Path)&'EMAILED',CLIP(PATH()) & '\AUTOEMAIL.INI')
    !      RUN('EMAILDIR',1)
          POST(Event:CloseWindow)
      END
  BRWTradeAcc.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
  ! Inserting (DBH 31/08/2007) # 9125 - Is this an automatic report?
  If Command('/SCHEDULE')
      x# = Instring('%',Command(),1,1)
      Access:REPSCHED.ClearKey(rpd:RecordNumberKey)
      rpd:RecordNumber = Clip(Sub(Command(),x# + 1,20))
      If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
          !Found
          Access:REPSCHCR.ClearKey(rpc:ReportCriteriaKey)
          rpc:ReportName = rpd:ReportName
          rpc:ReportCriteriaType = rpd:ReportCriteriaType
          If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
              !Found
  
              If rpc:AllAccounts
                  DO DASBRW::7:DASTAGALL
              Else ! If rpc:AllAccounts
                  Access:REPSCHAC.Clearkey(rpa:AccountNumberKey)
                  rpa:REPSCHCRRecordNumber = rpc:RecordNumber
                  Set(rpa:AccountNumberKey,rpa:AccountNumberKey)
                  Loop ! Begin Loop
                      If Access:REPSCHAC.Next()
                          Break
                      End ! If Access:REPSCHAC.Next()
                      If rpa:REPSCHCRREcordNumber <> rpc:RecordNumber
                          Break
                      End ! If rpa:REPSCHCRREcordNumber <> rpc:RecordNumber
                      AccountNumberValue = rpa:AccountNumber
                      Add(AccountNumber_Queue)
                  End ! Loop
              End ! If rpc:AllAccounts
  
              Case rpc:DateRangeType
              Of 1 ! Today Only
                  loc:StartDate = Today()
                  loc:EndDate = Today()
              Of 2 ! 1st Of Month
                  loc:StartDate = Date(Month(Today()),1,Year(Today()))
                  loc:EndDate = Today()
              Of 3 ! Whole Month
  ! Changing (DBH 31/01/2008) # 9711 - Does not account for a change of year
  !                loc:StartDate = Date(Month(Today()) - 1, 1, Year(Today()))
  ! to (DBH 31/01/2008) # 9711
                  loc:EndDate = Date(Month(Today()),1,Year(Today())) - 1
                  loc:StartDate = Date(Month(loc:EndDate),1,Year(loc:EndDate))
  ! End (DBH 31/01/2008) #9711
              End ! Case rpc:DateRangeType
  
              Do OKButton_Pressed
              Post(Event:CloseWindow)
          Else ! If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
              !Error
          End ! If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
  
      Else ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
          !Error
      End ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
  
  End ! If Command('/SCHEDULE')
  ! End (DBH 31/08/2007) #9125
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  AccountNumberCount = RECORDS(AccountNumber_QUEUE) !DAS Taging
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List{Prop:Alrt,239} = SpaceKey
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:DEFAULTS.Close
    Relate:GRNOTES.Close
    Relate:GRNOTESR.Close
    Relate:LOCATION.Close
    Relate:REPSCHAC.Close
    Relate:RETSALES.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OkButton
          DO OKButton_Pressed
    OF ?CancelButton
       POST(Event:CloseWindow)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020617'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020617'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020617'&'0')
      ***
    OF ?StartPopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          LOC:StartDate = TINCALENDARStyle1(LOC:StartDate)
          Display(?LOC:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?EndPopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          LOC:EndDate = TINCALENDARStyle1(LOC:EndDate)
          Display(?LOC:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?LOC:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?EndPopCalendar)
      CYCLE
    END
  OF ?List
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::7:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

DateToString PROCEDURE(IN:DATE)! STRING
    CODE
        !-----------------------------------------------
        RETURN LEFT(FORMAT(IN:DATE, @D8))
        !-----------------------------------------------
FillLocation         PROCEDURE()! LONG ! BOOL
    CODE
        !------------------------------------------
        FREE(Location_Queue)

! Changing (DBH 04/09/2007) # 9125 - Why doesn't this just have a tag list of locations?
!        LOOP x# = 1 TO RECORDS(BRWTradeAcc.Q)
!            GET(BRWTradeAcc.Q, x#)
!
!            IF BRWTradeAcc.Q.AccountNumberTag = '*'
!                CLEAR(Location_Queue)
!                    locQ:Location = BRWTradeAcc.Q.tra:SiteLocation
!                ADD(Location_Queue, +locQ:Location)
!            END !IF 
!        END !LOOP
! to (DBH 04/09/2007) # 9125
        Loop x# = 1 To Records(AccountNumber_Queue)
            Get(AccountNumber_queue,x#)
            Access:TRADEACC.ClearKey(tra:Account_Number_Key)
            tra:Account_Number = AccountNumberValue
            If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                !Found
                Clear(Location_Queue)
                locQ:Location = tra:SiteLocation
                Add(Location_Queue,+locQ:Location)
            Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                !Error
            End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign

        End ! Loop x# = 1 To Records(AccountNumber_Queue)
! End (DBH 04/09/2007) #9125

        IF RECORDS(Location_Queue) > 0
            RETURN True
        ELSE
            RETURN False
        END !IF
        !------------------------------------------
GetHeadAccount      PROCEDURE()
    CODE
        RETURN GETINI('BOOKING', 'HeadAccount', '', '.\SB2KDEF.INI')
GetLocation         PROCEDURE(IN:Location)! LONG ! BOOL
    CODE
        !------------------------------------------
        CLEAR(Location_Queue)
            locQ:Location = IN:Location
        GET(Location_Queue, +locQ:Location)

        CASE ERRORCODE()
        OF 00 ! Found
            IF locQ:Location = IN:Location
                WriteDebug('GetLocation(' & CLIP(IN:Location) & ')FOUND')

                RETURN True
            ELSE
                WriteDebug('GetLocation(' & CLIP(IN:Location) & ')PARTIAL MATCH')

                RETURN False ! Partial Match
            END !IF

        OF 30 ! NOT Found
            WriteDebug('GetLocation(' & CLIP(IN:Location) & ')NOT FOUND')

            RETURN False

        ELSE
            CancelPressed = True

            WriteDebug('GetLocation(' & CLIP(IN:Location) & ')ERROR="' & ERROR() & '"')

            RETURN False
        END !CASE
        !------------------------------------------
LoadGRNOTES             PROCEDURE( IN:StartDate, IN:EndDate, IN:First )! LONG ! BOOL
    CODE
!        !------------------------------------------
!        IF IN:First = True
!            IN:First = False
!
!            Access:GRNOTES.ClearKey(grn:Date_Recd_Key)
!                grn:Goods_Received_Date = LOC:StartDate
!            SET(grn:Date_Recd_Key,grn:Date_Recd_Key)
!        END !IF
!        !------------------------------------------
!        IF Access:GRNOTES.Next() <> Level:Benign
!            RETURN False
!        END ! IF
!
!        IF grn:Goods_Received_Date > LOC:EndDate
!            RETURN False
!        END ! IF
!
        RETURN True
!        !------------------------------------------
LoadORDERS      PROCEDURE( IN:OrderNumber )! LONG ! BOOL
    CODE
        !Received_Key             KEY(ord:All_Received,                              ord:Order_Number),DUP,NOCASE
        !Order_Number_Key         KEY(                                               ord:Order_Number),NOCASE,PRIMARY
        !Printed_Key              KEY(ord:Printed,                                   ord:Order_Number),DUP,NOCASE
        !Supplier_Printed_Key     KEY(ord:Printed, ord:Supplier,                     ord:Order_Number),DUP,NOCASE
        !Supplier_Received_Key    KEY(ord:Supplier,                ord:All_Received, ord:Order_Number),DUP,NOCASE
        !Supplier_Key             KEY(ord:Supplier,                                  ord:Order_Number),DUP,NOCASE

!        Access:ORDERS.ClearKey(ord:Order_Number_Key)
!            ord:Order_Number = IN:OrderNumber
!        SET(ord:Order_Number_Key, ord:Order_Number_Key)
!
!        IF Access:ORDERS.NEXT() <> Level:Benign
!            RETURN False
!        END !IF
!
!        IF NOT ord:Order_Number = IN:OrderNumber
!            RETURN False
!        END !IF
!
        RETURN True
LoadORDPARTS        PROCEDURE(IN:Order_Number, IN:GoodsReceivedDate, IN:First)! LONG ! BOOL
    CODE
!        IF IN:First = True
!            IN:First = False
!            !Account_Number_Key       KEY(orp:Account_Number, orp:Part_Type,     orp:All_Received,                                                orp:Part_Number)                    ,DUP,NOCASE
!            !Received_Part_Number_Key KEY(                                       orp:All_Received,                                                orp:Part_Number)                    ,DUP,NOCASE
!            !Description_Key          KEY(orp:Order_Number,   orp:Description,                                                                    orp:Part_Number)                    ,DUP,NOCASE
!            !Part_Number_Key          KEY(orp:Order_Number,                                                                                       orp:Part_Number,    orp:Description),DUP,NOCASE
!            !Order_Number_Key         KEY(orp:Order_Number,                                                                                       orp:Part_Ref_Number )               ,DUP,NOCASE
!            !Order_Type_Received      KEY(orp:Order_Number,   orp:Part_Type,       orp:All_Received, orp:Part_Number,         orp:Description)                                        ,DUP,NOCASE
!            !Ref_Number_Key           KEY(                    orp:Part_Ref_Number,                   orp:Part_Number,         orp:Description)                                        ,DUP,NOCASE
!            !Allocated_Account_Key    KEY(                    orp:Part_Type,       orp:All_Received, orp:Allocated_To_Sale, orp:Account_Number,   orp:Part_Number)                    ,DUP,NOCASE
!            !Allocated_Key            KEY(                    orp:Part_Type,       orp:All_Received, orp:Allocated_To_Sale,                       orp:Part_Number)                    ,DUP,NOCASE
!            !record_number_key        KEY(                                                         orp:record_number),NOCASE,PRIMARY
!
!            Access:ORDPARTS.ClearKey(orp:Order_Number_Key)
!                orp:Order_Number = IN:Order_Number
!                !orp:Part_Ref_Number =
!            SET(orp:Order_Number_Key, orp:Order_Number_Key)
!        END !IF
!
!        LOOP WHILE Access:ORDPARTS.NEXT() <> Level:Benign
!            IF NOT orp:Order_Number = IN:Order_Number
!                RETURN False
!            END !IF
!
!            IF NOT orp:Date_Received = IN:GoodsReceivedDate
!                CYCLE
!            END !IF
!
!        END !LOOP

LocalUseEuro        Procedure(String    func:AccountNumber)
    Code
    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
    sub:Account_Number  = func:AccountNumber
    If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Found
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = sub:Main_Account_Number
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
            If tra:Use_Sub_Accounts = 'YES' and tra:Invoice_Sub_Accounts = 'YES'
                !Check whether Sub Account uses the Euro.
                !This will change the screen displays to reflect this
                If sub:EuroApplies
                    Return Level:Benign
                End !If sub:EuroApplied = 1
            Else !If tra:Use_Sub_Accounts = 'YES' and tra:Invoice_Sub_Accounts = 'YES'
                IF tra:EuroApplies
                    Return Level:Benign
                End !IF tra:EuroApplies
            End !If tra:Use_Sub_Accounts = 'YES' and tra:Invoice_Sub_Accounts = 'YES'
        Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
        
    Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
    Return Level:Fatal
NumberToColumn PROCEDURE(IN:Long)! STRING
FirstCol STRING(1)
Temp     LONG
    CODE
        !-----------------------------------------------
        Temp = IN:Long - 1

        FirstCol = Temp / 26
        IF FirstCol = 0
            RETURN CHR(Temp+65)
        END !IF

        RETURN CHR(FirstCol+64) & CHR( (Temp % 26)+65 )
        !-----------------------------------------------
UpdateSupplierQueue         PROCEDURE( IN:Supplier, IN:PartNumber, IN:Location )
    CODE
        WriteDebug('UpdateSupplierQueue(' & CLIP(IN:Supplier) & ', ' & CLIP(IN:PartNumber) & ', ' & CLIP(IN:Location) & ')')

        supQ:AccountNumber = IN:Supplier
        supQ:Part_Number   = IN:PartNumber
        supQ:Location      = IN:Location
        GET(SupplierQueue, +supQ:AccountNumber, +supQ:Part_Number, +supQ:Location)

        CASE ERRORCODE()
        OF 00 ! Found
            WriteDebug('UpdateSupplierQueue(FOUND)')
            ! NULL
        OF 30 ! NOT Found
            WriteDebug('UpdateSupplierQueue(ADD NEW)')

            CLEAR(SupplierQueue)
                supQ:AccountNumber = IN:Supplier
                supQ:CompanyName   = sup:Company_Name
                supQ:Part_Number   = IN:PartNumber
                supQ:Description   = res:Description
                supQ:Location      = IN:Location
            ADD(SupplierQueue, +supQ:AccountNumber)
        ELSE
            CancelPressed = True

            WriteDebug('UpdateSupplierQueue(ERROR="' & ERROR() & '")')

            RETURN
        END !CASE

        supQ:Quantity += res:QuantityReceived
        supQ:Amount   += res:Item_Cost * res:QuantityReceived
        PUT(SupplierQueue)

        WriteDebug('UpdateSupplierQueue(END)')
WriteColumn PROCEDURE( IN:String, IN:StartNewRow )
Temp STRING(255)
    CODE
        !-----------------------------------------------
        Temp = IN:String
        IF CLIP(Temp) = ''
            Temp = ''' '
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            IF IN:StartNewRow
                clip:Value = Temp
            ELSE
                clip:Value = CLIP(clip:Value) & '<09>' & Temp
            END !IF

            RETURN
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
WriteDebug      PROCEDURE( IN:Message )
    CODE
        !-----------------------------------------------
        IF NOT debug:Active
            RETURN
        END !IF

        debug:Count += 1

        PUTINI(CLIP(LOC:ProgramName), debug:Count, IN:Message, 'C:\Debug.ini')
        !-----------------------------------------------
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRWTradeAcc.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     AccountNumber_QUEUE.AccountNumberValue = tra:Account_Number
     GET(AccountNumber_QUEUE,AccountNumber_QUEUE.AccountNumberValue)
    IF ERRORCODE()
      AccountNumberTag = ''
    ELSE
      AccountNumberTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (AccountNumberTag = '*')
    SELF.Q.AccountNumberTag_Icon = 2
  ELSE
    SELF.Q.AccountNumberTag_Icon = 1
  END


BRWTradeAcc.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRWTradeAcc.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRWTradeAcc.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW6::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW6::RecordStatus=ReturnValue
  IF BRW6::RecordStatus NOT=Record:OK THEN RETURN BRW6::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     AccountNumber_QUEUE.AccountNumberValue = tra:Account_Number
     GET(AccountNumber_QUEUE,AccountNumber_QUEUE.AccountNumberValue)
    EXECUTE DASBRW::7:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW6::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW6::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW6::RecordStatus
  RETURN ReturnValue

