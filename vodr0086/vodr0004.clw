

   MEMBER('vodr0086.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('VODR0004.INC'),ONCE        !Local module procedure declarations
                     END


ErrorLog             PROCEDURE  (func:ErrorLog)       ! Declare Procedure
tmp:ErrorLog         CSTRING(255),STATIC
ErrorLog    File,Driver('BASIC'),Pre(errlog),Name(tmp:ErrorLog),Create,Bindable,Thread
Record                  Record
TheDate                 String(10)
TheTime                 String(10)
Line                    String(1000)
                        End
                    End
  CODE
!    ! Inserting (DBH 26/06/2006) #7386 - Do not create error log for manual report
!    If glo:Automatic = False
!        Return
!    End ! If glo:Automatic = Fals
!    ! End (DBH 26/06/2006) #7386

    tmp:ErrorLog = Clip(glo:IMEIExceptionPath) & '\SWAPIMEI.LOG'
    Open(ErrorLog)
    If Error()
        Create(ErrorLog)
        Open(ErrorLog)
    End ! If Error()

    If Clip(func:ErrorLog) = ''
        Clear(errlog:Record)
    Else ! If Clip(func:ErrorLog) = ''
        errlog:TheDate = Format(Today(),@d06)
        errlog:TheTime = Format(Clock(),@t01)
        errlog:Line    = Clip(func:ErrorLog)
    End ! If Clip(func:ErrorLog) = ''

    Add(ErrorLog)
    Close(ErrorLog)
