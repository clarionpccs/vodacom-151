

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01004.INC'),ONCE        !Local module procedure declarations
                     END


ExcludeFromInvoice   PROCEDURE  (f_chargetype)        ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    access:chartype.clearkey(cha:charge_type_key)
    cha:charge_type = f_chargetype
    If access:chartype.tryfetch(cha:charge_type_key) = Level:Benign
        !Found
        If CHA:ExcludeInvoice
            Return Level:Fatal
        End!If CHA:ExcludeInvoice
    Else! If access:chartype.tryfetch(cha:charge_type_key) = Level:Benign
        !Error
    End! If access:chartype.tryfetch(cha:charge_type_key) = Level:Benign
    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
PricingRoutine       PROCEDURE  (F_Type)              ! Declare Procedure
save_par_id          USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
save_wpr_id          USHORT,AUTO
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    !Return Values
    !1  = No account
    !2  = Ignore Pricing Structures
    !3  = Pricing Error

    Case f_type
        Of 'C'
            If job:account_number = ''
                job:parts_cost = 0
                job:labour_cost = 0
                Return 1    !Cannot run pricing structure
            End!If job:account_number = ''

            If job:chargeable_job = 'YES'
                If job:ignore_chargeable_charges = 'YES'
                    Return 2    !Charges ignored
                End!If job:ignore_chargeable_charges = 'YES'

                access:chartype.clearkey(cha:charge_type_key)
                cha:charge_type = job:charge_type
                If access:chartype.tryfetch(cha:charge_type_key) = Level:Benign
                    !Found
                    If cha:no_charge = 'YES'
                        job:labour_cost = 0
                        job:parts_cost  = 0
                    Else!If cha:no_charge = 'YES'
                        If cha:zero_parts = 'YES'
                            job:parts_cost = 0
                        Else!If cha:zero_parts = 'YES''
                            save_par_id = access:parts.savefile()
                            access:parts.clearkey(par:part_number_key)
                            par:ref_number  = job:ref_number
                            set(par:part_number_key,par:part_number_key)
                            loop
                                if access:parts.next()
                                   break
                                end !if
                                if par:ref_number  <> job:ref_number      |
                                    then break.  ! end if
                                job:parts_cost  += par:sale_cost * par:quantity
                            end !loop
                            access:parts.restorefile(save_par_id)
                        End!If cha:zero_parts = 'YES''
                    End!If cha:no_charge = 'YES'

                    access:subtracc.clearkey(sub:account_number_key)
                    sub:account_number  = job:account_number
                    If access:subtracc.tryfetch(sub:account_number_key) = Level:Benign
                        !Found
                        access:tradeacc.clearkey(tra:account_number_key)
                        tra:account_number  = sub:main_account_number
                        If access:tradeacc.tryfetch(tra:account_number_key) = Level:Benign
                            !Found

                        Else! If access:tradeacc.tryfetch(tra:account_number_key) = Level:Benign
                            !Error
                            Case Missive('Cannot find the Main Account ' & Clip(job:Account_Number) & ' attached to job ' & Clip(job:Ref_Number) & '.','ServiceBase 3g',|
                                           'mstop.jpg','/OK')
                                Of 1 ! OK Button
                            End ! Case Missive


                            job:parts_cost = 0
                            job:labour_cost = 0

                            Return 1

                        End! If access:tradeacc.tryfetch(tra:account_number_key) = Level:Benign

                    Else! If access:subtracc.tryfetch(sub:account_number_key) = Level:Benign
                        !Error
                        Case Missive('Cannot find the Sub Account ' & Clip(job:Account_Number) & ' attached to job ' & Clip(job:Ref_Number) & '.','ServiceBase 3g',|
                                       'mstop.jpg','/OK')
                            Of 1 ! OK Button
                        End ! Case Missive
                        job:parts_cost = 0
                        job:labour_cost = 0

                        Return 1
                    End! If access:subtracc.tryfetch(sub:account_number_key) = Level:Benign

                    If tra:ZeroChargeable   = 'YES'
                        job:parts_cost   = 0
                    End!If tra:ZeroChargeable   = 'YES'

                    If tra:Invoice_Sub_accounts = 'YES'
                        !try and find a sub account charge structure
                        access:subchrge.clearkey(suc:model_repair_type_key)
                        suc:account_number = job:account_number
                        suc:model_number   = job:model_number
                        suc:charge_type    = job:charge_type
                        suc:unit_type      = job:unit_type
                        suc:repair_type    = job:repair_type
                        if access:subchrge.tryfetch(suc:model_repair_type_key) = Level:Benign
                            job:labour_cost  = suc:cost
                        Else!if access:subchrge.tryfetch(suc:model_repair_type_key) = Level:Benign
                            !Can't find, try the main account charge structure
                            access:trachrge.clearkey(trc:account_charge_key)
                            trc:account_number = job:account_number
                            trc:model_number   = job:model_number
                            trc:charge_type    = job:charge_type
                            trc:unit_type      = job:unit_type
                            trc:repair_type    = job:repair_type
                            if access:trachrge.tryfetch(trc:account_charge_key) = Level:Benign
                                job:labour_cost  = trc:cost
                            Else!if access:trachrge.tryfetch(trc:account_charge_key) = Level:Benign
                                !Can't find structure, try the Default Charges
                                access:stdchrge.clearkey(sta:model_number_charge_key)
                                sta:model_number = job:model_number
                                sta:charge_type  = job:charge_type
                                sta:unit_type    = job:unit_type
                                sta:repair_type  = job:repair_type
                                if access:stdchrge.tryfetch(sta:model_number_charge_key) = Level:Benign
                                    job:labour_cost  = sta:cost
                                Else!if access:stdchrge.tryfetch(sta:model_number_charge_key) = Level:Benign
                                    !Can't find default structure
                                    job:parts_cost = 0
                                    job:labour_cost = 0

                                    Return 3
                                End!if access:stdchrge.tryfetch(sta:model_number_charge_key) = Level:Benign
                            End!if access:trachrge.tryfetch(trc:account_charge_key) = Level:Benign
                        End!if access:subchrge.tryfetch(suc:model_repair_type_key) = Level:Benign
                    Else!If tra:Invoice_Sub_accounts = 'YES'
                        !Try and find a Trade Charge Structure
                        access:trachrge.clearkey(trc:account_charge_key)
                        trc:account_number = sub:main_account_number
                        trc:model_number   = job:model_number
                        trc:charge_type    = job:charge_type
                        trc:unit_type      = job:unit_type
                        trc:repair_type    = job:repair_type
                        if access:trachrge.tryfetch(trc:account_charge_key) = Level:Benign
                            job:labour_cost  = trc:cost
                        Else!if access:trachrge.tryfetch(trc:account_charge_key) = Level:Benign
                            !Can't find structure, try the Default Charges
                            access:stdchrge.clearkey(sta:model_number_charge_key)
                            sta:model_number = job:model_number
                            sta:charge_type  = job:charge_type
                            sta:unit_type    = job:unit_type
                            sta:repair_type  = job:repair_type
                            if access:stdchrge.tryfetch(sta:model_number_charge_key) = Level:Benign
                                job:labour_cost  = sta:cost
                            Else!if access:stdchrge.tryfetch(sta:model_number_charge_key) = Level:Benign
                                !Can't find default structure
                                job:parts_cost = 0
                                job:labour_cost = 0

                                Return 3
                            End!if access:stdchrge.tryfetch(sta:model_number_charge_key) = Level:Benign
                        End!if access:trachrge.tryfetch(trc:account_charge_key) = Level:Benign

                    End!If tra:Invoice_Sub_accounts = 'YES'
                Else! If access:chartype.tryfetch(cha:charge_type_key) = Level:Benign
                    !Error
                    job:parts_cost = 0
                    job:labour_cost = 0
                    Return 3    !Error
                End! If access:chartype.tryfetch(cha:charge_type_key) = Level:Benign
            End!If job:chargeble_job = 'YES'
        Of 'W'
    End!Case f_type
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
FullAccess           PROCEDURE  (func:AccessNumber,func:Password) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    If func:AccessNumber = 0
        Return Level:Fatal
    End !If func:AccessNumber = 0

    day#    = Sub(func:AccessNumber,1,5)
    time#   = Sub(func:AccessNumber,6,20)

    If func:Password <> INT(((day# * 3)/23) + ((time# * 2)/7))
        Return Level:Fatal
    End!If func:Password <> INT((((day#*3/23) + ((time#*2)/7)))

    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
PartsChanging        PROCEDURE  (func:OldPartNumber,func:NewPartNumber,func:OldDescription,func:NewDescription,func:OldSaleCost,func:NewSaleCost,func:OldPurchaseCost,func:NewPurchaseCost,func:OldRetailCost,func:NewRetailCost,func:OShelfLoc,func:NShelfLoc,func:OSecLoc,func:NSecLoc) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
save_loc_id          USHORT,AUTO
save_cwp_id          USHORT,AUTO
save_ccp_id          USHORT,AUTO
save_stm_id          USHORT,AUTO
save_sto_id          USHORT,AUTO
pos                  STRING(255)
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
! Moving Bar Window
RejectRecord         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO

Progress:Thermometer BYTE
ProgressWindow WINDOW('Progress...'),AT(,,164,64),FONT('Tahoma',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER,FONT('Tahoma',8,,)
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER,FONT('Tahoma',8,,)
     END
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    RecordsPerCycle     = 25
    RecordsProcessed    = 0
    PercentProgress     = 0
    Setcursor(cursor:wait)
    Open(ProgressWindow)
    Progress:Thermometer    = 0
    ?Progress:PctText{prop:text} = '0% Completed'
    ?Progress:UserString{prop:text} = 'Updating Stock'

    RecordsToProcess = Records(LOCATION)

    Save_loc_ID = Access:LOCATION.SaveFile()
    Set(loc:Location_Key)
    Loop
        If Access:LOCATION.NEXT()
           Break
        End !If

        Do GetNextRecord2

        Save_sto_ID = Access:STOCK.SaveFile()
        Access:STOCK.ClearKey(sto:Location_Key)
        sto:Location    = loc:Location
        sto:Part_Number = func:OldPartNumber
        Set(sto:Location_Key,sto:Location_Key)
        Loop
            If Access:STOCK.NEXT()
               Break
            End !If
            If sto:Location    <> loc:Location      |
            Or sto:Part_Number <> func:OldPartNumber      |
                Then Break.  ! End If

            If func:OldPartNumber <> func:NewPartNumber
                pos = Position(sto:Location_Key)
                sto:Part_Number = func:NewPartNumber
                Access:STOCK.Update()
                Reset(sto:Location_Key,pos)
            End!If func:OldPartNumber <> func:NewPartNumber

            If func:OldDescription <> func:NewDescription
                sto:Description = func:NewDescription
            End!If func:OldDescription <> func:NewDescription

            If GETINI('STOCK','UseAveragePurchaseCost',,CLIP(PATH())&'\SB2KDEF.INI') <> 1

                If func:OldSaleCost <> func:NewSaleCost
                    sto:Sale_Cost   = func:NewSaleCost
                End!If func:OldSaleCost <> func:NewSaleCost

                If func:OldPurchaseCost <> func:NewPurchaseCost
                    sto:Purchase_Cost   = func:NewPurchaseCost
                End!If func:OldPurchaseCost <> func:NewPurchaseCost

                If func:OldRetailCost <> func:NewRetailCost
                    sto:Retail_Cost     = func:NewRetailCost
                End!If func:OldRetailCost <> func:NewRetailCost

            End !If GETINI('STOCK','UseAveragePurchaseCost',,CLIP(PATH())&'\SB2KDEF.INI') <> 1

            If func:OShelfLoc <> func:NShelfLoc
                sto:Shelf_Location  = func:NShelfLoc
            End !If func:OShelfLoc <> func:NShelfLoc

            If func:OSecLoc <> func:NSecLoc
                sto:Second_Location = func:NSecLoc
            End !If func:OSecLoc <> func:NSecLoc

            Access:STOCK.Update()

            get(stohist,0)
            if access:stohist.primerecord() = level:benign
                shi:ref_number           = sto:ref_number
                access:users.clearkey(use:password_key)
                use:password              = glo:password
                access:users.fetch(use:password_key)
                shi:user                  = use:user_code
                shi:date                 = today()
                shi:transaction_type     = 'ADD'
                shi:despatch_note_number = ''
                shi:job_number           = ''
                shi:quantity             = 0
                shi:purchase_cost        = STO:PURCHASE_COST
                shi:sale_cost            = STO:SALE_COST
                shi:retail_cost          = STO:RETAIL_COST
                shi:information          = 'PREVIOUS DETAILS:-'
                If func:OldPartNumber <> func:NewPartNumber
                    shi:Information = Clip(shi:Information) & '<13,10>PART NUMBER: ' & CLip(func:OldPartNumber)
                End!If func:OldPartNumber <> func:NewPartNumber
                If func:OldDescription <> func:NewDescription
                    shi:Information = Clip(shi:Information) & '<13,10>DESCRIPTION: ' & CLip(func:OldDescription)
                End!If func:OldDescription <> func:NewDescriptin
                If GETINI('STOCK','UseAveragePurchaseCost',,CLIP(PATH())&'\SB2KDEF.INI') <> 1
                    If func:OldPurchaseCost <> func:NewPurchaseCost
                        shi:Information = Clip(shi:Information) & '<13,10>PURCHASE COST: ' & CLip(func:OldPurchaseCost)
                    End!If func:OldPurchaseCost <> func:NewPurchaseCost
                    If func:OldSaleCost <> func:NewSaleCost
                        shi:Information = Clip(shi:Information) & '<13,10>TRADE PRICE: ' & CLip(func:OldSaleCost)
                    End!If func:OldSaleCost <> func:NewSaleCost
                    If func:OldRetailCost <> func:NewRetailCost
                        shi:Information = Clip(shi:Information) & '<13,10>RETAIL PRICE: ' & CLip(func:OldRetailCost)
                    End!If func:OldRetailCost <> func:NewRetailCost
                End !If GETINI('STOCK','UseAveragePurchaseCost',,CLIP(PATH())&'\SB2KDEF.INI') <> 1
                If func:OShelfLoc <> func:NShelfLoc
                    shi:Information = Clip(shi:Information) & '<13,10>RETAIL PRICE: ' & CLip(func:OShelfLoc)
                End !If func:OShelfLoc <> func:NShelfLoc
                If func:OSecLoc <> func:NSecLoc
                    shi:Information = Clip(shi:Information) & '<13,10>RETAIL PRICE: ' & CLip(func:OSecLoc)
                End !If func:OSecLoc <> func:NSecLoc

                shi:notes                = 'DETAILS CHANGED'
                if access:stohist.insert()
                   access:stohist.cancelautoinc()
                end
            end!if access:stohist.primerecord() = level:benign

            Save_stm_ID = Access:STOMODEL.SaveFile()
            Access:STOMODEL.ClearKey(stm:Mode_Number_Only_Key)
            stm:Ref_Number   = sto:Ref_Number
            Set(stm:Mode_Number_Only_Key,stm:Mode_Number_Only_Key)
            Loop
                If Access:STOMODEL.NEXT()
                   Break
                End !If
                If stm:Ref_Number   <> sto:Ref_Number      |
                    Then Break.  ! End If
                If func:OldPartNumber <> func:NewPartNumber
                    stm:Part_Number = func:NewPartNumber
                End!If func:OldPartNumber <> func:NewPartNumber

                If func:OldDescription <> func:NewDescription
                    stm:Description = func:NewDescription
                End!If func:OldDescription <> func:NewDescription

                Access:STOMODEL.Update()
            End !Loop
            Access:STOMODEL.RestoreFile(Save_stm_ID)


        End !Loop
        Access:STOCK.RestoreFile(Save_sto_ID)

    End !Loop
    Access:LOCATION.RestoreFile(Save_loc_ID)

    Progress:Thermometer    = 0
    RecordsPerCycle     = 25
    RecordsProcessed    = 0
    PercentProgress     = 0

    ?Progress:UserString{prop:text} = 'Updating Chargeable Common Faults'

    RecordsToProcess = Records(COMMONCP)

    Save_ccp_ID = Access:COMMONCP.SaveFile()
    Access:COMMONCP.ClearKey(ccp:PartNumberKey)
    ccp:Part_Number = func:OldPartNumber
    Set(ccp:PartNumberKey,ccp:PartNumberKey)
    Loop
        If Access:COMMONCP.NEXT()
           Break
        End !If
        Do GetNextRecord2
        If ccp:Part_Number <> func:OldPartNumber      |
            Then Break.  ! End If
        If func:OldPartNumber <> func:NewPartNumber
            pos = Position(ccp:PartNumberKey)
            ccp:Part_Number = func:NewPartNumber
            Access:COMMONCP.Update()
            Reset(ccp:PartNumberKey,pos)
        End!If func:OldPartNumber <> func:NewPartNumber

        If func:OldDescription <> func:NewDescription
            ccp:Description = func:NewDescription
        End!If func:OldDescription <> func:NewDescription
        If GETINI('STOCK','UseAveragePurchaseCost',,CLIP(PATH())&'\SB2KDEF.INI') <> 1
            If func:OldSaleCost <> func:NewSaleCost
                ccp:Sale_Cost   = func:NewSaleCost
            End!If func:OldSaleCost <> func:NewSaleCost

            If func:OldPurchaseCost <> func:NewPurchaseCost
                ccp:Purchase_Cost   = func:NewPurchaseCost
            End!If func:OldPurchaseCost <> func:NewPurchaseCost
        End !If GETINI('STOCK','UseAveragePurchaseCost',,CLIP(PATH())&'\SB2KDEF.INI') <> 1
        Access:COMMONCP.Update()
    End !Loop
    Access:COMMONCP.RestoreFile(Save_ccp_ID)

    ?Progress:UserString{prop:text} = 'Updating Warranty Common Faults'
    Progress:Thermometer    = 0
    RecordsPerCycle     = 25
    RecordsProcessed    = 0
    PercentProgress     = 0

    RecordsToProcess = Records(COMMONWP)

    Save_cwp_ID = Access:COMMONWP.SaveFile()
    Access:COMMONWP.ClearKey(cwp:PartNumberKey)
    cwp:Part_Number = func:OldPartNumber
    Set(cwp:PartNumberKey,cwp:PartNumberKey)
    Loop
        If Access:COMMONWP.NEXT()
           Break
        End !If
        If cwp:Part_Number <> func:OldPartNumber      |
            Then Break.  ! End If
        Do GetNextRecord2
        If func:OldPartNumber <> func:NewPartNumber
            pos = Position(cwp:PartNumberKey)
            cwp:Part_Number = func:NewPartNumber
            Access:COMMONWP.Update()
            Reset(cwp:PartNumberKey,pos)
        End!If func:OldPartNumber <> func:NewPartNumber

        If func:OldDescription <> func:NewDescription
            cwp:Description = func:NewDescription
        End!If func:OldDescription <> func:NewDescription

        If GETINI('STOCK','UseAveragePurchaseCost',,CLIP(PATH())&'\SB2KDEF.INI') <> 1
            If func:OldSaleCost <> func:NewSaleCost
                cwp:Sale_Cost   = func:NewSaleCost
            End!If func:OldSaleCost <> func:NewSaleCost

            If func:OldPurchaseCost <> func:NewPurchaseCost
                cwp:Purchase_Cost   = func:NewPurchaseCost
            End!If func:OldPurchaseCost <> func:NewPurchaseCost
        End !If GETINI('STOCK','UseAveragePurchaseCost',,CLIP(PATH())&'\SB2KDEF.INI') <> 1
        Access:COMMONWP.Update()
    End !Loop
    Access:COMMONWP.RestoreFile(Save_cwp_ID)



    Setcursor()
    Close(ProgressWindow)
GetNextRecord2      Routine
  RecordsProcessed += 1
  RecordsThisCycle += 1
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
    IF PercentProgress <> Progress:Thermometer THEN
      Progress:Thermometer = PercentProgress
      ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & '% Completed'
      DISPLAY()
    END
  END
EndPrintRun         Routine
    Progress:Thermometer = 100
    ?Progress:PctText{Prop:Text} = '100% Completed'
    Close(ProgressWindow)
    DISPLAY()
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
CompulsoryFieldCheck PROCEDURE  (func:Type)           ! Declare Procedure
save_taf_id          USHORT,AUTO
save_maf_id          USHORT,AUTO
save_par_id          USHORT,AUTO
save_wpr_id          USHORT,AUTO
tmp:ForceFaultCodes  BYTE(0)
tmp:FoundRequestedParts BYTE(0)
tmp:FoundOrderedParts BYTE(0)
tmp:FoundCommonFault BYTE(0)
save_aud_id          USHORT,AUTO
save_map_id          USHORT
Skip_non_Main        BYTE(0)
tmp:KeyRepair        BYTE(0)
tmp:FaultCode        STRING(255),DIM(20)
LocalDefaultLabour   REAL
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
local                      CLASS
ValidateFaultCodes             Procedure(),Byte
OutFaultPart                   Procedure(Long func:FieldNumber, String func:JobType),Byte
                           END ! local   CLASS
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
!Main Process
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    error# = 0
    glo:ErrorText = ''


    ! Inserting (DBH 09/11/2007) # 9278 - Save the fault codes in a "easy to use" form
    tmp:FaultCode[1] = job:Fault_Code1
    tmp:FaultCode[2] = job:Fault_Code2
    tmp:FaultCode[3] = job:Fault_Code3
    tmp:FaultCode[4] = job:Fault_Code4
    tmp:FaultCode[5] = job:Fault_Code5
    tmp:FaultCode[6] = job:Fault_Code6
    tmp:FaultCode[7] = job:Fault_Code7
    tmp:FaultCode[8] = job:Fault_Code8
    tmp:FaultCode[9] = job:Fault_Code9
    tmp:FaultCode[10] = job:Fault_Code10
    tmp:FaultCode[11] = job:Fault_Code11
    tmp:FaultCode[12] = job:Fault_Code12
    tmp:FaultCode[13] = wob:FaultCode13
    tmp:FaultCode[14] = wob:FaultCode14
    tmp:FaultCode[15] = wob:FaultCode15
    tmp:FaultCode[16] = wob:FaultCode16
    tmp:FaultCode[17] = wob:FaultCode17
    tmp:FaultCode[18] = wob:FaultCode18
    tmp:FaultCode[19] = wob:FaultCode19
    tmp:FaultCode[20] = wob:FaultCode20
    ! End (DBH 09/11/2007) #9278

    If ForceTransitType(func:Type)
        If job:Transit_Type = ''
            error# = 1
            glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Transit Type Missing.'
        End!If job:Transit_Type = ''
    End !If ForceTransitType(func:Type)

    If job:POP = 'REJ' And job:Warranty_Job = 'YES'
        error# = 1
        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Cannot be a warranty job, it has been rejected (no POP).'
    End !If job:POP = 'REJ' And job:Warranty_Job = 'YES'

    If ForceMobileNumber(func:Type)
        If job:Mobile_Number = ''
            error# = 1
            glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Mobile Number Missing.'
        End!If job:Mobile_Number = ''
    End !If ForceMobileNumber(func:Type)



    ! Inserting (DBH 07/12/2007) # 9491 - Check if an SMS alert number has been filled in
    Access:JOBSE2.Clearkey(jobe2:RefNumberKey)
    jobe2:RefNumber = job:Ref_Number
    If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
        If jobe2:SMSNotification And jobe2:SMSAlertNumber = ''
            If job:Mobile_Number <> ''
                ! May as well use the mobile number if it exists (DBH: 07/12/2007)
                jobe2:SMSAlertNumber = job:Mobile_Number
                Access:JOBSE2.TryUpdate()
            Else ! If job:Mobile_Number <> ''
                Error# = 1
                glo:ErrorText = Clip(glo:ErrorText) & '<13,10>SMS Alert Number Missing.'
            End ! If job:Mobile_Number <> ''
        End ! If jobe2:SMSNotification And jobe2:SMSAlertNumber = ''

        If jobe2:CourierWaybillNumber = ''
            ! #11880 The compulsory-ness of the waybill is determined by the Transit Type setup. (Bryan: 05/05/2011)
            Access:TRANTYPE.Clearkey(trt:Transit_Type_Key)
            trt:Transit_Type = job:Transit_Type
            IF (Access:TRANTYPE.TryFetch(trt:Transit_Type_Key) = Level:Benign)
                ! #11880 If comp at booking, always compulsory, otherwise check at completion (Bryan: 05/05/2011)
                IF (trt:WaybillComBook = 1 OR |
                        (func:Type = 'C' And trt:WaybillComComp = 1))
                    Error# = 1
                    glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Courier Waybill Number Missing.'
                END
            END ! IF (Access:TRANTYPE.TryFetch(trt:Transit_Type_Key) = Level:Benign)
        END ! If jobe2:CourierWaybillNumber = ''

! #11880 The compulsory-ness of the waybill is determined by the Transit Type setup. (Bryan: 05/05/2011)
    !        ! Inserting (DBH 24/01/2008) # 9612 - Check for waybill courier number
    !        If job:Transit_Type = 'ARRIVED FRANCHISE'
    !            If jobe2:CourierWaybillNumber = ''
    !                If SecurityCheck('AMEND COURIER WAYBILL NUMBER') = 0
    !                    ! Only force field if the user has access to change it
    !                    Error# = 1
    !                    glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Courier Waybill Number Missing.'
    !                End ! If func:Type = 'B' Or SecurityCheck('AMEND COURIER WAYBILL NUMBER') = 0
    !            End ! If jobe2:CourierWaybillNumber = ''
    !        End ! If job:Transit_Type = 'ARRIVED FRANCHISE'
    !        ! End (DBH 24/01/2008) #9612

        !Added by Paul 05/10/2010 - Log no 11691
        If job:Chargeable_Job = 'YES' then
            If func:Type = 'C' then
                !we need to check of the default labour cost is now lower than the labour cost set on the job
                Do DefaultLabourCosts
                !now we have the default labour cost - is it less than the labour cost entered
                ! #11773 Only check if ignoring costs. (Bryan: 28/10/2010)
                If jobe:RRCCLabourCost > LocalDefaultLabour AND jobe:IgnoreRRCChaCosts = 1 then
                    !labour costs need to be changed
                    Error# = 1
                    glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Override Labour Cost is Higher than Default Cost.'
                End
            End
        End
    End ! If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
    ! End (DBH 07/12/2007) #9491

    If ForceModelNumber(func:Type)
        If job:Model_Number = ''
            error# = 1
            glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Model Number Missing.'
        End!If job:Model_Number = ''
    End !If ForceModelNumber(func:Type)

    If ForceUnitType(func:Type)
        If job:Unit_Type = ''
            error# = 1
            glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Unit Type Type Missing.'
        End!If job:Unit_Type = ''
    End !If ForceUnitType(func:Type)

    If ForceColour(func:Type)
        If job:Colour = ''
            error# = 1
            If Clip(GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
                glo:ErrorText = Clip(glo:ErrorText) & '<13,10>' & Clip(GETINI('RENAME','ColourName',,CLIP(PATH())&'\SB2KDEF.INI')) & ' Missing.'
            Else !If Clip(GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
                glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Colour Missing'
            End !Clip(GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
        End!If job:Unit_Type = ''
    End !If ForceColour(func:Type)

    Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
    jbn:RefNumber = job:Ref_Number
    Access:JOBNOTES.TryFetch(jbn:RefNumberKey)

    If ForceFaultDescription(func:Type)
        If jbn:Fault_Description = ''
            error# = 1
            glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Fault Description Missing.'
        End!If jbn:Fault_Description = ''
    End !If ForceFaultDescription(func:Type)

    Access:MANUFACT.ClearKey(man:Manufacturer_Key)
    man:Manufacturer = job:Manufacturer
    If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        !Found
        If man:UseInvTextForFaults = FALSE
            If ~man:QAAtCompletion And job:QA_Passed <> 'YES'
                If ForceInvoiceText(func:Type)
                    IF jbn:Invoice_Text = ''
                        error# = 1
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Invoice Text Missing.'
                    End!IF jbn:Invoice_Text = ''
                End !If ForceInvoiceText(func:Type)
            END
         ELSE
           !Check to see if any havebeen added to the file?!
            If ForceInvoiceText(func:Type)
                fail# = 0
                Access:ReptyDef.ClearKey(rtd:ManRepairTypeKey)
                rtd:Manufacturer = job:manufacturer
                rtd:Repair_Type = job:Repair_Type_Warranty
                IF Access:ReptyDef.Fetch(rtd:ManRepairTypeKey)
                  !Error!
                ELSE
                  IF rtd:Warranty = 'YES'
                    IF rtd:ExcludeFromEDI = TRUE
                      !Fail Outfault!
                      fail# = 1
                    END
                  END
                END

                JOFL# = 0
                Access:JobOutFL.ClearKey(joo:JobNumberKey)
                joo:JobNumber = job:ref_number
                SET(joo:JobNumberKey,joo:JobNumberKey)
                LOOP
                 IF Access:JobOutFL.Next()
                   BREAK
                 END
                 IF joo:JobNumber <> job:ref_number
                   BREAK
                 END
                 !Do not include free text Out Faults
                 If job:Warranty_Job = 'YES' AND fail# = 0
                   If joo:FaultCode = '0'
                      !JOFL# = 0
                      CYCLE
                   End !If joo:FaultCode <> 0

                   IF SUB(joo:FaultCode,1,1) = '*'
                      !JOFL# = 0
                      CYCLE
                   End !If joo:FaultCode <> 0
                 END !If job:Warranty_Job = 'YES' AND fail# = 0
                 jofl# = 1
                END
                IF JOFL# = 0
                    Access:MANFAUPA.ClearKey(map:MainFaultKey)
                    map:Manufacturer = job:Manufacturer
                    map:MainFault    = 1
                    If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
                        !Found
                        If job:Chargeable_Job = 'YES'
                            If Local.OutFaultPart(map:Field_Number,'C')
                                JOFL# = 1
                            End !If Local.OutFaultPart(map:Field_Number,'C')
                        End !If job:Chargeable_Job = 'YES'

                        If job:Warranty_Job = 'YES' And JOFL# = 0
                            If Local.OutFaultPart(map:Field_Number,'W')
                                JOFL# = 1
                            End !If Local.OutFaultPart(map:Field_Number,'W')
                        End !If job:Warranty_Job = 'YES'

                    Else!If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>')
                    End!If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
                End !If JOFL#
                If JOFL# = 0
                    error# = 1
                    glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Out Faults Missing.'
                End!IF JOFL# = 0
           End !If ForceInvoiceText(func:Type)
        End
    END

    If ForceIMEI(func:Type)
        If job:ESN = ''
            error# = 1
            glo:ErrorText = Clip(glo:ErrorText) & '<13,10>I.M.E.I. Number Missing.'
        End!If job:ESN = ''
    End !If ForceIMEI(func:Type)

    If ForceMSN(job:Manufacturer,func:Type)
        If job:MSN = ''
            error# = 1
            glo:ErrorText = Clip(glo:ErrorText) & '<13,10>M.S.N. Missing.'
        End!If job:MSN = ''
    End !If ForceMSN(func:Type)

    !Check I.M.E.I. Number Length
    If CheckLengthNoError(job:ESN,job:Model_Number,'IMEI')
        Error# = 1
        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>I.M.E.I. Number Incorrect Length.'
    End !If CheckLength(job:ESN,job:Model_Number,'IMEI')

    If MSNRequired(job:Manufacturer) = Level:Benign
        If CheckLengthNoError(job:MSN,job:Model_Number,'MSN')
            Error# = 1
            glo:ErrorText = Clip(glo:ErrorText) & '<13,10>M.S.N. Incorrect Length.'
        End !If CheckLength(job:MSN,job:Model_Number,'MSN')
        If man:ApplyMSNFormat
            If CheckFaultFormat(job:MSN,man:MSNFormat)
                Error# = 1
                glo:ErrorText = Clip(glo:ErrorText) & '<13,10>M.S.N. Incorrect Format.'
            End !If CheckFaultFormat(job:MSN,man:MSNFormat)
        End !If man:ApplyMSNFormat
    End !If MSNRequired(job:Manufacturer) = Level:Benign

    If (def:Force_Job_Type = 'B' And func:Type = 'B') Or |
        (def:Force_Job_Type <> 'I' And func:Type = 'C')
        If job:Chargeable_Job <> 'YES' And job:Warranty_Job <> 'YES'
            error# = 1
            glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Job Type(s) Missing.'
        End!If job:Chargeable_Job <> 'YES' And job:Warranty_Job <> 'YES'
        If job:Chargeable_Job = 'YES' And job:Charge_Type = ''
            Error# = 1
            glo:ErrorText   = Clip(glo:ErrorText) & '<13,10>Chargeable Charge Type Missing.'
        End !If job:Chargeable_Job = 'YES' And job:Charge_Type = ''
        If job:Warranty_Job = 'YES' and job:Warranty_Charge_Type = ''
            Error# = 1
            glo:ErrorText   = Clip(glo:ErrorText) & '<13,10>Warranty Charge Type Missing.'
        End !If job:Warranty_Job = 'YES' and job:Warranty_Charge_Type = ''
    End!If def:Force_Job_Type = 'B'

    If (def:Force_Engineer = 'B' And func:Type = 'B') Or |
        (def:Force_Engineer <> 'I' And func:Type = 'C')
        If job:Engineer = '' And job:Workshop = 'YES'
            error# = 1
            glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Engineer Missing.'
        End!If job:Engineer = ''
    End!If def:Force_Engineer = 'B'

    If ForceRepairType(func:Type)
        If (job:Chargeable_Job = 'YES' And job:Repair_Type = '') Or |
            (job:Warranty_Job = 'YES' And job:Repair_Type_Warranty = '')
            error# = 1
            glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Repair Type(s) Missing.'
        Else
            !Check to see if the repair type is an "Exchange Fee" or "Handling Fee" one.
            If job:Chargeable_Job = 'YES'
                Access:REPTYDEF.ClearKey(rtd:ChaManRepairTypeKey)
                rtd:Manufacturer = job:Manufacturer
                rtd:Chargeable   = 'YES'
                rtd:Repair_Type  = job:Repair_Type
                If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                    !Found
                    If rtd:BER = 10
                        error# = 1
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Chargeable Repair Type is for "Exchange Fee".'
                    End !If rtd:BER = 10 or rtd:BER = 11
                    If rtd:BER = 11
                        error# = 1
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Chargeable Repair Type is for "Handling Fee".'
                    End !If rtd:BER = 11
                Else!If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                End!If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
            End !If job:Chargeable_Job = 'YES'
            If job:Warranty_Job = 'YES'
                Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
                rtd:Manufacturer = job:Manufacturer
                rtd:Warranty   = 'YES'
                rtd:Repair_Type  = job:Repair_Type_Warranty
                If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
                    !Found
                    If rtd:BER = 10
                        error# = 1
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Warranty Repair Type is for "Exchange Fee".'
                    End !If rtd:BER = 10 or rtd:BER = 11
                    If rtd:BER = 11
                        error# = 1
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Warranty Repair Type is for "Handling Fee".'
                    End !If rtd:BER = 11
                Else!If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                End!If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
            End !If job:Chargeable_Job = 'YES'
        End
    End!If def:Force_Repair_Type = 'B'

    If ForceAuthorityNumber(func:Type)
        If job:Authority_Number = ''
            error# = 1
            glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Authority Number Missing.'
        End
    Else
        If job:Chargeable_Job = 'YES'
            Access:CHARTYPE.Clearkey(cha:Warranty_Key)
            cha:Charge_Type = job:Charge_Type
            cha:Warranty    = 'NO'
            If Access:CHARTYPE.Tryfetch(cha:Warranty_Key) = Level:Benign
                !Found
                If cha:ForceAuthorisation
                    If job:Authority_Number = ''
                        error# = 1
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Authority Number Missing.'
                    End !If job:Authority_Number = ''
                End !If cha:ForceAuthorisation
            Else ! If Access:CHARTYPE.Tryfetch(cha:Warranty_Key) = Level:Benign
                !Error
            End !If Access:CHARTYPE.Tryfetch(cha:Warranty_Key) = Level:Benign
        End !If job:Chargeable_Job = 'YES'
    End !If ForceAuthorityNumber(func:Type)


    If ForceIncomingCourier(func:Type)
        If job:Incoming_Courier = ''
            error# = 1
            glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Incoming Courier Missing.'
        End!If job:Incoming_Courier = ''
    End !If ForceIncomingCourier(func:Type)

    If ForceCourier(func:Type)
        If job:Courier = ''
            error# = 1
            glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Courier Missing.'
        End!If job:Courier = ''
    End !If ForceCourier(func:Type)

    If ForceDOP(job:Transit_Type,job:Manufacturer,job:Warranty_Job,func:Type)
        If job:DOP = ''
            error# = 1
            glo:ErrorText = Clip(glo:ErrorText) & '<13,10>DOP Missing, required by Transit Type.'
        End !If job:DOP = ''
    End !If ForceDOP(job:Transit_Type,job:Manufacturer,job:Warranty_Job,func:Type)

    If ForceLocation(job:Transit_Type,job:Workshop,func:Type)
        If job:Location = ''
            error# = 1
            glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Location Missing, required by Transit Type.'
        End !If job:Location = ''
    End !If ForceLocation(job:Transit_Type,job:Workshop,func:Type)

    If ForceCustomerName(job:Account_Number,func:Type) = 1  !TB13307 - J - 04/07/14 - there are now three options 0 = hide, 1 = force, 2 = show but not force
         If job:Initial = '' And job:Title = '' And job:Surname = ''
            error# = 1
            glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Customer Name Missing.'
         End!If job:Initial = '' And job:Title = '' And job:Surname = ''
    End !If ForceCustomerName(job:Account_Number,func:Type)

    If ForcePostcode(func:type)
        If job:Postcode = ''
            error# = 1
            glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Postcode Missing.'
        End!If job:Postcode = ''
    End !If ForcePostcode(func:type)

    If ForceDeliveryPostcode(func:Type)
        If job:Postcode_Delivery = ''
            error# = 1
            glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Delivery Postcode Missing.'
        End!If job:Postcode_Delivery = ''
    End !If ForceDeliveryPostcode(func:Type)

    Error# = local.ValidateFaultCodes()

    !The Trade Fault codes, should be independent of Charge Types,
    !therefore, they are compulsory if the boxes are ticked.
    access:jobse.clearkey(jobe:refnumberkey)
    jobe:refnumber  = job:Ref_number
    If access:jobse.tryfetch(jobe:refnumberkey) = Level:Benign
      !Found

        access:subtracc.clearkey(sub:account_number_key)
        sub:account_number = job:account_number
        if access:subtracc.fetch(sub:account_number_key) = level:benign
            access:tradeacc.clearkey(tra:account_number_key)
            tra:account_number = sub:main_account_number
            If access:tradeacc.tryfetch(tra:account_number_Key) = Level:Benign
            End!If access:tradeacc.tryfetch(tra:account_number_Key) = Level:Benign
        End!if access:subtracc.fetch(sub:account_number_key) = level:benign
        If ForceNetwork(func:Type)
            If jobe:Network = ''
                Error# = 1
                glo:errortext = Clip(glo:errortext) & '<13,10>Network Missing.'
            End !If jobe:Network = ''
        End !If ForceNetwork(func:Type)

    Else! If access:jobse.tryfetch(jobe:refnumberkey                ) = Level:Benign
      !Error
    End! If access:jobse.tryfetch(jobe:refnumberkey                ) = Level:Benign

    !Check For Adjustments only at completion

    LookForParts# = 0
    If job:warranty_job = 'YES' And (func:Type = 'C' OR func:Type = 'T') ! #12363 Force if going to third party too (DBH: 15/02/2012)
        access:manufact.clearkey(man:manufacturer_key)
        man:manufacturer  = job:manufacturer
        If access:manufact.tryfetch(man:manufacturer_key) = Level:Benign
          !Found
            If man:forceparts
                LookForParts# = 1

            End!If man:forceparts


        Else! If access:manufact.tryfetch(man:manufacturer_key) = Level:Benign
          !Error
        End! If access:.tryfetch(man:manufacturer_key) = Level:Benign

        Access:REPTYDEF.ClearKey(rtd:ManRepairTypeKey)
        rtd:Manufacturer = job:Manufacturer
        rtd:Repair_Type  = job:Repair_Type_Warranty
        If Access:REPTYDEF.TryFetch(rtd:ManRepairTypeKey) = Level:Benign
            !Found
            If rtd:ForceAdjustment
                LookForParts# = 1
            End !If rtd:ForceAdjustment
        Else!If Access:REPTYDEF.TryFetch(rtd:ManRepairTypeKey) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End!If Access:REPTYDEF.TryFetch(rtd:ManRepairTypeKey) = Level:Benign


        If LookForParts#
            setcursor(cursor:wait)
            parts# = 0
            access:warparts.clearkey(wpr:part_number_key)
            wpr:ref_number  = job:ref_number
            set(wpr:part_number_key,wpr:part_number_key)
            loop
                if access:warparts.next()
                   break
                end !if
                if wpr:ref_number  <> job:ref_number      |
                    then break.  ! end if
                parts# = 1
                Break
            end !loop
            setcursor()
            If parts# = 0
                error# = 1
                glo:errortext = Clip(glo:errortext) & '<13,10>Warranty Spares Missing'
            End !If found# = 0
        End !If LookForParts#
    End!If job:warranty_job = 'YES'

    If (def:Force_Spares = 'B' And func:Type = 'B') Or |
        (def:Force_Spares <> 'I' And func:Type = 'C')
        If job:chargeable_job    = 'YES'
            setcursor(cursor:wait)
            parts# = 0
            access:parts.clearkey(par:part_number_key)
            par:ref_number  = job:ref_number
            set(par:part_number_key,par:part_number_key)
            loop
                if access:parts.next()
                   break
                end !if
                if par:ref_number  <> job:ref_number      |
                    then break.  ! end if
                parts# = 1
                Break
            end !loop
            setcursor()
            If parts# = 0
                error# = 1
                glo:errortext = Clip(glo:errortext) & '<13,10>Chargeable Spares Missing'
            End !If found# = 0
        End !If job:job_Type = 'CHARGEABLE'
    End!If def:Force_Spares = 'B'

    !Qa before Completion

    ! Set Key Repair to "1". Turn it off, if Key Repair Is Required.
    tmp:KeyRepair = 1
    Access:MANUFACT.ClearKey(man:Manufacturer_Key)
    man:Manufacturer = job:Manufacturer
    If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        !Found
        If man:UseQA And func:Type = 'C'
            If ~man:QAAtCompletion And job:QA_Passed <> 'YES'
                error# = 1
                glo:errortext = Clip(glo:errortext) & '<13,10>QA Has Not Been Passed'

            End !If man:QAAtCompletion And job:QA_Passed <> 'YES'
        End !If man:UseQA
        If man:KeyRepairRequired And (func:Type = 'C' OR func:Type = 'T') ! Check for Key Repair When Despatching To Third Party
            tmp:KeyRepair = 0
        End ! If man:KeyRepairRequired
    Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign

    !Parts Remaining
    tmp:FoundRequestedParts = 0
    tmp:FoundOrderedParts = 0

    !Also check if parts have not been removed,
    !if rapid stock is being used
    RestockPart# = 0


    CountParts# = 0
    setcursor(cursor:wait)
    save_wpr_id = access:warparts.savefile()
    access:warparts.clearkey(wpr:part_number_key)
    wpr:ref_number  = job:ref_number
    Set(wpr:part_number_key,wpr:part_number_key)
    Loop
        if access:warparts.next()
           break
        end !if
        if wpr:ref_number  <> job:ref_number      |
            then break.  ! end if
        !Do not validate a Exchange unit
        If wpr:Part_Number = 'EXCH'
            Cycle
        End !If wpr:Part_Number = 'EXCH'

        ! Inserting (DBH 23/11/2007) # 9568 - Only check key repair if there ARE parts attached
        CountParts# += 1
        ! End (DBH 23/11/2007) #9568

        !Bodge to make Fault Code 3 compulsory for warranty adjustments for Siemens
        If job:Manufacturer = 'SIEMENS' and func:Type = 'C'
            If wpr:Fault_Code3 = '' and wpr:Adjustment = 'YES'
                glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Fault Code 3 Missing On Warranty Adjustment'
            End !If wpr:Fault_Code3 = '' and wpr:Adjustment = 'YES'
        End !If job:Manufacturer = 'SIEMENS'

        If ForceFaultCodes(job:Chargeable_Job,job:Warranty_Job,|
                    job:Charge_Type,job:Warranty_Charge_Type,|
                    job:Repair_Type,job:Repair_Type_Warranty,'W')
            Do WarrantyPartFaultCodes
        Else
            ! This may be wrong, but if Fault Codes aren't required, don't check for Key Repair (DBH: 06/11/2007)
            tmp:KeyRepair = 1
        End!If tmp:ForceFaultCodes
        Access:STOCK.Clearkey(sto:Ref_Number_Key)
        sto:Ref_Number  = par:Part_Ref_Number
        If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            !Found

        Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            !Error
        End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
        If RapidLocation(sto:Location) & sto:Location <> ''
            If wpr:Status = 'RET'
                RestockPart# += 1
            End !If wpr:Status = 'RET'
        End !If GETINI('STOCK','UseRapidStockAllocation',,CLIP(PATH())&'\SB2KDEF.INI') = 1

        If wpr:pending_ref_number <> '' and wpr:Order_Number = ''
            tmp:FoundRequestedParts = 1
            Break
        End
        If wpr:order_number <> '' And wpr:date_received = ''
            tmp:FoundOrderedParts = 1
            Break
        End
    End !loop
    access:warparts.restorefile(save_wpr_id)
    setcursor()

! Changing (DBH 23/11/2007) # 9568 - Only check for Key Repair if there are parts attached
!    If tmp:KeyRepair = 0
! to (DBH 23/11/2007) # 9568
    If tmp:KeyRepair = 0 And CountParts# > 0
! End (DBH 23/11/2007) #9568
        Error# = 1
        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>None of the warranty parts have been marked as "Key Repair".'
    End ! If tmp:KeyRepair = 0

    If func:Type = 'C'
        setcursor(cursor:wait)
        save_par_id = access:parts.savefile()
        access:parts.clearkey(par:part_number_key)
        par:ref_number  = job:ref_number
        set(par:part_number_key,par:part_number_key)
        loop
            if access:parts.next()
               break
            end !if
            if par:ref_number  <> job:ref_number      |
                then break.  ! end if

            Access:STOCK.Clearkey(sto:Ref_Number_Key)
            sto:Ref_Number  = par:Part_Ref_Number
            If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                !Found

            Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                !Error
            End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign

            If par:Part_Number <> 'EXCH'
                If ForceFaultCodes(job:Chargeable_Job,job:Warranty_Job,|
                                    job:Charge_Type,job:Warranty_Charge_Type,|
                                    job:Repair_Type,job:Repair_Type_Warranty,'X')
                    Do ChargeablePartFaultCodes
                End !If ForceFaultCodes(job:Chargeable_Job,job:Warranty_Job,|
            End!If tmp:ForceFaultCodes
            If RapidLocation(sto:Location) & sto:Location <> ''
                If par:Status = 'RET'
                    RestockPart# += 1
                End !If wpr:Status = 'RET'
            End !If GETINI('STOCK','UseRapidStockAllocation',,CLIP(PATH())&'\SB2KDEF.INI') = 1

            If par:pending_ref_number <> '' and par:Order_Number = ''
                tmp:FoundRequestedParts = 1
                Break
            End
            If par:order_number <> '' And par:date_received = ''
                tmp:FoundOrderedParts = 1
                Break
            End

        end !loop
        access:parts.restorefile(save_par_id)
        setcursor()

        If tmp:FoundRequestedParts = 1
            error# = 1
            If def:SummaryOrders
                glo:errortext = Clip(glo:errortext) & '<13,10>There are Unreceived spares attached'
            Else !If def:SummaryOrders
                glo:errortext = Clip(glo:errortext) & '<13,10>There are Unordered spares attached'
            End !If def:SummaryOrders

        Elsif tmp:FoundOrderedParts = 1
            error# = 1
            glo:errortext = Clip(glo:errortext) & '<13,10>There are Unreceived spares attached'
        End

        If RestockPart#
            Error# = 1
            glo:ErrorText = Clip(glo:ErrorText) & '<13,10>There are spares that have NOT been returned to stock by Allocations'
        End !If RestockPart#
    End!If func:Type = 'C'


    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
    sub:Account_Number  = job:Account_Number
    If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Found
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = sub:Main_Account_Number
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
            If tra:Invoice_Sub_Accounts = 'YES'
                If sub:ForceOrderNumber and job:Order_Number = ''
                    error# = 1
                    glo:errorText   = Clip(glo:ErrorText) & '<13,10>Order Number Missing.'
                End !If tra:ForceOrderNumber
            Else !If tra:Invoice_Sub_Accounts
                If tra:ForceOrderNumber and job:Order_Number = ''
                    error# = 1
                    glo:errorText   = Clip(glo:ErrorText) & '<13,10>Order Number Missing.'
                End !If tra:ForceOrderNumber
            End !If tra:Invoice_Sub_Accounts
            If func:Type = 'C'
                If tra:ForceCommonFault
                    tmp:FoundCommonFault = 0
                    Save_aud_ID = Access:AUDIT.SaveFile()
                    Access:AUDIT.ClearKey(aud:Action_Key)
                    aud:Ref_Number = job:Ref_Number
                    aud:Action     = 'INSERTED COMMON FAULT'
                    Set(aud:Action_Key,aud:Action_Key)
                    Loop
                        If Access:AUDIT.NEXT()
                           Break
                        End !If
                        If aud:Ref_Number <> job:Ref_Number      |
                        Or aud:Action     <> 'INSERTED COMMON FAULT'      |
                            Then Break.  ! End If
                        tmp:FoundCommonFault = 1
                        Break
                    End !Loop
                    Access:AUDIT.RestoreFile(Save_aud_ID)
                    If tmp:FoundCommonFault = 0
                        error# = 1
                        glo:errortext = Clip(glo:errortext) & '<13,10>Common Fault Not Been Applied.'

                    End!If tmp:FoundCommonFault = 1
                End !If tra:ForceCommonFault
            End !If func:Type = 'C'

        Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign

    Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign

    If ForceAccessories(func:Type)
        If NoAccessories(job:Ref_Number)
            error# = 1
            glo:errortext = Clip(glo:errortext) & '<13,10>Job Accessories Missing.'
        End !If AccError# = 1
    End !Clip(GETINI('COMPULSORY','JobAccessories',,CLIP(PATH())&'\SB2KDEF.INI')) = 'B'


    If GETINI('ESTIMATE','ForceAccepted',,CLIP(PATH())&'\SB2KDEF.INI') = 1
        If job:Chargeable_Job = 'YES' and func:Type = 'C'
            If job:Estimate = 'YES' and job:Estimate_Accepted <> 'YES' And job:Estimate_Rejected <> 'YES'
                Error# = 1
                glo:errortext = Clip(glo:errortext) & '<13,10>Estimate has not been Accepted/Rejected.'
            End !If job:Estimate = 'YES' and (job:Estiamte_Accepted <> 'YES' Or job:Estimate_Rejected <> 'YES')
        End !If job:Chargeable_Job = 'YES'
    End !If GETINI('ESTIMATE','ForceAccepted',,CLIP(PATH())&'\SB2KDEF.INI') = 1


WarrantyPartFaultCodes        Routine
Data
local:FaultCode                  String(30),Dim(12)
Code
    Access:STOCK.Clearkey(sto:Ref_Number_Key)
    sto:Ref_Number = wpr:Part_Ref_Number
    If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign

    End ! If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign

    Clear(local:FaultCode)

    Access:MANFAUPA.Clearkey(map:Field_Number_Key)
    map:Manufacturer = job:Manufacturer
    map:Field_Number = 0
    Set(map:Field_Number_Key,map:Field_Number_Key)
    Loop
        If Access:MANFAUPA.Next()
            Break
        End ! If Access:MANFAUPA.Next()
        If map:Manufacturer <> job:Manufacturer
            Break
        End ! If map:Manufacturer <> job:Manufacturer
        If map:NotAvailable
            Cycle
        End ! If map:NotAvailable

        Case map:Field_Number
        Of 1
            local:FaultCode[map:Field_Number] = wpr:Fault_Code1
        Of 2
            local:FaultCode[map:Field_Number] = wpr:Fault_Code2
        Of 3
            local:FaultCode[map:Field_Number] = wpr:Fault_Code3
        Of 4
            local:FaultCode[map:Field_Number] = wpr:Fault_Code4
        Of 5
            local:FaultCode[map:Field_Number] = wpr:Fault_Code5
        Of 6
            local:FaultCode[map:Field_Number] = wpr:Fault_Code6
        Of 7
            local:FaultCode[map:Field_Number] = wpr:Fault_Code7
        Of 8
            local:FaultCode[map:Field_Number] = wpr:Fault_Code8
        Of 9
            local:FaultCode[map:Field_Number] = wpr:Fault_Code9
        Of 10
            local:FaultCode[map:Field_Number] = wpr:Fault_Code10
        Of 11
            local:FaultCode[map:Field_Number] = wpr:Fault_Code11
        Of 12
            local:FaultCode[map:Field_Number] = wpr:Fault_Code12
        End ! Case map:Field_Number
    End ! Loop

    Access:MANFAUPA.Clearkey(map:Field_Number_Key)
    map:Manufacturer = job:Manufacturer
    map:Field_Number = 0
    Set(map:Field_Number_Key,map:Field_Number_Key)
    Loop
        If Access:MANFAUPA.Next()
            Break
        End ! If Access:MANFAUPA.Next()
        If map:Manufacturer <> job:Manufacturer
            Break
        End ! If map:Manufacturer <> job:Manufacturer

        If map:MainFault = 0
            If sto:Accessory <> 'YES' And man:ForceAccessoryCode
                Cycle
            End ! If sto:Accessory <> 'YES' And man:ForceAcessoryCode
        End ! If map:MainFault = 0

        If (map:Compulsory = 'YES' And wpr:Adjustment = 'NO') Or |
            (map:Compulsory = 'YES' And wpr:Adjustment = 'YES' And map:CompulsoryForAdjustment)

            ! Compulsory if not Adjustment, or if Adjustment and fault marked as compulsory FOR adjustment
            If local:FaultCode[map:Field_Number] = ''
                Error# = 1
                glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
            End ! If local:FaultCode[map:Field_Number] = ''
        End ! If map:Compulsory = 'YES'

        If map:RestrictLength
            If Len(Clip(local:FaultCode[map:Field_Number])) < map:LengthFrom Or Len(Clip(local:FaultCode[map:Field_Number])) > map:LengthTo
                Error# = 1
                glo:errortext = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
            End ! If Len(Clip(local:FaultCode[map:Field_Number])) < map:LengthFrom Or Len(Clip(local:FaultCode[map:Field_Number])) > map:LengthTo
        End ! If map:RestrictLength

        If map:ForceFormat = 'YES'
            If CheckFaultFormat(local:FaultCode[map:Field_Number],map:FieldFormat)
                Error# = 1
                glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
            End ! If CheckFaultFormat(local:FaultCode[map:Field_Number],map:FieldFormat)
        End ! If map:ForceFormat = 'YES'

        If map:KeyRepair
            If local:FaultCode[map:Field_Number] = 1
                ! The Key Repair Fault Code should only by 1 or 0.
                tmp:KeyRepair = 1
            End ! If local:FaultCode[map:Field_Number] <> ''
        End ! If map:KeyRepair
    End ! Loop


! Deleting (DBH 24/10/2007 14:13:11) (DBH 24/10/2007 14:13:28) - TrkBs: 9278
!WarrantyPartFaultCodes      Routine
    !Warranty Parts Fault Codes
    !Before we start Check if PART is an accessory

!    Save_map_ID = Access:MANFAUPA.SaveFile()
!    Access:MANFAUPA.ClearKey(map:Field_Number_Key)
!    map:Manufacturer = job:Manufacturer
!    Set(map:Field_Number_Key,map:Field_Number_Key)
!    Loop
!        If Access:MANFAUPA.NEXT()
!           Break
!        End !If
!        If map:Manufacturer <> job:Manufacturer      |
!            Then Break.  ! End If
        !Check ok!
!        Access:Stock.ClearKey(sto:Ref_Number_Key)
!        sto:Ref_Number = wpr:Part_Ref_Number
!        IF Access:Stock.Fetch(sto:Ref_Number_Key)
          !Error!
!        ELSE
!          IF sto:Accessory <> 'YES' AND map:Manufacturer = 'MOTOROLA' !ALERT WORKAROUND!
!            Skip_non_main = TRUE
!          END
!        END
!        IF Skip_non_Main = TRUE
!          IF map:MainFault = FALSE
!            CYCLE
!          END
!        END
!        Case map:Field_Number
!            Of 1
!                If map:Compulsory = 'YES'
!                    If wpr:Fault_Code1 = ''
!                        Error# = 1
!                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If wpr:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'
!                If map:restrictlength
!                    If Len(Clip(wpr:Fault_Code1)) < map:lengthfrom or Len(Clip(wpr:Fault_Code1)) > map:lengthto
!                        error# = 1
!                        glo:errortext = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
!                    End!If Len(Clip(wpr:Fault_Code1)) < map:lengthfrom or Len(Clip(wpr:Fault_Code1)) > map:lengthto
!                End!If map:restrictlength

!                If map:ForceFormat
!                    If CheckFaultFormat(wpr:Fault_Code1,map:FieldFormat)
!                        error# = 1
!                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
!                    End !If CheckFaultFormat(wpr:Fault_Code1,map:FieldFormat)
!                End !If map:ForceFormat


!            Of 2
!                If map:Compulsory = 'YES'
!                    If wpr:Fault_Code2 = ''
!                        Error# = 1
!                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If wpr:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

!                If map:restrictlength
!                    If Len(Clip(wpr:Fault_Code2)) < map:lengthfrom or Len(Clip(wpr:Fault_Code2)) > map:lengthto
!                        error# = 1
!                        glo:errortext = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
!                    End!If Len(Clip(wpr:Fault_Code1)) < map:lengthfrom or Len(Clip(wpr:Fault_Code1)) > map:lengthto
!                End!If map:restrictlength
!                If map:ForceFormat
!                    If CheckFaultFormat(wpr:Fault_Code2,map:FieldFormat)
!                        error# = 1
!                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
!                    End !If CheckFaultFormat(wpr:Fault_Code1,map:FieldFormat)
!                End !If map:ForceFormat

!            Of 3
!                If map:Compulsory = 'YES'
!                    If wpr:Fault_Code3 = ''
!                        Error# = 1
!                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If wpr:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

!                If map:restrictlength
!                    If Len(Clip(wpr:Fault_Code3)) < map:lengthfrom or Len(Clip(wpr:Fault_Code3)) > map:lengthto
!                        error# = 1
!                        glo:errortext = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
!                    End!If Len(Clip(wpr:Fault_Code1)) < map:lengthfrom or Len(Clip(wpr:Fault_Code1)) > map:lengthto
!                End!If map:restrictlength
!                If map:ForceFormat
!                    If CheckFaultFormat(wpr:Fault_Code3,map:FieldFormat)
!                        error# = 1
!                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
!                    End !If CheckFaultFormat(wpr:Fault_Code1,map:FieldFormat)
!                End !If map:ForceFormat


!            Of 4
!                If map:Compulsory = 'YES'
!                    If wpr:Fault_Code4 = ''
!                        Error# = 1
!                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If wpr:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

!                If map:restrictlength
!                    If Len(Clip(wpr:Fault_Code4)) < map:lengthfrom or Len(Clip(wpr:Fault_Code4)) > map:lengthto
!                        error# = 1
!                        glo:errortext = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
!                    End!If Len(Clip(wpr:Fault_Code1)) < map:lengthfrom or Len(Clip(wpr:Fault_Code1)) > map:lengthto
!                End!If map:restrictlength
!                If map:ForceFormat
!                    If CheckFaultFormat(wpr:Fault_Code4,map:FieldFormat)
!                        error# = 1
!                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
!                    End !If CheckFaultFormat(wpr:Fault_Code1,map:FieldFormat)
!                End !If map:ForceFormat


!            Of 5
!                If map:Compulsory = 'YES'
!                    If wpr:Fault_Code5 = ''
!                        Error# = 1
!                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If wpr:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

!                If map:restrictlength
!                    If Len(Clip(wpr:Fault_Code5)) < map:lengthfrom or Len(Clip(wpr:Fault_Code5)) > map:lengthto
!                        error# = 1
!                        glo:errortext = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
!                    End!If Len(Clip(wpr:Fault_Code1)) < map:lengthfrom or Len(Clip(wpr:Fault_Code1)) > map:lengthto
!                End!If map:restrictlength
!                If map:ForceFormat
!                    If CheckFaultFormat(wpr:Fault_Code5,map:FieldFormat)
!                        error# = 1
!                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
!                    End !If CheckFaultFormat(wpr:Fault_Code1,map:FieldFormat)
!                End !If map:ForceFormat


!            Of 6
!                If map:Compulsory = 'YES'
!                    If wpr:Fault_Code6 = ''
!                        Error# = 1
!                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If wpr:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

!                If map:restrictlength
!                    If Len(Clip(wpr:Fault_Code6)) < map:lengthfrom or Len(Clip(wpr:Fault_Code6)) > map:lengthto
!                        error# = 1
!                        glo:errortext = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
!                    End!If Len(Clip(wpr:Fault_Code1)) < map:lengthfrom or Len(Clip(wpr:Fault_Code1)) > map:lengthto
!                End!If map:restrictlength
!                If map:ForceFormat
!                    If CheckFaultFormat(wpr:Fault_Code6,map:FieldFormat)
!                        error# = 1
!                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
!                    End !If CheckFaultFormat(wpr:Fault_Code1,map:FieldFormat)
!                End !If map:ForceFormat


!            Of 7
!                If map:Compulsory = 'YES'
!                    If wpr:Fault_Code7 = ''
!                        Error# = 1
!                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If wpr:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

!                If map:restrictlength
!                    If Len(Clip(wpr:Fault_Code7)) < map:lengthfrom or Len(Clip(wpr:Fault_Code7)) > map:lengthto
!                        error# = 1
!                        glo:errortext = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
!                    End!If Len(Clip(wpr:Fault_Code1)) < map:lengthfrom or Len(Clip(wpr:Fault_Code1)) > map:lengthto
!                End!If map:restrictlength
!                If map:ForceFormat
!                    If CheckFaultFormat(wpr:Fault_Code7,map:FieldFormat)
!                        error# = 1
!                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
!                    End !If CheckFaultFormat(wpr:Fault_Code1,map:FieldFormat)
!                End !If map:ForceFormat


!            Of 8
!                If map:Compulsory = 'YES'
!                    If wpr:Fault_Code8 = ''
!                        Error# = 1
!                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If wpr:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

!                If map:restrictlength
!                    If Len(Clip(wpr:Fault_Code8)) < map:lengthfrom or Len(Clip(wpr:Fault_Code8)) > map:lengthto
!                        error# = 1
!                        glo:errortext = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
!                    End!If Len(Clip(wpr:Fault_Code1)) < map:lengthfrom or Len(Clip(wpr:Fault_Code1)) > map:lengthto
!                End!If map:restrictlength
!                If map:ForceFormat
!                    If CheckFaultFormat(wpr:Fault_Code8,map:FieldFormat)
!                        error# = 1
!                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
!                    End !If CheckFaultFormat(wpr:Fault_Code1,map:FieldFormat)
!                End !If map:ForceFormat


!            Of 9
!                If map:Compulsory = 'YES'
!                    If wpr:Fault_Code9 = ''
!                        Error# = 1
!                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If wpr:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

!                If map:restrictlength
!                    If Len(Clip(wpr:Fault_Code9)) < map:lengthfrom or Len(Clip(wpr:Fault_Code9)) > map:lengthto
!                        error# = 1
!                        glo:errortext = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
!                    End!If Len(Clip(wpr:Fault_Code1)) < map:lengthfrom or Len(Clip(wpr:Fault_Code1)) > map:lengthto
!                End!If map:restrictlength
!                If map:ForceFormat
!                    If CheckFaultFormat(wpr:Fault_Code9,map:FieldFormat)
!                        error# = 1
!                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
!                    End !If CheckFaultFormat(wpr:Fault_Code1,map:FieldFormat)
!                End !If map:ForceFormat


!            Of 10
!                If map:Compulsory = 'YES'
!                    If wpr:Fault_Code10 = ''
!                        Error# = 1
!                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If wpr:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

!                If map:restrictlength
!                    If Len(Clip(wpr:Fault_Code10)) < map:lengthfrom or Len(Clip(wpr:Fault_Code10)) > map:lengthto
!                        error# = 1
!                        glo:errortext = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
!                    End!If Len(Clip(wpr:Fault_Code1)) < map:lengthfrom or Len(Clip(wpr:Fault_Code1)) > map:lengthto
!                End!If map:restrictlength
!                If map:ForceFormat
!                    If CheckFaultFormat(wpr:Fault_Code10,map:FieldFormat)
!                        error# = 1
!                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
!                    End !If CheckFaultFormat(wpr:Fault_Code1,map:FieldFormat)
!                End !If map:ForceFormat



!            Of 11
!                If map:Compulsory = 'YES'
!                    If wpr:Fault_Code11 = ''
!                        Error# = 1
!                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If wpr:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

!                If map:restrictlength
!                    If Len(Clip(wpr:Fault_Code11)) < map:lengthfrom or Len(Clip(wpr:Fault_Code11)) > map:lengthto
!                        error# = 1
!                        glo:errortext = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
!                    End!If Len(Clip(wpr:Fault_Code1)) < map:lengthfrom or Len(Clip(wpr:Fault_Code1)) > map:lengthto
!                End!If map:restrictlength
!                If map:ForceFormat
!                    If CheckFaultFormat(wpr:Fault_Code11,map:FieldFormat)
!                        error# = 1
!                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
!                    End !If CheckFaultFormat(wpr:Fault_Code1,map:FieldFormat)
!                End !If map:ForceFormat


!            Of 12
!                If map:Compulsory = 'YES'
!                    If wpr:Fault_Code12 = ''
!                        Error# = 1
!                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If wpr:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

!                If map:restrictlength
!                    If Len(Clip(wpr:Fault_Code12)) < map:lengthfrom or Len(Clip(wpr:Fault_Code12)) > map:lengthto
!                        error# = 1
!                        glo:errortext = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
!                    End!If Len(Clip(wpr:Fault_Code1)) < map:lengthfrom or Len(Clip(wpr:Fault_Code1)) > map:lengthto
!                End!If map:restrictlength
!                If map:ForceFormat
!                    If CheckFaultFormat(wpr:Fault_Code12,map:FieldFormat)
!                        error# = 1
!                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (War) ' & Clip(wpr:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
!                    End !If CheckFaultFormat(wpr:Fault_Code1,map:FieldFormat)
!                End !If map:ForceFormat

!        End !Case map:Field_Number
!    End !Loop
!    Access:MANFAUPA.RestoreFile(Save_map_ID)

! End (DBH 24/10/2007 14:13:11)
ChargeablePartFaultCodes      Routine
    !Warranty Parts Fault Codes
    !Before we start Check if PART is an accessory

    Save_map_ID = Access:MANFAUPA.SaveFile()
    Access:MANFAUPA.ClearKey(map:Field_Number_Key)
    map:Manufacturer = job:Manufacturer
    Set(map:Field_Number_Key,map:Field_Number_Key)
    Loop
        If Access:MANFAUPA.NEXT()
           Break
        End !If
        If map:Manufacturer <> job:Manufacturer      |
            Then Break.  ! End If
        !Check ok!
        Access:Stock.ClearKey(sto:Ref_Number_Key)
        sto:Ref_Number = par:Part_Ref_Number
        IF Access:Stock.Fetch(sto:Ref_Number_Key)
          !Error!
        ELSE
          IF sto:Accessory <> 'YES' AND map:Manufacturer = 'MOTOROLA' !ALERT WORKAROUND!
            Skip_non_main = TRUE
          END
        END
        IF Skip_non_Main = TRUE
          IF map:MainFault = FALSE
            CYCLE
          END
        END
        Case map:Field_Number
            Of 1
!                If map:Compulsory = 'YES'
!                    If par:Fault_Code1 = ''
!                        Error# = 1
!                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If par:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'
                If map:restrictlength
                    If Len(Clip(par:Fault_Code1)) < map:lengthfrom or Len(Clip(par:Fault_Code1)) > map:lengthto
                        error# = 1
                        glo:errortext = Clip(glo:ErrorText) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(par:Fault_Code1)) < map:lengthfrom or Len(Clip(par:Fault_Code1)) > map:lengthto
                End!If map:restrictlength

                If map:ForceFormat
                    If CheckFaultFormat(par:Fault_Code1,map:FieldFormat)
                        error# = 1
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(par:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat


            Of 2
!                If map:Compulsory = 'YES'
!                    If par:Fault_Code2 = ''
!                        Error# = 1
!                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If par:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(par:Fault_Code2)) < map:lengthfrom or Len(Clip(par:Fault_Code2)) > map:lengthto
                        error# = 1
                        glo:errortext = Clip(glo:ErrorText) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(par:Fault_Code1)) < map:lengthfrom or Len(Clip(par:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(par:Fault_Code2,map:FieldFormat)
                        error# = 1
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(par:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat

            Of 3
!                If map:Compulsory = 'YES'
!                    If par:Fault_Code3 = ''
!                        Error# = 1
!                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If par:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(par:Fault_Code3)) < map:lengthfrom or Len(Clip(par:Fault_Code3)) > map:lengthto
                        error# = 1
                        glo:errortext = Clip(glo:ErrorText) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(par:Fault_Code1)) < map:lengthfrom or Len(Clip(par:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(par:Fault_Code3,map:FieldFormat)
                        error# = 1
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(par:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat


            Of 4
!                If map:Compulsory = 'YES'
!                    If par:Fault_Code4 = ''
!                        Error# = 1
!                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If par:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(par:Fault_Code4)) < map:lengthfrom or Len(Clip(par:Fault_Code4)) > map:lengthto
                        error# = 1
                        glo:errortext = Clip(glo:ErrorText) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(par:Fault_Code1)) < map:lengthfrom or Len(Clip(par:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(par:Fault_Code4,map:FieldFormat)
                        error# = 1
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(par:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat


            Of 5
!                If map:Compulsory = 'YES'
!                    If par:Fault_Code5 = ''
!                        Error# = 1
!                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If par:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(par:Fault_Code5)) < map:lengthfrom or Len(Clip(par:Fault_Code5)) > map:lengthto
                        error# = 1
                        glo:errortext = Clip(glo:ErrorText) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(par:Fault_Code1)) < map:lengthfrom or Len(Clip(par:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(par:Fault_Code5,map:FieldFormat)
                        error# = 1
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(par:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat


            Of 6
!                If map:Compulsory = 'YES'
!                    If par:Fault_Code6 = ''
!                        Error# = 1
!                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If par:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(par:Fault_Code6)) < map:lengthfrom or Len(Clip(par:Fault_Code6)) > map:lengthto
                        error# = 1
                        glo:errortext = Clip(glo:ErrorText) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(par:Fault_Code1)) < map:lengthfrom or Len(Clip(par:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(par:Fault_Code6,map:FieldFormat)
                        error# = 1
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(par:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat


            Of 7
!                If map:Compulsory = 'YES'
!                    If par:Fault_Code7 = ''
!                        Error# = 1
!                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If par:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(par:Fault_Code7)) < map:lengthfrom or Len(Clip(par:Fault_Code7)) > map:lengthto
                        error# = 1
                        glo:errortext = Clip(glo:ErrorText) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(par:Fault_Code1)) < map:lengthfrom or Len(Clip(par:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(par:Fault_Code7,map:FieldFormat)
                        error# = 1
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(par:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat


            Of 8
!                If map:Compulsory = 'YES'
!                    If par:Fault_Code8 = ''
!                        Error# = 1
!                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If par:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(par:Fault_Code8)) < map:lengthfrom or Len(Clip(par:Fault_Code8)) > map:lengthto
                        error# = 1
                        glo:errortext = Clip(glo:ErrorText) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(par:Fault_Code1)) < map:lengthfrom or Len(Clip(par:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(par:Fault_Code8,map:FieldFormat)
                        error# = 1
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(par:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat


            Of 9
!                If map:Compulsory = 'YES'
!                    If par:Fault_Code9 = ''
!                        Error# = 1
!                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If par:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(par:Fault_Code9)) < map:lengthfrom or Len(Clip(par:Fault_Code9)) > map:lengthto
                        error# = 1
                        glo:errortext = Clip(glo:ErrorText) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(par:Fault_Code1)) < map:lengthfrom or Len(Clip(par:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(par:Fault_Code9,map:FieldFormat)
                        error# = 1
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(par:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat


            Of 10
!                If map:Compulsory = 'YES'
!                    If par:Fault_Code10 = ''
!                        Error# = 1
!                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If par:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(par:Fault_Code10)) < map:lengthfrom or Len(Clip(par:Fault_Code10)) > map:lengthto
                        error# = 1
                        glo:errortext = Clip(glo:ErrorText) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(par:Fault_Code1)) < map:lengthfrom or Len(Clip(par:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(par:Fault_Code10,map:FieldFormat)
                        error# = 1
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(par:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat



            Of 11
!                If map:Compulsory = 'YES'
!                    If par:Fault_Code11 = ''
!                        Error# = 1
!                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If par:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(par:Fault_Code11)) < map:lengthfrom or Len(Clip(par:Fault_Code11)) > map:lengthto
                        error# = 1
                        glo:errortext = Clip(glo:ErrorText) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(par:Fault_Code1)) < map:lengthfrom or Len(Clip(par:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(par:Fault_Code11,map:FieldFormat)
                        error# = 1
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(par:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat


            Of 12
!                If map:Compulsory = 'YES'
!                    If par:Fault_Code12 = ''
!                        Error# = 1
!                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Missing: ' & Clip(map:Field_Name)
!                    End !If par:Fault_Code1 = ''
!                End !If map:Compulsory = 'YES'

                If map:restrictlength
                    If Len(Clip(par:Fault_Code12)) < map:lengthfrom or Len(Clip(par:Fault_Code12)) > map:lengthto
                        error# = 1
                        glo:errortext = Clip(glo:ErrorText) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code Invalid Length: ' & Clip(map:Field_name)
                    End!If Len(Clip(par:Fault_Code1)) < map:lengthfrom or Len(Clip(par:Fault_Code1)) > map:lengthto
                End!If map:restrictlength
                If map:ForceFormat
                    If CheckFaultFormat(par:Fault_Code12,map:FieldFormat)
                        error# = 1
                        glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Part (Cha) ' & Clip(par:Part_Number) & ' - Fault Code, "' & Clip(map:Field_Name) & '", Invalid Format: ' & map:FieldFormat
                    End !If CheckFaultFormat(par:Fault_Code1,map:FieldFormat)
                End !If map:ForceFormat

        End !Case map:Field_Number
    End !Loop
    Access:MANFAUPA.RestoreFile(Save_map_ID)
DefaultLabourCosts     Routine

    !work out what the default labour cost should be


    If InvoiceSubAccounts(job:Account_Number)
        access:subchrge.clearkey(suc:model_repair_type_key)
        suc:account_number = job:account_number
        suc:model_number   = job:model_number
        suc:charge_type    = job:charge_type
        suc:unit_type      = job:unit_type
        suc:repair_type    = job:repair_type
        if access:subchrge.fetch(suc:model_repair_type_key)
            access:trachrge.clearkey(trc:account_charge_key)
            trc:account_number = sub:main_account_number
            trc:model_number   = job:model_number
            trc:charge_type    = job:charge_type
            trc:unit_type      = job:unit_type
            trc:repair_type    = job:repair_type
            if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
                LocalDefaultLabour      = trc:RRCRate
            End!if access:trachrge.fetch(trc:account_charge_key)
        Else
            LocalDefaultLabour      = suc:RRCRate
        End!if access:subchrge.fetch(suc:model_repair_type_key)
    Else !If InvoiceSubAccounts(job:Account_Number)
        access:trachrge.clearkey(trc:account_charge_key)
        trc:account_number = sub:main_account_number
        trc:model_number   = job:model_number
        trc:charge_type    = job:charge_type
        trc:unit_type      = job:unit_type
        trc:repair_type    = job:repair_type
        if access:trachrge.fetch(trc:account_charge_key) = Level:Benign
            LocalDefaultLabour      = trc:RRCRate
        End!if access:trachrge.fetch(trc:account_charge_key)

    End !If InvoiceSubAccounts(job:Account_Number)

    If LocalDefaultLabour = 0 then
        access:stdchrge.clearkey(sta:model_number_charge_key)
        sta:model_number = job:model_number
        sta:charge_type  = job:charge_type
        sta:unit_type    = job:unit_type
        sta:repair_type  = job:repair_type
        if access:stdchrge.fetch(sta:model_number_charge_key)
        Else !if access:stdchrge.fetch(sta:model_number_charge_key)
            LocalDefaultLabour      = sta:RRCRate
        end !if access:stdchrge.fetch(sta:model_number_charge_key)
    End !LocalDefaultLabour = 0 then

Local.OutFaultPart      Procedure(Long  func:FieldNumber,String func:JobType)
Code
    Case func:JobType
        Of 'C'
            Save_par_ID = Access:PARTS.SaveFile()
            Access:PARTS.ClearKey(par:Part_Number_Key)
            par:Ref_Number  = job:Ref_Number
            Set(par:Part_Number_Key,par:Part_Number_Key)
            Loop
                If Access:PARTS.NEXT()
                   Break
                End !If
                If par:Ref_Number  <> job:Ref_Number      |
                    Then Break.  ! End If
                Case func:FieldNumber
                    Of 1
                        If par:Fault_Code1 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''
                    Of 2
                        If par:Fault_Code2 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 3
                        If par:Fault_Code3 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 4
                        If par:Fault_Code4 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 5
                        If par:Fault_Code5 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 6
                        If par:Fault_Code6 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 7
                        If par:Fault_Code7 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 8
                        If par:Fault_Code8 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 9
                        If par:Fault_Code9 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 10
                        If par:Fault_Code10 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 11
                        If par:Fault_Code11 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 12
                        If par:Fault_Code12 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''
                End !Case map:Field_Number
            End !Loop
            Access:PARTS.RestoreFile(Save_par_ID)

            Return Found#
        Of 'W'
            Save_wpr_ID = Access:WARPARTS.SaveFile()
            Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
            wpr:Ref_Number  = job:Ref_Number
            Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
            Loop
                If Access:WARPARTS.NEXT()
                   Break
                End !If
                If wpr:Ref_Number  <> job:Ref_Number      |
                    Then Break.  ! End If
                Case func:FieldNumber
                    Of 1
                        If wpr:Fault_Code1 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''
                    Of 2
                        If wpr:Fault_Code2 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 3
                        If wpr:Fault_Code3 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 4
                        If wpr:Fault_Code4 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 5
                        If wpr:Fault_Code5 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 6
                        If wpr:Fault_Code6 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 7
                        If wpr:Fault_Code7 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 8
                        If wpr:Fault_Code8 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 9
                        If wpr:Fault_Code9 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 10
                        If wpr:Fault_Code10 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 11
                        If wpr:Fault_Code11 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''

                    Of 12
                        If wpr:Fault_Code12 <> ''
                            Found# = 1
                            Break
                        End !If par:Fault_Code1 <> ''
                End !Case map:Field_Number
            End !Loop
            Access:WARPARTS.RestoreFile(Save_wpr_ID)

            Return Found#

    End !Case func:Type
local.ValidateFaultCodes        Procedure()
local:PartFaultCode     String(30),Dim(12)
local:CExcludeFromEDI           Byte(0)
local:WExcludeFromEDI           Byte(0)
Code
    Error# = 0
    ChaComp# = False
    WarComp# = False
    If ForceFaultCodes(job:Chargeable_Job,job:Warranty_Job,job:Charge_Type,job:Warranty_Charge_Type,job:Repair_type,job:Repair_Type_Warranty,'C')
        ChaComp# = True
        ! Inserting (DBH 01/05/2008) # 9723 - Fault codes should be forced. Check exclude from edi flag
        Access:REPTYDEF.Clearkey(rtd:ChaManRepairTypeKey)
        rtd:Manufacturer = job:Manufacturer
        rtd:Chargeable = 'YES'
        rtd:Repair_Type = job:Repair_Type
        If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
            ! Found
            If rtd:ExcludeFromEDI = 1
                local:CExcludeFromEDI = 1
            End ! If rtd:ExcludeFromEDI = 1
        Else ! If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
        End ! If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
        ! End (DBH 01/05/2008) #9723


    End ! job:Warranty_Charge_Type,job:Repair_type,job:Repair_Type_Warranty,'C')

    If ForceFaultCodes(job:Chargeable_Job,job:Warranty_Job,job:Charge_Type,job:Warranty_Charge_Type,job:Repair_type,job:Repair_Type_Warranty,'W')
        WarComp# = True
        ! Inserting (DBH 01/05/2008) # 9723 - Fault codes should be forced. Check exclude from edi flag
        Access:REPTYDEF.Clearkey(rtd:WarManRepairTypeKey)
        rtd:Manufacturer = job:Manufacturer
        rtd:Warranty = 'YES'
        rtd:Repair_Type = job:Repair_Type_Warranty
        If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
            ! Found
            If rtd:ExcludeFromEDI = 1
                local:WExcludeFromEDI = 1
            End ! If rtd:ExcludeFromEDI = 1
        Else ! If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
        End ! If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
        ! End (DBH 01/05/2008) #9723
    End ! job:Warranty_Charge_Type,job:Repair_type,job:Repair_Type_Warranty,'C')

    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
    tra:Account_Number = wob:HeadACcountNumber
    If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        Access:LOCATION.Clearkey(loc:Location_Key)
        loc:Location = tra:SiteLocation
        If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
        End ! If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
    End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign


    setcursor(cursor:wait)
    save_maf_id = access:manfault.savefile()
    access:manfault.clearkey(maf:field_number_key)
    maf:manufacturer = job:manufacturer
    maf:Field_Number = 0
    set(maf:field_number_key,maf:field_number_key)
    loop
        if access:manfault.next()
           break
        end !if
        if maf:manufacturer <> job:manufacturer      |
            then break.  ! end if

! Changing (DBH 09/11/2007) # 9278 - Logic improved (i hope)
!        If (maf:Compulsory_At_Booking = 'YES' And ~maf:CompulsoryIfExchange And WarComp# = True) Or |  !Maybe long winded but sometimes logic fails me
!            (maf:Compulsory_At_Booking = 'YES' And maf:CompulsoryIfExchange And job:Exchange_Unit_Number <> 0 And WarComp# = True) Or |
!            (maf:Compulsory = 'YES' And func:Type = 'C' And ~maf:CompulsoryIfExchange And WarComp# = True) Or |
!            (maf:Compulsory = 'YES' And func:Type = 'C' And maf:CompulsoryIfExchange And job:Exchange_Unit_Number <> 0 And WarComp# = True) Or |
!            (maf:CharCompulsoryBooking = True And ChaComp# = True) Or |
!            (maf:CharCompulsory = True And func:Type = 'C' And ChaComp# = True)
! to (DBH 09/11/2007) # 9278
        Continue# = 0
        If WarComp# = 1
            ! Warranty Job: Force Fault Codes (DBH: 09/11/2007)
            If maf:CompulsoryIfExchange = 0
                ! Doesn't matter if exchange is attached (DBH: 09/11/2007)
                If maf:Compulsory_At_Booking = 'YES' Or (maf:Compulsory = 'YES' and (func:Type = 'C' OR func:Type = 'T')) ! #12363 Force if going to third party too (DBH: 15/02/2012)
                    ! Compulsory at booking, or compulsory at completion (DBH: 09/11/2007)
                    Continue# = 1
                End ! If maf:Compulsory_At_Booking = 'YES' Or (maf:Compulsory = 'YES' and func:Type = 'C')
            Else ! If maf:CompulsoryIfExchange = 0
                ! Only forced if exchange attached (DBH: 09/11/2007)
                If job:Exchange_Unit_Number <> 0
                    If maf:Compulsory_At_Booking = 'YES' Or (maf:Compulsory = 'YES' and (func:Type = 'C' OR func:Type = 'T')) ! #12363 Force if going to third party too (DBH: 15/02/2012)
                        ! Compulsory at booking, or compulsory at completion (DBH: 09/11/2007)
                        Continue# = 1
                    End ! If maf:Compulsory_At_Booking = 'YES' Or (maf:Compulsory = 'YES' and func:Type = 'C')
                End ! If job:Exchange_Unit_Number <> 0
            End ! If maf:CompulsoryIfExchange = 0
            If Continue# = 1
                ! Inserting (DBH 06/05/2008) # 9723 - Only force fault code if the warranty repair type matches
                If maf:CompulsoryForRepairType = 1
                    If maf:CompulsoryRepairType <> job:Repair_Type_Warranty
                        ! Fault Code is only compulsory when the repair type matches (DBH: 06/05/2008)
                        Continue# = 0
                    End ! If maf:CompulsoryRepairType <> job:Repair_Type_Warranty
                End ! If maf:CompulsoryForRepairType = 1
                ! End (DBH 06/05/2008) #9723
            End ! If Continue# = 1
        End ! If WarComp# = 1

        If ChaComp# = 1
            ! Chargeable Job: Force Fault Codes (DBH: 09/11/2007)
            If maf:CharCompulsoryBooking = 1 Or (maf:CharCompulsory = 1 and func:Type = 'C')
                ! Compulsory at booking, or compulsory at completion (DBH: 09/11/2007)
                Continue# = 1
            End ! If maf:Compulsory_At_Booking = 'YES' or (maf:Compulsory = 'YES' and func:Type = 'C')
        End ! If ChaComp# = 1

        If Continue# = 0
            ! Check all the part fault codes to see if any of the value mean this fault code is forced (DBH: 09/11/2007)
            Access:WARPARTS.Clearkey(wpr:Part_Number_Key)
            wpr:Ref_Number = job:Ref_Number
            Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
            Loop
                If Access:WARPARTS.Next()
                    Break
                End ! If Access:WARPARTS.Next()
                If wpr:Ref_Number <> job:Ref_Number
                    Break
                End ! If wpr:Ref_Number <> job:Ref_Number

                ! Save the fault codes in an easy format (DBH: 09/11/2007)
                local:PartFaultCode[1] = wpr:Fault_Code1
                local:PartFaultCode[2] = wpr:Fault_Code2
                local:PartFaultCode[3] = wpr:Fault_Code3
                local:PartFaultCode[4] = wpr:Fault_Code4
                local:PartFaultCode[5] = wpr:Fault_Code5
                local:PartFaultCode[6] = wpr:Fault_Code6
                local:PartFaultCode[7] = wpr:Fault_Code7
                local:PartFaultCode[8] = wpr:Fault_Code8
                local:PartFaultCode[9] = wpr:Fault_Code9
                local:PartFaultCode[10] = wpr:Fault_Code10
                local:PartFaultCode[11] = wpr:Fault_Code11
                local:PartFaultCode[12] = wpr:Fault_Code12

                Loop x# = 1 To 12
                    If local:PartFaultCode[x#] <> ''
                        Access:MANFPALO.Clearkey(mfp:Field_Key)
                        mfp:Manufacturer = job:Manufacturer
                        mfp:Field_Number = x#
                        mfp:Field = local:PartFaultCode[x#]
                        If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
                            If mfp:ForceJobFaultCode
                                If mfp:ForceFaultCodeNumber = maf:Field_Number
                                    Continue# = 1
                                    Break
                                End ! If mfp:ForceFaultCodeNumber = 1
                            End ! If mfp:ForceJobFaultCode
                        End ! If Access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign
                    End ! If local:PartFaultCode[x#] <> ''
                End ! Loop x# = 1 To 12
                If Continue# = 1
                    Break
                End ! If Continue# = 1
            End ! Loop
        End ! If Continue# = 0

        If Continue# = 0
            Cycle
        End ! If Continue# = 0
! End (DBH 09/11/2007) #9278

!            Stop('maf:Compulsory_At_Booking: ' & maf:Compulsory_At_Booking & |
!                '<13,10>maf:CompulsoryIfExchange: ' & maf:CompulsoryIfExchange & |
!                '<13,10>job:Exchange_Unit_Number: ' & job:Exchange_Unit_Number & |
!                '<13,10>maf:Compulsory: ' & maf:Compulsory & |
!                '<13,10>maf:CharCompulsory: ' & maf:CharCompulsory & |
!                '<13,10>maf:CharCompulsoryBooking:' & maf:CharCompulsoryBooking & |
!                '<13,10>ChaComp#: ' & ChaComp# & |
!                '<13,10>WarComp#: ' & WarComp# & |
!                '<13,10,13,10>maf:Field_Name: ' & maf:Field_Name)

        ! Inserting (DBH 14/03/2007) # 8718 - Do not force fault codes (apart from in and out faults) for obf jobs,
        If ~man:UseFaultCodesForOBF And jobe:OBFValidated = 1
            If maf:InFault <> 1 And maf:MainFault <> 1
                Cycle
            End ! If maf:InFault <> 1 And maf:MainFault <> 1
        End ! If ~man:UseFaultCodeForOBF
        ! End (DBH 14/03/2007) #8718

        ! Inserting (DBH 01/05/2008) # 9723 - If exclude from edi and fault code forced, only force the in/out fault
        If WarComp# = 1 And local:WExcludeFromEDI = 1
            If maf:InFault <> 1 And maf:MainFault <> 1
                Cycle
            End ! If maf:InFault <> 1 And maf:MainFault <>1
        End ! If WarComp# = 1 And local:WExcludeFromEDI = 1
        If ChaComp# = 1 And local:CExcludeFromEDI = 1
            If maf:InFault <> 1 And maf:MainFault <> 1
                Cycle
            End ! If maf:InFault <> 1 And maf:MainFault <> 1
        End ! If ChaComp# = 1 And local:CExcludeFromEDI = 1
        ! End (DBH 01/05/2008) #9723

        ! An old override. Don't know why this exists, but thought it safer not to delete (DBH: 09/11/2007)
        If job:Manufacturer = 'ERICSSON' and job:DOP = ''
            Cycle
        End ! If job:Manufacturer = 'ERICSSON' and job:DOP = ''

        ! If fault code is hidden due to another code being blank, then don't check (DBH: 09/11/2007)
        If maf:HideRelatedCodeIfBlank
            If tmp:FaultCode[maf:BlankRelatedCode] = ''
                Cycle
            End ! If tmp:FaultCode[maf:BlankRelatedCode} = ''
        End ! If maf:HideRelatedCodeIfBlank

        ! Is the fault code restricted by site location (DBH: 09/11/2007)
        If maf:RestrictAvailability
            If (maf:RestrictServiceCentre = 1 And ~loc:Level1) Or |
                (maf:RestrictServiceCentre = 2 And ~loc:Level2) Or |
                (maf:RestrictServiceCentre = 3 And ~loc:Level3)
                Cycle
            End ! (maf:RestrictServiceCentre = 3 And ~loc:Level3)
        End ! If maf:RestrictAvailability

        If maf:RestrictLength
            If Len(Clip(tmp:FaultCode[maf:Field_Number])) < maf:LengthFrom Or Len(Clip(tmp:FaultCode[maf:Field_Number])) > maf:LengthTo
                error# = 1
                glo:errortext = Clip(glo:errortext) & '<13,10>Fault Code Invalid Length: ' & Clip(maf:Field_name)
            End ! If Len(Clip(tmp:FaultCode[maf:Field_Number])) < maf:LengthFrom Or Len(Clip(tmp:FaultCode[maf:Field_Number])) > maf:LengthTo
        End ! If maf:RetrictLength

        If maf:ForceFormat
            If CheckFaultFormat(tmp:FaultCode[maf:Field_Number],maf:FieldFormat)
                error# = 1
                glo:ErrorText = Clip(glo:ErrorText) & '<13,10>Fault Code, "' & Clip(maf:Field_Name) & '", Invalid Format: ' & maf:FieldFormat
            End ! If CheckFaultFormat(tmp:FaultCode[maf:Field_Number],maf:FieldFormat)
        End ! If maf:ForceFormat

        If tmp:FaultCode[maf:Field_Number] = ''
            error# = 1
            glo:errortext = Clip(glo:errortext) & '<13,10>Fault Code Missing: ' & Clip(maf:field_name)
        End ! If tmp:FaultCode[maf:Field_Number] = ''

    End !loop
    access:manfault.restorefile(save_maf_id)
    setcursor()

    Return Error#
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
CustomerNameRequired PROCEDURE  (func:AccountNumber)  ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ReturnByte           BYTE
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    ReturnByte = 0
    !TB13307 - J - 01/07/14 - change needed to allow show name(1) and Force name(2)

    !Check the trade account details to see if a customer name is required
    Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
    sub:Account_Number = func:AccountNumber
    If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = sub:Main_Account_Number
        If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
            If tra:Invoice_Sub_Accounts = 'YES'
                if sub:Force_Customer_name = 1 then     !this is displayed as "show customer name"
                    ReturnByte = 2
                END
                If sub:ForceEndUserName
                    ReturnByte = 1
                End !If sub:ForceEndUserName
            Else !If tra:Invoice_Sub_Accounts = 'YES'
                If tra:Use_Contact_Name = 'YES'
                    ReturnByte = 1
                End!If tra:Use_Contact_Name = 'YES'
            End !If tra:Invoice_Sub_Accounts = 'YES'
        End!If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
    End!If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
    Return ReturnByte
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
MSNRequired          PROCEDURE  (func:Manufacturer)   ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Access:MANUFACT.ClearKey(man:Manufacturer_Key)
    man:Manufacturer = func:Manufacturer
    If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        If man:Use_MSN = 'YES'
            Return Level:Benign
        End!If man:Use_MSN = 'YES'
    End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
    Return Level:Fatal
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
MakePartsRequest     PROCEDURE  (func:Supplier,func:PartNumber,func:Description,func:Quantity) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    access:ORDPEND.clearkey(ope:Supplier_Key)
    ope:Supplier    = func:Supplier
    ope:Part_Number = func:PartNumber
    If access:ORDPEND.tryfetch(ope:Supplier_Key) = Level:Benign
        !Found
        ope:Quantity    += func:Quantity
        Access:ORDPEND.Update()
    Else! If access:ORDPEND.tryfetch(ope:Supplier_Key) = Level:Benign
        !Error
        !Assert(0,'Fetch Error')
        If Access:ORDPEND.PrimeRecord() = Level:Benign
            !ope:Part_Ref_Number = sto:Ref_Number
            ope:Part_Type       = 'STO'
            ope:Supplier        = func:Supplier
            ope:Part_Number     = func:PartNumber
            ope:Description     = func:Description
            !ope:Job_Number      = ''
            ope:Quantity        = func:Quantity

            If Access:ORDPEND.Tryinsert()
                Access:ORDPEND.CancelAutoInc()
            End!If access:ORDPEND.tryinsert()
        End!If access:ORDPEND.primerecord() = Level:Benign
    End! If access:ORDPEND.tryfetch(ope:Supplier_Key) = Level:Benign

! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
MainStoreLocation    PROCEDURE                        ! Declare Procedure
retValue             STRING(30)
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    RETURN (VodacomClass.MainStoreLocation())

!    Relate:LOCATION_ALIAS.Open()
!
!    retValue = ''
!
!    Access:LOCATION_ALIAS.ClearKey(loc_ali:Main_Store_Key)
!    loc_ali:Main_Store = 'YES'
!    Set(loc_ali:Main_Store_Key,loc_ali:Main_Store_Key)
!    If Access:LOCATION_ALIAS.NEXT() = Level:Benign
!        If loc_ali:Main_Store = 'YES'
!            retValue =  loc_ali:Location
!        End!If loc_ali:Main_Store = 'YES'
!    End!If Access:LOCATION_ALIAS.NEXT() = Level:Benign
!
!
!    Relate:LOCATION_ALIAS.Close()
!
!    Return retValue
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
StripNonAlpha        PROCEDURE  (in_string)           ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    in_string = CLIP(LEFT(in_string))

    STR_LEN#  = LEN(in_string)
    STR_POS#  = 1

    in_string = UPPER(SUB(in_string,STR_POS#,1)) & SUB(in_string,STR_POS#+1,STR_LEN#-1)

    LOOP STR_POS# = 1 TO STR_LEN#

     IF ~(Val(SUB(in_string,STR_POS#,1)) > 64 And Val(SUB(in_string,STR_POS#,1)) < 91) And |
         ~(Val(SUB(in_string,STR_POS#,1)) > 47 And Val(SUB(in_string,STR_POS#,1)) < 58)
        in_string = SUB(in_string,1,STR_POS#-1) & SUB(in_string,STR_POS#+1,STR_LEN#-1)
     .
    .

    RETURN(in_string)
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
