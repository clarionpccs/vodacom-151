

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01009.INC'),ONCE        !Local module procedure declarations
                     END


TransferParts        PROCEDURE  (func:Type)           ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
save_par_id          USHORT,AUTO
save_wpr_id          USHORT,AUTO
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Case func:Type
        Of 'W'  !To Warranty

            count# = 0
            setcursor(cursor:wait)
            save_par_id = access:parts.savefile()
            access:parts.clearkey(par:part_number_key)
            par:ref_number  = job:ref_number
            set(par:part_number_key,par:part_number_key)
            loop
                if access:parts.next()
                   break
                end !if
                if par:ref_number  <> job:ref_number      |
                    then break.  ! end if
                If par:pending_ref_number <> ''
                    access:ordpend.clearkey(ope:ref_number_key)
                    ope:ref_number = par:pending_ref_number
                    if access:ordpend.tryfetch(ope:ref_number_key) = Level:Benign
                        ope:part_type = 'JOB'
                        access:ordpend.update()
                    End!if access:ordpend.tryfetch(ope:ref_number_key) = Level:Benign
                End!If wpr:pending_ref_number <> ''
                If par:order_number <> ''
                    access:ordparts.clearkey(orp:record_number_key)
                    orp:record_number   = par:order_number
                    If access:ordparts.tryfetch(orp:record_number_key) = Level:Benign
                        orp:part_type = 'JOB'
                        access:ordparts.update()
                    End!If access:ordparts.tryfetch(orp:record_number_key) = Level:Benign
                End!If wpr:order_number <> ''
                get(warparts,0)
                if access:warparts.primerecord() = Level:Benign
                    record_number$  = wpr:record_number
                    wpr:record      :=: par:record
                    wpr:record_number   = record_number$
                    if access:warparts.insert()
                        access:warparts.cancelautoinc()
                    End!if access:parts.insert()
                End!if access:parts.primerecord() = Level:Benign
                Delete(parts)
                count# += 1
            end !loop
            access:parts.restorefile(save_par_id)
            setcursor()
            if count# <> 0

                get(audit,0)
                if access:audit.primerecord() = level:benign
                    aud:ref_number    = job:ref_number
                    aud:date          = today()
                    aud:time          = clock()
                    aud:type          = 'JOB'
                    access:users.clearkey(use:password_key)
                    use:password = glo:password
                    access:users.fetch(use:password_key)
                    aud:user = use:user_code
                    aud:action        = 'CHARGEABLE PARTS TRANSFERRED TO WARRANTY'
                    access:audit.insert()
                end!�if access:audit.primerecord() = level:benign
            End!if count# <> 0
            Access:MODELNUM.Clearkey(mod:Model_Number_Key)
            mod:Model_Number    = job:Model_NUmber
            If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                !Found
                job:EDI = PendingJob(mod:Manufacturer)
                job:Manufacturer    = mod:Manufacturer
            Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
        Of 'C' !To Chargeable
            count# = 0
            setcursor(cursor:wait)
            save_wpr_id = access:warparts.savefile()
            access:warparts.clearkey(wpr:part_number_key)
            wpr:ref_number  = job:ref_number
            set(wpr:part_number_key,wpr:part_number_key)
            loop
                if access:warparts.next()
                   break
                end !if
                if wpr:ref_number  <> job:ref_number      |
                    then break.  ! end if
                If wpr:pending_ref_number <> ''
                    access:ordpend.clearkey(ope:ref_number_key)
                    ope:ref_number = wpr:pending_ref_number
                    if access:ordpend.tryfetch(ope:ref_number_key) = Level:Benign
                        ope:part_type = 'JOB'
                        access:ordpend.update()
                    End!if access:ordpend.tryfetch(ope:ref_number_key) = Level:Benign
                End!If wpr:pending_ref_number <> ''
                If wpr:order_number <> ''
                    access:ordparts.clearkey(orp:record_number_key)
                    orp:record_number   = wpr:order_number
                    If access:ordparts.tryfetch(orp:record_number_key) = Level:Benign
                        orp:part_type = 'JOB'
                        access:ordparts.update()
                    End!If access:ordparts.tryfetch(orp:record_number_key) = Level:Benign
                End!If wpr:order_number <> ''
                get(parts,0)
                if access:parts.primerecord() = Level:Benign
                    record_number$  = par:record_number
                    par:record      :=: wpr:record
                    par:record_number   = record_number$
                    if access:parts.insert()
                        access:parts.cancelautoinc()
                    End!if access:parts.insert()
                End!if access:parts.primerecord() = Level:Benign
                Delete(warparts)
                count# += 1
            end !loop
            access:warparts.restorefile(save_wpr_id)
            setcursor()
            if count# <> 0
                get(audit,0)
                if access:audit.primerecord() = level:benign
                    aud:ref_number    = job:ref_number
                    aud:date          = today()
                    aud:time          = clock()
                    aud:type          = 'JOB'
                    access:users.clearkey(use:password_key)
                    use:password = glo:password
                    access:users.fetch(use:password_key)
                    aud:user = use:user_code
                    aud:action        = 'WARRANTY PARTS TRANSFERRED TO CHARGEABLE'
                    access:audit.insert()
                end!�if access:audit.primerecord() = level:benign

            End!if count# <> 0
            Access:MODELNUM.Clearkey(mod:Model_Number_Key)
            mod:Model_Number    = job:Model_NUmber
            If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                !Found
                job:EDI = PendingJob(mod:Manufacturer)
                job:Manufacturer    = mod:Manufacturer
            Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign

    End !func:Type
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
RapidLocation        PROCEDURE  (func:Location)       ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Return (VodacomClass.IsRapidLocation(func:Location))
!
!    Access:LOCATION_ALIAS.Clearkey(loc_ali:Location_Key)
!    loc_ali:Location    = func:Location
!    If Access:LOCATION_ALIAS.Tryfetch(loc_ali:Location_Key) = Level:Benign
!        !Found
!        If loc_ali:UseRapidStock
!            Return Level:Fatal
!        End !If loc_ali:UseRapidStock
!    Else ! If Access:LOCATION_ALIAS.Tryfetch(loc_ali:Location_Key) = Level:Benign
!        !Error
!    End !If Access:LOCATION_ALIAS.Tryfetch(loc_ali:Location_Key) = Level:Benign
!    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
DeleteEstimatePart   PROCEDURE  (func:Type)           ! Declare Procedure
save_orp_id          USHORT,AUTO
save_ori_id          USHORT,AUTO
save_orh_id          USHORT,AUTO
tmp:Delete           BYTE(0)
tmp:Scrap            BYTE(0)
tmp:StockNumber      LONG
tmp:AttachBySolder   BYTE(0)
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
Local                CLASS
CreateStockHistory   PROCEDURE(String func:Information)
                     END

locAuditNotes       STRING(255)
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    tmp:Delete  = 0
    tmp:Scrap   = 0


    If epr:Adjustment = 'YES' Or epr:UsedOnRepair = 0
        Case Missive('Are you sure you want to delete this part?','ServiceBase 3g',|
            'mquest.jpg','\No|/Yes')
        Of 2 ! Yes Button
            tmp:Delete = 1
        Of 1 ! No Button
        End ! Case Missive

    Else !If epr:Adjustment = 'YES'
        tmp:StockNumber = 0
        tmp:AttachBySolder = 0

        If epr:Part_Ref_Number <> ''
            Access:STOCK.Clearkey(sto:Ref_Number_Key)
            sto:Ref_Number  = epr:Part_Ref_Number
            If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                !Found
                tmp:StockNumber = sto:Ref_Number
                tmp:AttachBySolder  = sto:AttachBySolder
            Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
        Else !If epr:Part_Ref_Number <>

        End !If epr:Part_Ref_Number <>

        !Just delete if the part is a sundry - 11837 (DBH: 09-12-2010)
        If tmp:StockNumber And sto:Sundry_Item = 'YES'
            Case Missive('Are you sure you want to delete this part?','ServiceBase 3g',|
                'mquest.jpg','\No|/Yes')
            Of 1 ! No Button
            Of 2 ! Yes Button
                tmp:Delete = 1
            End ! Case Missive
        Else !If tmp:StockNumber And sto:Sundry_Item = 'YES'

            If func:Type = 0 And RapidLocation(sto:Location)
                If tmp:AttachBySolder
                    Case Missive('The selected part is "Attached By Solder" and therefore will be scrapped.','ServiceBase 3g',|
                        'mquest.jpg','\No|/Scrap')
                    Of 2 ! Scrap Button
                        tmp:Delete  = 1
                        tmp:Scrap   = 1
                        Do AddSolder
                    Of 1 ! No Button
                    End ! Case Missive
                Else !If tmp:AttachBySolder
                    If epr:PartAllocated = 0
                        Case Missive('This part has not yet been allocated. '&|
                            '<13,10>'&|
                            '<13,10>Are you sure you want to delete it?','ServiceBase 3g',|
                            'mquest.jpg','\No|/Yes')
                        Of 2 ! Yes Button
                            tmp:Delete = 1
                            Access:STOCK.Clearkey(sto:Ref_Number_Key)
                            sto:Ref_Number  = tmp:StockNumber
                            If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                !Found
                                sto:Quantity_Stock += epr:Quantity
                                If Access:STOCK.Update() = Level:Benign
                                    Local.CreateStockHistory('UNALLOCATED PART REMOVED')
                                    tmp:Delete = 1
                                    ! #10396 Now has stock. Part unsuspended. (DBH: 16/07/2010)
                                    if (sto:Suspend)
                                        sto:Suspend = 0
                                        If (Access:STOCK.TryUpdate() = Level:Benign)
                                            If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                'ADD', | ! Transaction_Type
                                                '', | ! Depatch_Note_Number
                                                0, | ! Job_Number
                                                0, | ! Sales_Number
                                                0, | ! Quantity
                                                epr:Purchase_Cost, | ! Purchase_Cost
                                                epr:Sale_Cost, | ! Sale_Cost
                                                epr:Retail_Cost, | ! Retail_Cost
                                                'PART UNSUSPENDED', | ! Notes
                                                '') ! Information
                                            End ! AddToStockHistory
                                        End ! If (Access:STOCK.TryUpdate() = Level:Benign)
                                    end

                                End !If Access:STOCK.Update() = Level:Benign
                            Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                !Error
                                !Assert(0,'<13,10>Fetch Error<13,10>')
                            End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign

                        Of 1 ! No Button
                        End ! Case Missive

                    Else !If epr:PartAllocated = 0
                        Case Missive('Are you sure you want to mark this part to be returned by allocations?','ServiceBase 3g',|
                            'mquest.jpg','\No|/Yes')
                        Of 2 ! Yes Button
                            epr:PartAllocated = 0
                            epr:Status  = 'RET'
                            Access:ESTPARTS.Update()
                            AddToStockAllocation(epr:Record_Number,'EST',epr:Quantity,'RET',job:Engineer)
                            Return 0
                        Of 1 ! No Button
                        End ! Case Missive

                    End !If epr:PartAllocated = 0
                End !If tmp:AttachBySolder

            Else !If func:Type = 0 And RapidLocation(sto:Location)
                Case Missive('You have selected to delete this part.'&|
                    '<13,10>'&|
                    '<13,10>Do you wish to RESTOCK it, or SCRAP it?','ServiceBase 3g',|
                    'mquest.jpg','\Cancel|Scrap|Restock')
                Of 3 ! Restock Button
                    If tmp:StockNumber
                        Case Missive('The selected part was originally taked from location ' & Clip(sto:Location) & '.'&|
                            '<13,10>'&|
                            '<13,10>Do you wish to return it to it''s ORIGINAL location, or to a NEW location?','ServiceBase 3g',|
                            'mquest.jpg','\Cancel|New|Original')
                        Of 3 ! Original Button
                            Access:STOCK.Clearkey(sto:Ref_Number_Key)
                            sto:Ref_Number  = tmp:StockNumber
                            If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                !Found
                                sto:Quantity_stock += epr:Quantity
                                If Access:STOCK.Update() = Level:Benign
                                    Local.CreateStockHistory('')
                                    tmp:Delete = 1
                                    ! #10396 Now has stock. Part unsuspended. (DBH: 16/07/2010)
                                    if (sto:Suspend)
                                        sto:Suspend = 0
                                        If (Access:STOCK.TryUpdate() = Level:Benign)
                                            If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                'ADD', | ! Transaction_Type
                                                '', | ! Depatch_Note_Number
                                                0, | ! Job_Number
                                                0, | ! Sales_Number
                                                0, | ! Quantity
                                                epr:Purchase_Cost, | ! Purchase_Cost
                                                epr:Sale_Cost, | ! Sale_Cost
                                                epr:Retail_Cost, | ! Retail_Cost
                                                'PART UNSUSPENDED', | ! Notes
                                                '') ! Information
                                            End ! AddToStockHistory
                                        End ! If (Access:STOCK.TryUpdate() = Level:Benign)
                                    end

                                End !If Access:STOCK.Update() = Level:Benign
                            Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                                !Error
                                !Assert(0,'<13,10>Fetch Error<13,10>')
                            End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                        Of 2 ! New Button
                            glo:Select1 = ''
                            Pick_New_Location
                            If glo:Select1 <> ''
                                Access:STOCK.Clearkey(sto:Location_Part_Description_Key)
                                sto:Location    = glo:Select1
                                sto:Part_Number = epr:Part_Number
                                sto:Description = epr:Description
                                If Access:STOCK.Tryfetch(sto:Location_Part_Description_Key) = Level:Benign
                                    !Found
                                    sto:Quantity_Stock += epr:Quantity
                                    If Access:STOCK.Update() = Level:Benign
                                        Local.CreateStockHistory('')
                                        tmp:Delete = 1
                                        ! #10396 Now has stock. Part unsuspended. (DBH: 16/07/2010)
                                        if (sto:Suspend)
                                            sto:Suspend = 0
                                            If (Access:STOCK.TryUpdate() = Level:Benign)
                                                If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                                                    'ADD', | ! Transaction_Type
                                                    '', | ! Depatch_Note_Number
                                                    0, | ! Job_Number
                                                    0, | ! Sales_Number
                                                    0, | ! Quantity
                                                    epr:Purchase_Cost, | ! Purchase_Cost
                                                    epr:Sale_Cost, | ! Sale_Cost
                                                    epr:Retail_Cost, | ! Retail_Cost
                                                    'PART UNSUSPENDED', | ! Notes
                                                    '') ! Information
                                                End ! AddToStockHistory
                                            End ! If (Access:STOCK.TryUpdate() = Level:Benign)
                                        end

                                    End !If Access:STOCK.Update() = Level:Benign
                                Else! If Access:STOCK.Tryfetch(sto:Location_Part_Description_Key) = Level:Benign
                                    !Error
                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                    Case Missive('Cannot find the selected part in location ' & CLip(glo:Select1) & '.'&|
                                        '<13,10>'&|
                                        '<13,10>Do you wish to add it as a NEW part in this location, or SCRAP it?','ServiceBase 3g',|
                                        'mquest.jpg','\Cancel|Scrap|New')
                                    Of 3 ! New Button
                                        glo:Select2 = ''
                                        glo:Select3 = ''
                                        Pick_Locations
                                        If glo:Select1 <> ''
                                            If Access:STOCK.PrimeRecord() = Level:Benign
                                                sto:Part_Number     = epr:Part_Number
                                                sto:Description     = epr:Description
                                                sto:Supplier        = epr:Supplier
                                                sto:Purchase_Cost   = epr:Purchase_Cost
                                                sto:Sale_Cost       = epr:Sale_Cost
                                                sto:Retail_Cost     = epr:Retail_Cost
                                                sto:Shelf_Location  = glo:select2
                                                sto:Manufacturer    = job:Manufacturer
                                                sto:Location        = glo:Select1
                                                sto:Second_Location = glo:Select3
                                                If Access:STOCK.TryInsert() = Level:Benign
                                                    !Insert Successful
                                                    Local.CreateStockHistory('')
                                                    tmp:Delete = 1
                                                Else !If Access:STOCK.TryInsert() = Level:Benign
                                                    !Insert Failed
                                                End !If Access:STOCK.TryInsert() = Level:Benign
                                            End !If Access:STOCK.PrimeRecord() = Level:Benign
                                        End !If glo:Select1 <> ''
                                    Of 2 ! Scrap Button
                                        tmp:Delete  = 1
                                        tmp:Scrap   = 1
                                    Of 1 ! Cancel Button
                                    End ! Case Missive
                                End! If Access:STOCK.Tryfetch(sto:Location_Part_Description_Key) = Level:Benign

                            End !If glo:Select1 <> ''
                        Of 1 ! Cancel Button
                        End ! Case Missive

                    Else !If tmp:StockNumber
                        glo:Select1 = ''
                        glo:Select2 = ''
                        glo:Select3 = ''
                        Pick_Locations
                        If glo:Select1 <> ''
                            If Access:STOCK.PrimeRecord() = Level:Benign
                                sto:Part_Number     = epr:Part_Number
                                sto:Description     = epr:Description
                                sto:Supplier        = epr:Supplier
                                sto:Purchase_Cost   = epr:Purchase_Cost
                                sto:Sale_Cost       = epr:Sale_Cost
                                sto:Retail_Cost     = epr:Retail_Cost
                                sto:Shelf_Location  = glo:Select2
                                sto:Manufacturer    = job:Manufacturer
                                sto:Location        = glo:Select1
                                sto:Second_Location = glo:Select3
                                If Access:STOCK.TryInsert() = Level:Benign
                                    !Insert Successful
                                    Local.CreateStockHistory('')
                                    tmp:Delete = 1
                                Else !If Access:STOCK.TryInsert() = Level:Benign
                                    !Insert Failed
                                End !If Access:STOCK.TryInsert() = Level:Benign
                            End !If Access:STOCK.PrimeRecord() = Level:Benign
                        End !If glo:Select1 <> ''
                    End !If tmp:StockNumber
                Of 2 ! Scrap Button
                    tmp:Delete  = 1
                    tmp:Scrap   = 1
                Of 1 ! Cancel Button
                End ! Case Missive

            End !If func:Type = 0 And RapidLocation(sto:Location)
        END

    End !If epr:Adjustment = 'YES'
    If tmp:Scrap
        !Remove part from stock allocation tabe - L873 (DBH: 24-07-2003)
        If epr:UsedOnRepair
            RemoveFromStockAllocation(epr:Record_Number,'EST')
        End !If epr:UsedOnRepair

        locAuditNotes         = 'DESCRIPTION: ' & Clip(epr:Description)
        IF (tmp:StockNumber > 0)
            ! #11368 Add Extra Scrap Information (DBH: 11/06/2010)
            Access:STOCK.Clearkey(sto:Ref_Number_Key)
            sto:Ref_Number = tmp:StockNumber
            If (Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign)
                locAuditNotes = clip(locAuditNotes) & '<13,10>LOCATION: ' & clip(sto:Location) & |
                    '<13,10>QTY: ' & par:Quantity
            End
        END

        IF (AddToAudit(job:Ref_Number,'JOB','SCRAP ESTIMATE PART: ' & Clip(epr:Part_Number),locAuditNotes))
        END ! IF


                    ! #11368 Add Scrap information to Stock History (DBH: 11/06/2010)
        if (tmp:StockNumber > 0)
            If AddToStockHistory(sto:Ref_Number,'ADD','',par:Ref_Number,0,0,sto:Purchase_Cost,sto:Sale_Cost,sto:Retail_Cost,|
                    'PART SCRAPPED',|
                    'TYPE: ESTIMATE' & |
                    '<13,10>QUANTITY: ' & epr:Quantity)
            End ! '<13,10>NEW PART: ' & Clip(local:NewPartNumber) & ' - ' & Clip(local:NewDescription))
        end
    Else !If Scrap# = 1
        If tmp:Delete
            !Remove part from stock allocation tabe - L873 (DBH: 24-07-2003)
            If epr:UsedOnRepair
                RemoveFromStockAllocation(epr:Record_Number,'EST')
            End !If epr:UsedOnRepair
        End !If tmp:Delete
    End !If Scrap# = 1
    Return tmp:Delete
AddSolder       Routine
    If Access:STOFAULT.PrimeRecord() = Level:Benign
        stf:PartNumber  = par:Part_Number
        stf:Description = par:Description
        stf:Quantity    = par:Quantity
        stf:PurchaseCost    = par:Purchase_Cost
        stf:ModelNumber = job:Model_Number
        stf:IMEI        = job:ESN
        stf:Engineer    = job:Engineer
        Access:USERS.Clearkey(use:Password_Key)
        use:Password    = glo:Password
        If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
            !Found

        Else ! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
            !Error
        End !If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        stf:StoreUserCode   = use:User_Code
        stf:PartType        = 'SOL'
        If Access:STOFAULT.TryInsert() = Level:Benign
            !Insert Successful

        Else !If Access:STOFAULT.TryInsert() = Level:Benign
            !Insert Failed
            Access:STOFAULT.CancelAutoInc()
        End !If Access:STOFAULT.TryInsert() = Level:Benign
    End !If Access:STOFAULT.PrimeRecord() = Level:Benign



Local.CreateStockHistory          Procedure(String    func:Information)
Code
    If AddToStockHistory(sto:Ref_Number, | ! Ref_Number
                         'ADD', | ! Transaction_Type
                         par:Despatch_Note_Number, | ! Depatch_Note_Number
                         job:Ref_Number, | ! Job_Number
                         0, | ! Sales_Number
                         epr:Quantity, | ! Quantity
                         epr:Purchase_Cost, | ! Purchase_Cost
                         epr:Sale_Cost, | ! Sale_Cost
                         epr:Retail_Cost, | ! Retail_Cost
                         func:Information, | ! Notes
                         'ESTIMATE PART REMOVED FROM JOB') ! Information
        ! Added OK
        tmp:Delete = 1
    Else ! AddToStockHistory
        ! Error
    End ! AddToStockHistory
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
VATRate              PROCEDURE  (STRING func:AccountNumber,STRING func:Type) ! Declare Procedure
save_vatcode_id      USHORT,AUTO
save_tradeacc_id     USHORT,AUTO
save_subtracc_id     USHORT,AUTO
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    RETURN (VodacomClass.VatRate(func:AccountNumber,func:Type))

!    Relate:VATCODE.Open
!    Relate:SUBTRACC.Open
!    Relate:TRADEACC.Open
!
!    Save_SUBTRACC_ID = Access:SUBTRACC.SaveFile()
!    Save_TRADEACC_ID = Access:TRADEACC.SaveFile()
!    Save_VATCODE_ID = Access:VATCODE.SaveFile()
!
!    Return$ = 0
!
!    Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
!    sub:Account_Number = func:AccountNumber
!    If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
!        !Found
!        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
!        tra:Account_Number = sub:Main_Account_Number
!        If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
!            !Found
!            If tra:Use_Sub_Accounts = 'YES' And tra:Invoice_Sub_Accounts = 'YES'
!                Access:VATCODE.ClearKey(vat:Vat_Code_Key)
!                Case func:Type
!                    Of 'L'
!                        vat:Vat_Code    = sub:Labour_Vat_Code
!                    Of 'P'
!                        vat:Vat_Code    = sub:Parts_Vat_Code
!                End !Case func:Type
!                If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
!                    !Found
!                    Return$ = vat:Vat_Rate
!                Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
!                    !Error
!                End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
!
!            Else ! If tra:Use_Sub_Accounts = 'YES' And tra:Invoice_Sub_Accounts = 'YES'
!                Access:VATCODE.ClearKey(vat:Vat_Code_Key)
!                Case func:Type
!                    Of 'L'
!                        vat:Vat_Code    = tra:Labour_Vat_Code
!                    Of 'P'
!                        vat:Vat_Code    = tra:Parts_Vat_Code
!                End !Case func:Type
!                If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
!                    !Found
!                    Return$ = vat:Vat_Rate
!                Else ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
!                    !Error
!                End ! If Access:VATCODE.TryFetch(vat:Vat_Code_Key) = Level:Benign
!
!            End ! If tra:Use_Sub_Accounts = 'YES' And tra:Invoice_Sub_Accounts = 'YES'
!        Else ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
!            !Error
!        End ! If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
!    Else ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
!        !Error
!    End ! If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
!
!    Access:SUBTRACC.RestoreFile(Save_SUBTRACC_ID)
!    Access:TRADEACC.RestoreFile(Save_TRADEACC_ID)
!    Access:VATCODE.RestoreFile(Save_VATCODE_ID)
!
!    Relate:VATCODE.Close
!    Relate:SUBTRACC.Close
!    Relate:TRADEACC.Close
!
!    Return Return$
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
VATRateInvoice       PROCEDURE  (func:InvoiceNumber,func:Type) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
    inv:Invoice_Number  = func:InvoiceNumber
    If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
        !Found
        Case func:Type
            Of 'L'
                Return inv:Vat_Rate_Labour
            Of 'P'
                Return inv:Vat_Rate_Parts
        End !Case func:Type
    Else ! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
        !Error
    End !If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
InWarrantyMarkUp     PROCEDURE  (func:Manufacturer,func:SiteLocation) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Access:MANUFACT.Clearkey(man:Manufacturer_Key)
    man:Manufacturer    = func:Manufacturer
    If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
        !Found
        Access:MANMARK.Clearkey(mak:SiteLocationKey)
        mak:RefNumber   = man:RecordNumber
        mak:SiteLocation    = func:SiteLocation
        If Access:MANMARK.Tryfetch(mak:SiteLocationKey) = Level:Benign
            !Found
            Return mak:InWarrantyMarkup
        Else ! If Access:MANMARK.Tryfetch(mak:SiteLocationKey) = Level:Benign
            !Error
        End !If Access:MANMARK.Tryfetch(mak:SiteLocationKey) = Level:Benign
    Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
        !Error
    End !If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
    Return 0
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
LocationChange       PROCEDURE  (func:location)       ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Previous_Loc         STRING(30)
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
! Changing (DBH 07/08/2006) # 8081 - Don't need to check for the existance of the location
!    Access:LocInter.Open()
!    Access:LocInter.UseFile()
!    Access:LocatLog.Open()
!    Access:Locatlog.UseFile()
!    !Routine to change location and write to History!
!    !Hold Previous!
!    Previous_Loc = job:Location
!    !Update Real
!    !Check location exists!
!    Access:LocInter.ClearKey(loi:Location_Key)
!    loi:Location = func:location
!    IF Access:LocInter.Fetch(loi:Location_Key)
!      !Error!
!    ELSE
!      job:Location = func:location
!      !Write the change in the LocatLog!
!      Access:LocatLog.PrimeRecord()
!      lot:RefNumber = job:Ref_Number
!      lot:TheDate = TODAY()
!      lot:TheTime = CLOCK()
!      Access:USERS.Clearkey(use:Password_Key)
!      use:Password    = glo:password
!      If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
!         !Found
!      Else! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
!         !Error
!         !Assert(0,'<13,10>Fetch Error<13,10>')
!      End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
!      lot:UserCode =  use:User_Code
!      IF Previous_Loc = ''
!        Previous_Loc = 'N/A'
!      END
!      lot:PreviousLocation = Previous_Loc
!      lot:NewLocation = func:location
!      Access:LocatLog.Update()
!    END
!    Access:LocatLog.Close()
!    Access:LocInter.Close()
! to (DBH 07/08/2006) # 8081
    Access:LocatLog.Open()
    Access:Locatlog.UseFile()
    ! Routine to change location and write to History!
    ! Hold Previous!
    Previous_Loc = job:Location
    ! Update Real
    job:Location = func:location
    ! Write the change in the LocatLog!
    Access:LocatLog.PrimeRecord()
    lot:RefNumber = job:Ref_Number
    lot:TheDate   = TODAY()
    lot:TheTime   = CLOCK()

    Access:USERS.Clearkey(use:Password_Key)
    use:Password    = glo:password
    If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
    ! Found
    Else! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
       ! Error
    ! Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
    lot:UserCode =  use:User_Code

    IF Previous_Loc = ''
        Previous_Loc = 'N/A'
    END ! IF

    lot:PreviousLocation = Previous_Loc
    lot:NewLocation      = func:location

    Access:LocatLog.Update()
    Access:LocatLog.Close()
! End (DBH 07/08/2006) #8081

! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
CanJobInvoiceBePrinted PROCEDURE  (func:ShowMessages) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
!Can the invoice of the selected job be printed/created?


    !Found
    If job:Chargeable_Job <> 'YES'
        If func:ShowMessages
            Case Missive('Cannot Invoice!'&|
              '<13,10>'&|
              '<13,10>The selected job is NOT a Chargeable Job.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
            Of 1 ! OK Button
            End ! Case Missiveon
        End !If func:ShowMessages
        Return Level:Fatal
    End !If job:Chargeable_Job <> 'YES'

    Access:JOBSE.Clearkey(jobe:RefNumberKey)
    jobe:RefNumber  = job:Ref_Number
    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        !Found
        If ~glo:WebJob And ~jobe:HubRepair
            IF func:ShowMessages
                Case Missive('Cannot Invoice!'&|
                  '<13,10>'&|
                  '<13,10>The selected job is an RRC Job.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                Of 1 ! OK Button
                End ! Case Missive
            End !IF func:ShowMessages
            Return Level:Fatal
        End !If ~glo:WebJob And ~jobe:HubRepair
    Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        !Error
    End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

    If job:Bouncer = 'X'
        If func:ShowMessages
            Case Missive('Cannot Invoice!'&|
              '<13,10>'&|
              '<13,10>The selected job has been marked as a Bouncer.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
            Of 1 ! OK Button
            End ! Case Missive
        End !If func:ShowMessages
        Return Level:Fatal
    End !If job:Bouncer <> ''

    If job:Date_Completed = '' And job:Exchange_Unit_Number = 0
        If func:ShowMessages
            Case Missive('Cannot Invoice!'&|
              '<13,10>'&|
              '<13,10>The selected job has not been completed.','ServiceBase 3g',|
                           'mstop.jpg','/OK')
            Of 1 ! OK Button
            End ! Case Missive
        End !If func:ShowMessages
        Return Level:Fatal
    End !If job:Date_Completed = ''

    !There should now be now way to complete a job without a default structure

    If job:Ignore_Chargeable_Charges = 'YES'
        !Check the sub total depending on where the invoice
        !is being raised - L907 (DBH: 05-08-2003)
        If glo:WebJob
            If jobe:RRCCSubTotal = 0
                If func:ShowMessages
                    Case Missive('Cannot Invoice!'&|
                      '<13,10>'&|
                      '<13,10>The selected job has not been priced.','ServiceBase 3g',|
                                   'mstop.jpg','/OK')
                    Of 1 ! OK Button
                    End ! Case Missive
                End !If func:ShowMessages
                Return Level:Fatal

            End !If jobe:RRCCSubTotal = 0
        Else !If glo:WebJob
            If job:Sub_Total = 0
                If func:ShowMessages
                    Case Missive('Cannot Invoice!'&|
                      '<13,10>'&|
                      '<13,10>The selected job has not been priced.','ServiceBase 3g',|
                                   'mstop.jpg','/OK')
                    Of 1 ! OK Button
                    End ! Case Missive
                End !If func:ShowMessages
                Return Level:Fatal

            End !If job:Sub_Total = 0
        End !If glo:WebJob
    End !If job_ali:Ignore_Chargeable_Charges = 'YES' and job:Sub_Total = 0

    !Is this job a single invoice, and not part of a multiple invoice?
    If job:Invoice_Number <> ''
        Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
        inv:Invoice_Number =job:Invoice_Number
        If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
           !Found
            If inv:Invoice_Type <> 'SIN'
                If func:ShowMessages
                    Case Missive('Cannot Invoice!'&|
                      '<13,10>'&|
                      '<13,10>The selected job is NOT a Single Invoice. It may be part of a Multiple Invoice.','ServiceBase 3g',|
                                   'mstop.jpg','/OK')
                    Of 1 ! OK Button
                    End ! Case Missive
                End !If func:ShowMessages
                Return Level:Fatal
            End !If inv:Invoice_Type <> 'SIN'
        Else! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
            !Error
            Assert(0,'<13,10>Fetch Error<13,10>')
            Return Level:Fatal
        End! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
    End !If job:Invoice_Number <> ''

    If InvoiceSubAccounts(job:Account_Number)
        access:vatcode.clearkey(vat:vat_code_key)
        vat:vat_code = sub:labour_vat_code
        if access:vatcode.fetch(vat:vat_code_key)
            If func:ShowMessages
                Case Missive('Cannot Invoice!'&|
                  '<13,10>'&|
                  '<13,10>A V.A.T. Rate has not been setup for this Sub Account.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                Of 1 ! OK Button
                End ! Case Missive
            End !If func:ShowMessages
            Return Level:Fatal
        end!if access:vatcode.fetch(vat:vat_code_key)
        access:vatcode.clearkey(vat:vat_code_key)
        vat:vat_code = sub:parts_vat_code
        if access:vatcode.fetch(vat:vat_code_key)
            If func:ShowMessages
                Case Missive('Cannot Invoice!'&|
                  '<13,10>'&|
                  '<13,10>A V.A.T. Rate has not been setup for this Head Account.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                Of 1 ! OK Button
                End ! Case Missive
            End !If func:ShowMessages
            Return Level:Fatal
        end!if access:vatcode.fetch(vat:vat_code_key)
    Else !If InvoiceSubAccounts(job:Account_Number)
        access:vatcode.clearkey(vat:vat_code_key)
        vat:vat_code = tra:labour_vat_code
        if access:vatcode.fetch(vat:vat_code_key)
            If func:ShowMessages
                Case Missive('Cannot Invoice!'&|
                  '<13,10>'&|
                  '<13,10>A V.A.T. Rate has not been setup for this Sub Account.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                Of 1 ! OK Button
                End ! Case Missive

            End !If func:ShowMessages
            Return Level:Fatal
        end!if access:vatcode.fetch(vat:vat_code_key)
        access:vatcode.clearkey(vat:vat_code_key)
        vat:vat_code = tra:parts_vat_code
        if access:vatcode.fetch(vat:vat_code_key)
            If Func:ShowMessages
                Case Missive('Cannot Invoice!'&|
                  '<13,10>'&|
                  '<13,10>A V.A.T. Rate has not been setup for this Head Account.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                Of 1 ! OK Button
                End ! Case Missive
            End !If Func:ShowMessages
            Return Level:Fatal
        end!if access:vatcode.fetch(vat:vat_code_key)

    End !If InvoiceSubAccounts(job:Account_Number)

    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
SentToHub            PROCEDURE  (func:RefNumber)      ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    RETURN (VodacomClass.JobSentToHub(func:RefNumber))

!    If jobe:HubRepair Or jobe:HubRepairDate <> ''
!        Return Level:Fatal
!    Else !If jobe:HubRepair
!        Access:LOCATLOG.ClearKey(lot:NewLocationKey)
!        lot:RefNumber   = func:RefNumber
!        lot:NewLocation = GETINI('RRC','ARCLocation',,CLIP(PATH())&'\SB2KDEF.INI')
!        If Access:LOCATLOG.TryFetch(lot:NewLocationKey) = Level:Benign
!            Return Level:Fatal
!        End !If Access:LOCATLOG.TryFetch(lot:NewLocationKey)
!    End !If jobe:HubRepair
!
!    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
SendFileToClient     PROCEDURE  (SentFilename)        ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
LocalFilename        STRING(255)
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
!Send a file to client
    localFilename = SentFilename
    if exists(LocalFilename)

        flq:Filename = LocalFilename
        add(FileListQueue)
        Return" = ClarioNET:SendFilesToClient(1,0)

    ELSE
        Case Missive('Error - Filename does not exist.','ServiceBase 3g',|
                       'mstop.jpg','/OK')
            Of 1 ! OK Button
        End ! Case Missive
    END
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
