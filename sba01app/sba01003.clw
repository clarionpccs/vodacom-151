

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01003.INC'),ONCE        !Local module procedure declarations
                     END


CountBouncer         PROCEDURE                        ! Declare Procedure
tmp:DateBooked       DATE
tmp:count            LONG
save_job2_id         USHORT,AUTO
save_joo_id          USHORT,AUTO
save_mfo_id          USHORT,AUTO
save_wpr_id          USHORT,AUTO
save_par_id          USHORT,AUTO
save_wob_id          USHORT,AUTO
tmp:IMEI             STRING(30)
tmp:Manufacturer     STRING(30)
tmp:IgnoreChargeable BYTE(0)
tmp:IgnoreWarranty   BYTE(0)
locBouncerType       BYTE
locBouncerTime       LONG
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
!    MAP
!OutFaultExcluded         Procedure(Long local:JobNumber,String local:WarrantyJob,String local:ChargeableJob),Byte
!BouncerBit               Procedure(BYTE fType),Byte
!    END
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    RETURN (VodacomClass.CountJobBouncers())

!    !Pass the Job Number. Use this to get the job's IMEI Number, Date Booked and Manufacturer
!    !From that count how many times the IMEI number has been booked in before
!    tmp:IgnoreChargeable = GETINI('BOUNCER','IgnoreChargeable',,CLIP(PATH())&'\SB2KDEF.INI')
!    tmp:IgnoreWarranty  = GETINI('BOUNCER','IgnoreWarranty',,CLIP(PATH())&'\SB2KDEF.INI')
!
!    Access:MANUFACT.Clearkey(man:Manufacturer_Key)
!    man:Manufacturer = job:Manufacturer
!    IF (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign)
!    END
!
!
!    CheckBouncer# = 1
!    If func:CJob = 'YES'
!        If tmp:IgnoreChargeable
!            CheckBouncer# = 0
!        Else !If GETINI('BOUNCER','IgnoreChargeable',,CLIP(PATH())&'\SB2KDEF.INI') = 1
!            Access:CHARTYPE.ClearKey(cha:Charge_Type_Key)
!            cha:Charge_Type = func:CCharge
!            If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!                !Found
!                If cha:ExcludeFromBouncer
!                    CheckBouncer# = 0
!                Else !If cha:ExcludeFromBouncer
!                    Access:REPTYDEF.ClearKey(rtd:ChaManRepairTypeKey)
!                    rtd:Manufacturer = job:Manufacturer
!                    rtd:Chargeable   = 'YES'
!                    rtd:Repair_Type  = func:CRepair
!                    If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
!                        !Found
!                        If rtd:ExcludeFromBouncer
!                            CheckBouncer# = 0
!                        End !If rtd:ExcludeFromBouncer
!                    Else!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
!                        !Error
!                        !Assert(0,'<13,10>Fetch Error<13,10>')
!                    End!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
!                End !If cha:ExcludeFromBouncer
!            Else!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!                !Error
!                !Assert(0,'<13,10>Fetch Error<13,10>')
!            End!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!        End !If GETINI('BOUNCER','IgnoreChargeable',,CLIP(PATH())&'\SB2KDEF.INI') = 1
!    End !If func:CJob = 'YES'
!
!    !Is the job's outfault excluded?
!    If CheckBouncer#
!        If OutFaultExcluded(job:Ref_Number,job:Warranty_job,job:Chargeable_Job)
!            CheckBouncer# = 0
!        End !If Local.OutFaultExcluded()
!    End !If CheckBouncer#
!
!    !Is the job's infault excluded?
!    If CheckBouncer#
!        Access:MANFAULT.ClearKey(maf:InFaultKey)
!        maf:Manufacturer = job:Manufacturer
!        maf:InFault      = 1
!        If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
!            Access:MANFAULO.ClearKey(mfo:Field_Key)
!            mfo:Manufacturer = job:Manufacturer
!            mfo:Field_Number = maf:Field_Number
!            Case maf:Field_Number
!                Of 1
!                    mfo:Field        = job:Fault_Code1
!                Of 2
!                    mfo:Field        = job:Fault_Code2
!                Of 3
!                    mfo:Field        = job:Fault_Code3
!                Of 4
!                    mfo:Field        = job:Fault_Code4
!                Of 5
!                    mfo:Field        = job:Fault_Code5
!                Of 6
!                    mfo:Field        = job:Fault_Code6
!                Of 7
!                    mfo:Field        = job:Fault_Code7
!                Of 8
!                    mfo:Field        = job:Fault_Code8
!                Of 9
!                    mfo:Field        = job:Fault_Code9
!                Of 10
!                    mfo:Field        = job:Fault_Code10
!                Of 11
!                    mfo:Field        = job:Fault_Code11
!                Of 12
!                    mfo:Field        = job:Fault_Code12
!            End !Case maf:Field_Number
!            If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
!                !Found
!                If mfo:ExcludeFromBouncer
!                    CheckBouncer# = 0
!                End !If mfo:ExcludeBouncer
!            Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
!                !Error
!                !Assert(0,'<13,10>Fetch Error<13,10>')
!            End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
!
!        Else!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
!            !Error
!            !Assert(0,'<13,10>Fetch Error<13,10>')
!        End!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
!    End !If CheckBouncer#
!
!    !Lets write the bouncers into a memory queue
!    Free(glo:Queue20)
!    Clear(glo:Queue20)
!
!
!    If CheckBouncer#
!        If func:WJob = 'YES'
!            If tmp:IgnoreWarranty
!                CheckBouncer# = 0
!            Else !If GETINI('BOUNCER','IgnoreChargeable',,CLIP(PATH())&'\SB2KDEF.INI') = 1
!
!                Access:CHARTYPE.ClearKey(cha:Charge_Type_Key)
!                cha:Charge_Type = func:WCharge
!                If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!                    !Found
!                    If cha:ExcludeFromBouncer
!                        CheckBouncer# = 0
!                    Else !If cha:ExcludeFromBouncer
!                        Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
!                        rtd:Manufacturer = job:Manufacturer
!                        rtd:Warranty     = 'YES'
!                        rtd:Repair_Type  = func:WRepair
!                        If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
!                            !Found
!                            If rtd:ExcludeFromBouncer
!                                CheckBouncer# = 0
!                            End !If rtd:ExcludeFromBouncer
!                        Else!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
!                            !Error
!                            !Assert(0,'<13,10>Fetch Error<13,10>')
!                        End!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
!                    End !If cha:ExcludeFromBouncer
!                Else!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!                    !Error
!                    !Assert(0,'<13,10>Fetch Error<13,10>')
!                End!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!
!                ! #11887 This is warranty. Check if the manufacturer is excluded. (Bryan: 24/05/2011)
!                IF (CheckBouncer#)
!                    IF (man:DoNotBounceWarranty = 1)
!                        CheckBouncer# = 0
!                    END
!                END ! IF (CheckBouncer#)
!
!            End !If GETINI('BOUNCER','IgnoreChargeable',,CLIP(PATH())&'\SB2KDEF.INI') = 1
!        End !If func:CJob = 'YES'
!    End !If CheckBouncer#
!
!    If CheckBouncer#
!        tmp:count    = 0
!        If f_IMEI <> 'N/A' And f_IMEI <> ''
!            Set(defaults)
!            Access:Defaults.Next()
!
!            setcursor(cursor:wait)
!            save_job2_id = access:jobs2_alias.savefile()
!            access:jobs2_alias.clearkey(job2:esn_key)
!            job2:esn = f_imei
!            set(job2:esn_key,job2:esn_key)
!            loop
!                if access:jobs2_alias.next()
!                   break
!                end !if
!                if job2:esn <> f_imei      |
!                    then break.  ! end if
!                yldcnt# += 1
!                if yldcnt# > 25
!                   yield() ; yldcnt# = 0
!                end !if
!
!                If job2:Cancelled = 'YES'
!                    Cycle
!                End !If job2:Cancelled = 'YES'
!
!                If job2:Exchange_Unit_Number <> ''
!                    Cycle
!                End !If job2:Exchange_Unit_Number <> ''
!
!                If job2:Chargeable_Job = 'YES'
!                    If tmp:IgnoreChargeable
!                        Cycle
!                    End !If GETINI('BOUNCER','IgnoreChargeable',,CLIP(PATH())&'\SB2KDEF.INI') = 1
!
!                    Access:CHARTYPE.ClearKey(cha:Charge_Type_Key)
!                    cha:Charge_Type = job2:Charge_Type
!                    If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!                        !Found
!                        If cha:ExcludeFromBouncer
!                            Cycle
!                        Else !If cha:ExcludeFromBouncer
!                            If job2:Repair_Type <> ''
!                                Access:REPTYDEF.ClearKey(rtd:ChaManRepairTypeKey)
!                                rtd:Manufacturer = job:Manufacturer
!                                rtd:Chargeable   = 'YES'
!                                rtd:Repair_Type  = job2:Repair_Type
!                                If Access:REPTYDEF.TryFetch(rtd:ChaManRepairTypeKey) = Level:Benign
!                                    !Found
!                                    If rtd:ExcludeFromBouncer
!                                        Cycle
!                                    End !If rtd:ExcludeFromBouncer
!                                Else!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
!                                    !Error
!                                    !Assert(0,'<13,10>Fetch Error<13,10>')
!                                End!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
!
!                            End !If job2:Repair_Type <> ''
!                        End !If cha:ExcludeFromBouncer
!                    Else!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!                        !Error
!                        !Assert(0,'<13,10>Fetch Error<13,10>')
!                    End!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!                End !If func:CJob = 'YES'
!
!
!                If job2:Warranty_Job = 'YES'
!                    If tmp:IgnoreWarranty
!                        Cycle
!                    End !If tmp:IgnoreWarranty
!
!                    Access:CHARTYPE.ClearKey(cha:Charge_Type_Key)
!                    cha:Charge_Type = job2:Warranty_Charge_Type
!                    If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!                        !Found
!                        If cha:ExcludeFromBouncer
!                            Cycle
!                        Else !If cha:ExcludeFromBouncer
!                            If job2:Repair_Type_Warranty <> ''
!                                Access:REPTYDEF.ClearKey(rtd:WarManRepairTypeKey)
!                                rtd:Manufacturer = job:Manufacturer
!                                rtd:Warranty     = 'YES'
!                                rtd:Repair_Type  = job2:Repair_Type_Warranty
!                                If Access:REPTYDEF.TryFetch(rtd:WarManRepairTypeKey) = Level:Benign
!                                    !Found
!                                    If rtd:ExcludeFromBouncer
!                                        Cycle
!                                    End !If rtd:ExcludeFromBouncer
!                                Else!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
!                                    !Error
!                                    !Assert(0,'<13,10>Fetch Error<13,10>')
!                                End!If Access:REPTYDEF.TryFetch(rtd:Chargeable_Key) = Level:Benign
!                            End !If job2:Repair_Type_Warranty <> ''
!                        End !If cha:ExcludeFromBouncer
!                    Else!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!                        !Error
!                        !Assert(0,'<13,10>Fetch Error<13,10>')
!                    End!If Access:CHARTYPE.TryFetch(cha:Charge_Type_Key) = Level:Benign
!                End !If func:CJob = 'YES'
!
!                IF (man:UseBouncerRules = 1 AND func:WJob = 'YES' AND man:BouncerPeriodIMEI > 0)
!                    ! #11887 Manufacturer Bouncer Rules Apply.
!                    ! Bouncer IMEI period set. So don't get faults, just check date (Bryan: 24/05/2011)
!                    IF (BouncerBit(1) = 1)
!                        ! Job is a bouncer based on the IMEI date. No need to carry on
!                        CYCLE
!                    END
!                END
!
!                !Has job got the same In Fault?
!
!                checkInFault# = 1
!                IF (man:UseBouncerRules = 1 AND man:BouncerInFault = 0 AND func:WJob = 'YES')
!                    ! #11887 Manufacturer Bouncer Rules Apply.
!                    ! Should In Fault be compare (Bryan: 24/05/2011)
!                    checkInFault# = 0
!                END
!
!                IF (checkInFault# = 1)
!                    Access:MANFAULT.ClearKey(maf:InFaultKey)
!                    maf:Manufacturer = job:Manufacturer
!                    maf:InFault      = 1
!                    If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
!                        !Found
!                        Case maf:Field_Number
!                            Of 1
!                                If job2:Fault_Code1 <> job:Fault_Code1
!                                    Cycle
!                                End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                            Of 2
!                                If job2:Fault_Code2 <> job:Fault_Code2
!                                    Cycle
!                                End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                            Of 3
!                                If job2:Fault_Code3 <> job:Fault_Code3
!                                    Cycle
!                                End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                            Of 4
!                                If job2:Fault_Code4 <> job:Fault_Code4
!                                    Cycle
!                                End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                            Of 5
!                                If job2:Fault_Code5 <> job:Fault_Code5
!                                    Cycle
!                                End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                            Of 6
!                                If job2:Fault_Code6 <> job:Fault_Code6
!                                    Cycle
!                                End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                            Of 7
!                                If job2:Fault_Code7 <> job:Fault_Code7
!                                    Cycle
!                                End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                            Of 8
!                                If job2:Fault_Code8 <> job:Fault_Code8
!                                    Cycle
!                                End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                            Of 9
!                                If job2:Fault_Code9 <> job:Fault_Code9
!                                    Cycle
!                                End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                            Of 10
!                                If job2:Fault_Code10 <> job:Fault_Code10
!                                    Cycle
!                                End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                            Of 11
!                                If job2:Fault_Code11 <> job:Fault_Code11
!                                    Cycle
!                                End !If job2:Fault_Code1 <> job:Fault_Code1
!
!                            Of 12
!                                If job2:Fault_Code12 <> job:Fault_Code12
!                                    Cycle
!                                End !If job2:Fault_Code1 <> job:Fault_Code1
!                        End !Case maf:Field_Number
!                        !Is the infault excluded?
!                        Access:MANFAULO.ClearKey(mfo:Field_Key)
!                        mfo:Manufacturer = job:Manufacturer
!                        mfo:Field_Number = maf:Field_Number
!                        Case maf:Field_Number
!                            Of 1
!                                mfo:Field        = job2:Fault_Code1
!                            Of 2
!                                mfo:Field        = job2:Fault_Code2
!                            Of 3
!                                mfo:Field        = job2:Fault_Code3
!                            Of 4
!                                mfo:Field        = job2:Fault_Code4
!                            Of 5
!                                mfo:Field        = job2:Fault_Code5
!                            Of 6
!                                mfo:Field        = job2:Fault_Code6
!                            Of 7
!                                mfo:Field        = job2:Fault_Code7
!                            Of 8
!                                mfo:Field        = job2:Fault_Code8
!                            Of 9
!                                mfo:Field        = job2:Fault_Code9
!                            Of 10
!                                mfo:Field        = job2:Fault_Code10
!                            Of 11
!                                mfo:Field        = job2:Fault_Code11
!                            Of 12
!                                mfo:Field        = job2:Fault_Code12
!                        End !Case maf:Field_Number
!                        If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
!                            !Found
!                            If mfo:ExcludeFromBouncer
!                                Cycle
!                            End !If mfo:ExcludeBouncer
!                        Else!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
!                            !Error
!                            !Assert(0,'<13,10>Fetch Error<13,10>')
!                        End!If Access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign
!
!                    Else!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
!                        !Error
!                        !Assert(0,'<13,10>Fetch Error<13,10>')
!                    End!If Access:MANFAULT.TryFetch(maf:InFaultKey) = Level:Benign
!                END ! IF (checkInFault# = 1)
!
!                !Are the outfaults excluded?
!                checkOutFault# = 1
!                IF (man:UseBouncerRules = 1 AND man:BouncerOutFault = 0 AND func:WJob = 'YES')
!                    ! #11887 Manufacturer Bouncer Rules Apply.
!                    ! Should Out Fault be compare (Bryan: 24/05/2011)
!                    checkOutFault# = 0
!                END
!                IF (checkOutFault# = 1)
!                    If OutFaultExcluded(job2:Ref_Number,job2:Warranty_job,job2:Chargeable_Job)
!                        Cycle
!                    End !If Local.OutFaultExcluded()
!                END
!
!                If job2:ref_number <> f_refnumber
!                    IF (BouncerBit(0))
!                        ! Call normal bouncer rules based on default dates
!                    END
!                End!If job2:esn <> job2:ref_number
!            end !loop
!            access:jobs2_alias.restorefile(save_job2_id)
!            setcursor()
!        End!If access:jobs2_alias.clearkey(job2:RefNumberKey) = Level:Benign
!    End !If CheckBouncer#
!    Return tmp:count
! Routines
! Local Procedures
!BouncerBit     Procedure(BYTE fType)
!CODE
!    locBouncerType = GETINI('BOUNCER','BouncerType',,CLIP(PATH())&'\SB2KDEF.INI')
!
!    locBouncerTime = def:BouncerTime
!    IF (man:UseBouncerRules = 1 AND func:WJob = 'YES')
!        ! #11887 Manufacturer Bouncer Rules Apply.
!        ! Use Manufacturer Bouncer Type (Bryan: 24/05/2011)
!        locBouncerType = man:BouncerType
!        locBouncerTime = man:BouncerPeriod
!    END
!
!    IF (fType = 1) ! User Manufacturer IMEI time
!        locBouncerTime = man:BouncerPeriodIMEI
!    END
!
!    
!    CASE locBouncerType
!    Of 2 !Despatched Date
!        If job2:Date_Despatched <> ''
!            If job2:date_despatched + locBouncerTime > f_datebooked And job2:date_despatched <= f_datebooked
!                tmp:count += 1
!                GLO:Pointer20 = job2:Ref_Number
!                ADd(glo:Queue20)
!                Return 1
!            End!If job2:date_booked + man:warranty_period < Today()
!
!        Else !If job2:Date_Despatched <> ''
!
!            !Need to get the webjob record for the bouncer job
!            !Will save the file, and then restore it afterwards.
!            !Therefore, no records should be lost.
!
!            Save_wob_ID = Access:WEBJOB.SaveFile()
!
!            Access:WEBJOB.Clearkey(wob:RefNumberKey)
!            wob:RefNumber   = job2:Ref_Number
!            If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
!                !Found
!
!            Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
!                !Error
!            End !If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
!
!
!            If wob:DateJobDespatched <> ''
!                If wob:DateJobDespatched + locBouncerTime > f_datebooked And wob:DateJobDespatched <= f_datebooked
!
!                    tmp:count += 1
!                    GLO:Pointer20 = job2:Ref_Number
!                    ADd(glo:Queue20)
!                    Return 1
!                End!If job2:date_booked + man:warranty_period < Today()
!
!            End !If wob:JobDateDespatched <> ''
!
!            Access:WEBJOB.RestoreFile(Save_wob_ID)
!
!        End !If job2:Date_Despatched <> ''
!    Of 1 !Completed Date
!        If job2:date_completed + locBouncerTime > f_datebooked And job2:date_completed <= f_datebooked
!            tmp:count += 1
!            GLO:Pointer20 = job2:Ref_Number
!            ADd(glo:Queue20)
!            Return 1
!        End!If job2:date_booked + man:warranty_period < Today()
!    Else !Booking Date
!        If job2:date_booked + locBouncerTime > f_datebooked And job2:date_booked <= f_datebooked
!            tmp:count += 1
!            GLO:Pointer20 = job2:Ref_Number
!            ADd(glo:Queue20)
!            Return 1
!        End!If job2:date_booked + man:warranty_period < Today()
!    End !Case GETINI('BOUNCER','BouncerType',,CLIP(PATH())&'\SB2KDEF.INI')
!    Return 0
!OutFaultExcluded       Procedure(Long local:JobNumber,String local:WarrantyJob,String local:ChargeableJob)
!local:FaultCode     String(30)
!Code
!    !Loop through outfaults
!    Save_joo_ID = Access:JOBOUTFL.SaveFile()
!    Access:JOBOUTFL.ClearKey(joo:JobNumberKey)
!    joo:JobNumber = local:JobNumber
!    Set(joo:JobNumberKey,joo:JobNumberKey)
!    Loop
!        If Access:JOBOUTFL.NEXT()
!           Break
!        End !If
!        If joo:JobNumber <> local:JobNumber      |
!            Then Break.  ! End If
!        !Which is the main out fault?
!        Access:MANFAULT.ClearKey(maf:MainFaultKey)
!        maf:Manufacturer = job:Manufacturer
!        maf:MainFault    = 1
!        If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
!            !Lookup the Fault Code lookup to see if it's excluded
!            Save_mfo_ID = Access:MANFAULO.SaveFile()
!            Access:MANFAULO.ClearKey(mfo:Field_Key)
!            mfo:Manufacturer = job:Manufacturer
!            mfo:Field_Number = maf:Field_Number
!            mfo:Field        = joo:FaultCode
!            Set(mfo:Field_Key,mfo:Field_Key)
!            Loop
!                If Access:MANFAULO.NEXT()
!                   Break
!                End !If
!                If mfo:Manufacturer <> job:Manufacturer      |
!                Or mfo:Field_Number <> maf:Field_Number      |
!                Or mfo:Field        <> joo:FaultCode      |
!                    Then Break.  ! End If
!                If Clip(mfo:Description) = Clip(joo:Description)
!                    !Make sure the descriptions match in case of duplicates
!                    If mfo:ExcludeFromBouncer
!                        Return Level:Fatal
!                    End !If mfo:ExcludeFromBoucer
!                    Break
!                End !If Clip(mfo:Description) = Clip(joo:Description)
!            End !Loop
!            Access:MANFAULO.RestoreFile(Save_mfo_ID)
!
!        Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
!            !Error
!        End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
!
!    End !Loop
!    Access:JOBOUTFL.RestoreFile(Save_joo_ID)
!
!    !Is an outfault records on parts for this manufacturer
!    Access:MANFAUPA.ClearKey(map:MainFaultKey)
!    map:Manufacturer = job:Manufacturer
!    map:MainFault    = 1
!    If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
!        !Found
!        !Loop through the parts as see if any of the faults codes are excluded
!        If local:WarrantyJob = 'YES'
!
!            Save_wpr_ID = Access:WARPARTS.SaveFile()
!            Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
!            wpr:Ref_Number  = local:JobNumber
!            Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
!            Loop
!                If Access:WARPARTS.NEXT()
!                   Break
!                End !If
!                If wpr:Ref_Number  <> local:JobNumber      |
!                    Then Break.  ! End If
!                !Which is the main out fault?
!                Access:MANFAULT.ClearKey(maf:MainFaultKey)
!                maf:Manufacturer = job:Manufacturer
!                maf:MainFault    = 1
!                If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
!                    !Lookup the Fault Code lookup to see if it's excluded
!
!                    !Work out which part fault code is the outfault
!                    !and use that for the lookup
!                    Case map:Field_Number
!                        Of 1
!                            local:FaultCode        = wpr:Fault_Code1
!                        Of 2
!                            local:FaultCode        = wpr:Fault_Code2
!                        Of 3
!                            local:FaultCode        = wpr:Fault_Code3
!                        Of 4
!                            local:FaultCode        = wpr:Fault_Code4
!                        Of 5
!                            local:FaultCode        = wpr:Fault_Code5
!                        Of 6
!                            local:FaultCode        = wpr:Fault_Code6
!                        Of 7
!                            local:FaultCode        = wpr:Fault_Code7
!                        Of 8
!                            local:FaultCode        = wpr:Fault_Code8
!                        Of 9
!                            local:FaultCode        = wpr:Fault_Code9
!                        Of 10
!                            local:FaultCode        = wpr:Fault_Code10
!                        Of 11
!                            local:FaultCode        = wpr:Fault_Code11
!                        Of 12
!                            local:FaultCode        = wpr:Fault_Code12
!                    End !Case map:Field_Number
!
!                    Save_mfo_ID = Access:MANFAULO.SaveFile()
!                    Access:MANFAULO.ClearKey(mfo:Field_Key)
!                    mfo:Manufacturer = job:Manufacturer
!                    mfo:Field_Number = maf:Field_Number
!                    mfo:Field        = local:FaultCode
!                    Set(mfo:Field_Key,mfo:Field_Key)
!                    Loop
!                        If Access:MANFAULO.NEXT()
!                           Break
!                        End !If
!                        If mfo:Manufacturer <> job:Manufacturer      |
!                        Or mfo:Field_Number <> maf:Field_Number      |
!                        Or mfo:Field        <> local:FaultCode      |
!                            Then Break.  ! End If
!                        !This fault relates to a specific part fault code number??
!                        If mfo:RelatedPartCode <> 0 And map:UseRelatedJobCode
!                            If mfo:RelatedPartCode <> maf:Field_Number
!                                Cycle
!                            End !If mfo:RelatedPartCode <> maf:Field_Number
!                        End !If mfo:RelatedPartCode <> 0
!                        IF mfo:ExcludeFromBouncer
!                            Return Level:Fatal
!                        End !IF mfo:ExcludeFromBouncer
!                    End !Loop
!                    Access:MANFAULO.RestoreFile(Save_mfo_ID)
!
!                Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
!                    !Error
!                End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
!
!            End !Loop
!            Access:WARPARTS.RestoreFile(Save_wpr_ID)
!        End !If job:Warranty_Job = 'YES'
!
!        If local:ChargeableJob = 'YES'
!            !Loop through the parts as see if any of the faults codes are excluded
!            Save_par_ID = Access:PARTS.SaveFile()
!            Access:PARTS.ClearKey(par:Part_Number_Key)
!            par:Ref_Number  = local:JobNumber
!            Set(par:Part_Number_Key,par:Part_Number_Key)
!            Loop
!                If Access:PARTS.NEXT()
!                   Break
!                End !If
!                If par:Ref_Number  <> local:JobNumber      |
!                    Then Break.  ! End If
!                !Which is the main out fault?
!                Access:MANFAULT.ClearKey(maf:MainFaultKey)
!                maf:Manufacturer = job:Manufacturer
!                maf:MainFault    = 1
!                If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
!                    !Lookup the Fault Code lookup to see if it's excluded
!
!                    !Work out which part fault code is the outfault
!                    !and use that for the lookup
!                    Case map:Field_Number
!                        Of 1
!                            local:FaultCode        = par:Fault_Code1
!                        Of 2
!                            local:FaultCode        = par:Fault_Code2
!                        Of 3
!                            local:FaultCode        = par:Fault_Code3
!                        Of 4
!                            local:FaultCode        = par:Fault_Code4
!                        Of 5
!                            local:FaultCode        = par:Fault_Code5
!                        Of 6
!                            local:FaultCode        = par:Fault_Code6
!                        Of 7
!                            local:FaultCode        = par:Fault_Code7
!                        Of 8
!                            local:FaultCode        = par:Fault_Code8
!                        Of 9
!                            local:FaultCode        = par:Fault_Code9
!                        Of 10
!                            local:FaultCode        = par:Fault_Code10
!                        Of 11
!                            local:FaultCode        = par:Fault_Code11
!                        Of 12
!                            local:FaultCode        = par:Fault_Code12
!                    End !Case map:Field_Number
!
!                    Save_mfo_ID = Access:MANFAULO.SaveFile()
!                    Access:MANFAULO.ClearKey(mfo:Field_Key)
!                    mfo:Manufacturer = job:Manufacturer
!                    mfo:Field_Number = maf:Field_Number
!                    mfo:Field        = local:FaultCode
!                    Set(mfo:Field_Key,mfo:Field_Key)
!                    Loop
!                        If Access:MANFAULO.NEXT()
!                           Break
!                        End !If
!                        If mfo:Manufacturer <> job:Manufacturer      |
!                        Or mfo:Field_Number <> maf:Field_Number      |
!                        Or mfo:Field        <> local:FaultCode      |
!                            Then Break.  ! End If
!                        !This fault relates to a specific part fault code number??
!                        If mfo:RelatedPartCode <> 0 And map:UseRelatedJobCode
!                            If mfo:RelatedPartCode <> maf:Field_Number
!                                Cycle
!                            End !If mfo:RelatedPartCode <> maf:Field_Number
!                        End !If mfo:RelatedPartCode <> 0
!                        IF mfo:ExcludeFromBouncer
!                            Return Level:Fatal
!                        End !IF mfo:ExcludeFromBouncer
!                    End !Loop
!                    Access:MANFAULO.RestoreFile(Save_mfo_ID)
!
!                Else!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
!                    !Error
!                End!If Access:MANFAULT.TryFetch(maf:MainFaultKey) = Level:Benign
!
!            End !Loop
!            Access:PARTS.RestoreFile(Save_par_ID)
!        End !If job:Chargeable_Job = 'YES'
!    Else!If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
!        !Error
!    End!If Access:MANFAUPA.TryFetch(map:MainFaultKey) = Level:Benign
!
!    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
ForceDespatch        PROCEDURE                        ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
!Force a job to be marked as despatched
    job:Date_Despatched     = Today()
    job:Consignment_Number  = 'N/A'
    access:users.clearkey(use:Password_Key)
    use:Password        = glo:Password
    access:users.tryfetch(use:Password_Key)
    job:Despatch_User    = use:User_Code
    Get(desbatch,0)
    If access:desbatch.primerecord() = Level:Benign
        If access:desbatch.insert()
            access:desbatch.cancelautoinc()
        End!If access:desbatch.insert()
    End!If access:desbatch.primerecord() = Level:Benign
    job:Despatch_Number  = dbt:Batch_Number
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
ToBeLoaned           PROCEDURE                        ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
!Will this or does this job have an Loan unit on it?
!1 = Loan unit is already attached to the job
!2 = Loan unit is NOT attached, but the transit type is marked 'Loan Unit Required'
!3 = Loan unit is NOT attached, but the Loan Status is 103 Loan Unit Required
    If job:Loan_unit_number <> ''
        Return 1
    End!If job:Loan_unit_number <> ''

    access:trantype.clearkey(trt:transit_type_key)
    trt:transit_type = job:transit_type
    if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
        If trt:Loan_unit = 'YES'
            Return 2
        End!If trt:Loan = 'YES'
    End!if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign

    If Sub(job:Loan_status,1,3) = '103'
        Return 3
    End!If Sub(job:Loan_status,1,3) = '108'

    Return Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
TradeList            PROCEDURE  (f_type)              ! Declare Procedure
save_inv_id          USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Tahoma',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Tahoma',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Tahoma',8,,)
     end
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Case f_type
        Of 'INVOICE'
            recordspercycle     = 25
            recordsprocessed    = 0
            percentprogress     = 0
            setcursor(cursor:wait)
            open(progresswindow)
            progress:thermometer    = 0
            ?progress:pcttext{prop:text} = '0% Completed'
            ?progress:userstring{prop:text} = 'Building Account List'

            recordstoprocess    = Records(invoice)

            set(tradetmp)
            loop
                if access:tradetmp.next()
                   break
                end !if
                Delete(tradetmp)
            end !loop

            setcursor(cursor:wait)

            save_inv_id = access:invoice.savefile()
            access:invoice.clearkey(inv:Account_Number_Key)
            set(inv:Account_Number_Key)
            loop
                if access:invoice.next()
                   break
                end !if
                Do getNextRecord2

                access:tradetmp.clearkey(tratmp:accountnokey)
                tratmp:account_number = inv:Account_Number
                if access:tradetmp.tryfetch(tratmp:accountnokey)
                    get(tradetmp,0)
                    if access:tradetmp.primerecord() = Level:Benign
                        tratmp:account_number = inv:Account_Number
                        if access:tradetmp.insert()
                           access:tradetmp.cancelautoinc()
                        end
                    end!if access:tradetmp.primerecord() = Level:Benign
                end!if access:tradetmp.tryfetch(tratmp:accountnokey)

            end !loop
            access:invoice.restorefile(save_inv_id)

            setcursor()
            close(progresswindow)
    End!Case f_type
getnextrecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
CommonFaults         PROCEDURE  (f_RefNumber)         ! Declare Procedure
save_wpr_id          USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:EngineersLocation STRING(30)
save_map_id          USHORT,AUTO
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
!Get The Engineer's Location

    tmp:EngineersLocation = ''
    access:users.clearkey(use:password_key)
    use:password    = glo:password
    access:users.fetch(use:password_key)
    If use:user_type = 'ENGINEER'
        If job:engineer <> ''
            If use:user_code <> job:engineer
                Case Missive('You are not marked as the Engineer Allocated to this job.'&|
                  '<13,10>'&|
                  '<13,10>Do you wish to change the Engineer?','ServiceBase 3g',|
                               'mquest.jpg','\No|/Yes')
                Of 1 ! No Button
                Of 2 ! Yes Button
                    job:engineer    = use:user_code
                End ! Case Missive
            Else!If use:user_code <> job:engineer

            End!If use:user_code <> job:engineer
        Else!If job:engineer = ''
            job:engineer = use:user_code
        End!If job:engineer = ''

    End!If use:user_type = 'ENGINEER'
    access:users.clearkey(use:user_code_key)
    use:user_code   = job:engineer
    access:users.tryfetch(use:user_code_key)
    tmp:EngineersLocation   = use:location

Compile('***',Debug=1)
    Message('Adding Common Fault: ' & f_refnumber & |
            '|Engineers Location: ' & tmp:EngineersLocation,'Debug Message',icon:exclamation)
***

!Get The Common Fault
    access:commonfa.clearkey(com:ref_number_Key)
    com:ref_number  = f_refnumber
    If access:commonfa.tryfetch(com:ref_number_key) = Level:Benign
!Add the notes
        access:jobnotes.clearkey(jbn:RefNumberKey)
        jbn:RefNumber   = job:Ref_Number
        If access:jobnotes.tryfetch(jbn:RefNumberKey)
            Get(jobnotes,0)
            If access:jobnotes.primerecord() = Level:Benign
                jbn:refnumber   = job:ref_number
                access:jobnotes.tryinsert()
            End!If access:jobnotes.primerecord() = Level:Benign
        End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
        jbn:engineers_notes     = com:engineers_notes
        jbn:invoice_text        = com:invoice_text
        access:jobnotes.update()

        job:chargeable_job  = com:chargeable_job
        job:warranty_job    = com:warranty_job
        job:repair_type     = com:chargeable_repair_type
        job:repair_type_warranty    = com:warranty_repair_type
        job:charge_type     = com:chargeable_charge_type
        job:warranty_charge_type    = com:warranty_charge_type
        If job:fault_code1  = ''
            job:fault_code1 = com:fault_code1
        End!If job:fault_code1  = ''
        If job:fault_code2  = ''
            job:fault_code2 = com:fault_code2
        End!If job:fault_code1  = ''
        If job:fault_code3  = ''
            job:fault_code3 = com:fault_code3
        End!If job:fault_code1  = ''
        If job:fault_code4  = ''
            job:fault_code4 = com:fault_code4
        End!If job:fault_code1  = ''
        If job:fault_code5  = ''
            job:fault_code5 = com:fault_code5
        End!If job:fault_code1  = ''
        If job:fault_code6  = ''
            job:fault_code6 = com:fault_code6
        End!If job:fault_code1  = ''
        If job:fault_code7  = ''
            job:fault_code7 = com:fault_code7
        End!If job:fault_code1  = ''
        If job:fault_code8  = ''
            job:fault_code8 = com:fault_code8
        End!If job:fault_code1  = ''
        If job:fault_code9  = ''
            job:fault_code9 = com:fault_code9
        End!If job:fault_code1  = ''
        If job:fault_code10 = ''
            job:fault_code10 = com:fault_code10
        End!If job:fault_code1  = ''
        If job:fault_code11  = ''
            job:fault_code11 = com:fault_code11
        End!If job:fault_code1  = ''
        If job:fault_code12  = ''
            job:fault_code12 = com:fault_code12
        End!If job:fault_code1  = ''
        nostockc# = 0
        nostockw# = 0

        nostockc# = CommonFaultsChar(f_refnumber,tmp:EngineersLocation)
        nostockw# = CommonFaultsWar(f_refnumber,tmp:EngineersLocation)
        If nostockc# Or nostockw#
            Case Missive('One or more of the parts you have just attached to this job are NOT in Stock.'&|
              '<13,10>They will now appear on the next Order Geneation.','ServiceBase 3g',|
                           'midea.jpg','/OK')
            Of 1 ! OK Button
            End ! Case Missive
        End

        IF (AddToAudit(job:Ref_Number,'JOB', 'INSERTED COMMON FAULT', 'CATEGORY: ' & Clip(com:category) & |
                                '<13,10>DESCRIPTION: ' & Clip(com:description)))
        END ! IF


!Check The Fault Codes
        glo:errortext = ''
        setcursor(cursor:wait)
        save_wpr_id = access:warparts.savefile()
        access:warparts.clearkey(wpr:part_number_key)
        wpr:ref_number  = job:ref_number
        set(wpr:part_number_key,wpr:part_number_key)
        loop
            if access:warparts.next()
               break
            end !if
            if wpr:ref_number  <> job:ref_number      |
                then break.  ! end if
            save_map_id = access:manfaupa.savefile()
            access:manfaupa.clearkey(map:field_number_key)
            map:manufacturer = job:manufacturer
            set(map:field_number_key,map:field_number_key)
            loop
                if access:manfaupa.next()
                   break
                end !if
                if map:manufacturer <> job:manufacturer      |
                    then break.  ! end if
                Case map:field_number
                    Of 1
                        If map:compulsory = 'YES' and wpr:fault_code1 = ''
                            glo:errortext = '<13,10>Fault Code  ' & Clip(map:field_name) & ' Missing On Part ' & Clip(wpr:part_number)
                        End!If map:compulsory = 'YES' and wpr:fault_code1 = ''
                    Of 2
                        If map:compulsory = 'YES' and wpr:fault_code2 = ''
                            glo:errortext = '<13,10>Fault Code  ' & Clip(map:field_name) & ' Missing On Part ' & Clip(wpr:part_number)
                        End!If map:compulsory = 'YES' and wpr:fault_code1 = ''
                    Of 3
                        If map:compulsory = 'YES' and wpr:fault_code3 = ''
                            glo:errortext = '<13,10>Fault Code  ' & Clip(map:field_name) & ' Missing On Part ' & Clip(wpr:part_number)
                        End!If map:compulsory = 'YES' and wpr:fault_code1 = ''

                    Of 4
                        If map:compulsory = 'YES' and wpr:fault_code4 = ''
                            glo:errortext = '<13,10>Fault Code  ' & Clip(map:field_name) & ' Missing On Part ' & Clip(wpr:part_number)
                        End!If map:compulsory = 'YES' and wpr:fault_code1 = ''

                    Of 5
                        If map:compulsory = 'YES' and wpr:fault_code5 = ''
                            glo:errortext = '<13,10>Fault Code  ' & Clip(map:field_name) & ' Missing On Part ' & Clip(wpr:part_number)
                        End!If map:compulsory = 'YES' and wpr:fault_code1 = ''

                    Of 6
                        If map:compulsory = 'YES' and wpr:fault_code6 = ''
                            glo:errortext = '<13,10>Fault Code  ' & Clip(map:field_name) & ' Missing On Part ' & Clip(wpr:part_number)
                        End!If map:compulsory = 'YES' and wpr:fault_code1 = ''

                    Of 7
                        If map:compulsory = 'YES' and wpr:fault_code7 = ''
                            glo:errortext = '<13,10>Fault Code  ' & Clip(map:field_name) & ' Missing On Part ' & Clip(wpr:part_number)
                        End!If map:compulsory = 'YES' and wpr:fault_code1 = ''

                    Of 8
                        If map:compulsory = 'YES' and wpr:fault_code8 = ''
                            glo:errortext = '<13,10>Fault Code  ' & Clip(map:field_name) & ' Missing On Part ' & Clip(wpr:part_number)
                        End!If map:compulsory = 'YES' and wpr:fault_code1 = ''

                    Of 9
                        If map:compulsory = 'YES' and wpr:fault_code9 = ''
                            glo:errortext = '<13,10>Fault Code  ' & Clip(map:field_name) & ' Missing On Part ' & Clip(wpr:part_number)
                        End!If map:compulsory = 'YES' and wpr:fault_code1 = ''

                    Of 10
                        If map:compulsory = 'YES' and wpr:fault_code10 = ''
                            glo:errortext = '<13,10>Fault Code  ' & Clip(map:field_name) & ' Missing On Part ' & Clip(wpr:part_number)
                        End!If map:compulsory = 'YES' and wpr:fault_code1 = ''

                    Of 11
                        If map:compulsory = 'YES' and wpr:fault_code11 = ''
                            glo:errortext = '<13,10>Fault Code  ' & Clip(map:field_name) & ' Missing On Part ' & Clip(wpr:part_number)
                        End!If map:compulsory = 'YES' and wpr:fault_code1 = ''

                    Of 12
                        If map:compulsory = 'YES' and wpr:fault_code12 = ''
                            glo:errortext = '<13,10>Fault Code  ' & Clip(map:field_name) & ' Missing On Part ' & Clip(wpr:part_number)
                        End!If map:compulsory = 'YES' and wpr:fault_code1 = ''

                End!Case map:field_number
            end !loop
            access:manfaupa.restorefile(save_map_id)
        end !loop
        access:warparts.restorefile(save_wpr_id)
        setcursor()

        If glo:ErrorText <> ''
            glo:ErrorText = 'Common Faults Warning!:<13,10>' & glo:ErrorText
            Error_Text
            glo:ErrorText = ''
        End!If glo:ErrorText <> ''
       If job:ignore_warranty_charges <> 'YES'
            Pricing_Routine('W',labour",parts",pass",claim",handling",exchange",RRCRate",RRCParts")
            If pass" = True
                job:labour_cost_warranty = labour"
                job:parts_cost_warranty  = parts"
                job:sub_total_warranty = job:labour_cost_warranty + job:parts_cost_warranty + job:courier_cost_warranty
                Access:JOBSE.Clearkey(jobe:RefNumberKey)
                jobe:RefNumber  = job:Ref_Number
                If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                    !Found
                    jobe:ClaimValue     = claim"
                    jobe:HandlingFee    = handling"
                    jobe:ExchangeRate   = exchange"
                    jobe:RRCWLabourCost = RRCRate"
                    jobe:RRCWPartsCost  = RRCParts"
                    jobe:RRCWSubTotal   = jobe:RRCWLabourCost + jobe:RRCWPartsCost + job:Courier_Cost_Warranty
                    Access:JOBSE.Update()
                Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                    !Error
                End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            End!If pass" = False
        End!If job:ignore_warranty_charges <> 'YES'

       If job:ignore_Chargeable_charges <> 'YES'
            Pricing_Routine('C',labour",parts",pass",claim",handling",exchange",RRCRate",RRCParts")
            If pass" = True
                job:labour_cost = labour"
                job:parts_cost  = parts"
                job:sub_total = job:labour_cost + job:parts_cost + job:courier_cost

                Access:JOBSE.Clearkey(jobe:RefNumberKey)
                jobe:RefNumber  = job:Ref_Number
                If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                    !Found
                    jobe:RRCCLabourCost = RRCRate"
                    jobe:RRCCPartsCost  = RRCParts"
                    jobe:RRCCSubTotal   = jobe:RRCCLabourCost + jobe:RRCCPartsCost + job:Courier_Cost
                    jobe:HandlingFee    = handling"
                    Access:JOBSE.Update()
                Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                    !Error
                End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            End!If pass" = False
        End!If job:ignore_warranty_charges <> 'YES'
    Else!If access:commonfa.tryfetch(com:ref_number_key) = Level:Benign
        Case Missive('Unable to find the selected Common Fault.','ServiceBase 3g',|
                       'mstop.jpg','/OK')
        Of 1 ! OK Button
        End ! Case Missive
    End!If access:commonfa.tryfetch(com:ref_number_key) = Level:Benign
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
CommonFaultsChar     PROCEDURE  (f_refnumber,f_location) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:engstockrefnumber LONG
tmp:engstockquantity LONG
tmp:mainstockrefnumber LONG
tmp:mainstockquantity LONG
tmp:engstocksundry   BYTE(0)
tmp:mainstocksundry  BYTE(0)
tmp:mainstore        STRING(30)
tmp:ordered          BYTE(0)
save_ccp_id          USHORT,AUTO
save_loc_id          USHORT,AUTO
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
!Add Chargeable Common Fault Parts
    setcursor(cursor:wait)
    save_ccp_id = access:commoncp.savefile()
    access:commoncp.clearkey(ccp:ref_number_key)
    ccp:ref_number = f_refnumber
    set(ccp:ref_number_key,ccp:ref_number_key)
    loop
        if access:commoncp.next()
           break
        end !if
        if ccp:ref_number <> f_refnumber      |
            then break.  ! end if

        If ccp:exclude_from_order = 'YES'
            If access:parts.primerecord() = Level:Benign
                par:ref_number           = job:ref_number
                par:adjustment           = ccp:adjustment
                par:part_number          = CCP:Part_Number
                par:description          = CCP:Description
                par:supplier             = CCP:Supplier
                par:purchase_cost        = CCP:Purchase_Cost
                par:sale_cost            = CCP:Sale_Cost
                par:quantity             = CCP:Quantity
                par:fault_codes_checked  = 'YES'
                par:fault_code1    = ccp:fault_code1
                par:fault_code2    = ccp:fault_code2
                par:fault_code3    = ccp:fault_code3
                par:fault_code4    = ccp:fault_code4
                par:fault_code5    = ccp:fault_code5
                par:fault_code6    = ccp:fault_code6
                par:fault_code7    = ccp:fault_code7
                par:fault_code8    = ccp:fault_code8
                par:fault_code9    = ccp:fault_code9
                par:fault_code10   = ccp:fault_code10
                par:fault_code11   = ccp:fault_code11
                par:fault_code12   = ccp:fault_code12
                par:exclude_from_order   = ccp:exclude_from_order
                par:date_ordered         = Today()
                access:PARTS.tryinsert()
            End!If access:parts.primerecord() = Level:Benign
        Else!If ccp:exclude_from_order = 'YES'
            !Clear the variables
            tmp:engstockrefnumber   = 0
            tmp:engstockquantity    = 0
            tmp:engstocksundry      = 0
            tmp:mainstockrefnumber  = 0
            tmp:mainstockquantity   = 0
            tmp:mainstocksundry     = 0

Compile('***',Debug=1)
    Message('Chargeable Part Required: ' & ccp:part_number & |
            'Quantity: ' & ccp:quantity,'Debug Message',icon:exclamation)
***

            !Find the part at the Engineers Location
            access:stock.clearkey(sto:location_key)
            sto:location    = f_location
            sto:part_number = ccp:part_number
            if access:stock.tryfetch(sto:location_key) = Level:Benign
                !Part Number has been found in the Engineers' Location
                !Try and match Model Number
                access:stomodel.clearkey(stm:model_number_key)
                stm:ref_number   = sto:ref_number
                stm:manufacturer = job:manufacturer
                stm:model_number = job:model_number
                if access:stomodel.tryfetch(stm:model_number_key) = Level:Benign
Compile('***',Debug=1)
    Message('Chargeable Part Found At Engineers Location: ' & |
            '|Stock Ref Number: ' & sto:ref_number & |
            '|Quantity Stock: ' & sto:quantity_stock,'Debug Message',icon:exclamation)
***
                    !Found Part At Engineers Location
                    If sto:Suspend
                        tmp:engstockrefnumber   = 0
                        tmp:engstockquantity    = 0
                    Else !If sto:Suspend
                        tmp:engstockrefnumber   = sto:ref_number
                        tmp:engstockquantity    = sto:quantity_stock
                        If sto:sundry_item = 'YES'
                            tmp:engstocksundry = 1
                        Else!If sto:sundry_item = 'YES'
                            tmp:engstocksundry = 0
                        End!If sto:sundry_item = 'YES'

                    End !If sto:Suspend
                Else!if access:stomodel.tryfetch(stm:model_number_key) = Level:Benign
                    !Part Number does not match Model Number
                End!if access:stomodel.tryfetch(stm:model_number_key) = Level:Benign
            Else!if access:stock.tryfetch(sto:location_key) = Level:Benign
                !Part Number NOT found at engineer's location
            End!if access:stock.tryfetch(sto:location_key) = Level:Benign

!            !Find the Main Store Location
!            tmp:mainstore   = ''
!            setcursor(cursor:wait)
!            save_loc_id = access:location.savefile()
!            access:location.clearkey(loc:main_store_key)
!            loc:main_store = 'YES'
!            set(loc:main_store_key,loc:main_store_key)
!            loop
!                if access:location.next()
!                   break
!                end !if
!                if loc:main_store <> 'YES'      |
!                    then break.  ! end if
!                tmp:mainstore   = loc:location
!                Break
!            end !loop
!            access:location.restorefile(save_loc_id)
!            setcursor()
!
!            if tmp:mainstore <> ''
!                !If a Main Store Location exists
!                access:stock.clearkey(sto:location_key)
!                sto:location    = tmp:mainstore
!                sto:part_number = ccp:part_number
!                if access:stock.tryfetch(sto:location_key) = Level:Benign
!                    !Part Number exists in Main Store
!                    !Does the Model Number match?
!                    access:stomodel.clearkey(stm:model_number_key)
!                    stm:ref_number   = sto:Ref_number
!                    stm:manufacturer = job:manufacturer
!                    stm:model_number = job:model_number
!                    if access:stomodel.tryfetch(stm:model_number_key) = Level:Benign
!Compile('***',Debug=1)
!    Message('Chargeable Part AT Main Store Location: ' &|
!            '|Stock Ref Number: ' & sto:ref_number & |
!            '|Quantity: ' & sto:quantity_stock,'Debug Message',icon:exclamation)
!***
!                        If sto:Suspend
!                            tmp:mainstockrefnumber  = 0
!                            tmp:mainstockquantity   = 0
!                        Else !If sto:Suspend
!                            tmp:mainstockrefnumber  = sto:ref_number
!                            tmp:mainstockquantity   = sto:quantity_stock
!
!                            If sto:sundry_item = 'YES'
!                                tmp:mainstocksundry = 1
!                            Else!If sto:sundry_item = 'YES'
!                                tmp:mainstocksundry = 0
!                            End!If sto:sundry_item = 'YES'
!
!                        End !If sto:Suspend
!
!                    Else!if access:stomodel.tryfetch(stm:model_number_key) = Level:Benign
!                        !Part Number does not match Model Number
!                    End!if access:stomodel.tryfetch(stm:model_number_key) = Level:Benign
!                Else!if access:stock.tryfetch(sto:location_key) = Level:Benign
!                    !Part Number does not exists in Main Store
!                End!if access:stock.tryfetch(sto:location_key) = Level:Benign
!            End!if tmp:mainstore <> ''

            If tmp:engstockrefnumber = 0 !And tmp:mainstockrefnumber = 0
                Case Missive('Cannot add the following part to this job. It has been suspended or cannot be found in the Engineer''s Location:'&|
                  '<13,10>'&|
                  '<13,10>' & Clip(ccp:Part_Number) & ' - ' & Clip(ccp:Description) & '.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                Of 1 ! OK Button
                End ! Case Missive
            Else
                tmp:ordered = 0
                Do AddParts

            End!If tmp:engstockrefnumber = 0 And tmp:mainstockrefnumber = 0


        End!If ccp:exclude_from_order = 'YES'

    end !loop
    access:commoncp.restorefile(save_ccp_id)
    setcursor()
    Return tmp:ordered
AddParts        Routine
    If access:parts.primerecord() = Level:Benign
        !Insert a New Part
        par:quantity             = CCP:Quantity
        Do AddPartDetails

        If tmp:engstockrefnumber <> 0
            !Engineers Part Exists
            If tmp:engstocksundry
                !If part is a sundry, don't take from stock and just add it.
                par:part_ref_number = tmp:engstockrefnumber
                par:date_ordered    = Today()
                access:parts.tryinsert()
            Else!If tmp:engstocksundry
                If ccp:quantity > tmp:engstockquantity
                    !The Quantity Required is more than in the Engineers Location
                    If tmp:engstockquantity = 0
                        !If there is NO engineers stock
                        If tmp:mainstockrefnumber <> 0
                            !If there is stock in Main Stock
                            If par:quantity > tmp:mainstockquantity
                                !If Stock required is more than Main Stock Quantity
                                If access:ordpend.primerecord() = Level:Benign
                                    ope:part_ref_number = tmp:engstockrefnumber
                                    ope:job_number      = job:ref_number
                                    ope:part_type       = 'JOB'
                                    ope:supplier        = par:supplier
                                    ope:part_number     = par:part_number
                                    If tmp:mainstockquantity = 0
                                        !If there is NO Main Store stock
                                        !Order all the stock for the engineer part
                                        ope:quantity        = ccp:quantity
                                        access:ordpend.insert()
                                        tmp:ordered = 1
                                        par:pending_ref_number  = ope:ref_number
                                        par:part_ref_number = tmp:engstockrefnumber
                                        access:parts.insert()
                                    Else!If tmp:mainstockquantity = 0
                                        !If there is some Main Store Stock, but not enough
                                        !make an order for the difference, and change the
                                        !part's quantity to match what was taken from Main Store
                                        ope:quantity    = ccp:quantity - tmp:mainstockquantity
                                        access:ordpend.insert()
                                        tmp:ordered = 1
                                        par:quantity        = tmp:mainstockquantity
                                        par:date_ordered    = Today()
                                        par:part_ref_number = tmp:mainstockquantity
                                        access:parts.insert()

                                        !Take the item from Stock
                                        access:stock.clearkey(sto:ref_number_key)
                                        sto:ref_number      = tmp:mainstockrefnumber
                                        If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
                                            !Take The quantity from stock
                                            sto:quantity_stock  = 0
                                            access:stock.update()

                                            If access:stohist.primerecord() = Level:Benign
                                                shi:quantity    = tmp:mainstockquantity
                                                Do AddStockHistory
                                            End!If access:stohist.primerecord() = Level:Benign
                                        End!If access:stock.tryfetch(sto:ref_number_key) = Level:Benign

                                        !Create a new part for the Pending Order
                                        If access:parts.primerecord() = Level:benign
                                            par:quantity    = ccp:quantity - tmp:mainstockquantity
                                            par:pending_ref_number  = ope:ref_number
                                            access:parts.insert()
                                        End!If access:parts.primerecord() = Level:benign
                                    End!If tmp:mainstockquantity = 0
                                 End!If access:ordpend.primerecord() = Level:Benign
                            Else!If par:quantity > tmp:mainstockquantity
                                !There is sufficent Stock In the Main Store
                                !Take the item from stock
                                par:quantity    = ccp:quantity
                                par:date_ordered    = Today()
                                par:part_ref_number = tmp:mainstockrefnumber
                                access:parts.insert()

                                access:stock.clearkey(sto:ref_number_key)
                                sto:ref_number  = tmp:mainstockrefnumber
                                If access:stock.tryfetch(sto:ref_number_key) = level:benign
                                    sto:quantity_stock -= ccp:quantity
                                    If sto:quantity_stock < 0
                                        sto:quantity_stock = 0
                                    End!If sto:quantity_stock < 0
                                    access:stock.update()

                                    If access:stohist.primerecord() = Level:benign
                                        shi:quantity    = ccp:quantity
                                        Do AddStockHistory
                                    End!If access:stohist.primerecord() = Level:benign
                                End!If access:stock.tryfetch(sto:ref_number_key) = level:benign
                            End!If par:quantity > tmp:mainstockquantity
                        Else!If tmp:mainstockrefnumber <> 0
                            !There is No Main Store Stock Item
                            !Create an order from the Engineers Part
                            If access:ordpend.primerecord() = Level:Benign
                                ope:part_ref_number = tmp:engstockrefnumber
                                ope:job_number      = job:ref_number
                                ope:part_type       = 'JOB'
                                ope:supplier        = par:supplier
                                ope:part_number     = par:part_number
                                ope:quantity        = ccp:quantity
                                access:ordpend.insert()
                                tmp:ordered = 1
                                par:quantity        = ccp:quantity
                                par:pending_ref_number  = ope:ref_number
                                access:parts.insert()
                            End!If access:ordpend.primerecord() = Level:Benign
                        End!If tmp:mainstockrefnumber <> 0
                    Else!If tmp:engstockquantity = 0
                        !There is some stock in the Engineer's Location, but not enough
                        !Take the quantity left from engineers stock
                        par:quantity        = tmp:engstockquantity
                        par:date_ordered    = Today()
                        par:part_ref_number = tmp:engstockrefnumber
                        access:parts.insert()

                        access:stock.clearkey(sto:ref_number_key)
                        sto:ref_number  = tmp:engstockrefnumber
                        If access:stock.tryfetch(sto:ref_number_key) = level:Benign
                            sto:quantity_stock = 0
                            access:stock.update()

                            If access:stohist.primerecord() = Level:Benign
                                shi:quantity    = tmp:engstockquantity
                                Do AddStockHistory
                            End!If access:stohist.primerecord() = Level:Benign
                        End!If access:stock.tryfetch(sto:ref_number_key) = level:Benign

                        !Is there a main store item as well?
                        If tmp:mainstockrefnumber <> 0
                            !There is a main store item, is there enough in stock?
                            If (ccp:quantity - tmp:engstockquantity) > tmp:mainstockquantity
                                !Quantity requested is higher than main store and engineer put together
                                !Create an order
                                If access:ordpend.primerecord() = Level:benign
                                    ope:part_ref_number = tmp:engstockrefnumber
                                    ope:job_number      = job:ref_number
                                    ope:part_type       = 'JOB'
                                    ope:supplier        = par:supplier

                                    If access:parts.primerecord() = Level:Benign
                                        Do AddPartDetails
                                        par:part_ref_number = tmp:engstockrefnumber
                                        If tmp:mainstockquantity = 0
                                            !There is no stock in Main Store
                                            par:quantity    = ccp:quantity - tmp:engstockquantity
                                            par:pending_ref_number = ope:ref_number
                                            access:parts.insert()
                                            ope:quantity    = par:quantity
                                            access:ordpend.insert()
                                            tmp:ordered = 1
                                        Else!If tmp:mainstockquantity = 0
                                            !Take the stock from main store.
                                            !Create an order pending for the rest
                                            par:quantity    = tmp:mainstockquantity
                                            par:date_ordered    = Today()
                                            par:part_ref_number = tmp:mainstockrefnumber
                                            access:parts.insert()

                                            If access:parts.primerecord() = Level:benign
                                                Do AddPartDetails
                                                par:part_ref_number = tmp:engstockrefnumber
                                                par:quantity        = ccp:quantity - (tmp:engstockquantity + tmp:mainstockquantity)
                                                par:pending_ref_number  = ope:ref_number
                                                access:parts.insert()
                                                ope:quantity        = par:quantity
                                                access:ordpend.insert()
                                                tmp:ordered = 1
                                            End!If access:parts.primerecord() = Level:benign

                                            Access:stock.clearkey(sto:ref_number_key)
                                            sto:ref_number  = tmp:mainstockrefnumber
                                            If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
                                                sto:quantity_stock = 0
                                                access:stock.update()
                                            End!If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
                                            If access:stohist.primerecord() = Level:Benign
                                                shi:quantity    = tmp:mainstockquantity
                                                Do AddStockHistory
                                            End!If access:stohist.primerecord() = Level:Benign
                                        End!If tmp:mainstockquantity = 0
                                    End!If access:parts.primerecord() = Level:Benign
                                End!If access:ordpend.primerecord(0 = Level:benign
                            Else!If (ccp:quantity - tmp:engstockquantity) > tmp:mainstockquantity
                                !The is sufficient stock left in the Main Store
                                par:quantity    = ccp:quantity - tmp:engstockquantity
                                par:part_ref_number = tmp:mainstockrefnumber
                                par:date_ordered    = Today()
                                access:parts.insert()

                                access:stock.clearkey(sto:ref_number_key)
                                sto:ref_number  = tmp:mainstockrefnumber
                                If Access:stock.tryfetch(sto:ref_number_key) = level:Benign
                                    sto:quantity_stock  -= (ccp:quantity - tmp:engstockquantity)
                                    If sto:quantity_stock < 0
                                        sto:quantity_stock = 0
                                    End!If sto:quantity_stock < 0
                                    access:stock.update()
                                End!If Access:stock.tryfetch(sto:ref_number_key) = level:Benign

                                If access:stohist.primerecord() = Level:Benign
                                    shi:quantity    = ccp:quantity - tmp:engstockquantity
                                    Do AddStockHistory
                                End!If access:stohist.primerecord() = Level:Benign
                            End!If (ccp:quantity - tmp:engstockquantity) > tmp:mainstockquantity
                        Else!If tmp:mainstockrefnumber <> 0
                            !If there is no Main Store Item
                            !Create an order for the remaining
                            If access:ordpend.primerecord() = Level:Benign
                                ope:part_ref_number = tmp:engstockrefnumber
                                ope:job_number      = job:ref_number
                                ope:part_type       = 'JOB'
                                ope:supplier        = par:supplier
                                ope:part_number     = par:part_number
                                ope:quantity        = ccp:quantity - tmp:engstockquantity
                                access:ordpend.insert()
                                tmp:ordered = 1
                                If access:parts.primerecord() = Level:Benign
                                    Do AddPartDetails
                                    par:pending_ref_number  = ope:ref_number
                                    par:part_ref_number     = tmp:engstockrefnumber
                                    par:quantity            = ccp:quantity - tmp:engstockquantity
                                    access:parts.insert()
                                End!If access:parts.primerecord() = Level:Benign
                            End!If access:ordpend.primerecord() = Level:Benign
                        End!If tmp:mainstockrefnumber <> 0
                    End!If tmp:engstockquantity = 0
                Else!If ccp:quantity > tmp:engstockquantity
                    !If there is enough in the engineer's location
                    par:quantity   = ccp:quantity
                    par:date_ordered    = Today()
                    par:part_ref_number = tmp:engstockrefnumber
                    access:parts.insert()
                    access:stock.clearkey(sto:ref_number_key)
                    sto:ref_number  = tmp:engstockrefnumber
                    If access:stock.tryfetch(sto:ref_number_key) = Level:benign
                        sto:quantity_stock -= ccp:quantity
                        If sto:quantity_stock < 0
                            sto:quantity_stock = 0
                        End!If sto:quantity_stock < 0
                        access:stock.update()
                        If access:stohist.primerecord() = level:Benign
                            shi:quantity    = ccp:quantity
                            Do AddStockHistory
                        End!If access:stohist.primerecord() = level:Benign
                    End!If access:stock.tryfetch(sto:ref_number_key) = Level:benign
                End!If ccp:quantity > tmp:engstockquantity
            End!If tmp:engstocksundry
        Else!If tmp:engstockrefnumber <> 0
            !If there is no engineer's stock
            !Is there a main stock item?
            If tmp:mainstockrefnumber <> 0
                !There is a main store item
                If tmp:mainstocksundry
                    par:quantity    = tmp:mainstockquantity
                    par:date_ordered = Today()
                    par:part_ref_number  = tmp:mainstockrefnumber
                    access:parts.insert()
                Else!If tmp:mainstocksundry
                    If ccp:quantity > tmp:mainstockquantity
                        !Quantity Required is more than main store quantity
                        If access:ordpend.primerecord() = Level:Benign
                            ope:part_ref_number     = tmp:mainstockrefnumber
                            ope:job_number          = job:ref_number
                            ope:part_type           = 'JOB'
                            ope:supplier            = par:supplier
                            ope:part_number         = par:part_number
                            If tmp:mainstockquantity = 0
                                !If there is no stock in the main store
                                ope:quantity    = ccp:quantity
                                access:ordpend.insert()
                                tmp:ordered = 1
                                par:quantity    = ccp:quantity
                                par:pending_ref_number  = ope:ref_number
                                par:part_ref_number     = tmp:mainstockrefnumber
                                access:parts.insert()
                            Else!If tmp:mainstockquantity = 0
                                !take the remaining items from main store, and order the rest
                                ope:quantity    = ccp:quantity - tmp:mainstockquantity
                                access:ordpend.insert()
                                tmp:ordered = 1
                                par:quantity        = tmp:mainstockquantity
                                par:date_ordered    = Today()
                                par:part_ref_number = tmp:mainstockrefnumber
                                access:parts.insert()
                                access:stock.clearkey(sto:ref_number_key)
                                sto:ref_number      = tmp:mainstockrefnumber
                                If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
                                    sto:quantity_stock = 0
                                    access:stock.update()
                                    If access:stohist.primerecord() = Level:Benign
                                        shi:quantity    = tmp:mainstockquantity
                                        If access:stohist.tryinsert()
                                            access:stohist.cancelautoinc()
                                        End!If access:stohist.tryinsert()
                                    End!If access:stohist.primerecord() = Level:Benign
                                End!If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
                                If access:parts.primerecord() = Level:Benign
                                    Do AddPartDetails
                                    par:quantity            = ccp:quantity - tmp:mainstockquantity
                                    par:pending_ref_number  = ope:ref_number
                                    par:part_ref_number     = tmp:mainstockrefnumber
                                    If access:parts.tryinsert()
                                        access:parts.cancelautoinc()
                                    End!If access:parts.tryinsert()
                                End!If access:parts.primerecord() = Level:Benign
                            End!If tmp:mainstockquantity = 0
                        End!If access:ordpend.primerecord() = Level:Benign
                    Else!If ccp:quantity > tmp:mainstockquantity
                        !If there is enough quantity in the main store
                        par:quantity        = ccp:quantity
                        par:date_ordered    = Today()
                        par:part_ref_number = tmp:mainstockrefnumber
                        access:parts.insert()
                        access:stock.clearkey(sto:ref_number_key)
                        If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
                            !Found
                            sto:quantity_stock -= ccp:quantity
                            If sto:quantity_stock < 0
                                sto:quantity_stock = 0
                            End!If sto:quantity_stock < 0
                            access:stock.update()
                        Else! If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
                            !Error
                        End! If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
                        If access:stohist.primerecord() = Level:Benign
                            shi:quantity    = ccp:quantity
                            Do AddStockHistory
                        End!If access:stohist.primerecord() = Level:Benign
                    End!If ccp:quantity > tmp:mainstockquantity
                End!If tmp:mainstocksundry
            Else!If tmp:mainstockrefnumber <> 0
                !If there is no main store item
                If access:ordpend.primerecord() = Level:Benign
                    ope:part_ref_number     = ''
                    ope:job_number          = job:ref_number
                    ope:part_type           = 'JOB'
                    ope:supplier            = par:supplier
                    ope:part_number         = par:part_number
                    ope:quantity            = par:quantity
                    If access:ordpend.tryinsert()
                        access:ordpend.cancelautoinc()
                    End!If access:ordpend.tryinsert()
                    par:pending_ref_number  = ope:ref_number
                    access:parts.insert()
                End!If access:ordpend.primerecord() = Level:Benign
            End!If tmp:mainstockrefnumber <> 0
        End!If tmp:engstockrefnumber <> 0
    End!If access:parts.primerecord() = Level:Benign
AddPartDetails      Routine
    par:ref_number           = job:ref_number
    par:adjustment           = ccp:adjustment
    par:part_number          = CCP:Part_Number
    par:description          = CCP:Description
    par:supplier             = CCP:Supplier
    par:purchase_cost        = CCP:Purchase_Cost
    par:sale_cost            = CCP:Sale_Cost

    par:fault_codes_checked  = 'YES'
    If par:fault_Code1 = ''
        par:Fault_Code1         = ccp:fault_Code1
    End!If par:FaultCode1           = ''
    If par:Fault_Code2          = ''
        par:Fault_Code2         = ccp:fault_Code2
    End!If par:FaultCode2           = ''
    If par:Fault_Code3          = ''
        par:Fault_Code3         = ccp:fault_Code3
    End!If par:FaultCode3           = ''
    If par:Fault_Code4          = ''
        par:Fault_Code4         = ccp:fault_Code4
    End!If par:FaultCode4           = ''
    If par:Fault_Code5          = ''
        par:Fault_Code5         = ccp:fault_Code5
    End!If par:FaultCode5           = ''
    If par:Fault_Code6          = ''
        par:Fault_Code6         = ccp:fault_Code6
    End!If par:FaultCode6           = ''
    If par:Fault_Code7          = ''
        par:Fault_Code7         = ccp:fault_Code7
    End!If par:FaultCode7           = ''
    If par:Fault_Code8          = ''
        par:Fault_Code8         = ccp:fault_Code8
    End!If par:FaultCode8           = ''
    If par:Fault_Code9          = ''
        par:Fault_Code9         = ccp:fault_Code9
    End!If par:FaultCode9           = ''
    If par:Fault_Code10         = ''
        par:Fault_Code10        = ccp:fault_Code10
    End!If par:FaultCode10      = ''
    If par:Fault_Code11         = ''
        par:Fault_Code11        = ccp:fault_Code11
    End!If par:FaultCode11      = ''
    If par:Fault_Code12         = ''
        par:Fault_Code12        = ccp:fault_Code12
    End
    par:exclude_from_order   = ccp:exclude_from_order
AddStockHistory     Routine
    shi:ref_number           = sto:ref_number
    access:users.clearkey(use:password_key)
    use:password              = glo:password
    access:users.fetch(use:password_key)
    shi:user                  = use:user_code
    shi:date                 = today()
    shi:transaction_type     = 'DEC'
    shi:despatch_note_number = par:despatch_note_number
    shi:job_number           = job:ref_number
    shi:purchase_cost        = par:purchase_cost
    shi:sale_cost            = par:sale_cost
    shi:retail_cost          = par:retail_cost
    shi:notes                = 'STOCK DECREMENTED'
    if access:stohist.insert()
       access:stohist.cancelautoinc()
    end
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
CommonFaultsWar      PROCEDURE  (f_refnumber,f_location) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:engstockrefnumber LONG
tmp:engstockquantity LONG
tmp:mainstockrefnumber LONG
tmp:mainstockquantity LONG
tmp:engstocksundry   BYTE(0)
tmp:mainstocksundry  BYTE(0)
tmp:mainstore        STRING(30)
tmp:ordered          BYTE(0)
save_cwp_id          USHORT,AUTO
save_loc_id          USHORT,AUTO
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
!Add Warranty Common Fault Parts
    setcursor(cursor:wait)
    save_cwp_id = access:commonwp.savefile()
    access:commonwp.clearkey(cwp:ref_number_key)
    cwp:ref_number = f_refnumber
    set(cwp:ref_number_key,cwp:ref_number_key)
    loop
        if access:commonwp.next()
           break
        end !if
        if cwp:ref_number <> f_refnumber      |
            then break.  ! end if

        If cwp:exclude_from_order = 'YES'
            If access:WARPARTS.primerecord() = Level:Benign
                wpr:ref_number           = job:ref_number
                wpr:adjustment           = cwp:adjustment
                wpr:part_number          = cwp:Part_Number
                wpr:description          = cwp:Description
                wpr:supplier             = cwp:Supplier
                wpr:purchase_cost        = cwp:Purchase_Cost
                wpr:sale_cost            = cwp:Sale_Cost
                wpr:quantity             = cwp:Quantity
                wpr:fault_codes_checked  = 'YES'
                wpr:fault_code1    = cwp:fault_code1
                wpr:fault_code2    = cwp:fault_code2
                wpr:fault_code3    = cwp:fault_code3
                wpr:fault_code4    = cwp:fault_code4
                wpr:fault_code5    = cwp:fault_code5
                wpr:fault_code6    = cwp:fault_code6
                wpr:fault_code7    = cwp:fault_code7
                wpr:fault_code8    = cwp:fault_code8
                wpr:fault_code9    = cwp:fault_code9
                wpr:fault_code10   = cwp:fault_code10
                wpr:fault_code11   = cwp:fault_code11
                wpr:fault_code12   = cwp:fault_code12

                wpr:exclude_from_order   = cwp:exclude_from_order
                wpr:date_ordered         = Today()
                Access:WARPARTS.Tryinsert()
            End!If access:parts.primerecord() = Level:Benign
        Else!If cwp:exclude_from_order = 'YES'
            !Clear the variables
            tmp:engstockrefnumber   = 0
            tmp:engstockquantity    = 0
            tmp:engstocksundry      = 0
            tmp:mainstockrefnumber  = 0
            tmp:mainstockquantity   = 0
            tmp:mainstocksundry     = 0
Compile('***',Debug=1)
    Message('Warranty Part Required: ' & cwp:part_number & |
            'Quantity: ' & cwp:quantity,'Debug Message',icon:exclamation)
***

            !Find the part at the Engineers Location
            access:stock.clearkey(sto:location_key)
            sto:location    = f_location
            sto:part_number = cwp:part_number
            if access:stock.tryfetch(sto:location_key) = Level:Benign
                !Part Number has been found in the Engineers' Location
                !Try and match Model Number
                access:stomodel.clearkey(stm:model_number_key)
                stm:ref_number   = sto:ref_number
                stm:manufacturer = job:manufacturer
                stm:model_number = job:model_number
                if access:stomodel.tryfetch(stm:model_number_key) = Level:Benign
Compile('***',Debug=1)
    Message('Warranty Part Found At Engineers Location: ' & |
            '|Stock Ref Number: ' & sto:ref_number & |
            '|Quantity Stock: ' & sto:quantity_stock,'Debug Message',icon:exclamation)
***

                    !Found Part At Engineers Location
                    If sto:Suspend
                        tmp:engstockrefnumber   = 0
                        tmp:engstockquantity    = 0
                    Else !If sto:Suspend
                        tmp:engstockrefnumber   = sto:ref_number
                        tmp:engstockquantity    = sto:quantity_stock
                        If sto:sundry_item = 'YES'
                            tmp:engstocksundry = 1
                        Else!If sto:sundry_item = 'YES'
                            tmp:engstocksundry = 0
                        End!If sto:sundry_item = 'YES'
                    End !If sto:Suspend

                Else!if access:stomodel.tryfetch(stm:model_number_key) = Level:Benign
                    !Part Number does not match Model Number
                End!if access:stomodel.tryfetch(stm:model_number_key) = Level:Benign
            Else!if access:stock.tryfetch(sto:location_key) = Level:Benign
                !Part Number NOT found at engineer's location
            End!if access:stock.tryfetch(sto:location_key) = Level:Benign

            !As with jobs, parts should only come from the Engineer's Location,
            !not from Main Store.
            !If I keep the Main Store variables at zero, above, then I shouldn't need
            !to change anything else.

!            !Find the Main Store Location
!            tmp:mainstore   = ''
!            setcursor(cursor:wait)
!            save_loc_id = access:location.savefile()
!            access:location.clearkey(loc:main_store_key)
!            loc:main_store = 'YES'
!            set(loc:main_store_key,loc:main_store_key)
!            loop
!                if access:location.next()
!                   break
!                end !if
!                if loc:main_store <> 'YES'      |
!                    then break.  ! end if
!                tmp:mainstore   = loc:location
!                Break
!            end !loop
!            access:location.restorefile(save_loc_id)
!            setcursor()
!
!
!            if tmp:mainstore <> ''
!Compile('***',Debug=1)
!    Message('Warranty: There IS a main store: ' & tmp:mainstore,'Debug Message',icon:exclamation)
!***
!                !If a Main Store Location exists
!                access:stock.clearkey(sto:location_key)
!                sto:location    = tmp:mainstore
!                sto:part_number = cwp:part_number
!                if access:stock.tryfetch(sto:location_key) = Level:Benign
!                    !Part Number exists in Main Store
!                    !Does the Model Number match?
!                    access:stomodel.clearkey(stm:model_number_key)
!                    stm:ref_number   = sto:Ref_number
!                    stm:manufacturer = job:manufacturer
!                    stm:model_number = job:model_number
!                    if access:stomodel.tryfetch(stm:model_number_key) = Level:Benign
!Compile('***',Debug=1)
!    Message('Warranty Part AT Main Store Location: ' &|
!            '|Stock Ref Number: ' & sto:ref_number & |
!            '|Quantity: ' & sto:quantity_stock,'Debug Message',icon:exclamation)
!***
!
!                        If sto:Suspend
!                            tmp:mainstockrefnumber  = 0
!                            tmp:mainstockquantity   = 0
!                        Else !If sto:Suspend
!                            tmp:mainstockrefnumber  = sto:ref_number
!                            tmp:mainstockquantity   = sto:quantity_stock
!
!                            If sto:sundry_item = 'YES'
!                                tmp:mainstocksundry = 1
!                            Else!If sto:sundry_item = 'YES'
!                                tmp:mainstocksundry = 0
!                            End!If sto:sundry_item = 'YES'
!                        End !If sto:Suspend
!
!
!                    Else!if access:stomodel.tryfetch(stm:model_number_key) = Level:Benign
!                        !Part Number does not match Model Number
!                    End!if access:stomodel.tryfetch(stm:model_number_key) = Level:Benign
!                Else!if access:stock.tryfetch(sto:location_key) = Level:Benign
!                    !Part Number does not exists in Main Store
!                End!if access:stock.tryfetch(sto:location_key) = Level:Benign
!            End!if tmp:mainstore <> ''

            If tmp:engstockrefnumber = 0 !And tmp:mainstockrefnumber = 0
                Case Missive('Cannot add the following part to this job. It has been suspended or cannot be found in the Engineer''s Location:'&|
                  '<13,10>'&|
                  '<13,10>' & Clip(cwp:Part_Number) & ' - ' & Clip(cwp:Description) & '.','ServiceBase 3g',|
                               'mstop.jpg','/OK')
                Of 1 ! OK Button
                End ! Case Missive
            Else
                tmp:ordered = 0
                Do AddParts

            End!If tmp:engstockrefnumber = 0 And tmp:mainstockrefnumber = 0


        End!If cwp:exclude_from_order = 'YES'

    end !loop
    access:commonwp.restorefile(save_cwp_id)
    setcursor()
    Return tmp:ordered
AddParts        Routine
    If access:warparts.primerecord() = Level:Benign
        !Insert a New Part
        wpr:quantity             = cwp:Quantity
        Do AddPartDetails

        If tmp:engstockrefnumber <> 0
            !Engineers Part Exists
            If tmp:engstocksundry
                !If part is a sundry, don't take from stock and just add it.
                wpr:part_ref_number = tmp:engstockrefnumber
                wpr:date_ordered    = Today()
                access:warparts.tryinsert()
            Else!If tmp:engstocksundry
                If cwp:quantity > tmp:engstockquantity
                    !The Quantity Required is more than in the Engineers Location
                    If tmp:engstockquantity = 0
                        !If there is NO engineers stock
                        If tmp:mainstockrefnumber <> 0
                            !If there is stock in Main Stock
                            If wpr:quantity > tmp:mainstockquantity
                                !If Stock required is more than Main Stock Quantity
                                If access:ordpend.primerecord() = Level:Benign
                                    ope:part_ref_number = tmp:engstockrefnumber
                                    ope:job_number      = job:ref_number
                                    ope:part_type       = 'WAR'
                                    ope:supplier        = wpr:supplier
                                    ope:part_number     = wpr:part_number
                                    If tmp:mainstockquantity = 0
                                        !If there is NO Main Store stock
                                        !Order all the stock for the engineer part
                                        ope:quantity        = cwp:quantity
                                        access:ordpend.insert()
                                        tmp:ordered = 1
                                        wpr:pending_ref_number  = ope:ref_number
                                        wpr:part_ref_number = tmp:engstockrefnumber
                                        access:warparts.insert()
                                    Else!If tmp:mainstockquantity = 0
                                        !If there is some Main Store Stock, but not enough
                                        !make an order for the difference, and change the
                                        !part's quantity to match what was taken from Main Store
                                        ope:quantity    = cwp:quantity - tmp:mainstockquantity
                                        access:ordpend.insert()
                                        tmp:ordered = 1
                                        wpr:quantity        = tmp:mainstockquantity
                                        wpr:date_ordered    = Today()
                                        wpr:part_ref_number = tmp:mainstockquantity
                                        access:warparts.insert()

                                        !Take the item from Stock
                                        access:stock.clearkey(sto:ref_number_key)
                                        sto:ref_number      = tmp:mainstockrefnumber
                                        If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
                                            !Take The quantity from stock
                                            sto:quantity_stock  = 0
                                            access:stock.update()

                                            If access:stohist.primerecord() = Level:Benign
                                                shi:quantity    = tmp:mainstockquantity
                                                Do AddStockHistory
                                            End!If access:stohist.primerecord() = Level:Benign
                                        End!If access:stock.tryfetch(sto:ref_number_key) = Level:Benign

                                        !Create a new part for the Pending Order
                                        If access:warparts.primerecord() = Level:benign
                                            wpr:quantity    = cwp:quantity - tmp:mainstockquantity
                                            wpr:pending_ref_number  = ope:ref_number
                                            access:warparts.insert()
                                        End!If access:warparts.primerecord() = Level:benign
                                    End!If tmp:mainstockquantity = 0
                                 End!If access:ordpend.primerecord() = Level:Benign
                            Else!If wpr:quantity > tmp:mainstockquantity
                                !There is sufficent Stock In the Main Store
                                !Take the item from stock
                                wpr:quantity    = cwp:quantity
                                wpr:date_ordered    = Today()
                                wpr:part_ref_number = tmp:mainstockrefnumber
                                access:warparts.insert()

                                access:stock.clearkey(sto:ref_number_key)
                                sto:ref_number  = tmp:mainstockrefnumber
                                If access:stock.tryfetch(sto:ref_number_key) = level:benign
                                    sto:quantity_stock -= cwp:quantity
                                    If sto:quantity_stock < 0
                                        sto:quantity_stock = 0
                                    End!If sto:quantity_stock < 0
                                    access:stock.update()

                                    If access:stohist.primerecord() = Level:benign
                                        shi:quantity    = cwp:quantity
                                        Do AddStockHistory
                                    End!If access:stohist.primerecord() = Level:benign
                                End!If access:stock.tryfetch(sto:ref_number_key) = level:benign
                            End!If wpr:quantity > tmp:mainstockquantity
                        Else!If tmp:mainstockrefnumber <> 0
                            !There is No Main Store Stock Item
                            !Create an order from the Engineers Part
                            If access:ordpend.primerecord() = Level:Benign
                                ope:part_ref_number = tmp:engstockrefnumber
                                ope:job_number      = job:ref_number
                                ope:part_type       = 'WAR'
                                ope:supplier        = wpr:supplier
                                ope:part_number     = wpr:part_number
                                ope:quantity        = cwp:quantity
                                access:ordpend.insert()
                                tmp:ordered = 1
                                wpr:quantity        = cwp:quantity
                                wpr:pending_ref_number  = ope:ref_number
                                access:warparts.insert()
                            End!If access:ordpend.primerecord() = Level:Benign
                        End!If tmp:mainstockrefnumber <> 0
                    Else!If tmp:engstockquantity = 0
                        !There is some stock in the Engineer's Location, but not enough
                        !Take the quantity left from engineers stock
                        wpr:quantity        = tmp:engstockquantity
                        wpr:date_ordered    = Today()
                        wpr:part_ref_number = tmp:engstockrefnumber
                        access:warparts.insert()

                        access:stock.clearkey(sto:ref_number_key)
                        sto:ref_number  = tmp:engstockrefnumber
                        If access:stock.tryfetch(sto:ref_number_key) = level:Benign
                            sto:quantity_stock = 0
                            access:stock.update()

                            If access:stohist.primerecord() = Level:Benign
                                shi:quantity    = tmp:engstockquantity
                                Do AddStockHistory
                            End!If access:stohist.primerecord() = Level:Benign
                        End!If access:stock.tryfetch(sto:ref_number_key) = level:Benign

                        !Is there a main store item as well?
                        If tmp:mainstockrefnumber <> 0
                            !There is a main store item, is there enough in stock?
                            If (cwp:quantity - tmp:engstockquantity) > tmp:mainstockquantity
                                !Quantity requested is higher than main store and engineer put together
                                !Create an order
                                If access:ordpend.primerecord() = Level:benign
                                    ope:part_ref_number = tmp:engstockrefnumber
                                    ope:job_number      = job:ref_number
                                    ope:part_type       = 'WAR'
                                    ope:supplier        = wpr:supplier

                                    If access:warparts.primerecord() = Level:Benign
                                        Do AddPartDetails
                                        wpr:part_ref_number = tmp:engstockrefnumber
                                        If tmp:mainstockquantity = 0
                                            !There is no stock in Main Store
                                            wpr:quantity    = cwp:quantity - tmp:engstockquantity
                                            wpr:pending_ref_number = ope:ref_number
                                            access:warparts.insert()
                                            ope:quantity    = wpr:quantity
                                            access:ordpend.insert()
                                            tmp:ordered = 1
                                        Else!If tmp:mainstockquantity = 0
                                            !Take the stock from main store.
                                            !Create an order pending for the rest
                                            wpr:quantity    = tmp:mainstockquantity
                                            wpr:date_ordered    = Today()
                                            wpr:part_ref_number = tmp:mainstockrefnumber
                                            access:warparts.insert()

                                            If access:warparts.primerecord() = Level:benign
                                                Do AddPartDetails
                                                wpr:part_ref_number = tmp:engstockrefnumber
                                                wpr:quantity        = cwp:quantity - (tmp:engstockquantity + tmp:mainstockquantity)
                                                wpr:pending_ref_number  = ope:ref_number
                                                access:warparts.insert()
                                                ope:quantity        = wpr:quantity
                                                access:ordpend.insert()
                                                tmp:ordered = 1
                                            End!If access:warparts.primerecord() = Level:benign

                                            Access:stock.clearkey(sto:ref_number_key)
                                            sto:ref_number  = tmp:mainstockrefnumber
                                            If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
                                                sto:quantity_stock = 0
                                                access:stock.update()
                                            End!If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
                                            If access:stohist.primerecord() = Level:Benign
                                                shi:quantity    = tmp:mainstockquantity
                                                Do AddStockHistory
                                            End!If access:stohist.primerecord() = Level:Benign
                                        End!If tmp:mainstockquantity = 0
                                    End!If access:warparts.primerecord() = Level:Benign
                                End!If access:ordpend.primerecord(0 = Level:benign
                            Else!If (cwp:quantity - tmp:engstockquantity) > tmp:mainstockquantity
                                !The is sufficient stock left in the Main Store
                                wpr:quantity    = cwp:quantity - tmp:engstockquantity
                                wpr:part_ref_number = tmp:mainstockrefnumber
                                wpr:date_ordered    = Today()
                                access:warparts.insert()

                                access:stock.clearkey(sto:ref_number_key)
                                sto:ref_number  = tmp:mainstockrefnumber
                                If Access:stock.tryfetch(sto:ref_number_key) = level:Benign
                                    sto:quantity_stock  -= (cwp:quantity - tmp:engstockquantity)
                                    If sto:quantity_stock < 0
                                        sto:quantity_stock = 0
                                    End!If sto:quantity_stock < 0
                                    access:stock.update()
                                End!If Access:stock.tryfetch(sto:ref_number_key) = level:Benign

                                If access:stohist.primerecord() = Level:Benign
                                    shi:quantity    = cwp:quantity - tmp:engstockquantity
                                    Do AddStockHistory
                                End!If access:stohist.primerecord() = Level:Benign
                            End!If (cwp:quantity - tmp:engstockquantity) > tmp:mainstockquantity
                        Else!If tmp:mainstockrefnumber <> 0
                            !If there is no Main Store Item
                            !Create an order for the remaining
                            If access:ordpend.primerecord() = Level:Benign
                                ope:part_ref_number = tmp:engstockrefnumber
                                ope:job_number      = job:ref_number
                                ope:part_type       = 'WAR'
                                ope:supplier        = wpr:supplier
                                ope:part_number     = wpr:part_number
                                ope:quantity        = cwp:quantity - tmp:engstockquantity
                                access:ordpend.insert()
                                tmp:ordered = 1
                                If access:warparts.primerecord() = Level:Benign
                                    Do AddPartDetails
                                    wpr:pending_ref_number  = ope:ref_number
                                    wpr:part_ref_number     = tmp:engstockrefnumber
                                    wpr:quantity            = cwp:quantity - tmp:engstockquantity
                                    access:warparts.insert()
                                End!If access:warparts.primerecord() = Level:Benign
                            End!If access:ordpend.primerecord() = Level:Benign
                        End!If tmp:mainstockrefnumber <> 0
                    End!If tmp:engstockquantity = 0
                Else!If cwp:quantity > tmp:engstockquantity
                    !If there is enough in the engineer's location
                    wpr:quantity   = cwp:quantity
                    wpr:date_ordered    = Today()
                    wpr:part_ref_number = tmp:engstockrefnumber
                    access:warparts.insert()
                    access:stock.clearkey(sto:ref_number_key)
                    sto:ref_number  = tmp:engstockrefnumber
                    If access:stock.tryfetch(sto:ref_number_key) = Level:benign
                        sto:quantity_stock -= cwp:quantity
                        If sto:quantity_stock < 0
                            sto:quantity_stock = 0
                        End!If sto:quantity_stock < 0
                        access:stock.update()
                        If access:stohist.primerecord() = level:Benign
                            shi:quantity    = cwp:quantity
                            Do AddStockHistory
                        End!If access:stohist.primerecord() = level:Benign
                    End!If access:stock.tryfetch(sto:ref_number_key) = Level:benign
                End!If cwp:quantity > tmp:engstockquantity
            End!If tmp:engstocksundry
        Else!If tmp:engstockrefnumber <> 0
            !If there is no engineer's stock
            !Is there a main stock item?
            If tmp:mainstockrefnumber <> 0
                !There is a main store item
                If tmp:mainstocksundry
                    wpr:quantity    = tmp:mainstockquantity
                    wpr:date_ordered = Today()
                    wpr:part_ref_number  = tmp:mainstockrefnumber
                    access:warparts.insert()
                Else!If tmp:mainstocksundry
                    If cwp:quantity > tmp:mainstockquantity
                        !Quantity Required is more than main store quantity
                        If access:ordpend.primerecord() = Level:Benign
                            ope:part_ref_number     = tmp:mainstockrefnumber
                            ope:job_number          = job:ref_number
                            ope:part_type           = 'WAR'
                            ope:supplier            = wpr:supplier
                            ope:part_number         = wpr:part_number
                            If tmp:mainstockquantity = 0
                                !If there is no stock in the main store
                                ope:quantity    = cwp:quantity
                                access:ordpend.insert()
                                tmp:ordered = 1
                                wpr:quantity    = cwp:quantity
                                wpr:pending_ref_number  = ope:ref_number
                                wpr:part_ref_number     = tmp:mainstockrefnumber
                                access:warparts.insert()
                            Else!If tmp:mainstockquantity = 0
                                !take the remaining items from main store, and order the rest
                                ope:quantity    = cwp:quantity - tmp:mainstockquantity
                                access:ordpend.insert()
                                tmp:ordered = 1
                                wpr:quantity        = tmp:mainstockquantity
                                wpr:date_ordered    = Today()
                                wpr:part_ref_number = tmp:mainstockrefnumber
                                access:warparts.insert()
                                access:stock.clearkey(sto:ref_number_key)
                                sto:ref_number      = tmp:mainstockrefnumber
                                If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
                                    sto:quantity_stock = 0
                                    access:stock.update()
                                    If access:stohist.primerecord() = Level:Benign
                                        shi:quantity    = tmp:mainstockquantity
                                        If access:stohist.tryinsert()
                                            access:stohist.cancelautoinc()
                                        End!If access:stohist.tryinsert()
                                    End!If access:stohist.primerecord() = Level:Benign
                                End!If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
                                If access:warparts.primerecord() = Level:Benign
                                    Do AddPartDetails
                                    wpr:quantity            = cwp:quantity - tmp:mainstockquantity
                                    wpr:pending_ref_number  = ope:ref_number
                                    wpr:part_ref_number     = tmp:mainstockrefnumber
                                    If access:warparts.tryinsert()
                                        access:warparts.cancelautoinc()
                                    End!If access:warparts.tryinsert()
                                End!If access:warparts.primerecord() = Level:Benign
                            End!If tmp:mainstockquantity = 0
                        End!If access:ordpend.primerecord() = Level:Benign
                    Else!If cwp:quantity > tmp:mainstockquantity
                        !If there is enough quantity in the main store
                        wpr:quantity        = cwp:quantity
                        wpr:date_ordered    = Today()
                        wpr:part_ref_number = tmp:mainstockrefnumber
                        access:warparts.insert()
                        access:stock.clearkey(sto:ref_number_key)
                        If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
                            !Found
                            sto:quantity_stock -= cwp:quantity
                            If sto:quantity_stock < 0
                                sto:quantity_stock = 0
                            End!If sto:quantity_stock < 0
                            access:stock.update()
                        Else! If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
                            !Error
                        End! If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
                        If access:stohist.primerecord() = Level:Benign
                            shi:quantity    = cwp:quantity
                            Do AddStockHistory
                        End!If access:stohist.primerecord() = Level:Benign
                    End!If cwp:quantity > tmp:mainstockquantity
                End!If tmp:mainstocksundry
            Else!If tmp:mainstockrefnumber <> 0
                !If there is no main store item
                If access:ordpend.primerecord() = Level:Benign
                    ope:part_ref_number     = ''
                    ope:job_number          = job:ref_number
                    ope:part_type           = 'WAR'
                    ope:supplier            = wpr:supplier
                    ope:part_number         = wpr:part_number
                    ope:quantity            = wpr:quantity
                    If access:ordpend.tryinsert()
                        access:ordpend.cancelautoinc()
                    End!If access:ordpend.tryinsert()
                    wpr:pending_ref_number  = ope:ref_number
                    access:warparts.insert()
                End!If access:ordpend.primerecord() = Level:Benign
            End!If tmp:mainstockrefnumber <> 0
        End!If tmp:engstockrefnumber <> 0
    End!If access:warparts.primerecord() = Level:Benign
AddPartDetails      Routine
    wpr:ref_number           = job:ref_number
    wpr:adjustment           = cwp:adjustment
    wpr:part_number          = cwp:Part_Number
    wpr:description          = cwp:Description
    wpr:supplier             = cwp:Supplier
    wpr:purchase_cost        = cwp:Purchase_Cost
    wpr:sale_cost            = cwp:Sale_Cost

    wpr:fault_codes_checked  = 'YES'
    If wpr:fault_Code1 = ''
        wpr:Fault_Code1         = cwp:fault_Code1
    End!If wpr:FaultCode1           = ''
    If wpr:Fault_Code2          = ''
        wpr:Fault_Code2         = cwp:fault_Code2
    End!If wpr:FaultCode2           = ''
    If wpr:Fault_Code3          = ''
        wpr:Fault_Code3         = cwp:fault_Code3
    End!If wpr:FaultCode3           = ''
    If wpr:Fault_Code4          = ''
        wpr:Fault_Code4         = cwp:fault_Code4
    End!If wpr:FaultCode4           = ''
    If wpr:Fault_Code5          = ''
        wpr:Fault_Code5         = cwp:fault_Code5
    End!If wpr:FaultCode5           = ''
    If wpr:Fault_Code6          = ''
        wpr:Fault_Code6         = cwp:fault_Code6
    End!If wpr:FaultCode6           = ''
    If wpr:Fault_Code7          = ''
        wpr:Fault_Code7         = cwp:fault_Code7
    End!If wpr:FaultCode7           = ''
    If wpr:Fault_Code8          = ''
        wpr:Fault_Code8         = cwp:fault_Code8
    End!If wpr:FaultCode8           = ''
    If wpr:Fault_Code9          = ''
        wpr:Fault_Code9         = cwp:fault_Code9
    End!If wpr:FaultCode9           = ''
    If wpr:Fault_Code10         = ''
        wpr:Fault_Code10        = cwp:fault_Code10
    End!If wpr:FaultCode10      = ''
    If wpr:Fault_Code11         = ''
        wpr:Fault_Code11        = cwp:fault_Code11
    End!If wpr:FaultCode11      = ''
    If wpr:Fault_Code12         = ''
        wpr:Fault_Code12        = cwp:fault_Code12
    End
    wpr:exclude_from_order   = cwp:exclude_from_order
AddStockHistory     Routine
    shi:ref_number           = sto:ref_number
    access:users.clearkey(use:password_key)
    use:password              = glo:password
    access:users.fetch(use:password_key)
    shi:user                  = use:user_code
    shi:date                 = today()
    shi:transaction_type     = 'DEC'
    shi:despatch_note_number = wpr:despatch_note_number
    shi:job_number           = job:ref_number
    shi:purchase_cost        = wpr:purchase_cost
    shi:sale_cost            = wpr:sale_cost
    shi:retail_cost          = wpr:retail_cost
    shi:notes                = 'STOCK DECREMENTED'
    if access:stohist.insert()
       access:stohist.cancelautoinc()
    end
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
StripReturn          PROCEDURE  (in_string)           ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    in_string = CLIP(LEFT(in_string))

    STR_LEN#  = LEN(in_string)
    STR_POS#  = 1

    in_string = UPPER(SUB(in_string,STR_POS#,1)) & SUB(in_string,STR_POS#+1,STR_LEN#-1)

    LOOP STR_POS# = 1 TO STR_LEN#

     IF VAL(SUB(in_string,STR_POS#,1)) < 32 Or VAL(SUB(in_string,STR_POS#,1)) > 126
        in_string = SUB(in_string,1,STR_POS#-1) & ' ' & SUB(in_string,STR_POS#+1,STR_LEN#-1)
     .
    .

    RETURN(in_string)
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
StripComma           PROCEDURE  (in_string)           ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Return BHStripReplace(in_string,',',' ')

!    in_string = CLIP(LEFT(in_string))
!
!
!    STR_LEN#  = LEN(in_string)
!    STR_POS#  = 1
!
!    in_string = UPPER(SUB(in_string,STR_POS#,1)) & SUB(in_string,STR_POS#+1,STR_LEN#-1)
!
!    LOOP STR_POS# = 1 TO STR_LEN#
!
!     IF SUB(in_string,STR_POS#,1) = ','
!        in_string = SUB(in_string,1,STR_POS#-1) & ' ' & SUB(in_string,STR_POS#+1,STR_LEN#-1)
!     .
!    .
!
!    RETURN(in_string)
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
StripPoint           PROCEDURE  (in_string)           ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    in_string = CLIP(LEFT(in_string))

    STR_LEN#  = LEN(in_string)
    STR_POS#  = 1

    in_string = UPPER(SUB(in_string,STR_POS#,1)) & SUB(in_string,STR_POS#+1,STR_LEN#-1)

    LOOP STR_POS# = 1 TO STR_LEN#

     IF SUB(in_string,STR_POS#,1) = '.'
        in_string = SUB(in_string,1,STR_POS#-1) & SUB(in_string,STR_POS#+1,STR_LEN#-1)
     .
    .

    RETURN(in_string)
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
