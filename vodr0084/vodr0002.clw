

   MEMBER('vodr0084.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('VODR0002.INC'),ONCE        !Local module procedure declarations
                     END


StagingTableReconciliationReport PROCEDURE            !Generated from procedure template - Window

Progress:Thermometer BYTE
local                CLASS
ReturnBranchID       Procedure(String f:Account),String
UpdateProgressWindow Procedure(String func:Text)
ReturnSQLDate        Procedure(Date f:Date),String
WriteToLine          Procedure(String f:Type,String f:Type2,Long f:LineNumber,Real f:Total)
                     END
tmp:VersionNumber    STRING(30)
tmp:Desktop          CSTRING(255)
tmp:ExcelFileName    CSTRING(255)
tmp:RepositoryDir    CSTRING(255)
tmp:DatabaseServer   CSTRING(255)
tmp:DatabaseName     CSTRING(255)
tmp:Line500Report    BYTE(0)
tmp:Line500StartDate DATE
tmp:Line500EndDate   DATE
tmp:OracleReport     BYTE
tmp:OracleStartDate  DATE
tmp:OracleEndDate    DATE
tmp:CIDReport        BYTE
tmp:CIDStartDate     DATE
tmp:CIDEndDate       DATE
tmp:GRNSuffix        STRING(2)
POQueue              QUEUE,PRE(poque)
PurchaseOrderNumber  STRING(20)
BatchValue           STRING(20)
                     END
StatusQueue          QUEUE,PRE(staque)
StatusMessage        STRING(60)
                     END
QuickWindow          WINDOW('Window'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(160,64,360,300),USE(?PanelMain),FILL(0D6EAEFH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(164,84,352,44),USE(?PanelTip),FILL(0D6EAEFH)
                       GROUP('Top Tip'),AT(168,88,344,36),USE(?GroupTip),BOXED,TRN
                       END
                       STRING(@s255),AT(172,104,300,),USE(SRN:TipText),TRN
                       BUTTON,AT(476,92,36,32),USE(?ButtonHelp),TRN,FLAT,ICON('F1Help.jpg')
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Staging Table Reconciliation Report'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(164,132,352,198),USE(?Panel55),FILL(09A6A7CH)
                       CHECK('Line 500 Report'),AT(264,140),USE(tmp:Line500Report),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0')
                       BUTTON,AT(388,150),USE(?PopCalendar),TRN,FLAT,ICON('lookupp.jpg')
                       PROMPT('Start Date'),AT(264,156),USE(?tmp:Line500StartDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@d6b),AT(320,154,64,10),USE(tmp:Line500StartDate),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                       BUTTON,AT(388,174),USE(?PopCalendar:2),TRN,FLAT,ICON('lookupp.jpg')
                       PROMPT('End Date'),AT(264,178),USE(?tmp:Line500EndDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@d6),AT(320,178,64,10),USE(tmp:Line500EndDate),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                       CHECK('Oracle Report'),AT(264,200),USE(tmp:OracleReport),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0')
                       PROMPT('Start Date'),AT(264,214),USE(?tmp:OracleStartDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@d6),AT(320,214,64,10),USE(tmp:OracleStartDate),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                       BUTTON,AT(387,210),USE(?PopCalendar:3),TRN,FLAT,ICON('lookupp.jpg')
                       PROMPT('End Date'),AT(264,234),USE(?tmp:OracleEndDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@d6),AT(320,236,64,10),USE(tmp:OracleEndDate),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                       BUTTON,AT(388,232),USE(?PopCalendar:4),TRN,FLAT,ICON('lookupp.jpg')
                       CHECK('CID Report'),AT(264,264),USE(tmp:CIDReport),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0')
                       BUTTON,AT(388,274),USE(?PopCalendar:5),TRN,FLAT,ICON('lookupp.jpg')
                       PROMPT('Start Date'),AT(264,278),USE(?tmp:CIDStartDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@d6),AT(320,278,64,10),USE(tmp:CIDStartDate),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                       PROMPT('End Date'),AT(264,300),USE(?tmp:CIDEndDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@d6),AT(320,300,64,10),USE(tmp:CIDEndDate),FONT(,,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                       BUTTON,AT(388,298),USE(?PopCalendar:6),TRN,FLAT,ICON('lookupp.jpg')
                       BUTTON,AT(376,332),USE(?Ok),TRN,FLAT,MSG('Accept operation'),TIP('Accept Operation'),ICON('printp.jpg')
                       BUTTON,AT(444,332),USE(?Cancel),TRN,FLAT,MSG('Cancel Operation'),TIP('Cancel Operation'),ICON('cancelp.jpg')
                       PROMPT('Report Version'),AT(168,340),USE(?ReportVersion),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                     END

!Progress Window (DBH: 22-03-2004)
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progresswindow WINDOW('Progress...'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH), |
         CENTER,IMM,ICON('cellular3g.ico'),WALLPAPER('sbback.jpg'),TILED,TIMER(1),GRAY,DOUBLE
       PANEL,AT(160,64,360,300),USE(?Panel5),FILL(0D6E7EFH)
       PANEL,AT(164,68,352,12),USE(?Panel1),FILL(09A6A7CH)
       LIST,AT(208,88,268,206),USE(?List1),VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH), |
           FORMAT('20L(2)|M'),FROM(StatusQueue),GRID(COLOR:White)
       PANEL,AT(164,84,352,246),USE(?Panel4),FILL(09A6A7CH)
       PROGRESS,USE(progress:thermometer),AT(206,314,268,12),RANGE(0,100)
       STRING(''),AT(259,300,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,080FFFFH,FONT:bold), |
           COLOR(09A6A7CH)
       STRING(''),AT(232,136,161,10),USE(?progress:pcttext),TRN,HIDE,CENTER,FONT('Arial',8,,)
       PANEL,AT(164,332,352,28),USE(?Panel2),FILL(09A6A7CH)
       PROMPT('Report Progress'),AT(168,70),USE(?WindowTitle2),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
       BUTTON,AT(444,332),USE(?ProgressCancel),TRN,FLAT,LEFT,ICON('Cancelp.jpg')
       BUTTON,AT(376,332),USE(?Button:OpenReportFolder),TRN,FLAT,HIDE,ICON('openrepp.jpg')
       BUTTON,AT(444,332),USE(?Finish),TRN,FLAT,HIDE,ICON('Finishp.jpg')
     END
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
E1                   Class(oiExcel)
Init                   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0),byte,proc,virtual,name('INIT@F17OIEXCEL')
Kill                   PROCEDURE (byte pUnloadCOM=1),byte,proc,virtual,name('KILL@F17OIEXCEL')
TakeEvent              PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0),virtual,name('TAKEEVENT@F17OIEXCEL')
                     End

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!File Declarations
mqoracle_por         FILE,DRIVER('Scalable'),OEM,OWNER(DatabaseName),NAME('mqoracle_por'),PRE(por),BINDABLE,THREAD
pk_index                 KEY(por:mop_number),NAME('pk_index'),NOCASE,PRIMARY
idx_message_id           KEY(por:mop_message_id),DUP,NAME('idx_message_id')
Record                   RECORD,PRE()
mop_number                  LONG
mop_message_id              STRING(24)
mop_currency_code           STRING(3)
mop_currency_unit_pr        DECIMAL(17,2)
mop_deliver_to_req          LONG
mop_dest_org_id             CSTRING(21)
mop_gl_date                 DATE
mop_interface_src           CSTRING(21)
mop_item_id                 LONG
mop_org_id                  LONG
mop_prepare_id              LONG
mop_qty                     LONG
mop_supplier_number         STRING(15)
mop_sb_po_no                STRING(13)
mop_type                    STRING(1)
mop_resend_yn               STRING(1)
mop_timestamp               STRING(8)
mop_timestamp_GROUP         GROUP,OVER(mop_timestamp)
mop_timestamp_DATE            DATE
mop_timestamp_TIME            TIME
                            END
mop_response                STRING(3)
mop_err_mess                CSTRING(255)
mop_url                     CSTRING(255)
                         END
                     END

sbmq_stf             FILE,DRIVER('Scalable'),OEM,OWNER(DatabaseName),NAME('sbmq_stf'),PRE(stf),BINDABLE,THREAD
pk_index                 KEY(stf:ms_number),NAME('pk_index'),NOCASE,PRIMARY
idx_message_id           KEY(stf:ms_message_id),DUP,NAME('idx_mesage_id')
Record                   RECORD,PRE()
ms_number                   LONG
ms_trans_desc               STRING(50)
ms_bill_ac_no               STRING(10)
ms_cust_ord_no              STRING(20)
ms_sb_inv_no                STRING(10)
ms_amount_excl_vat          DECIMAL(17,2)
ms_vat_rate                 DECIMAL(17,2)
ms_cost                     DECIMAL(17,2)
ms_qty                      LONG
ms_inv_date                 DATE
ms_inv_due_date             DATE
ms_date_req                 DATE
ms_serv_code                STRING(20)
ms_message_id               STRING(24)
ms_resend_yn                STRING(1)
ms_timestamp                STRING(8)
ms_timestamp_GROUP          GROUP,OVER(ms_timestamp)
ms_timestamp_DATE             DATE
ms_timestamp_TIME             TIME
                            END
ms_response                 STRING(3)
ms_err_mess                 CSTRING(255)
                         END
                     END

sbmq_riv             FILE,DRIVER('Scalable'),OEM,OWNER(DatabaseName),NAME('sbmq_riv'),PRE(riv),BINDABLE,THREAD
pk_index                 KEY(riv:mr_number),NAME('pk_index'),NOCASE,PRIMARY
idx_message_id           KEY(riv:mr_message_id),DUP,NAME('idx_message_id')
Record                   RECORD,PRE()
mr_number                   LONG
mr_main_ret_acc_no          STRING(20)
mr_sub_ret_acc_no           STRING(20)
mr_franchise_branch         STRING(10)
mr_sb_inv_no                STRING(10)
mr_service_code             STRING(20)
mr_amt_excl_vat             DECIMAL(17,2)
mr_vat_rate                 DECIMAL(17,2)
mr_cost                     DECIMAL(17,2)
mr_inv_date                 DATE
mr_inv_due_date             DATE
mr_despatch_date            DATE
mr_date_required            DATE
mr_err_mess                 CSTRING(255)
mr_resend_yn                STRING(1)
mr_message_id               STRING(24)
mr_response                 STRING(3)
mr_timestamp                STRING(8)
mr_timestamp_GROUP          GROUP,OVER(mr_timestamp)
mr_timestamp_DATE             DATE
mr_timestamp_TIME             TIME
                            END
                         END
                     END                       

sbmq_aow             FILE,DRIVER('Scalable'),OEM,OWNER(DatabaseName),NAME('sbmq_aow'),PRE(aow),BINDABLE,THREAD
pk_index                 KEY(aow:ma_number),NAME('pk_index'),NOCASE,PRIMARY
idx_message_id           KEY(aow:ma_message_id),DUP,NAME('idx_message_id')
Record                   RECORD,PRE()
ma_number                   LONG
ma_trans_desc               STRING(50)
ma_bill_ac_no               STRING(10)
ma_sb_repair_job            STRING(20)
ma_sb_inv_no                STRING(10)
ma_amount_excl_vat          DECIMAL(17,2)
ma_vat_rate                 DECIMAL(17,2)
ma_cost                     DECIMAL(17,2)
ma_qty                      LONG
ma_inv_date                 DATE
ma_inv_due_date             DATE
ma_date_req                 DATE
ma_serv_code                STRING(20)
ma_message_id               STRING(24)
ma_resend_yn                STRING(1)
ma_err_mess                 CSTRING(255)
ma_timestamp                STRING(8)
ma_timestamp_GROUP          GROUP,OVER(ma_timestamp)
ma_timestamp_DATE             DATE
ma_timestamp_TIME             TIME
                            END
ma_response                 STRING(3)
                         END
                     END                       

sbmq_cid             FILE,DRIVER('Scalable'),OEM,OWNER(DatabaseName),NAME('sbmq_cid'),PRE(cid),BINDABLE,THREAD
pk_number                KEY(cid:cid_number),NAME('pk_number'),NOCASE,PRIMARY
idx_message_id           KEY(cid:cid_message_id),DUP,NAME('idx_mesage_id')
Record                   RECORD,PRE()
cid_number                  LONG
cid_message_id              STRING(24)
cid_msisdn_or_iccid         STRING(10)
cid_account_id              CSTRING(61)
cid_source                  CSTRING(61)
cid_topic_code              CSTRING(61)
cid_comment                 CSTRING(255)
cid_response                STRING(3)
cid_timestamp               STRING(8)
cid_timestamp_GROUP         GROUP,OVER(cid_timestamp)
cid_timestamp_DATE            DATE
cid_timestamp_TIME            TIME
                            END
cid_err_mess                CSTRING(255)
                         END
                     END 

content_man          FILE,DRIVER('Scalable'),OEM,OWNER(DatabaseName),NAME('content_man'),PRE(cm),BINDABLE,THREAD
pk_index                 KEY(cm:cm_number),NAME('pk_index'),NOCASE,PRIMARY
idx_pdf_path             KEY(cm:cm_pdf_path),DUP,NOCASE,OPT
Record                   RECORD,PRE()
cm_number                   LONG
cm_pdf_path                 STRING(255)
cm_meta_data                STRING(255)
cm_pid                      STRING(255)
cm_err_mess                 STRING(255)
cm_datetime                 STRING(8)
cm_datetime_GROUP           GROUP,OVER(cm_datetime)
cm_datetime_DATE              DATE
cm_datetime_TIME              TIME
                            END
cm_response                 STRING(3)
                         END
                     END 
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

CIDReport           Routine
Data
local:CurrentLine            Long()
local:InvoiceCount           Long()
local:ReceivedCount          Long()
local:ReceivedCost           Real()
local:InvoiceTotal           Real()
local:TotalInvoiceCost       Real()
Code
    local.UpdateProgressWindow('')
    local.UpdateProgressWindow('==== CID ====')
    local.UpdateProgressWindow('')
    E1.InsertWorksheet()
    E1.RenameWorkSheet('CID')
    E1.WriteToCell('Staging Table Reconciliation Report', 'A1')
    E1.WriteToCell(tmp:VersionNumber, 'J1')
    E1.WriteToCell('CID', 'A3')
    E1.WriteToCell('Date From', 'A4')
    E1.WriteToCell(Format(tmp:CIDStartDate, @d06), 'B4')
    E1.WriteToCell('Date To', 'A5')
    E1.WriteToCell(Format(tmp:CIDEndDate, @d06), 'B5')
    E1.WriteToCell('Quantity', 'D6')
    E1.WriteToCell('Quantity', 'H6')
    E1.WriteToCell('Bookings Transmitted', 'A7')
    E1.WriteToCell('Bookings Received', 'A8')
    E1.WriteToCell('Despatches Trasmitted','G7')
    E1.WriteToCell('Despatches Received','G8')

    E1.WriteToCell('Exceptions','A9')
    E1.WriteToCell('Booking Exceptions','A12')
    E1.WriteToCell('Job Number','A13')

    local.UpdateProgressWindow('Calculating Send / Receive Data ...')

    local:CurrentLine = 14
    local:InvoiceCount = 0

    Access:AUDIT.ClearKey(aud:ActionOnlyKey)
    aud:Date       = tmp:CIDStartDate
    aud:Action     = 'RAPID NEW JOB BOOKING INITIAL ENTRY'
    Set(aud:ACtionOnlyKey,aud:ActionOnlyKey)
    Loop
        If Access:AUDIT.NEXT()
           Break
        End !If
        If aud:Date       > tmp:CIDEndDate       |
        Or aud:Action     <> 'RAPID NEW JOB BOOKING INITIAL ENTRY'      |
            Then Break.  ! End If

        Do GetNextRecord2
        Do CancelCheck
        If tmp:Cancel
            Break
        End ! If tmp:Cancel

        Access:JOBSE.Clearkey(jobe:RefNumberKey)
        jobe:RefNumber  = aud:Ref_Number
        If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            ! Found
            If ~jobe:VSACustomer
                Cycle
            End ! If ~jobe:VSACustomer
        Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            ! Error
            Cycle
        End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

        Access:JOBS.Clearkey(job:Ref_Number_Key)
        job:Ref_Number  = aud:Ref_Number
        If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
            ! Found

        Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
            ! Error
            Cycle
        End ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign

        local:InvoiceCount += 1
        sbmq_cid{prop:sql} = 'SELECT * FROM "sbmq_cid" WHERE "cid_msisdn_or_iccid" = ''' & Clip(job:Mobile_Number) & ''' AND "cid_topic_code" = ''Repair Booked In'''
        Next(sbmq_cid)
        If Error()
            local.WriteToLine('CID','Booking',local:CurrentLine,0)
            local:CurrentLine += 1
        Else ! If Error()
            local:ReceivedCount += 1
        End ! If Error()
    End !Loop

    E1.WriteToCell(local:InvoiceCount,'D7')
    E1.WriteToCell(local:ReceivedCount,'D8')

    local:InvoiceCount = 0
    
    local:ReceivedCount = 0
    local:CurrentLine += 1

    E1.WriteToCell('Despatch Exceptions','A' & local:CurrentLine)
    E1.SetCellFontStyle('Bold','A' & local:CurrentLine)
    local:CurrentLine += 1

    E1.WriteToCell('Job Number','A' & local:CurrentLine)
    E1.SetCellFontStyle('Bold','A' & local:CurrentLine,'C' & local:CurrentLine)
    E1.SetCellBackgroundColor(color:silver, 'A' & local:CurrentLine, 'J' & local:CurrentLine)

    local:CurrentLine += 1

    Access:AUDIT.ClearKey(aud:DateJobKey)
    aud:Date       = tmp:CIDStartDate
    Set(aud:DateJobKey,aud:DateJobKey)
    Loop
        If Access:AUDIT.NEXT()
           Break
        End !If
        If aud:Date       > tmp:CIDEndDate       |
            Then Break.  ! End If

        If ~Instring('JOB DESPATCHED VIA',aud:Action,1,1) And |
                aud:Action <> 'DESPATCH FROM RRC' And |
                ~Instring('EXCHANGE DESPATCHED VIA',aud:Action,1,1)
            Cycle
        End ! If Instring('JOB DESPATCHED',aud:Action,1,1)

        If Instring('JOB DESPATCHED VIA',aud:Action,1,1)
            If jobe:WebJob
                Cycle
            End ! If jobe:WebJob
        End ! If aud:Action = 'DESPATCH FROM ARC'

        Do GetNextRecord2
        Do CancelCheck
        If tmp:Cancel
            Break
        End ! If tmp:Cancel

        Access:JOBSE.Clearkey(jobe:RefNumberKey)
        jobe:RefNumber  = aud:Ref_Number
        If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            ! Found
            If ~jobe:VSACustomer
                Cycle
            End ! If ~jobe:VSACustomer
        Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            ! Error
            Cycle
        End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

        Access:JOBS.Clearkey(job:Ref_Number_Key)
        job:Ref_Number  = aud:Ref_Number
        If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
            ! Found

        Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
            ! Error
            Cycle
        End ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign

        local:InvoiceCount += 1
        sbmq_cid{prop:sql} = 'SELECT * FROM "sbmq_cid" WHERE "cid_msisdn_or_iccid" = ''' & Clip(job:Mobile_Number) & ''' AND "cid_topic_code" = ''Repair Collected'''
        Next(sbmq_cid)
        If Error()
            local.WriteToLine('CID','Despatch',local:CurrentLine,0)
            local:CurrentLine += 1
        Else ! If Error()
            local:ReceivedCount += 1
        End ! If Error()
    End !Loop

    E1.WriteToCell(local:InvoiceCount,'H7')

    E1.WriteToCell(local:ReceivedCount,'H8')

    E1.WriteToCell('=(D7-D8)', 'D9')
    E1.WriteToCell('=(H7-H8)', 'H9')


    E1.SetCellFontName('Tahoma', 'A1', 'J' & local:CurrentLine)
    E1.SetCellFontSize(8, 'J1')
    E1.SetCellFontSize(8, 'A2', 'J' & local:CurrentLine)
    E1.SetCellFontSize(10, 'A1')
    E1.SetCellFontStyle('Bold', 'A1', 'A12')
    E1.SetCellFontStyle('Bold', 'D6', 'I6')
    E1.SetCellFontStyle('Bold', 'A13', 'I13')
    E1.SetCellFontStyle('Bold','G7','G8')
    E1.SetColumnWidth('A', 'J')
    E1.SetCellBackgroundColor(color:silver, 'A1', 'J1')
    E1.SetCellBackgroundColor(color:silver, 'A13', 'J13')
    E1.SelectCells('A14')
OracleReport        Routine
Data
local:CurrentLine            Long()
local:InvoiceCount           Long()
local:ReceivedCount          Long()
local:ReceivedCost           Real()
local:InvoiceTotal           Real()
local:TotalInvoiceCost       Real()
Code
    local.UpdateProgressWindow('')
    local.UpdateProgressWindow('==== Oracle ====')
    local.UpdateProgressWindow('')
    E1.InsertWorksheet()
    E1.RenameWorkSheet('Oracle')
    E1.WriteToCell('Staging Table Reconciliation Report', 'A1')
    E1.WriteToCell(tmp:VersionNumber, 'J1')
    E1.WriteToCell('Oracle', 'A3')
    E1.WriteToCell('Date From', 'A4')
    E1.WriteToCell(Format(tmp:OracleStartDate, @d06), 'B4')
    E1.WriteToCell('Date To', 'A5')
    E1.WriteToCell(Format(tmp:OracleEndDate, @d06), 'B5')
    E1.WriteToCell('Quantity', 'D6')
    !E1.WriteToCell('Value', 'E6')
    E1.WriteToCell('Quantity', 'H6')
    !E1.WriteToCell('Value', 'I6')
    E1.WriteToCell('Part Receipts Transmitted', 'A7')
    E1.WriteToCell('Part Receipts Received', 'A8')
    E1.WriteToCell('3rd Party Receipts Transmitted','G7')
    E1.WriteToCell('3rd Party Receipts Received','G8')


    E1.WriteToCell('Exceptions','A9')
    E1.WriteToCell('Part Receipts Exceptions','A12')
    E1.WriteToCell('PO Number','A13')
    E1.WriteToCell('GRV Number','B13')
    E1.WriteToCell('Currency','C13')
    E1.WriteToCell('Value (ex-VAT)','D13')

    local.UpdateProgressWindow('Calculating Send / Receive Data ...')

    local:CurrentLine = 14
    local:InvoiceCount = 0

    Access:GRNOTES.ClearKey(grn:Date_Recd_Key)
    grn:Goods_Received_Date = tmp:OracleStartDate
    Set(grn:Date_Recd_Key,grn:Date_Recd_Key)
    Loop
        If Access:GRNOTES.NEXT()
           Break
        End !If
        If grn:Goods_Received_Date > tmp:OracleEndDate       |
            Then Break.  ! End If
        If grn:BatchRunNotPrinted = True
            Cycle
        End ! If grn:BatchRunNotPrinted = True

        tmp:GRNSUffix = 0
        Access:GRNOTESALIAS.Clearkey(grnali:Order_Number_Key)
        grnali:Order_Number = grn:Order_Number
        grnali:Goods_Received_Date = 0
        Set(grnali:Order_Number_Key,grnali:Order_Number_Key)
        Loop ! Begin GRNOTESALIAS Loop
            If Access:GRNOTESALIAS.Next()
                Break
            End ! If !Access
            If grnali:Order_Number <> grn:Order_Number
                Break
            End ! If

            Do GetNextRecord2
            Do CancelCheck
            If tmp:Cancel
                Break
            End ! If tmp:Cancel
            !Does this GRN Have Parts Attached?

            PartsAttached# = False
            FoundValue# = False
            Access:ORDPARTS.ClearKey(orp:Part_Number_Key)
            orp:Order_Number = grnali:Order_Number
            Set(orp:Part_Number_Key,orp:Part_Number_Key)
            Loop
                If Access:ORDPARTS.NEXT()
                   Break
                End !If
                If orp:Order_Number <> grnali:Order_Number      |
                    Then Break.  ! End If
                If orp:GRN_Number = grnali:Goods_Received_Number
                    PartsAttached# = True
                Else
                    Cycle
                End ! If orp:GRN_Number = grnali:Goods_Received_Number

                If orp:Date_Received = ''
                    Cycle
                End ! If orp:Date_Received = ''

            End !Loop
            If PartsAttached# = False
                Cycle
            End ! If PartsAttached# = False
            tmp:GRNSuffix += 1
            If grn:Goods_Received_Number = grnali:Goods_Received_Number
                Break
            End ! If grn:Goods_Received_Number = grnali:GoodsReceivedNumber
        End ! End GRNOTESALIAS Loop

        local:InvoiceTotal = 0

        ! The cost of parts - TrkBs: 6300 (DBH: 13-09-2005)
        Access:ORDPARTS.ClearKey(orp:Order_Number_Key)
        orp:Order_Number    = grn:Order_Number
        Set(orp:Order_Number_Key,orp:Order_Number_Key)
        Loop
            If Access:ORDPARTS.NEXT()
               Break
            End !If
            If orp:Order_Number    <> grn:Order_Number      |
                Then Break.  ! End If
            If orp:GRN_Number <> grn:Goods_Received_Number
                Cycle
            End ! If orp:GRN_Number <> grn:Goods_Received_Number
            If orp:Date_Received = ''
                Cycle
            End ! If orp:Date_Received = ''
            ! Use the total of each part, rather than applying the currency at the end - TrkBs: 6264 (DBH: 07-09-2005)
            If grn:CurrencyCode <> ''
                Case grn:DivideMultiply
                Of '*'
                    local:InvoiceTotal += ((orp:Purchase_Cost / grn:DailyRate) * orp:Quantity)
                Of '/'
                    local:InvoiceTotal += ((orp:Purchase_Cost * grn:DailyRate) * orp:Quantity)
                End ! Case grnali:DivideMultiply
            Else ! If grnali:CurrencyCode <> ''
                local:InvoiceTotal += (orp:Purchase_Cost * orp:Quantity)
            End ! If grnali:CurrencyCode <> ''
        End !Loop

        local:InvoiceCount += 1

        mqoracle_por{prop:sql} = 'SELECT * FROM "mqoracle_por" WHERE "mop_sb_po_no" = ''SS' & Format(grn:Order_Number,@n08) & '/' & Format(tmp:GRNSuffix,@n02) & ''''
        Next(mqoracle_por)
        If Error()
            !Cannot find entry in stage table
            local.WriteToLine('Oracle','Parts',local:CurrentLine,local:InvoiceTotal)
            local:CurrentLine += 1
        Else
            local:ReceivedCount += 1
            local:ReceivedCost += por:mop_currency_unit_pr
        End ! If Error()
    End !Loop

    E1.WriteToCell(local:InvoiceCount,'D7')
    !E1.WriteToCell(local:TotalInvoiceCost,'E7')

    Free(POQueue)
    Access:TRDBATCH.ClearKey(trb:ReturnDateKey)
    trb:DateReturn = tmp:OracleStartDate
    Set(trb:ReturnDateKey,trb:ReturnDateKey)
    Loop
        If Access:TRDBATCH.NEXT()
           Break
        End !If
        If trb:DateReturn > tmp:OracleEndDate      |
            Then Break.  ! End If
        ! Make sure we only pick up trasmitted batches - TrkBs: 6300 (DBH: 13-09-2005)
        If trb:PurchaseOrderNumber = '' Or trb:BatchRunNotPrinted = True
            Cycle
        End ! If trb:PurchaseOrderNumber = '' Or trb:BatchRunNotPrinted = True

        Access:JOBSE.Clearkey(jobe:RefNumberKey)
        jobe:RefNumber  = trb:Ref_Number
        If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            ! Found

        Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            ! Error
            Cycle
        End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        poque:PurchaseOrderNumber = trb:PurchaseOrderNumber
        Get(POQueue,poque:PurchaseOrderNumber)
        If Error()
            poque:PurchaseOrderNumber = trb:PurchaseOrderNumber
            poque:BatchValue = jobe:ARC3rdPartyCost
            Add(POQueue)
        Else ! If Error()
            poque:BatchValue += jobe:ARC3rdPartyCost
            Put(POQueue)
        End ! If Error()
    End !Loop

    local:InvoiceCount = 0

    local:CurrentLine += 1

    E1.WriteToCell('3rd Party Receipts Exceptions','A' & local:CurrentLine)
    E1.SetCellFontStyle('Bold','A' & local:CurrentLine)
    local:CurrentLine += 1

    E1.WriteToCell('PO Number','A' & local:CurrentLine)
    E1.WriteToCell('Value','B' & local:CurrentLine)
    E1.SetCellFontStyle('Bold','A' & local:CurrentLine,'C' & local:CurrentLine)
    E1.SetCellBackgroundColor(color:silver, 'A' & local:CurrentLine, 'J' & local:CurrentLine)

    E1.WriteToCell(local:ReceivedCount,'D8')

    local:ReceivedCount = 0
    local:ReceivedCost = 0
    local:TotalInvoiceCost = 0

    local:CurrentLine += 1

    Sort(POQueue,poque:PurchaseOrderNumber)
    Loop queue# = 1 To Records(POQueue)
        Get(POQueue,queue#)
        local:InvoiceCount += 1
        local:TotalInvoiceCost += poque:BatchValue

        mqoracle_por{prop:sql} = 'SELECT * FROM "mqoracle_por" WHERE "mop_sb_po_no" = ''SS3' & Format(poque:PurchaseOrderNumber,@n07) & '/01'''
        Next(mqoracle_por)
        If Error()
            !Cannot find entry in stage table
            local.WriteToLine('Oracle','3rdParty',local:CurrentLine,poque:BatchValue)
            local:CurrentLine += 1
        Else
            local:ReceivedCount += 1
            local:ReceivedCost += por:mop_currency_unit_pr
        End ! If Error()
    End ! Loop queue# = 1 To Records(POQueue)

    E1.WriteToCell(local:InvoiceCount,'H7')

    E1.WriteToCell(local:ReceivedCount,'H8')

    E1.WriteToCell('=(D7-D8)', 'D9')
    E1.WriteToCell('=(H7-H8)', 'H9')


    E1.SetCellFontName('Tahoma', 'A1', 'J' & local:CurrentLine)
    E1.SetCellFontSize(8, 'J1')
    E1.SetCellFontSize(8, 'A2', 'J' & local:CurrentLine)
    E1.SetCellFontSize(10, 'A1')
    E1.SetCellFontStyle('Bold', 'A1', 'A12')
    E1.SetCellFontStyle('Bold', 'D6', 'I6')
    E1.SetCellFontStyle('Bold', 'A13', 'I13')
    E1.SetCellFontStyle('Bold','G7','G8')
    E1.SetCellNumberFormat(oix:NumberFormatNumber,, 2,, 'C14', 'C' & local:CurrentLine)
    E1.SetColumnWidth('A', 'J')
    E1.SetCellBackgroundColor(color:silver, 'A1', 'J1')
    E1.SetCellBackgroundColor(color:silver, 'A13', 'J13')
    E1.SelectCells('A14')
CmanagerReport        Routine
Data
local:CurrentLine            Long()
local:InvoiceCount           Long()
local:ReceivedCount          Long()
local:ReceivedCost           Real()
local:InvoiceTotal           Real()
local:TotalInvoiceCost       Real()
Code
    local.UpdateProgressWindow('')
    local.UpdateProgressWindow('==== CManager ====')
    local.UpdateProgressWindow('')
    E1.InsertWorksheet()
    E1.RenameWorkSheet('CManager')
    E1.WriteToCell('Staging Table Reconciliation Report', 'A1')
    E1.WriteToCell(tmp:VersionNumber, 'J1')
    E1.WriteToCell('CManager', 'A3')
    E1.WriteToCell('Date From', 'A4')
    E1.WriteToCell(Format(tmp:OracleStartDate, @d06), 'B4')
    E1.WriteToCell('Date To', 'A5')
    E1.WriteToCell(Format(tmp:OracleEndDate, @d06), 'B5')
    E1.WriteToCell('Quantity', 'D6')
    !E1.WriteToCell('Value', 'E6')
    E1.WriteToCell('Quantity', 'H6')
    !E1.WriteToCell('Value', 'I6')
    E1.WriteToCell('Part Receipts Transmitted', 'A7')
    E1.WriteToCell('Part Receipts Received', 'A8')
    E1.WriteToCell('3rd Party Receipts Transmitted','G7')
    E1.WriteToCell('3rd Party Receipts Received','G8')


    E1.WriteToCell('Exceptions','A9')
    E1.WriteToCell('Part Receipts Exceptions','A12')
    E1.WriteToCell('PO Number','A13')
    E1.WriteToCell('GRV Number','B13')
    E1.WriteToCell('Currency','C13')
    E1.WriteToCell('Value (ex-VAT)','D13')

    local.UpdateProgressWindow('Calculating Send / Receive Data ...')

    local:CurrentLine = 14
    local:InvoiceCount = 0

    Access:GRNOTES.ClearKey(grn:Date_Recd_Key)
    grn:Goods_Received_Date = tmp:OracleStartDate
    Set(grn:Date_Recd_Key,grn:Date_Recd_Key)
    Loop
        If Access:GRNOTES.NEXT()
           Break
        End !If
        If grn:Goods_Received_Date > tmp:OracleEndDate       |
            Then Break.  ! End If
        If grn:BatchRunNotPrinted = True
            Cycle
        End ! If grn:BatchRunNotPrinted = True

        tmp:GRNSUffix = 0
        Access:GRNOTESALIAS.Clearkey(grnali:Order_Number_Key)
        grnali:Order_Number = grn:Order_Number
        grnali:Goods_Received_Date = 0
        Set(grnali:Order_Number_Key,grnali:Order_Number_Key)
        Loop ! Begin GRNOTESALIAS Loop
            If Access:GRNOTESALIAS.Next()
                Break
            End ! If !Access
            If grnali:Order_Number <> grn:Order_Number
                Break
            End ! If

            Do GetNextRecord2
            Do CancelCheck
            If tmp:Cancel
                Break
            End ! If tmp:Cancel
            !Does this GRN Have Parts Attached?

            PartsAttached# = False
            FoundValue# = False
            Access:ORDPARTS.ClearKey(orp:Part_Number_Key)
            orp:Order_Number = grnali:Order_Number
            Set(orp:Part_Number_Key,orp:Part_Number_Key)
            Loop
                If Access:ORDPARTS.NEXT()
                   Break
                End !If
                If orp:Order_Number <> grnali:Order_Number      |
                    Then Break.  ! End If
                If orp:GRN_Number = grnali:Goods_Received_Number
                    PartsAttached# = True
                Else
                    Cycle
                End ! If orp:GRN_Number = grnali:Goods_Received_Number

                If orp:Date_Received = ''
                    Cycle
                End ! If orp:Date_Received = ''

            End !Loop
            If PartsAttached# = False
                Cycle
            End ! If PartsAttached# = False
            tmp:GRNSuffix += 1
            If grn:Goods_Received_Number = grnali:Goods_Received_Number
                Break
            End ! If grn:Goods_Received_Number = grnali:GoodsReceivedNumber
        End ! End GRNOTESALIAS Loop

        local:InvoiceTotal = 0

        ! The cost of parts - TrkBs: 6300 (DBH: 13-09-2005)
        Access:ORDPARTS.ClearKey(orp:Order_Number_Key)
        orp:Order_Number    = grn:Order_Number
        Set(orp:Order_Number_Key,orp:Order_Number_Key)
        Loop
            If Access:ORDPARTS.NEXT()
               Break
            End !If
            If orp:Order_Number    <> grn:Order_Number      |
                Then Break.  ! End If
            If orp:GRN_Number <> grn:Goods_Received_Number
                Cycle
            End ! If orp:GRN_Number <> grn:Goods_Received_Number
            If orp:Date_Received = ''
                Cycle
            End ! If orp:Date_Received = ''
            ! Use the total of each part, rather than applying the currency at the end - TrkBs: 6264 (DBH: 07-09-2005)
            If grn:CurrencyCode <> ''
                Case grn:DivideMultiply
                Of '*'
                    local:InvoiceTotal += ((orp:Purchase_Cost / grn:DailyRate) * orp:Quantity)
                Of '/'
                    local:InvoiceTotal += ((orp:Purchase_Cost * grn:DailyRate) * orp:Quantity)
                End ! Case grnali:DivideMultiply
            Else ! If grnali:CurrencyCode <> ''
                local:InvoiceTotal += (orp:Purchase_Cost * orp:Quantity)
            End ! If grnali:CurrencyCode <> ''
        End !Loop

        local:InvoiceCount += 1

        content_man{prop:sql} = 'SELECT * FROM "content_man" WHERE "cm_meta_data" LIKE ''%SS' & Format(grn:Order_Number,@n08) & '/' & Format(tmp:GRNSuffix,@n02) & '%'''
        Next(content_man)
        If Error()
            !Cannot find entry in stage table
            local.WriteToLine('Oracle','Parts',local:CurrentLine,local:InvoiceTotal)
            local:CurrentLine += 1
        Else
            local:ReceivedCount += 1
            local:ReceivedCost += por:mop_currency_unit_pr
        End ! If Error()
    End !Loop

    E1.WriteToCell(local:InvoiceCount,'D7')
    !E1.WriteToCell(local:TotalInvoiceCost,'E7')

    Free(POQueue)
    Access:TRDBATCH.ClearKey(trb:ReturnDateKey)
    trb:DateReturn = tmp:OracleStartDate
    Set(trb:ReturnDateKey,trb:ReturnDateKey)
    Loop
        If Access:TRDBATCH.NEXT()
           Break
        End !If
        If trb:DateReturn > tmp:OracleEndDate      |
            Then Break.  ! End If
        ! Make sure we only pick up trasmitted batches - TrkBs: 6300 (DBH: 13-09-2005)
        If trb:PurchaseOrderNumber = '' Or trb:BatchRunNotPrinted = True
            Cycle
        End ! If trb:PurchaseOrderNumber = '' Or trb:BatchRunNotPrinted = True

        Access:JOBSE.Clearkey(jobe:RefNumberKey)
        jobe:RefNumber  = trb:Ref_Number
        If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            ! Found

        Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            ! Error
            Cycle
        End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        poque:PurchaseOrderNumber = trb:PurchaseOrderNumber
        Get(POQueue,poque:PurchaseOrderNumber)
        If Error()
            poque:PurchaseOrderNumber = trb:PurchaseOrderNumber
            poque:BatchValue = jobe:ARC3rdPartyCost
            Add(POQueue)
        Else ! If Error()
            poque:BatchValue += jobe:ARC3rdPartyCost
            Put(POQueue)
        End ! If Error()
    End !Loop

    local:InvoiceCount = 0

    local:CurrentLine += 1

    E1.WriteToCell('3rd Party Receipts Exceptions','A' & local:CurrentLine)
    E1.SetCellFontStyle('Bold','A' & local:CurrentLine)
    local:CurrentLine += 1

    E1.WriteToCell('PO Number','A' & local:CurrentLine)
    E1.WriteToCell('Value','B' & local:CurrentLine)
    E1.SetCellFontStyle('Bold','A' & local:CurrentLine,'C' & local:CurrentLine)
    E1.SetCellBackgroundColor(color:silver, 'A' & local:CurrentLine, 'J' & local:CurrentLine)

    E1.WriteToCell(local:ReceivedCount,'D8')

    local:ReceivedCount = 0
    local:ReceivedCost = 0
    local:TotalInvoiceCost = 0

    local:CurrentLine += 1

    Sort(POQueue,poque:PurchaseOrderNumber)
    Loop queue# = 1 To Records(POQueue)
        Get(POQueue,queue#)
        local:InvoiceCount += 1
        local:TotalInvoiceCost += poque:BatchValue

        content_man{prop:sql} = 'SELECT * FROM "content_man" WHERE "cm_meta_data" LIKE ''%SS3' & Format(poque:PurchaseOrderNumber,@n07) & '/01%'''
        Next(content_man)
        If Error()
            !Cannot find entry in stage table
            local.WriteToLine('Oracle','3rdParty',local:CurrentLine,poque:BatchValue)
            local:CurrentLine += 1
        Else
            local:ReceivedCount += 1
            local:ReceivedCost += por:mop_currency_unit_pr
        End ! If Error()
    End ! Loop queue# = 1 To Records(POQueue)

    E1.WriteToCell(local:InvoiceCount,'H7')

    E1.WriteToCell(local:ReceivedCount,'H8')

    E1.WriteToCell('=(D7-D8)', 'D9')
    E1.WriteToCell('=(H7-H8)', 'H9')


    E1.SetCellFontName('Tahoma', 'A1', 'J' & local:CurrentLine)
    E1.SetCellFontSize(8, 'J1')
    E1.SetCellFontSize(8, 'A2', 'J' & local:CurrentLine)
    E1.SetCellFontSize(10, 'A1')
    E1.SetCellFontStyle('Bold', 'A1', 'A12')
    E1.SetCellFontStyle('Bold', 'D6', 'I6')
    E1.SetCellFontStyle('Bold', 'A13', 'I13')
    E1.SetCellFontStyle('Bold','G7','G8')
    E1.SetCellNumberFormat(oix:NumberFormatNumber,, 2,, 'C14', 'C' & local:CurrentLine)
    E1.SetColumnWidth('A', 'J')
    E1.SetCellBackgroundColor(color:silver, 'A1', 'J1')
    E1.SetCellBackgroundColor(color:silver, 'A13', 'J13')
    E1.SelectCells('A14')
Line500Report       Routine
Data
local:CurrentLine            Long()
local:InvoiceCount           Long()
local:RIV                    Byte(0)
local:AOW                    Byte(0)
local:RRCAOW                 Byte(0)
local:ReceivedCount          Long()
local:ReceivedCost           Real()
local:InvoiceTotal           Real()
local:TotalInvoiceCost       Real()
Code
        local.UpdateProgressWindow('')
        local.UpdateProgressWindow('==== Line 500 ====')
        local.UpdateProgressWindow('')
        E1.RenameWorkSheet('Line500')
        E1.WriteToCell('Staging Table Reconciliation Report', 'A1')
        E1.WriteToCell(tmp:VersionNumber, 'E1')
        E1.WriteToCell('Line 500', 'A3')
        E1.WriteToCell('Date From', 'A4')
        E1.WriteToCell(Format(tmp:Line500StartDate, @d06), 'B4')
        E1.WriteToCell('Date To', 'A5')
        E1.WriteToCell(Format(tmp:Line500EndDate, @d06), 'B5')
        E1.WriteToCell('Quantity', 'D6')
        E1.WriteToCell('Value', 'E6')
        E1.WriteToCell('Invoices Transmitted', 'A7')
        E1.WriteToCell('Invoices Received', 'A8')

        local:ReceivedCount = 0
        local:ReceivedCost  = 0

        E1.WriteToCell('Exceptions', 'A9')
        E1.WriteToCell('=(D7-D8)', 'D9')
        E1.WriteToCell('=(E7-E8)', 'E9')
        E1.WriteToCell('Exceptions', 'A12')

        E1.WriteToCell('Type', 'A13')
        E1.WriteToCell('Job No/Sale No', 'B13')
        E1.WriteToCell('Invoice No', 'C13')
        E1.WriteToCell('Invoice Date', 'D13')
        E1.WriteToCell('Value', 'E13')

        local:CurrentLine = 14

        local:InvoiceCount     = 0
        local:TotalInvoiceCost = 0

        local.UpdateProgressWindow('Calculating Send / Received Data ...')

        Access:INVOICE.ClearKey(inv:Date_Created_Key)
        inv:Date_Created = tmp:Line500StartDate
        Set(inv:Date_Created_Key, inv:Date_Created_Key)
        Loop
            If Access:INVOICE.NEXT()
                Break
            End ! If
            If inv:Date_Created > tmp:Line500EndDate       |
                Then Break   ! End If
            End ! If

            Do GetNextRecord2
            Do CancelCheck
            If tmp:Cancel
                Break
            End ! If tmp:Cancel

            If inv:Invoice_Type <> 'SIN' And inv:Invoice_Type <> 'RET'
                Cycle
            End ! If inv:Invoice_Type <> 'SIN' Or inv:Invoice_Type <> 'RET'

            If inv:Invoice_Type = 'SIN'
                Access:JOBS.ClearKey(job:InvoiceNumberKey)
                job:Invoice_Number = inv:Invoice_Number
                If Access:JOBS.TryFetch(job:InvoiceNumberKey) = Level:Benign
                ! Found
                Else ! If Access:JOBS.TryFetch(job:InvoiceNumberKey) = Level:Benign
                    ! Error
                    Cycle
                End ! If Access:JOBS.TryFetch(job:InvoiceNumberKey) = Level:Benign

                Access:JOBSE.Clearkey(jobe:RefNumberKey)
                jobe:RefNumber  = job:Ref_Number
                If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                ! Found

                Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                    ! Error
                    Cycle
                End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

                Access:WEBJOB.Clearkey(wob:RefNumberKey)
                wob:RefNumber   = job:Ref_Number
                If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                ! Found

                Else ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign
                    ! Error
                    Cycle
                End ! If Access:WEBJOB.Tryfetch(wob:RefNumberKey) = Level:Benign

                local:RIV    = False
                local:AOW    = False
                local:RRCAOW = False
                If jobe:WebJob
                    local:RIV = True
                    ! This is an RRC job. Check if it should of created an aow? - TrkBs: 6300 (DBH: 12-09-2005)
                    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                    tra:Account_Number  = wob:HeadAccountNumber
                    If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                        ! Found
! Changing (DBH 11/23/2005) #6744 - If not company owned, then only a genric account would create a message
!                         If tra:CompanyOwned
!                             Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
!                             sub:Account_Number  = job:Account_Number
!                             If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
!                                 ! Found
!                                 If sub:Generic_Account <> True
!                                     local:RIV    = False
!                                     local:RRCAOW = True
!                                 End ! If sub:Generic_Account = True
!                             Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
!                             ! Error
!                             End ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
!                         End ! If tra:CompanyOwned
! to (DBH 11/23/2005) #6744
                        Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                        sub:Account_Number  = job:Account_Number
                        If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                            ! Found
                            If sub:Generic_Account <> True
                                If tra:CompanyOwned
                                    local:RIV    = False
                                    local:RRCAOW = True
                                Else ! If tra:CompanyOwned
                                    ! Not generic, not company owned. No original message was created - TrkBs: 6774 (DBH: 23-11-2005)
                                    Cycle
                                End ! If tra:CompanyOwned
                            End ! If sub:Generic_Account = True
                        Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                        ! Error
                        End ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
! End (DBH 11/23/2005) #6744
                    Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                        ! Error
                        Cycle
                    End ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                End ! If jobe:WebJob

                If jobe:HubRepair = True
                    local:AOW = True
                End ! If jobe:HubRepair = True

                If local:AOW = True

                    If ~Inrange(inv:ARCInvoiceDate, tmp:Line500StartDate, tmp:Line500EndDate)
                        Cycle
                    End ! If ~Inrange(inv:ARCInvoiceDate,tmp:Line500StartDate,tmp:Line500EndDate)


                    local:InvoiceCount += 1
                    local:InvoiceTotal  = Round(job:Invoice_Courier_Cost + | ! Invoice Sub Total
                                               job:Invoice_Labour_Cost +   |
                                               job:Invoice_Parts_Cost  +   |
                                               (job:Invoice_Courier_Cost * (inv:Vat_Rate_Labour / 100) + | ! Invoice VAT Rate
                                                job:Invoice_Parts_Cost * (inv:Vat_Rate_Parts / 100) +    |
                                                job:Invoice_Labour_Cost * (inv:Vat_Rate_Labour / 100)), .01)
                    local:TotalInvoiceCost += local:InvoiceTotal
                    sbmq_aow{prop:SQL} = 'SELECT * FROM "sbmq_aow" WHERE "ma_sb_inv_no" = ''Z' & Format(Clip(inv:Invoice_Number) & Clip(local.ReturnBranchID(GETINI('BOOKING', 'HeadAccount',, Clip(Path()) & '\SB2KDEF.INI'))), @n09) & ''''
                    Next(sbmq_aow)
                    If Error()
                        !!CANNOT FIND ENTRY IN STAGING TABLE
                        local.WriteToLine('Line500', 'Job', local:CurrentLine, local:InvoiceTotal)
                        local:CurrentLine += 1
                    Else ! If Error()
                        local:ReceivedCount += 1
                        local:ReceivedCost += aow:ma_cost
                    End ! If Error()
                End ! If local:AOW = True

                If local:RIV = True
                    If ~Inrange(inv:RRCInvoiceDate, tmp:Line500StartDate, tmp:Line500EndDate)
                        Cycle
                    End ! If ~Inrange(inv:RRCInvoiceDate,tmp:Line500StartDate,tmp:Line500EndDate)

                    local:InvoiceCount += 1
                    local:InvoiceTotal  = Round(job:Invoice_Courier_Cost + | ! RRC Invoice Sub Total
                                               jobe:InvRRCCLabourCost +    |
                                               jobe:InvRRCCPartsCost +     |
                                               (job:Invoice_Courier_Cost * (inv:Vat_Rate_Labour / 100) + | ! RRC Invoice VAT
                                                jobe:InvRRCCLabourCost * (inv:Vat_Rate_Parts / 100) +    |
                                                jobe:InvRRCCPartsCost * (inv:Vat_Rate_Labour / 100)), .01)
                    local:TotalInvoiceCost += local:InvoiceTotal
                    sbmq_riv{prop:SQL} = 'SELECT * FROM "sbmq_riv" WHERE "mr_sb_inv_no" = ''Z' & Format(Clip(inv:Invoice_Number) & Clip(local.ReturnBranchID(wob:HeadACcountNumber)), @n09) & ''''
                    Next(sbmq_riv)
                    If Error()
                        !!Cannot Find Entry In Staging Table


                        local.WriteToLine('Line500', 'Job', local:CurrentLine, local:InvoiceTotal)
                        local:CurrentLine += 1
                    Else ! If Error()
                        local:ReceivedCount += 1
                        local:ReceivedCost += riv:mr_cost
                    End ! If Error()
                End ! If local:RIV = True

                If local:RRCAOW = True
                    If ~Inrange(inv:RRCInvoiceDate, tmp:Line500StartDate, tmp:Line500EndDate)
                        Cycle
                    End ! If ~Inrange(inv:RRCInvoiceDate,tmp:Line500StartDate,tmp:Line500EndDate)

                    local:InvoiceCount += 1
                    local:InvoiceTotal  = Round(job:Invoice_Courier_Cost + | ! RRC Invoice Sub Total
                                               jobe:InvRRCCLabourCost +    |
                                               jobe:InvRRCCPartsCost +     |
                                               (job:Invoice_Courier_Cost * (inv:Vat_Rate_Labour / 100) + | ! RRC Invoice VAT
                                                jobe:InvRRCCLabourCost * (inv:Vat_Rate_Parts / 100) +    |
                                                jobe:InvRRCCPartsCost * (inv:Vat_Rate_Labour / 100)), .01)
                    local:TotalInvoiceCost += local:InvoiceTotal
                    sbmq_aow{prop:SQL} = 'SELECT * FROM "sbmq_aow" WHERE "ma_sb_inv_no" = ''Z' & Format(Clip(inv:Invoice_Number) & Clip(local.ReturnBranchID(wob:HeadAccountNumber)), @n09) & ''''
                    Next(sbmq_aow)
                    If Error()
                        !!CANNOT FIND ENTRY IN STAGING TABLE

                        local.WriteToLine('Line500', 'Job', local:CurrentLine, local:InvoiceTotal)
                        local:CurrentLine += 1
                    Else ! If Error()
                        local:ReceivedCount += 1
                        local:ReceivedCost += aow:ma_cost
                    End ! If Error()
                End ! If local:RRCAOW = True
            End ! If inv:Invoice_Type = 'SIN'

            If inv:Invoice_Type = 'RET'

                Access:RETSALES.Clearkey(ret:Invoice_Number_Key)
                ret:Invoice_Number = inv:Invoice_Number
                If Access:RETSALES.TryFetch(ret:Invoice_Number_Key) = Level:Benign

                Else ! If Access:RETSALES.TryFetch(ret:Invoice_Number_Key) = Level:Benign
                    Cycle
                End ! If Access:RETSALES.TryFetch(ret:Invoice_Number_Key) = Level:Benign

                local:InvoiceCount     += 1
                local:TotalInvoiceCost += Round(ret:Invoice_Sub_Total + (ret:Invoice_Sub_Total * inv:Vat_Rate_Retail / 100), .01)
                sbmq_stf{prop:SQL} = 'SELECT * FROM "sbmq_stf" WHERE "ms_sb_inv_no" = ''Z' & Format(Clip(inv:Invoice_Number), @n09) & ''''
                Next(sbmq_stf)
                If Error()
                    ! Cannot Find Entry In Staging Table
                    local.WriteToLine('Line500', 'Retail', local:CurrentLine, Round(ret:Invoice_Sub_Total + (ret:Invoice_Sub_Total * inv:Vat_Rate_Retail / 100), .01))
                    local:CurrentLine += 1
                Else ! If Error()
                    local:ReceivedCount += 1
                    local:ReceivedCost += stf:ms_cost
                End ! If Error()
            End ! If inv:Invoice_Type = 'RET'
        End ! Loop

        E1.WriteToCell(local:InvoiceCount, 'D7')
        E1.WriteToCell(local:TotalInvoiceCost, 'E7')

        E1.WriteToCell(local:ReceivedCount, 'D8')
        E1.WriteToCell(local:ReceivedCost, 'E8')

        E1.SetCellFontName('Tahoma', 'A1', 'E' & local:CurrentLine)
        E1.SetCellFontSize(8, 'E1')
        E1.SetCellFontSize(8, 'A2', 'E' & local:CurrentLine)
        E1.SetCellFontSize(10, 'A1')
        E1.SetCellFontStyle('Bold', 'A1', 'A12')
        E1.SetCellFontStyle('Bold', 'D6', 'E6')
        E1.SetCellFontStyle('Bold', 'A13', 'E13')
        E1.SetCellNumberFormat(oix:NumberFormatNumber,, 2,, 'E1', 'E' & local:CurrentLine)
        E1.SetColumnWidth('A', 'E')
        E1.SetCellBackgroundColor(color:silver, 'A1', 'E1')
        E1.SetCellBackgroundColor(color:silver, 'A13', 'E13')
        E1.SelectCells('A14')
Reporting                    Routine
   Data
local:LocalPath              String(255)
local:ReportStartDate        Date()
local:ReportStartTime        Time()
excel:ProgramName               Cstring(255)
excel:Filename                  Cstring(255)
local:Desktop                   Cstring(255)
    Code

    excel:ProgramName = 'Staging Table Reconciliation Report'
    ! Create Folder In My Documents
    SHGetSpecialFolderPath( GetDesktopWindow(), local:Desktop, 5, FALSE )
    local:Desktop = Clip(local:Desktop) & '\ServiceBase Export'

    !Does the Export Folder already Exists?
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    local:Desktop = Clip(local:Desktop) & '\' & Clip(excel:ProgramName)
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))


    excel:FileName = Clip(local:Desktop) & '\' & Clip(excel:ProgramName) & ' ' & Format(Today(),@d12)

    If Exists(excel:FileName & '.xls')
        Remove(excel:FileName & '.xls')
        If Error()
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('Cannot get access to the selected document:'&|
                '|' & Clip(excel:FileName) & ''&|
                '|'&|
                '|Ensure the file is not in use and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If Error()
    End !If Exists(excel:FileName)

     ! _____________________________________________________________________

    ! Open Program Window (DBH: 10-03-2004)
    recordspercycle      = 25
    recordsprocessed     = 0
    percentprogress      = 0
    progress:thermometer = 0
    recordstoprocess     = 50
    Open(ProgressWindow)

    ?progress:userstring{prop:text} = 'Running...'
    ?progress:pcttext{prop:text}    = '0% Completed'

    local:ReportStartDate = Today()
    local:ReportStartTime = Clock()

    local.UpdateProgressWindow('Report Started: ' & Format(local:ReportStartDate, @d6b) & ' ' & Format(local:ReportStartTime, @t1b))
    local.UpdateProgressWindow('')

    If E1.Init(0, 0) = False
        Case Missive('An error has occured creating your excel document. ' & |
                     '|Please try again.', 'ServiceBase 3g',                 |
                     'mstop.jpg', '/OK')
        Of 1 ! OK Button
        End ! Case Missive
        Exit
    End ! If E1.Init(0,0) = False

    E1.NewWorkBook()

    If tmp:Line500Report
        Do Line500Report
    End ! If tmp:Line500Report

    ! _____________________________________________________________________

    If tmp:OracleReport
        Do OracleReport
        DO CManagerReport
    End ! If tmp:OracleReport

    If tmp:CIDReport
        Do CIDReport
    End ! If tmp:CIDReport

    E1.SaveAs(excel:Filename)
    E1.CloseWorkbook(2)

    If tmp:Cancel = 0 Or tmp:Cancel = 2
        local.UpdateProgressWindow('================')
        local.UpdateProgressWindow('Report Finished: ' & Format(Today(), @d6b) & ' ' & Format(Clock(), @t1b))
    Else! If tmp:Cancel = False
        staque:StatusMessage = '=========='
        Add(StatusQueue)
        Select(?List1, Records(StatusQueue))

        staque:StatusMessage = 'Report CANCELLED: ' & Format(Today(), @d6b) & ' ' & Format(Clock(), @t1b)
        Add(StatusQueue)
        Select(?List1, Records(StatusQueue))

    End ! If tmp:Cancel = False

    BHReturnDaysHoursMins(BHTimeDifference24Hr(local:ReportStartDate, Today(), local:ReportStartTime, Clock()), Days#, Hours#, Mins#)

    local.UpdateProgressWindow('Time To Finish: ' & Days# & ' Dys, ' & Format(Hours#, @n02) & ':' & Format(Mins#, @n02))
    Add(StatusQueue)
    Select(?List1, Records(StatusQueue))

    Do EndPrintRun
    ?progress:userstring{prop:text} = 'Finished...'

    Display()
    ?ProgressCancel{prop:Hide} = 1
    ?Finish{prop:Hide}         = 0
    ?Button:OpenReportFolder{Prop:Hide} = 0
    Accept
        Case Field()
            Of ?Finish
                Case Event()
                    Of Event:Accepted
                        Break

                End !Case Event()
            Of ?Button:OpenReportFolder
                Case Event()
                Of Event:Accepted
                    RUN('EXPLORER.EXE ' & Clip(local:Desktop))
                End ! Case Event()
        End !Case Field()
    End !Accept
    Close(ProgressWindow)
    Post(Event:CloseWindow)
getnextrecord2      routine !Progress Window Routines (DBH: 22-03-2004)
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress >= 100
        recordsprocessed        = 0
        percentprogress         = 0
        progress:thermometer    = 0
        recordsthiscycle        = 0
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    !0{prop:Text} = ?progress:pcttext{prop:text}
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                Yield()
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case Missive('Do you want to finish off building the excel document with the data you have compiled so far, or just quit now?','ServiceBase 3g',|
                       'mquest.jpg','\Cancel|Quit|/Finish') 
            Of 3 ! Finish Button
                tmp:Cancel = 2
            Of 2 ! Quit Button
                tmp:Cancel = 1
            Of 1 ! Cancel Button
        End ! Case Missive
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    display()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020638'&'0'
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('StagingTableReconciliationReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Ok,RequestCancelled)
  SELF.AddItem(?Cancel,RequestCancelled)
  tmp:RepositoryDir = GETINI('XML', 'RepositoryFolder',, Clip(Path()) & '\SB2KDEF.INI')
  tmp:DatabaseServer = GETINI('REPOSITORY','DATABASE_SERVER',,Clip(tmp:RepositoryDir) & '\REPOSITORY.INI')
  tmp:DatabaseName = GETINI('REPOSITORY','DATABASE_NAME',,Clip(tmp:RepositoryDir) & '\REPOSITORY.INI')
  
  DataBaseName = tmp:DatabaseServer & '|' & tmp:DatabaseName
  
  tmp:Line500StartDate = Deformat('01/' & Month(Today()) & '/' & Year(Today()),@d6)
  tmp:Line500EndDate  = Today()
  tmp:OracleStartDate = tmp:Line500StartDate
  tmp:OracleEndDate = tmp:Line500EndDate
  tmp:CIDStartDate = tmp:Line500StartDate
  tmp:CIDEndDate = tmp:Line500EndDate
  Share(mqoracle_por)
  If Error()
      Case Missive('Cannot access file "mqoracle_por"'&|
        '|' & Clip(fileerror()) & '.','ServiceBase 3g',|
                     'mstop.jpg','/OK') 
          Of 1 ! OK Button
      End ! Case Missive
  End ! If Error()
  
  Share(sbmq_stf)
  If Error()
      Case Missive('Cannot access file "sbmq_stf"'&|
        '|' & Clip(fileerror()) & '.','ServiceBase 3g',|
                     'mstop.jpg','/OK') 
          Of 1 ! OK Button
      End ! Case Missive
  End ! If Error()
  
  Share(sbmq_riv)
  If Error()
      Case Missive('Cannot access file "sbmq_riv"'&|
        '|' & Clip(fileerror()) & '.','ServiceBase 3g',|
                     'mstop.jpg','/OK') 
          Of 1 ! OK Button
      End ! Case Missive
  End ! If Error()
  
  Share(sbmq_aow)
  If Error()
      Case Missive('Cannot access file "sbmq_aow"'&|
        '|' & Clip(fileerror()) & '.','ServiceBase 3g',|
                     'mstop.jpg','/OK') 
          Of 1 ! OK Button
      End ! Case Missive
  End ! If Error()
  
  Share(sbmq_cid)
  If Error()
      Case Missive('Cannot access file "sbmq_cid"'&|
        '|' & Clip(fileerror()) & '.','ServiceBase 3g',|
                     'mstop.jpg','/OK') 
          Of 1 ! OK Button
      End ! Case Missive
  End ! If Error()
  
  Share(content_man)
  If Error()
      Case Missive('Cannot access file "content_man"'&|
        '|' & Clip(fileerror()) & '.','ServiceBase 3g',|
                     'mstop.jpg','/OK') 
          Of 1 ! OK Button
      End ! Case Missive
  End ! If Error()
  Relate:AUDIT.Open
  Relate:GRNOTES.Open
  Relate:GRNOTESAlias.Open
  Relate:RETSALES.Open
  Relate:TRDPONO.Open
  Relate:WEBJOB.Open
  Access:INVOICE.UseFile
  Access:JOBS.UseFile
  Access:JOBSE.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:ORDPARTS.UseFile
  Access:TRDBATCH.UseFile
  SELF.FilesOpened = True
  OPEN(QuickWindow)
  SELF.Opened=True
  Bryan.CompFieldColour()
  ?tmp:Line500StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:Line500EndDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:OracleStartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:OracleEndDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:CIDStartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:CIDEndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  ! ============= SET VERSION NUMBER ================
  Include('..\ReportVersion.inc')
  tmp:VersionNumber = 'Version Number: ' & Clip(tmp:VersionNumber) & '5004'
  ?ReportVersion{prop:Text} = tmp:VersionNumber
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  IF ?tmp:Line500Report{Prop:Checked} = True
    UNHIDE(?PopCalendar)
    UNHIDE(?PopCalendar:2)
    ENABLE(?tmp:Line500StartDate)
    ENABLE(?tmp:Line500EndDate)
    ENABLE(?tmp:Line500StartDate:Prompt)
    ENABLE(?tmp:Line500EndDate:Prompt)
  END
  IF ?tmp:Line500Report{Prop:Checked} = False
    HIDE(?PopCalendar)
    HIDE(?PopCalendar:2)
    DISABLE(?tmp:Line500StartDate)
    DISABLE(?tmp:Line500EndDate)
    DISABLE(?tmp:Line500StartDate:Prompt)
    DISABLE(?tmp:Line500EndDate:Prompt)
  END
  IF ?tmp:OracleReport{Prop:Checked} = True
    UNHIDE(?PopCalendar:3)
    UNHIDE(?PopCalendar:4)
    ENABLE(?tmp:OracleStartDate)
    ENABLE(?tmp:OracleEndDate)
    ENABLE(?tmp:OracleStartDate:Prompt)
    ENABLE(?tmp:OracleEndDate:Prompt)
  END
  IF ?tmp:OracleReport{Prop:Checked} = False
    HIDE(?PopCalendar:3)
    HIDE(?PopCalendar:4)
    DISABLE(?tmp:OracleStartDate)
    DISABLE(?tmp:OracleEndDate)
    DISABLE(?tmp:OracleStartDate:Prompt)
    DISABLE(?tmp:OracleEndDate:Prompt)
  END
  IF ?tmp:CIDReport{Prop:Checked} = True
    UNHIDE(?PopCalendar:6)
    UNHIDE(?PopCalendar:5)
    ENABLE(?tmp:CIDStartDate)
    ENABLE(?tmp:CIDEndDate)
    ENABLE(?tmp:CIDStartDate:Prompt)
    ENABLE(?tmp:CIDEndDate:Prompt)
  END
  IF ?tmp:CIDReport{Prop:Checked} = False
    HIDE(?PopCalendar:5)
    HIDE(?PopCalendar:6)
    DISABLE(?tmp:CIDStartDate)
    DISABLE(?tmp:CIDEndDate)
    DISABLE(?tmp:CIDStartDate:Prompt)
    DISABLE(?tmp:CIDEndDate:Prompt)
  END
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  Close(mqoracle_por)
  Close(sbmq_stf)
  Close(sbmq_riv)
  Close(sbmq_aow)
  Close(sbmq_cid)
  Close(content_man)
    Relate:AUDIT.Close
    Relate:GRNOTES.Close
    Relate:GRNOTESAlias.Close
    Relate:RETSALES.Close
    Relate:TRDPONO.Close
    Relate:WEBJOB.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020638'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020638'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020638'&'0')
      ***
    OF ?tmp:Line500Report
      IF ?tmp:Line500Report{Prop:Checked} = True
        UNHIDE(?PopCalendar)
        UNHIDE(?PopCalendar:2)
        ENABLE(?tmp:Line500StartDate)
        ENABLE(?tmp:Line500EndDate)
        ENABLE(?tmp:Line500StartDate:Prompt)
        ENABLE(?tmp:Line500EndDate:Prompt)
      END
      IF ?tmp:Line500Report{Prop:Checked} = False
        HIDE(?PopCalendar)
        HIDE(?PopCalendar:2)
        DISABLE(?tmp:Line500StartDate)
        DISABLE(?tmp:Line500EndDate)
        DISABLE(?tmp:Line500StartDate:Prompt)
        DISABLE(?tmp:Line500EndDate:Prompt)
      END
      ThisWindow.Reset
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:Line500StartDate = TINCALENDARStyle1(tmp:Line500StartDate)
          Display(?tmp:Line500StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:Line500EndDate = TINCALENDARStyle1(tmp:Line500EndDate)
          Display(?tmp:Line500EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tmp:OracleReport
      IF ?tmp:OracleReport{Prop:Checked} = True
        UNHIDE(?PopCalendar:3)
        UNHIDE(?PopCalendar:4)
        ENABLE(?tmp:OracleStartDate)
        ENABLE(?tmp:OracleEndDate)
        ENABLE(?tmp:OracleStartDate:Prompt)
        ENABLE(?tmp:OracleEndDate:Prompt)
      END
      IF ?tmp:OracleReport{Prop:Checked} = False
        HIDE(?PopCalendar:3)
        HIDE(?PopCalendar:4)
        DISABLE(?tmp:OracleStartDate)
        DISABLE(?tmp:OracleEndDate)
        DISABLE(?tmp:OracleStartDate:Prompt)
        DISABLE(?tmp:OracleEndDate:Prompt)
      END
      ThisWindow.Reset
    OF ?PopCalendar:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:OracleStartDate = TINCALENDARStyle1(tmp:OracleStartDate)
          Display(?tmp:OracleStartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:OracleEndDate = TINCALENDARStyle1(tmp:OracleEndDate)
          Display(?tmp:OracleEndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tmp:CIDReport
      IF ?tmp:CIDReport{Prop:Checked} = True
        UNHIDE(?PopCalendar:6)
        UNHIDE(?PopCalendar:5)
        ENABLE(?tmp:CIDStartDate)
        ENABLE(?tmp:CIDEndDate)
        ENABLE(?tmp:CIDStartDate:Prompt)
        ENABLE(?tmp:CIDEndDate:Prompt)
      END
      IF ?tmp:CIDReport{Prop:Checked} = False
        HIDE(?PopCalendar:5)
        HIDE(?PopCalendar:6)
        DISABLE(?tmp:CIDStartDate)
        DISABLE(?tmp:CIDEndDate)
        DISABLE(?tmp:CIDStartDate:Prompt)
        DISABLE(?tmp:CIDEndDate:Prompt)
      END
      ThisWindow.Reset
    OF ?PopCalendar:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:CIDStartDate = TINCALENDARStyle1(tmp:CIDStartDate)
          Display(?tmp:CIDStartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:6
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:CIDEndDate = TINCALENDARStyle1(tmp:CIDEndDate)
          Display(?tmp:CIDEndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Ok
      ThisWindow.Update
      Do Reporting
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeEvent()
    E1.TakeEvent ('', '')
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?tmp:Line500StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?tmp:Line500EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  OF ?tmp:OracleStartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:3)
      CYCLE
    END
  OF ?tmp:OracleEndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:4)
      CYCLE
    END
  OF ?tmp:CIDStartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:5)
      CYCLE
    END
  OF ?tmp:CIDEndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:6)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

local.WriteToLine       Procedure(String f:Type,String f:Type2,Long f:LineNumber,Real f:Total)
Code
    Case f:Type
    Of 'Line500'
        Case f:Type2
        Of 'Job'
            E1.WriteToCell('Job','A' & f:LineNumber)
            E1.WriteToCell(job:Ref_Number,'B' & f:LineNumber)
            E1.WriteToCell(inv:Invoice_Number,'C' & f:LineNumber)
            E1.WriteToCell(Format(inv:Date_Created,@d06),'D' & f:LineNumber)
            E1.WriteToCell(Format(f:Total,@n_14.2),'E' & f:LineNumber)
        Of 'Retail'
            E1.WriteToCell('Retail Sale','A' & f:LineNumber)
            E1.WriteToCell(ret:Ref_Number,'B' & f:LineNumber)
            E1.WriteToCell(inv:Invoice_Number,'C' & f:LineNumber)
            E1.WriteToCell(Format(inv:Date_Created,@d06),'D' & f:LineNumber)
            E1.WriteToCell(Format(f:Total,@n_14.2),'E' & f:LineNumber)
        End !Case f:Type2
    Of 'Oracle'
        Case f:Type2
        Of 'Parts'
            E1.WriteToCell('SS' & Format(grn:Order_Number,@n08) & '/' & Format(tmp:GRNSuffix,@n02),'A' & f:LineNumber)
            E1.WriteToCell(grn:Goods_Received_Number,'B' & f:LineNumber)
            If grn:CurrencyCode = ''
                E1.WriteToCell('ZAR','C' & f:LineNumber)
            Else ! If grn:CurrencyCode = ''
                E1.WriteToCell(grn:CurrencyCode,'C' & f:LineNumber)
            End ! If grn:CurrencyCode = ''
            E1.WriteToCell(Format(f:Total,@n_14.2),'D' & f:LineNumber)
        Of '3rdParty'
            E1.WriteToCell('SS3' & Format(poque:PurchaseOrderNumber,@n07) & '/01','A' & f:LineNumber)
            E1.WriteToCell(Format(f:Total,@n_14.2),'B' & f:LineNumber)
        End ! Case f:Type2
    Of 'CID'
        E1.WriteToCell(job:Ref_Number,'A' & f:LineNumber)
    End !Case f:Type
local.ReturnSQLDate         Procedure(Date f:Date)
Code
    Return Year(f:Date) & '-' & Format(Month(f:date),@n02) & '-' & Format(Day(f:date),@n02)
local.ReturnBranchID            Procedure(String f:Account)
Code
    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
    tra:Account_Number = f:Account
    If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        Return tra:BranchIdentification
    Else !If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
        Return ''
    End !If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
Local.UpdateProgressWindow      Procedure(String    func:Text)
Code
    staque:StatusMessage = Clip(func:Text)
    Add(StatusQueue)
    Select(?List1,Records(StatusQueue))
    Display()
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
!---------------------------------------------------------------------------------
E1.Init   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0)
ReturnValue   byte
  CODE
  ReturnValue = PARENT.Init (pStartVisible,pEnableEvents)
  self.TakeSnapShotOfWindowPos()
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.Kill   PROCEDURE (byte pUnloadCOM=1)
ReturnValue   byte
  CODE
  self.RestoreSnapShotOfWindowPos()
  ReturnValue = PARENT.Kill (pUnloadCOM)
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
E1.TakeEvent   PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0)
  CODE
  PARENT.TakeEvent (pEventString1,pEventString2,pEventNumber,pEventType,pEventStatus)
  if pEventType = 0  ! Generated by CapeSoft Office Inside
    case event()
      of event:accepted
        case field()
      end
    end
  end

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

