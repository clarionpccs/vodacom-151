

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER459.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
ClearGenericTagFile  PROCEDURE  (NetWebServerWorker p_web,<STRING pTagType>) ! Declare Procedure

  CODE
        Relate:SBO_GenericTagFile.Open()
        Access:SBO_GenericTagFile.ClearKey(tagf:KeyTagged)
        tagf:SessionID  = p_web.SessionID
        IF (pTagType <> '')
            tagf:TagType    = pTagType
        END ! IF
        SET(tagf:KeyTagged,tagf:KeyTagged)
        LOOP UNTIL Access:SBO_GenericTagFile.Next() <> Level:Benign
            IF (tagf:SessionID <> p_web.SessionID)
                BREAK
            END
            IF (pTagType <> '' AND tagf:TagType <> pTagType)
                BREAK
            END ! IF
            tagf:Tagged = 0  ! Thinking that it's quick to put that to delete
            Access:SBO_GenericTagFile.TryUpdate()
        END ! IF      
        Relate:SBO_GenericTagFile.Close()
