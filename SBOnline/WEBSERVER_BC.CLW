  MEMBER('Webserver.clw')

  PRAGMA('define(init_priority=>3)')

  MAP
    MODULE('Webserver_BC0.CLW')
Webserver_BC0:DctInit             PROCEDURE
Webserver_BC0:DctKill             PROCEDURE
Webserver_BC0:FilesInit           PROCEDURE
    END
    MODULE('Webserver_BC1.CLW')
Webserver_BC1:DctInit             PROCEDURE
Webserver_BC1:DctKill             PROCEDURE
Webserver_BC1:FilesInit           PROCEDURE
    END
    MODULE('Webserver_BC2.CLW')
Webserver_BC2:DctInit             PROCEDURE
Webserver_BC2:DctKill             PROCEDURE
Webserver_BC2:FilesInit           PROCEDURE
    END
    MODULE('Webserver_BC3.CLW')
Webserver_BC3:DctInit             PROCEDURE
Webserver_BC3:DctKill             PROCEDURE
Webserver_BC3:FilesInit           PROCEDURE
    END
    MODULE('Webserver_BC4.CLW')
Webserver_BC4:DctInit             PROCEDURE
Webserver_BC4:DctKill             PROCEDURE
Webserver_BC4:FilesInit           PROCEDURE
    END
    MODULE('Webserver_BC5.CLW')
Webserver_BC5:DctInit             PROCEDURE
Webserver_BC5:DctKill             PROCEDURE
Webserver_BC5:FilesInit           PROCEDURE
    END
    MODULE('Webserver_BC6.CLW')
Webserver_BC6:DctInit             PROCEDURE
Webserver_BC6:DctKill             PROCEDURE
Webserver_BC6:FilesInit           PROCEDURE
    END
    MODULE('Webserver_BC7.CLW')
Webserver_BC7:DctInit             PROCEDURE
Webserver_BC7:DctKill             PROCEDURE
Webserver_BC7:FilesInit           PROCEDURE
    END
    MODULE('Webserver_BC8.CLW')
Webserver_BC8:DctInit             PROCEDURE
Webserver_BC8:DctKill             PROCEDURE
Webserver_BC8:FilesInit           PROCEDURE
    END
    MODULE('Webserver_BC9.CLW')
Webserver_BC9:DctInit             PROCEDURE
Webserver_BC9:DctKill             PROCEDURE
Webserver_BC9:FilesInit           PROCEDURE
    END
    MODULE('Webserver_BCA.CLW')
Webserver_BCA:DctInit             PROCEDURE
Webserver_BCA:DctKill             PROCEDURE
Webserver_BCA:FilesInit           PROCEDURE
    END
    MODULE('Webserver_BCB.CLW')
Webserver_BCB:DctInit             PROCEDURE
Webserver_BCB:DctKill             PROCEDURE
Webserver_BCB:FilesInit           PROCEDURE
    END
  END

DctInit PROCEDURE
  CODE
  Webserver_BC0:DctInit
  Webserver_BC1:DctInit
  Webserver_BC2:DctInit
  Webserver_BC3:DctInit
  Webserver_BC4:DctInit
  Webserver_BC5:DctInit
  Webserver_BC6:DctInit
  Webserver_BC7:DctInit
  Webserver_BC8:DctInit
  Webserver_BC9:DctInit
  Webserver_BCA:DctInit
  Webserver_BCB:DctInit
  Webserver_BC0:FilesInit
  Webserver_BC1:FilesInit
  Webserver_BC2:FilesInit
  Webserver_BC3:FilesInit
  Webserver_BC4:FilesInit
  Webserver_BC5:FilesInit
  Webserver_BC6:FilesInit
  Webserver_BC7:FilesInit
  Webserver_BC8:FilesInit
  Webserver_BC9:FilesInit
  Webserver_BCA:FilesInit
  Webserver_BCB:FilesInit


DctKill PROCEDURE
  CODE
  Webserver_BC0:DctKill
  Webserver_BC1:DctKill
  Webserver_BC2:DctKill
  Webserver_BC3:DctKill
  Webserver_BC4:DctKill
  Webserver_BC5:DctKill
  Webserver_BC6:DctKill
  Webserver_BC7:DctKill
  Webserver_BC8:DctKill
  Webserver_BC9:DctKill
  Webserver_BCA:DctKill
  Webserver_BCB:DctKill

