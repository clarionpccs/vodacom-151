

   MEMBER('Webserver.clw')                                 ! This is a MEMBER module

                     MAP
                       INCLUDE('WEBSERVER071.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
ClearTagFile         PROCEDURE  (NetWebServerWorker p_web) ! Declare Procedure
FilesOpened     BYTE(0)

  CODE
        do openFiles
        STREAM(TagFile)

    Access:TAGFILE.Clearkey(tag:keyTagged)
    tag:sessionID    = p_web.sessionID
    set(tag:keyTagged,tag:keyTagged)
        loop
            if (Access:TAGFILE.Next())
                Break
            end ! if (Access:TAGFILE.Next())
            if (tag:sessionID    <> p_web.sessionID)
                Break
            end ! if (tag:sessionID    <> p_web.sessionID)
            DELETE(TAGFILE)
        end ! loop
        FLUSH(TagFile)

    do closeFiles
!--------------------------------------
OpenFiles  ROUTINE
  Access:TagFile.Open                                      ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TagFile.UseFile                                   ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:TagFile.Close
     FilesOpened = False
  END
