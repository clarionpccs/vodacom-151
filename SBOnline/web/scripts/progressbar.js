//function drawProgressBar(returnURL)
//{  
//    var x = window.innerWidth;
//    var y = window.innerHeight;
//    var paramLeft = Math.round((((x / 2) - (250 / 2)) / x) * 100);
//    var paramTop = Math.round((((y / 2) - (350 / 2)) / y) * 100) - 5;    
//    paramLeft = 5;
//    paramTop = 5;
//    
//    document.write('<div id="progressframe" class="nt-process">');
//    document.write('<p class="nt-process-text">Please wait while ServiceBase searches for matching records<br/><br/>This may take a few seconds...</br><br/></p>')
//    document.write('<p id="progtitle" class="nt-process-text"><br/><br/>');
//    document.write('<div id="progbarwrapper" class="smallish-progress-wrapper">');  
//    document.write('<div id="progbar" class="smallish-progress-bar"></div>');  
//    document.write('<div id="progtext" class="smallish-progress-text"></div>');  
//    document.write('    </div>'); 
//    document.write('<br/><a href="<!-- s:returnURL-->">Cancel Process</a>');
//    document.write('</p>');
//    document.write('</div>');
//}

function updateProgressBar(text,percent)
{             
    if (percent > 100) {
        percent = 100;
    }
    //var width = document.getElementById("progressframe").style.width;
    var width = 300;
    var pixels = width * (percent / 100);  
    document.getElementById("progbar").style.width = pixels + "px";
    document.getElementById("progtext").innerHTML = percent + "%";
    document.getElementById("progtitle").innerHTML = text;
}
