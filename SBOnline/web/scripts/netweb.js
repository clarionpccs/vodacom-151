;
ajaxEngine.registerRequest( 'SetSessionValue', 'SetSessionValue' );

var hadfocus='';
var setfocus='';
var popuperror=0;
function afterSv(){
  GreenAll();
  sortables_init();
  var d = document.getElementById('alert_div');
  if (d != null){
    var i = d.innerHTML;
    var r = /(<([^>]+)>)/gi;
    i = i.replace(r, "");
    if ((i != '') && (i != '\n') && (i != '\n\n') && (i != '<br />')){
      var thissound=document.getElementById('soundfail');
      if (thissound != null){
        thissound.Play();
      }
      if (popuperror==1){
        alert(i);
      }
      if (hadfocus != ''){
        var e = document.getElementById(hadfocus);
        if (e != null){
          e.focus();
        }
        hadfocus = '';
      }
    } else {
      var thissound=document.getElementById('soundok');
      if (thissound != null){
        thissound.Play();
      }
      if (setfocus != ''){
        var e = document.getElementById(setfocus);
        if (e != null){
          e.focus();
        }
        setfocus = '';
      } else {
        var e = document.getElementById(hadfocus);
        for(var d = 0; d < document.forms.length; d++){
          for(var f = 0; f < document.forms[d].elements.length; f++){
            if(document.forms[d].elements[f].id == e.id){
              nextFocus(document.forms[d],e.name);
              e = null;
            }
          }
        }
      }
    }
  }
}

var tables = [];
function GreenAll(){
  for(var e = 0; e < tables.length; e++){
     tables[e].table=document.getElementById(tables[e].tid); // necessary after ajax call
     if (tables[e].table != null){
       tables[e].parseCell();
       tables[e].greenBar();
     }
  }
  hidebusy();
}

function hidebusy(){
  var b = document.getElementById('_busy');
  if (b != null){
    b.style.visibility = 'hidden';
  }
}

// browseTable object handles all browse table related functionality.
function browseTable(id,frm,phn,rows,sp,gb,mo,hs,par,ufu,uft,dc,dcm,phf,sufu,suft,fpr){
this.id = id;
this.tid = id + '_tbl';
this.table=document.getElementById(this.tid);
this.bgOver='#DDDDDD';
this.bgHigh='#CCCCCC';
this.bgOne='#FFFFFF';
this.bgTwo='#F8F8F8';
this.rowselected=-1;
this.rowshigh=rows;
this.rc=-1;
this.rs=0;
this.sproc=sp;
this.par = par;
this.gb=gb; // greenbar table.
this.mo=mo; // mouse over
this.hs=hs; // highlight selected field
this.form=document.getElementById(frm);
this.frm=frm;
this.phn=phn;
this.ufu=ufu;
this.uft=uft;
this.selectufu=sufu;
this.selectuft=suft;
this.ct=0;
this.vs=0;
this.eipclm=0;
this.dc=dc;
this.dcm=dcm;
this.quiet = 0;
tables[tables.length] = this;
this.parseCell();
this.value = phf;
this.fpr = fpr;
}
// send async request to server
browseTable.prototype.sv = function (p0,p1,p2,p3,p4){
  var options = {onComplete: function(){afterSv()}};
  options.parameters = '&_ParentProc='+this.par;
  if (p1 != undefined){options.parameters = options.parameters + '&' + p1;}
  if (p2 != undefined){options.parameters = options.parameters + '&' + p2;}
  if (p3 != undefined){options.parameters = options.parameters + '&' + p3;}
  if (p4 != undefined){options.parameters = options.parameters + '&' + p4;}
  options.parameters = options.parameters + '&_rnd_=' + Math.random(); // force uncached
  ajaxEngine.sendRequest(p0,options);
}

browseTable.prototype.showbusy = function (){
 var b = document.getElementById('_busy');
 if (b != null){
   b.style.visibility = 'visible';
 }
}

// recursive function to find the first checkbox which is "inside" c.
function getCheckbox(c){
 if (c.type == 'checkbox'){
  return c;
 }
 if (c.firstChild != null){
  a = getCheckbox(c.firstChild);
  if (a != null){
  return a;
  }
 }
 while (c.nextSibling != null){
  a = getCheckbox(c.nextSibling);
  if (a != null){
   return a;
  }
 }
 return null;
}

// checkbox on top of column to set checkbox in whole column.
browseTable.prototype.ec = function(hcb){
 for(var i=0;i<this.table.tHead.rows[0].cells.length;i++){
  if (this.table.tHead.rows[0].cells[i] == hcb.parentNode){
   var c = i;
  }
 }
 this.quiet = 1;
 for(i=0;i<this.table.tBodies[0].rows.length;i++){
  var o = this.table.tBodies[0].rows[i].cells[c].firstChild;
  cb = getCheckbox(o);
  if (cb != null){
    cb.checked = hcb.checked;
    cb.onclick();
  }
 }
 this.quiet = 0;
}

// called to trigger eip
browseTable.prototype.eip = function(t,c,v){
 this.sv(this.sproc,'_EIPClm='+c,'ViewState='+v,'Value='+FieldValue(t),'_Quiet='+this.quiet);
}

// parses the top, left cell
browseTable.prototype.parseCell = function (){
 if (this.table == null){
   return;
 }
 if (this.table.tBodies[0] == null){
   return;
 }
 if (this.table.tBodies[0].rows[0] == null){
   return;
 }
 if (this.table.tBodies[0].rows[0].cells[0] == null){
   return;
 }
 var s = this.table.tBodies[0].rows[0].cells[0].innerHTML;
 var x = s.indexOf('<!--');
 if (x==0){
  var y = s.indexOf('-->');
  if (y>x){
   s = s.slice(x+4,y);
   var parts = s.split(',');
   for (x = 0; x < parts.length; x +=2){
    switch (parts[x]){
     case "selected":
      if (this.hs ==1){
       this.rowselected = parts[x+1]-1;
      }
      break;
     case "rows":
      this.rowshigh = parts[x+1];
      break;
     case "value":
      this.value = parts[x+1];
      break;
    }
   }
  }
  s = this.table.tBodies[0].rows[0].cells[0].innerHTML;
  this.table.tBodies[0].rows[0].cells[0].innerHTML = s.slice(y+3);
 }
}
// green'bars the table. And highlights selected row.
browseTable.prototype.greenBar = function (){
 var x=0;
 var i=0;
 var j=0;
 if (this.table == null){
   return;
 }
 if (this.table.tBodies[0] == null){
   return;
 }
 if ((this.gb == 0) && (this.hs == 0) && (this.mo == 0))
 {
	return;
 }
 if (this.rowshigh>0){
  for(i=0;i<this.table.tBodies[0].rows.length;i = i - -this.rowshigh){
   for(j=0;j<this.rowshigh;j++){
    if ((parseInt(i/this.rowshigh)==this.rowselected)){
     if (this.gb==1){
      if (x==0){
       this.rs = this.bgOne;
      } else {
       this.rs = this.bgTwo;
      }
     } else {
      this.rs = this.table.tBodies[0].rows[i+j].bgColor;
     }
     this.table.tBodies[0].rows[i+j].bgColor= this.bgHigh;
    } else {
     if (this.table.tBodies[0].rows[i+j] != null){
      if (this.gb==1){
       if (x==0){
        this.table.tBodies[0].rows[i+j].bgColor=this.bgOne;
       } else {
        this.table.tBodies[0].rows[i+j].bgColor=this.bgTwo;
       }
      } else {
      this.table.tBodies[0].rows[i+j].bgColor=this.bgOne;
      }
     }
    }
   }
  if (x==0){x=1} else {x=0};
  }
 }
}
// onmouseover
browseTable.prototype.omv = function (r){
 if (this.mo==1){
  this.rc = r.bgColor
  var x=r.sectionRowIndex % this.rowshigh;
  var l=r.sectionRowIndex - (r.sectionRowIndex % this.rowshigh);
  for(x=0;x<this.rowshigh;x++){
   if (this.table.tBodies[0].rows[l+x] != null){
     this.table.tBodies[0].rows[l+x].bgColor=this.bgOver;
   }
  }
 }
}
//onmouseout
browseTable.prototype.omt = function (r){
 if (this.mo==1){
  if (this.rc == -1){
   var x=r.sectionRowIndex % this.rowshigh;
   if (x % 2 == 0){
    this.rc = this.bgOne;
   } else {
    this.rc = this.bgTwo;
   }
  }
  var x=r.sectionRowIndex % this.rowshigh;
  var l=r.sectionRowIndex - (r.sectionRowIndex % this.rowshigh);
  for(x=0;x<this.rowshigh;x++){
   if (this.table.tBodies[0].rows[l+x] != null){
    this.table.tBodies[0].rows[l+x].bgColor=this.rc;
   }
  }
 }
}
// clickrow
browseTable.prototype.cr = function (r,phf,s){
  // this.rs holds the greenbar color under the previously selected color
  // this.rowselected holds the previous row selected
  var i = parseInt(r.sectionRowIndex/this.rowshigh);
  if (i != this.rowselected){
    var k = this.rowselected * this.rowshigh;
    if (this.hs ==1){
      for(var j=0;j<this.rowshigh;j++){
        this.table.tBodies[0].rows[k+j].bgColor= this.rs;
      }
    }
    this.rowselected = i;
		if (this.mo==0){
			this.rc = r.bgColor
		}
    this.rs = this.rc
    if (this.hs ==1){
      this.rc = this.bgHigh;
    }
    k = this.rowselected * this.rowshigh;
    if (this.hs ==1){
      for(j=0;j<this.rowshigh;j++){
        this.table.tBodies[0].rows[k+j].bgColor= this.bgHigh;
      }
    }
    this.sv(this.sproc,'_Clicked=1','id='+this.phn,'Value='+phf);
    if (this.par != '' && this.par != this.id){
      var lurl = this.par+'_'+this.id+'_value';
      this.sv(lurl.toLowerCase(),'event=1',this.phn+'='+phf,'_Silent='+s);
    }
    //this.setvalue(phf);
    this.value = phf;
  }
  return;
}

// finds the element, and sets the value
browseTable.prototype.setvalue = function (phf){
  this.form=document.getElementById(this.frm);
  if ((phf != null) && (phf != '')){
    this.value = phf;
  }
  if (this.phn != null){
    var e = document.getElementById(this.phn.replace(/:/g,"__"));
  }
  if ((e == null) && (this.phn != '')){
    var rid = document.createElement('INPUT');
    rid.type = 'hidden';
    rid.name = this.phn.replace(/:/g,"__");
    rid.value = this.value;
    this.form.appendChild(rid);
  } else {
    if (e != null){
      e.value = this.value;
    }
  }
}

// sets greenbaring,selected and mouse over colors for the browse.
browseTable.prototype.setGreenBar = function (h,o,t,v){
  if ((h != '') && (h != null)){
    this.bgHigh= h;
  }
  if ((o != '') && (o != null)){
    this.bgOne=o;
  }
  if ((t != '') && (t != null)){
    this.bgTwo=t;
  }
  if ((v != '') && (v != null)){
    this.bgOver=v;
  }
}

browseTable.prototype.ufut = function (){
  this.form=document.getElementById(this.frm);
  this.form.action = this.ufu;
  this.form.target = this.uft;
}

browseTable.prototype.alertParent = function(){
  if (this.par != '' && this.par != this.id){
    var lurl = this.par+'_'+this.id+'_value';
    this.sv(lurl.toLowerCase(),'event=1');
  }
}

browseTable.prototype.first = function (){
 this.showbusy();
 this.sv(this.sproc,'Refresh=first')
 this.alertParent();
}

browseTable.prototype.previous = function (){
 this.showbusy();
 this.sv(this.sproc,'Refresh=previous')
 this.alertParent();
}

browseTable.prototype.next = function (){
 this.showbusy();
 this.sv(this.sproc,'Refresh=next')
 this.alertParent();
}

browseTable.prototype.last = function (){
 this.showbusy();
 this.sv(this.sproc,'Refresh=last')
 this.alertParent();
}

browseTable.prototype.cl = function (){
 this.showbusy();
 this.sv(this.sproc,'Refresh=clearlocate')
 this.alertParent();
}

browseTable.prototype.locate = function (id,v){
 this.showbusy();
 this.sv(this.sproc,'Refresh=locate',id+'='+v)
 this.alertParent();
}

browseTable.prototype.sort = function (s){
 this.showbusy();
 this.sv(this.sproc,'Refresh=sort',this.id+'_Sort='+s);
 this.alertParent();
}

browseTable.prototype.insert = function (){
 this.ufut();
 this.dsb('insert_btn');
}

browseTable.prototype.change = function (phf){
 this.setvalue(phf);
 this.ufut();
 this.dsb('change_btn');
}

browseTable.prototype.view = function (phf){
 this.setvalue(phf);
 this.ufut();
 this.dsb('view_btn');
}

browseTable.prototype.copy = function (phf){
 this.setvalue(phf);
 this.ufut();
 this.dsb('copy_btn');
}

browseTable.prototype.deleteb = function (phf,url){
 this.form=document.getElementById(this.frm);
 if(this.dc==1){
  if(confirm(this.dcm)==false){
   return false;
  }
 }
 this.setvalue(phf);
 this.form.action = url+'?FromForm='+this.fpr;
 this.form.target = '_self';
 this.dsb('deleteb_btn');
}

browseTable.prototype.select = function (phf){
 this.setvalue(phf);
 this.form.action = this.selectufu;
 this.form.target = this.selectuft;
 this.dsb('select_btn');
}

browseTable.prototype.lookup = function (lurl,ltar){
 this.form.action = lurl;
 this.form.target = ltar;
 this.dsb('lookup_btn');
}
  
// Lookup Date
browseTable.prototype.sd = function (id,p,ec,vs){
 this.ct = document.getElementById(id);
 switch (p){
 case "@D6":
 case "@D06":
  var c = new calendar6(this.ct);
  break;
 case "@D2":
 case "@D02":
  var c = new calendar2(this.ct);
  break;
 }
 this.eipclm = ec;
 this.vs = vs;
 c.popup();
}

// Reset After Date
browseTable.prototype.rad = function(){
 this.eip(this.ct,this.eipclm,this.vs);
}

browseTable.prototype.dsb = function (n){
 this.form=document.getElementById(this.frm);
 for (var e=0 ; e < this.form.elements.length; e++) {
  if(this.form.elements[e].type=='button'){
   if (this.form.target == "" || this.form.target == "_self"){
    this.form.elements[e].disabled = true;
   }
  }
 }
 var pb = document.createElement('INPUT');
 pb.type = 'hidden';
 pb.name = 'pressedButton';
 pb.value = n;
 this.form.appendChild(pb);
 this.form.submit();
}

browseTable.prototype.removeOtherBrowses = function (){
 var f=document.getElementById(fn);
 var dv=document.getElementById(dn);
 var a;
 var b;
 if (dv != null){
  var divs = dv.getElementsByTagName('DIV');
  for(var e = divs.length-1; e>=0 ; e--){
   if ((divs[e].id != dn) && (divs[e].id != '')){
    removeElement(fn,divs[e].id);
   }
  }
  if (f != null){
   for(var e = f.elements.length-1; e>=0 ; e--) {
    a = f.elements[e].parentNode.id;
    b = dv.id
    if (a==b){
     try{
      dv.removeChild(f.elements[e]);
     } catch (e) {
     }
    }
   }
  }
 }
}

function dsb(f,b,n,prid,prv){
 var i=0;
 if (n=='deleteb_btn'){
  if(confirm('Are you sure you want to delete this record?')==false){
   return false;
  }
 }
 for (var e=0 ; e < f.elements.length; e++) {
  if(f.elements[e].type=='button'){
   if (f.target == "" || f.target == "_self"){
    f.elements[e].disabled = true;
   }
  }
  else {
   if (f.elements[e].name == prid){
    f.elements[e].value = prv;
    i = 1;
   }
  }
 }
 if ((i==0) && (prid != '')){
  var rid = document.createElement('INPUT');
  rid.type = 'hidden';
  rid.name = prid;
  rid.value = prv;
  f.appendChild(rid);
 }
 var pb = document.createElement('INPUT');
 pb.type = 'hidden';
 pb.name = 'pressedButton';
 pb.value = n;
 f.appendChild(pb);
 osf(f);
  f.submit();
}

function osf(f){
  if(f.target=='' || f.target=='_self' || f.target=='_top') {
    for (var e=0 ; e < f.elements.length; e++) {
      if(f.elements[e].type=='button'){
  f.elements[e].disabled = true;
      }
    }
  }
}

function ml(ta,ml,e){
  var k;
  if(window.event){ // IE
    k = e.keyCode
  } else if(e.which){ // Netscape/Firefox/Opera/Safari
    k = e.which
  };
  if (k==8 || k==null || k > 60000){
    return true;
  }
  return (ta.value.length <= ml);
}

function nextFocus(f,pname,skipone){
  var i = 0;
  var j = 0;
  if (skipone==2){ // pname is specified control to get focus
    for (var e=0 ; e < f.elements.length; e++) {
      if(f.elements[e].name==pname){
  try{
    f.elements[e].focus();
  } catch (e) {
  }
  break;
      }
    }
  } else {
    for (var e=0 ; e < f.elements.length; e++) {
      if (i==1){
  if ((f.elements[e].type == "text") || (f.elements[e].type == "textarea") || (f.elements[e].type == "checkbox") || (f.elements[e].type == "radio") || (f.elements[e].type == "select-one")){
    //|| (f.elements[e].type == "button")
    if(f.elements[e].readOnly != true){
      if((skipone==1) && (j==0)){
        j = 1;
      } else {
        try{
    f.elements[e].focus();
        } catch (e) {
        }
        break;
      }
    }
  }
      }
      else{
  if(pname==''){
    if(f.elements[e].readOnly != true){
      try{
        f.elements[e].focus();
      } catch (e) {
      }
      break;
    }
  } else {
    if(f.elements[e].name==pname){
      i = 1;
    }
  }
      }
    }
  }
}


function removeElement(fn,dn){
 var f=document.getElementById(fn);
 var dv=document.getElementById(dn);
 var a;
 var b;
 if (dv != null){
  var divs = dv.getElementsByTagName('DIV');
  for(var e = divs.length-1; e>=0 ; e--){
   if ((divs[e].id != dn) && (divs[e].id != '')){
    removeElement(fn,divs[e].id);
   }
  }
  if (f != null){
   for(var e = f.elements.length-1; e>=0 ; e--) {
    a = f.elements[e].parentNode.id;
    b = dv.id
    if (a==b){
     try{
      dv.removeChild(f.elements[e]);
     } catch (e) {
     }
    }
   }
  }
 }
}

var wizActive=0;
var nextBtn;
var prevBtn;
var finishBtn;
var wizMax=0;
var wizHeight=0;
var wizX=0;
var wizTabs = new Array();

function initWizard(fn,tb,at,hi){
  var f = document.getElementById(fn);
  for (var e=0 ; e < f.elements.length; e++) {
    if(f.elements[e].name=='wiznext_btn'){
      nextBtn = f.elements[e];
    }
    if(f.elements[e].name=='wizprevious_btn'){
      prevBtn = f.elements[e];
    }
    if(f.elements[e].name=='save_btn'){
      finishBtn = f.elements[e];
    }
  }
  if (arguments.length > 3){
    wizHeight = hi;
  }
  wizMax = tb.length-1;
  wizTabs = tb;
  for(e=0 ; e < tb.length ; e++){
    wizTabs[e] = document.getElementById(tb[e]);
    if (wizTabs[e].offsetHeight > wizHeight && arguments.length <= 3){
      wizHeight = wizTabs[e].offsetHeight;
    }
    if (e>0){
      wizTabs[e].style.top = wizTabs[0].offsetTop + 'px';
    }
    wizHide(wizTabs[e])
  }
  wizHeight = wizHeight + 'px';
  wizActive = at;
  wizShow(wizTabs[wizActive],wizMax);
  wizButtons(wizActive,wizMax,prevBtn,nextBtn,finishBtn);
}
function wizNext(){
  if (wizActive < wizMax){
    wizHide(wizTabs[wizActive]);
    wizActive++;
    wizShow(wizTabs[wizActive],wizMax);
    wizButtons(wizActive,wizMax,prevBtn,nextBtn,finishBtn);
  }
}
function wizPrev(){
  if (wizActive > 0){
    wizHide(wizTabs[wizActive]);
    wizActive--;
    wizShow(wizTabs[wizActive],wizMax);
    wizButtons(wizActive,wizMax,prevBtn,nextBtn,finishBtn);
  }
}
function wizHide(a){
  if (a < 0) return;
  a.style.display='none';
  a.style.visibility='hidden';
  a.style.height = 0;
}
function wizShow(a,m){
  if (a > m) return;
  a.style.display='';
  a.style.visibility='visible';
  a.style.height = wizHeight;
}

function wizButtons(a,m,p,n,f){
  if (a ==0){
    p.disabled = true;
  }  else {
    p.disabled = false;
  }
  if (a == m){
    n.disabled = true;
    if (f != null){
      f.disabled = false;
    }
  }  else {
    n.disabled = false;
    if (f != null){
      f.disabled = true;
    }
  }
}

function FieldValue(f,e){
  var ans ='';
  var typ = f.type;
  var i = 0;
  var j = 0;
  if (typ == undefined){
    typ = f[0].type;
  }
  switch (typ){
  case "radio":
    j = f.length;
    for(i = 0; i < j; i++) {
      if(f[i].checked) {
  ans = f[i].value;
  break;
      }
    }
    break;
  case "checkbox":
    if (f.checked){
      ans = f.value;
    }
    break;
  case "select-multiple":
    j = f.length;
    for(i = 0; i < j; i++) {
      if(f.options[i].selected) {
        ans = ans + ';|;' + f.options[i].value;
    }
    }
    break;
  default: ans = f.value;
  }
  // if called as a post, do not encode & and %. If called from EIP then do.
  if ((e == 0) || (e == undefined)){
                ans = ans.replace(/%/,"%25");
                ans = ans.replace(/&/,"%26");
        }
  return ans
}

var onloads = new Array();
function bodyOnLoad() {
  for ( var i = 0 ; i < onloads.length ; i++ )
    onloads[i]();
}

var defaultButton='';
var defaultButtonSubmit=1;
var defaultButtonId=null;
function oe(ta,e) {
   var a=true;
   var k;
   if(window.event){ // IE
     k = e.keyCode
   } else if(e.which){ // Netscape/Firefox/Opera/Safari
     k = e.which
   };
   if ( k == "13" || k == "3") {
      a = false;
      k = null;
      defaultButtonId = document.getElementById(defaultButton);
      if (defaultButtonId != null){
  if (defaultButtonSubmit==1){
    // if default button is going to submit the form then just
    if (ta.onchange){
      ta.onchange();
    }
    if (defaultButtonId.click){
      defaultButtonId.click();
    }
  } else {
    // otherwise
    if (ta.onchange){
      ta.onchange();
    }
    window.setTimeout('defaultButtonId.click();',100);
    if (e && e.preventDefault)
      e.preventDefault(); // FF style
    return false; // IE style
  }
      } else {
   if (ta.onchange){
     ta.onchange();
   }
      }
   }
   return a;
}

function setDefaultButton(b,s){
  defaultButton = b;
  defaultButtonSubmit = s;
}

function SetSessionValue(name,value){
  ajaxEngine.sendRequest('SetSessionValue',name + '=' + value);
}

// SetServer
function sv(id,name,ev,val,par,sil){
 var options = {onComplete: function(){afterSv()}};
 if(par==undefined){
  val = escape(val);
  ev = escape(ev);
  options.parameters = "event=" + ev + '&' + "value=" +val;
 }else{
  options.parameters = ev+ '&' +val+ '&' +par+ '&' +sil;
 }
 hadfocus = id;
 options.parameters = options.parameters + '&_rnd_=' + Math.random(); // force uncached
 ajaxEngine.sendRequest(name,options);
}

//Set timer
function SetTimer(name,t,par,sil){
 if(par==undefined)  {par='fred=1'};
 if(sil==undefined)  {sil='fred=2'};
 sv('',name,'','',par,sil);
 setTimeout('SetTimer(\'' + name +'\','+t+',\''+par +'\',\'' + sil+'\')',t);
};

// Register Div for Rico
function RegRico(a){
 ajaxEngine.registerRequest( a, a );
 ajaxEngine.registerAjaxElement( a + '_div' );
 rrd(a);
}

// Register all divs for Rico
function rrd(a){
 var d=document.getElementById(a + '_div');
 var inputArr = d.getElementsByTagName('div');
 for (var i = 0; i < inputArr.length; i++) {
  if (inputArr[i].id != ''){
   ajaxEngine.registerAjaxElement( inputArr[i].id )
  }  
 }
}

// SelectDate and ResetAfterDate called by Date Lookup button
var cr1;
var cs;
var ct;
var cb1;
var cb2;
// SelectDate
function sd(f,e,p,r,b1,b2){
 ct = document.forms[f].elements[e];
 switch (p){
 case "@D6":
 case "@D06":
  var c = new calendar6(ct);
  break;
 case "@D2":
 case "@D02":
  var c = new calendar2(ct);
  break;
 }
 c.popup();
 if (arguments.length == 4){
  cr1 = r;
  cs = 1;
 }
 if (arguments.length == 6){
  cr1 = r;
  cs = 2;
  cb1 = b1;
  cb2 = b2;
 }
}
// ResetAfterDate
function rad(){
 if (cs==1){
  sv('',cr1,1,ct.value);
  cs = 0;
 }
 if (cs==2){
  sv('',cr1,cb1,cb2,'Value='+ct.value);
  cs = 0;
 }
}
