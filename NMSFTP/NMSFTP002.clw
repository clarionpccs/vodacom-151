

   MEMBER('NMSFTP.clw')                                    ! This is a MEMBER module

                     MAP
                       INCLUDE('NMSFTP002.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('NMSFTP001.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('NMSFTP003.INC'),ONCE        !Req'd for module callout resolution
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Starting             PROCEDURE                             ! Declare Procedure
!Declarations
AllFiles QUEUE(File:queue),PRE(FIL)        !Inherit exact declaration of File:queue
         END
RequestStatusQueue  Queue(),Pre(reqque)
MessageID                   String(30)
Code                        String(30)
Description                 String(30)
SourceSiteID                String(30)
ClientReferenceNumber       String(30) ! Batch Number
ClientBookingNumber         String(30) ! Job Number
ErrorCode                   String(30)
ErrorDescription            String(100)
                    End ! RequestStatusQueue  Queue(),Pre(reqque)

PreAlertSourceSiteQueue        Queue,Pre(souque)
ID                                String(30) ! Third Party ID    
                            End ! PreAlertSourceSiteQueue        Queue(souque)
                            
PreAlertWaybillQueue        Queue,Pre(wayque)                            
Number                    String(30)    
                            End ! PreAlertWaybillQueue        Queue(wayque)
                            
PreAlertBookingQueue        Queue,Pre(booque)                            
ClientBookingNumber            String(30)
OriginalSerialNumber           String(30)
FaultSymptomCode               String(30)
WarrantyOption                 String(30)
                            End ! PreAlertBookingQueue        Queue,Pre(booque)

local       Class
AddToAudit      Procedure(String f:Action)
            End

tmp:XMLFolder           CString(255)
tmp:ReceivedFolder      Cstring(255)
tmp:MessageID           CString(30)
tmp:ThirdPartyID        CString(30)
tmp:WaybillNumber       CString(30)
tmp:BatchNumber         CString(30)


JOBS::State  USHORT
AUDIT::State  USHORT
NMSPRE::State  USHORT
TRDPARTY::State  USHORT
TRDBATCH::State  USHORT
FilesOpened     BYTE(0)

  CODE
!Processing
    AddToLog('=== NMS Process Start ===')
    AddToLog('Start FTP')
    FTPProcess(0) ! Upload
    tmp:XMLFolder = GETINI('NMSFTP','XMLFolder',,Clip(Path()) & '\SB2KDEF.INI')

    tmp:ReceivedFolder = tmp:XMLFolder & '\Received'

    Do OpenFiles
    Do SaveFiles

    AddToLog('Process Received Files')

    Directory(AllFiles,tmp:ReceivedFolder & '\RBR_*.xml',ff_:Directory)
    Loop x# = 1 To Records(AllFiles)
        Get(AllFiles,x#)
        Free(RequestStatusQueue)
        tmp:MessageID = ''
        tmp:BatchNumber = ''
        If ~XML:LoadFromFile(tmp:ReceivedFolder & '\' & Clip(fil:Name))
            If ~XML:FindNextNode('Message','Id')
                a" = XML:ReadCurrentData(tmp:MessageID)
            End ! If ~XML:FindNextNode('Message','Id')
            XML:GotoTop
            If ~XML:FindNextNode('ShipmentOrderResponse')
                Recs# = XML:LoadQueue(RequestStatusQueue,1,1)
                Get(RequestStatusQueue,1)

                If ~Error()
                    Case Clip(Upper(reqque:Description))
                    Of 'ORDER SUCCESSFUL'
                        Access:TRDPARTY.ClearKey(trd:ascidkey)
                        trd:ASCID = Clip(Upper(reqque:SourceSiteID))
                        If Access:TRDPARTY.TryFetch(trd:ascidkey) = Level:Benign
                            !Found
                            ! Remove the date/time stamp from the batch number
                            pos# = Instring('-',reqque:ClientReferenceNumber,1,1)
                            Access:TRDBATCH.Clearkey(trb:Batch_Number_Key)
                            trb:Company_Name = trd:Company_Name
                            trb:Batch_Number = Sub(reqque:ClientReferenceNumber,1,pos#-1)
                            Set(trb:Batch_Number_Key,trb:Batch_Number_Key)
                            Loop ! Begin Loop
                                If Access:TRDBATCH.Next()
                                    Break
                                End ! If Access:TRDBATCH.Next()
                                If trb:Company_Name <> trd:Company_Name
                                    Break
                                End ! If trb:Company_Name <> trd:Company_Name
                                If trb:Batch_Number <> reqque:ClientReferenceNumber
                                    Break
                                End ! If trb:Batch_Number <> reqque:ClientReferenceNumber
                                Access:JOBS.ClearKey(job:Ref_Number_Key)
                                job:Ref_Number = trb:Ref_Number
                                If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                                    !Found
                                    local.AddToAudit(Clip(Upper(reqque:Description)))
                                Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                                    !Error
                                End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign

                            End ! Loop

                            If ~XML:CreateXMLFile(Clip(tmp:XMLFolder) & '\ToSend\RBRR_' & Clip(reqque:ClientReferenceNumber) & '_' & Format(Today(),@d12) & Clock() & '.xml','1.0')
                                XML:CreateParent('ResponseReceipt')
                                XML:CreateAttribute('Version','4.0')
                                XML:AddParent()
                                    XML:AddElement('MessageID',tmp:MessageID)
                                    XML:AddElement('SiteID',GETINI('NMSFTP','SiteID',,Clip(Path()) & '\SB2KDEF.INI'))
                                    XML:AddElement('ReceivedStatus','01')
                                    XML:AddElement('ReceivedTimestamp',Format(Today(),@d05) & ' ' & Format(Clock(),@t04))
                                    XML:AddElement('TimeZone','01')
                                XML:CloseParent()
                                XML:CloseXMLFile()
                            End ! If ~XML:CreateXMLFile(Clip(tmp:XMLFolder) & '\ToSend\' & Format(Today(),@d12) & Format(Clock(),@t5) & '.xml')

                            Rename(tmp:ReceivedFolder & '\' & Clip(fil:Name),tmp:XMLFolder & '\Processed\' & Clip(fil:Name))

                        Else ! If Access:TRDPARTY.TryFetch(trd:ascidkey) = Level:Benign
                            !Error
                            AddToLog('Invalid ASC ID: ' & Clip(Upper(reqque:SourceSiteID)) & ', File: ' & Clip(fil:Name))
                            Rename(tmp:ReceivedFolder & '\' & Clip(fil:Name),tmp:XMLFolder & '\bad\' & Clip(fil:Name))
                        End ! If Access:TRDPARTY.TryFetch(trd:ascidkey) = Level:Benign

                    Else ! Case Clip(Upper(reqque:Description))
                        Access:JOBS.ClearKey(job:Ref_Number_Key)
                        job:Ref_Number = reqque:ClientBookingNumber
                        If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                            !Found
                            local.AddToAudit(Clip(Upper(reqque:Description)))

                            If ~XML:CreateXMLFile(Clip(tmp:XMLFolder) & '\ToSend\RBRR_' & job:Ref_Number & '_' & Format(Today(),@d12) & Clock() & '.xml','1.0')
                                XML:CreateParent('ResponseReceipt')
                                XML:CreateAttribute('Version','4.0')
                                XML:AddParent()
                                    XML:AddElement('MessageID',tmp:MessageID)
                                    XML:AddElement('SiteID',GETINI('NMSFTP','SiteID',,Clip(Path()) & '\SB2KDEF.INI'))
                                    XML:AddElement('ReceivedStatus','01')
                                    XML:AddElement('ReceivedTimestamp',Format(Today(),@d05) & ' ' & Format(Clock(),@t04))
                                    XML:AddElement('TimeZone','01')
                                XML:CloseParent()
                                XML:CloseXMLFile()
                            End ! If ~XML:CreateXMLFile(Clip(tmp:XMLFolder) & '\ToSend\' & Format(Today(),@d12) & Format(Clock(),@t5) & '.xml')

                            Rename(tmp:ReceivedFolder & '\' & Clip(fil:Name),tmp:XMLFolder & '\Processed\' & Clip(fil:Name))
                        Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                            !Error
                            AddToLog('Invalid Client Booking Number: ' & Clip(Upper(reqque:ClientBookingNumber)) & ', File: ' & Clip(fil:Name))
                            Rename(tmp:ReceivedFolder & '\' & Clip(fil:Name),tmp:XMLFolder & '\bad\' & Clip(fil:Name))
                            Cycle
                        End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign

                    End ! Case Clip(Upper(reqque:Description))
                    
                End ! If ~Error()
            End ! If ~XML:FindNextNode('RequestStatus')
            XML:Free()
        End ! If ~XML:LoadFromFile(Clip(fil:Name))
    End ! Loop x# = 1 To Records(AllFile)

    AddToLog('Process PreAlert Files')

    Directory(AllFiles,tmp:ReceivedFolder & '\PABR_*.xml',ff_:Directory)
    Loop x# = 1 To Records(AllFiles)
        Get(AllFiles,x#)
        tmp:MessageID = ''
        tmp:ThirdPartyID = ''
        tmp:WaybillNumber = ''

        If ~XML:LoadFromFile(tmp:ReceivedFolder & '\' & Clip(fil:Name))
            If ~XML:FindNextNode('Message','Id')
                a" = XML:ReadCurrentData(tmp:MessageID)
            End ! If ~XML:FindNextNode('Message','Id')
            XML:GotoTop
            If ~XML:FindNextNode('PrealertResponse','SourceSite')
                Recs# = XML:LoadQueue(PreAlertSourceSiteQueue,1,0)
                Get(PreAlertSourceSiteQueue,1)
                If ~Error()
                    tmp:ThirdPartyID = souque:ID
                End ! If ~Error()
            End ! If ~XML:FindNextNode('')
            XML:GotoTop
            If ~XML:FindNextNode('SourceSite','AirWayBill')
                Recs# = XML:LoadQueue(PreAlertWaybillQueue,0,1)
                Get(PreAlertWaybillQueue,1)
                If ~Error()
                    tmp:WaybillNumber = wayque:Number
                End ! If ~Error()
            End ! If ~XML:FindNextNode('AirWayBill')
            XML:GotoTop


            If tmp:ThirdPartyID = ''
                AddToLog('Invalid Third Party ID: ' & Clip(Upper(tmp:ThirdPartyID)) & ', File: ' & Clip(fil:Name))
                Rename(tmp:ReceivedFolder & '\' & Clip(fil:Name),tmp:XMLFolder & '\bad\' & Clip(fil:Name))

                Cycle
            End ! If tmp:thirdPartyID = 0
            If ~XML:FindNextNode('Airwaybill','Booking')
                Recs# = XML:LoadQueue(PreAlertBookingQueue,0,1)
                Loop y# = 1 To Recs#
                    Get(PreAlertBookingQueue,y#)
                    If Access:NMSPRE.PrimeRecord() = Level:Benign
                        nms:JobNumber = Clip(booque:ClientBookingNumber)
                        nms:IMEINumber = Clip(booque:OriginalSerialNumber)
                        nms:ThirdPartyID = tmp:ThirdPartyID
                        nms:WayBillNumber = tmp:WaybillNumber
                        nms:FaultSymptomCode = Clip(booque:FaultSymptomCode)
                        nms:WarrantyOption = Clip(booque:WarrantyOption)
                        nms:Processed = 0
                        If Access:NMSPRE.TryInsert() = Level:Benign
                            !Insert
                        Else ! If Access:NMSPRE.TryInsert() = Level:Benign
                            Access:NMSPRE.CancelAutoInc()
                        End ! If Access:NMSPRE.TryInsert() = Level:Benign
                    End ! If Access.NMSPRE.PrimeRecord() = Level:Benign
                End ! Loop y# = 1 To Recs#
                If ~XML:CreateXMLFile(Clip(tmp:XMLFolder) & '\ToSend\PABRR_' & Clip(reqque:ClientReferenceNumber) & '_' & Format(Today(),@d12) & Clock() & '.xml','1.0')
                    XML:CreateParent('ResponseReceipt')
                    XML:CreateAttribute('Version','4.0')
                    XML:AddParent()
                        XML:AddElement('MessageID',tmp:MessageID)
                        XML:AddElement('SiteID',GETINI('NMSFTP','SiteID',,Clip(Path()) & '\SB2KDEF.INI'))
                        XML:AddElement('ReceivedStatus','01')
                        XML:AddElement('ReceivedTimestamp',Format(Today(),@d05) & ' ' & Format(Clock(),@t04))
                        XML:AddElement('TimeZone','01')
                    XML:CloseParent()
                    XML:CloseXMLFile()
                End ! If ~XML:CreateXMLFile(Clip(tmp:XMLFolder) & '\ToSend\' & Format(Today(),@d12) & Format(Clock(),@t5) & '.xml')

                Rename(tmp:ReceivedFolder & '\' & Clip(fil:Name),tmp:XMLFolder & '\Processed\' & Clip(fil:Name))
            End ! If ~XML:FindNextNode('Booking')
        End ! If ~XML:LoadFromFile(tmp:ReceivedFolder & '\' & Clip(fil:Name))
    End ! Loop x# = 1 To Records(AllFiles)

    Do RestoreFiles
    Do CloseFiles

    AddToLog('=== NMS Process Finished ===')
SaveFiles  ROUTINE
  JOBS::State = Access:JOBS.SaveFile()                     ! Save File referenced in 'Other Files' so need to inform its FileManager
  AUDIT::State = Access:AUDIT.SaveFile()                   ! Save File referenced in 'Other Files' so need to inform its FileManager
  NMSPRE::State = Access:NMSPRE.SaveFile()                 ! Save File referenced in 'Other Files' so need to inform its FileManager
  TRDPARTY::State = Access:TRDPARTY.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
  TRDBATCH::State = Access:TRDBATCH.SaveFile()             ! Save File referenced in 'Other Files' so need to inform its FileManager
RestoreFiles  ROUTINE
  IF JOBS::State <> 0
    Access:JOBS.RestoreFile(JOBS::State)                   ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF AUDIT::State <> 0
    Access:AUDIT.RestoreFile(AUDIT::State)                 ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF NMSPRE::State <> 0
    Access:NMSPRE.RestoreFile(NMSPRE::State)               ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF TRDPARTY::State <> 0
    Access:TRDPARTY.RestoreFile(TRDPARTY::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
  IF TRDBATCH::State <> 0
    Access:TRDBATCH.RestoreFile(TRDBATCH::State)           ! Restore File referenced in 'Other Files' so need to inform its FileManager
  END
!--------------------------------------
OpenFiles  ROUTINE
  Access:JOBS.Open                                         ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:JOBS.UseFile                                      ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:AUDIT.Open                                        ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:AUDIT.UseFile                                     ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:NMSPRE.Open                                       ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:NMSPRE.UseFile                                    ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRDPARTY.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRDPARTY.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  Access:TRDBATCH.Open                                     ! Open File referenced in 'Other Files' so need to inform its FileManager
  Access:TRDBATCH.UseFile                                  ! Use File referenced in 'Other Files' so need to inform its FileManager
  FilesOpened = True
!--------------------------------------
CloseFiles ROUTINE
  IF FilesOpened THEN
     Access:JOBS.Close
     Access:AUDIT.Close
     Access:NMSPRE.Close
     Access:TRDPARTY.Close
     Access:TRDBATCH.Close
     FilesOpened = False
  END
local.AddToAudit        Procedure(String f:Action)
Code
    If Access:AUDIT.PrimeRecord() = Level:Benign
        aud:Ref_Number  = job:Ref_Number
        aud:Date        = Today()
        aud:Time        = Clock()
        aud:User        = '&SB'
        aud:Action      = 'NMS BOOKING FOR REPAIR RESPONSE: ' & Clip(f:Action)
        aud:Notes       = 'CODE: ' & Upper(Clip(reqque:Code))
        If f:Action = 'ORDER FAILED' OR f:Action = 'ORDER PARTIALLY CREATED'
            aud:Notes = Clip(aud:Notes) & '<13,10>ERROR CODE: ' & Upper(Clip(reqque:ErrorCode)) & |
                                        '<13,10>ERROR DESC: ' & Upper(Clip(reqque:ErrorDescription))
        End ! If f:Action = 'FAILED'
        aud:Type        = 'JOB'
        If Access:AUDIT.TryInsert() = Level:Benign
            ! Insert Successful
            AddToLog('Reponse Received: ' & job:Ref_Number & ' - ' & Clip(f:Action))
        Else ! If Access:AUDIT.TryInsert() = Level:Benign
            ! Insert Failed
            Access:AUDIT.CancelAutoInc()
        End ! If Access:AUDIT.TryInsert() = Level:Benign
    End !If Access:AUDIT.PrimeRecord() = Level:Benign


