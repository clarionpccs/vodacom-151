  MEMBER('NMSFTP.clw')

  INCLUDE('ABFILE.INC'),ONCE

  MAP
NMSFTP_BC5:DctInit    PROCEDURE
NMSFTP_BC5:DctKill    PROCEDURE
NMSFTP_BC5:FilesInit  PROCEDURE
  END

Hide:Access:USMASSIG CLASS(FileManager),TYPE               ! FileManager for USMASSIG
Init                   PROCEDURE(),DERIVED
Kill                   PROCEDURE(),DERIVED
UseFile                PROCEDURE(BYTE UseType = UseType:Uses),BYTE,PROC,DERIVED
                     END


Hide:Relate:USMASSIG CLASS(RelationManager),TYPE           ! RelationManager for USMASSIG
Init                   PROCEDURE
DeferedAddRelations    PROCEDURE(),DERIVED
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:USUASSIG CLASS(FileManager),TYPE               ! FileManager for USUASSIG
Init                   PROCEDURE(),DERIVED
Kill                   PROCEDURE(),DERIVED
UseFile                PROCEDURE(BYTE UseType = UseType:Uses),BYTE,PROC,DERIVED
                     END


Hide:Relate:USUASSIG CLASS(RelationManager),TYPE           ! RelationManager for USUASSIG
Init                   PROCEDURE
DeferedAddRelations    PROCEDURE(),DERIVED
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBSLOCK CLASS(FileManager),TYPE               ! FileManager for JOBSLOCK
Init                   PROCEDURE(),DERIVED
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
UseFile                PROCEDURE(BYTE UseType = UseType:Uses),BYTE,PROC,DERIVED
                     END


Hide:Relate:JOBSLOCK CLASS(RelationManager),TYPE           ! RelationManager for JOBSLOCK
Init                   PROCEDURE
DeferedAddRelations    PROCEDURE(),DERIVED
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:TRAHUBAC CLASS(FileManager),TYPE               ! FileManager for TRAHUBAC
Init                   PROCEDURE(),DERIVED
Kill                   PROCEDURE(),DERIVED
UseFile                PROCEDURE(BYTE UseType = UseType:Uses),BYTE,PROC,DERIVED
                     END


Hide:Relate:TRAHUBAC CLASS(RelationManager),TYPE           ! RelationManager for TRAHUBAC
Init                   PROCEDURE
DeferedAddRelations    PROCEDURE(),DERIVED
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:STOHISTE CLASS(FileManager),TYPE               ! FileManager for STOHISTE
Init                   PROCEDURE(),DERIVED
Kill                   PROCEDURE(),DERIVED
UseFile                PROCEDURE(BYTE UseType = UseType:Uses),BYTE,PROC,DERIVED
                     END


Hide:Relate:STOHISTE CLASS(RelationManager),TYPE           ! RelationManager for STOHISTE
Init                   PROCEDURE
DeferedAddRelations    PROCEDURE(),DERIVED
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:MODEXCHA CLASS(FileManager),TYPE               ! FileManager for MODEXCHA
Init                   PROCEDURE(),DERIVED
Kill                   PROCEDURE(),DERIVED
UseFile                PROCEDURE(BYTE UseType = UseType:Uses),BYTE,PROC,DERIVED
                     END


Hide:Relate:MODEXCHA CLASS(RelationManager),TYPE           ! RelationManager for MODEXCHA
Init                   PROCEDURE
DeferedAddRelations    PROCEDURE(),DERIVED
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBSOBF_ALIAS CLASS(FileManager),TYPE          ! FileManager for JOBSOBF_ALIAS
Init                   PROCEDURE(),DERIVED
Kill                   PROCEDURE(),DERIVED
UseFile                PROCEDURE(BYTE UseType = UseType:Uses),BYTE,PROC,DERIVED
                     END


Hide:Relate:JOBSOBF_ALIAS CLASS(RelationManager),TYPE      ! RelationManager for JOBSOBF_ALIAS
Init                   PROCEDURE
DeferedAddRelations    PROCEDURE(),DERIVED
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBPAYMT_ALIAS CLASS(FileManager),TYPE         ! FileManager for JOBPAYMT_ALIAS
Init                   PROCEDURE(),DERIVED
Kill                   PROCEDURE(),DERIVED
UseFile                PROCEDURE(BYTE UseType = UseType:Uses),BYTE,PROC,DERIVED
                     END


Hide:Relate:JOBPAYMT_ALIAS CLASS(RelationManager),TYPE     ! RelationManager for JOBPAYMT_ALIAS
Init                   PROCEDURE
DeferedAddRelations    PROCEDURE(),DERIVED
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBS_ALIAS CLASS(FileManager),TYPE             ! FileManager for JOBS_ALIAS
Init                   PROCEDURE(),DERIVED
Kill                   PROCEDURE(),DERIVED
UseFile                PROCEDURE(BYTE UseType = UseType:Uses),BYTE,PROC,DERIVED
                     END


Hide:Relate:JOBS_ALIAS CLASS(RelationManager),TYPE         ! RelationManager for JOBS_ALIAS
Init                   PROCEDURE
DeferedAddRelations    PROCEDURE(),DERIVED
Kill                   PROCEDURE(),DERIVED
                     END


_Hide:Access:USMASSIG &Hide:Access:USMASSIG,AUTO,THREAD
_Hide:Relate:USMASSIG &Hide:Relate:USMASSIG,AUTO,THREAD
_Hide:Access:USUASSIG &Hide:Access:USUASSIG,AUTO,THREAD
_Hide:Relate:USUASSIG &Hide:Relate:USUASSIG,AUTO,THREAD
_Hide:Access:JOBSLOCK &Hide:Access:JOBSLOCK,AUTO,THREAD
_Hide:Relate:JOBSLOCK &Hide:Relate:JOBSLOCK,AUTO,THREAD
_Hide:Access:TRAHUBAC &Hide:Access:TRAHUBAC,AUTO,THREAD
_Hide:Relate:TRAHUBAC &Hide:Relate:TRAHUBAC,AUTO,THREAD
_Hide:Access:STOHISTE &Hide:Access:STOHISTE,AUTO,THREAD
_Hide:Relate:STOHISTE &Hide:Relate:STOHISTE,AUTO,THREAD
_Hide:Access:MODEXCHA &Hide:Access:MODEXCHA,AUTO,THREAD
_Hide:Relate:MODEXCHA &Hide:Relate:MODEXCHA,AUTO,THREAD
_Hide:Access:JOBSOBF_ALIAS &Hide:Access:JOBSOBF_ALIAS,AUTO,THREAD
_Hide:Relate:JOBSOBF_ALIAS &Hide:Relate:JOBSOBF_ALIAS,AUTO,THREAD
_Hide:Access:JOBPAYMT_ALIAS &Hide:Access:JOBPAYMT_ALIAS,AUTO,THREAD
_Hide:Relate:JOBPAYMT_ALIAS &Hide:Relate:JOBPAYMT_ALIAS,AUTO,THREAD
_Hide:Access:JOBS_ALIAS &Hide:Access:JOBS_ALIAS,AUTO,THREAD
_Hide:Relate:JOBS_ALIAS &Hide:Relate:JOBS_ALIAS,AUTO,THREAD


NMSFTP_BC5:DctInit PROCEDURE
  CODE
  _Hide:Access:USMASSIG &= NEW(Hide:Access:USMASSIG)
  _Hide:Relate:USMASSIG &= NEW(Hide:Relate:USMASSIG)
  _Hide:Access:USUASSIG &= NEW(Hide:Access:USUASSIG)
  _Hide:Relate:USUASSIG &= NEW(Hide:Relate:USUASSIG)
  _Hide:Access:JOBSLOCK &= NEW(Hide:Access:JOBSLOCK)
  _Hide:Relate:JOBSLOCK &= NEW(Hide:Relate:JOBSLOCK)
  _Hide:Access:TRAHUBAC &= NEW(Hide:Access:TRAHUBAC)
  _Hide:Relate:TRAHUBAC &= NEW(Hide:Relate:TRAHUBAC)
  _Hide:Access:STOHISTE &= NEW(Hide:Access:STOHISTE)
  _Hide:Relate:STOHISTE &= NEW(Hide:Relate:STOHISTE)
  _Hide:Access:MODEXCHA &= NEW(Hide:Access:MODEXCHA)
  _Hide:Relate:MODEXCHA &= NEW(Hide:Relate:MODEXCHA)
  _Hide:Access:JOBSOBF_ALIAS &= NEW(Hide:Access:JOBSOBF_ALIAS)
  _Hide:Relate:JOBSOBF_ALIAS &= NEW(Hide:Relate:JOBSOBF_ALIAS)
  _Hide:Access:JOBPAYMT_ALIAS &= NEW(Hide:Access:JOBPAYMT_ALIAS)
  _Hide:Relate:JOBPAYMT_ALIAS &= NEW(Hide:Relate:JOBPAYMT_ALIAS)
  _Hide:Access:JOBS_ALIAS &= NEW(Hide:Access:JOBS_ALIAS)
  _Hide:Relate:JOBS_ALIAS &= NEW(Hide:Relate:JOBS_ALIAS)
  Relate:USMASSIG &= _Hide:Relate:USMASSIG
  Relate:USUASSIG &= _Hide:Relate:USUASSIG
  Relate:JOBSLOCK &= _Hide:Relate:JOBSLOCK
  Relate:TRAHUBAC &= _Hide:Relate:TRAHUBAC
  Relate:STOHISTE &= _Hide:Relate:STOHISTE
  Relate:MODEXCHA &= _Hide:Relate:MODEXCHA
  Relate:JOBSOBF_ALIAS &= _Hide:Relate:JOBSOBF_ALIAS
  Relate:JOBPAYMT_ALIAS &= _Hide:Relate:JOBPAYMT_ALIAS
  Relate:JOBS_ALIAS &= _Hide:Relate:JOBS_ALIAS


NMSFTP_BC5:FilesInit PROCEDURE
  CODE
  _Hide:Relate:USMASSIG.Init
  _Hide:Relate:USUASSIG.Init
  _Hide:Relate:JOBSLOCK.Init
  _Hide:Relate:TRAHUBAC.Init
  _Hide:Relate:STOHISTE.Init
  _Hide:Relate:MODEXCHA.Init
  _Hide:Relate:JOBSOBF_ALIAS.Init
  _Hide:Relate:JOBPAYMT_ALIAS.Init
  _Hide:Relate:JOBS_ALIAS.Init


NMSFTP_BC5:DctKill PROCEDURE
  CODE
  _Hide:Relate:USMASSIG.Kill
  DISPOSE(_Hide:Relate:USMASSIG)
  _Hide:Relate:USUASSIG.Kill
  DISPOSE(_Hide:Relate:USUASSIG)
  _Hide:Relate:JOBSLOCK.Kill
  DISPOSE(_Hide:Relate:JOBSLOCK)
  _Hide:Relate:TRAHUBAC.Kill
  DISPOSE(_Hide:Relate:TRAHUBAC)
  _Hide:Relate:STOHISTE.Kill
  DISPOSE(_Hide:Relate:STOHISTE)
  _Hide:Relate:MODEXCHA.Kill
  DISPOSE(_Hide:Relate:MODEXCHA)
  _Hide:Relate:JOBSOBF_ALIAS.Kill
  DISPOSE(_Hide:Relate:JOBSOBF_ALIAS)
  _Hide:Relate:JOBPAYMT_ALIAS.Kill
  DISPOSE(_Hide:Relate:JOBPAYMT_ALIAS)
  _Hide:Relate:JOBS_ALIAS.Kill
  DISPOSE(_Hide:Relate:JOBS_ALIAS)


Hide:Relate:USMASSIG.Init PROCEDURE
  CODE
  _Hide:Access:USMASSIG.Init
  SELF.Init(Access:USMASSIG,1)


Hide:Access:USMASSIG.Init PROCEDURE

  CODE
  SELF.Initialized = False
  SELF.Buffer &= usm:Record
  SELF.FileNameValue = 'USMASSIG'
  SELF.SetErrors(GlobalErrors)
  SELF.File &= USMASSIG
  PARENT.Init
  Access:USMASSIG &= SELF


Hide:Access:USMASSIG.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:USMASSIG &= NULL                                  ! File manager has been destroyed to assign null, it is an error to reference this after this point


Hide:Access:USMASSIG.UseFile PROCEDURE(BYTE UseType = UseType:Uses)

ReturnValue          BYTE,AUTO

  CODE
  IF UseType ~= UseType:Initialize
    SELF.UseFile(UseType:Initialize)                       !Recursive call to ensure initialization takes place
  END
  IF UseType = UseType:Initialize AND ~SELF.Initialized
    SELF.InUseFile = True
    SELF.Init(USMASSIG,GlobalErrors)
    SELF.Create = 1
    SELF.LockRecover = 10
    SELF.AddKey(usm:Model_Number_Key,'By Model Number',0)
  END
                                                           !SELF.InUseFile will be set to False in PARENT
  ReturnValue = PARENT.UseFile(UseType)
  RETURN ReturnValue


Hide:Relate:USMASSIG.DeferedAddRelations PROCEDURE

  CODE
  DO AddRelations_1
  
  PARENT.DeferedAddRelations

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:USERS)


Hide:Relate:USMASSIG.Kill PROCEDURE

  CODE
  _Hide:Access:USMASSIG.Kill                               ! Kill the file manager
  PARENT.Kill
  Relate:USMASSIG &= NULL                                  ! Assign NULL to the RelationManager's FileManager reference, it is an error to reference this after this point
  DISPOSE(_Hide:Access:USMASSIG)                           ! destroy the file manager


Hide:Relate:USUASSIG.Init PROCEDURE
  CODE
  _Hide:Access:USUASSIG.Init
  SELF.Init(Access:USUASSIG,1)


Hide:Access:USUASSIG.Init PROCEDURE

  CODE
  SELF.Initialized = False
  SELF.Buffer &= usu:Record
  SELF.FileNameValue = 'USUASSIG'
  SELF.SetErrors(GlobalErrors)
  SELF.File &= USUASSIG
  PARENT.Init
  Access:USUASSIG &= SELF


Hide:Access:USUASSIG.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:USUASSIG &= NULL                                  ! File manager has been destroyed to assign null, it is an error to reference this after this point


Hide:Access:USUASSIG.UseFile PROCEDURE(BYTE UseType = UseType:Uses)

ReturnValue          BYTE,AUTO

  CODE
  IF UseType ~= UseType:Initialize
    SELF.UseFile(UseType:Initialize)                       !Recursive call to ensure initialization takes place
  END
  IF UseType = UseType:Initialize AND ~SELF.Initialized
    SELF.InUseFile = True
    SELF.Init(USUASSIG,GlobalErrors)
    SELF.Create = 1
    SELF.LockRecover = 10
    SELF.AddKey(usu:Unit_Type_Key,'By Unit Type',0)
    SELF.AddKey(usu:Unit_Type_Only_Key,'By Unit Type',0)
  END
                                                           !SELF.InUseFile will be set to False in PARENT
  ReturnValue = PARENT.UseFile(UseType)
  RETURN ReturnValue


Hide:Relate:USUASSIG.DeferedAddRelations PROCEDURE

  CODE
  DO AddRelations_1
  
  PARENT.DeferedAddRelations

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:USERS)

  SELF.AddRelation(Relate:UNITTYPE)


Hide:Relate:USUASSIG.Kill PROCEDURE

  CODE
  _Hide:Access:USUASSIG.Kill                               ! Kill the file manager
  PARENT.Kill
  Relate:USUASSIG &= NULL                                  ! Assign NULL to the RelationManager's FileManager reference, it is an error to reference this after this point
  DISPOSE(_Hide:Access:USUASSIG)                           ! destroy the file manager


Hide:Relate:JOBSLOCK.Init PROCEDURE
  CODE
  _Hide:Access:JOBSLOCK.Init
  SELF.Init(Access:JOBSLOCK,1)


Hide:Access:JOBSLOCK.Init PROCEDURE

  CODE
  SELF.Initialized = False
  SELF.Buffer &= lock:Record
  SELF.FileNameValue = 'JOBSLOCK'
  SELF.SetErrors(GlobalErrors)
  SELF.File &= JOBSLOCK
  PARENT.Init
  Access:JOBSLOCK &= SELF


Hide:Access:JOBSLOCK.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBSLOCK &= NULL                                  ! File manager has been destroyed to assign null, it is an error to reference this after this point


Hide:Access:JOBSLOCK.PrimeFields PROCEDURE

  CODE
  lock:DateLocked = Today()                                ! Assign initial field value
  lock:TimeLocked = Clock()                                ! Assign initial field value
  PARENT.PrimeFields


Hide:Access:JOBSLOCK.UseFile PROCEDURE(BYTE UseType = UseType:Uses)

ReturnValue          BYTE,AUTO

  CODE
  IF UseType ~= UseType:Initialize
    SELF.UseFile(UseType:Initialize)                       !Recursive call to ensure initialization takes place
  END
  IF UseType = UseType:Initialize AND ~SELF.Initialized
    SELF.InUseFile = True
    SELF.Init(JOBSLOCK,GlobalErrors)
    SELF.Create = 1
    SELF.LockRecover = 10
    SELF.AddKey(lock:RecordNumberKey,'By Record Number',1)
    SELF.AddKey(lock:JobNumberKey,'By Job Number',0)
  END
                                                           !SELF.InUseFile will be set to False in PARENT
  ReturnValue = PARENT.UseFile(UseType)
  RETURN ReturnValue


Hide:Relate:JOBSLOCK.DeferedAddRelations PROCEDURE

  CODE
  DO AddRelations_1
  
  PARENT.DeferedAddRelations

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Relate:JOBSLOCK.Kill PROCEDURE

  CODE
  _Hide:Access:JOBSLOCK.Kill                               ! Kill the file manager
  PARENT.Kill
  Relate:JOBSLOCK &= NULL                                  ! Assign NULL to the RelationManager's FileManager reference, it is an error to reference this after this point
  DISPOSE(_Hide:Access:JOBSLOCK)                           ! destroy the file manager


Hide:Relate:TRAHUBAC.Init PROCEDURE
  CODE
  _Hide:Access:TRAHUBAC.Init
  SELF.Init(Access:TRAHUBAC,1)


Hide:Access:TRAHUBAC.Init PROCEDURE

  CODE
  SELF.Initialized = False
  SELF.Buffer &= TRA1:Record
  SELF.FileNameValue = 'TRAHUBAC'
  SELF.SetErrors(GlobalErrors)
  SELF.File &= TRAHUBAC
  PARENT.Init
  Access:TRAHUBAC &= SELF


Hide:Access:TRAHUBAC.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:TRAHUBAC &= NULL                                  ! File manager has been destroyed to assign null, it is an error to reference this after this point


Hide:Access:TRAHUBAC.UseFile PROCEDURE(BYTE UseType = UseType:Uses)

ReturnValue          BYTE,AUTO

  CODE
  IF UseType ~= UseType:Initialize
    SELF.UseFile(UseType:Initialize)                       !Recursive call to ensure initialization takes place
  END
  IF UseType = UseType:Initialize AND ~SELF.Initialized
    SELF.InUseFile = True
    SELF.Init(TRAHUBAC,GlobalErrors)
    SELF.Create = 1
    SELF.LockRecover = 10
    SELF.AddKey(TRA1:RecordNoKey,'TRA1:RecordNoKey',1)
    SELF.AddKey(TRA1:HeadAccSubAccKey,'TRA1:HeadAccSubAccKey',0)
    SELF.AddKey(TRA1:HeadAccSubAccNameKey,'TRA1:HeadAccSubAccNameKey',0)
    SELF.AddKey(TRA1:HeadAccSubAccBranchKey,'TRA1:HeadAccSubAccBranchKey',0)
  END
                                                           !SELF.InUseFile will be set to False in PARENT
  ReturnValue = PARENT.UseFile(UseType)
  RETURN ReturnValue


Hide:Relate:TRAHUBAC.DeferedAddRelations PROCEDURE

  CODE
  DO AddRelations_1
  
  PARENT.DeferedAddRelations

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:TRADEACC)


Hide:Relate:TRAHUBAC.Kill PROCEDURE

  CODE
  _Hide:Access:TRAHUBAC.Kill                               ! Kill the file manager
  PARENT.Kill
  Relate:TRAHUBAC &= NULL                                  ! Assign NULL to the RelationManager's FileManager reference, it is an error to reference this after this point
  DISPOSE(_Hide:Access:TRAHUBAC)                           ! destroy the file manager


Hide:Relate:STOHISTE.Init PROCEDURE
  CODE
  _Hide:Access:STOHISTE.Init
  SELF.Init(Access:STOHISTE,1)


Hide:Access:STOHISTE.Init PROCEDURE

  CODE
  SELF.Initialized = False
  SELF.Buffer &= stoe:Record
  SELF.FileNameValue = 'STOHISTE'
  SELF.SetErrors(GlobalErrors)
  SELF.File &= STOHISTE
  PARENT.Init
  Access:STOHISTE &= SELF


Hide:Access:STOHISTE.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:STOHISTE &= NULL                                  ! File manager has been destroyed to assign null, it is an error to reference this after this point


Hide:Access:STOHISTE.UseFile PROCEDURE(BYTE UseType = UseType:Uses)

ReturnValue          BYTE,AUTO

  CODE
  IF UseType ~= UseType:Initialize
    SELF.UseFile(UseType:Initialize)                       !Recursive call to ensure initialization takes place
  END
  IF UseType = UseType:Initialize AND ~SELF.Initialized
    SELF.InUseFile = True
    SELF.Init(STOHISTE,GlobalErrors)
    SELF.Create = 1
    SELF.LockRecover = 10
    SELF.AddKey(stoe:RecordNumberKey,'By Record Number',1)
    SELF.AddKey(stoe:SHIRecordNumberKey,'stoe:SHIRecordNumberKey',0)
  END
                                                           !SELF.InUseFile will be set to False in PARENT
  ReturnValue = PARENT.UseFile(UseType)
  RETURN ReturnValue


Hide:Relate:STOHISTE.DeferedAddRelations PROCEDURE

  CODE
  DO AddRelations_1
  
  PARENT.DeferedAddRelations

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:STOHIST)


Hide:Relate:STOHISTE.Kill PROCEDURE

  CODE
  _Hide:Access:STOHISTE.Kill                               ! Kill the file manager
  PARENT.Kill
  Relate:STOHISTE &= NULL                                  ! Assign NULL to the RelationManager's FileManager reference, it is an error to reference this after this point
  DISPOSE(_Hide:Access:STOHISTE)                           ! destroy the file manager


Hide:Relate:MODEXCHA.Init PROCEDURE
  CODE
  _Hide:Access:MODEXCHA.Init
  SELF.Init(Access:MODEXCHA,1)


Hide:Access:MODEXCHA.Init PROCEDURE

  CODE
  SELF.Initialized = False
  SELF.Buffer &= moa:Record
  SELF.FileNameValue = 'MODEXCHA'
  SELF.SetErrors(GlobalErrors)
  SELF.File &= MODEXCHA
  PARENT.Init
  Access:MODEXCHA &= SELF


Hide:Access:MODEXCHA.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:MODEXCHA &= NULL                                  ! File manager has been destroyed to assign null, it is an error to reference this after this point


Hide:Access:MODEXCHA.UseFile PROCEDURE(BYTE UseType = UseType:Uses)

ReturnValue          BYTE,AUTO

  CODE
  IF UseType ~= UseType:Initialize
    SELF.UseFile(UseType:Initialize)                       !Recursive call to ensure initialization takes place
  END
  IF UseType = UseType:Initialize AND ~SELF.Initialized
    SELF.InUseFile = True
    SELF.Init(MODEXCHA,GlobalErrors)
    SELF.Create = 1
    SELF.LockRecover = 10
    SELF.AddKey(moa:RecordNumberKey,'moa:RecordNumberKey',1)
    SELF.AddKey(moa:AccountNumberKey,'By Account Number',0)
  END
                                                           !SELF.InUseFile will be set to False in PARENT
  ReturnValue = PARENT.UseFile(UseType)
  RETURN ReturnValue


Hide:Relate:MODEXCHA.DeferedAddRelations PROCEDURE

  CODE
  DO AddRelations_1
  
  PARENT.DeferedAddRelations

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:MODELNUM)


Hide:Relate:MODEXCHA.Kill PROCEDURE

  CODE
  _Hide:Access:MODEXCHA.Kill                               ! Kill the file manager
  PARENT.Kill
  Relate:MODEXCHA &= NULL                                  ! Assign NULL to the RelationManager's FileManager reference, it is an error to reference this after this point
  DISPOSE(_Hide:Access:MODEXCHA)                           ! destroy the file manager


Hide:Relate:JOBSOBF_ALIAS.Init PROCEDURE
  CODE
  _Hide:Access:JOBSOBF_ALIAS.Init
  SELF.Init(Access:JOBSOBF_ALIAS,1)
  SELF.SetAlias(Relate:JOBSOBF)


Hide:Access:JOBSOBF_ALIAS.Init PROCEDURE

  CODE
  SELF.Initialized = False
  SELF.Buffer &= jofali:Record
  SELF.AliasedFile &= Access:JOBSOBF                       !This is a File Alias, so assign aliased file manager
  SELF.FileNameValue = 'JOBSOBF_ALIAS'
  SELF.SetErrors(GlobalErrors)
  SELF.File &= JOBSOBF_ALIAS
  PARENT.Init
  Access:JOBSOBF_ALIAS &= SELF


Hide:Access:JOBSOBF_ALIAS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBSOBF_ALIAS &= NULL                             ! File manager has been destroyed to assign null, it is an error to reference this after this point


Hide:Access:JOBSOBF_ALIAS.UseFile PROCEDURE(BYTE UseType = UseType:Uses)

ReturnValue          BYTE,AUTO

  CODE
  IF UseType ~= UseType:Initialize
    SELF.UseFile(UseType:Initialize)                       !Recursive call to ensure initialization takes place
  END
  IF UseType = UseType:Initialize AND ~SELF.Initialized
    SELF.InUseFile = True
    SELF.Init(JOBSOBF_ALIAS,GlobalErrors)
    SELF.Create = 1
    SELF.LockRecover = 10
    SELF.AddKey(jofali:RecordNumberKey,'By Record Number',1)
    SELF.AddKey(jofali:RefNumberKey,'By Job Number',0)
    SELF.AddKey(jofali:StatusRefNumberKey,'By Job Number',0)
    SELF.AddKey(jofali:StatusIMEINumberKey,'By I.M.E.I. Number',0)
    SELF.AddKey(jofali:HeadAccountCompletedKey,'By Date Completed',0)
    SELF.AddKey(jofali:HeadAccountProcessedKey,'By Date Processed',0)
  END
                                                           !SELF.InUseFile will be set to False in PARENT
  ReturnValue = PARENT.UseFile(UseType)
  RETURN ReturnValue


Hide:Relate:JOBSOBF_ALIAS.DeferedAddRelations PROCEDURE

  CODE
  DO AddRelations_1
  
  PARENT.DeferedAddRelations

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS_ALIAS)


Hide:Relate:JOBSOBF_ALIAS.Kill PROCEDURE

  CODE
  _Hide:Access:JOBSOBF_ALIAS.Kill                          ! Kill the file manager
  PARENT.Kill
  Relate:JOBSOBF_ALIAS &= NULL                             ! Assign NULL to the RelationManager's FileManager reference, it is an error to reference this after this point
  DISPOSE(_Hide:Access:JOBSOBF_ALIAS)                      ! destroy the file manager


Hide:Relate:JOBPAYMT_ALIAS.Init PROCEDURE
  CODE
  _Hide:Access:JOBPAYMT_ALIAS.Init
  SELF.Init(Access:JOBPAYMT_ALIAS,1)
  SELF.SetAlias(Relate:JOBPAYMT)


Hide:Access:JOBPAYMT_ALIAS.Init PROCEDURE

  CODE
  SELF.Initialized = False
  SELF.Buffer &= jpt_ali:Record
  SELF.AliasedFile &= Access:JOBPAYMT                      !This is a File Alias, so assign aliased file manager
  SELF.FileNameValue = 'JOBPAYMT_ALIAS'
  SELF.SetErrors(GlobalErrors)
  SELF.File &= JOBPAYMT_ALIAS
  PARENT.Init
  Access:JOBPAYMT_ALIAS &= SELF


Hide:Access:JOBPAYMT_ALIAS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBPAYMT_ALIAS &= NULL                            ! File manager has been destroyed to assign null, it is an error to reference this after this point


Hide:Access:JOBPAYMT_ALIAS.UseFile PROCEDURE(BYTE UseType = UseType:Uses)

ReturnValue          BYTE,AUTO

  CODE
  IF UseType ~= UseType:Initialize
    SELF.UseFile(UseType:Initialize)                       !Recursive call to ensure initialization takes place
  END
  IF UseType = UseType:Initialize AND ~SELF.Initialized
    SELF.InUseFile = True
    SELF.Init(JOBPAYMT_ALIAS,GlobalErrors)
    SELF.Create = 1
    SELF.LockRecover = 10
    SELF.AddKey(jpt_ali:Record_Number_Key,'By Record Number',1)
    SELF.AddKey(jpt_ali:All_Date_Key,'By Date',0)
    SELF.AddKey(jpt_ali:Loan_Deposit_Key,'jpt_ali:Loan_Deposit_Key',0)
  END
                                                           !SELF.InUseFile will be set to False in PARENT
  ReturnValue = PARENT.UseFile(UseType)
  RETURN ReturnValue


Hide:Relate:JOBPAYMT_ALIAS.DeferedAddRelations PROCEDURE

  CODE
  DO AddRelations_1
  
  PARENT.DeferedAddRelations

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Relate:JOBPAYMT_ALIAS.Kill PROCEDURE

  CODE
  _Hide:Access:JOBPAYMT_ALIAS.Kill                         ! Kill the file manager
  PARENT.Kill
  Relate:JOBPAYMT_ALIAS &= NULL                            ! Assign NULL to the RelationManager's FileManager reference, it is an error to reference this after this point
  DISPOSE(_Hide:Access:JOBPAYMT_ALIAS)                     ! destroy the file manager


Hide:Relate:JOBS_ALIAS.Init PROCEDURE
  CODE
  _Hide:Access:JOBS_ALIAS.Init
  SELF.Init(Access:JOBS_ALIAS,1)
  SELF.SetAlias(Relate:JOBS)


Hide:Access:JOBS_ALIAS.Init PROCEDURE

  CODE
  SELF.Initialized = False
  SELF.Buffer &= job_ali:Record
  SELF.AliasedFile &= Access:JOBS                          !This is a File Alias, so assign aliased file manager
  SELF.FileNameValue = 'JOBS_ALIAS'
  SELF.SetErrors(GlobalErrors)
  SELF.File &= JOBS_ALIAS
  PARENT.Init
  Access:JOBS_ALIAS &= SELF


Hide:Access:JOBS_ALIAS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBS_ALIAS &= NULL                                ! File manager has been destroyed to assign null, it is an error to reference this after this point


Hide:Access:JOBS_ALIAS.UseFile PROCEDURE(BYTE UseType = UseType:Uses)

ReturnValue          BYTE,AUTO

  CODE
  IF UseType ~= UseType:Initialize
    SELF.UseFile(UseType:Initialize)                       !Recursive call to ensure initialization takes place
  END
  IF UseType = UseType:Initialize AND ~SELF.Initialized
    SELF.InUseFile = True
    SELF.Init(JOBS_ALIAS,GlobalErrors)
    SELF.Create = 1
    SELF.LockRecover = 10
    SELF.AddKey(job_ali:Ref_Number_Key,'By Job Number',1)
    SELF.AddKey(job_ali:Model_Unit_Key,'By Job Number',0)
    SELF.AddKey(job_ali:EngCompKey,'By Job Number',0)
    SELF.AddKey(job_ali:EngWorkKey,'By Job Number',0)
    SELF.AddKey(job_ali:Surname_Key,'By Surname',0)
    SELF.AddKey(job_ali:MobileNumberKey,'By Mobile Number',0)
    SELF.AddKey(job_ali:ESN_Key,'By E.S.N. / I.M.E.I.',0)
    SELF.AddKey(job_ali:MSN_Key,'By M.S.N.',0)
    SELF.AddKey(job_ali:AccountNumberKey,'By Account Number',0)
    SELF.AddKey(job_ali:AccOrdNoKey,'By Order Number',0)
    SELF.AddKey(job_ali:Model_Number_Key,'By Model Number',0)
    SELF.AddKey(job_ali:Engineer_Key,'By Engineer',0)
    SELF.AddKey(job_ali:Date_Booked_Key,'By Date Booked',0)
    SELF.AddKey(job_ali:DateCompletedKey,'By Date Completed',0)
    SELF.AddKey(job_ali:ModelCompKey,'By Completed Date',0)
    SELF.AddKey(job_ali:By_Status,'By Job Number',0)
    SELF.AddKey(job_ali:StatusLocKey,'By Job Number',0)
    SELF.AddKey(job_ali:Location_Key,'By Location',0)
    SELF.AddKey(job_ali:Third_Party_Key,'By Third Party',0)
    SELF.AddKey(job_ali:ThirdEsnKey,'By ESN',0)
    SELF.AddKey(job_ali:ThirdMsnKey,'By MSN',0)
    SELF.AddKey(job_ali:PriorityTypeKey,'By Priority',0)
    SELF.AddKey(job_ali:Unit_Type_Key,'By Unit Type',0)
    SELF.AddKey(job_ali:EDI_Key,'By Job Number',0)
    SELF.AddKey(job_ali:InvoiceNumberKey,'By Invoice_Number',0)
    SELF.AddKey(job_ali:WarInvoiceNoKey,'By Invoice Number',0)
    SELF.AddKey(job_ali:Batch_Number_Key,'By Job Number',0)
    SELF.AddKey(job_ali:Batch_Status_Key,'By Ref Number',0)
    SELF.AddKey(job_ali:BatchModelNoKey,'By Job Number',0)
    SELF.AddKey(job_ali:BatchInvoicedKey,'By Ref Number',0)
    SELF.AddKey(job_ali:BatchCompKey,'By Job Number',0)
    SELF.AddKey(job_ali:ChaInvoiceKey,'job_ali:ChaInvoiceKey',0)
    SELF.AddKey(job_ali:InvoiceExceptKey,'By Job Number',0)
    SELF.AddKey(job_ali:ConsignmentNoKey,'By Cosignment Number',0)
    SELF.AddKey(job_ali:InConsignKey,'By Consignment Number',0)
    SELF.AddKey(job_ali:ReadyToDespKey,'By Job Number',0)
    SELF.AddKey(job_ali:ReadyToTradeKey,'By Job Number',0)
    SELF.AddKey(job_ali:ReadyToCouKey,'By Job Number',0)
    SELF.AddKey(job_ali:ReadyToAllKey,'By Job Number',0)
    SELF.AddKey(job_ali:DespJobNumberKey,'By Job Number',0)
    SELF.AddKey(job_ali:DateDespatchKey,'By Job Number',0)
    SELF.AddKey(job_ali:DateDespLoaKey,'By Job Number',0)
    SELF.AddKey(job_ali:DateDespExcKey,'By Job Number',0)
    SELF.AddKey(job_ali:ChaRepTypeKey,'By Chargeable Repair Type',0)
    SELF.AddKey(job_ali:WarRepTypeKey,'By Warranty Repair Type',0)
    SELF.AddKey(job_ali:ChaTypeKey,'job_ali:ChaTypeKey',0)
    SELF.AddKey(job_ali:WarChaTypeKey,'job_ali:WarChaTypeKey',0)
    SELF.AddKey(job_ali:Bouncer_Key,'By Job Number',0)
    SELF.AddKey(job_ali:EngDateCompKey,'By Date Completed',0)
    SELF.AddKey(job_ali:ExcStatusKey,'By Job Number',0)
    SELF.AddKey(job_ali:LoanStatusKey,'By Job Number',0)
    SELF.AddKey(job_ali:ExchangeLocKey,'By Job Number',0)
    SELF.AddKey(job_ali:LoanLocKey,'By Job Number',0)
    SELF.AddKey(job_ali:BatchJobKey,'By Job Number',0)
    SELF.AddKey(job_ali:BatchStatusKey,'By Job Number',0)
  END
                                                           !SELF.InUseFile will be set to False in PARENT
  ReturnValue = PARENT.UseFile(UseType)
  RETURN ReturnValue


Hide:Relate:JOBS_ALIAS.DeferedAddRelations PROCEDURE

  CODE
  DO AddRelations_1
  
  PARENT.DeferedAddRelations

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBSOBF_ALIAS,RI:CASCADE,RI:CASCADE,jofali:RefNumberKey)
  SELF.AddRelationLink(job_ali:Ref_Number,jofali:RefNumber)

  SELF.AddRelation(Relate:BOUNCER,RI:CASCADE,RI:CASCADE,bou:Bouncer_Job_Only_Key)
  SELF.AddRelationLink(job_ali:Ref_Number,bou:Bouncer_Job_Number)

  SELF.AddRelation(Relate:REPAIRTY)


Hide:Relate:JOBS_ALIAS.Kill PROCEDURE

  CODE
  _Hide:Access:JOBS_ALIAS.Kill                             ! Kill the file manager
  PARENT.Kill
  Relate:JOBS_ALIAS &= NULL                                ! Assign NULL to the RelationManager's FileManager reference, it is an error to reference this after this point
  DISPOSE(_Hide:Access:JOBS_ALIAS)                         ! destroy the file manager

