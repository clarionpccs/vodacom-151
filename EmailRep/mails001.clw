

   MEMBER('mailshed.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('MAILS001.INC'),ONCE        !Local module procedure declarations
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

CurTime              TIME
MinTime              TIME
RunNoWait            LONG
AppWindow            WINDOW('Email Scheduler'),AT(,,83,20),FONT('Arial',8,,FONT:regular,CHARSET:ANSI),GRAY,DOUBLE
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = 1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:REPMAIL.Open
  SELF.FilesOpened = True
  OPEN(AppWindow)
  SELF.Opened=True
  AppWindow{PROP:ICONIZE} = TRUE
  !main loop
  LOOP
    Access:REPMAIL.CLEARKEY(rpm:Date_Key)
    SET(rpm:Date_Key,rpm:Date_Key)
    MinTime = 8640000
    RunNoWait = 0
    LOOP
      IF Access:REPMAIL.NEXT() THEN BREAK.
      IF TODAY() >= rpm:Next_Scheduled_Date THEN
         CurTime = CLOCK()
         IF CurTime < rpm:Next_Scheduled_Time THEN
            IF rpm:Next_Scheduled_Time - CurTime < MinTime THEN
               MinTime = rpm:Next_Scheduled_Time - CurTime
            END !IF
         ELSE
             RunNoWait = 1
         END !IF
      END !IF
    END !LOOP
    IF MinTime > 90000 THEN
       MinTime = 90000
    END !IF
    IF RunNoWait = 1 THEN
       RUN(CLIP(PATH()) & '\EmailRep.exe',1)
    ELSE
        CurTime = CLOCK()
        LOOP UNTIL CLOCK() > CurTime + MinTime
          YIELD()
        END !LOOP
        RUN(CLIP(PATH()) & '\EmailRep.exe',1)
    END !IF
  END !LOOP
  
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:REPMAIL.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue

