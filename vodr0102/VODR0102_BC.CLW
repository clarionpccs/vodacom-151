  MEMBER('vodr0102.clw')

  PRAGMA('define(init_priority=>3)')

  MAP
    MODULE('vodr0102_BC0.CLW')
vodr0102_BC0:DctInit             PROCEDURE
vodr0102_BC0:DctKill             PROCEDURE
vodr0102_BC0:FilesInit           PROCEDURE
    END
    MODULE('vodr0102_BC1.CLW')
vodr0102_BC1:DctInit             PROCEDURE
vodr0102_BC1:DctKill             PROCEDURE
vodr0102_BC1:FilesInit           PROCEDURE
    END
    MODULE('vodr0102_BC2.CLW')
vodr0102_BC2:DctInit             PROCEDURE
vodr0102_BC2:DctKill             PROCEDURE
vodr0102_BC2:FilesInit           PROCEDURE
    END
    MODULE('vodr0102_BC3.CLW')
vodr0102_BC3:DctInit             PROCEDURE
vodr0102_BC3:DctKill             PROCEDURE
vodr0102_BC3:FilesInit           PROCEDURE
    END
    MODULE('vodr0102_BC4.CLW')
vodr0102_BC4:DctInit             PROCEDURE
vodr0102_BC4:DctKill             PROCEDURE
vodr0102_BC4:FilesInit           PROCEDURE
    END
    MODULE('vodr0102_BC5.CLW')
vodr0102_BC5:DctInit             PROCEDURE
vodr0102_BC5:DctKill             PROCEDURE
vodr0102_BC5:FilesInit           PROCEDURE
    END
    MODULE('vodr0102_BC6.CLW')
vodr0102_BC6:DctInit             PROCEDURE
vodr0102_BC6:DctKill             PROCEDURE
vodr0102_BC6:FilesInit           PROCEDURE
    END
  END

DctInit PROCEDURE
  CODE
  vodr0102_BC0:DctInit
  vodr0102_BC1:DctInit
  vodr0102_BC2:DctInit
  vodr0102_BC3:DctInit
  vodr0102_BC4:DctInit
  vodr0102_BC5:DctInit
  vodr0102_BC6:DctInit
  vodr0102_BC0:FilesInit
  vodr0102_BC1:FilesInit
  vodr0102_BC2:FilesInit
  vodr0102_BC3:FilesInit
  vodr0102_BC4:FilesInit
  vodr0102_BC5:FilesInit
  vodr0102_BC6:FilesInit


DctKill PROCEDURE
  CODE
  vodr0102_BC0:DctKill
  vodr0102_BC1:DctKill
  vodr0102_BC2:DctKill
  vodr0102_BC3:DctKill
  vodr0102_BC4:DctKill
  vodr0102_BC5:DctKill
  vodr0102_BC6:DctKill

