

   MEMBER('vodr0057.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('VODR0001.INC'),ONCE        !Local module procedure declarations
                     END


SevenDayTATReport PROCEDURE                           !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::6:TAGFLAG          BYTE(0)
DASBRW::6:TAGDISPSTATUS    BYTE(0)
DASBRW::6:QUEUE           QUEUE
Pointer2                      LIKE(glo:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------
CommandLine STRING(255)
tmpPos      LONG

                     MAP   ! UpdateSummaryQueue
!After                       PROCEDURE (STRING, STRING), STRING
AppendString         PROCEDURE( STRING, STRING, STRING ),STRING

DateReturnedToRRC       PROCEDURE(), STRING
DateToString        PROCEDURE( DATE ), STRING
!Before                      PROCEDURE (STRING, STRING), STRING
GetDeliveryDateAtARC        PROCEDURE( LONG ), BYTE
GetHeadAccount PROCEDURE( STRING )
GetReturnDateToRRC        PROCEDURE( LONG, DATE ), DATE
GetSubAccount PROCEDURE( STRING )
HoursBetween                PROCEDURE  (TIME, TIME),LONG

LoadEngineer         PROCEDURE( STRING ), LONG ! BOOL
LoadEXCHANGE   PROCEDURE( LONG ), LONG ! BOOL
LoadLOCATLOG   PROCEDURE( LONG, *LONG ), LONG ! BOOL
LoadLOCATLOG_NewLocationKey   PROCEDURE( LONG, STRING, LONG ), LONG ! BOOL
LoadSUBTRACC   PROCEDURE( STRING ), LONG ! BOOL
LoadSUBTRACCByTradeAcc_Alias   PROCEDURE( STRING, *LONG ), LONG ! BOOL
LoadTRADEACC   PROCEDURE( STRING ), LONG ! BOOL
LoadWEBJOB          PROCEDURE( LONG ), LONG ! BOOL
NumberToColumn PROCEDURE( LONG ), STRING
UpdateEngineerQueue  PROCEDURE( STRING )
UpdateSummaryQueue   PROCEDURE( STRING, STRING )
WorkingDaysBetween          PROCEDURE  (DATE, DATE, BYTE, BYTE), LONG
WorkingHoursBetween         PROCEDURE  (DATE, DATE, TIME, TIME, LONG, LONG),LONG
WriteColumn PROCEDURE( STRING, LONG=False )
WriteDebug      PROCEDURE( STRING )
    END !MAP
!--------------------------------------------------------------
tmp:VersionNumber    STRING(30)
Automatic            BYTE
nbx                  SHORT
Param_Group          GROUP,PRE()
LOC:ReportType       STRING(20)
LOC:EndDate          DATE
LOC:StartDate        DATE
LOC:GreaterThanDays  LONG
                     END
Clipboard_Group      GROUP,PRE(clip)
OriginalValue        STRING(255)
Saved                BYTE
Value                CSTRING(200)
                     END
Excel_Group          GROUP,PRE()
Excel                SIGNED
excel:DateFormat     STRING('dd mmm yyyy')
excel:ColumnName     STRING(50)
excel:ColumnWidth    REAL
excel:CommentText    STRING(255)
excel:OperatingSystem REAL
excel:Visible        BYTE
                     END
Sheet_Group          GROUP,PRE(sheet)
TempLastCol          STRING(1)
HeadLastCol          STRING('C')
DataLastCol          STRING('O')
HeadSummaryRow       LONG(9)
DataSectionRow       LONG(9)
DataHeaderRow        LONG(11)
                     END
HeaderQueue          QUEUE,PRE(head)
ColumnName           STRING(30)
ColumnWidth          REAL
NumberFormat         STRING(20)
                     END
Local_Group          GROUP,PRE(LOC)
ApplicationName      STRING('ServiceBase')
ARCDateBooked        DATE
RRCDateBooked        DATE
ARCLocation          STRING(30)
RRCLocation          STRING(30)
RRCDateReturned      DATE
StatusReceivedAtRRC  STRING(30)
DesktopPath          STRING(255)
Filename             STRING(255)
Path                 STRING(255)
DespatchToCustomer   STRING(30)
ProgramName          STRING(100)
RepairCentreType     STRING(3)
SectionName          STRING(32)
Text                 STRING(255)
Username             STRING(100)
Version              STRING('3.1.0001')
WorkingHours         DECIMAL(10,4)
                     END
Progress_Group       GROUP,PRE()
Progress:Text        STRING(100)
RecordCount          LONG
                     END
SummaryQueue         QUEUE,PRE(sq)
Status               STRING(30)
Type                 STRING(10)
Count                LONG
                     END
EngineerQueue        QUEUE,PRE(engq)
UserCode             STRING(3)
Forename             STRING(30)
Surname              STRING(30)
JobsBooked           LONG
                     END
HeadAccount_Queue    QUEUE,PRE(haQ)
AccountNumber        STRING(15)
AccountName          STRING(30)
BranchIdentification STRING(2)
IncludeSaturday      LONG
IncludeSunday        LONG
                     END
SubAccount_Queue     QUEUE,PRE(saQ)
AccountNumber        STRING(15)
AccountName          STRING(30)
HeadAccountNumber    STRING(15)
HeadAccountName      STRING(30)
BranchIdentification STRING(2)
Count                LONG
                     END
Debug_Group          GROUP,PRE(debug)
Active               LONG
Count                LONG
                     END
JobNumber_Queue      QUEUE,PRE(jobnumQ)
JobNumber            LONG
HeadAccountNumber    STRING(15)
                     END
Misc_Group           GROUP,PRE()
DoAll                STRING(1)
GUIMode              BYTE
LocalHeadAccount     STRING(30)
LocalTag             STRING(1)
LocalTimeOut         LONG
Result               BYTE
                     END
Ignore_Completed     BYTE
BRW5::View:Browse    VIEW(TRADEACC)
                       PROJECT(tra:Account_Number)
                       PROJECT(tra:Company_Name)
                       PROJECT(tra:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
LocalTag               LIKE(LocalTag)                 !List box control field - type derived from local data
LocalTag_Icon          LONG                           !Entry's icon ID
tra:Account_Number     LIKE(tra:Account_Number)       !List box control field - type derived from field
tra:Company_Name       LIKE(tra:Company_Name)         !List box control field - type derived from field
tra:RecordNumber       LIKE(tra:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
MainWindow           WINDOW('Report'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(60,38,560,360),USE(?PanelMain),FILL(0D6EAEFH)
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(64,56,552,44),USE(?PanelTip),FILL(0D6EAEFH)
                       GROUP('Top Tip'),AT(68,60,544,36),USE(?GroupTip),BOXED,TRN
                       END
                       STRING(@s255),AT(72,76,496,),USE(SRN:TipText),TRN
                       BUTTON,AT(576,64,36,32),USE(?ButtonHelp),TRN,FLAT,ICON('F1Help.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('7 Day TAT Report Criteria'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(64,104,552,258),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Criteria'),USE(?Tab1)
                           BUTTON('&Rev tags'),AT(228,268,52,12),USE(?DASREVTAG),DISABLE,HIDE
                           BUTTON('sho&W tags'),AT(228,284,52,12),USE(?DASSHOWTAG),DISABLE,HIDE
                           BUTTON,AT(484,298),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                           STRING('Days > Than'),AT(250,132),USE(?StrDaysGreaterThan),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           SPIN(@n-14),AT(326,132,64,10),USE(LOC:GreaterThanDays),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ
                           CHECK('Ignore Completed Date'),AT(326,146),USE(Ignore_Completed),FONT('Tahoma',,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1','0')
                           STRING('Start Date'),AT(250,112),USE(?StrDaysGreaterThan:2),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@D6b),AT(326,112,64,10),USE(LOC:StartDate),FONT(,,,FONT:bold),COLOR(COLOR:White),REQ
                           BUTTON,AT(394,108),USE(?PopCalendarStart),FLAT,LEFT,ICON('lookupp.jpg')
                           CHECK('All Repair Centres'),AT(206,164),USE(DoAll),TRN,FONT('Tahoma',,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('Y','N')
                           CHECK('Show Excel'),AT(417,164),USE(excel:Visible),TRN,LEFT,FONT('Tahoma',,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),TIP('Tick To Show Excel During Report Creation.<13,10>Only Tick if you are having problems' &|
   ' <13,10>with Excel reports.'),VALUE('1','0')
                           LIST,AT(206,176,270,182),USE(?List),IMM,VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)FJ@s1@60L(2)|F~Account Number~@s15@120L(2)|F~Company Name~@s30@'),FROM(Queue:Browse)
                           BUTTON,AT(484,228),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(484,264),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                         END
                       END
                       BUTTON,AT(480,366),USE(?OkButton),TRN,FLAT,LEFT,TIP('Click to print report'),ICON('printp.jpg'),DEFAULT
                       BUTTON,AT(548,366),USE(?CancelButton),TRN,FLAT,LEFT,TIP('Click to close this form'),ICON('cancelp.jpg')
                       PROMPT('Report Version'),AT(68,376),USE(?ReportVersion),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                     END

!---   ProgressBar Declarations   --------------------------------------------------
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte
 
progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY,DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
!-----------------------------------------------------------------------------------
!---   Excel EQUATES   -------------------------------------------------------------
xlCalculationManual    EQUATE(0FFFFEFD9h) ! XlCalculation
xlCalculationAutomatic EQUATE(0FFFFEFF7h) ! XlCalculation
!----------------------------------------------------

!----------------------------------------------------
xlNone       EQUATE(0FFFFEFD2h)
xlContinuous EQUATE(        1 ) ! xlFillStyle
xlThin       EQUATE(        2 ) ! XlBorderWeight
!----------------------------------------------------

!----------------------------------------------------
xlAutomatic   EQUATE(0FFFFEFF7h) ! Constants.
xlSolid       EQUATE(        1 ) ! Constants.
xlLeft        EQUATE(0FFFFEFDDh) ! Constants.
xlRight       EQUATE(0FFFFEFC8h) ! Constants.
xlCenter      EQUATE(0FFFFEFF4h) ! Constants.
xlLastCell    EQUATE(       11 ) ! Constants.
xlTop         EQUATE(0FFFFEFC0h) ! Constants.
xlBottom      EQUATE(0FFFFEFF5h) ! Constants.

xlTopToBottom EQUATE(        1 ) ! Constants.
!----------------------------------------------------

!----------------------------------------------------
xlDiagonalDown     EQUATE( 5) ! XlBordersIndex
xlDiagonalUp       EQUATE( 6) ! XlBordersIndex
xlEdgeLeft         EQUATE( 7) ! XlBordersIndex
xlEdgeTop          EQUATE( 8) ! XlBordersIndex
xlEdgeBottom       EQUATE( 9) ! XlBordersIndex
xlEdgeRight        EQUATE(10) ! XlBordersIndex
xlInsideVertical   EQUATE(11) ! XlBordersIndex
xlInsideHorizontal EQUATE(12) ! XlBordersIndex
!----------------------------------------------------

!----------------------------------------------------
xlA1         EQUATE(        1 ) ! XlReferenceStyle
xlR1C1       EQUATE(0FFFFEFCAh) ! XlReferenceStyle
!----------------------------------------------------

!----------------------------------------------------
xlFilterCopy    EQUATE(2) ! XlFilterAction
xlFilterInPlace EQUATE(1) ! XlFilterAction
!----------------------------------------------------

!----------------------------------------------------
xlGuess EQUATE(0) ! XlYesNoGuess
xlYes   EQUATE(1) ! XlYesNoGuess
xlNo    EQUATE(2) ! XlYesNoGuess
!----------------------------------------------------

!----------------------------------------------------
xlAscending  EQUATE(1) ! XlSortOrder
xlDescending EQUATE(2) ! XlSortOrder
!----------------------------------------------------

!----------------------------------------------------
xlLandscape EQUATE(2) ! XlPageOrientation
xlPortrait  EQUATE(1) ! XlPageOrientation
!----------------------------------------------------

!----------------------------------------------------
xlDownThenOver EQUATE( 1 ) ! XlOrder
xlOverThenDown EQUATE( 2 ) ! XlOrder
!-----------------------------------------------------------------------------------
!---   MS Office EQUATES   ---------------------------------------------------------
!----------------------------------------------------
msoCTrue          EQUATE(        1 ) ! MsoTriState
msoFalse          EQUATE(        0 ) ! MsoTriState
msoTriStateMixed  EQUATE(0FFFFFFFEh) ! MsoTriState
msoTriStateToggle EQUATE(0FFFFFFFDh) ! MsoTriState
msoTrue           EQUATE(0FFFFFFFFh) ! MsoTriState
!----------------------------------------------------

!----------------------------------------------------
msoScaleFromBottomRight EQUATE(2) ! MsoScaleFrom
msoScaleFromMiddle      EQUATE(1) ! MsoScaleFrom
msoScaleFromTopLeft     EQUATE(0) ! MsoScaleFrom
!----------------------------------------------------

!----------------------------------------------------
msoPropertyTypeBoolean EQUATE(2) ! MsoDocProperties
msoPropertyTypeDate    EQUATE(3) ! MsoDocProperties
msoPropertyTypeFloat   EQUATE(5) ! MsoDocProperties
msoPropertyTypeNumber  EQUATE(1) ! MsoDocProperties
msoPropertyTypeString  EQUATE(4) ! MsoDocProperties
!----------------------------------------------------
!-----------------------------------------------------------------------------------
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW5                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW5::Sort0:Locator  StepLocatorClass                 !Default Locator
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::6:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW5.UpdateBuffer
   glo:Queue2.Pointer2 = tra:RecordNumber
   GET(glo:Queue2,glo:Queue2.Pointer2)
  IF ERRORCODE()
     glo:Queue2.Pointer2 = tra:RecordNumber
     ADD(glo:Queue2,glo:Queue2.Pointer2)
    LocalTag = 'Y'
  ELSE
    DELETE(glo:Queue2)
    LocalTag = ''
  END
    Queue:Browse.LocalTag = LocalTag
  IF (LocalTag = 'Y')
    Queue:Browse.LocalTag_Icon = 2
  ELSE
    Queue:Browse.LocalTag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW5.Reset
  FREE(glo:Queue2)
  LOOP
    NEXT(BRW5::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue2.Pointer2 = tra:RecordNumber
     ADD(glo:Queue2,glo:Queue2.Pointer2)
  END
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue2)
  BRW5.Reset
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::6:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue2)
    GET(glo:Queue2,QR#)
    DASBRW::6:QUEUE = glo:Queue2
    ADD(DASBRW::6:QUEUE)
  END
  FREE(glo:Queue2)
  BRW5.Reset
  LOOP
    NEXT(BRW5::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::6:QUEUE.Pointer2 = tra:RecordNumber
     GET(DASBRW::6:QUEUE,DASBRW::6:QUEUE.Pointer2)
    IF ERRORCODE()
       glo:Queue2.Pointer2 = tra:RecordNumber
       ADD(glo:Queue2,glo:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASSHOWTAG Routine
   CASE DASBRW::6:TAGDISPSTATUS
   OF 0
      DASBRW::6:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::6:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::6:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW5.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------
OKButtonPressed             ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        CancelPressed = False
        debug:Active = True
        !-----------------------------------------------------------------
        SETCURSOR(CURSOR:Wait)

        DO ProgressBar_Setup
        DO XL_Setup
        !-----------------------------------------------------------------

        !-----------------------------------------------
        ! 23 Oct 2002 John
        ! R003, berrjo :Insert the report exe name EG VODR56.V01
        ! Do not attach the EXE extenstion but show the version number.  Increment the version number each time changed.
        ! Make this standard on all reports sent for correction.
        ! LOC:Version STRING(8) 3.1.0000 3.1 to match sevicebase version (and dictionary) .0000 increment before each compile
        !
        !LOC:Version = '3.1.0004'
        !-----------------------------------------------

        !-----------------------------------------------------------------
        ! 15 Oct 2002 John
        ! From Joe change report name from ARC for all trade accounts to match their role (arc/rrc)
        !
        !LOC:ProgramName        = 'ARC ' & CLIP(LOC:GreaterThanDays) & ' Day Turnaround Time Progress Report'
        !-----------------------------------------------------------------

        !-----------------------------------------------------------------
        ! SB2KDef.ini
        ![RRC]
        !- LOCATION ------------------------------------------------------
        ! RRCLocation            = AT FRANCHISE
        ! InTransit              = IN-TRANSIT TO ARC
        ! ARCLocation            = RECEIVED AT ARC
        ! RTMLocation            = SENT TO 3RD PARTY
        ! DespatchToCustomer     = DESPATCHED
        !- STATUS --------------------------------------------------------
        ! StatusSendToARC        = 450 SEND TO ARC
        ! StatusDespatchedToARC  = 451 DESPATCHED TO ARC
        ! StatusReceivedAtARC    = 452 RECEIVED AT ARC
        ! StatusSendToRRC        = 453 SEND TO RRC
        ! StatusDespatchedToRRC  = 454 DESPATCHED TO RRC
        ! StatusReceivedAtRRC    = 455 RECEIVED AT RRC
        ! InTransitRRC           = IN-TRANSIT TO RRC
        ! StatusARCReceivedQuery = 456 RECEIVED AT ARC (QUERY)
        ! StatusRRCReceivedQuery = 457 RECEIVED AT RRC (QUERY)
        !
        !LOC:StartDate          = TODAY() - LOC:GreaterThanDays
        LOC:EndDate             = TODAY() - LOC:GreaterThanDays ! 15 Oct 2002 John For use by FillJobNumberQueue
        LOC:ARCLocation         = GETINI('RRC',         'ARCLocation',     'RECEIVED AT ARC', '.\SB2KDEF.INI') ! [RRC]ARCLocation=RECEIVED AT ARC
        LOC:RRCLocation         = GETINI('RRC',         'RRCLocation',        'AT FRANCHISE', '.\SB2KDEF.INI') ! [RRC]RRCLocation=AT FRANCHISE
        LOC:StatusReceivedAtRRC = GETINI('RRC', 'StatusReceivedAtRRC', '455 RECEIVED AT RRC', '.\SB2KDEF.INI') ! [RRC]StatusReceivedAtRRC=455 RECEIVED AT RRC
        !-----------------------------------------------------------------

        !-----------------------------------------------------------------
        ! 15 Oct 2002 John
        ! Exclude if location indicates the unit is being passed back to customer
        !
        LOC:DespatchToCustomer = GETINI('RRC', 'DespatchToCustomer',      'DESPATCHED', '.\SB2KDEF.INI') ! [RRC]DespatchToCustomer=DESPATCHED
        !-----------------------------------------------------------------

        !-----------------------------------------------------------------
        ! 9 Oct 2002 John Comented out, Using "TODAY() - LOC:GreaterThanDays" to reduce workload.
        !   This code is redundant as :-
        !       1) LOC:StartDate wasn't used
        !       2) Returns earliest job:Date_Booked so is equivalent as searching on "" ie (all Jobs)
        !
!        Access:JOBS.CLEARKEY(job:Date_Booked_Key)
!        !   job:Date_Booked = ''
!        SET(job:Date_Booked_Key,job:Date_Booked_Key)
!        IF Access:JOBS.NEXT() = Level:Benign
!            LOC:StartDate = job:Date_Booked
!        END !IF
        !-----------------------------------------------------------------
        !DO FillJobNumberQueue
        nbx = 0

        If Automatic = True
            !Get account criteria for automated ini  (DBH: 06-05-2004)
            Free(glo:Queue2)
            Set(tra_ali:Account_Number_Key)
            Loop
                If Access:TRADEACC_ALIAS.NEXT()
                   Break
                End !If
                If tra_ali:BranchIdentification = ''
                    Cycle
                End !If tra:BranchIdentification = ''

                If GETINI('TAT',tra_ali:Account_Number,,CLIP(Path()) & '\REPAUTO.INI') = 1
                    glo:Pointer2 = tra_ali:RecordNumber
                    Add(glo:Queue2)
                End !If GETINI('TAT','tra:Account_Number',,CLIP(Path()) & '\REPAUTO.INI') = 1
            End !Loop

        End !If Automatic = True

        Access:TRADEACC_ALIAS.CLEARKEY(tra_ali:Account_Number_Key)
        SET(tra_ali:Account_Number_Key,tra_ali:Account_Number_Key)

        LOOP WHILE Access:TRADEACC_ALIAS.NEXT() = Level:Benign
            IF CancelPressed
                BREAK
            END !IF

            CASE tra_ali:Account_Number
            OF LocalHeadAccount
                LOC:RepairCentreType = 'ARC'
                !MESSAGE('ARC')
            OF 'XXXRRC' ! Is this a Vodacom exclusive ?
                CYCLE

            ELSE
                IF tra_ali:RemoteRepairCentre = 0
                    CYCLE
                END !IF

                LOC:RepairCentreType = 'RRC'

            END !CASE

            !-----------------------------------------------------------------
            ! 15 Oct 2002 John
            ! From Joe relayed by Ash 14:50 drop the ARC/RRC from title of sheet.
            !
            ! 15 Oct 2002 John
            ! From Joe change report name from ARC for all trade accounts to match their role (arc/rrc)
            !
            !LOC:ProgramName = LOC:RepairCentreType & ' ' & CLIP(LOC:GreaterThanDays) & ' Day Turnaround Time Progress Report'
            LOC:ProgramName = CLIP(LOC:GreaterThanDays) & ' Day Turnaround Time Progress Report'
            !-----------------------------------------------------------------

! STU's code
!            IF tra_ali:Account_Number <> LocalHeadAccount THEN
!                IF tra_ali:RemoteRepairCentre = 0 THEN CYCLE.
!            END !IF
!
!            IF tra_ali:Account_Number = 'XXXRRC' THEN CYCLE. ! Is this a Vodacom exclusive ?
            !-----------------------------------------------------------------

            IF DoAll <> 'Y' THEN
                glo:Queue2.Pointer2 = tra_ali:RecordNumber
                GET(glo:Queue2,glo:Queue2.Pointer2)
                IF ERROR() THEN CYCLE.
            END !IF

            LOC:FileName = ''
            DO ExportSetup
            IF LOC:FileName = '' THEN CYCLE.

            IF Automatic = TRUE
              IF tra_ali:EmailAddress = ''
                CYCLE
              END
            END
            nbx = 1

            DO ExportBody
            DO ExportFinalize
        END !LOOP
        !-----------------------------------------------------------------
        DO XL_Finalize

        DO ProgressBar_Finalise

        SETCURSOR()
        !-----------------------------------------------------------------
        POST(Event:CloseWindow)

        !    IF Option1 = 1 !'Summary'
        !        Summary  (LOC:StartDate, LOC:EndDate)
        !    ELSE
        !        Detailed (LOC:StartDate, LOC:EndDate)
        !    END !IF
        !-----------------------------------------------------------------
    EXIT
!--------------------------------------------------------------
ExportSetup                             ROUTINE
    DATA
temp DATE
    CODE
        !-----------------------------------------------------------------
        IF CLIP(LOC:FileName) = ''
            DO LoadFileDialog
        END !IF

        IF LOC:FileName = ''


            CancelPressed = True

            EXIT
        END !IF LOC:FileName = ''

        IF LOC:FileName <> '' THEN
           IF LOWER(RIGHT(LOC:FileName, 4)) <> '.xls'
              LOC:FileName = CLIP(LOC:FileName) & '.xls'
           END !IF
        END !IF
        !-----------------------------------------------------------------
        DO XL_AddWorkbook
        DO InitColumns
        !-----------------------------------------------------------------
    EXIT

ExportBody                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        DO CreateTitleSheet
        DO CreateDataSheet
        DO WriteHeadSummary
        !-----------------------------------------------------------------
    EXIT
ExportFinalize                          ROUTINE
    DATA
FilenameLength LONG
StartAt        LONG
SUBLength      LONG
    CODE
        !-----------------------------------------------------------------
        DO ResetClipboard

        IF CancelPressed
            DO XL_DropAllWorksheets
            DO XL_Finalize

            EXIT
        END !IF
        !-----------------------------------------------------------------


        !-------------------------------
        ! 10 Oct 2002 John
        ! 1) Remove Summary Sheet Completely
        Excel{'Sheets("Sheet1").Select'}
        IF Excel{'ActiveSheet.Name'} = 'Sheet1'
            Excel{'ActiveWindow.SelectedSheets.Delete'}
        END !IF
        !-------------------------------


        Excel{'Sheets("Sheet3").Select'}
        Excel{'ActiveWindow.SelectedSheets.Delete'}

        Excel{'Sheets("Summary").Select'}
        Excel{'Range("A1").Select'}

        Excel{'Application.ActiveWorkBook.SaveAs("' & LEFT(CLIP(LOC:FileName)) & '")'}
        Excel{'Application.ActiveWorkBook.Close()'}
        !-----------------------------------------------------------------
!        IF MATCH(LOC:Filename, CLIP(LOC:DesktopPath) & '*')
!            FilenameLength = LEN(CLIP(LOC:Filename   ))
!            StartAt        = LEN(CLIP(LOC:DesktopPath)) + 1
!            SUBLength      = FilenameLength - StartAt + 1
!
!            Case MessageEx('Export Completed.'                                & |
!                           '<13,10>'                                          & |
!                           '<13,10>Spreadsheet saved to your Desktop'         & |
!                           '<13,10>'                                          & |
!                           '<13,10>' & SUB(LOC:Filename, StartAt, SUBLength),   |
!                         LOC:ApplicationName,                                   |
!                         'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
!            Of 1 ! &OK Button
!            End!Case MessageEx
!        ELSE
!            Case MessageEx('Export Completed.'                        & |
!                           '<13,10>'                                  & |
!                           '<13,10>Spreadsheet saved to '             & |
!                           '<13,10>'                                  & |
!                           '<13,10>' & CLIP(LOC:Filename),              |
!                         LOC:ApplicationName,                           |
!                         'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
!            Of 1 ! &OK Button
!            End!Case MessageEx
!        END !IF
        !-----------------------------------------------------------------
    EXIT
!--------------------------------------------------------------
CreateTitleSheet                            ROUTINE
    DATA
    CODE
        !-------------------------------
        ! 10 Oct 2002 John
        ! 1) Remove Summary Sheet Completely
        EXIT
        !-------------------------------


        !-----------------------------------------------------------------
        Excel{'ActiveWorkBook.Sheets("Sheet1").Select'}
        !-----------------------------------------------------------------
        LOC:Text          = 'Summary'
        sheet:TempLastCol = sheet:HeadLastCol

        DO CreateWorksheetHeader
        !-----------------------------------------------------------------
        DO XL_SetWorksheetPortrait

        Excel{'ActiveSheet.Columns("A:A").ColumnWidth'}                         = 35
        Excel{'ActiveSheet.Columns("B:B").ColumnWidth'}                         = 35
        Excel{'ActiveSheet.Columns("C:' & sheet:HeadLastCol & '").ColumnWidth'} = 12
        !-----------------------------------------------------------------
        Excel{'Range("A' & sheet:HeadSummaryRow & '").Select'}
            DO XL_SetBold
            Excel{'ActiveCell.Formula'} = 'Summary'

        Excel{'Range("A' & sheet:HeadSummaryRow & ':' & sheet:HeadLastCol & sheet:HeadSummaryRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------------------------
        ! Prepare For summary details
        !
        Excel{'Range("A' & sheet:HeadSummaryRow+1 & '").Select'}
        !-----------------------------------------------------------------
    EXIT
CreateDataSheet                                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF

        FREE(EngineerQueue)
        CLEAR(EngineerQueue)

        FREE(SummaryQueue)
        CLEAR(SummaryQueue)

        FREE(SubAccount_Queue)
        CLEAR(SubAccount_Queue)
        !-----------------------------------------------------------------
        DO XL_AddSheet
            DO XL_SetWorksheetLandscape
        !-----------------------------------------------------------------
        LOC:SectionName   = 'Checking ' & CLIP(tra_ali:Account_Number) ! 'Detailed'
        excel:CommentText = ''
        sheet:TempLastCol = sheet:DataLastCol

        DO CreateSectionHeader
        !------------------------------------------
        ! AccountNumberKey KEY( job:Account_Number, job:Ref_Number ),DUP,NOCASE
        ! Date_Booked_Key  KEY( job:date_booked                    ),DUP,NOCASE
        ! DateCompletedKey KEY( job:Date_Completed                 ),DUP,NOCASE
        !
        Access:JOBS.ClearKey(job:Date_Booked_Key)
            job:Date_Booked = LOC:StartDate
        SET(job:Date_Booked_Key,job:Date_Booked_Key)
        !------------------------------------------
        Progress:Text    = CLIP(LOC:SectionName)
        RecordsToProcess = RECORDS(Jobs)

        DO ProgressBar_LoopPre
        !-----------------------------------------------------------------
        LOOP UNTIL Access:JOBS.NEXT()
            !-------------------------------------------------------------
            DO ProgressBar_Loop

            IF CancelPressed
                BREAK
            END !IF
            !-------------------------------------------------------------
            DO WriteColumns
            !-------------------------------------------------------------
        END !LOOP

        DO ProgressBar_LoopPost
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        DO WriteDataSummary
        !-----------------------------------------------------------------
    EXIT
!--------------------------------------------------------------
CreateSectionHeader                                        ROUTINE
    DATA
LastColumn STRING(1)
    CODE
        !-----------------------------------------------       
        LOC:Text = CLIP(LOC:SectionName)

        DO CreateWorksheetHeader
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataSectionRow & ':' & sheet:DataLastCol & sheet:DataSectionRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder


        !-----------------------------------------------
        ! 15 Oct 2002 John
        ! Remove "Section Name:", "Detailed"
        ! Keep sheet name as "Detailed"
        !
!        Excel{'Range("A' & sheet:DataSectionRow & '").Select'}
!            Excel{'ActiveCell.Formula'}         = 'Section Name:'
!            DO XL_ColRight
!                Excel{'ActiveCell.Formula'}     = CLIP(LOC:SectionName)
!                DO XL_SetBold
!                DO XL_HorizontalAlignmentLeft
        !-----------------------------------------------


        Excel{'Range("A' & sheet:DataSectionRow & '").Select'} ! was E
            Excel{'ActiveCell.Formula'}         = 'Total Records:'
            DO XL_HorizontalAlignmentRight
            DO XL_ColRight
                Excel{'ActiveCell.Formula'}     = 0
                DO XL_HorizontalAlignmentLeft
        !-----------------------------------------------


        !-----------------------------------------------
        DO SetColumns

        Excel{'Range("A' & sheet:DataHeaderRow+1 & '").Select'}
        !-----------------------------------------------
    EXIT
CreateWorksheetHeader                                                               ROUTINE
    DATA
CurrentRow LONG(3)
    CODE
        !-----------------------------------------------       
        Excel{'ActiveSheet.Name'} = CLIP(tra_ali:Account_Number)
        !-----------------------------------------------       
        Excel{'Range("A1").Select'}
            Excel{'ActiveCell.Formula'}   = CLIP(LOC:ProgramName) & ' ' & CLIP(tra_ali:Account_Number) 
            DO XL_SetBold
            Excel{'ActiveCell.Font.Size'} = 16

        Excel{'Range("D3").Select'}
            Excel{'ActiveCell.Formula'}   = Clip(tmp:VersionNumber)
            DO XL_SetBold
            Excel{'ActiveCell.Font.Size'} = 8


        Excel{'Range("A1:' & sheet:TempLastCol & '1").Select'}
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------
        Excel{'Range("A' & CurrentRow & ':' & sheet:TempLastCol & '3").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A' & CurrentRow & '").Select'}
            Excel{'ActiveCell.Formula'}          = 'Criteria'
            DO XL_SetBold


        !-----------------------------------------------
        ! 15 Oct 2002 John
        ! To speed up report generation add criteria to run the report from
        !   a chosen date
        !
        ! 8 Oct 2002 John
        ! JOE: Exclude" Date From" and "Date To". 
        ! The "Created By" Name should be the users full name
        !
        CurrentRow += 1
           Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.FormulaR1C1'}      = 'Start Date:'
               DO XL_ColRight
                   DO XL_FormatDate
                   DO XL_HorizontalAlignmentLeft
                   Excel{'ActiveCell.Formula'}      = DateToString(LOC:StartDate)
        !-----------------------------------------------


        !-----------------------------------------------
        ! 8 Oct 2002 John
        ! JOE: Exclude" Date From" and "Date To". 
        ! The "Created By" Name should be the users full name
        !
        !CurrentRow += 1
        !    Excel{'Range("A' & CurrentRow & '").Select'}
        !        Excel{'ActiveCell.FormulaR1C1'}      = 'End Date:'
        !        DO XL_ColRight
        !            DO XL_FormatDate
        !            DO XL_HorizontalAlignmentLeft
        !            Excel{'ActiveCell.Formula'}      = LEFT(FORMAT(LOC:EndDate, @D8))
        !-----------------------------------------------


        !-----------------------------------------------
        ! 8 Oct 2002 John
        ! JOE: Exclude" Date From" and "Date To". 
        ! The "Created By" Name should be the users full name
        ! Answer: LOC:UserName has full name unless use:Surname & use:Forename are blank
        !
        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.Formula'}          = 'Created By:'
                DO XL_ColRight
                    Excel{'ActiveCell.Formula'}      = LOC:UserName
        !-----------------------------------------------

        CurrentRow += 1
            Excel{'Range("A' & CurrentRow & '").Select'}
                Excel{'ActiveCell.Formula'}          = 'Date Created:'
                DO XL_ColRight
                    DO XL_FormatDate
                    DO XL_HorizontalAlignmentLeft
                    Excel{'ActiveCell.Formula'}      = DateToString(TODAY())

        Excel{'Range("A4:' & sheet:TempLastCol & CurrentRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------
        sheet:DataSectionRow = CurrentRow           + 2
        sheet:DataHeaderRow  = sheet:DataSectionRow + 2
        sheet:HeadSummaryRow = sheet:DataSectionRow
        !-----------------------------------------------
    EXIT
!--------------------------------------------------------------
SetColumns                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataHeaderRow & ':' & sheet:DataLastCol & sheet:DataHeaderRow & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & sheet:DataHeaderRow  & '").Select'}
        !-----------------------------------------------
        LOOP x# = 1 TO RECORDS(HeaderQueue)
            GET(HeaderQueue, x#)

            Excel{'ActiveCell.Formula'}         = head:ColumnName
                Excel{'ActiveCell.ColumnWidth'} = head:ColumnWidth
                DO XL_ColRight
        END !LOOP
        !-----------------------------------------------
        Excel{'Range("A' & sheet:DataHeaderRow+1  & '").Select'}
        Excel{'ActiveWindow.FreezePanes'} = True
        !-----------------------------------------------
    EXIT
InitColumns                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        FREE(HeaderQueue)

        head:ColumnName       = 'SB Job Number'
            head:ColumnWidth  = 12.00
            head:NumberFormat = '###0'
            ADD(HeaderQueue)

        !-----------------------------------------------
        ! 8 Oct 2002 John
        !ANKE: Should this not just be called "Branch Number"?
        !   Joe: This should be column B
        ! Renamed from "Franchise Branch Number"
        ! 9 Oct 2002 John, Changed back
        head:ColumnName       = 'Franchise Branch Number'
            head:ColumnWidth  = 12.00
            head:NumberFormat = '00'
            ADD(HeaderQueue)
        !-----------------------------------------------

        !-----------------------------------------------
        ! ANKE: Should this not just be called "Branch Job Number"? May cause some confusion
        !   Joe: Also make this column c same as all the other reports.
        ! Moved to Column C, left name as Ash checking with Joe as he was told all
        !   should read "Franchise Job Number"
        head:ColumnName       = 'Franchise Job Number'
            head:ColumnWidth  = 12.00
            head:NumberFormat = '###0'
            ADD(HeaderQueue)
        !-----------------------------------------------

        head:ColumnName       = 'RRC Date Received'
            head:ColumnWidth  = 12.00
            head:NumberFormat = excel:DateFormat
            ADD(HeaderQueue)

        head:ColumnName       = 'ARC Date Received'
            head:ColumnWidth  = 12.00
            head:NumberFormat = excel:DateFormat
            ADD(HeaderQueue)


        !-------------------------------
        ! 10 Oct 2002 John
        ! 2) If report is run for a _RRC_ then the column "Days Since Receipt At ARC" should
        !       be renamed to "Days Since Receipt At RRC" and should show number of days
        !       since the job was received at _RRC_.
        !
        IF LOC:RepairCentreType = 'ARC'
            head:ColumnName   = 'Days Since Receipt At ARC'
        ELSE
            head:ColumnName   = 'Days Since Receipt At RRC'
        END !IF
            head:ColumnWidth  = 12.00
            head:NumberFormat = '###0'
            ADD(HeaderQueue)
        !-------------------------------


        head:ColumnName       = 'Date Job Completed'
            head:ColumnWidth  = 12.00
            head:NumberFormat = excel:DateFormat
            ADD(HeaderQueue)


        !-------------------------------
        ! 23 Oct 2002 John
        ! R002, MICHALAN: Include a new column indicating "Date Received back at RRC" if repaired and completed at ARC
        head:ColumnName       = 'Date Returned To RRC'
            head:ColumnWidth  = 12.00
            head:NumberFormat = excel:DateFormat
            ADD(HeaderQueue)
        !-------------------------------


        ! DO WriteColumn_Manufacturer
        head:ColumnName       = 'Make'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)


        ! DO WriteColumn_ModelNumber
        head:ColumnName       = 'Model'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        ! DO WriteColumn_TradeAccountNumber
        head:ColumnName       = 'Account'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        ! DO WriteColumn_TradeAccountName
        head:ColumnName       = 'Name'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        ! DO WriteColumn_ESN                    ! IMEI
        head:ColumnName       = 'IMEI'
            head:ColumnWidth  = 20.00
            head:NumberFormat = '###############'
            ADD(HeaderQueue)

        !-----------------------------------------------
        ! 23 Oct 2002 John
        ! R002, MICHALAN: Insert a new column indicating the location of the job.
        head:ColumnName       = 'Location'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)
        !-----------------------------------------------

        ! DO WriteColumn_CurrentStatus_Job      ! Current  Status
        head:ColumnName       = 'Current Status'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        ! DO WriteColumn_CurrentStatus_Exchange ! Exchange Status
        head:ColumnName       = 'Exchange Status'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        ! DO WriteColumn_CurrentStatus_Loan     ! Loan     Status
        head:ColumnName       = 'Loan Status'
            head:ColumnWidth  = 20.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        ! DO WriteColumn_Engineer
        head:ColumnName       = 'Engineer'
            head:ColumnWidth  = 15.00
            head:NumberFormat = chr(64) ! Text (AT) Sign
            ADD(HeaderQueue)

        ! Insert --- New columns (DBH: 25/02/2009) #10591
        ! Warranty Job,
        head:ColumnName         = 'Warranty'
        head:ColumnWidth        = 15.00
        head:NumberFormat       = chr(64)
        add(headerQueue)

        ! Third Party Site
        head:ColumnName         = 'Third Party Site'
        head:ColumnWidth        = 20.00
        head:NumberFormat       = chr(64)
        add(headerQueue)

        ! Days Since Quote Accepted
        head:ColumnName         = 'Days Since Quote Accepted'
        head:ColumnWidth        = 20.00
        head:NumberFormat       = chr(64)
        add(headerQueue)
        ! end --- (DBH: 25/02/2009) #10591

        !-----------------------------------------------
        sheet:DataLastCol = NumberToColumn(RECORDS(HeaderQueue))
        !-----------------------------------------------
    EXIT


!        head:ColumnName       = 'zzz'
!            head:ColumnWidth  = 11.00
!            head:NumberFormat = '###0'
!            head:NumberFormat = '#,##0.00'
!            head:NumberFormat = '0'
!            head:NumberFormat = '0000'
!            head:NumberFormat = excel:DateFormat
!            head:NumberFormat = chr(64) ! Text (AT) Sign
!            ADD(HeaderQueue)
FormatColumns                                                   ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF RecordCount < 1
            EXIT
        END !IF
        !-----------------------------------------------
        LOOP x# = 1 TO RECORDS(HeaderQueue)
            !-------------------------------------------
            GET(HeaderQueue, x#)
            IF ERRORCODE()
                CYCLE
            END !IF

            Excel{'Range("' & CHR(64+x#) & sheet:DataHeaderRow+1 & ':' & CHR(64+x#) & sheet:DataHeaderRow+RecordCount & '").Select'}
                Excel{'Selection.NumberFormat'} = head:NumberFormat
                !DO XL_HighLight ! DEBUG
                DO XL_HorizontalAlignmentRight
            !-------------------------------------------
        END !LOOP
        !-----------------------------------------------
    EXIT
WriteColumns                                                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !WriteDebug('WriteColumns(JOB="' & job:Ref_Number & '")')
        !-----------------------------------------------


        !-----------------------------------------------
        IF     LOC:RepairCentreType = 'RRC'
            !-------------------------------------------
            IF GetDeliveryDateAtARC(job:Ref_Number)
                ! RRC -> ARC  <====================(1)
                !
                !Message('RRC?????')
                EXIT
            END !IF
            !-------------------------------------------
            ! RRC -> RRC
            !
            LOC:RRCDateBooked   = job:Date_Booked
            LOC:ARCDateBooked   = ''
            LOC:RRCDateReturned = ''

            DO WriteColumns_RRC
            !-------------------------------------------
        ELSE
            !-------------------------------------------
            ! LOC:RepairCentreType = 'ARC'
            !-------------------------------------------
            IF NOT LoadWEBJOB(job:Ref_Number)
                ! ERROR

                EXIT
            END !IF

            GetHeadAccount(wob:HeadAccountNumber)
            !-------------------------------------------

            IF HaQ:AccountNumber = LocalHeadAccount
                !---------------------------------------
                ! ARC -> ARC
                !---------------------------------------
                ! 22 Oct 2002 John
                ! If the job was booked at the ARC, the RRC date received should be blank.
                ! The date displayed here is the booking date for the job not the date it was received at the RRC.
                ! In fact none of these jobs were booked at the RRC therefore this column [RRC Date Received-john] should be blank.
                !
                LOC:RRCDateBooked   = ''
                LOC:ARCDateBooked   = job:Date_Booked
                LOC:RRCDateReturned = ''

                DO WriteColumns_ARC
                !---------------------------------------
            ELSE
              IF GetDeliveryDateAtARC(job:Ref_Number) = TRUE
                !---------------------------------------
                ! RRC -> ARC  <====================(1)
                !---------------------------------------
                LOC:RRCDateBooked   = job:Date_Booked
                !OC:ARCDateBooked   = set by GetDeliveryDateAtARC()
                LOC:RRCDateReturned = GetReturnDateToRRC(job:Ref_Number, LOC:ARCDateBooked)
                IF LOC:RRCDateReturned = 0
                  LOC:RRCDateReturned = ''
                END
                !MESSAGE('HIT CRITERIA - JOB NO'&job:ref_Number)
                DO WriteColumns_ARC
                !---------------------------------------
              ELSE
                !MESSAGE('FAILED?!')
              END
            !ELSE
                !---------------------------------------
                ! RRC -> RRC Unreachable code
                !---------------------------------------
            !    EXIT
                !---------------------------------------
            END !IF
            !-------------------------------------------
        END !IF
        !-----------------------------------------------
    EXIT



          
!        !-----------------------------------------------
!        ! 22 Oct 2002 John
!        ! If the job was booked at the ARC, the RRC date received should be blank.
!        ! The date displayed here is the booking date for the job not the date it was received at the RRC.
!        ! In fact none of these jobs were booked at the RRC therefore this column [RRC Date Received-john] should be blank.
!        !
!        IF LOC:RepairCentreType = 'ARC'
!            LOC:RRCDateBooked = ''
!            LOC:ARCDateBooked = job:Date_Booked
!
!            DO WriteColumns_ARC
!
!        ELSIF GetDeliveryDateAtARC(job:Ref_Number)
!            LOC:RRCDateBooked = ''
!
!            DO WriteColumns_ARC
!
!        ELSE
!            LOC:RRCDateBooked = job:Date_Booked
!            LOC:ARCDateBooked = ''
!
!            DO WriteColumns_RRC
!
!        END !IF
!        !---------------------------------------------------
WriteColumns_ARC                                                       ROUTINE
    DATA
WorkingDays LONG
    CODE
        !-----------------------------------------------
        !WriteDebug('WriteColumns_ARC()')
        !-----------------------------------------------


!        !-----------------------------------------------
!        ! 8 Oct 2002 John
!        GetSubAccount(job:Account_Number)
!
!        IF (CLIP(saQ:HeadAccountNumber) <> CLIP(tra_ali:Account_Number)) !AND (LOC:RRCDateBooked > 0)
!            !WriteDebug('WriteColumns_ARC(RRC Refered Job)(saQ:HeadAccountNumber"' & CLIP(saQ:HeadAccountNumber) & '" <> tra_ali:Account_Number"' & CLIP(tra_ali:Account_Number) & '")')
!            
!            EXIT
!        END !IF
        !-----------------------------------------------


        !-----------------------------------------------
        ! 15 Oct 2002 John
        ! Exclude if location indicates the unit is being passed back to customer
        !
        IF CLIP(job:Location) = CLIP(LOC:DespatchToCustomer)
            !WriteDebug('WriteColumns_ARC(FAIL)(job:Location"' & CLIP(LOC:DespatchToCustomer) & '" = LOC:DespatchToCustomer"' & CLIP(LOC:DespatchToCustomer) & '")')

            EXIT
        END !IF
        !-----------------------------------------------


        !-----------------------------------------------
        ! 8 Oct 2002 John
        ! ANKE: Are exchanges taken into account here? See highlighted row further down.
        ! Joe: If a job has an exchange issued and a date exists in the exchange despatch fields then exclude the job from the report.
        !
        IF job:Date_Despatched > 0
            !WriteDebug('WriteColumns_ARC(FAIL)(job:Date_Despatched"' & DateToString(job:Date_Despatched) & '" > 0)')

            EXIT
        END !IF
        !-----------------------------------------------


        !-----------------------------------------------
        ! 15 Oct 2002 John
        ! 11) IF JOB has an Exchange_Consignment_Number THEN exclude
        !
        IF (job:Exchange_Unit_Number > 0) ! AND (job:Exchange_Despatched > 0)
            !WriteDebug('WriteColumns_ARC(FAIL)(job:Exchange_Unit_Number"' & job:Exchange_Unit_Number & '" > 0)')

            EXIT
        END !IF
        !-----------------------------------------------


        !-----------------------------------------------
        ! 15 Oct 2002 John
        ! 5) Only show jobs having a turnaround greater than ">" LOC:GreaterThanDays
        !   so for 7 only turnaround 8 days and up would be on the report
        !
        IF (LOC:ARCDateBooked < 1)
            !WriteDebug('WriteColumns_ARC(FAIL)(LOC:ARCDateBooked"' & CLIP(DateToString(LOC:ARCDateBooked)) & '" < 1)')

            EXIT
        END !IF
        !GetHeadAccount(wob:HeadAccountNumber)
        GetHeadAccount(LocalHeadAccount)
        IF job:Date_Completed > 0 AND Ignore_Completed = FALSE
            !-------------------------------------------
            ! 11 Nov 2002 John
            ! MICHALAN: ARC date received 16 Oct, Date job completed 17 Oct = 1 day, not 5
            WorkingDays = WorkingDaysBetween(LOC:ARCDateBooked, job:Date_Completed, haQ:IncludeSaturday, haQ:IncludeSunday)
            !-------------------------------------------
        ELSE
            WorkingDays = WorkingDaysBetween(LOC:ARCDateBooked,            TODAY(), haQ:IncludeSaturday, haQ:IncludeSunday)
        END !IF

        IF (WorkingDays <= LOC:GreaterThanDays)
            !WriteDebug('WriteColumns_ARC(FAIL)(WorkingDays"' & WorkingDays & '" <= LOC:GreaterThanDays"' & LOC:GreaterThanDays & '"), LOC:ARCDateBooked="' & DateToString(LOC:ARCDateBooked) & '"')

            EXIT
        END !IF
        !-----------------------------------------------


        !===============================================
        !WriteDebug('WriteColumns_ARC(OK)')

        GetSubAccount(job:Account_Number)

        GetHeadAccount(wob:HeadAccountNumber)

        saQ:Count += 1
        PUT(SubAccount_Queue)

        UpdateSummaryQueue( job:Current_Status, 'JOB' )
        UpdateEngineerQueue( job:Engineer )
        !-----------------------------------------------
        RecordCount += 1
            !-----------------------------------------------
            WriteColumn( job:Ref_Number, True             ) ! SB Job Number


            !-----------------------------------------------
            ! 8 Oct 2002 John
            !ANKE: Should this not just be called "Branch Number"?
            !   Joe: This should be column B
            ! Swapped column B & C
            WriteColumn( haQ:BranchIdentification         ) ! Branch Number
            WriteColumn( wob:JobNumber                    ) ! Franchise Job Number
            !-----------------------------------------------


            WriteColumn( DateToString(LOC:RRCDateBooked)  ) ! RRC Date Received
            WriteColumn( DateToString(LOC:ARCDateBooked)  ) ! ARC Date Received


            !-------------------------------
            ! 10 Oct 2002 John
            ! 2) If report is run for a _RRC_ then the column "Days Since Receipt At ARC" should
            !       be renamed to "Days Since Receipt At RRC" and should show number of days
            !       since the job was received at _RRC_.
            !
!           Days Since Receipt At ARC'
            !
            WriteColumn( WorkingDays                      ) ! Days Since Receipt At RRC
            !-------------------------------


            WriteColumn( DateToString(job:Date_Completed) ) ! Date Job Completed

            !-----------------------------------------------
            ! 23 Oct 2002 John
            ! R002, MICHALAN: Include a new column indicating "Date Received back at RRC" if repaired and completed at ARC
            WriteColumn( DateToString(LOC:RRCDateReturned)              ) ! Date Received back at RRC
            !-----------------------------------------------

            WriteColumn( job:Manufacturer                 ) ! Make
            WriteColumn( job:Model_Number                 ) ! Model


            !-----------------------------------------------
            ! 23 Oct 2002 John
            ! R002, MICHALAN: The Account Name is incorrect. This column should reflect the name relating to the Account Number.
            WriteColumn( saQ:AccountNumber                ) ! Account
            WriteColumn( saQ:AccountName                  ) ! Name
            !-----------------------------------------------


            WriteColumn( job:ESN                          ) ! IMEI

            !-----------------------------------------------
            ! 23 Oct 2002 John
            ! R002, MICHALAN: Insert a new column indicating the location of the job.
            WriteColumn( job:Location                     ) ! Location
            !-----------------------------------------------

            WriteColumn( job:Current_Status               ) ! Current Status
            WriteColumn( job:Exchange_Status              ) ! Exchange Status
            WriteColumn( job:Loan_Status                  ) ! Loan Status

            DO WriteColumn_Engineer ! Engineer'

            ! Insert --- New columns (DBH: 23/03/2009) #10591
            if (job:Warranty_Job = 'YES')
                WriteColumn ('YES')
            else ! if (job:Warranty_Job = 'YES')
                WriteColumn ('NO')
            end ! if (job:Warranty_Job = 'YES')

            WriteColumn( job:Third_Party_Site )

            Do WriteColumn_DaysSinceQuote
            ! end --- (DBH: 23/03/2009) #10591


            !-----------------------------------------------
        IF excel:OperatingSystem < 5
            DO PassViaClipboard
        END !IF
        !---------------------------------------------------
        DO XL_ColFirst
        DO XL_RowDown
        !---------------------------------------------------
    EXIT
WriteColumns_RRC                                                       ROUTINE
    DATA
WorkingDays LONG
    CODE
        !-----------------------------------------------
        !WriteDebug('WriteColumns_RRC()')
        !-----------------------------------------------
! STU's Code
!        IF (TODAY() - job:Date_Booked) <= LOC:GreaterThanDays THEN CYCLE.
        !-----------------------------------------------


        !-----------------------------------------------
        IF NOT LoadWEBJOB(job:Ref_Number)
            ! ERROR

            EXIT
        END !IF
        !-----------------------------------------------


        !-----------------------------------------------
        IF CLIP(wob:HeadAccountNumber) <> CLIP(tra_ali:Account_Number)
            !WriteDebug('WriteColumns_RRC(FAIL)(wob:HeadAccountNumber"' & CLIP(wob:HeadAccountNumber) & '" <> tra_ali:Account_Number"' & CLIP(tra_ali:Account_Number) & '")')

            EXIT
        END !IF
! STU's Code
!       Access:SUBTRACC.CLEARKEY(sub:Account_Number_Key)
!           sub:Account_Number = job:Account_Number
!       IF Access:SUBTRACC.Fetch(sub:Account_Number_Key) THEN
!           IF job:Account_Number <> tra_ali:Account_Number THEN CYCLE.
!       ELSE
!           IF sub:Main_Account_Number <> tra_ali:Account_Number THEN CYCLE.
!       END !IF
        !-----------------------------------------------


        !-----------------------------------------------
        ! 15 Oct 2002 John
        ! Exclude if location indicates the unit is being passed back to customer
        !
        IF CLIP(job:Location) = CLIP(LOC:DespatchToCustomer)
            !WriteDebug('WriteColumns_RRC(FAIL)(job:Location"' & CLIP(LOC:DespatchToCustomer) & '" = LOC:DespatchToCustomer"' & CLIP(LOC:DespatchToCustomer) & '")')

            EXIT
        END !IF
        !-----------------------------------------------


        !-----------------------------------------------
        ! 8 Oct 2002 John
        ! ANKE: Are exchanges taken into account here? See highlighted row further down.
        ! Joe: If a job has an exchange issued and a date exists in the exchange despatch fields then exclude the job from the report.
        !
        IF job:Date_Despatched > 0
            !WriteDebug('WriteColumns_RRC(FAIL)(job:Date_Despatched"' & DateToString(job:Date_Despatched) & '" > 0)')

            EXIT
        END !IF
        !-----------------------------------------------


        !-----------------------------------------------
        ! 15 Oct 2002 John
        ! 11) IF JOB has an Exchange_Consignment_Number THEN exclude
        IF (job:Exchange_Unit_Number > 0) ! AND (job:Exchange_Despatched > 0)
            !WriteDebug('WriteColumns_RRC(FAIL)(job:Exchange_Unit_Number"' & job:Exchange_Unit_Number & '" > 0)')

            EXIT
        END !IF
!
! STU's Code
!       IF job:Date_Despatched <> '' THEN CYCLE.
!       Access:EXCHANGE.CLEARKEY(xch:Ref_Number_Key)
!       xch:Ref_Number = job:Exchange_Unit_Number
!       IF ~Access:EXCHANGE.Fetch(xch:Ref_Number_Key) THEN
!           IF job:Exchange_Despatched <> '' THEN CYCLE.
!       END !IF
        !-----------------------------------------------


        !-----------------------------------------------
        ! 15 Oct 2002 John
        ! 5) Only show jobs having a turnaround greater than ">" LOC:GreaterThanDays
        !   so for 7 only turnaround 8 days and up would be on the report
        !
        IF (LOC:RRCDateBooked < 1)
            !WriteDebug('WriteColumns_RRC(FAIL)(LOC:ARCDateBooked"' & CLIP(DateToString(LOC:RRCDateBooked)) & '" < 1)')

            EXIT
        END !IF

        IF job:Date_Completed > 0 AND Ignore_Completed = FALSE
            !-------------------------------------------
            ! 11 Nov 2002 John
            ! MICHALAN: ARC date received 16 Oct, Date job completed 17 Oct = 1 day, not 5
            WorkingDays = WorkingDaysBetween(LOC:RRCDateBooked, job:Date_Completed, haQ:IncludeSaturday, haQ:IncludeSunday)
            !-------------------------------------------
        ELSE
            WorkingDays = WorkingDaysBetween(LOC:RRCDateBooked,            TODAY(), haQ:IncludeSaturday, haQ:IncludeSunday)
        END !IF

        IF (WorkingDays <= LOC:GreaterThanDays)
            !WriteDebug('WriteColumns_RRC(FAIL)(WorkingDays"' & WorkingDays & '" <= LOC:GreaterThanDays"' & LOC:GreaterThanDays & '"), LOC:RRCDateBooked="' & DateToString(LOC:RRCDateBooked) & '"')

            EXIT
        END !IF
        !-----------------------------------------------


        !-----------------------------------------------
        IF NOT LoadWEBJOB(job:Ref_Number)
            ! ERROR

            EXIT
        END !IF

        GetSubAccount(job:Account_Number)

        GetHeadAccount(wob:HeadAccountNumber)

        !-----------------------------------------------


        !===============================================
        !WriteDebug('WriteColumns_RRC(OK)')

        saQ:Count += 1
        PUT(SubAccount_Queue)

        UpdateSummaryQueue( job:Current_Status, 'JOB' )
        UpdateEngineerQueue( job:Engineer )
!
!       IF job:ExcService = 'Y'
!           UpdateSummaryQueue job:Exchange_Status, 'EXCHANGE'
!       END !IF
!
!       IF job:LoaService = 'Y'
!           UpdateSummaryQueue job:Loan_Status, 'LOAN'
!       END !IF
        !-----------------------------------------------
        RecordCount += 1
            !-----------------------------------------------
            WriteColumn( job:Ref_Number, True              ) ! SB Job Number

            !-----------------------------------------------
            ! 8 Oct 2002 John
            !ANKE: Should this not just be called "Branch Number"?
            !   Joe: This should be column B
            ! Swapped column B & C
            !
            ! 11 Nov 2002 John 
            ! MICHALAN: Some jobs were booked in at the RRC, this should show the RRC Branch number not ARC branch number
            ! changed to haQ:BranchIdentification to saQ:BranchIdentification
            WriteColumn( saQ:BranchIdentification          ) ! Branch Number
            WriteColumn( wob:JobNumber                     ) ! Franchise Job Number
            !-----------------------------------------------

            WriteColumn( LOC:RRCDateBooked                 ) ! RRC Date Received
            WriteColumn( ''                                ) ! ARC Date Received


            !-------------------------------
            ! 10 Oct 2002 John
            ! 2) If report is run for a _RRC_ then the column "Days Since Receipt At ARC" should
            !       be renamed to "Days Since Receipt At RRC" and should show number of days
            !       since the job was received at _RRC_.
            !
            WriteColumn( WorkingDays                       ) ! Days Since Receipt At RRC
            !-------------------------------


            WriteColumn( DateToString(job:Date_Completed)  ) ! Date Job Completed

            !-----------------------------------------------
            ! 23 Oct 2002 John
            ! R002, MICHALAN: Include a new column indicating "Date Received back at RRC" if repaired and completed at ARC
            WriteColumn( DateToString(LOC:RRCDateReturned) ) ! Date Received back at RRC
            !-----------------------------------------------

            WriteColumn( job:Manufacturer                  ) ! Make
            WriteColumn( job:Model_Number                  ) ! Model


            !-----------------------------------------------
            ! 23 Oct 2002 John
            ! R002, MICHALAN: The Account Name is incorrect. This column should reflect the name relating to the Account Number.
            WriteColumn( saQ:AccountNumber                 ) ! Account
            WriteColumn( saQ:AccountName                   ) ! Name
            !-----------------------------------------------


            WriteColumn( job:ESN                           ) ! IMEI

            !-----------------------------------------------
            ! 23 Oct 2002 John
            ! MICHALAN: Insert a new column indicating the location of the job.
            WriteColumn( job:Location                      ) ! Current Status
            !-----------------------------------------------

            WriteColumn( job:Current_Status                ) ! Current Status
            WriteColumn( job:Exchange_Status               ) ! Exchange Status
            WriteColumn( job:Loan_Status                   ) ! Loan Status

            DO WriteColumn_Engineer ! Engineer'
            ! Insert --- New columns (DBH: 23/03/2009) #10591
            if (job:Warranty_Job = 'YES')
                WriteColumn ('YES')
            else ! if (job:Warranty_Job = 'YES')
                WriteColumn ('NO')
            end ! if (job:Warranty_Job = 'YES')

            WriteColumn( job:Third_Party_Site )

            Do WriteColumn_DaysSinceQuote
            ! end --- (DBH: 23/03/2009) #10591

            !-----------------------------------------------
        IF excel:OperatingSystem < 5
            DO PassViaClipboard
        END !IF
        !---------------------------------------------------
        DO XL_ColFirst
        DO XL_RowDown
        !---------------------------------------------------
    EXIT
!--------------------------------------------------------------
WriteColumn_JobNumber                       ROUTINE ! Job Number
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = job:Ref_Number

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Ref_Number

        YIELD()

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_FranchiseJobNumber  ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Access:WEBJOB.CLEARKEY(wob:RefNumberKey)
        wob:RefNumber = job:Ref_Number
        IF Access:WEBJOB.Fetch(wob:RefNumberKey) = Level:Benign
            IF excel:OperatingSystem < 5 THEN
               clip:Value = CLIP(clip:Value) & '<09>' & wob:JobNumber

               EXIT
            END !IF
            !-----------------------------------------------
            Excel{'ActiveCell.Formula'}  = wob:JobNumber
            !-----------------------------------------------
        END !IF

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_FranchiseBranchNumber  ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        ! 22 Oct 2002 John
        ! There are 862 jobs in this list out of 6000 jobs currently booked onto the system.
        !   It is impossible to believe that some of the overdue jobs in the database were not originally booked in at the RRC.
        !   In that event this column should show the RRC Branch number as well as the ARC branch number.
        !
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & saQ:BranchIdentification ! tra_ali:BranchIdentification

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = saQ:BranchIdentification ! tra_ali:BranchIdentification

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_RRCDateBooked  ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & DateToString(LOC:RRCDateBooked)

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = DateToString(LOC:RRCDateBooked)

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_ARCDateBooked  ROUTINE
    DATA
Temp STRING(30)
    CODE
        !-----------------------------------------------
        IF LOC:ARCDateBooked > 1
            Temp = DateToString(LOC:ARCDateBooked)
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & Temp

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_DaysSinceRecAtARC  ROUTINE
    DATA
WorkingDays LONG
    CODE
        !-----------------------------------------------
        IF LOC:ARCDateBooked > 1
            !-------------------------------------------
            ! 8 Oct 2002 John
            ! ANKE: Must show working days only -  weekends to be excluded from the  calculation
            !
            WorkingDays = WorkingDaysBetween(LOC:ARCDateBooked, TODAY(), haQ:IncludeSaturday, haQ:IncludeSunday)
            !-------------------------------------------
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5 THEN
           clip:Value = CLIP(clip:Value) & '<09>' & WorkingDays

           EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'} = WorkingDays

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_DaysSinceRecAtRRC  ROUTINE
    DATA
WorkingDays LONG
    CODE
        !-----------------------------------------------
        WorkingDays = WorkingDaysBetween(LOC:RRCDateBooked, TODAY(), haQ:IncludeSaturday, haQ:IncludeSunday)
        !-----------------------------------------------
        IF excel:OperatingSystem < 5 THEN
           clip:Value = CLIP(clip:Value) & '<09>' & WorkingDays

           EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'} = WorkingDays

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_DateCompleted                   ROUTINE ! Date Completed
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & DateToString(job:Date_Completed)

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = DateToString(job:Date_Completed)

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_Turnaround                      ROUTINE ! Turnaround
    DATA
Temp            LONG
IncludeSaturday LONG(False)
IncludeSunday   LONG(False)
    CODE
        !-----------------------------------------------
        IF tra:UseTimingsFrom = 'TRA'
            IncludeSaturday = tra:IncludeSaturday
            IncludeSunday   = tra:IncludeSunday
        ELSE
            IF def:Include_Saturday = 'YES'
                IncludeSaturday = True
            END !IF

            IF def:Include_Saturday = 'YES'
                IncludeSunday   = True
            END !IF
        END !IF

        Temp = WorkingDaysBetween(job:date_booked, job:Date_Completed, IncludeSaturday, IncludeSunday)
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & Temp

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp ! (job:Date_Completed - job:date_booked)

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_Manufacturer                ROUTINE ! Make
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Manufacturer
                         
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Manufacturer

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_ModelNumber                 ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        ! Model
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Model_Number

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}      = job:Model_Number

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_TradeAccountNumber                              ROUTINE ! Account Number
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Account_Number

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Account_Number

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_TradeAccountName            ROUTINE ! Write value to data cell
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & saQ:HeadAccountName

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = saQ:HeadAccountName

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_ESN                         ROUTINE ! IMEI
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:ESN

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:ESN

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_CurrentStatus_Job                       ROUTINE ! Write value to data cell
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Current_Status

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Current_Status

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_CurrentStatus_Exchange                      ROUTINE ! Write value to data cell
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Exchange_Status

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Exchange_Status

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_CurrentStatus_Loan                      ROUTINE ! Write value to data cell
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & job:Loan_Status

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = job:Loan_Status

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_Engineer                ROUTINE
    DATA
Temp STRING(100)
    CODE
        !-----------------------------------------------
        Temp = CLIP(AppendString(engq:Surname, engq:Forename, ', '))
        Temp = CLIP(AppendString(Temp,         engq:UserCode, '-' ))
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            clip:Value = CLIP(clip:Value) & '<09>' & Temp

            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
!WriteColumn_UnitType                    ROUTINE ! Unit Type
!    DATA
!    CODE
!        !-----------------------------------------------
!        IF excel:OperatingSystem < 5
!            clip:Value = CLIP(clip:Value) & '<09>' & job:Unit_Type
!
!            EXIT
!        END !IF
!        !-----------------------------------------------
!        Excel{'ActiveCell.Formula'}  = job:Unit_Type
!
!        DO XL_ColRight
!        !-----------------------------------------------
!    EXIT
!WriteColumn_MSN                         ROUTINE ! MSN
!    DATA
!    CODE
!        !-----------------------------------------------
!        IF excel:OperatingSystem < 5
!            clip:Value = CLIP(clip:Value) & '<09>' & job:MSN
!
!            EXIT
!        END !IF
!        !-----------------------------------------------
!        Excel{'ActiveCell.Formula'}  = job:MSN
!
!        DO XL_ColRight
!        !-----------------------------------------------
!    EXIT
WriteColumn_DaysSinceQuote    Routine
Data
local:DateEstimateAccepted      Date()
Code
    !Days Since Quote Accepted
    local:DateEstimateAccepted = 0
    Access:AUDIT.Clearkey(aud:Ref_Number_Key)
    aud:Ref_Number    = job:Ref_Number
    aud:Date    = Today()
    set(aud:Ref_Number_Key,aud:Ref_Number_Key)
    loop
        if (Access:AUDIT.Next())
            Break
        end ! if (Access:AUDIT.Next())
        if (aud:Ref_Number    <> job:Ref_Number)
            Break
        end ! if (aud:Ref_Number    <> job:Ref_Number)
        if (Instring('ESTIMATE ACCEPTED',aud:Action,1,1))
            local:DateEstimateAccepted = aud:Date
            Break
        end ! if (Instring('ESTIMATE ACCEPTED',aud:Action,1,1))
    end ! loop

    if (local:DateEstimateACcepted = 0)
        WriteColumn('')
    else ! if (local:DateEstimateACcepted = 0)
        countDays# = 0
        Access:TRADEACC_ALIAS.Clearkey(tra_ali:Account_Number_Key)
        tra_ali:Account_Number = wob:HeadAccountNumber
        if (Access:TRADEACC_ALIAS.TryFetch(tra_ali:Account_Number_Key) = Level:Benign)
            ! Found
            loop x# = local:DateEstimateAccepted To Today()

                if (tra_ali:IncludeSaturday = 0)
                    if (x# % 7 = 6)
                        cycle
                    end ! if (x# % 7 = 6)
                end ! if (tra_ali:IncludeSaturday = 0)
                if (tra_ali:IncludeSunday = 0)
                    if (x# % 7 = 0)
                        cycle
                    end ! if (x# % 7 = 6)
                end ! if (tra_ali:IncludeSaturday = 0)

                Access:TRABUSHR.Clearkey(tbh:TypeDateKey)
                tbh:RefNumber    = tra_ali:RecordNumber
                tbh:TheDate        = x#
                if (Access:TRABUSHR.TryFetch(tbh:TypeDateKey) = Level:Benign)
                    ! Found
                    if (tbh:ExceptionType = 1)
                        cycle
                    end ! if (tbh:ExceptionType = 1)
                else ! if (Access:TRABUSHR.TryFetch(tbh:TypeDateKey) = Level:Benign)
                    ! Error
                end ! if (Access:TRABUSHR.TryFetch(tbh:TypeDateKey) = Level:Benign)

                countDays# += 1
            end ! loop x# = local:DateEstimateAccepted To Today()
        else ! if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
            ! Error
        end ! if (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
        WriteColumn(countDays#)
    end ! if (local:DateEstimateACcepted = 0)
!--------------------------------------------------------------
WriteDataSummary                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        ! summary details
        !                                                                             
        Excel{'ActiveWorkBook.Sheets("' & CLIP(LOC:SectionName) & '").Select'}

        DO FormatColumns

        Excel{'Range("B' & sheet:DataSectionRow & '").Select'} ! was F 15 Oct 2002 John
            DO XL_HorizontalAlignmentLeft
            Excel{'ActiveCell.Formula'}         = RecordCount

        Excel{'Range("C' & sheet:DataSectionRow & '").Select'} ! was G 15 Oct 2002 John
            DO XL_HorizontalAlignmentRight
            Excel{'ActiveCell.Formula'}         = 'Showing'

        Excel{'Range("D' & sheet:DataSectionRow & '").Select'} ! was H 15 Oct 2002 John
            DO XL_HorizontalAlignmentLeft
            Excel{'ActiveCell.Formula'}         = '=SUBTOTAL(2, A' & sheet:DataHeaderRow+1 & ':A' & sheet:DataHeaderRow+RecordCount & ')'

        Excel{'Range("A' & sheet:DataHeaderRow  & '").Select'}
            DO XL_DataAutoFilter
        !-----------------------------------------------------------------      
    EXIT
WriteHeadSummary                    ROUTINE
    DATA
    CODE
        !-------------------------------
        ! 10 Oct 2002 John
        ! 1) Remove Summary Sheet Completely
        EXIT
        !-------------------------------


        !-----------------------------------------------------------------
        ! summary sheet details
        !
        Excel{'ActiveWorkBook.Sheets("Summary").Select'}
        !-----------------------------------------------------------------
        IF RecordCount < 1
            Excel{'ActiveCell.Formula'} = 'None Found'

            EXIT
        END !IF
        !-----------------------------------------------------------------
        DO Summary
        DO SummarySubAccount
        DO SummaryEngineer
        !-----------------------------------------------------------------
    EXIT
!--------------------------------------------------------------
Summary         ROUTINE
    DATA
QueueIndex   LONG
ResultsCount LONG

HeaderRow    LONG
FirstRow     LONG
LastRow      LONG
    CODE
        !-----------------------------------------------------------------
        ResultsCount = RECORDS(SummaryQueue)

        HeaderRow = Excel{'ActiveCell.Row'}  ! first blank line
        FirstRow  = HeaderRow + 1
        LastRow   = HeaderRow + ResultsCount

        Excel{'Range("A' & HeaderRow & ':' & sheet:HeadLastCol & HeaderRow & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & HeaderRow & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'}      = 'Status'
            Excel{'ActiveCell.Offset(0, 1).Formula'}      = 'Count'

        Excel{'Range("A' & FirstRow & '").Select'}
        IF ResultsCount < 1
            Excel{'ActiveCell.Formula'}                   = 'No Statuses Found'
        END !IF

        SORT(SummaryQueue, +sq:Status)
        LOOP QueueIndex = 1 TO ResultsCount
            !-------------------------------------------------------------
            GET(SummaryQueue, QueueIndex)

            Excel{'ActiveCell.Offset(0, 0).Formula'}      = sq:Status
            Excel{'ActiveCell.Offset(0, 1).Formula'}      = sq:Count

            DO XL_RowDown
            !-------------------------------------------------------------
        END !LOOP

        Excel{'Range("A' & LastRow+1 & ':' & sheet:HeadLastCol & LastRow+1 & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & LastRow+1 & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'}      = 'Sub-Totals'
            Excel{'ActiveCell.Offset(0, 1).Formula'}      = '=SUBTOTAL(9, B' & FirstRow & ':B' & LastRow & ')'
        !-----------------------------------------------------------------
        Excel{'Range("A' & LastRow+3 & '").Select'}
        !-----------------------------------------------------------------
    EXIT
SummarySubAccount       ROUTINE
    DATA
QueueIndex   LONG
ResultsCount LONG

HeaderRow    LONG
FirstRow     LONG
LastRow      LONG
    CODE
        !-----------------------------------------------------------------
        ! Totals Trade Account
        !
        ResultsCount = RECORDS(SubAccount_Queue)

        HeaderRow = Excel{'ActiveCell.Row'}  ! first blank line
        FirstRow  = HeaderRow + 1
        LastRow   = HeaderRow + ResultsCount

        Excel{'Range("A' & HeaderRow & ':' & sheet:HeadLastCol & HeaderRow & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & HeaderRow & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'}          = 'Account'
            Excel{'ActiveCell.Offset(0, 1).Formula'}          = 'Name'
            Excel{'ActiveCell.Offset(0, 2).Formula'}          = 'Jobs'

        Excel{'Range("A' & FirstRow & '").Select'}
        IF ResultsCount < 1
            Excel{'ActiveCell.Formula'}                       = 'No Sub Accounts Found'
        END !IF

        SORT(SubAccount_Queue, +saQ:AccountNumber)
        LOOP QueueIndex = 1 TO ResultsCount
            !-------------------------------------------------------------
            GET(SubAccount_Queue, QueueIndex)

            Excel{'ActiveCell.Offset(0, 0).Formula'}          = saQ:AccountNumber
            Excel{'ActiveCell.Offset(0, 1).Formula'}          = saQ:AccountName
            Excel{'ActiveCell.Offset(0, 2).Formula'}          = saQ:Count
                Excel{'ActiveCell.Offset(0, 2).NumberFormat'} = '###0'

            DO XL_RowDown
            !-------------------------------------------------------------
        END !LOOP

        Excel{'Range("A' & LastRow+1 & ':' & sheet:HeadLastCol & LastRow+1 & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & LastRow+1 & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'}          = 'Sub-Totals'
            Excel{'ActiveCell.Offset(0, 2).Formula'}          = '=SUBTOTAL(9, C' & FirstRow & ':C' & LastRow & ')'
        !-----------------------------------------------------------------
        Excel{'Range("A' & LastRow+3 & '").Select'}
        !-----------------------------------------------------------------
    EXIT
SummaryEngineer     ROUTINE
    DATA
QueueIndex   LONG
ResultsCount LONG

HeaderRow    LONG
FirstRow     LONG
LastRow      LONG
    CODE
        !-----------------------------------------------------------------
        ! Totals Engineer
        !
        ResultsCount = RECORDS(EngineerQueue)

        HeaderRow = Excel{'ActiveCell.Row'}  ! first blank line
        FirstRow  = HeaderRow + 1
        LastRow   = HeaderRow + ResultsCount

        Excel{'Range("A' & HeaderRow & ':' & sheet:HeadLastCol & HeaderRow & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & HeaderRow & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'}          = 'Engineer'
            Excel{'ActiveCell.Offset(0, 1).Formula'}          = 'Name'
            Excel{'ActiveCell.Offset(0, 2).Formula'}          = 'Jobs'

        Excel{'Range("A' & FirstRow & '").Select'}
        IF ResultsCount < 1
            Excel{'ActiveCell.Offset(0, 0).Formula'}          = 'No Engineers Found'
        END !IF

        SORT(EngineerQueue, +engq:UserCode)
        LOOP QueueIndex = 1 TO ResultsCount
            !-------------------------------------------------------------
            GET(EngineerQueue, QueueIndex)

            Excel{'ActiveCell.Offset(0, 0).Formula'}          = engq:UserCode
            Excel{'ActiveCell.Offset(0, 1).Formula'}          = AppendString(engq:Surname, engq:Forename, ', ')
            Excel{'ActiveCell.Offset(0, 2).Formula'}          = engq:JobsBooked
                Excel{'ActiveCell.Offset(0, 2).NumberFormat'} = '###0'

            DO XL_RowDown
            !-------------------------------------------------------------
        END !LOOP

        Excel{'Range("A' & LastRow+1 & ':' & sheet:HeadLastCol & LastRow+1 & '").Select'}
            DO XL_SetColumnHeader

        Excel{'Range("A' & LastRow+1 & '").Select'}
            Excel{'ActiveCell.Offset(0, 0).Formula'}          = 'Sub-Totals'
            Excel{'ActiveCell.Offset(0, 2).Formula'}          = '=SUBTOTAL(9, C' & FirstRow & ':C' & HeaderRow+ResultsCount & ')'
        !-----------------------------------------------------------------
        Excel{'Range("A' & LastRow+3 & '").Select'}
        !-----------------------------------------------------------------
    EXIT
!--------------------------------------------------------------
FillJobNumberQueue              ROUTINE
    DATA
    CODE
        !------------------------------------------
        ! AccountNumberKey KEY( job:Account_Number, job:Ref_Number ),DUP,NOCASE
        ! Date_Booked_Key  KEY( job:date_booked                    ),DUP,NOCASE
        ! DateCompletedKey KEY( job:Date_Completed                 ),DUP,NOCASE
        !
        Access:JOBS.ClearKey(job:Date_Booked_Key)
            job:Date_Booked = LOC:StartDate
        SET(job:Date_Booked_Key,job:Date_Booked_Key)
        !------------------------------------------
        Progress:Text    = 'Finding Jobs'
        RecordsToProcess = RECORDS(Jobs)

        DO ProgressBar_LoopPre
        !-----------------------------------------------------------------
        LOOP UNTIL Access:JOBS.NEXT()
            !-------------------------------------------------------------
            DO ProgressBar_Loop

            IF CancelPressed
                BREAK
            END !IF
            !-------------------------------------------------------------
            IF job:Date_Booked > LOC:EndDate
                BREAK
            END !IF

            IF job:Date_Despatched > 0
                CYCLE
            ELSIF (job:Exchange_Unit_Number > 0)
                CYCLE
            ELSIF NOT LoadSUBTRACC(job:Account_Number)
                CYCLE
            ELSIF NOT LoadTRADEACC(sub:Main_Account_Number)
                CYCLE
            END !IF
            !=============================================================
            CLEAR(JobNumber_Queue)
                jobnumQ:JobNumber         = job:Ref_Number
                jobnumQ:HeadAccountNumber = sub:Main_Account_Number
            ADD(JobNumber_Queue)
            !-------------------------------------------------------------
        END !LOOP

        SORT(JobNumber_Queue, +jobnumQ:HeadAccountNumber, +jobnumQ:JobNumber)

        DO ProgressBar_LoopPost
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
    EXIT
GetFileName                             ROUTINE ! Generate default file name
    DATA
local:Desktop         CString(255)
DeskTopExists   BYTE
ApplicationPath STRING(255)
excel:ProgramName       Cstring(255)
    CODE
    excel:ProgramName = '7 Day TAT Report'
    SHGetSpecialFolderPath( GetDesktopWindow(), local:Desktop, 5, FALSE )
    local:Desktop = Clip(local:Desktop) & '\ServiceBase Export'

    !Does the Export Folder already Exists?
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    local:Desktop = Clip(local:Desktop) & '\' & Clip(excel:ProgramName)
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Missive('An error has occured finding, or creating the folder for the report.'&|
                '|' & Clip(Clip(local:Desktop)) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                           'mstop.jpg','/&OK')
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    loc:Path = local:Desktop
    loc:FileName = Clip(local:Desktop) & '\' & CLIP(tra_ali:Account_Number) & ' ' & Format(Today(),@d12)
!
!        !-----------------------------------------------
!        ! Generate default file name
!        !
!        !-----------------------------------------------
!        ! 4 Dec 2001 John Stop using def:Export_Path as previously directed
!        ! 4 Dec 2001 John Use generated date to create file name not parameters LOC:StartDate,LOC:EndDate
!        !
!        IF CLIP(LOC:FileName) <> ''
!            ! Already generated 
!            EXIT
!        END !IF
!
!        DeskTopExists = False
!
!        SHGetSpecialFolderPath( GetDesktopWindow(), Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
!        LOC:DesktopPath = Desktop
!
!        ApplicationPath = Desktop & '\' & LOC:ApplicationName & ' Export'
!        IF EXISTS(CLIP(ApplicationPath))
!            LOC:Path      = CLIP(ApplicationPath) & '\'
!            DeskTopExists = True
!
!        ELSE
!            Desktop  = CLIP(ApplicationPath)
!
!            IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!!                MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!!            ELSE
!!                MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
!            END !IF
!
!            IF EXISTS(CLIP(ApplicationPath))
!                LOC:Path      = CLIP(ApplicationPath) & '\'
!                !DeskTopExists = True
!            END !IF
!
!        END !IF
!
!        IF DeskTopExists
!            ApplicationPath = CLIP(LOC:Path) & CLIP(LOC:ProgramName)
!            Desktop         = CLIP(ApplicationPath)
!
!            IF NOT EXISTS(CLIP(ApplicationPath))
!                IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!!                    MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!!                ELSE
!!                    MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
!                END !IF
!            END !IF
!
!            IF EXISTS(CLIP(ApplicationPath))
!                LOC:Path = CLIP(ApplicationPath) & '\'
!            END !IF
!        END !IF
!
!        !-----------------------------------------------
!        ! 23 Oct 2002 John
!        ! R003, berrjo :Insert the report exe name EG VODR56.V01
!        ! Do not attach the EXE extenstion but show the version number.  Increment the version number each time changed.
!        ! Make this standard on all reports sent for correction.
!        ! LOC:Version STRING(8) 3.1.0000 3.1 to match sevicebase version (and dictionary) .0000 increment before each compile
!        !
!        !LOC:FileName = SHORTPATH(CLIP(LOC:Path)) & CLIP(tra_ali:Account_Number) & ' VODR0057 ' & FORMAT(TODAY(), @D12) & '.xls'
!        LOC:FileName = SHORTPATH(CLIP(LOC:Path)) & CLIP(tra_ali:Account_Number) & ' VODR0057 ' & FORMAT(TODAY(), @D12) & '.xls'
!        !-----------------------------------------------
!    EXIT
GetUserName                            ROUTINE
    DATA
CommandLine STRING(255)
tmpPos      LONG
    CODE
        !-----------------------------------------------
        CommandLine = CLIP(COMMAND(''))

        Automatic = FALSE

!        LOC:UserName = LOC:ApplicationName
!
!        Case MessageEx('Fail User Check For "' & CLIP(CommandLine) & '"?', LOC:ApplicationName & ' DEBUG',|
!                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
!            Of 1 ! &Yes Button
!                POST(Event:CloseWindow)
!
!                EXIT
!            Of 2 ! &No Button
!                EXIT
!        End!Case MessageEx
        !-----------------------------------------------
        tmpPos = INSTRING('%', CommandLine)
        IF NOT tmpPos
            Case Missive('You must run this report from ServiceBase''s "Custom Reports".','ServiceBase 3g',|
                           'mstop.jpg','/OK') 
                Of 1 ! OK Button
            End ! Case Missive

            POST(Event:CloseWindow)

           EXIT
        END !IF tmpPos
        !-----------------------------------------------
        skip# = 0

        SET(USERS)
        Access:USERS.Clearkey(use:Password_Key)
        glo:Password = CLIP(SUB(CommandLine, tmpPos + 1, 30))
      
        Access:USERS.Clearkey(use:Password_Key)
        use:Password = glo:Password
        If Access:USERS.Tryfetch(use:Password_Key)
!            !Assert(0,'<13,10>Fetch Error<13,10>')
!            Case MessageEx('Unable to find your logged in user details.', |
!                    LOC:ApplicationName,                                  |
!                    'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,LocalTimeOut)
!                Of 1 ! &OK Button
!            End!Case MessageEx
!
!            POST(Event:CloseWindow)
!
!           EXIT
            LOC:UserName = 'AUTOMATED PROCEDURE'
            Automatic = TRUE
            skip# = 1

        End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        !-----------------------------------------------
        IF skip# = 0


            LOC:UserName = AppendString(use:Forename, use:Surname, ' ')

    !        IF CLIP(LOC:UserName) = ''
    !            LOC:UserName = use:Surname
    !        ELSE
    !            LOC:UserName = CLIP(use:Forename) & ' ' & use:Surname 
    !        END !IF

            IF CLIP(LOC:UserName) = ''
                LOC:UserName = '<' & use:User_Code & '>'
            END !IF
        End !IF skip# = 0

        !-----------------------------------------------

    EXIT
LoadFileDialog                          ROUTINE ! Ask user if file name is empty
    DATA
OriginalPath STRING(255)
    CODE
        !-----------------------------------------------
        IF CLIP(LOC:Filename) = ''
            DO GetFileName 
        END !IF

!        OriginalPath = PATH()
!        SETPATH(LOC:Path) ! Required for Win95/98
!            IF NOT FILEDIALOG('Save Spreadsheet', LOC:Filename, 'Microsoft Excel Workbook|*.XLS', |
!                FILE:KeepDir + FILE:Save + FILE:NoError + FILE:LongName)
!
!                LOC:Filename = ''
!            END !IF
!        SETPATH(OriginalPath)

        UPDATE()
        DISPLAY()
        !-----------------------------------------------
    EXIT
LoadSUBTRACC                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !
        Result = False

        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = job:Account_Number

        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key) <> Level:Benign
            EXIT
        END !IF

        IF sub:Account_Number <> job:Account_Number
            EXIT
        END !IF

        Result = True
        !-----------------------------------------------
    EXIT
PassViaClipboard                                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF clip:Saved = False
            clip:OriginalValue = CLIPBOARD()
            clip:Saved         = True
        END !IF
        !-----------------------------------------------
        SETCLIPBOARD(CLIP(clip:Value))
            Excel{'ActiveSheet.Paste()'}
        clip:Value = ''
        !-----------------------------------------------
    EXIT
ResetClipboard                                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF clip:Saved = False
            EXIT
        END !IF

        SETCLIPBOARD(clip:OriginalValue)
        
        clip:Saved = False
        !-----------------------------------------------
    EXIT
!--------------------------------------------------------------
ProgressBar_Setup                       ROUTINE
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
            RecordsPerCycle      = 25
            RecordsProcessed     = 0
            RecordsToProcess     = 10 !***The Number Of Records, or at least a guess***
            PercentProgress      = 0
            Progress:thermometer = 0

            thiswindow.reset(1) !This may error, if so, remove this line.
            open(progresswindow)
            progresswindow{PROP:Timer} = 1
         
            ?progress:userstring{prop:text} = 'Running...'
            ?progress:pcttext{prop:text}    = '0% Completed'
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***

ProgressBar_LoopPre          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***

        RecordsProcessed = 0
        RecordCount      = 0

        ?Progress:UserString{PROP:Text} = CLIP(Progress:Text)

        !RecordsToProcess = RECORDS(JOBS) !***The Number Of Records, or at least a guess***
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
        !-----------------------------------------------
    EXIT

ProgressBar_Loop             ROUTINE
    DATA
newPctText STRING(255)
    CODE
        !-----------------------------------------------
        !***Place the following code inside the loop. The first line updates the progress bar, and the rest allows the user to cancel the routine. This can be removed if necessary. ****

        Do ProgressBar_GetNextRecord2 !Necessary

        Do ProgressBar_CancelCheck
        If CancelPressed
            CLOSE(ProgressWindow)
        End!If 
        !-----------------------------------------------
        newPctText = 'Checked(' & RecordsProcessed & '), Found(' & RecordCount & ')'

        IF ?progress:pcttext{prop:text} <> newPctText
            ?progress:pcttext{prop:text} = newPctText

            Display()
        END !IF

        !***Place the following code inside the loop. The first line updates the progress bar, and the rest allows the user to cancel the routine. This can be removed if necessary. ****
        !-----------------------------------------------
    EXIT

ProgressBar_LoopPost         ROUTINE
    DATA
newPctText STRING(255)
    CODE
        !-----------------------------------------------
        ?Progress:UserString{PROP:Text} = CLIP(Progress:Text) & ' Done(' & RecordsProcessed & '/' & RecordCount & ')'

        newPctText = 'Checked(' & RecordsProcessed & '), Found(' & RecordCount & ')'

        IF ?progress:pcttext{prop:text} <> newPctText
            ?progress:pcttext{prop:text} = newPctText
        END !IF
        !-----------------------------------------------
    EXIT

ProgressBar_Finalise                        ROUTINE
        !**** End Of Loop ***
            Do ProgressBar_EndPrintRun
            close(progresswindow)
        !**** End Of Loop ***
        ! when in report procedure->LocalResponse = RequestCompleted

ProgressBar_GetNextRecord2                  routine
    recordsprocessed += 1
    recordsthiscycle += 1

    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        !?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end

ProgressBar_CancelCheck                     routine
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept

    If cancel# = 1
        BEEP(BEEP:SystemAsterisk)  ;  YIELD()

        Case Missive('Are you sure you want to cancel?','ServiceBase 3g',|
                       'mquest.jpg','\No|/Yes') 
            Of 2 ! Yes Button
                CancelPressed = True
            Of 1 ! No Button
        End ! Case Missive
    End!If cancel# = 1

ProgressBar_EndPrintRun                     routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()

RefreshWindow                          ROUTINE
    !|
    !| This routine is used to keep all displays and control templates current.
    !|
    !IF window{Prop:AcceptAll} THEN EXIT.
    IF MainWindow{Prop:AcceptAll} THEN EXIT.
    
    DISPLAY()
    !ForceRefresh = False
XL_Setup                                                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel = Create(0, Create:OLE)
        Excel{PROP:Create} = 'Excel.Application'

        Excel{'Application.DisplayAlerts'}  = False         ! no alerts to the user (do you want to save the file?) ! MOVED 10 BDec 2001 John
        Excel{'Application.ScreenUpdating'} = excel:Visible ! False
        Excel{'Application.Visible'}        = excel:Visible ! False

        Excel{'Application.Calculation'}    = xlCalculationManual
        
        DO XL_GetOperatingSystem
        !-----------------------------------------------
    EXIT
XL_Finalize                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Application.Calculation'}    = xlCalculationAutomatic

        Excel{PROP:DEACTIVATE}
        !-----------------------------------------------
    EXIT
XL_AddSheet                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveWorkBook.Sheets("Sheet3").Select'}

        Excel{'ActiveWorkBook.Sheets.Add'}
        !-----------------------------------------------
    EXIT
XL_AddWorkbook               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Application.Workbooks.Add()'}

        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Title")'}            = CLIP(LOC:ProgramName) & ' ' & CLIP(tra_ali:Account_Number)
        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Author")'}           = CLIP(LOC:UserName)
        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Application Name")'} = LOC:ApplicationName
        !-----------------------------------------------
        ! 4 Dec 2001 John
        ! Delete empty sheets

        Excel{'Sheets("Sheet2").Select'}
        Excel{'ActiveWindow.SelectedSheets.Delete'}

        Excel{'Sheets("Sheet1").Select'}
        !-----------------------------------------------
    EXIT
XL_SaveAllWorksheets                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOOP WHILE Excel{'WorkBooks.Count'} > 0
            Excel{'ActiveWorkBook.Save()'}
            Excel{'ActiveWorkBook.Close()'}
        END !LOOP
        !-----------------------------------------------
    EXIT
XL_DropAllWorksheets                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOOP WHILE Excel{'WorkBooks.Count'} > 0
            Excel{'ActiveWorkBook.Close(' & FALSE & ')'}
        END !LOOP
        !-----------------------------------------------
    EXIT
XL_AddComment                ROUTINE
    DATA
xlComment CSTRING(20)
    CODE
        !-----------------------------------------------
        Excel{'Selection.AddComment("' & CLIP(excel:CommentText) & '")'}

!        xlComment{'Shape.IncrementLeft'} = 127.50
!        xlComment{'Shape.IncrementTop'}  =   8.25
!        xlComment{'Shape.ScaleWidth'}    =   1.76
!        xlComment{'Shape.ScaleHeight'}   =   0.69
        !-----------------------------------------------
    EXIT
XL_RowDown                   ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(1, 0).Select'}
        !-----------------------------------------------
    EXIT
XL_ColLeft                   ROUTINE
    DATA
CurrentCol LONG
    CODE
        !-----------------------------------------------
        CurrentCol = Excel{'ActiveCell.Col'}
        IF CurrentCol = 1
            EXIT
        END !IF

        Excel{'ActiveCell.Offset(0, -1).Select'}
        !-----------------------------------------------
    EXIT
XL_ColRight                  ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(0, 1).Select'}
        !-----------------------------------------------
    EXIT
XL_ColFirstSelect                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, Cells(' & Excel{'ActiveCell.Row'} & ', 1)).Select'}
        !Excel{'ActiveCell.Offset(0, -' & Excel{'ActiveCell.Column'} - 1 & ').Select'}
        !-----------------------------------------------
    EXIT
XL_ColFirst                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(0, -' & Excel{'ActiveCell.Column'} - 1 & ').Select'}
        !-----------------------------------------------
    EXIT
XL_SetLastCell                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, ActiveCell.SpecialCells(' & xlLastCell & ')).Select'}
        !-----------------------------------------------
    EXIT
XL_SetGrid                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlDiagonalDown & ').LineStyle'} = xlNone
        Excel{'Selection.Borders(' & xlDiagonalUp   & ').LineStyle'} = xlNone

        DO XL_SetBorder

        Excel{'Selection.Borders(' & xlInsideVertical & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlInsideVertical & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlInsideVertical & ').ColorIndex'} = xlAutomatic

        Excel{'Selection.Borders(' & xlInsideHorizontal & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlInsideHorizontal & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlInsideHorizontal & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorder                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_SetBorderLeft
        DO XL_SetBorderRight
        DO XL_SetBorderBottom
        DO XL_SetBorderTop
        !-----------------------------------------------
    EXIT
XL_SetBorderLeft                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeLeft & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeLeft & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeLeft & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderRight                           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeRight & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeRight & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeRight & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderBottom                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeBottom & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeBottom & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeBottom & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderTop                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeTop & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeTop & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeTop & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_HighLight                           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Interior.ColorIndex'}        = 6 ! YELLOW
        Excel{'Selection.Interior.Pattern'}           = xlSolid
        Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic
        !-----------------------------------------------
XL_SetTitle                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Interior.ColorIndex'}        = 15 ! GREY
        Excel{'Selection.Interior.Pattern'}           = xlSolid
        Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetWrapText                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.WrapText'}    = True
        !-----------------------------------------------
    EXIT
XL_SetGreyText                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Font.ColorIndex'}    = 16 ! grey
        !-----------------------------------------------
    EXIT
XL_SetBold                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Font.Bold'} = True
        !-----------------------------------------------
    EXIT
XL_FormatDate                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.NumberFormat'} = excel:DateFormat
        !-----------------------------------------------
    EXIT
XL_FormatNumber                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.NumberFormat'} = '#,##0.00'
        !-----------------------------------------------
    EXIT
XL_FormatTextBox                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.ShapeRange.IncrementLeft'} = 127.5
        Excel{'Selection.ShapeRange.IncrementTop'}  =   8.25
!        Excel{'Selection.ShapeRange.ScaleWidth'}    =  '1.76, ' & msoFalse & ', ' & msoScaleFromBottomRight
!        Excel{'Selection.ShapeRange.ScaleHeight'}   =  '0.69, ' & msoFalse & ', ' & msoScaleFromTopLeft
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentCentre            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlCenter
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentLeft              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlLeft
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentRight             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlRight
        !-----------------------------------------------
    EXIT
XL_VerticalAlignmentTop                 ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.VerticalAlignment'} = xlTop
        !-----------------------------------------------
    EXIT
XL_VerticalAlignmentCentre            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.VerticalAlignment'} = xlCenter
        !-----------------------------------------------
    EXIT
XL_SetColumnHeader                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_SetTitle
        DO XL_SetGrid
        DO XL_SetWrapText
        DO XL_SetBold
        !-----------------------------------------------
    EXIT
XL_SetColumn_Date                       ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = 'dd Mmm yyyy'

!        !Excel{'ActiveCell.FormulaR1C1'}  = LEFT(FORMAT(ret:Invoice_Date, @D8-))
        !-----------------------------------------------
    EXIT
XL_SetColumn_Number                     ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = '#,##0.00'
        !-----------------------------------------------
    EXIT
XL_SetColumn_Percent                    ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = '0.00%'
!        Excel{'ActiveCell.NumberFormat'} = CHR(64)       ! Text (AT) Sign
        !-----------------------------------------------
    EXIT
XL_SetColumn_Text                       ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'} = CHR(64) ! Text (AT) Sign
        !-----------------------------------------------
    EXIT
XL_DataSelectRange                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, ActiveCell.SpecialCells(' & xlLastCell & ')).Select'}
        !-----------------------------------------------
    EXIT
XL_DataHideDuplicates                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.AdvancedFilter Action:=' & xlFilterInPlace & ', Unique:=' & True }
        !-----------------------------------------------
    EXIT
XL_DataSortRange                            ROUTINE
    DATA
    CODE                     
        !-----------------------------------------------
        DO XL_DataSelectRange
        Excel{'ActiveWindow.ScrollColumn'} = 1
        Excel{'Selection.Sort Key1:=Range("' & LOC:Text & '"), Order1:=' & xlAscending & ', Header:=' & xlGuess & |
            ', OrderCustom:=1, MatchCase:=' & False & ', Orientation:=' & xlTopToBottom}
        !-----------------------------------------------
    EXIT
XL_DataAutoFilter                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.AutoFilter'}
        !-----------------------------------------------
    EXIT
XL_SetWorksheetLandscape                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.Orientation'}     = xlLandscape
!        Excel{'ActiveSheet.PageSetup.FitToPagesWide'} = 1
!        Excel{'ActiveSheet.PageSetup.FitToPagesTall'} = 9999
!        Excel{'ActiveSheet.PageSetup.Order'}          = xlOverThenDown
        !-----------------------------------------------
    EXIT
XL_SetWorksheetPortrait                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.Orientation'} = xlPortrait
        !-----------------------------------------------
    EXIT
XL_PrintTitleRows                               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.PrintTitleRows'} = CLIP(LOC:Text)
        !-----------------------------------------------
    EXIT
XL_GetCurrentCell                               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOC:Text = Excel{'Application.ConvertFormula( "RC", ' & xlR1C1 & ', ' & xlA1 & ')'}
        !-----------------------------------------------
    EXIT

XL_MergeCells                                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlCenter
        Excel{'Selection.VerticalAlignment'}   = xlBottom
        Excel{'Selection.MergeCells'}          = True
        !-----------------------------------------------
    EXIT

XL_GetOperatingSystem                                                          ROUTINE
    DATA
OperatingSystem     STRING(50)
tmpLen              LONG
LEN_OperatingSystem LONG
    CODE
        !-----------------------------------------------
        excel:OperatingSystem = 0.0    ! Default/error value
        OperatingSystem       = Excel{'Application.OperatingSystem'}
        LEN_OperatingSystem   = LEN(CLIP(OperatingSystem))

        LOOP x# = LEN_OperatingSystem TO 1 BY -1

            CASE SUB(OperatingSystem, x#, 1)
            OF '0' OROF'1' OROF '2' OROF '3' OROF '4' OROF '5' OROF '6' OROF '7' OROF '8' OROF '9' OROF '.'
                ! NULL
            ELSE
                tmpLen                = LEN_OperatingSystem-x#+1
                excel:OperatingSystem = CLIP(SUB(OperatingSystem, x#+1, tmpLen))

                EXIT
            END !CASE
        END !LOOP
        !-----------------------------------------------
    EXIT
!--------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020614'&'0'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('SevenDayTATReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:EXCHANGE.Open
  Relate:TRADEACC_ALIAS.Open
  Relate:WEBJOB.Open
  Access:JOBS.UseFile
  Access:SUBTRACC.UseFile
  Access:USERS.UseFile
  Access:LOCATLOG.UseFile
  Access:AUDSTATS.UseFile
  Access:TRABUSHR.UseFile
  SELF.FilesOpened = True
  BRW5.Init(?List,Queue:Browse.ViewPosition,BRW5::View:Browse,Queue:Browse,Relate:TRADEACC,SELF)
  OPEN(MainWindow)
  SELF.Opened=True
  ! ========= Set Report Version =============
  Include('..\ReportVersion.inc')
  tmp:VersionNumber = 'Version Number: ' & Clip(tmp:VersionNumber) & '5003'
  ?ReportVersion{prop:Text} = tmp:VersionNumber
      LOC:ProgramName         = 'ARC 7 Day Turnaround Time Progress Report'  ! Job=1600    Cust=in house
      MainWindow{PROP:Text}   = CLIP(LOC:ProgramName)
      ?Tab1{PROP:Text}        = CLIP(LOC:ProgramName) & ' Criteria'
  
      excel:Visible  = False
      debug:Active   = False
  
      !----------------------------------------------------------------
      ! 15 Oct 2002 John
      ! To speed up report generation add criteria to run the report from
      !   a chosen date
      !
      LOC:GreaterThanDays = 7
      LOC:StartDate       = TODAY() - 90
      LOC:EndDate         = TODAY()
      !----------------------------------------------------------------
  
      SET(defaults)
      access:defaults.next()
  
      DO GetUserName
  
      IF GUIMode = 1 THEN
         LocalTimeOut = 500
         MainWindow{PROP:ICONIZE} = TRUE
      END !IF
  
      LocalHeadAccount = GETINI('BOOKING','HEADACCOUNT','',CLIP(PATH()) & '\SB2KDEF.INI')

    If Command('/DAILY') Or Command('/WEEKLY') Or Command('/MONTHLY')
        LOC:UserName = 'AUTOMATED PROCEDURE'
        Automatic = True
    End !If Command('/DAILY') Or Command('/WEEKLY') Or Command('/MONTHLY')

      IF Automatic = TRUE
        CommandLine = CLIP(COMMAND(''))
        tmpPos = INSTRING('&', CommandLine)
        LOC:StartDate       = TODAY()-720 !DATE( MONTH(CLIP(SUB(CommandLine, tmpPos + 1, 30))), 1, YEAR(CLIP(SUB(CommandLine, tmpPos + 1, 30))) )
        LOC:EndDate       = CLIP(SUB(CommandLine, tmpPos + 1, 30)) !
       END
  
      DISPLAY
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?LOC:StartDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRW5.Q &= Queue:Browse
  BRW5.AddSortOrder(,tra:Account_Number_Key)
  BRW5.AddLocator(BRW5::Sort0:Locator)
  BRW5::Sort0:Locator.Init(,tra:Account_Number,1,BRW5)
  BRW5.SetFilter('((tra:RemoteRepairCentre = 1 OR tra:Account_Number = LocalHeadAccount) AND (tra:Account_Number <<> ''XXXRRC''))')
  BIND('LocalTag',LocalTag)
  BIND('LocalHeadAccount',LocalHeadAccount)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW5.AddField(LocalTag,BRW5.Q.LocalTag)
  BRW5.AddField(tra:Account_Number,BRW5.Q.tra:Account_Number)
  BRW5.AddField(tra:Company_Name,BRW5.Q.tra:Company_Name)
  BRW5.AddField(tra:RecordNumber,BRW5.Q.tra:RecordNumber)
      IF Automatic = TRUE
          IF loc:startdate = ''
            LOC:StartDate = TODAY()
            LOC:EndDate = TODAY()
            DoAll = 'Y'
  
          END
          loc:GreaterThanDays = 3
  
          If Command('/DAILY')
              month#  = Month(Today())
              If month# = 1
                  loc:StartDate = Deformat(Day(Today()) & '/12/' & Year(Today())-1,@d6)
              Else !If month# = 1
                  loc:StartDate = Deformat(Day(Today()) & '/' & Month(Today())-1 & '/' & Year(Today()),@d6)
              End !If month# = 1
          End !If Command('/DAILY')
          If Command('/WEEKLY')
              Loop day# = Today() To Today()-7 By -1
                  If day# %7 = 1
                      loc:StartDate = day#
                      Break
                  End !If day# %7 = 2
              End !Loop day# = Today() To Today()-7 By -1
  !            Loop day# = Today() To Today() + 7 By -1
  !                IF day# %7 = 0
  !                    loc:EndDate = day#
  !                    Break
  !                End !IF day# %7 = 0
  !            End !Loop day# = Today() To Today() + 7 By -1
          End !If Command('/WEEKLY')
          If Command('/MONTHLY')
              loc:StartDate = Deformat('1/' & Month(Today()) & '/' & Year(Today()),@d6)
              !loc:EndDate = Deformat('1/' & Month(Today()) + 1 & '/' & Year(Today()),@d6) - 1
          End !If Command('/MONTHLY')
  
  
  
          DO OKButtonPressed
          IF nbx = 1
              !POST(Event:Closewindow)
              !Call email program!
              PUTINI('MAIN','InputDir',CLIP(Loc:Path),CLIP(PATH()) & '\AUTOEMAIL.INI')
              PUTINI('MAIN','ExportDir',CLIP(Loc:Path)&'EMAILED',CLIP(PATH()) & '\AUTOEMAIL.INI')
              RUN('EMAILDIR',1)
          END
          POST(Event:CloseWindow)
      END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List{Prop:Alrt,239} = SpaceKey
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:TRADEACC_ALIAS.Close
    Relate:WEBJOB.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?DoAll
      IF DoAll = 'Y' THEN
         HIDE(?List)
         HIDE(?DASTAG)
         HIDE(?DASTAGALL)
         HIDE(?DASUNTAGALL)
      ELSE
          UNHIDE(?List)
          UNHIDE(?DASTAG)
          UNHIDE(?DASTAGALL)
          UNHIDE(?DASUNTAGALL)
      END !IF
    OF ?OkButton
          DO OKButtonPressed
    OF ?CancelButton
       POST(Event:CloseWindow)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020614'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020614'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020614'&'0')
      ***
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?PopCalendarStart
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          LOC:StartDate = TINCALENDARStyle1(LOC:StartDate)
          Display(?LOC:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?LOC:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendarStart)
      CYCLE
    END
  OF ?List
    CASE EVENT()
    OF EVENT:AlertKey
      IF RECORDS(BRW5) <> 0 AND DoAll <> 'Y' AND KeyCode() = MouseLeft2 THEN
         POST(Event:Accepted,?DASTAG)
      END !IF
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

AppendString        PROCEDURE(IN:Start, IN:Add, IN:Separator)!STRING
ReturnValue STRING(255)
    CODE
        !-----------------------------------------------------------------
        ! IN:Start,     Accumulating string
        ! IN:Add,       String to add to accumulating string
        ! IN:Separator, Separator if both non null
        !               ie ',' for comma delimited, or ' AND ' for SQL
        !               Use (literals) or (variables without excesive trailing white space).
        !-----------------------------------------------------------------
        IF CLIP(IN:Start) = ''
            ReturnValue = IN:Add
        ELSIF CLIP(IN:Add) = ''
            ReturnValue = IN:Start
        ELSE
            ReturnValue = CLIP(IN:Start) & IN:Separator & CLIP(IN:Add)
        END !IF

        RETURN ReturnValue
        !-----------------------------------------------------------------
        ! Example
        !SQL STRING(255)
        !   CODE
        !   IF ?CHOICE1
        !       SQL = '"CANCELLED" = "YES"'
        !   END !IF
        !
        !   IF ?Date1 > 0
        !       SQL = AppendString( SQL, '"DATE_BOOKED" = "' LEFT(FORMAT(job:Date_Booked, @D10)) '"', ' AND ')
        !   END !IF
        !
        !   IF ?CHOICE2
        !       SQL = AppendString( SQL, '"COMPLETE" = "YES"', ' AND ')
        !   END !IF
        !-----------------------------------------------------------------
DateReturnedToRRC       PROCEDURE()! STRING
    CODE
        !-----------------------------------------------------------------
        ! 23 Oct 2002 John
        ! R002, MICHALAN: Include a new column indicating "Date Received back at RRC" if repaired and completed at ARC
        ! Currently this function is a placeholder for when the correct algorithm has
        !   been decided.
        RETURN ''  ! DateToString(DateReturned)
        !-----------------------------------------------------------------
DateToString        PROCEDURE( IN:Date )! STRING
    CODE
        !-----------------------------------------------------------------
        RETURN LEFT(FORMAT(IN:Date, @D8))
        !-----------------------------------------------------------------
HoursBetween    PROCEDURE  (StartTime, EndTime)!LONG
Hours LONG
    CODE
        !-----------------------------------------------------------------
        IF StartTime = EndTime
            RETURN 0
        ELSIF StartTime > EndTime
            RETURN HoursBetween(EndTime, StartTime)
        END !IF

        Hours = (EndTime - StartTime) / (60 * 60 * 100)  ! 100 100ths = 1sec, 60x1sec = 1min, 60x1min=1 hour

        RETURN Hours
        !-----------------------------------------------------------------
GetDeliveryDateAtARC        PROCEDURE( IN:JobNumber )! LONG ! BOOL
!First LONG(True)
!    CODE
!        !-----------------------------------------------------------------
!        !WriteDebug('GetDeliveryDateAtARC(' & IN:JobNumber & ')')
!
!        LOC:ARCDateBooked = ''
!        First = False
!    SentToARC# = False
!
!        LOOP WHILE LoadLOCATLOG_NewLocationKey( IN:JobNumber, LOC:ARCLocation, First)
!            !-------------------------------------------------------------
!            !First = False
!            IF CLIP(lot:NewLocation) = CLIP(LOC:ARCLocation)
!                !WriteDebug('GetDeliveryDateAtARC(OK)"' & CLIP(lot:NewLocation) & '"')
!                LOC:ARCDateBooked = lot:TheDate
!                SentToARC# = True
!
!                RETURN True
!            END !IF
!            !-------------------------------------------------------------
!        END !LOOP
!
!        !WriteDebug('GetDeliveryDateAtARC(FAIL)')
!        RETURN False
!        !-----------------------------------------------------------------



!GetDeliveryDateAtARC        PROCEDURE( IN:JobNumber )! LONG ! BOOL
    CODE
    SentToARC# = False
    LOC:ARCDateBooked = ''
    Access:LOCATLOG.Clearkey(lot:DateKey)
    lot:RefNumber = in:JOBNumber
    Set(lot:DateKey,lot:DateKey)
    Loop 
        If Access:LOCATLOG.Next()
            Break
        End !
        If lot:RefNumber <> in:JobNumber 
            Break
        End !
        If lot:NewLocation = loc:ARCLocation
            SentToARC# = True
            loc:ARCDateBooked = lot:TheDate
            !MESSAGE('TRUE CONDITION MET')
            Break
        End!    
      
    End !Loop

    Return SentToARC#













!GetDeliveryDateAtARC        PROCEDURE( IN:JobNumber )! LONG ! BOOL
!First LONG(True)
!    CODE
!        !-----------------------------------------------------------------
!        !WriteDebug('GetDeliveryDateAtARC(' & IN:JobNumber & ')')
!
!        LOC:ARCDateBooked = ''
!        First = False
!
!        LOOP WHILE LoadLOCATLOG( IN:JobNumber, First)
!            !-------------------------------------------------------------
!            !First = False
!            IF CLIP(lot:NewLocation) = CLIP(LOC:ARCLocation)
!                !WriteDebug('GetDeliveryDateAtARC(OK)"' & CLIP(lot:NewLocation) & '"')
!                LOC:ARCDateBooked = lot:TheDate
!
!                RETURN True
!            END !IF
!            !-------------------------------------------------------------
!        END !LOOP
!
!        !WriteDebug('GetDeliveryDateAtARC(FAIL)')
!        RETURN False
!        !-----------------------------------------------------------------
GetHeadAccount PROCEDURE( IN:AccountNumber )
DoAdd LONG(False)
    CODE
        !-----------------------------------------------------------------  
        !WriteDebug('GetHeadAccount(' & CLIP(IN:AccountNumber) & ')')

        haq:AccountNumber  = IN:AccountNumber
        GET(HeadAccount_Queue, +haq:AccountNumber)

        CASE ERRORCODE()
        OF 00 ! FOUND
            IF haq:AccountNumber  = IN:AccountNumber
                ! Found
            ELSE
                ! Partial Match - ADD
                !DoAdd = True
            END !IF

        OF 30 ! NOT Found
            ! Not in queue - ADD
            DoAdd = True
        ELSE
            CancelPressed = True
        END !IF
        !-----------------------------------------------------------------
        IF DoAdd
            CLEAR(HeadAccount_Queue)
                haq:AccountNumber            = IN:AccountNumber

                IF LoadTRADEACC(IN:AccountNumber)
                    haq:AccountName          = tra:Company_Name
                    haq:BranchIdentification = tra:BranchIdentification

                    !IF tra:UseTimingsFrom = 'TRA'
                        IF tra:IncludeSaturday = 'YES'
                            haQ:IncludeSaturday = True
                        END !IF

                        IF tra:IncludeSunday = 'YES'
                            haQ:IncludeSunday = True
                        END !IF
                    !ELSE
                    !    IF def:Include_Saturday = 'YES'
                    !        haQ:IncludeSaturday = True
                    !    END !IF
                    !
                    !    IF def:Include_Sunday = 'YES'
                    !        haQ:IncludeSunday = True
                    !    END !IF
                    !END !IF

                ELSE
                    haq:AccountName          = '*T'
                    haq:BranchIdentification = '*T'
                END !IF

            ADD(HeadAccount_Queue, +haq:AccountNumber)
        END !IF
        !-----------------------------------------------------------------
GetReturnDateToRRC        PROCEDURE( IN:JobNumber, IN:DelivedToARCDate )! DATE
First LONG(True)
    CODE
        !-----------------------------------------------------------------
        !WriteDebug('GetReturnDateToRRC(' & IN:JobNumber & ')')

        LOC:RRCDateReturned = ''
        First = False
        Access:LocatLog.ClearKey(lot:DateKey)
            lot:RefNumber = IN:JobNumber
            lot:TheDate = IN:DelivedToARCDate
        SET(lot:DateKey,lot:DateKey)

        LOOP WHILE Access:LocatLog.NEXT() = Level:Benign
            !-------------------------------------------------------------
            IF    lot:RefNumber <> IN:JobNumber
                BREAK
            END

            IF lot:NewLocation <> LOC:RRCLocation !LOC:StatusReceivedAtRRC
              CYCLE
            END
            !MESSAGE('OK')
            IF (lot:TheDate > IN:DelivedToARCDate)
                !WriteDebug('GetReturnDateToRRC(OK) lot:TheDate"' & DateToString(lot:TheDate) & '" > IN:DelivedToARCDate"' & DateToString(IN:DelivedToARCDate) & '"')
                LOC:RRCDateReturned = lot:TheDate

                BREAK
            END !IF
            !-------------------------------------------------------------
        END !LOOP

        RETURN LOC:RRCDateReturned
        !-----------------------------------------------------------------
GetSubAccount PROCEDURE( IN:AccountNumber )
DoAdd LONG(False)
    CODE
        !-----------------------------------------------------------------
        !WriteDebug('GetSubAccount(' & CLIP(IN:AccountNumber) & ')')

        saQ:AccountNumber  = IN:AccountNumber
        GET(SubAccount_Queue, +saQ:AccountNumber)

        CASE ERRORCODE()
        OF 00 ! FOUND
            IF saQ:AccountNumber  = IN:AccountNumber
                ! Found
            ELSE
                ! Partial Match - ADD
                DoAdd = True

            END !IF
        OF 30 ! NOT Found
            ! Not in queue - ADD
            DoAdd = True

        ELSE
            CancelPressed = True

            RETURN
        END !CASE
        !-----------------------------------------------------------------
        IF DoAdd = True
            !WriteDebug('GetSubAccount(ADD)')

            CLEAR(SubAccount_Queue)
                saq:AccountNumber            = IN:AccountNumber

                IF LoadSUBTRACC(job:Account_Number)
                    GetHeadAccount(sub:Main_Account_Number)

                    saq:AccountName          = sub:Company_Name
                    saQ:HeadAccountNumber    = sub:Main_Account_Number
                    saQ:HeadAccountName      = haQ:AccountName
                    saQ:BranchIdentification = haQ:BranchIdentification

                END !IF
            ADD(SubAccount_Queue, +saq:AccountNumber)
        END !IF
!        saQ:Count += 1
!        PUT(SubAccount_Queue)
        !-----------------------------------------------------------------
LoadLOCATLOG   PROCEDURE( IN:JobNumber, IN:First )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        !WriteDebug('LoadLOCATLOG(' & IN:JobNumber & ', ' & IN:First & ')')

        IF IN:First
            IN:First = False
            !WriteDebug('LoadLOCATLOG(First)')

            Access:LOCATLOG.ClearKey(lot:DateKey)
                lot:RefNumber = IN:JobNumber
            SET(lot:DateKey, lot:DateKey)
        END !IF

        IF Access:LOCATLOG.NEXT() <> Level:Benign
            !WriteDebug('LoadLOCATLOG(NEXT=Fail)')

            RETURN False
        END !IF

        IF NOT lot:RefNumber = IN:JobNumber
            !WriteDebug('LoadLOCATLOG(NOT Found)')

            RETURN False
        END !IF

        !WriteDebug('LoadLOCATLOG(OK)')

        RETURN True
        !-----------------------------------------------
LoadLOCATLOG_NewLocationKey   PROCEDURE( IN:JobNumber, IN:NewLocation, IN:First )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        !WriteDebug('LoadLOCATLOG_NewLocationKey(' & IN:JobNumber & ', ' & IN:First & ')')

        IF IN:First
            IN:First = False
            !WriteDebug('LoadLOCATLOG_NewLocationKey(First)')

            Access:LOCATLOG.ClearKey(lot:NewLocationKey)
                lot:RefNumber   = IN:JobNumber
                lot:NewLocation = IN:NewLocation
            SET(lot:NewLocationKey, lot:NewLocationKey)
        END !IF

        IF Access:LOCATLOG.NEXT() <> Level:Benign
            !WriteDebug('LoadLOCATLOG_NewLocationKey(NEXT=Fail)')

            RETURN False
        END !IF

        IF NOT lot:RefNumber = IN:JobNumber
            !WriteDebug('LoadLOCATLOG_NewLocationKey(JobNumber NOT Found)')

            RETURN False
        END !IF

        IF NOT lot:NewLocation = IN:NewLocation
            !WriteDebug('LoadLOCATLOG_NewLocationKey(NewLoaction NOT Found)')

            RETURN False
        END !IF
               
        !WriteDebug('LoadLOCATLOG_NewLocationKey(OK)')

        RETURN True
        !-----------------------------------------------
LoadEngineer    PROCEDURE(IN:UserCode)!,BYTE BOOL
    CODE
        !-----------------------------------------------------------------
        SET(USERS)
        Access:USERS.Clearkey(use:User_Code_Key)
            use:User_Code = IN:UserCode
        SET(use:User_Code_Key, use:User_Code_Key)

        If Access:USERS.NEXT() <> Level:Benign
            RETURN False
        END !IF

        IF NOT use:User_Code = IN:UserCode
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------------------------
LoadEXCHANGE   PROCEDURE( IN:ExchangeUnitNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
        xch:Ref_Number = xch:Ref_Number

        IF Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) <> Level:Benign
            RETURN False
        END !IF

        IF NOT xch:Ref_Number = xch:Ref_Number
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadSUBTRACC   PROCEDURE( IN:AccountNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = IN:AccountNumber

        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key) <> Level:Benign
            !WriteDebug('LoadSUBTRACC(' & CLIP(IN:AccountNumber) & ') EOF')

            RETURN False
        END !IF

        IF sub:Account_Number <> IN:AccountNumber
            !WriteDebug('LoadSUBTRACC(' & CLIP(IN:AccountNumber) & ') EOI')

            RETURN False
        END !IF

        !WriteDebug('LoadSUBTRACC(' & CLIP(IN:AccountNumber) & ') OK')
        RETURN True
        !-----------------------------------------------
LoadSUBTRACCByTradeAcc_Alias   PROCEDURE( IN:AccountNumber, INOUT:First )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        IF INOUT:First
            INOUT:First= False

            Access:SUBTRACC.ClearKey(sub:Main_Account_Key)
            sub:Main_Account_Number = IN:AccountNumber
        END !IF

        IF Access:SUBTRACC.TryFetch(sub:Main_Account_Key) <> Level:Benign
            RETURN False
        END !IF

        IF sub:Main_Account_Number = IN:AccountNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadTRADEACC   PROCEDURE( IN:AccountNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = IN:AccountNumber

        IF Access:TRADEACC.TryFetch(tra:Account_Number_Key) <> Level:Benign
            !WriteDebug('LoadTRADEACC(' & CLIP(IN:AccountNumber) & ') EOF')
            RETURN False
        END !IF

        IF tra:Account_Number <> IN:AccountNumber
            !WriteDebug('LoadTRADEACC(' & CLIP(IN:AccountNumber) & ') EOI')

            RETURN False
        END !IF

        !WriteDebug('LoadTRADEACC(' & CLIP(IN:AccountNumber) & ') OK')
        RETURN True
        !-----------------------------------------------
LoadWEBJOB          PROCEDURE( IN:JobNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:WEBJOB.CLEARKEY(wob:RefNumberKey)
        wob:RefNumber = IN:JobNumber

        IF Access:WEBJOB.Fetch(wob:RefNumberKey) = Level:Benign
            !WriteDebug('LoadWEBJOB(' & IN:JobNumber & ')OK')
            RETURN True
        END !IF

        !WriteDebug('LoadWEBJOB(' & IN:JobNumber & ')FAIL')

        RETURN False
        !-----------------------------------------------

NumberToColumn PROCEDURE(IN:Long)! STRING
FirstCol STRING(1)
Temp     LONG
    CODE
        !-----------------------------------------------
        Temp = IN:Long - 1

        FirstCol = Temp / 26
        IF FirstCol = 0
            RETURN CHR(Temp+65)
        END !IF

        RETURN CHR(FirstCol+64) & CHR( (Temp % 26)+65 )
        !-----------------------------------------------
UpdateEngineerQueue     PROCEDURE(IN:UserCode)
    CODE
        !-----------------------------------------------------------------
        engq:UserCode  = IN:UserCode
        GET(EngineerQueue, +engq:UserCode)
        IF ERRORCODE()
            ! Not in queue - ADD
            IF LoadEngineer(IN:UserCode)
                engq:Forename   = use:Forename
                engq:Surname    = use:Surname
            ELSE
                engq:Forename   = ''
                engq:Surname    = ''
            END !IF
            engq:UserCode       = IN:UserCode
            engq:JobsBooked     = 0

            ADD(EngineerQueue, +engq:UserCode)
        END !IF
        !-----------------------------------------------------------------
        ! In queue - AMEND
        engq:JobsBooked += 1

        PUT(EngineerQueue, +engq:UserCode)
        !-----------------------------------------------------------------
UpdateSummaryQueue PROCEDURE( IN:Status, IN:StatusType )
    CODE
        !-----------------------------------------------------------------
        sq:Status  = IN:Status
        sq:Type    = IN:StatusType
        GET(SummaryQueue, +sq:Status, +sq:Type)
        IF ERRORCODE()
            ! Not in queue - ADD
            sq:Status       = IN:Status
            sq:Type         = IN:StatusType
            sq:Count        = 0

            ADD(SummaryQueue, +sq:Status, +sq:Type)
        END !IF
        !-----------------------------------------------------------------
        ! In queue - AMEND
        sq:Count += 1

        PUT(SummaryQueue, +sq:Status, +sq:Type)
        !-----------------------------------------------------------------
WorkingDaysBetween PROCEDURE  ( IN:StartDate, IN:EndDate, IN:IncludeSaturday, IN:IncludeSunday )!,long ! Declare Procedure
Weeks        LONG

DaysDiff     LONG
Days         LONG(0)

DaysPerWeek  LONG(5)
TempDate1    DATE
TempDate2    DATE
    CODE
        !-------------------------------------------
        IF (IN:StartDate > IN:EndDate)
            RETURN WorkingDaysBetween(IN:EndDate, IN:StartDate, IN:IncludeSaturday, IN:IncludeSunday)
        END !IF
        !-------------------------------------------
        IF (IN:StartDate = IN:EndDate)
            RETURN Days
        END !IF
        !-------------------------------------------
        IF IN:IncludeSaturday
            DaysPerWeek += 1
        END !IF

        IF IN:IncludeSunday
            DaysPerWeek += 1
        END !IF
        !-------------------------------------------
        TempDate1 = IN:StartDate
        
        LOOP
            CASE (TempDate1 % 7)
            OF 0
                If IN:IncludeSunday
                    Days += 1
                End !If
            OF 5
                BREAK
            OF 6
                If IN:IncludeSaturday
                    Days += 1
                End !If
            ELSE
                Days += 1
            END !CASE

            TempDate1 += 1
            If (TempDate1 = IN:EndDate) Then
                RETURN Days
            End !If

        END !LOOP
        !-------------------------------------------
        TempDate2     = IN:EndDate
        
        LOOP
            CASE (TempDate2 % 7)
            OF 0
                If IN:IncludeSunday
                    Days += 1
                End !If
            OF 5
                BREAK
            OF 6
                If IN:IncludeSaturday
                    Days += 1
                End !If
            ELSE
                Days += 1
            END !CASE

            TempDate2 -= 1
            If (TempDate1 = TempDate2) Then
                RETURN Days
            End !If

        END !LOOP
        !-------------------------------------------
        Weeks = (TempDate2 - TempDate1) / 7
        !-------------------------------------------
!        message('DaysBefore           =' & Days                  & '<13,10>' & |
!                '(Weeks * DaysPerWeek)=' & (Weeks * DaysPerWeek) & '<13,10>' & |
!                ' ===================== <13,10>'                             & |
!                'TOTAL                =' & Days + (Weeks * DaysPerWeek) )

        RETURN Days + (Weeks * DaysPerWeek)
        !-------------------------------------------
WorkingHoursBetween PROCEDURE  (StartDate, EndDate, StartTime, EndTime, IN:IncludeSaturday, IN:IncludeSunday) ! LONG
DaysBetween   LONG
Hours         LONG
    CODE
        DaysBetween = WorkingDaysBetween(StartDate, EndDate, IN:IncludeSaturday, IN:IncludeSunday)
        Hours       = 0

        IF DaysBetween = 0
            Hours += HoursBetween(StartTime, EndTime)
        ELSIF DaysBetween = 1
            Hours += HoursBetween(StartTime,            def:End_Work_Hours)
            Hours += HoursBetween(def:Start_Work_Hours, EndTime           )
        ELSE
            Hours  = (DaysBetween - 1) * LOC:WorkingHours

            Hours += HoursBetween(StartTime,            def:End_Work_Hours)
            Hours += HoursBetween(def:Start_Work_Hours, EndTime           )
        END !IF

        RETURN Hours
WriteColumn PROCEDURE( IN:String, IN:StartNewRow )
Temp STRING(255)
    CODE
        !-----------------------------------------------
        Temp = IN:String
        IF CLIP(Temp) = ''
            Temp = ''' '
        END !IF
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            IF IN:StartNewRow
                clip:Value = Temp
            ELSE
                clip:Value = CLIP(clip:Value) & '<09>' & Temp
            END !IF

            RETURN
        END !IF
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}  = Temp

        DO XL_ColRight
        !-----------------------------------------------
WriteDebug      PROCEDURE( IN:Message )
    CODE
        !-----------------------------------------------
        !IF NOT debug:Active
            RETURN
        !END !IF

       !debug:Count += 1

        !PUTINI(CLIP(LOC:ProgramName) & ' '  & CLIP(tra_ali:Account_Number), debug:Count, IN:Message, 'C:\Debug.ini')
        !-----------------------------------------------
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW5.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = tra:RecordNumber
     GET(glo:Queue2,glo:Queue2.Pointer2)
    IF ERRORCODE()
      LocalTag = ''
    ELSE
      LocalTag = 'Y'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (LocalTag = 'Y')
    SELF.Q.LocalTag_Icon = 2
  ELSE
    SELF.Q.LocalTag_Icon = 1
  END


BRW5.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW5.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW5.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW5::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW5::RecordStatus=ReturnValue
  IF BRW5::RecordStatus NOT=Record:OK THEN RETURN BRW5::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = tra:RecordNumber
     GET(glo:Queue2,glo:Queue2.Pointer2)
    EXECUTE DASBRW::6:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW5::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW5::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW5::RecordStatus
  RETURN ReturnValue

