

   MEMBER('vodr0083.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('VODR0002.INC'),ONCE        !Local module procedure declarations
                     END


SparesForecastingReport PROCEDURE                          ! Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::12:TAGFLAG         BYTE(0)
DASBRW::12:TAGMOUSE        BYTE(0)
DASBRW::12:TAGDISPSTATUS   BYTE(0)
DASBRW::12:QUEUE          QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
locEndDate           DATE                                  !
locTrue              BYTE(1)                               !
locTag               STRING(1)                             !
locClipboard         ANY                                   !
locNumberOfDays      LONG                                  !
locIntervalPeriod    LONG                                  !
locHeadAccountNumber STRING(30)                            !
locARCLocation       STRING(30)                            !
locARCCompanyName    STRING(30)                            !
locMainStoreLocation STRING(30)                            !
locUserName          STRING(60)                            !
tmp:VersionNumber    STRING(30)                            !
locVersion           STRING(30)                            !
excelFilename        CSTRING(255)                          !
excelProgramName     STRING(60)                            !
locPartsExportedCount LONG                                 !
locExchangeExportedCount LONG                              !
progress:Thermometer BYTE                                  !
locFilePath          CSTRING(255)                          !
statusQueue          QUEUE,PRE(staque)                     !
statusMessage        STRING(60)                            !
                     END                                   !
csvQueue             QUEUE,PRE(csv)                        !
location             STRING(20)                            !
recordCount          STRING(20)                            !
filename             STRING(255)                           !
lastCell             STRING(30)                            !
                     END                                   !
BRW10::View:Browse   VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                       PROJECT(loc:Active)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
locTag                 LIKE(locTag)                   !List box control field - type derived from local data
locTag_Icon            LONG                           !Entry's icon ID
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
loc:Active             LIKE(loc:Active)               !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('ServiceBase 3g'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular),COLOR(0D6EAEFH),IMM,ICON('Cellular3g.ico'),HLP('SparesForecastingReport'),WALLPAPER('sbback.jpg'),TILED,SYSTEM,GRAY,DOUBLE
                       PANEL,AT(64,39,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PROMPT('Spares Forecasting Report'),AT(67,42),USE(?WindowTitle:2),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                       PROMPT('SRN:000000'),AT(565,42),USE(?SRNNumber),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold)
                       PANEL,AT(64,359,552,31),USE(?PanelButton),FILL(09A6A7CH)
                       BUTTON,AT(552,362),USE(?Cancel),FLAT,RIGHT,COLOR(09A6A7CH),MSG('Cancel Operation'),TIP('Cancel Operation'),ICON('cancelp.jpg')
                       PANEL,AT(64,52,552,303),USE(?PANEL100),FILL(09A6A7CH),BEVEL(1)
                       PROMPT('Number Of Days'),AT(141,180),USE(?locNumberOfDays:Prompt),RIGHT,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                       ENTRY(@s8),AT(246,180,64,10),USE(locNumberOfDays),RIGHT(2),FONT(,,COLOR:Black,FONT:bold),COLOR(COLOR:White)
                       PROMPT('Interval Period'),AT(141,212),USE(?locIntervalPeriod:Prompt),RIGHT,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                       ENTRY(@s8),AT(246,212,64,10),USE(locIntervalPeriod),RIGHT(2),FONT(,,COLOR:Black,FONT:bold),COLOR(COLOR:White)
                       PROMPT('End Date'),AT(144,244),USE(?locEndDate:Prompt),CENTER,FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                       ENTRY(@d17),AT(246,244,64,10),USE(locEndDate),RIGHT(2),FONT(,,COLOR:Black,FONT:bold),COLOR(COLOR:White)
                       LIST,AT(401,84,209,235),USE(?List),IMM,FONT(,,,FONT:regular),COLOR(COLOR:White),FORMAT('10L(2)J@s1@120L(2)|M~Location~@s30@'),FROM(Queue:Browse)
                       BUTTON,AT(400,325),USE(?DASTAG),FLAT,COLOR(09A6A7CH),ICON('tagitemp.jpg')
                       BUTTON,AT(536,325),USE(?DASTAGAll),FLAT,COLOR(09A6A7CH),ICON('tagallp.jpg')
                       BUTTON,AT(467,325),USE(?DASUNTAGALL),FLAT,COLOR(09A6A7CH),ICON('untagalp.jpg')
                       BUTTON('xx'),AT(236,126,8,10),USE(?DASREVTAG),HIDE
                       BUTTON('xx'),AT(192,110,5,5),USE(?DASSHOWTAG),HIDE
                       PROMPT('Report Version:'),AT(69,369),USE(?ReportVersion),FONT(,,COLOR:White,FONT:bold),COLOR(09A6A7CH)
                       BUTTON,AT(315,244),USE(?Calendar),FLAT,COLOR(09A6A7CH),ICON('lookupp.jpg')
                       BUTTON,AT(480,362),USE(?buttonPrint),FLAT,COLOR(09A6A7CH),ICON('printp.jpg')
                       BUTTON,AT(651,2),USE(?ButtonHelp),TRN,FLAT,ICON('F1Helpsw.jpg')
                     END

    !Progress Window (DBH: 22-03-2004)
rejectrecord        long
recordstoprocess    long,auto
recordsprocessed    long,auto
recordspercycle     long,auto
recordsthiscycle    long,auto
percentprogress     byte
recordstatus        byte,auto
tmp:cancel          byte

progresswindow      WINDOW('Progress...'),AT(0,0,680,428),CENTER,ICON('cellular3g.ico'),FONT('Tahoma', 8,, FONT:regular),GRAY,TIMER(1),IMM,COLOR(00D6EAEFh),DOUBLE
                        PANEL, AT(160,64,360,300), USE(?Panel5), FILL(00D6E7EFh)
                        PANEL, AT(164,68,352,12), USE(?Panel1), FILL(009A6A7Ch)
                        LIST, AT(208,88,268,206), USE(?List1), FONT(,, 00010101h,), FORMAT('20L(2)|M'), |
                            FLAT, COLOR(COLOR:White, COLOR:White, 009A6A7Ch), VSCROLL, GRID(COLOR:WHITE), FROM(StatusQueue)
                        PANEL, AT(164,84,352,246), USE(?Panel4), FILL(009A6A7Ch)
                        PROGRESS, AT(206,314,268,12), USE(progress:thermometer), RANGE(0,100)
                        STRING(''), AT(259,300,161,10), USE(?progress:userstring), FONT('Arial', 8, 0080FFFFh, FONT:bold), |
                            COLOR(009A6A7Ch), CENTER
                        STRING(''), AT(232,136,161,10), USE(?progress:pcttext), FONT('Arial', 8), HIDE, TRN, CENTER
                        PANEL, AT(164,332,352,28), USE(?Panel2), FILL(009A6A7Ch)
                        PROMPT('Report Progress'), AT(168,70), USE(?WindowTitle2), FONT('Tahoma', 8, COLOR:White, FONT:bold), |
                            TRN
                        BUTTON, AT(444,332), USE(?ProgressCancel), ICON('Cancelp.jpg'), FLAT, LEFT, TRN
                        BUTTON, AT(376,332), USE(?Button:OpenReportFolder), ICON('openrepp.jpg'), HIDE, FLAT, TRN
                        BUTTON, AT(444,332), USE(?Finish), ICON('Finishp.jpg'), HIDE, FLAT, TRN
                    END
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED                 ! Method added to host embed code
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
e1                   Class(oiExcel)
Init                   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0),byte,proc,virtual,name('INIT@F17OIEXCEL')
Kill                   PROCEDURE (byte pUnloadCOM=1),byte,proc,virtual,name('KILL@F17OIEXCEL')
TakeEvent              PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0),virtual,name('TAKEEVENT@F17OIEXCEL')
                     End

BRW10                CLASS(BrowseClass)                    ! Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Fetch                  PROCEDURE(BYTE Direction),DERIVED   ! Method added to host embed code
SetQueueRecord         PROCEDURE(),DERIVED                 ! Method added to host embed code
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
ValidateRecord         PROCEDURE(),BYTE,DERIVED            ! Method added to host embed code
                     END

BRW10::Sort0:Locator StepLocatorClass                      ! Default Locator
Calendar13           CalendarClass
                            !Export Files (DBH: 22-03-2004)
glo:ExportFile      STRING(255),STATIC
ExportFile          File,Driver('ASCII'),Pre(exp),Name(glo:ExportFile),Create,Bindable,Thread
Record                  Record
Line                        String(10000)
                        End
                    End
    ! Class
locClass            CLASS
updateProgressWindow    PROCEDURE(STRING fText)
DrawBox                 PROCEDURE(String fTL,String fTR,String fBL,String fBR,Long fColour)
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::12:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW10.UpdateBuffer
   glo:Queue.Pointer = loc:Location
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = loc:Location
     ADD(glo:Queue,glo:Queue.Pointer)
    locTag = '*'
  ELSE
    DELETE(glo:Queue)
    locTag = ''
  END
    Queue:Browse.locTag = locTag
  IF (locTag = '*')
    Queue:Browse.locTag_Icon = 2
  ELSE
    Queue:Browse.locTag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::12:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW10.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW10::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = loc:Location
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW10.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::12:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW10.Reset
  SETCURSOR
  BRW10.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::12:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::12:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::12:QUEUE = glo:Queue
    ADD(DASBRW::12:QUEUE)
  END
  FREE(glo:Queue)
  BRW10.Reset
  LOOP
    NEXT(BRW10::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::12:QUEUE.Pointer = loc:Location
     GET(DASBRW::12:QUEUE,DASBRW::12:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = loc:Location
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW10.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::12:DASSHOWTAG Routine
   CASE DASBRW::12:TAGDISPSTATUS
   OF 0
      DASBRW::12:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::12:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::12:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW10.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
Reporting           Routine  ! Do the Report (DBH: 22-03-2004)
Data
local:LoopCount     Long()
local:LocalPath     String(255)
local:CurrentAccount        String(30)
local:ReportStartDate       Date()
local:ReportStartTime       Time()
local:CountJobsExported     Long()
local:DateStatusChanged     Date()
local:Interval      Long
local:RecordCount   Long
local:FirstTimeInLoop       Long()
local:IntervalLoop  Long()
local:QuantityUsedOnJobs    Long()
local:QuantitySoldByMainStore       Long()
local:QuantityOnBackOrder   Long()
local:QuantityOnHand        Long()
local:QuantityRetailOnBackOrder     Long()
local:SiteLocationAccount   String(30)
local:JobsWithSpares        Long()
local:Desktop       Cstring(255)

locBuildJobsQueue   Byte(0)
qJobNumbers QUEUE
jobNumber       LONG()
cJob            Byte(0)
wJob            Byte(0)
            END

Code

        ! Create Folder In My Documents
    excelProgramName = 'Spares Forecast Report'
    SHGetSpecialFolderPath( GetDesktopWindow(), local:Desktop, 5, FALSE )
    local:Desktop = Clip(local:Desktop) & '\ServiceBase Export'

        !Does the Export Folder already Exists?
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Message('An error has occured finding, or creating the folder for the report.'&|
                '|'&|
                '|' & Clip(local:Desktop) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                Icon:Hand,'&OK',1)
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    local:Desktop = Clip(local:Desktop) & '\' & Clip(excelProgramName)
    If ~Exists(Clip(local:Desktop))
        If MkDir(local:Desktop)
            Beep(Beep:SystemHand)  ;  Yield()
            Case Message('An error has occured finding, or creating the folder for the report.'&|
                '|'&|
                '|' & Clip(local:Desktop) & ''&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                Icon:Hand,'&OK',1)
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If MkDir(local:Desktop)
    End !If Exists(Clip(local:Desktop))

    excelFileName = Clip(local:Desktop) & '\' & Clip(excelProgramName) & ' ' & Format(Today(),@d12)

    If Exists(excelFileName & '.xls')
        Remove(excelFileName & '.xls')
        If EXISTS(excelFileName & '.xls')
            Beep(Beep:SystemHand)  ;  Yield()
            Case Message('Cannot get access to the Excel Document. Make sure it is not open, then try again.','ServiceBase',|
                Icon:Hand,'&OK',1)
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End !If Error()
    End !If Exists(excelFileName)

    If locNumberOfDays % locIntervalPeriod <> 0
        Beep(Beep:SystemHand)  ;  Yield()
        Case Message('The entered "Number Of Days" and "Interval Period" are invalid.','ServiceBase',|
            Icon:Hand,'&OK',1)
            Of 1 ! &OK Button
        End!Case Message
        Exit
    End ! If locNumberOfDays % locIntervalPeriod <> 0

        ! Set the temp folder for the csv files (DBH: 10-03-2004)
    If GetTempPathA(255, locFilePath)
        If Sub(locFilePath, -1, 1) = '\'
            local:LocalPath = Clip(locFilePath) & ''
        Else ! If Sub(locFilePath,-1,1) = '\'
            local:LocalPath = Clip(locFilePath) & '\'
        End ! If Sub(locFilePath,-1,1) = '\'
    End ! If

    If COMMAND('/DEBUG')
        Remove('c:\debug.ini')
    End ! If COMMAND('/DEBUG')

        ! Open Program Window (DBH: 10-03-2004)
    recordspercycle      = 25
    recordsprocessed     = 0
    percentprogress      = 0
    progress:thermometer = 0
    recordstoprocess     = 50
    Open(ProgressWindow)

    ?progress:userstring{prop:text} = 'Running...'
    ?progress:pcttext{prop:text}    = '0% Completed'


    local:ReportStartDate = Today()
    local:ReportStartTime = Clock()

    If Command('/DEBUG')
        locClass.UpdateProgressWindow('========')
        locClass.UpdateProgressWindow('*** DEBUG MODE ***')
        locClass.UpdateProgressWindow('========')
        locClass.UpdateProgressWindow('')
    End ! If Command('/DEBUG')

    locClass.UpdateProgressWindow('Report Started: ' & Format(local:ReportStartDate, @d6b) & ' ' & Format(local:ReportStartTime, @t1b))
    locClass.UpdateProgressWindow('')
    locClass.UpdateProgressWindow('Creating Export Files...')

        ! _____________________________________________________________________

        ! CREATE CSV FILES

        ! _____________________________________________________________________



    local:Interval = locNumberOfDays / locIntervalPeriod

    Loop local:LoopCount = 1 To Records(glo:Queue)
        Get(glo:Queue, local:LoopCount)

        locClass.UpdateProgressWindow(Clip(glo:Pointer) & ': Checking Quantity Used')

        glo:ExportFile = Clip(local:LocalPath) & Clip(glo:Pointer) & '-SPARESFORETMP.CSV'
        local:RecordCount = 0
        Remove(ExportFile)
        Create(ExportFile)
        Open(ExportFile)

        Clear(exp:record)
        exp:Line = '"","","","","",""'

        Loop x# = locNumberOfDays to local:Interval By - local:Interval
            exp:Line = Clip(exp:Line) & ',"' & Clip(x# - local:Interval) & ' - ' & Clip(x#) & ' Days"'
                ! Inserting (DBH 18/08/2008) # 9798 - Only show on Main Store Tab
            If glo:Pointer = 'MAIN STORE'
                    ! End (DBH 18/08/2008) #9798
                exp:Line = Clip(exp:Line) & ',""'
            End ! exp:Line = Clip(exp:Line) & '                                         !this is an extra (many) column for main store but this is only a title
        End ! Loop x# = locNumberOfDays to local:Interval By - local:Interval
        exp:Line = Clip(exp:Line) & ',"","","","",""'
        Add(ExportFile)

        Clear(exp:Record)
        exp:Line = '"Manufacturer","Supplier"'
        exp:Line = Clip(exp:Line) & ',"Part Number"'
        exp:Line = Clip(exp:Line) & ',"Description"'
        exp:Line = Clip(exp:Line) & ',"Selected Location Average Cost"'
        exp:Line = Clip(exp:Line) & ',"Selected Location Bin Number"'

        Loop x# = locNumberOfDays to local:Interval By - local:Interval
            exp:Line = Clip(exp:Line) & ',"Quantity Used On Jobs"'
                ! Inserting (DBH 18/08/2008) # 9798 - Only show on Main Store Tab
            If glo:Pointer = 'MAIN STORE'
                    ! End (DBH 18/08/2008) #9798
                exp:Line = Clip(exp:Line) & ',"Quantity Sold By Main Store"'           !this is first extra line for main store
            End ! If glo:Pointer = 'MAIN STORE'

        End ! Loop x# = locNumberOfDays to local:Interval By - local:Interval
        exp:Line = CLip(exp:Line) & ',"Quantity On Retail Backorder at Main Store"'
        exp:Line = Clip(exp:Line) & ',"Selected Location Stock On Hand"'
            ! Inserting (DBH 18/08/2008) # 9798 - Only show on Main Store Tab
        If glo:Pointer = 'MAIN STORE'
                ! End (DBH 18/08/2008) #9798
                ! -----------------------------------------
                ! DBH 16/10/2008 #10516
                ! Inserting: Re-instate this column
            exp:Line = Clip(exp:Line) & ',"ARC Stock On Hand"'                         !this is second extra line for main store
                ! End: DBH 16/10/2008 #10516
                ! -----------------------------------------                            !this is third extra line for main store
            exp:Line = Clip(exp:Line) & ',"Total Stock On Hand Nationally"'
        End ! If glo:Pointer = 'MAIN STORE''
            ! Inserting (DBH 18/08/2008) # 9798 - Only show for Main Store tab
        If glo:Pointer = 'MAIN STORE'
                ! End (DBH 18/08/2008) #9798
            exp:Line = Clip(exp:Line) & ',"Main Store Quantity on Order With Supplier"'    !this is fourth extra line for main store
        End ! If glo:Pointer = 'MAIN STORE'
            ! Inserting (DBH 20/04/2006) #7304 - Show how many jobs waiting for part
        exp:Line = Clip(exp:Line) & ',"Count Of Jobs Waiting For Spare Part"'
            ! End (DBH 20/04/2006) #7304
        Add(ExportFile)

        Access:STOCK.Clearkey(sto:Location_Key)
        sto:Location = glo:Pointer
        Set(sto:Location_Key, sto:Location_Key)
        Loop ! Begin STOCK Loop
            If Access:STOCK.Next()
                Break
            End ! If !Access
            If sto:Location <> glo:Pointer
                Break
            End ! If
            if (sto:Suspend)
                CYCLE
            end

            Loop sta# = 1 To Records(StatusQueue)
                Get(StatusQueue,sta#)
                If Instring(Clip(glo:Pointer),staque:StatusMessage,1,1)
                    staque:StatusMessage = Clip(glo:Pointer) & ': Checking Quantity Used'
                    Put(StatusQueue)
                    Select(?List1,Records(StatusQueue))
                    Display()
                    Break
                End ! If Instring(glo:Pointer,staque:StatusMessage,1,1)
            End ! Loop sta# = 1 To Records(StatusQueue)

            Do GetNextRecord2

            Do CancelCheck
            If tmp:Cancel
                Break
            End ! If tmp:Cancel

            Clear(exp:Record)
                !Manufacturer
            exp:Line = '"' & Clip(sto:Manufacturer)
                !Supplier
            exp:Line = Clip(exp:Line) & '","''' & Clip(sto:Supplier)
                !Part Number
            exp:Line = Clip(exp:Line) & '","''' & Clip(sto:Part_Number)
                !Description
            exp:Line = Clip(exp:Line) & '","' & Clip(sto:Description)
                !Purchase Cost
            If glo:Pointer = 'MAIN STORE'
                exp:Line = Clip(exp:Line) & '","' & Format(sto:Purchase_Cost,@n_14.2)
            Else ! If glo:Pointer = 'MAIN STORE'
                exp:Line = Clip(exp:Line) & '","' & Format(sto:AveragePurchaseCost,@n_14.2)
            End ! If glo:Pointer = 'MAIN STORE'
                !Bin Location
            exp:Line = Clip(exp:Line) & '","' & Clip(sto:Shelf_Location)

            local:FirstTimeInLoop = True


            Loop local:IntervalLoop = locNumberOfDays to local:Interval By -local:Interval

                    ! _____________________________________________________________________
                    !Quantity Used On Jobs

                local:QuantityUsedOnJobs = 0

                If glo:Pointer = 'MAIN STORE'
                        ! Check where this part has been used at ALL sites - TrkBs: 5805 (DBH: 28-07-2005)
                    Access:LOCATION.ClearKey(loc:ActiveLocationKey)
                    loc:Active   = True
                    Set(loc:ActiveLocationKey,loc:ActiveLocationKey)
                    Loop
                        If Access:LOCATION.NEXT()
                            Break
                        End !If
                        If loc:Active   <> True      |
                            Then Break.  ! End If

                        ! Do not count the MAIN STORE as it will never use parts on jobs - TrkBs: 6247 (DBH: 15-09-2005)
                        If loc:Location = 'MAIN STORE'
                            Cycle
                        End ! If loc:Location = 'MAIN STORE'

                        Access:STOCK_ALIAS.ClearKey(sto_ali:Location_Key)
                        sto_ali:Location    = loc:Location
                        sto_ali:Part_Number = sto:Part_Number
                        Set(sto_ali:Location_Key,sto_ali:Location_Key)
                        Loop
                            If Access:STOCK_ALIAS.NEXT()
                                Break
                            End !If
                            If sto_ali:Location    <> loc:Location      |
                                Or sto_ali:Part_Number <> sto:Part_Number      |
                                Then Break.  ! End If
                            if (sto_ali:Suspend)
                                CYCLE
                            end

                            Access:STOHIST.Clearkey(shi:Transaction_Type_Key)
                            shi:Ref_Number       = sto_ali:Ref_Number
                            shi:Transaction_Type = 'DEC'
                            If local:FirstTimeInLoop
                                shi:Date = locEndDate - local:IntervalLoop
                            Else ! If FirstTime#
                                shi:date = locEndDate - local:IntervalLoop - 1
                            End ! If FirstTime#
                            Set(shi:Transaction_Type_Key, shi:Transaction_Type_Key)
                            Loop ! Begin STOHIST Loop
                                If Access:STOHIST.Next()
                                    Break
                                End ! If !Access
                                If shi:Ref_Number <> sto_ali:Ref_Number
                                    Break
                                End ! If
                                If shi:Transaction_Type <> 'DEC'
                                    Break
                                End ! If
                                If shi:Date > (locEndDate - local:IntervalLoop) + local:Interval
                                    Break
                                End ! If shi:Date > (locEndDate - x#) + local:Interval

                                If shi:Job_Number > 0
                                    local:QuantityUsedOnJobs += shi:Quantity
                                End ! If shi:Job_Number = ''

                                Do GetNextRecord2

                                Do CancelCheck
                                If tmp:Cancel
                                    Break
                                End ! If tmp:Cancel


                            End ! End STOHIST Loop
                            If tmp:Cancel
                                Break
                            End ! If tmp:Cancel
                        End !Loop
                        If tmp:Cancel
                            Break
                        End ! If tmp:Cancel
                    End !Loop
                Else ! If glo:Pointer = 'MAIN STORE'
                    Access:STOHIST.Clearkey(shi:Transaction_Type_Key)
                    shi:Ref_Number       = sto:Ref_Number
                    shi:Transaction_Type = 'DEC'
                    If local:FirstTimeInLoop
                        shi:Date = locEndDate - local:IntervalLoop
                        If Command('/DEBUG')
                            If sto:Part_Number = 'L36851-Z2002-A40'
                                PUTINI(Clip(sto:Part_Number) & local:IntervalLoop,'Start Date',Format(locEndDate - local:IntervalLoop,@d6),'c:\debug.ini.INI')
                                PUTINI(Clip(sto:Part_Number) & local:IntervalLoop,'End Date',Format((locEndDate - local:IntervalLoop) + local:Interval,@d6),'c:\debug.ini.INI')
                            End ! If sto:Part_Number = 'L36851-Z2002-A40'
                        End ! If Command('/DEBUG')
                    Else ! If FirstTime#
                        shi:date = locEndDate - local:IntervalLoop + 1
                        If Command('/DEBUG')
                            If sto:Part_Number = 'L36851-Z2002-A40'
                                PUTINI(Clip(sto:Part_Number) & local:IntervalLoop,'Start Date',Format(locEndDate - local:IntervalLoop + 1,@d6),'c:\debug.ini.INI')
                                PUTINI(Clip(sto:Part_Number) & local:IntervalLoop,'End Date',Format((locEndDate - local:IntervalLoop) + local:Interval,@d6),'c:\debug.ini.INI')
                            End ! If sto:Part_Number = 'L36851-Z2002-A40'
                        End ! If Command('/DEBUG')
                    End ! If FirstTime#
                    Set(shi:Transaction_Type_Key, shi:Transaction_Type_Key)
                    Loop ! Begin STOHIST Loop
                        If Access:STOHIST.Next()
                            Break
                        End ! If !Access
                        If shi:Ref_Number <> sto:Ref_Number
                            Break
                        End ! If
                        If shi:Transaction_Type <> 'DEC'
                            Break
                        End ! If
                        If shi:Date > (locEndDate - local:IntervalLoop) + local:Interval
                            Break
                        End ! If shi:Date > (locEndDate - x#) + local:Interval

                        If Command('/DEBUG')
                            If sto:Part_Number = 'L36851-Z2002-A40'
                                PUTINI(Clip(sto:Part_Number) & local:IntervalLoop,Clip(shi:Record_Number) & ': Quantity',shi:Quantity,'c:\debug.ini.INI')
                            End ! If sto:Part_Number = 'L36851-Z2002-A40'
                        End ! If Command('/DEBUG')

                        If shi:Job_Number > 0
                            local:QuantityUsedOnJobs += shi:Quantity
                        End ! If shi:Job_Number = ''

                        Do GetNextRecord2

                        Do CancelCheck
                        If tmp:Cancel
                            Break
                        End ! If tmp:Cancel


                    End ! End STOHIST Loop
                End ! If glo:Pointer = 'MAIN STORE'
                If tmp:Cancel
                    Break
                End ! If tmp:Cancel

                exp:Line = Clip(exp:Line) & '","' & local:QuantityUsedOnJobs

                    ! _____________________________________________________________________
                    !Quantity Sold By Main Store

                If glo:Pointer = 'MAIN STORE'
                    local:QuantitySoldByMainStore = 0
                    Access:STOCK_ALIAS.ClearKey(sto_ali:Location_Key)
                    sto_ali:Location    = 'MAIN STORE'
                    sto_ali:Part_Number = sto:Part_Number
                    If Access:STOCK_ALIAS.TryFetch(sto_ali:Location_Key) = Level:Benign
                            !Found
                        if (sto:Suspend)
                            CYCLE
                        END
                        Access:RETSTOCK.ClearKey(res:PartRefNumberKey)
                        res:Part_Ref_Number = sto_ali:Ref_Number
                        Set(res:PartRefNumberKey,res:PartRefNumberKey)
                        Loop
                            If Access:RETSTOCK.NEXT()
                                Break
                            End !If
                            If res:Part_Ref_Number <> sto_ali:Ref_Number      |
                                Then Break.  ! End If

                            Do GetNextRecord2

                            Do CancelCheck
                            If tmp:Cancel
                                Break
                            End ! If tmp:Cancel

                            If res:Despatched <> 'YES'
                                Cycle
                            End ! If res:Despatched <> 'YES'

                            Access:RETSALES.ClearKey(ret:Ref_Number_Key)
                            ret:Ref_Number = res:Ref_Number
                            If Access:RETSALES.TryFetch(ret:Ref_Number_Key) = Level:Benign
                                    !Found
                                Access:INVOICE.ClearKey(inv:Invoice_Number_Key)
                                inv:Invoice_Number = ret:Invoice_Number
                                If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
                                        !Found
                                    If local:FirstTimeInLoop
                                        If inv:Date_Created < locEndDate - local:IntervalLoop
                                            Cycle
                                        End ! If inv:Date_Created < locEndDate - x#
                                    Else ! If FirstTime#
                                        If inv:Date_Created < locEndDate - local:IntervalLoop + 1
                                            Cycle
                                        End ! If inv:Date_Created < locEndDate - x#
                                    End ! If FirstTime#
                                    If inv:Date_Created > (locEndDate - local:IntervalLoop) + local:Interval
                                        Cycle
                                    End ! If inv:Date_Created > (locEndDate - x#) + local:Interval
                                    local:QuantitySoldByMainStore += res:Quantity

                                Else !If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
                                        !Error
                                End !If Access:INVOICE.TryFetch(inv:Invoice_Number_Key) = Level:Benign
                            Else !If Access:RETSALES.TryFetch(ret:Ref_Number_Key) = Level:Benign
                                    !Error
                            End !If Access:RETSALES.TryFetch(ret:Ref_Number_Key) = Level:Benign
                        End !Loop

                    Else !If Access:STOCK_ALIAS.TryFetch(sto_ali:Location_Key) = Level:Benign
                            !Error
                    End !If Access:STOCK_ALIAS.TryFetch(sto_ali:Location_Key) = Level:Benign
                    exp:Line = Clip(exp:Line) & '","' & local:QuantitySoldByMainStore
                Else ! If glo:Pointer = 'MAIN STORE'
                        ! Deleting (DBH 18/08/2008) # 9798 - Don't show on RRC tabs
                        !                    exp:Line = Clip(exp:Line) & '","'
                        ! End (DBH 18/08/2008) #9798
                End

                If tmp:Cancel
                    Break
                End ! If tmp:Cancel

                local:FirstTimeInLoop = False

            End ! Loop local:IntervalLoop = locNumberOfDays to local:Interval By -local:Interval
            If tmp:Cancel
                Break
            End ! If tmp:Cancel

            Loop sta# = 1 To Records(StatusQueue)
                Get(StatusQueue,sta#)
                If Instring(Clip(glo:Pointer),staque:StatusMessage,1,1)
                    staque:StatusMessage = Clip(glo:Pointer) & ': Checking Quantity On Back Order'
                    Put(StatusQueue)
                    Select(?List1,Records(StatusQueue))
                    Display()
                    Break
                End ! If Instring(glo:Pointer,staque:StatusMessage,1,1)
            End ! Loop sta# = 1 To Records(StatusQueue)
                ! _____________________________________________________________________
                !Quantity On Retail Backorder At Main Store

            local:QuantityRetailOnBackOrder = 0
            If glo:Pointer = 'MAIN STORE'
                Access:RETSTOCK.ClearKey(res:DespatchPartKey)
                res:Despatched  = 'PEN'
                res:Part_Number = sto:Part_Number
                Set(res:DespatchPartKey,res:DespatchPartKey)
                Loop
                    If Access:RETSTOCK.NEXT()
                        Break
                    End !If
                    If res:Despatched  <> 'PEN'      |
                        Or res:Part_Number <> sto:Part_Number      |
                        Then Break.  ! End If
                    local:QuantityRetailOnBackOrder += res:Quantity
                    Do GetNextRecord2

                    Do CancelCheck
                    If tmp:Cancel
                        Break
                    End ! If tmp:Cancel

                End !Loop
            Else ! If glo:Pointer = 'MAIN STORE'
                    ! Work out with Trade Account belongs to this Site Location - TrkBs: 6558 (DBH: 19-10-2005)
                Access:TRADEACC.ClearKey(tra:RecordNumberKey)
                tra:RecordNumber = 1
                Set(tra:RecordNumberKey,tra:RecordNumberKey)
                Loop
                    If Access:TRADEACC.NEXT()
                        Break
                    End !If
                    If tra:SiteLocation = glo:Pointer
                        local:SiteLocationAccount = tra:Account_Number
                        Break
                    End ! If tra:SiteLocation = glo:Pointer
                End !Loop

                If local:SiteLocationAccount <> ''
                        ! Now go through all Back Order parts, and find sales that match - TrkBs: 6558 (DBH: 19-10-2005)
                    Access:RETSTOCK.ClearKey(res:DespatchPartKey)
                    res:Despatched  = 'PEN'
                    res:Part_Number = sto:Part_Number
                    Set(res:DespatchPartKey,res:DespatchPartKey)
                    Loop
                        If Access:RETSTOCK.NEXT()
                            Break
                        End !If
                        If res:Despatched  <> 'PEN'      |
                            Or res:Part_Number <> sto:Part_Number      |
                            Then Break.  ! End If

                            ! Find the retail sales account, then use that to lookup the Trade Account - TrkBs: 6558 (DBH: 19-10-2005)
                        Access:RETSALES.ClearKey(ret:Ref_Number_Key)
                        ret:Ref_Number = res:Ref_Number
                        If Access:RETSALES.TryFetch(ret:Ref_Number_Key) = Level:Benign
                                !Found
                            Access:TRADEACC.ClearKey(tra:StoresAccountKey)
                            tra:StoresAccount = ret:Account_Number
                            If Access:TRADEACC.TryFetch(tra:StoresAccountKey) = Level:Benign
                                    !Found
                                If tra:Account_Number = local:SiteLocationAccount
                                    local:QuantityRetailOnBackOrder += res:Quantity
                                End ! If tra:Account_Number = local:SiteLocationAccount
                            Else !If Access:TRADEACC.TryFetch(tra:StoresAccountKey) = Level:Benign
                                    !Error
                            End !If Access:TRADEACC.TryFetch(tra:StoresAccountKey) = Level:Benign
                        Else !If Access:RETSALES.TryFetch(ret:Ref_Number_Key) = Level:Benign
                                !Error
                        End !If Access:RETSALES.TryFetch(ret:Ref_Number_Key) = Level:Benign

                        Do GetNextRecord2

                        Do CancelCheck
                        If tmp:Cancel
                            Break
                        End ! If tmp:Cancel

                    End !Loop
                End ! If local:SiteLocationAccount <> ''

            End ! If glo:Pointer = 'MAIN STORE'

            If tmp:Cancel
                Break
            End ! If tmp:Cancel

            exp:Line = Clip(exp:Line) & '","' & local:QuantityRetailOnBackOrder

                ! _____________________________________________________________________
                ! Selected Location Stock On Hand

            exp:Line = Clip(exp:Line) & '","' & sto:Quantity_Stock

                ! _____________________________________________________________________

                !            ! ARC Stock On Hand
                ! -----------------------------------------
                ! DBH 16/10/2008 #10516
                ! Inserting: Oh, we DO want this column now
            If glo:Pointer = 'MAIN STORE'
                    ! End: DBH 16/10/2008 #10516
                    ! -----------------------------------------
                Access:STOCK_ALIAS.ClearKey(sto_ali:Location_Key)
                sto_ali:Location    = 'AA20 ADVANCED REPAIR CENTRE'
                sto_ali:Part_Number = sto:Part_Number
                If Access:STOCK_ALIAS.TryFetch(sto_ali:Location_Key) = Level:Benign
                        !Found
                    exp:Line = Clip(exp:Line) & '","' & sto_ali:Quantity_Stock
                Else !If Access:STOCK_ALIAS.TryFetch(sto_ali:Location_Key) = Level:Benign
                        !Error
                    exp:Line = Clip(exp:Line) & '","0'
                End !If Access:STOCK_ALIAS.TryFetch(sto_ali:Location_Key) = Level:Benign
            End ! If glo:Pointer = 'MAIN STORE'
                ! _____________________________________________________________________
                ! Total Stock On Hand Nationally

            Loop sta# = 1 To Records(StatusQueue)
                Get(StatusQueue,sta#)
                If Instring(Clip(glo:Pointer),staque:StatusMessage,1,1)
                    staque:StatusMessage = Clip(glo:Pointer) & ': Checking Stock On Hand'
                    Put(StatusQueue)
                    Select(?List1,Records(StatusQueue))
                    Display()
                    Break
                End ! If Instring(glo:Pointer,staque:StatusMessage,1,1)
            End ! Loop sta# = 1 To Records(StatusQueue)

            If glo:Pointer = 'MAIN STORE'
                local:QuantityOnHand = 0
                Access:LOCATION.ClearKey(loc:ActiveLocationKey)
                loc:Active   = True
                Set(loc:ActiveLocationKey,loc:ActiveLocationKey)
                Loop
                    If Access:LOCATION.NEXT()
                        Break
                    End !If
                    If loc:Active   <> True      |
                        Then Break.  ! End If
                    Access:STOCK_ALIAS.ClearKey(sto_ali:Location_Key)
                    sto_ali:Location    = loc:Location
                    sto_ali:Part_Number = sto:Part_Number
                    Set(sto_ali:Location_Key,sto_ali:Location_Key)
                    Loop
                        If Access:STOCK_ALIAS.NEXT()
                            Break
                        End !If
                        If sto_ali:Location    <> loc:Location      |
                            Or sto_ali:Part_Number <> sto:Part_Number      |
                            Then Break.  ! End If
                        Do GetNextRecord2
                        if (sto_ali:Suspend)
                            CYCLE
                        END

                        Do CancelCheck
                        If tmp:Cancel
                            Break
                        End ! If tmp:Cancel

                        local:QuantityOnHand += sto_ali:Quantity_Stock
                    End !Loop
                End !Loop

                exp:Line = Clip(exp:Line) & '","' & local:QuantityOnHand
            Else ! If glo:Pointer = 'MAIN STORE'
                    ! Deleting (DBH 18/08/2008) # 9798 - DOn't show on RRC tabs
                    !                exp:Line = Clip(exp:Line) & '","'
                    ! End (DBH 18/08/2008) #9798
            End
            If tmp:Cancel
                Break
            End ! If tmp:Cancel

                ! _____________________________________________________________________
                ! Main Store Quantity On Order With Supplier
            If glo:Pointer = 'MAIN STORE'
                    ! Count the number of parts on orders that have not been received - TrkBs: 6247 (DBH: 06-09-2005)
                local:QuantityOnBackOrder = 0
                Access:ORDPARTS.Clearkey(orp:Ref_Number_Key)
                orp:Part_Ref_Number = sto:Ref_Number
                Set(orp:Ref_Number_Key,orp:Ref_Number_Key)
                Loop ! Begin ORDPARTS Loop
                    If Access:ORDPARTS.Next()
                        Break
                    End ! If !Access
                    If orp:Part_Ref_Number <> sto:Ref_Number
                        Break
                    End ! If

                    If orp:Order_Number <> ''
                        Access:ORDERS.Clearkey(ord:Order_Number_Key)
                        ord:Order_Number    = orp:Order_Number
                        If Access:ORDERS.Tryfetch(ord:Order_Number_Key) = Level:Benign
                                ! Found
                            If ord:All_Received <> 'YES'
                                If orp:All_Received <> 'YES'
                                    local:QuantityOnBackOrder += orp:Quantity
                                End ! If orp:All_Received <> 'YES'
                            End ! If ord:All_Recieved <> 'YES'
                        Else ! If Access:ORDERS.Tryfetch(ord:Order_Number_Key) = Level:Benign
                                ! Error
                        End ! If Access:ORDERS.Tryfetch(ord:Order_Number_Key) = Level:Benign
                    End ! If orp:Order_Number <> ''
                End ! End ORDPARTS Loop

                exp:Line = Clip(exp:Line) & '","' & local:QuantityOnBackOrder
            Else ! If glo:Pointer = 'MAIN STORE'
                    ! Deleting (DBH 18/08/2008) # 9798 - DOn't show on RRC tabs
                    !                exp:Line = Clip(exp:Line) & '","'
                    ! End (DBH 18/08/2008) #9798
            End ! If glo:Pointer = 'MAIN STORE'

                ! _____________________________________________________________________

                ! Count Of Jobs Waiting For Spare Part

                ! Inserting (DBH 20/04/2006) #7304 - Count jobs that have used this part and are at the particular status
            If glo:Pointer = 'MAIN STORE'
                exp:Line = Clip(exp:Line) & '","'
            Else ! If glo:Pointer = 'MAIN STORE'
                local:JobsWithSpares = 0

                IF (local:SiteLocationAccount = locHeadAccountNumber)
                        ! If ARC Site. The look for parts AT ARC, and all other
                        ! jobs that have been sent to the ARC (DBH: 16/02/2010) #11288

                    if (locBuildJobsQueue = FALSE)
                            ! #11349 Store the relevant job numbers in a Q. Should be quicker to sort through. (DBH: 01/07/2010)
                        locBuildJobsQueue = TRUE
                        Loop sta# = 1 To Records(StatusQueue)
                            Get(StatusQueue,sta#)
                            If Instring(Clip(glo:Pointer),staque:StatusMessage,1,1)
                                staque:StatusMessage = Clip(glo:Pointer) & ': Building Jobs List'
                                Put(StatusQueue)
                                Select(?List1,Records(StatusQueue))
                                Display()
                                Break
                            End ! If Instring(glo:Pointer,staque:StatusMessage,1,1)
                        End ! Loop sta# = 1 To Records(StatusQueue)
                        Access:JOBS.Clearkey(job:By_Status)
                        job:Current_Status = '330 SPARES REQUESTED'
                        set(job:By_Status,job:By_Status)
                        LOOP
                            if (job:Current_Status <> '330 SPARES REQUESTED')
                                BREAK
                            END

                            Do GetNextRecord2

                            Do CancelCheck
                            If tmp:Cancel
                                Break
                            End ! If tmp:Cancel
                            qJobNumbers.jobNumber = job:Ref_Number
                            get(qJobNumbers,qJobNumbers.jobNumber)
                            qJobNumbers.jobNumber = job:Ref_Number
                            qJobNumbers.cJob = FALSE
                            qJobNumbers.wJob = FALSE
                            if (job:Chargeable_Job = 'YES')
                                qJobNumbers.cJob = TRUE
                            END
                            if (job:Warranty_Job = 'YES')
                                qJobNumbers.wJob = TRUE
                            END

                            Add(qJobNumbers)
                        END
                        if (tmp:cancel)
                            BREAK
                        END

                        Access:JOBS.Clearkey(job:By_Status)
                        job:Current_Status = '340 BACK ORDER SPARES'
                        set(job:By_Status,job:By_Status)
                        LOOP
                            if (job:Current_Status <> '340 BACK ORDER SPARES')
                                BREAK
                            END
                            Do GetNextRecord2

                            Do CancelCheck
                            If tmp:Cancel
                                Break
                            End ! If tmp:Cancel

                            qJobNumbers.jobNumber = job:Ref_Number
                            get(qJobNumbers,qJobNumbers.jobNumber)
                            qJobNumbers.jobNumber = job:Ref_Number
                            qJobNumbers.cJob = FALSE
                            qJobNumbers.wJob = FALSE
                            if (job:Chargeable_Job = 'YES')
                                qJobNumbers.cJob = TRUE
                            END
                            if (job:Warranty_Job = 'YES')
                                qJobNumbers.wJob = TRUE
                            END

                            Add(qJobNumbers)
                        END

                        if (tmp:cancel)
                            BREAK
                        END
                    END

                    Loop jobQ# = 1 to Records(qJobNumbers)
                        get(qJobNumbers,jobQ#)

                        Do GetNextRecord2

                        Do CancelCheck
                        If tmp:Cancel
                            Break
                        End ! If tmp:Cancel

                        foundPartOnJob# = 0
                        IF (qJobNumbers.cJob)
                            Access:PARTS.ClearKey(par:RefPartRefNoKey)
                            par:Ref_Number = qJobNumbers.jobNumber
                            par:Part_Ref_Number = sto:Ref_Number
                            IF (Access:PARTS.TryFetch(par:RefPartRefNoKey) = Level:Benign)
                                    ! Found that part on a job at any site
                                local:jobsWithSpares += 1
                                foundPartsOnJob# = 0
                            END
                        END
                        IF (qJobNumbers.wJob AND foundPartOnJob# = 0)
                            Access:WARPARTS.ClearKey(wpr:RefPartRefNoKey)
                            wpr:Ref_Number = qJobNumbers.wJob
                            wpr:Part_Ref_Number = sto:Ref_Number
                            IF (Access:WARPARTS.TryFetch(wpr:RefPartRefNoKey) = Level:Benign)
                                local:jobsWithSpares += 1
                            END
                        END
                    END

!                        Access:JOBS.ClearKey(job:By_Status)
!                        job:Current_Status = '340 BACK ORDER SPARES'
!                        SET(job:By_Status,job:By_Status)
!                        LOOP
!                            IF (Access:JOBS.Next())
!                                BREAK
!                            END
!                            IF (job:Current_Status <> '340 BACK ORDER SPARES')
!                                BREAK
!                            END
!
!                            foundPartOnJob# = 0
!                            IF (job:Chargeable_Job = 'YES')
!                                Access:PARTS.ClearKey(par:RefPartRefNoKey)
!                                par:Ref_Number = job:Ref_Number
!                                par:Part_Ref_Number = sto:Ref_Number
!                                IF (Access:PARTS.TryFetch(par:RefPartRefNoKey) = Level:Benign)
!                                    ! Found that part on a job at any site
!                                    local:jobsWithSpares += 1
!                                    foundPartsOnJob# = 0
!                                END
!                            END
!                            IF (job:Warranty_Job = 'YES' AND foundPartOnJob# = 0)
!                                Access:WARPARTS.ClearKey(wpr:RefPartRefNoKey)
!                                wpr:Ref_Number = job:Ref_Number
!                                wpr:Part_Ref_Number = sto:Ref_Number
!                                IF (Access:WARPARTS.TryFetch(wpr:RefPartRefNoKey) = Level:Benign)
!                                    local:jobsWithSpares += 1
!                                END
!                            END
!               END
                ELSE
                        ! Not the ARC. Just include all jobs for this RRC
                        ! Loop through all jobs with the status - TrkBs: 7304 (DBH: 20-04-2006)
                    Access:WEBJOB.ClearKey(wob:HeadCurrentStatusKey)
                    wob:HeadAccountNumber = local:SiteLocationAccount
                    wob:Current_Status    = '330 SPARES REQUESTED'
                    Set(wob:HeadCurrentStatusKey,wob:HeadCurrentStatusKey)
                    Loop
                        If Access:WEBJOB.NEXT()
                            Break
                        End !If
                        If wob:HeadAccountNumber <> local:SiteLocationAccount      |
                            Or wob:Current_Status    <> '330 SPARES REQUESTED'      |
                            Then Break.  ! End If
                            ! If I can find the selected part in chargeable/warranty, count the job - TrkBs: 7304 (DBH: 20-04-2006)
                        Access:JOBS.Clearkey(job:Ref_Number_Key)
                        job:Ref_Number  = wob:RefNumber
                        If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                                ! Found

                        Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                                ! Error
                            Cycle
                        End ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign

                        FoundPartOnJob# = False
                        If job:Chargeable_Job = 'YES'
                            Access:PARTS.Clearkey(par:RefPartRefNoKey)
                            par:Ref_Number  = job:Ref_Number
                            par:Part_Ref_Number = sto:Ref_Number
                            If Access:PARTS.Tryfetch(par:RefPartRefNoKey) = Level:Benign
                                    ! Found
                                local:JobsWithSpares += 1
                                FoundPartOnJob# = True
                            Else ! If Access:PARTS.Tryfetch(par:RefPartRefNoKey) = Level:Benign
                                    ! Error
                            End ! If Access:PARTS.Tryfetch(par:RefPartRefNoKey) = Level:Benign
                        End ! If job:Chargeable_Job = 'YES'

                        If job:Warranty_Job = 'YES' And FoundPartOnJob# = False
                            Access:WARPARTS.Clearkey(wpr:RefPartRefNoKey)
                            wpr:Ref_Number  = job:Ref_Number
                            wpr:Part_Ref_Number = sto:Ref_Number
                            If Access:WARPARTS.Tryfetch(wpr:RefPartRefNoKey) = Level:Benign
                                    ! Found
                                local:JobsWithSpares += 1
                            Else ! If Access:WARPARTS.Tryfetch(wpr:RefPartRefNoKey) = Level:Benign
                                    ! Error
                            End ! If Access:WARPARTS.Tryfetch(wpr:RefPartRefNoKey) = Level:Benign
                        End ! If job:Warranty_Job = 'YES' And FoundPartOnJob# = False
                    End !Loop

                        ! Loop through all jobs with the status - TrkBs: 7304 (DBH: 20-04-2006)
                    Access:WEBJOB.ClearKey(wob:HeadCurrentStatusKey)
                    wob:HeadAccountNumber = local:SiteLocationAccount
                    wob:Current_Status    = '340 BACK ORDER SPARES'
                    Set(wob:HeadCurrentStatusKey,wob:HeadCurrentStatusKey)
                    Loop
                        If Access:WEBJOB.NEXT()
                            Break
                        End !If
                        If wob:HeadAccountNumber <> local:SiteLocationAccount      |
                            Or wob:Current_Status    <> '340 BACK ORDER SPARES'      |
                            Then Break.  ! End If
                            ! If I can find the selected part in chargeable/warranty, count the job - TrkBs: 7304 (DBH: 20-04-2006)
                        Access:JOBS.Clearkey(job:Ref_Number_Key)
                        job:Ref_Number  = wob:RefNumber
                        If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                                ! Found

                        Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                                ! Error
                            Cycle
                        End ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign

                        FoundPartOnJob# = False
                        If job:Chargeable_Job = 'YES'
                            Access:PARTS.Clearkey(par:RefPartRefNoKey)
                            par:Ref_Number  = job:Ref_Number
                            par:Part_Ref_Number = sto:Ref_Number
                            If Access:PARTS.Tryfetch(par:RefPartRefNoKey) = Level:Benign
                                    ! Found
                                local:JobsWithSpares += 1
                                FoundPartOnJob# = True
                            Else ! If Access:PARTS.Tryfetch(par:RefPartRefNoKey) = Level:Benign
                                    ! Error
                            End ! If Access:PARTS.Tryfetch(par:RefPartRefNoKey) = Level:Benign
                        End ! If job:Chargeable_Job = 'YES'

                        If job:Warranty_Job = 'YES' And FoundPartOnJob# = False
                            Access:WARPARTS.Clearkey(wpr:RefPartRefNoKey)
                            wpr:Ref_Number  = job:Ref_Number
                            wpr:Part_Ref_Number = sto:Ref_Number
                            If Access:WARPARTS.Tryfetch(wpr:RefPartRefNoKey) = Level:Benign
                                    ! Found
                                local:JobsWithSpares += 1
                            Else ! If Access:WARPARTS.Tryfetch(wpr:RefPartRefNoKey) = Level:Benign
                                    ! Error
                            End ! If Access:WARPARTS.Tryfetch(wpr:RefPartRefNoKey) = Level:Benign
                        End ! If job:Warranty_Job = 'YES' And FoundPartOnJob# = False

                    End !Loop
                        !end --- (DBH: 16/02/2010) #11288
                end

                exp:Line = Clip(exp:Line) & '","' & local:JobsWithSpares
            End ! If glo:Pointer = 'MAIN STORE'
                ! End (DBH 20/04/2006) #7304


            exp:Line = Clip(exp:Line) & '"'
            Add(ExportFile)
            local:RecordCount += 1
        End ! End STOCK Loop
        Close(ExportFile)

        Loop sta# = 1 To Records(StatusQueue)
            Get(StatusQueue,sta#)
            If Instring(Clip(glo:Pointer),staque:StatusMessage,1,1)
                staque:StatusMessage = Clip(glo:Pointer) & ': Completed'
                Put(StatusQueue)
                Select(?List1,Records(StatusQueue))
                Display()
                Break
            End ! If Instring(glo:Pointer,staque:StatusMessage,1,1)
        End ! Loop sta# = 1 To Records(StatusQueue)

        locClass.UpdateProgressWindow('========')
        locClass.UpdateProgressWindow('')

        local:RecordCount += 2

        csv:Location    = glo:Pointer
        csv:RecordCount = local:RecordCount
        csv:FileName    = glo:ExportFile
        If glo:Pointer = 'MAIN STORE'
            csv:LastCell    = E1.GetColumnAddressFromColumnNumber((locIntervalPeriod * 2) + 11)     !new supplier column makes this up to 11 spotted 6/11/12 TB12861
        Else ! If glo:Pointer = 'MAIN STORE'
            csv:LastCell    = E1.GetColumnAddressFromColumnNumber((locIntervalPeriod * 2) + 9)      !new supplier column makes this up to 9 spotted 6/11/12 TB12861
        End ! If glo:Pointer = 'MAIN STORE'

        Add(CSVQueue)
        If tmp:Cancel
            Break
        End ! If tmp:Cancel

    End ! Loop local:LoopCount = 1 To Records(glo:Queue)


        ! _____________________________________________________________________

    If tmp:Cancel = 0 Or tmp:Cancel = 2

        ?ProgressCancel{prop:Hide} = 1

        locClass.UpdateProgressWindow('================')
        locClass.UpdateProgressWindow('Building Excel Document..')
            ! _____________________________________________________________________

            ! CREATE EXPORT DOCUMENT, CREATE SHEETS, COLUMNS and AUTOFILTER
            ! _____________________________________________________________________

        If E1.Init(0, 0) = 0
            Beep(Beep:SystemHand)  ;  Yield()
            Case Message('An error has occured communicating with Excel.'&|
                '|'&|
                '|Please quit and try again.','ServiceBase',|
                Icon:Hand,'&OK',1)
                Of 1 ! &OK Button
            End!Case Message
            Exit
        End ! If E1.Init(0,0,1) = 0

        E1.NewWorkBook()
        E1.SaveAs(excelFileName)
        E1.CloseWorkBook(2)

        Sort(CSVQueue,-csv:Location)

        Loop x# = 1 To Records(CSVQueue)
            Get(CSVQueue,x#)

            E1.OpenWorkBook(Clip(csv:FileName))
            E1.Copy('A1',Clip(csv:LastCell) & Clip(csv:RecordCount))
            locClipboard = Clipboard()
            Loop Until Clip(Clipboard()) = ''
                SetClipBoard('')
            End ! Loop Until Clip(Clipboard()) = ''
            E1.CloseWorkbook(2)

            E1.OpenWorkbook(excelFileName)
            E1.InsertWorksheet()
            E1.RenameWorksheet(csv:Location)
            E1.SelectCells('A4')
            SetClipboard(locClipboard)
            E1.Paste()
            Loop Until Clip(Clipboard()) = ''
                SetClipBoard('')
            End ! Loop Until Clip(Clipboard()) = ''

            E1.WriteToCell('No. Of Days','A1')
            E1.WriteToCell(locNumberOfDays,'B1')
            E1.WriteToCell('Interval Period','A2')
            E1.WriteToCell(locIntervalPeriod,'B2')
            E1.WriteToCell('End Date','A3')
            E1.WriteToCell(Format(locEndDate,@d6),'B3')

            E1.SetCellFontName('Tahoma','A1',Clip(csv:LastCell) & (Clip(csv:RecordCount)+5))
            E1.SetCellFontSize(8,'A1',Clip(csv:LastCell) & (Clip(csv:RecordCount)+5))
            E1.SetCellFontStyle('Bold','A1',Clip(csv:LastCell) & '5')
            locClass.DrawBox('A1',Clip(csv:LastCell) & '1','A3',Clip(csv:LastCell) & '3',COLOR:Silver)
            locClass.DrawBox('A4',Clip(csv:LastCell) & '4','A5',Clip(csv:LastCell) & '5',COLOR:Silver)
            E1.SetCellNumberFormat(oix:NumberFormatCurrency,oix:CurrencySymbolNone,2,,'D6','D' & (Clip(csv:RecordCount)+5))
            E1.SetColumnWidth('A',Clip(csv:LastCell))
            E1.SelectCells('A6')
            E1.FreezePanes(1)

            E1.Save()
            E1.CloseWorkBook(2)
        End ! Loop x# = 1 To Records(CSVQueue)

        E1.Kill()

        locClass.UpdateProgressWindow('================')
        locClass.UpdateProgressWindow('Report Finished: ' & Format(Today(), @d6b) & ' ' & Format(Clock(), @t1b))

    End ! If tmp:Cancel = False

    BHReturnDaysHoursMins(BHTimeDifference24Hr(local:ReportStartDate, Today(), local:ReportStartTime, Clock()), Days#, Hours#, Mins#)

    locClass.UpdateProgressWindow('Time To Finish: ' & Days# & ' Dys, ' & Format(Hours#, @n02) & ':' & Format(Mins#, @n02))
    Add(StatusQueue)
    Select(?List1, Records(StatusQueue))

    Do EndPrintRun
    ?progress:userstring{prop:text} = 'Finished...'

    Display()
    ?ProgressCancel{prop:Hide} = 1
    ?Finish{prop:Hide}         = 0
    If Command('/SCHEDULE')
        If Access:REPSCHLG.PrimeRecord() = Level:Benign
            rlg:REPSCHEDRecordNumber = rpd:RecordNumber
            rlg:Information = 'Report Name: ' & Clip(rpd:ReportName) & ' - ' & Clip(rpd:ReportCriteriaType) & |
                '<13,10>Report Finished'
            If Access:REPSCHLG.TryInsert() = Level:Benign
                    !Insert
            Else ! If Access:REPSCHLG.TryInsert() = Level:Benign
                Access:REPSCHLG.CancelAutoInc()
            End ! If Access:REPSCHLG.TryInsert() = Level:Benign
        End ! If Access.REPSCHLG.PrimeRecord() = Level:Benign
    Else
        ?Button:OpenReportFolder{Prop:Hide} = 0
        Accept
            Case Field()
                Of ?Finish
                    Case Event()
                        Of Event:Accepted
                            Break

                    End !Case Event()
                Of ?Button:OpenReportFolder
                    Case Event()
                        Of Event:Accepted
                            RUN('EXPLORER.EXE ' & Clip(local:Desktop))
                    End ! Case Event()
            End !Case Field()
        End !Accept
    End
    Close(ProgressWindow)
    Post(Event:CloseWindow)
DrawPartsTitle      Routine   !Set Summary Screen (DBH: 22-03-2004)
    Data
local:LastRow   Long()
    Code
        local:LastRow = locPartsExportedCount + 13

        E1.SetColumnWidth('A','','15')

        E1.WriteToCell('Spares Requested Status Anaylsis Report','A1')
        E1.SetCellFontStyle('Bold','A1','E3')
        E1.SetCellFontSize(12,'A1')

        E1.WriteToCell('VODR0081 - ' & locVersion,'C3')

        E1.WriteToCell('Criteria','A3')
        E1.WriteToCell('End Date','A4')
        E1.WriteToCell(Format(Today(),@d18),'B4')
        E1.WriteToCell('Period','A5')
        E1.WriteToCell(locNumberOfDays,'B5')
        If locUserName <> ''
            E1.WriteToCell('Created By','A6')
            E1.WriteToCell(locUserName,'B6')
        End ! If locUserName <> ''
        E1.WriteToCell('Created Date','A7')
        E1.WriteToCell(Format(TOday(),@d18),'B7')
        E1.WriteToCell('Created Time','A8')
        E1.WriteToCell(FOrmat(Clock(),@t1),'B8')

        E1.WriteToCell('Section Name','A10')

        E1.WriteToCell('AA10 MAIN STORE','B10')
        E1.SetCellFontStyle('Bold','B4','B10')

        locClass.DrawBox('A1','L1','A1','L1',color:Silver)
        locClass.DrawBox('A3','L3','A3','L3',color:Silver)
        locClass.DrawBox('A4','L4','A8','L8',color:Silver)
        locClass.DrawBox('A10','L10','A10','L10',color:Silver)
        locClass.DrawBox('A12','L12','A12','L12',color:Silver)

        E1.WriteToCell('Total Records','D10')
        E1.WriteToCell(locPartsExportedCount,'E10')
        E1.SetCellFontStyle('Bold','E10')

        E1.WriteToCell('Showing','F10')
        E1.WriteToCell('=SUBTOTAL(3,A13:A' & local:LastRow & ')','G10')
        E1.SetCellFontStyle('Bold','G10')

        E1.SetCellFontName('Tahoma','A1', 'L' & local:LastRow)
        E1.SetCellFontSize(8,'A2','L' & local:LastRow)
        E1.SetColumnWidth('B','L','')


DrawExchangeTitle   Routine   !Set Summary Screen (DBH: 22-03-2004)
    Data
local:LastRow   Long()
    Code
        local:LastRow = locExchangeExportedCount + 13

        E1.SetColumnWidth('A','','15')

        E1.WriteToCell('Spares Requested Status Anaylsis Report','A1')
        E1.SetCellFontStyle('Bold','A1','E3')
        E1.SetCellFontSize(12,'A1')

        E1.WriteToCell('VODR0081 - ' & locVersion,'C3')

        E1.WriteToCell('Criteria','A3')
        E1.WriteToCell('End Date','A4')
        E1.WriteToCell(Format(Today(),@d18),'B4')
        E1.WriteToCell('Period','A5')
        E1.WriteToCell(locNumberOfDays,'B5')
        If locUserName <> ''
            E1.WriteToCell('Created By','A6')
            E1.WriteToCell(locUserName,'B6')
        End ! If locUserName <> ''
        E1.WriteToCell('Created Date','A7')
        E1.WriteToCell(Format(TOday(),@d18),'B7')
        E1.WriteToCell('Created Time','A8')
        E1.WriteToCell(FOrmat(Clock(),@t1),'B8')

        E1.WriteToCell('Section Name','A10')

        E1.WriteToCell('AA10 MAIN STORE','B10')
        E1.SetCellFontStyle('Bold','B4','B10')

        locClass.DrawBox('A1','J1','A1','J1',color:Silver)
        locClass.DrawBox('A3','J3','A3','J3',color:Silver)
        locClass.DrawBox('A4','J4','A8','J8',color:Silver)
        locClass.DrawBox('A10','J10','A10','J10',color:Silver)
        locClass.DrawBox('A12','J12','A12','J12',color:Silver)

        E1.WriteToCell('Total Records','D10')
        E1.WriteToCell(locExchangeExportedCount,'E10')
        E1.SetCellFontStyle('Bold','E10')

        E1.WriteToCell('Showing','F10')
        E1.WriteToCell('=SUBTOTAL(3,A13:A' & local:LastRow & ')','G10')
        E1.SetCellFontStyle('Bold','G10')

        E1.SetColumnWidth('B','J')
        E1.SetCellFontName('Tahoma','A1', 'J' & local:LastRow)
        E1.SetCellFontSize(8,'A2','J' & local:LastRow)
getnextrecord2      routine !Progress Window Routines (DBH: 22-03-2004)
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
        percentprogress = (recordsprocessed / recordstoprocess)*100
        if percentprogress >= 100
            recordsprocessed        = 0
            percentprogress         = 0
            progress:thermometer    = 0
            recordsthiscycle        = 0
        end
        if percentprogress <> progress:thermometer then
            progress:thermometer = percentprogress
            ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
        end
    end
                        !0{prop:Text} = ?progress:pcttext{prop:text}
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
        Of Event:Timer
            Break
        Of Event:CloseWindow
            cancel# = 1
            Break
        Of Event:accepted
            Yield()
            If Field() = ?ProgressCancel
                cancel# = 1
                Break
            End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Beep(Beep:SystemQuestion)  ;  Yield()
        Case Message('Do you wish to FINISH building the Excel Document with the data you have compiled so far, or just QUIT now?','ServiceBase',|
            Icon:Question,'&Finish|&Quit|&Cancel',3)
        Of 1 ! &Finish Button
            tmp:Cancel = 2
        Of 2 ! &Quit Button
            tmp:Cancel = 1
        Of 3 ! &Cancel Button
        End!Case Message
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    display()

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          Access:SRNTExt.Clearkey(srn:KeySRN)
          srn:SRNumber = '020652'&'0'
          If Access:SRNText.Fetch(srn:KeySRN)
              !Error
              SRN:TipText = 'Press F1 to access detailed help'
          End !Access:SRNText.Fetch(stn:KeySRN) = Level:Benign
          ?SRNNumber{Prop:Text}= 'SRN:020652'&'0'
          if clip(SRN:TipText)='' then SRN:TipText = 'Press F1 to access detailed help'.
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('SparesForecastingReport')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelFalse
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('locTag',locTag)                                    ! Added by: BrowseBox(ABC)
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:SRNText.Open()
  Relate:INVOICE.SetOpenRelated()
  Relate:INVOICE.Open                                      ! File INVOICE used by this procedure, so make sure it's RelationManager is open
  Relate:REPSCHCR.SetOpenRelated()
  Relate:REPSCHCR.Open                                     ! File REPSCHCR used by this procedure, so make sure it's RelationManager is open
  Relate:RETSALES.SetOpenRelated()
  Relate:RETSALES.Open                                     ! File RETSALES used by this procedure, so make sure it's RelationManager is open
  Relate:SRNTEXT.Open                                      ! File SRNTEXT used by this procedure, so make sure it's RelationManager is open
  Relate:STOCK_ALIAS.Open                                  ! File STOCK_ALIAS used by this procedure, so make sure it's RelationManager is open
  Relate:WEBJOB.Open                                       ! File WEBJOB used by this procedure, so make sure it's RelationManager is open
  Access:REPSCHLG.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:WARPARTS.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:PARTS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:JOBS.UseFile                                      ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ORDPARTS.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ORDERS.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:RETSTOCK.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:STOHIST.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:STOCK.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:REPSCHLC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:REPSCHED.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:USERS.UseFile                                     ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TRADEACC.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
      locEndDate = TODAY()
      locHeadAccountNumber = GETINI('BOOKING','HeadAccount',,CLIP(path()) & '\SB2KDEF.INI')
  
      Access:TRADEACC.ClearKey(tra:Account_Number_Key)
      tra:Account_Number = locHeadAccountNumber
      IF (Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign)
          locARCLocation = tra:SiteLocation
          locARCCompanyName = tra:Company_Name
      END
  
      ! Get the name of the Main Store
      Access:LOCATION.ClearKey(loc:Main_Store_Key)
      loc:Main_Store = 'YES'
      SET(loc:Main_Store_Key,loc:Main_Store_Key)
      LOOP
          IF (Access:LOCATION.Next())
              BREAK
          END
          IF (loc:Main_Store <> 'YES')
              BREAK
          END
          locMainStoreLocation = loc:Location
          BREAK
      END
  
      ! Put in username is run from ServiceBase
      pct# = INSTRING('%',COMMAND(),1,1)
      IF (pct#)
          Access:USERS.ClearKey(use:password_key)
          use:password = CLIP(SUB(COMMAND(),pct# + 1,30))
          IF (Access:USERS.TryFetch(use:password_key) = Level:Benign)
              locUserName = CLIP(use:Forename) & ' ' & clip(use:surname)
          END
      END
  BRW10.Init(?List,Queue:Browse.ViewPosition,BRW10::View:Browse,Queue:Browse,Relate:LOCATION,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
      ! === Set Version Number ===
      !INCLUDE('..\ReportVersion.inc')
      tmp:VersionNumber = CLIP(kVersionNumber) & '.' & kVODR0083
      locVersion = tmp:VersionNumber
      ?reportVersion{PROP:Text} = 'Report Version: ' & CLIP(locVersion)
  ?List{Prop:LineHeight} = 0
  Do DefineListboxStyle
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
      Alert(0078H) !F9
      Alert(0070H) !F1
  BRW10.Q &= Queue:Browse
  BRW10.AddSortOrder(,loc:ActiveLocationKey)               ! Add the sort order for loc:ActiveLocationKey for sort order 1
  BRW10.AddRange(loc:Active,locTrue)                       ! Add single value range limit for sort order 1
  BRW10.AddLocator(BRW10::Sort0:Locator)                   ! Browse has a locator for sort order 1
  BRW10::Sort0:Locator.Init(,loc:Location,1,BRW10)         ! Initialize the browse locator using  using key: loc:ActiveLocationKey , loc:Location
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW10.AddField(locTag,BRW10.Q.locTag)                    ! Field locTag is a hot field or requires assignment from browse
  BRW10.AddField(loc:Location,BRW10.Q.loc:Location)        ! Field loc:Location is a hot field or requires assignment from browse
  BRW10.AddField(loc:RecordNumber,BRW10.Q.loc:RecordNumber) ! Field loc:RecordNumber is a hot field or requires assignment from browse
  BRW10.AddField(loc:Active,BRW10.Q.loc:Active)            ! Field loc:Active is a hot field or requires assignment from browse
  BRW10.AddToolbarTarget(Toolbar)                          ! Browse accepts toolbar control
  SELF.SetAlerts()
      ! Is this an automatic report?
      IF (COMMAND('/SCHEDULE'))
          x# = Instring('%',Command(),1,1)
          Access:REPSCHED.ClearKey(rpd:RecordNumberKey)
          rpd:RecordNumber = Clip(Sub(Command(),x# + 1,20))
          If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
              !Found
              Access:REPSCHCR.ClearKey(rpc:ReportCriteriaKey)
              rpc:ReportName = rpd:ReportName
              rpc:ReportCriteriaType = rpd:ReportCriteriaType
              If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
                  !Found
                  If rpc:AllLocations
                      DO DASBRW::12:DASTAGALL
                  Else ! If rpc:AllLocations
                      Access:REPSCHLC.Clearkey(rpl:LocationKey)
                      rpl:REPSCHCRRecordNumber = rpc:RecordNumber
                      Set(rpl:LocationKey,rpl:LocationKey)
                      Loop ! Begin Loop
                          If Access:REPSCHLC.Next()
                              Break
                          End ! If Access:REPSCHLC.Next()
                          If rpl:REPSCHCRRecordNumber <> rpc:RecordNumber
                              Break
                          End ! If rpl:REPSCHCRRecordNumber <> rpc:RecordNumber
                          glo:Pointer = rpl:Location
                          Add(glo:Queue)
                      End ! Loop
                  End ! If rpc:AllLocations
  
                  locIntervalPeriod = rpc:IntervalPeriod
                  locNumberOfDays = rpc:NumberOfDays
  
                  Case rpc:DateRangeType
                  Of 1 ! Today Only
                      !tmp:StartDate = Today()
                      locEndDate = Today()
                  Of 2 ! 1st Of Month
                      !tmp:StartDate = Date(Month(Today()),1,Year(Today()))
                      locEndDate = Today()
                  Of 3 ! Whole Month
                      !tmp:StartDate = Date(Month(Today()) - 1, 1, Year(Today()))
                      locEndDate = Date(Month(Today()),1,Year(Today())) - 1
                  End ! Case rpc:DateRangeType
  
                  Do Reporting
                  Post(Event:CloseWindow)
              Else ! If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
                  !Error
              End ! If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
  
          Else ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
              !Error
          End ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
      END
  
  
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List{Prop:Alrt,239} = SpaceKey
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  Relate:SRNText.Close()
    Relate:INVOICE.Close
    Relate:REPSCHCR.Close
    Relate:RETSALES.Close
    Relate:SRNTEXT.Close
    Relate:STOCK_ALIAS.Close
    Relate:WEBJOB.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Calendar
      ThisWindow.Update
      Calendar13.SelectOnClose = True
      Calendar13.Ask('Select a Date',locEndDate)
      IF Calendar13.Response = RequestCompleted THEN
      locEndDate=Calendar13.SelectedDate
      DISPLAY(?locEndDate)
      END
      ThisWindow.Reset(True)
    OF ?buttonPrint
      ThisWindow.Update
          DO reporting
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & '020652'&'0')
          Else !glo:WebJob Then
              HelpBrowser('020652'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020652'&'0')
      ***
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeEvent()
    e1.TakeEvent ('', '')
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all field specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::12:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
  !Of Event:AlertKey
      If KeyCode() = 0078H
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & '020652'&'0')
          Else !glo:WebJob Then
              HelpBrowser('020652'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020652'&'0')
      ***
      End !If KeyCode() F9
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

locClass.updateProgressWindow       PROCEDURE(STRING fText)
    CODE
        staque:StatusMessage = CLIP(fText)
        Add(statusQueue)
        Select(?List1,RECORDS(statusQueue))
        DISPLAY()

locClass.DrawBox       Procedure(String fTL,String fTR,String fBL,String fBR,Long fColour)
    Code
        If fBR = ''
            fBR = fTR
        End !If fBR = ''

        If fBL = ''
            fBL = fTL
        End !If fBL = ''

        If fColour = 0
            fColour = oix:ColorWhite
        End !If fColour = ''
        E1.SetCellBackgroundColor(fColour,fTL,fBR)
        E1.SetCellBorders(fBL,fBR,oix:BorderEdgeBottom,oix:LineStyleContinuous)
        E1.SetCellBorders(fTL,fTR,oix:BorderEdgeTop,oix:LineStyleContinuous)
        E1.SetCellBorders(fTL,fBL,oix:BorderEdgeLeft,oix:LineStyleContinuous)
        E1.SetCellBorders(fTR,fBR,oix:BorderEdgeRight,oix:LineStyleContinuous)
!---------------------------------------------------------------------------------
e1.Init   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0)
ReturnValue   byte
  CODE
  ReturnValue = PARENT.Init (pStartVisible,pEnableEvents)
  self.TakeSnapShotOfWindowPos()
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
e1.Kill   PROCEDURE (byte pUnloadCOM=1)
ReturnValue   byte
  CODE
  self.RestoreSnapShotOfWindowPos()
  ReturnValue = PARENT.Kill (pUnloadCOM)
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
e1.TakeEvent   PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0)
  CODE
  PARENT.TakeEvent (pEventString1,pEventString2,pEventNumber,pEventType,pEventStatus)
  if pEventType = 0  ! Generated by CapeSoft Office Inside
    case event()
      of event:accepted
        case field()
      end
    end
  end

BRW10.Fetch PROCEDURE(BYTE Direction)

GreenBarIndex   LONG
  CODE
  PARENT.Fetch(Direction)


BRW10.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = loc:Location
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      locTag = ''
    ELSE
      locTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  
  IF (locTag = '*')
    SELF.Q.locTag_Icon = 2                                 ! Set icon from icon list
  ELSE
    SELF.Q.locTag_Icon = 1                                 ! Set icon from icon list
  END


BRW10.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW10.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW10::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW10::RecordStatus=ReturnValue
  IF BRW10::RecordStatus NOT=Record:OK THEN RETURN BRW10::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = loc:Location
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::12:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW10::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW10::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW10::RecordStatus
  RETURN ReturnValue

ds_Stop              PROCEDURE  (<string StopText>)        ! Declare Procedure
returned              long
TempTimeOut           long(0)
TempNoLogging         long(0)
Loc:StopText    string(4096)
  CODE
  if omitted(1) or StopText = '' then
    if ThisMessageBox.GetGlobalSetting('TranslationFile') <> ''
      Loc:StopText = getini('MessageBox_Text','StopDefault','Exit?',ThisMessageBox.GetGlobalSetting('TranslationFile'))
    else
      Loc:StopText = 'Exit?'
    end
  else
    Loc:StopText = StopText
  end
  if ThisMessageBox.GetGlobalSetting('TranslationFile') <> ''
    returned = ds_Message(Loc:StopText,getini('MessageBox_Text','StopHeader','Stop',ThisMessageBox.GetGlobalSetting('TranslationFile')),ICON:Hand,BUTTON:Abort+BUTTON:Ignore,BUTTON:Abort)
  else
    returned = ds_Message(Loc:StopText,'Stop',ICON:Hand,BUTTON:Abort+BUTTON:Ignore,BUTTON:Abort)
  end
  if returned = BUTTON:Abort then halt .
ds_Halt              PROCEDURE  (UNSIGNED Level=0,<STRING HaltText>) ! Declare Procedure
TempNoLogging         long(0)
  CODE
  if ~omitted(2) then
    if ThisMessageBox.GetGlobalSetting('TranslationFile') <> ''
      ds_Message(HaltText,getini('MessageBox_Text','HaltHeader','Halt',ThisMessageBox.GetGlobalSetting('TranslationFile')),ICON:Hand)
    else
      ds_Message(HaltText,'Halt',ICON:Hand)
    end
  end
  system{prop:HaltHook} = 0
  HALT(Level)
ds_Message PROCEDURE (STRING MessageTxt,<STRING HeadingTxt>,<STRING IconSent>,<STRING ButtonsPar>,UNSIGNED MsgDefaults=0,BOOL StylePar=FALSE) ! Generated from procedure template - Window

FilesOpened          BYTE                                  !
Loc:ButtonPressed    UNSIGNED                              !
EmailLink            CSTRING(4096)                         !
DontShowThisAgain       byte(0)         !CapeSoft MessageBox Data
LocalMessageBoxdata     group,pre(LMBD) !CapeSoft MessageBox Data
MessageText               cstring(4096)
HeadingText               cstring(1024)
UseIcon                   cstring(1024)
Buttons                   cstring(1024)
Defaults                  unsigned
                        end
window               WINDOW('Caption'),AT(,,404,108),FONT('Tahoma',8,,FONT:regular),COLOR(COLOR:White),GRAY
                       IMAGE,AT(11,18),USE(?Image1),HIDE
                       PROMPT(''''),AT(118,32),USE(?MainTextPrompt)
                       STRING('HyperActive Link'),AT(91,46),USE(?HALink),HIDE
                       STRING('Time Out:'),AT(103,54),USE(?TimerCounter),HIDE
                       CHECK('Dont Show This Again'),AT(85,65),USE(DontShowThisAgain),HIDE
                       BUTTON('Button 1'),AT(9,81,45,14),USE(?Button1),HIDE
                       BUTTON('Button 2'),AT(59,81,45,14),USE(?Button2),HIDE
                       BUTTON('Button 3'),AT(109,81,45,14),USE(?Button3),HIDE
                       BUTTON('Button 4'),AT(159,81,45,14),USE(?Button4),HIDE
                       BUTTON('Button 5'),AT(209,81,45,14),USE(?Button5),HIDE
                       BUTTON('Button 6'),AT(259,81,45,14),USE(?Button6),HIDE
                       BUTTON('Button 7'),AT(309,81,45,14),USE(?Button7),HIDE
                       BUTTON('Button 8'),AT(359,81,45,14),USE(?Button8),HIDE
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED                 ! Method added to host embed code
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Run                    PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop
  RETURN(Loc:ButtonPressed)

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
ThisMessageBoxLogInit          routine         !CapeSoft MessageBox Initialize properties routine
    if ~omitted(2) then LMBD:HeadingText = HeadingTxt .
    if ~omitted(3) then LMBD:UseIcon = IconSent .
    if ~omitted(4) then LMBD:Buttons = ButtonsPar .
    if ~omitted(5) then LMBD:Defaults = MsgDefaults .
    LMBD:MessageText = MessageTxt
    ThisMessageBox.FromExe = command(0)
    if instring('\',ThisMessageBox.FromExe,1,1)
      ThisMessageBox.FromExe = ThisMessageBox.FromExe[ (instring('\',ThisMessageBox.FromExe,-1,len(ThisMessageBox.FromExe)) + 1) : len(ThisMessageBox.FromExe) ]
    end
    ThisMessageBox.TrnStrings = 1
    ThisMessageBox.PromptControl = ?MainTextPrompt
    ThisMessageBox.IconControl = ?Image1
         !End of CapeSoft MessageBox Initialize properties routine

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
    ThisMessageBox.SavedResponse = GlobalResponse        !CapeSoft MessageBox Code - Preserves the GlobalResponse
  GlobalErrors.SetProcedureName('ds_Message')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Image1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
        GlobalRequest = ThisWindow.Request         !Keep GlobalRequest the correct value.
        if ThisMessageBox.MessagesUsed then
          return level:fatal
        end
        ThisMessageBox.MessagesUsed += 1
        do ThisMessageBoxLogInit                               !CapeSoft MessageBox Code
  SELF.Open(window)                                        ! Open window
  Do DefineListboxStyle
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  ThisMessageBox.open(LMBD:MessageText,LMBD:HeadingText,LMBD:UseIcon,LMBD:Buttons,LMBD:Defaults,StylePar)         !CapeSoft MessageBox Code
      alert(EscKey)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
              !CapeSoft MessageBox EndCode
    Loc:ButtonPressed = ThisMessageBox.ReturnValue
    if ThisMessageBox.MessagesUsed > 0 then ThisMessageBox.MessagesUsed -= 1 .
              !End of CapeSoft MessageBox EndCode
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
    ReturnValue = ThisMessageBox.SavedResponse           !CapeSoft MessageBox Code - Preserves the GlobalResponse
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ThisMessageBox.TakeEvent()                             !CapeSoft MessageBox Code
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

XFiles PROCEDURE                                           ! Generated from procedure template - Window

QuickWindow          WINDOW('Window'),AT(,,260,160),FONT('MS Sans Serif',8,,FONT:regular),CENTER,IMM,HLP('XFiles'),SYSTEM,GRAY,RESIZE
                       BUTTON('&OK'),AT(101,142,49,14),USE(?Ok),FLAT,LEFT,MSG('Accept operation'),TIP('Accept Operation'),ICON('WAOK.ICO')
                       BUTTON('&Cancel'),AT(154,142,49,14),USE(?Cancel),FLAT,LEFT,MSG('Cancel Operation'),TIP('Cancel Operation'),ICON('WACANCEL.ICO')
                       BUTTON('&Help'),AT(207,142,49,14),USE(?Help),FLAT,LEFT,MSG('See Help Window'),TIP('See Help Window'),ICON('WAHELP.ICO'),STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED                 ! Method added to host embed code
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False) ! Method added to host embed code
                     END


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('XFiles')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Ok
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Ok,RequestCancelled)                    ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Ok,RequestCompleted)                    ! Add the close control to the window manger
  END
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:SRNTEXT.Open                                      ! File SRNTEXT used by this procedure, so make sure it's RelationManager is open
  SELF.FilesOpened = True
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Surface)                        ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:SRNTEXT.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

HelpBrowser PROCEDURE (func:Path)                          ! Generated from procedure template - Window

fepath               STRING(255)                           !
LSolCtrlQ            QUEUE,PRE()                           !
SolaceUseRef         LONG                                  !
SolaceCtrlName       STRING(20)                            !
                     END                                   !
Window               WINDOW('Online Help'),AT(522,0,154,428),FONT('Tahoma',8,,FONT:regular),COLOR(09A6A7CH),TILED,SYSTEM,GRAY,MAX,RESIZE,IMM
                       BUTTON,AT(0,0),USE(?Button1),TRN,FLAT,ICON('closep.jpg'),STD(STD:Close)
                       PROMPT('Close Online Help'),AT(72,8),USE(?Prompt1),TRN,FONT(,,COLOR:White,FONT:bold)
                       PANEL,AT(-3,25,159,404),USE(?feControl)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED                 ! Method added to host embed code
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Open                   PROCEDURE(),DERIVED                 ! Method added to host embed code
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False) ! Method added to host embed code
Resize                 PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END


                     ! Start: FE Class Declaration
ThisViewer1          class(FeBrowser)
                     END
                     !End: FE Class Declaration


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
feGetFileFromUser_ThisViewer1  routine
  ThisViewer1.fe_AppPath = Path()           ! Used to restore current path after selection
  if ThisViewer1.fe_LastPath <> ''          ! Var set below
    SetPath (ThisViewer1.fe_LastPath)       ! Set path to location of last selected file
  end
  ThisViewer1.fe_RestorePath = ThisViewer1.fe_Path  ! current document's path
  ThisViewer1.fe_Path = ''
  if not FileDialog('Choose File to View', ThisViewer1.fe_Path, 'HTML Files|*.htm;*.html;*.mht|Text Files|*.txt|Image Files|*.gif;*.jpeg;*.jpg;*.png;*.art;*.au;*.aiff;*.xbm|XML Files|*.xml|EML Files|*.eml|All Files|*.*', 10000b)
    SetPath (ThisViewer1.fe_AppPath)        ! Restore path to app path if cancelled
    ThisViewer1.fe_Path = ThisViewer1.fe_RestorePath
  else
    ThisViewer1.fe_LastPath = Path()        ! Path of selected file
    SetPath (ThisViewer1.fe_AppPath)        ! Restore path
    ThisViewer1.Load (ThisViewer1.fe_Path)  ! Load the selected file
  end
 exit

fePathControlAccepted_ThisViewer1  routine
  ThisViewer1.fe_Path = ''
  ThisViewer1.Load (ThisViewer1.fe_Path)
  exit

feWindowClosing_ThisViewer1  routine
  ThisViewer1.Kill(2)  ! GJxxyy123 WIP test code, 5 Oct 2005
 exit

feRecoveryCode_ThisViewer1  routine
 ! This routine is obsolete.  Please read the docs or contact support@capesoft.com
 exit

feLoadInitialDocument_ThisViewer1  routine
  ! FileExplorer - Loading initial document as window opens
  ThisViewer1.fe_Path = fepath
  ThisViewer1.Load (ThisViewer1.fe_Path)
 exit

feWindowTakeFieldEvent_fePathControl_EventAlertKey_ThisViewer1  routine
  case keycode()
    of EnterKey
      post(event:accepted, )
    of CtrlEnter
      post(event:accepted, )
  end
 exit



ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('HelpBrowser')
  fepath = Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & Clip(func:Path) & '.htm'
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  ThisViewer1.TempDirLocation = 0    ! temp files stored with the exe
  
   ! The feBrowser object uses "Early Binding" at this stage.  At some point we might
   ! make it optional to use "Late Binding" instead.  This switch sets which approach
   ! we use.  Don't change it for now...
   ! ThisViewer1.UseLateBinding = Glo:UseLateBinding
  
  
   ! Uncomment this line of code to override the above code where the property is set...
   ! For testing purposes...  Set the switch in the ProgramOptions screen (disabled by default).
   ! ThisViewer1.AutoDialDefaultInternetConnection = Glo:DUNAutoDialOption
   ! 0 - do not dial the default dial up connection
   ! 1 - dial the "Windows Default" connection
   ! 2 - establish the default connection, then dial it
  
   ThisViewer1.BetaCallbacksOn = 0  ! GJxxyy FILEEXPLORER TEST CODE
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Button1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.Open(Window)                                        ! Open window
  Do DefineListboxStyle
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  Resizer.Init(AppStrategy:Spread)                         ! Controls will spread out as the window gets bigger
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Open PROCEDURE

  CODE
  PARENT.Open
  ThisViewer1.Init (?feControl, , , , 1)


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
   ThisViewer1.TakeEvent()
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  
    OF EVENT:feCallback
      ! The File Explorer dll will post this event if you return fe:DivertNavigation from the
      ! (virtual) method ThisViewer::EventCallback.
  
    OF EVENT:feDelayedClose  ! GJxxyy123 WIP added 051005
      ThisViewer1.Kill()
  
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    OF EVENT:CloseWindow
        ThisViewer1._WindowClosing()  ! GJ added 280205
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:CloseWindow
      do feWindowClosing_ThisViewer1
    OF EVENT:OpenWindow
      do feLoadInitialDocument_ThisViewer1
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Button1, Resize:FixLeft+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Button1
  SELF.SetStrategy(?feControl, Resize:FixLeft+Resize:FixTop, Resize:ConstantRight+Resize:ConstantBottom) ! Override strategy for ?feControl
  SELF.SetStrategy(?Prompt1, Resize:FixLeft+Resize:FixTop, Resize:LockSize) ! Override strategy for ?Prompt1


Resizer.Resize PROCEDURE

ReturnValue          BYTE,AUTO


  CODE
  ReturnValue = PARENT.Resize()
  ! Auto-Resizing the File Explorer COM Object
  ThisViewer1.Resize ()
  RETURN ReturnValue


