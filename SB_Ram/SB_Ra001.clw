

   MEMBER('sb_ram.clw')                               ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SB_RA001.INC'),ONCE        !Local module procedure declarations
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

LocalDir             QUEUE(File:Queue),PRE(LDR)
                     END
X                    LONG
Y                    LONG
FileDirectory        STRING(255)
FirstPartName        STRING(255)
ImportFileName       STRING(255),STATIC
OutputFileName       STRING(255),STATIC
TempFullLine         STRING(1000)
LocalToken           STRING(20)
EndToken             STRING(4)
LocalArchiveFlag     BYTE
SaveDirectory        STRING(255)
LastLetter           STRING(1)
LocalRegion          STRING(30)
XMLUserName          STRING(50)
XMLPassword          STRING(50)
window               WINDOW('SB3g_RAM'),AT(0,0,161,54),FONT('Tahoma',6,,FONT:regular,CHARSET:ANSI),ICON('Cellular3g.ico'),TILED,TIMER(500),GRAY,ICONIZE,DOUBLE,IMM
                       BUTTON,AT(28,4),USE(?Button1),FLAT,ICON('closep.jpg')
                       STRING('RAM Interface.'),AT(44,41),USE(?String1),TRN,FONT(,8,,,CHARSET:ANSI)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
    Map
XMLTrack  Procedure(String XMLDetail), String
    END
!outputFile and ImportFile definitions
OutputFile    File,Driver('ASCII'),Pre(outfile),Name(OutputFileName),Create,Bindable,Thread
Record       Record
OutputLine   String(3000)
             End
         End

ImportFile    File,Driver('ASCII'),Pre(impfile),Name(ImportFileName),Create,Bindable,Thread
Record       Record
InputLine    String(3000)
             End
         End


!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RefreshDefaults     Routine

    if GetIni('API','ACTIVE','',clip(path())&'\SB_API.INI') <> 'Y' then
        Post(Event:closewindow)
    END

    !read the ini file incase of any changes
    FileDirectory = getini('API','DIRECTORY','',clip(path())&'\SB_API.INI')

    LocalArchiveFlag = false
    if GetIni('API','ARCHIVE','',clip(path())&'\SB_API.INI') = 'Y' then
        SaveDirectory =  GetIni('API','ARCHIVEDIR','',clip(path())&'\SB_API.INI') 
        If clip(SaveDirectory) <> ''
            LocalArchiveFlag = true
        END
    END

    EXIT
ImportResponses     Routine

    if clip(FileDirectory) = '' then exit.

    !Scan the directory for any .in files
    Free(localdir)
    Directory(localdir,clip(FileDirectory)&'\*.in',ff_:NORMAL)
    if records(LocalDir) = 0 then exit.

    Loop X = 1 to records(LocalDir)

        Get(LocalDir,X)

        ImportFileName = clip(FileDirectory) & '\' & LocalDir:name
        FirstPartName  = ImportFileName[ 1 : len(clip(ImportFileName))-3 ]
        TempFullLine   = ''
        
        Open(ImportFile)
        
        If error() then
            message('Error on open file')
            break
        END

        Set(ImportFile)

        Loop
            Next(ImportFile)
            if error() then
                !nothing more to do
                Break
            END
            TempFullLine = clip(TempFullLine) &  left(impfile:Inputline)
        END !copy import file to TempFullLine

        Close(ImportFile)

        if LocalArchiveFlag then
            copy(ImportFileName,SaveDirectory)
        END

        remove(OutputFileName)

        !interpreting import file name goes here
        !find username and password
        XMLUserName = XMLTrack('Username')
        XMLPassword = XMLTrack('Password')

        Message('XML : '&clip(XMLUserName)&': '&clip(XMLPassword))
        !End dealing with import file
        
    END !loop through files in the directory

    EXIT


!    Outfile:Outputline = '<XML Desc>'      & 'Waybill Generated XML'         &  '</XML Desc>'
!    Add(outputFile)
!    Outfile:Outputline = '<Username>'      & 'User Name'                     &  '</Username>'
!    Add(outputFile)
!    Outfile:Outputline = '<Password>'      & 'Pa55w0rd'                      &  '</Password>'
!    Add(outputFile)
!    Outfile:Outputline = '<Waybill No>'    & '12345'                         &  '</Waybill No>'
!    Add(outputFile)
!    Outfile:Outputline = '<Waybill Type>'  & 'Job'                           &  '</Waybill Type>'
!    Add(outputFile)
!    Outfile:Outputline = '<Sender>'        & 'Vodacom Repairs Fourways Mall' &  '</Sender>'
!    Add(outputFile)
!    Outfile:Outputline = '<Date Time>'     & '04/11/1965 14:31'              &  '</Date Time>'
!    Add(outputFile)
!    Outfile:Outputline = '<Receiver>'      & 'Vodacom Repairs ARC'           &  '</Receiver>'
!    Add(outputFile)
!    Outfile:Outputline = '<Job No>'        & '23456'                         &  '</Job No>'
!    Add(outputFile)
!    Outfile:Outputline = '<Make Model>'    & 'Nokia 3300'                    &  '</Make Model>'
ExportRequests      Routine

    !look through new SB_RAM file to get unsent
    !look through new sB_RAM file to get those sent, but not replied and resend

    FirstPartName  = CLIP(FileDirectory)&'\Trial_'&format(clock(),@t05)

    OutputFileName =clip(FirstPartName) & '.txa'
    Create(OutputFile)
    Open(outputFile)
    
    !XML Field Name
    Outfile:Outputline = '<XML Desc>'      & 'Waybill Generated XML'         &  '</XML Desc>'
    Add(outputFile)
    Outfile:Outputline = '<Username>'      & 'User Name'                     &  '</Username>'
    Add(outputFile)
    Outfile:Outputline = '<Password>'      & 'Pa55w0rd'                      &  '</Password>'
    Add(outputFile)
    Outfile:Outputline = '<Waybill No>'    & '12345'                         &  '</Waybill No>'
    Add(outputFile)
    Outfile:Outputline = '<Waybill Type>'  & 'Job'                           &  '</Waybill Type>'
    Add(outputFile)
    Outfile:Outputline = '<Sender>'        & 'Vodacom Repairs Fourways Mall' &  '</Sender>'
    Add(outputFile)
    Outfile:Outputline = '<Date Time>'     & '04/11/1965 14:31'              &  '</Date Time>'
    Add(outputFile)
    Outfile:Outputline = '<Receiver>'      & 'Vodacom Repairs ARC'           &  '</Receiver>'
    Add(outputFile)
    Outfile:Outputline = '<Job No>'        & '23456'                         &  '</Job No>'
    Add(outputFile)
    Outfile:Outputline = '<Make Model>'    & 'Nokia 3300'                    &  '</Make Model>'
    Add(outputFile)

    Close(outputFile)

    if LocalArchiveFlag then
        copy(Outputfilename,SaveDirectory)
    END

    !this bit of code needs to be done to trigger pickup and send by API
    Rename(Outputfilename, clip(FirstPartName)&'.out')


    EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Button1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Button1
       POST(Event:CloseWindow)
    END
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:Timer
      !Timer runs every second and calls this section
      !message('Timer is run')
      
      !three parts to work
      !ONE
      !refetch the defaults in case anything has changed
      Do RefreshDefaults
      
      !TWO
      !
      !look through file to see if any Waybills are ready to send
      Do ExportRequests
      
      !THREE
      !
      !look to see if there have been any resonses and deal with them
      Do ImportResponses
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

XMLTrack  Procedure(String XMLDetail)    !usage localvariable = XMLTrack('DETAIL')

LeftString  String(20)
RightString String(20)
LeftEnd     long
RightEnd    Long

code

    leftString  = '<'&clip(XMLDetail)&'>'
    RightString = '</'&clip(XMLDetail)&'>'

    LeftEnd     = instring(clip(leftString),TempFullLine,1,1)
    LeftEnd    += len(clip(LeftString))

    If leftEnd = 0 then return('').

    RightEnd = instring(clip(RightString),TempFullLine,1,LeftEnd)  - 1

    if RightEnd = 0 then return('').

    return(TempFullLine[ leftEnd : RightEnd ])
