  MEMBER('impcurr.clw')

  INCLUDE('ABFILE.INC'),ONCE

  MAP
impcurr_BC0:DctInit    PROCEDURE
impcurr_BC0:DctKill    PROCEDURE
impcurr_BC0:FilesInit  PROCEDURE
  END

Hide:Access:CURRENCY CLASS(FileManager),TYPE               ! FileManager for CURRENCY
Init                   PROCEDURE(),DERIVED
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
UseFile                PROCEDURE(BYTE UseType = UseType:Uses),BYTE,PROC,DERIVED
                     END


Hide:Relate:CURRENCY CLASS(RelationManager),TYPE           ! RelationManager for CURRENCY
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


_Hide:Access:CURRENCY &Hide:Access:CURRENCY,AUTO,THREAD
_Hide:Relate:CURRENCY &Hide:Relate:CURRENCY,AUTO,THREAD


impcurr_BC0:DctInit PROCEDURE
  CODE
  _Hide:Access:CURRENCY &= NEW(Hide:Access:CURRENCY)
  _Hide:Relate:CURRENCY &= NEW(Hide:Relate:CURRENCY)
  Relate:CURRENCY &= _Hide:Relate:CURRENCY


impcurr_BC0:FilesInit PROCEDURE
  CODE
  _Hide:Relate:CURRENCY.Init


impcurr_BC0:DctKill PROCEDURE
  CODE
  _Hide:Relate:CURRENCY.Kill
  DISPOSE(_Hide:Relate:CURRENCY)


Hide:Relate:CURRENCY.Init PROCEDURE
  CODE
  _Hide:Access:CURRENCY.Init
  SELF.Init(Access:CURRENCY,1)


Hide:Access:CURRENCY.Init PROCEDURE

  CODE
  SELF.Initialized = False
  SELF.Buffer &= cur:Record
  SELF.FileNameValue = 'CURRENCY'
  SELF.SetErrors(GlobalErrors)
  SELF.File &= CURRENCY
  PARENT.Init
  Access:CURRENCY &= SELF


Hide:Access:CURRENCY.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:CURRENCY &= NULL                                  ! File manager has been destroyed to assign null, it is an error to reference this after this point


Hide:Access:CURRENCY.PrimeFields PROCEDURE

  CODE
  cur:LastUpdateDate = Today()                             ! Assign initial field value
  PARENT.PrimeFields


Hide:Access:CURRENCY.UseFile PROCEDURE(BYTE UseType = UseType:Uses)

ReturnValue          BYTE,AUTO

  CODE
  IF UseType ~= UseType:Initialize
    SELF.UseFile(UseType:Initialize)                       !Recursive call to ensure initialization takes place
  END
  IF UseType = UseType:Initialize AND ~SELF.Initialized
    SELF.InUseFile = True
    SELF.Init(CURRENCY,GlobalErrors)
    SELF.Create = 1
    SELF.LockRecover = 10
    SELF.AddKey(cur:RecordNumberKey,'By Record Number',1)
    SELF.AddKey(cur:CurrencyCodeKey,'By Currency Code',0)
    SELF.AddKey(cur:LastUpdateDateKey,'By Currency Code',0)
    SELF.AddKey(cur:CorrelationCodeKey,'By Correlation Code',0)
  END
                                                           !SELF.InUseFile will be set to False in PARENT
  ReturnValue = PARENT.UseFile(UseType)
  RETURN ReturnValue


Hide:Relate:CURRENCY.Kill PROCEDURE

  CODE
  _Hide:Access:CURRENCY.Kill                               ! Kill the file manager
  PARENT.Kill
  Relate:CURRENCY &= NULL                                  ! Assign NULL to the RelationManager's FileManager reference, it is an error to reference this after this point
  DISPOSE(_Hide:Access:CURRENCY)                           ! destroy the file manager

