

   MEMBER('impcurr.clw')                              ! This is a MEMBER module

                     MAP
                       INCLUDE('IMPCU001.INC'),ONCE        !Local module procedure declarations
                     END


Main                 PROCEDURE                        ! Declare Procedure
locImportFile       Cstring(255),static 
ImportFile          File,Driver('ASCII'),Pre(impfil),Name(locImportFile),Create,Bindable,Thread
Record                  Record
Space1                  String(8)
CurrencyCode            String(10) ! Allow extra space for future
Rate                    String(17) ! 6 decimal places
                        End
                    End

                    MAP
WriteToLog              Procedure(String fText)
                    END
prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

  CODE
   Relate:CURRENCY.Open
    WriteToLog('== Started Import ==')
    
    locImportFile = clip(GETINI('EXCHANGERATE','ImportPath',,Clip(Path()) & '\SB2KDEF.INI'))
    
    if (not exists(locImportFile))
        WriteToLog('Import File Doesn''t Exist')
        RETURN
    END
    
    Open(ImportFile)
    If (Error())
        WriteToLog('Error Opening File - ' & clip(error()))
        RETURN
    END
    
    count# = 0
    set(ImportFile)
    LOOP
        next(ImportFile)
        if (Error())
            BREAK
        END
        count# += 1
       
    END
    !prog.init(count#)

    updated# = 0
    read# = 0
    set(ImportFile,0)
    LOOP
        next(importfile)
        if (error())
            BREAK
        END
        !if (!prog.update())
        !    BREAK
        !END
        
        If (impfil:CurrencyCode = '' OR |
            Instring('Currency',impfil:CurrencyCode,1,1) or | 
            instring('Valuation',impfil:CurrencyCode,1,1))
            CYCLE
        END
        
        read# += 1
        Access:CURRENCY.Clearkey(cur:CurrencyCodeKey)
        cur:CurrencyCode = Clip(Upper(impfil:CurrencyCode))
        If (Access:CURRENCY.TryFetch(cur:CurrencyCodeKey))
            Access:CURRENCY.Clearkey(cur:CorrelationCodeKey)
            cur:CorrelationCode = Clip(Upper(impfil:CurrencyCode))
            If (Access:CURRENCY.TryFetch(cur:CorrelationCodeKey))
                CYCLE
            END
        END
        if (cur:DailyRate <> impfil:Rate)
            cur:DailyRate = impfil:Rate
            cur:LastUpdateDate = Today()
            Access:CURRENCY.TryUpdate()
            updated# += 1
        END
            
    END
    
    Close(ImportFile)
    !prog.kill()
    
    WriteToLog('Read: ' & read# & ' / Updated: ' & updated#)
    
    Remove(locImportFile)
    if (Exists(locImportFile))
        WriteToLog('Error: Import File Not Deleted - ' & Error())
    END
    WriteToLog('== Finished Import ==')
    
   Relate:CURRENCY.Close
WriteToLog          Procedure(String fText)
CODE
    LinePrint(Format(Today(),@d06) & ' ' & Format(clock(), @t01) & ': ' & Clip(fText),'IMPCURR.LOG')
prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        prog.ProgressSetup(func:Records)
prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Source
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

prog.ResetProgress      Procedure(Long func:Records)
CODE

    prog.recordsToProcess = func:Records
    prog.recordsprocessed = 0
    prog.percentProgress = 0
    prog.progressThermometer = 0
    prog.CNprogressThermometer = 0
    prog.skipRecords = 0
    prog.userText = ''
    prog.CNuserText = ''
    prog.percentText = '0% Completed'
    prog.CNpercentText = prog.percentText


prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (prog.InsideLoop(func:String))
prog.InsideLoop     Procedure(<String func:String>)
CODE

    prog.SkipRecords += 1
    If prog.SkipRecords < 20
        prog.RecordsProcessed += 1
        Return 0
    Else
        prog.SkipRecords = 0
    End
    if (func:String <> '')
        prog.UserText = Clip(func:String)
        prog.CNUserText = prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        prog.NextRecord()
        if (prog.CancelLoop())
            return 1
        end ! if (prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

prog.ProgressText        Procedure(String    func:String)
CODE

    prog.UserText = Clip(func:String)
    prog.CNUserText = prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

prog.Kill     Procedure()
    CODE
        prog.ProgressFinish()
prog.ProgressFinish     Procedure()
CODE

    prog.ProgressThermometer = 100
    prog.CNProgressThermometer = 100
    prog.PercentText = '100% Completed'
    prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

prog.NextRecord      Procedure()
CODE
    Yield()
    prog.RecordsProcessed += 1
    !If prog.percentprogress < 100
        prog.percentprogress = (prog.recordsprocessed / prog.recordstoprocess)*100
        If prog.percentprogress > 100 or prog.percentProgress < 0
            prog.percentprogress = 0
        End
        If prog.percentprogress <> prog.ProgressThermometer then
            prog.ProgressThermometer = prog.percentprogress
            prog.PercentText = format(prog:percentprogress,@n3) & '% Completed'
        End
    !End
    prog.CNPercentText = prog.PercentText
    prog.CNProgressThermometer = prog.ProgressThermometer
    Display()

prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
