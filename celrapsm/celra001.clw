

   MEMBER('celrapsm.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('CELRA001.INC'),ONCE        !Local module procedure declarations
                     END


Main                 PROCEDURE                        ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:MinimumLevel     LONG
tmp:QuantityToOrder  LONG
tmp:QuantityOnOrder  LONG
tmp:Usage            LONG
save_sto_id          USHORT,AUTO
save_orp_id          USHORT,AUTO
save_ope_id          USHORT,AUTO
save_shi_id          USHORT,AUTO
save_smin_id         USHORT,AUTO
tmp:Location         STRING(30)
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
  CODE
   Relate:LOCATION.Open
   Relate:DEFAULTS.Open
   Relate:PARTS.Open
   Relate:WARPARTS.Open
   Relate:ORDPARTS.Open
   Relate:ORDPEND.Open
   Relate:ORDERS.Open
   Relate:STOCK.Open
   Relate:STOHIST.Open
   Relate:SUPPLIER.Open
   Relate:DEFSTOCK.Open
   Relate:TRADEACC.Open
    ! Close Splash Window (DBH: 24-05-2005)
    PUTINI('STARTING','Run',True,)
          recordspercycle         = 25
          recordsprocessed        = 0
          percentprogress         = 0
          progress:thermometer    = 0
  !        thiswindow.reset(1)
          open(progresswindow)

          ?progress:userstring{prop:text} = 'Running...'
          ?progress:pcttext{prop:text} = '0% Completed'

          !Make File!
          glo:File_Name = GETINI('STOCK','ScheduledFileName',,CLIP(PATH()) & '\SB2KDEF.INI')
          IF glo:File_Name <> ''
            REMOVE(StockMin)
          END
          glo:File_Name   = 'S' & Format(CLock(),@n07) & '.TMP'
          PUTINI('STOCK','ScheduledFileName',glo:File_Name,CLIP(PATH()) & '\SB2KDEF.INI')
          PUTINI('STOCK','ScheduledFileDate',TODAY(),CLIP(PATH()) & '\SB2KDEF.INI')
          PUTINI('STOCK','ScheduledFileTime',CLOCK(),CLIP(PATH()) & '\SB2KDEF.INI')
          Relate:StockMin.Open()

          recordstoprocess    = Records(STOCK)

          Save_sto_ID = Access:STOCK.SaveFile()
          Access:STOCK.ClearKey(sto:Ref_Number_Key)
          sto:Ref_Number = 1
          Set(sto:Ref_Number_Key,sto:Ref_Number_Key)
          Loop
              If Access:STOCK.NEXT()
                 Break
              End !If
              Do GetNextRecord2
              cancelcheck# += 1
              If cancelcheck# > (RecordsToProcess/100)
                  Do cancelcheck
                  If tmp:cancel = 1
                      Break
                  End!If tmp:cancel = 1
                  cancelcheck# = 0
              End!If cancelcheck# > 50


              !Work Out Supplier Usage Value
              Access:SUPPLIER.ClearKey(sup:Company_Name_Key)
              sup:Company_Name = sto:Supplier
              If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
                  !Found

              Else!If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End!If Access:SUPPLIER.TryFetch(sup:Company_Name_Key) = Level:Benign

              !Work Out Minimum Level
              tmp:Usage   = 0
              tmp:MinimumLevel    = 0
              Save_shi_ID = Access:STOHIST.SaveFile()
              Access:STOHIST.ClearKey(shi:Transaction_Type_Key)
              shi:Ref_Number       = sto:Ref_Number
              shi:Transaction_Type = 'DEC'
              shi:Date             = Today() - sup:HistoryUsage
              Set(shi:Transaction_Type_Key,shi:Transaction_Type_Key)
              Loop
                  If Access:STOHIST.NEXT()
                     Break
                  End !If
                  If shi:Ref_Number       <> sto:Ref_Number      |
                  Or shi:Transaction_Type <> 'DEC'      |
                  Or shi:Date             > Today()     |
                      Then Break.  ! End If

                  IF INSTRING('STOCK AUDIT',shi:despatch_note_number,1)
                    CYCLE
                  END
                  tmp:Usage   += shi:Quantity

              End !Loop
              Access:STOHIST.RestoreFile(Save_shi_ID)

              tmp:MinimumLevel    = ABS(tmp:Usage * sup:Factor/100)
              !Work Out Quantity On Order
              If def:SummaryOrders !New Order Process

                  If tmp:MinimumLevel - sto:QuantityRequested - sto:Quantity_Stock > 1
                      If Access:STOCKMIN.PrimeRecord() = Level:Benign
                          smin:Location            = sto:Location
                          smin:PartNumber          = sto:Part_Number
                          smin:Description         = sto:Description
                          smin:Supplier            = sto:Supplier
                          smin:Usage               = tmp:Usage
                          smin:MinLevel            = tmp:MinimumLevel
                          smin:QuantityOnOrder     = sto:QuantityRequested
                          smin:QuantityInStock     = sto:Quantity_Stock
                          smin:QuantityRequired    = (tmp:MinimumLevel - sto:QuantityRequested - sto:Quantity_Stock)
                          smin:PartRefNumber       = sto:Ref_number
                          smin:QuantityJobsPending = ''

                          If Access:STOCKMIN.TryInsert() = Level:Benign
                              !Insert Successful
                          Else !If Access:STOCKMIN.TryInsert() = Level:Benign
                              !Insert Failed
                          End !If Access:STOCKMIN.TryInsert() = Level:Benign
                      End !If Access:STOCKMIN.PrimeRecord() = Level:Benign
                  End !If sto:Minimum_Stock - sto:QuantityRequested - sto:Quantity_Stock > 1
              Else !If def:SummaryOrders
                  tmp:QuantityToOrder = 0
                  save_ope_id = access:ordpend.savefile()                                                 !awaiting order
                  access:ordpend.clearkey(ope:part_ref_number_key)
                  ope:part_ref_number =  sto:ref_number
                  set(ope:part_ref_number_key,ope:part_ref_number_key)
                  loop
                      if access:ordpend.next()
                         break
                      end !if
                      if ope:part_ref_number <> sto:ref_number      |
                          then break.  ! end if
                      yldcnt# += 1
                      if yldcnt# > 25
                         yield() ; yldcnt# = 0
                      end !if
                      tmp:QuantityToOrder += ope:quantity
                  end !loop
                  access:ordpend.restorefile(save_ope_id)

                  tmp:QuantityOnOrder = 0
                  setcursor(cursor:wait)                                                                  !file. But check with the order
                  save_orp_id = access:ordparts.savefile()                                                !file first to see if the whole
                  access:ordparts.clearkey(orp:ref_number_key)                                            !order has been received. This
                  orp:part_ref_number = sto:ref_number                                                    !should hopefully speed things
                  set(orp:ref_number_key,orp:ref_number_key)                                              !up.
                  loop
                      if access:ordparts.next()
                         break
                      end !if
                      if orp:part_ref_number <> sto:ref_number      |
                          then break.  ! end if
                      If orp:order_number <> ''
                          access:orders.clearkey(ord:order_number_key)
                          ord:order_number = orp:order_number
                          if access:orders.fetch(ord:order_number_key) = Level:Benign
                              If ord:all_received = 'NO'
                                  If orp:all_received = 'NO'
                                      tmp:QuantityOnOrder += orp:quantity
                                  End!If orp:all_received <> 'YES'
                              End!If ord:all_received <> 'YES'
                          end!if access:orders.fetch(ord:order_number_key) = Level:Benign
                      End!If orp:order_number <> ''
                  end !loop
                  access:ordparts.restorefile(save_orp_id)

                  If tmp:MinimumLevel - (tmp:QuantityOnOrder + tmp:QuantityToOrder) - sto:Quantity_Stock > 1
                      If Access:STOCKMIN.PrimeRecord() = Level:Benign
                          smin:Location            = sto:Location
                          smin:PartNumber          = sto:Part_Number
                          smin:Description         = sto:Description
                          smin:Supplier            = sto:Supplier
                          smin:Usage               = tmp:Usage
                          smin:MinLevel            = tmp:MinimumLevel
                          smin:QuantityOnOrder     = (tmp:QuantityOnOrder + tmp:QuantityToOrder)
                          smin:QuantityInStock     = sto:Quantity_Stock
                          smin:QuantityRequired    = tmp:MinimumLevel - (tmp:QuantityOnOrder + tmp:QuantityToOrder) - sto:Quantity_Stock
                          smin:PartRefNumber       = sto:Ref_number
                          smin:QuantityJobsPending = ''

                          If Access:STOCKMIN.TryInsert() = Level:Benign
                              !Insert Successful
                          Else !If Access:STOCKMIN.TryInsert() = Level:Benign
                              !Insert Failed
                          End !If Access:STOCKMIN.TryInsert() = Level:Benign
                      End !If Access:STOCKMIN.PrimeRecord() = Level:Benign
                  End !If tmp:MinimumLevel - (tmp:QuantityOnOrder + tmp:QuantityToOrder) - sto:Quantity_Stock > 1

              End !If def:SummaryOrders

          End !Loop
          Access:STOCK.RestoreFile(Save_sto_ID)
          Do EndPrintRun
          close(progresswindow)
          Access:StockMin.Close()
   Relate:LOCATION.Close
   Relate:DEFAULTS.Close
   Relate:PARTS.Close
   Relate:WARPARTS.Close
   Relate:ORDPARTS.Close
   Relate:ORDPEND.Close
   Relate:ORDERS.Close
   Relate:STOCK.Close
   Relate:STOHIST.Close
   Relate:SUPPLIER.Close
   Relate:DEFSTOCK.Close
   Relate:TRADEACC.Close
    RETURN
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','ServiceBase 3g',|
                       'mquest.jpg','\No|/Yes') 
            Of 2 ! Yes Button
                tmp:cancel = 1
            Of 1 ! No Button
        End!Case MessageEx
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()
