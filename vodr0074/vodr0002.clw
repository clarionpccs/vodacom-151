

   MEMBER('vodr0074.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('VODR0002.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('VODR0001.INC'),ONCE        !Req'd for module callout resolution
                     END


WarrantyClaimReportCriteria PROCEDURE                 !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::2:TAGFLAG          BYTE(0)
DASBRW::2:TAGMOUSE         BYTE(0)
DASBRW::2:TAGDISPSTATUS    BYTE(0)
DASBRW::2:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
tmp:VersionNumber    STRING(30)
save_joo_id          USHORT,AUTO
save_wpr_id          USHORT,AUTO
tmp:JobNumber        LONG
tmp:SelectCriteria   BYTE(0)
tmp:tag              STRING(1)
tmp:SelectManufacturer BYTE(0)
tmp:Manufacturer     STRING(30)
tmp:StartDate        DATE
tmp:EndDate          DATE
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?tmp:Manufacturer
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(TRADEACC)
                       PROJECT(tra:Account_Number)
                       PROJECT(tra:Company_Name)
                       PROJECT(tra:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:tag                LIKE(tmp:tag)                  !List box control field - type derived from local data
tmp:tag_Icon           LONG                           !Entry's icon ID
tra:Account_Number     LIKE(tra:Account_Number)       !List box control field - type derived from field
tra:Company_Name       LIKE(tra:Company_Name)         !List box control field - type derived from field
tra:RecordNumber       LIKE(tra:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB3::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
window               WINDOW('Warranty Claim Report'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(60,38,560,360),USE(?PanelMain),FILL(0D6EAEFH)
                       PANEL,AT(64,40,552,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PANEL,AT(64,56,552,44),USE(?PanelTip),FILL(0D6EAEFH)
                       GROUP('Top Tip'),AT(68,60,544,36),USE(?GroupTip),BOXED,TRN
                       END
                       STRING(@s255),AT(72,76,496,),USE(SRN:TipText),TRN
                       BUTTON,AT(576,64,36,32),USE(?ButtonHelp),TRN,FLAT,ICON('F1Help.jpg')
                       PROMPT('SRN:0000000'),AT(564,42),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Warranty Claim Report Criteria'),AT(68,42),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(64,366,552,28),USE(?PanelButton),FILL(09A6A7CH)
                       PANEL,AT(64,104,552,258),USE(?Panel5),FILL(09A6A7CH)
                       PROMPT('Print Warranty Claim Report For single Job or select critiera'),AT(245,110),USE(?Prompt2),FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       PROMPT('Job Number'),AT(245,126),USE(?tmp:JobNumber:Prompt),TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@s8),AT(321,126,64,10),USE(tmp:JobNumber),FONT('Arial',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Job Number'),TIP('Job Number'),UPR
                       CHECK('Select Criteria'),AT(321,144),USE(tmp:SelectCriteria),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),MSG('Select Criteria'),TIP('Select Criteria'),VALUE('1','0')
                       GROUP,AT(64,154,552,206),USE(?Group:Criteria),HIDE
                         OPTION('Select Manufacturer'),AT(392,220,216,32),USE(tmp:SelectManufacturer),BOXED,FONT(,,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           RADIO('All'),AT(400,233),USE(?tmp:SelectManufacturer:Radio1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('0')
                           RADIO('One'),AT(440,233),USE(?tmp:SelectManufacturer:Radio2),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),VALUE('1')
                         END
                         COMBO(@s20),AT(480,234,124,10),USE(tmp:Manufacturer),IMM,DISABLE,VSCROLL,FONT('Arial',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                         BUTTON,AT(296,234),USE(?DASTAG),TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                         BUTTON,AT(296,266),USE(?DASTAGAll),TRN,FLAT,LEFT,ICON('tagallp.jpg')
                         BUTTON,AT(296,298),USE(?DASUNTAGALL),TRN,FLAT,LEFT,ICON('untagalp.jpg')
                         LIST,AT(72,172,216,186),USE(?List),IMM,VSCROLL,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)I@s1@60L(2)|M~Account Number~@s15@120L(2)|M~Company Name~@s30@'),FROM(Queue:Browse)
                         PROMPT('Select Account'),AT(72,158),USE(?Prompt3),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         PROMPT('Completed Start Date'),AT(408,286),USE(?tmp:StartDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@d6),AT(496,286,64,10),USE(tmp:StartDate),LEFT,FONT('Arial',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Completed Start Date'),TIP('Completed Start Date'),UPR
                         BUTTON,AT(564,282),USE(?LookupStartDate),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                         PROMPT('Completed End Date'),AT(408,306),USE(?tmp:EndDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         ENTRY(@d6),AT(496,306,64,10),USE(tmp:EndDate),LEFT,FONT('Arial',8,010101H,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Completed End Date'),TIP('Completed End Date'),UPR
                         BUTTON,AT(564,302),USE(?LookupEndDate),SKIP,TRN,FLAT,ICON('lookupp.jpg')
                       END
                       BUTTON('&Rev tags'),AT(256,252,2,2),USE(?DASREVTAG),HIDE
                       BUTTON('sho&W tags'),AT(252,272,2,2),USE(?DASSHOWTAG),HIDE
                       BUTTON,AT(480,366),USE(?OK),TRN,FLAT,LEFT,ICON('printp.jpg')
                       BUTTON,AT(548,366),USE(?Close),TRN,FLAT,LEFT,ICON('cancelp.jpg')
                       PROMPT('Report Version'),AT(68,376),USE(?ReportVersion),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
FDCB3                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::2:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW1.UpdateBuffer
   glo:Queue.Pointer = tra:Account_Number
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = tra:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:tag = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:tag = ''
  END
    Queue:Browse.tmp:tag = tmp:tag
  IF (tmp:tag = '*')
    Queue:Browse.tmp:tag_Icon = 2
  ELSE
    Queue:Browse.tmp:tag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::2:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW1.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = tra:Account_Number
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::2:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW1.Reset
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::2:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::2:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::2:QUEUE = glo:Queue
    ADD(DASBRW::2:QUEUE)
  END
  FREE(glo:Queue)
  BRW1.Reset
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::2:QUEUE.Pointer = tra:Account_Number
     GET(DASBRW::2:QUEUE,DASBRW::2:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = tra:Account_Number
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::2:DASSHOWTAG Routine
   CASE DASBRW::2:TAGDISPSTATUS
   OF 0
      DASBRW::2:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::2:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::2:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW1.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020602'&'0'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('WarrantyClaimReportCriteria')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:MANUFACT.Open
  SELF.FilesOpened = True
  tmp:StartDate   = Deformat('1/1/2000',@d6)
  tmp:EndDate     = Today()
  BRW1.Init(?List,Queue:Browse.ViewPosition,BRW1::View:Browse,Queue:Browse,Relate:TRADEACC,SELF)
  OPEN(window)
  SELF.Opened=True
  ?List{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  ! ========= Set Report Version =============
  Include('..\ReportVersion.inc')
  tmp:VersionNumber = 'Version Number: ' & Clip(tmp:VersionNumber) & '5001'
  ?ReportVersion{prop:Text} = tmp:VersionNumber
  BRW1.Q &= Queue:Browse
  BRW1.AddSortOrder(,tra:Account_Number_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,tra:Account_Number,1,BRW1)
  BIND('tmp:tag',tmp:tag)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW1.AddField(tmp:tag,BRW1.Q.tmp:tag)
  BRW1.AddField(tra:Account_Number,BRW1.Q.tra:Account_Number)
  BRW1.AddField(tra:Company_Name,BRW1.Q.tra:Company_Name)
  BRW1.AddField(tra:RecordNumber,BRW1.Q.tra:RecordNumber)
  IF ?tmp:SelectCriteria{Prop:Checked} = True
    UNHIDE(?Group:Criteria)
  END
  IF ?tmp:SelectCriteria{Prop:Checked} = False
    HIDE(?Group:Criteria)
  END
  FDCB3.Init(tmp:Manufacturer,?tmp:Manufacturer,Queue:FileDropCombo.ViewPosition,FDCB3::View:FileDropCombo,Queue:FileDropCombo,Relate:MANUFACT,ThisWindow,GlobalErrors,0,1,0)
  FDCB3.Q &= Queue:FileDropCombo
  FDCB3.AddSortOrder(man:Manufacturer_Key)
  FDCB3.AddField(man:Manufacturer,FDCB3.Q.man:Manufacturer)
  FDCB3.AddField(man:RecordNumber,FDCB3.Q.man:RecordNumber)
  ThisWindow.AddItem(FDCB3.WindowComponent)
  FDCB3.DefaultFill = 0
  BRW1.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:MANUFACT.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ButtonHelp
      ThisWindow.Update
      !Open The Help Page
      omit('***',ClarionetUsed=0)
          If glo:WebJob Then
              ClarioNET:CallClientProcedure('OPENHELP', Clip(GETINI('ONLINEHELP','URL',,CLIP(Path()) & '\EXTRADEFS.INI')) & 'srn' & '020602'&'0' & '.htm')
          Else !glo:WebJob Then
              HelpBrowser('020602'&'0')
          End !glo:WebJob
      ***
      Omit('***',ClarionetUsed = 1)
      HelpBrowser('020602'&'0')
      ***
    OF ?tmp:SelectCriteria
      IF ?tmp:SelectCriteria{Prop:Checked} = True
        UNHIDE(?Group:Criteria)
      END
      IF ?tmp:SelectCriteria{Prop:Checked} = False
        HIDE(?Group:Criteria)
      END
      ThisWindow.Reset
    OF ?tmp:SelectManufacturer
      Case tmp:SelectManufacturer
          Of 0
              ?tmp:Manufacturer{prop:Disable} = 1
          Of 1
              ?tmp:Manufacturer{prop:Disable} = 0
      End !tmp:SelectManufacturer
      Display()
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::2:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::2:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::2:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?LookupStartDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?LookupEndDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::2:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::2:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?OK
      ThisWindow.Update
      If tmp:SelectCriteria = 1
          tmp:JobNumber = 0
      End !tmp:SelectCriteria = 0
      If tmp:SelectManufacturer = 0
          tmp:Manufacturer = ''
      End !tmp:SelectManufacturer = 0
      WarrantyClaimReport(tmp:JobNumber,tmp:StartDate,tmp:EndDate,tmp:Manufacturer)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupStartDate)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupEndDate)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::2:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRW1.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = tra:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:tag = ''
    ELSE
      tmp:tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:tag = '*')
    SELF.Q.tmp:tag_Icon = 2
  ELSE
    SELF.Q.tmp:tag_Icon = 1
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW1::RecordStatus=ReturnValue
  IF BRW1::RecordStatus NOT=Record:OK THEN RETURN BRW1::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = tra:Account_Number
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::2:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW1::RecordStatus
  RETURN ReturnValue

