

   MEMBER('archive.clw')                              ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('ARCHI001.INC'),ONCE        !Local module procedure declarations
                     END


Main PROCEDURE                                        !Generated from procedure template - Frame

tmp:ArchivePath      STRING(255)
tmp:VersionNumber    STRING(30)
tmp:LocalServiceBaseVersion STRING(30)
tmp:ArchiveServiceBaseVersion STRING(30)
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:AlreadyExists    BYTE(0)
pos                  STRING(255)
AppFrame             WINDOW('Caption'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,MAXIMIZE,DOUBLE
                       PANEL,AT(160,64,360,300),USE(?PanelMain:2),FILL(0D6EAEFH)
                       PROMPT('ServiceBase Archive Facility'),AT(168,70),USE(?WindowTitle:2),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse:2),FILL(09A6A7CH)
                       PANEL,AT(164,84,352,244),USE(?Panel4),FILL(09A6A7CH)
                       PROMPT('ServiceBase Archive Routine'),AT(260,96),USE(?Prompt2),TRN,FONT(,12,0FFFF80H,FONT:bold)
                       PROMPT('It is not recommended that this archiving is performed while ServiceBase is in u' &|
   'se.'),AT(182,122),USE(?Prompt3),TRN,FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       GROUP('Location Of "Archived" version of ServiceBase'),AT(173,140,335,92),USE(?Group1),BOXED,TRN,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         BUTTON,AT(472,156),USE(?LookupFile),TRN,FLAT,ICON('lookupp.jpg')
                         STRING(@s255),AT(308,160,160,12),USE(tmp:ArchivePath),FONT(,10,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         PROMPT('"Archive" ServiceBase Path:'),AT(180,160),USE(?Prompt8)
                         PROMPT('"Live" ServiceBase Version:'),AT(180,180),USE(?Prompt6)
                         STRING(@s30),AT(308,180),USE(tmp:LocalServiceBaseVersion),FONT(,10,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         STRING(@s30),AT(308,196),USE(tmp:ArchiveServiceBaseVersion),FONT(,10,080FFFFH,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         PROMPT('"Archive" ServiceBase Version:'),AT(180,196),USE(?Prompt7)
                         PROMPT('If the "archive" version of ServiceBase does not match, loss of data may occur.'),AT(193,216),USE(?Prompt4),TRN,FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       END
                       BUTTON,AT(384,252),USE(?PopCalendar),TRN,FLAT,ICON('lookupp.jpg')
                       GROUP('Archive Date Range'),AT(172,240,336,72),USE(?Group2),BOXED,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                         PROMPT('All jobs BOOKED within the above date range, regardless of status, will be archi' &|
   'ved.'),AT(184,296),USE(?Prompt10),FONT(,,0D0FFD0H,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       END
                       PROMPT('Start Date'),AT(260,256),USE(?tmp:StartDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@d6b),AT(315,256,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Start Date'),TIP('Start Date'),UPR
                       BUTTON,AT(384,272),USE(?PopCalendar:2),TRN,FLAT,ICON('lookupp.jpg')
                       PROMPT('End Date'),AT(260,276),USE(?tmp:EndDate:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       ENTRY(@d6b),AT(315,276,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('End Date'),TIP('End Date'),UPR
                       PROMPT('Report Version'),AT(172,316),USE(?ReportVersion),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                       BUTTON,AT(444,332),USE(?Close),TRN,FLAT,ICON('closep.jpg')
                       PANEL,AT(164,332,352,28),USE(?PanelButton:2),FILL(09A6A7CH)
                       BUTTON,AT(172,332),USE(?Button:BeginArchive),TRN,FLAT,ICON('begarcp.jpg')
                     END

Prog        Class
recordsToProcess    Long
recordsProcessed    Long
percentProgress     Byte
skipRecords         Long
hidePercentText     Byte
userText            String(100)
percentText         String(100)
progressThermometer Byte
CNuserText            String(100)
CNpercentText         String(100)
CNprogressThermometer Byte

ProgressSetup      Procedure(Long func:Records)
Init               Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(<String func:String>),Byte
Update             Procedure(<String func:String>),Byte
ProgressText       Procedure(String func:String)
NextRecord         Procedure()
CancelLoop         Procedure(),Byte
ProgressFinish     Procedure()
Kill               Procedure()

            End
! moving bar window
Prog:ProgressWindow WINDOW('Progress...'),AT(,,210,63),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Prog.ProgressThermometer),AT(4,17,201,11),RANGE(0,100)
       STRING(@s100),AT(3,34,203,10),USE(Prog.PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(3,3,203,10),USE(Prog.UserText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(80,44,50,16),USE(?Prog:CancelButton)
     END

omit('***',ClarionetUsed=0)

!static webjob window
Prog:CNProgressWindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(Prog.CNUserText),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?Prog:CNPrompt),FONT(,14,,FONT:bold)
     END
***

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FileLookup7          SelectFileClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

DisplayVersion      Routine
    Relate:DEFAULTV.Open()
    file:DEFAULTV = Clip(tmp:ArchivePath) & '\DEFAULTV.DAT'
    Relate:ARCDEFAULTV.Open()
    Set(DEFAULTV)
    Access:DEFAULTV.Next()
    Set(ARCDEFAULTV)
    Access:ARCDEFAULTV.Next()
    tmp:LocalServiceBaseVersion = defv:VersionNumber
    tmp:ArchiveServiceBaseVersion = adefv:VersionNumber
    Relate:DEFAULTV.Close()
    Relate:ARCDEFAULTV.Close()
    Display()

ThisWindow.Ask PROCEDURE

  CODE
  0{prop:Text} = 'ServiceBase 3g'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain:2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:SMSMAIL.Open
  SELF.FilesOpened = True
  OPEN(AppFrame)
  SELF.Opened=True
  ! ========= Set Report Version =============
  Include('..\ReportVersion.inc')
  tmp:VersionNumber = 'Version Number: ' & Clip(tmp:VersionNumber) & '5006'
  ?ReportVersion{prop:Text} = tmp:VersionNumber
  tmp:ArchivePath = Clip(GETINI('ARCHIVE','ArchivePath',,Clip(Path()) & '\SB2KDEF.INI'))
  If Clip(tmp:ArchivePath) = ''
      Case Missive('The path to the "Archive" ServiceBase has not been set.'&|
        '||You must select this now.','ServiceBase 3g',|
                     'mexclam.jpg','/OK') 
          Of 1 ! OK Button
      End ! Case Missive
      If filedialog ('Choose Location Of "Archive" ServiceBase',tmp:ArchivePath,'All Directories|*.*', |
                  file:save+file:keepdir + file:noerror + file:longname + file:directory)
          PUTINI('ARCHIVE','ArchivePath',tmp:ArchivePath,Clip(Path()) & '\SB2KDEF.INI')
          Display()
      Else ! file:save+file:keepdir + file:noerror + file:longname + file:directory)
          Post(Event:CloseWindow)
      End ! file:save+file:keepdir + file:noerror + file:longname + file:directory)
  
  End ! If Clip(tmp:ArchivePath) = ''
  Do DisplayVersion
  tmp:EndDate = Today() - 730
  tmp:StartDate = Deformat('1/1/' & Year(tmp:EndDate),@d6)
  
  Bryan.CompFieldColour()
      omit('***',ClarionetUsed=0)
      If ~clarionetserver:Active()
      ***
      Channel = DDESERVER('ARCHIVE') !set program as a DDE Server
      omit('***',ClarionetUsed=0)
      End !If clarionetserver:Active()
      ***
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  FileLookup7.Init
  FileLookup7.Flags=BOR(FileLookup7.Flags,FILE:LongName)
  FileLookup7.Flags=BOR(FileLookup7.Flags,FILE:Directory)
  FileLookup7.SetMask('All Files','*.*')
  FileLookup7.WindowTitle='Select "Archive" ServiceBase Path'
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:SMSMAIL.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LookupFile
      tmp:ArchivePath = Upper(FileLookup7.Ask(1)  )
      DISPLAY
      PUTINI('ARCHIVE','ArchivePath',tmp:ArchivePath,Clip(Path()) & '\SB2KDEF.INI')
      Do DisplayVersion
    OF ?PopCalendar
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Button:BeginArchive
      If tmp:StartDate = 0 Or tmp:EndDate = 0 Or tmp:StartDate > tmp:EndDate Or tmp:EndDate > Today() - 730
          Case Missive('Invalid date range!','ServiceBase 3g',|
                         'mstop.jpg','/OK') 
              Of 1 ! OK Button
          End ! Case Missive
          Cycle
      End ! If tmp:StartDate = 0 Or tmp:EndDate = 0
      Case Missive('You have selected to Archive ALL jobs between: '&|
        '|' & Format(tmp:StartDate,@d6) & ' and ' &|
        '|' & Format(tmp:EndDate,@d6) & |
        '||Click ''OK'' to confirm this and begin.','ServiceBase 3g',|
                     'mexclam.jpg','\Cancel|/OK') 
          Of 2 ! OK Button
      
              LinePrint('Begin Archive. ' & Format(Today(),@d06) & ' ' & Format(Clock(),@t4),'ARCHIVE.LOG')
              LinePrint('"Live" Path: ' & Clip(Path()),'ARCHIVE.LOG')
              LinePrint('"Archive" Path: ' & Clip(tmp:ArchivePath),'ARCHIVE.LOG')
              LinePrint('Date Range: ' & Format(tmp:StartDate,@d6) & ' and ' & Format(tmp:EndDate,@d6),'ARCHIVE.LOG')
              file:JOBS       = Clip(tmp:ArchivePath) & '\JOBS.DAT'
              file:JOBSE2     = Clip(tmp:ArchivePath) & '\JOBSE2.DAT'
              file:JOBSE3     = Clip(tmp:ArchivePath) & '\JOBSE3.DAT'
              file:JOBSCONS   = Clip(tmp:ArchivePath) & '\JOBSCONS.DAT'
              file:JOBACCNO   = Clip(tmp:ArchivePath) & '\JOBACCNO.DAT'
              file:JOBOUTFL   = Clip(tmp:ArchivePath) & '\JOBOUTFL.DAT'
              file:WEBJOB     = Clip(tmp:ArchivePath) & '\WEBJOB.DAT'
              file:LOCATLOG   = Clip(tmp:ArchivePath) & '\LOCATLOG.DAT'
              file:JOBSENG    = Clip(tmp:ArchivePath) & '\JOBSENG.DAT'
              file:AUDSTATS   = Clip(tmp:ArchivePath) & '\AUDSTATS.DAT'
              file:JOBSE      = Clip(tmp:ArchivePath) & '\JOBSE.DAT'
              file:JOBTHIRD   = Clip(tmp:ArchivePath) & '\JOBTHIRD.DAT'
              file:CONTHIST   = Clip(tmp:ArchivePath) & '\CONTHIST.DAT'
              file:WARPARTS   = Clip(tmp:ArchivePath) & '\WARPARTS.DAT'
              file:AUDIT      = Clip(tmp:ArchivePath) & '\AUDIT.DAT'
              file:AUDIT2     = Clip(tmp:ArchivePath) & '\AUDIT2.DAT'
              file:ESTPARTS   = Clip(tmp:ArchivePath) & '\ESTPARTS.DAT'
              file:JOBSTAGE   = Clip(tmp:ArchivePath) & '\JOBSTAGE.DAT'
              file:JOBEXHIS   = Clip(tmp:ArchivePath) & '\JOBEXHIS.DAT'
              file:JOBLOHIS   = Clip(tmp:ArchivePath) & '\JOBLOHIS.DAT'
              file:PARTS      = Clip(tmp:ArchivePath) & '\PARTS.DAT'
              file:JOBACC     = Clip(tmp:ArchivePath) & '\JOBACC.DAT'
              file:JOBSOBF    = Clip(tmp:ArchivePath) & '\JOBSOBF.DAT'
              file:JOBSWARR   = Clip(tmp:ArchivePath) & '\JOBSWARR.DAT'
              file:JOBSTAMP   = Clip(tmp:ArchivePath) & '\JOBSTAMP.DAT'
              file:SMSMAIL    = Clip(tmp:ArchivePath) & '\SMSMAIL.DAT'
      
      
              If Relate:JOBS.Open()
                  Cycle
              End ! If Relate:JOBS.Open()
              If Relate:JOBSE2.Open()
                  Cycle
              End ! If Relate:JOBS2.Open()
              If Relate:JOBSE3.Open()
                  Cycle
              End ! If Relate:JOBS2.Open()
              If Relate:JOBSCONS.Open()
                  Cycle
              End ! If Relate:JOBSCONS.Open()
              If Relate:JOBACCNO.Open()
                  Cycle
              End ! If RElateJOBACCNO.Open()
              If Relate:JOBOUTFL.Open()
                  Cycle
              End ! If Relate:JOBOUTFL.Open()
              If Relate:WEBJOB.Open()
                  Cycle
              End ! If Relate:WEBJOB.Open()
              If Relate:LOCATLOG.Open()
                  Cycle
              End ! If Relate:LOCATLOG.Open()
              If Relate:JOBSENG.Open()
                  Cycle
              End ! If Relate:JOBSENG.Open()
              If Relate:AUDSTATS.Open()
                  Cycle
              End ! If Relate:AUDSTATS.Open()
              If Relate:JOBSE.Open()
                  Cycle
              End ! If Relate:JOBSE.Open()
              If Relate:JOBTHIRD.Open()
                  Cycle
              End ! If Relate:JOBTHIRD.Open()
              If Relate:CONTHIST.Open()
                  Cycle
              End ! If Relate:CONTHIST.Open()
              If Relate:WARPARTS.Open()
                  Cycle
              End ! If Relate:WARPARTS.Open()
              If Relate:AUDIT.Open()
                  Cycle
              End ! If Relate:AUDIT.Open()
              If Relate:AUDIT2.Open()
                  Cycle
              End ! If Relate:AUDIT.Open()
              If Relate:ESTPARTS.Open()
                  Cycle
              End ! If Relate:ESTPARTS.Open()
              If Relate:JOBSTAGE.Open()
                  Cycle
              End ! If Relate:JOBSAGE.Open()
              If Relate:JOBEXHIS.Open()
                  Cycle
              End ! If Relate:JOBEXHIS.Open()
              If Relate:JOBLOHIS.Open()
                  Cycle
              End ! If Relate:JOBLOHIS.Open()
              If Relate:PARTS.Open()
                  Cycle
              End ! If Relate:PARTS.Open()
              If Relate:JOBACC.Open()
                  Cycle
              End ! If Relate:JOBACC.Open()
              If Relate:JOBSOBF.Open()
                  Cycle
              End ! If Relate:JOBSOBF.Open()
              If Relate:JOBSWARR.Open()
                  Cycle
              End ! If Relate:JOBSWARR.Open()
              If Relate:JOBSTAMP.Open()
                  Cycle
              End ! If Relate:JOBSTAMP.Open()
              If Relate:SMSMAIL.Open()
                  Cycle
              End ! If Relate:SMSMail.Open()
      !===========================================================
              If Relate:ARCJOBS.Open()
                  Cycle
              End ! If Relate:ARCJOBS.Open()
      
              If Relate:ARCJOBSE2.Open()
                  Cycle
              End ! If Relate:ARCJOBS2.Open()
              If Relate:ARCJOBSE3.Open()
                  Cycle
              End ! If Relate:ARCJOBS3.Open()
      
              If Relate:ARCJOBSCONS.Open()
                  Cycle
              End ! If Relate:ARCJOBSCONS.Open()
      
              If Relate:ARCJOBACCNO.Open()
                  Cycle
              End ! If RElateJOBACCNO.Open()
      
              If Relate:ARCJOBOUTFL.Open()
                  Cycle
              End ! If Relate:ARCJOBOUTFL.Open()
              If Relate:ARCWEBJOB.Open()
                  Cycle
              End ! If Relate:ARCWEBJOB.Open()
              If Relate:ARCLOCATLOG.Open()
                  Cycle
              End ! If Relate:ARCLOCATLOG.Open()
              If Relate:ARCJOBSENG.Open()
                  Cycle
              End ! If Relate:ARCJOBSENG.Open()
              If Relate:ARCAUDSTATS.Open()
                  Cycle
              End ! If Relate:ARCAUDSTATS.Open()
              If Relate:ARCJOBSE.Open()
                  Cycle
              End ! If Relate:ARCJOBSE.Open()
              If Relate:ARCJOBTHIRD.Open()
                  Cycle
              End ! If Relate:ARCJOBTHIRD.Open()
              If Relate:ARCCONTHIST.Open()
                  Cycle
              End ! If Relate:ARCCONTHIST.Open()
              If Relate:ARCWARPARTS.Open()
                  Cycle
              End ! If Relate:ARCWARPARTS.Open()
              If Relate:ARCAUDIT.Open()
                  Cycle
              End ! If Relate:ARCAUDIT.Open()
              If Relate:ARCAUDIT2.Open()
                  Cycle
              End ! If Relate:AUDIT.Open()
              If Relate:ARCESTPARTS.Open()
                  Cycle
              End ! If Relate:ARCESTPARTS.Open()
              If Relate:ARCJOBSTAGE.Open()
                  Cycle
              End ! If Relate:ARCJOBSAGE.Open()
              If Relate:ARCJOBEXHIS.Open()
                  Cycle
              End ! If Relate:ARCJOBEXHIS.Open()
              If Relate:ARCJOBLOHIS.Open()
                  Cycle
              End ! If Relate:ARCJOBLOHIS.Open()
              If Relate:ARCPARTS.Open()
                  Cycle
              End ! If Relate:ARCPARTS.Open()
              If Relate:ARCJOBACC.Open()
                  Cycle
              End ! If Relate:ARCJOBACC.Open()
              If Relate:ARCJOBSOBF.Open()
                  Cycle
              End ! If Relate:ARCJOBSOBF.Open()
              If Relate:ARCJOBSWARR.Open()
                  Cycle
              End ! If Relate:ARCJOBSWARR.Open()
              If Relate:ARCJOBSTAMP.Open()
                  Cycle
              End ! If Relate:JOBSTAMP.Open()
              If Relate:ARCSMSMAIL.Open()
                  Cycle
              End ! If Relate:JOBSTAMP.Open()
      
      
              CountJobs# = 0
              SetCursor(Cursor:Wait)
              Access:JOBS.Clearkey(job:Date_Booked_Key)
              job:Date_Booked = tmp:StartDate
              Set(job:Date_Booked_Key,job:Date_Booked_Key)
              Loop ! Begin Loop
                  If Access:JOBS.Next()
                      Break
                  End ! If Access:JOBS.Next()
                  If job:Date_Booked > tmp:EndDate
                      Break
                  End ! If job:Date_Booked > tmp:EndDate
                  CountJobs# += 1
              End ! Loop
      
              SetCursor()
      
              Prog.ProgressSetup(CountJobs#)
      
              arc# = 0
              Access:JOBS.Clearkey(job:Date_Booked_Key)
              job:Date_Booked = tmp:StartDate
              Set(job:Date_Booked_Key,job:Date_Booked_Key)
              Loop ! Begin Loop
                  If Access:JOBS.Next()
                      Break
                  End ! If Access:JOBS.Next()
                  If job:Date_Booked > tmp:EndDate
                      Break
                  End ! If job:Date_Booked > tmp:EndDate
                  If Prog.InsideLoop()
                      LinePrint('Archive Cancelled By User','ARCHIVE.LOG')
                      Break
                  End ! If Prog.InsideLoop()
      
      ! Deleting (DBH 26/09/2008) # 10262 - All jobs are archived
      !            If job:Date_Completed = ''
      !                ! Double Check (DBH: 24/11/2006)
      !                Cycle               
      !            End ! If job:Date_Completed = ''
      ! End (DBH 26/09/2008) #10262
      
                  Access:ARCJOBS.ClearKey(ajob:Ref_Number_Key)
                  ajob:Ref_Number = job:Ref_Number
                  If Access:ARCJOBS.TryFetch(ajob:Ref_Number_Key) = Level:Benign
                      !Found
                      tmp:AlreadyExists = True
                      
                      ajob:Record :=: job:Record
                      If Access:ARCJOBS.TryUpdate()
                          ! Couldn't update the job. Don't carry on (DBH: 27/11/2006)
                          Cycle
                      End ! If Access:ARCJOBS.Update()
                  Else ! If Access:ARCJOBS.TryFetch(ajob:Ref_Number_Key) = Level:Benign
                      !Error
                      tmp:AlreadyExists = False
                      
                      If Access:ARCJOBS.PrimeRecord() = Level:Benign
                          ajob:Record :=: job:Record
                          If Access:ARCJOBS.TryInsert() = Level:Benign
                              !Insert
                          Else ! If Access:AJOB.TryInsert() = Level:Benign
                              Cycle
                              Access:ARCJOBS.CancelAutoInc()
                          End ! If Access:AJOB.TryInsert() = Level:Benign
                      Else !  ! If Access.AJOB.PrimeRecord() = Level:Benign
                          Cycle
                      End ! If Access.AJOB.PrimeRecord() = Level:Benign
                      
                  End ! If Access:ARCJOBS.TryFetch(ajob:Ref_Number_Key) = Level:Benign
                  
                  Access:AUDIT.Clearkey(aud:Ref_Number_Key)
                  aud:Ref_Number = job:Ref_Number
                  Set(aud:Ref_Number_Key,aud:Ref_Number_Key)
                  Loop ! Begin Loop
                      If Access:AUDIT.Next()
                          Break
                      End ! If Access:AUDIT.Next()
                      If aud:Ref_Number <> job:Ref_Number
                          Break
                      End ! If aud:Ref_Number <> job:Ref_Number
                      
                      Access:ARCAUDIT.ClearKey(aaud:Record_Number_Key)
                      aaud:Record_Number = aud:Record_Number
                      If Access:ARCAUDIT.TryFetch(aaud:Record_Number_Key) = Level:Benign
                          !Found
                          aaud:Record :=: aud:Record
                          If Access:ARCAUDIT.TryUpdate() = Level:Benign
                              Delete(AUDIT)
                          Else ! If Access:ARCAUDIT.Update() = Level:Benign
                              Cycle
                          End ! If Access:ARCAUDIT.Update() = Level:Benign
                      Else ! If Access:ARCAUDIT.TryFetch(aaud:Record_Number_Key) = Level:Benign
                          !Error
                          If Access:ARCAUDIT.PrimeRecord() = Level:Benign
                              aaud:Record :=: aud:Record
                              If Access:ARCAUDIT.TryInsert() = Level:Benign
                                  !Insert
                                  Delete(AUDIT)
                              Else ! If Access:ARCAUDIT.TryInsert() = Level:Benign
                                  Access:ARCAUDIT.CancelAutoInc()
                              End ! If Access:ARCAUDIT.TryInsert() = Level:Benign
                          End ! If Access.ARCAUDIT.PrimeRecord() = Level:Benign
                      End ! If Access:ARCAUDIT.TryFetch(aaud:Record_Number_Key) = Level:Benign
      
                      Access:AUDIT2.Clearkey(aud2:AUDRecordNumberKey)
                      aud2:AUDRecordNumber = aud:Record_Number
                      IF (Access:AUDIT2.TryFetch(aud2:AUDRecordNumberKey) = Level:Benign)
      
                          Access:ARCAUDIT2.Clearkey(aaud2:RecordNumberKey)
                          aaud2:RecordNumber = aud2:RecordNumber
                          IF (Access:AUDIT2.TryFetch(aaud2:RecordNumberKey) = Level:Benign)
                              aaud2:Record :=: aud2:Record
                              IF (Access:ARCAUDIT2.TryUpdate() = Level:Benign)
                                  DELETE(AUDIT2)
                              END ! IF
                          ELSE ! IF
                              If Access:ARCAUDIT2.PrimeRecord() = Level:Benign
                                  aaud2:Record :=: aud:Record
                                  If Access:ARCAUDIT2.TryInsert() = Level:Benign
                                      !Insert
                                      Delete(AUDIT2)
                                  Else ! If Access:ARCAUDIT.TryInsert() = Level:Benign
                                      Access:ARCAUDIT2.CancelAutoInc()
                                  End ! If Access:ARCAUDIT.TryInsert() = Level:Benign
                              End ! If Access.ARCAUDIT.PrimeRecord() = Level:Benign
                          END ! IF
                      END ! IF
      
                  End ! Loop
      
                  Access:AUDSTATS.Clearkey(aus:DateChangedKey)
                  aus:RefNumber = job:Ref_Number
                  Set(aus:DateChangedKey,aus:DateChangedKey)
                  Loop ! Begin Loop
                      If Access:AUDSTATS.Next()
                          Break
                      End ! If Access:AUDSTATS.Next()
                      If aus:RefNumber <> job:Ref_Number
                          Break
                      End ! If aus:RefNumber <> job:Ref_Number
                      
                      Access:ARCAUDSTATS.ClearKey(aaus:RecordNumberKey)
                      aaus:RecordNumber = aus:RecordNumber
                      If Access:ARCAUDSTATS.TryFetch(aaus:RecordNumberKey) = Level:Benign
                          !Found
                          aaus:Record :=: aus:Record
                          If Access:ARCAUDSTATS.TryUpdate() = Level:Benign
                              Delete(AUDSTATS)
                          Else ! If Access:ARCAUDSTATS.TryUpdate() = Level:Benign
                              Cycle
                          End ! If Access:ARCAUDSTATS.TryUpdate() = Level:Benign
                      Else ! If Access:ARCAUDSTATS.TryFetch(aaus:RecordNumberKey) = Level:Benign
                          !Error
                          If Access:ARCAUDSTATS.PrimeRecord() = Level:Benign
                              aaus:Record :=: aus:Record
                              If Access:ARCAUDSTATS.TryInsert() = Level:Benign
                                  !Insert
                                  Delete(AUDSTATS)
                              Else ! If Access:ARCAUDSTATS.TryInsert() = Level:Benign
                                  Access:ARCAUDSTATS.CancelAutoInc()
                              End ! If Access:ARCAUDSTATS.TryInsert() = Level:Benign
                          End ! If Access.ARCAUDSTATS.PrimeRecord() = Level:Benign
                      End ! If Access:ARCAUDSTATS.TryFetch(aaus:RecordNumberKey) = Level:Benign
                  End ! Loop
      
                  Access:CONTHIST.Clearkey(cht:Ref_Number_Key)
                  cht:Ref_Number = job:Ref_Number
                  Set(cht:Ref_Number_Key,cht:Ref_Number_Key)
                  Loop ! Begin Loop
                      If Access:CONTHIST.Next()
                          Break
                      End ! If Access:CONTHIST.Next()
                      If cht:Ref_Number <> job:Ref_Number
                          Break
                      End ! If cht:Ref_Number <> job:Ref_Number
                      
                      Access:ARCCONTHIST.ClearKey(acht:Record_Number_Key)
                      acht:Record_Number = cht:Record_Number
                      If Access:ARCCONTHIST.TryFetch(acht:Record_Number_Key) = Level:Benign
                          !Found
                          acht:Record :=: cht:Record
                          If Access:ARCCONTHIST.TryUpdate() = Level:Benign
                              Delete(CONTHIST)
                          Else ! If Access:ARCCONTHIST.TryUpdate() = Level:Benign
                              Cycle
                          End ! If Access:ARCCONTHIST.TryUpdate() = Level:Benign
                      Else ! If Access:ARCCONTHIST.TryFetch(acht:Record_Number_Key) = Level:Benign
                          !Error
                          If Access:ARCCONTHIST.PrimeRecord() = Level:Benign
                              acht:Record :=: cht:Record
                              If Access:ARCCONTHIST.TryInsert() = Level:Benign
                                  !Insert
                                  Delete(CONTHIST)
                              Else ! If Access:ARCCONTHIST.TryInsert() = Level:Benign
                                  Access:ARCCONTHIST.CancelAutoInc()
                              End ! If Access:ARCCONTHIST.TryInsert() = Level:Benign
                          End ! If Access.ARCCONTHIST.PrimeRecord() = Level:Benign                                        
                      End ! If Access:ARCCONTHIST.TryFetch(acht:Record_Number_Key) = Level:Benign
                  End ! Loop
                                  
                  Access:ESTPARTS.Clearkey(epr:Part_Number_Key)
                  epr:Ref_Number = job:Ref_Number
                  Set(epr:Part_Number_Key,epr:Part_Number_Key)
                  Loop ! Begin Loop
                      If Access:ESTPARTS.Next()
                          Break
                      End ! If Access:ESTPARTS.Next()                                    
                      If epr:Ref_Number <> job:Ref_Number
                          Break
                      End ! If est:Ref_Number <> job:Ref_Number
                      
                      Access:ARCESTPARTS.ClearKey(aepr:Record_Number_Key)
                      aepr:Record_Number = epr:Record_Number
                      If Access:ARCESTPARTS.TryFetch(aepr:Record_Number_Key) = Level:Benign
                          !Found
                          aepr:Record :=: epr:Record
                          If Access:ARCESTPARTS.TryUpdate() = Level:Benign
                              Delete(ESTPARTS)
                          Else ! If Access:ARCESTPARTS.TryUpdate() = Level:Benign
                              Cycle
                          End ! If Access:ARCESTPARTS.TryUpdate() = Level:Benign
                      Else ! If Access:ARCESTPARTS.TryFetch(aepr:Record_Number_Key) = Level:Benign
                          !Error
                          If Access:ARCESTPARTS.PrimeRecord() = Level:Benign
                              aepr:Record :=: epr:Record
                              If Access:ARCESTPARTS.TryInsert() = Level:Benign
                                  !Insert
                                  Delete(ESTPARTS)
                              Else ! If Access:ARCESTPARTS.TryInsert() = Level:Benign
                                  Access:ARCESTPARTS.CancelAutoInc()
                              End ! If Access:ARCESTPARTS.TryInsert() = Level:Benign
                          End ! If Access.ARCESTPARTS.PrimeRecord() = Level:Benign            
                      End ! If Access:ARCESTPARTS.TryFetch(aepr:Record_Number_Key) = Level:Benign
                  End ! Loop
                              
                  Access:JOBACC.Clearkey(jac:Ref_Number_Key)
                  jac:Ref_Number = job:Ref_Number
                  Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
                  Loop ! Begin Loop
                      If Access:JOBACC.Next()
                          Break
                      End ! If Access:JOBACC.Next()                                
                      If jac:Ref_Number <> job:Ref_Number
                          Break
                      End ! If jac:Ref_Number <> job:Ref_Number
                      
                      Access:ARCJOBACC.ClearKey(ajac:Ref_Number_Key)
                      ajac:Ref_Number = jac:Ref_number
                      ajac:Accessory = jac:Accessory
                      If Access:ARCJOBACC.TryFetch(ajac:Ref_Number_Key) = Level:Benign
                          !Found
                          ajac:Record :=: jac:Record
                          If Access:ARCJOBACC.TryUpdate() = Level:Benign
                              ! OK
                              Delete(JOBACC)
                          Else ! If Access:ARCJOBACC.TryUpdate() = Level:Benign
                              ! Error
                              Cycle
                          End ! If Access:ARCJOBACC.TryUpdate() = Level:Benign
                         
                      Else ! If Access:ARCJOBACC.TryFetch(ajac:Ref_Number_Key) = Level:Benign
                          !Error
                          If Access:ARCJOBACC.PrimeRecord() = Level:Benign
                              ajac:Record :=: jac:Record
                              If Access:ARCJOBACC.TryInsert() = Level:Benign
                                  !Insert
                                  Delete(JOBACC)
                              Else ! If Access:ARCJOBACC.TryInsert() = Level:Benign
                                  Access:ARCJOBACC.CancelAutoInc()
                              End ! If Access:ARCJOBACC.TryInsert() = Level:Benign
                          End ! If Access.ARCJOBACC.PrimeRecord() = Level:Benign            
                         
                      End ! If Access:ARCJOBACC.TryFetch(ajac:Ref_Number_Key) = Level:Benign
                      
                  End ! Loop
                                  
                  Access:JOBACCNO.Clearkey(joa:AccessoryNumberKey)
                  joa:RefNumber = job:Ref_Number                        
                  Set(joa:AccessoryNumberKey,joa:AccessoryNumberKey)
                  Loop ! Begin Loop
                      If Access:JOBACCNO.Next()
                          Break
                      End ! If Access:JOBACCNO.Next()    
                      If joa:RefNumber <> job:Ref_Number
                          Break
                      End ! If joa:Ref_Number <> job:Ref_Number
                      
                      Access:ARCJOBACCNO.ClearKey(ajoa:RecordNumberKey)
                      ajoa:RecordNumber = joa:RecordNumber
                      If Access:ARCJOBACCNO.TryFetch(ajoa:RecordNumberKey) = Level:Benign
                          !Found
                          ajoa:Record :=: joa:Record
                          If Access:ARCJOBACCNO.TryUpdate() = Level:Benign
                              ! OK
                              Delete(JOBACCNO)
                          Else ! If Access:ARCJOBACCNO.TryUpdate() = Level:Benign
                              ! Error
                              Cycle
                          End ! If Access:ARCJOBACCNO.TryUpdate() = Level:Benign
                      Else ! If Access:ARCJOBACCNO.TryFetch(ajoa:RecordNumberKey) = Level:Benign
                          !Error
                          If Access:ARCJOBACCNO.PrimeRecord() = Level:Benign
                              ajoa:Record :=: joa:Record
                              If Access:ARCJOBACCNO.TryInsert() = Level:Benign
                                  !Insert
                                  Delete(JOBACCNO)
                              Else ! If Access:ARCJOBACCNO.TryInsert() = Level:Benign
                                  Access:ARCJOBACCNO.CancelAutoInc()
                              End ! If Access:ARCJOBACCNO.TryInsert() = Level:Benign
                          End ! If Access.ARCJOBACCNO.PrimeRecord() = Level:Benign                                        
                      End ! If Access:ARCJOBACCNO.TryFetch(ajoa:RecordNumberKey) = Level:Benign
                  End ! Loop
                              
                  Access:JOBEXHIS.Clearkey(jxh:Ref_Number_Key)
                  jxh:Ref_Number = job:Ref_Number                                
                  Set(jxh:Ref_Number_Key,jxh:Ref_Number_Key)
                  Loop ! Begin Loop
                      If Access:JOBEXHIS.Next()
                          Break
                      End ! If Access:JOBEXHIS.Next()                            
                      If jxh:Ref_Number <> job:Ref_Number
                          Break
                      End ! If jxh:Ref_Number <> job:Ref_Number
                      
                      Access:ARCJOBEXHIS.ClearKey(ajxh:Record_Number_Key)
                      ajxh:Record_Number = jxh:Record_Number
                      If Access:ARCJOBEXHIS.TryFetch(ajxh:Record_Number_Key) = Level:Benign
                          !Found
                          ajxh:Record :=: jxh:Record
                          If Access:ARCJOBEXHIS.TryUpdate() = Level:Benign
                              ! OK
                              Delete(ARCJOBEXHIS)
                          Else ! If Access:ARCJOBEXHIS.TryUpdate() = Level:Benign
                              ! Error
                              Cycle
                          End ! If Access:ARCJOBEXHIS.TryUpdate() = Level:Benign
                      Else ! If Access:ARCJOBEXHIS.TryFetch(ajxh:Record_Number_Key) = Level:Benign
                          !Error
                          If Access:ARCJOBEXHIS.PrimeRecord() = Level:Benign
                              ajxh:Record :=: jxh:Record
                              If Access:ARCJOBEXHIS.TryInsert() = Level:Benign
                                  !Insert
                                  Delete(JOBEXHIS)
                              Else ! If Access:ARCJOBEXHIS.TryInsert() = Level:Benign
                                  Access:ARCJOBEXHIS.CancelAutoInc()
                              End ! If Access:ARCJOBEXHIS.TryInsert() = Level:Benign
                          End ! If Access.ARCJOBEXHIS.PrimeRecord() = Level:Benign                                        
                      End ! If Access:ARCJOBEXHIS.TryFetch(ajxh:Record_Number_Key) = Level:Benign
                  End ! Loop
                              
                  Access:JOBLOHIS.Clearkey(jlh:Ref_Number_Key)
                  jlh:Ref_Number = job:Ref_Number
                  Set(jlh:Ref_Number_Key,jlh:Ref_Number_Key)
                  Loop ! Begin Loop
                      If Access:JOBLOHIS.Next()
                          Break
                      End ! If Access:JOBLOHIS.Next()
                      If jlh:Ref_Number <> job:Ref_Number
                          Break
                      End ! If jlh:Ref_Number <> job:Ref_Number
                      
                      Access:ARCJOBLOHIS.ClearKey(ajlh:Record_Number_Key)
                      ajlh:Record_Number = jlh:Record_Number
                      If Access:ARCJOBLOHIS.TryFetch(ajlh:Record_Number_Key) = Level:Benign
                          !Found
                          ajlh:Record :=: jlh:Record
                          If Access:ARCJOBLOHIS.TryUpdate() = Level:Benign
                              ! OK
                              Delete(JOBLOHIS)
                          Else ! If Access:ARCJOBLOHI.TryUpdate() = Level:Benign
                              ! Error
                              Cycle
                          End ! If Access:ARCJOBLOHI.TryUpdate() = Level:Benign
                      Else ! If Access:ARCJOBLOHIS.TryFetch(ajlh:Record_Number_Key) = Level:Benign
                          !Error
                          If Access:ARCJOBLOHIS.PrimeRecord() = Level:Benign
                              ajlh:Record :=: jlh:Record
                              If Access:ARCJOBLOHIS.TryInsert() = Level:Benign
                                  !Insert
                                  Delete(JOBLOHIS)
                              Else ! If Access:ARCJOBLOHIS.TryInsert() = Level:Benign
                                  Access:ARCJOBLOHIS.CancelAutoInc()
                              End ! If Access:ARCJOBLOHIS.TryInsert() = Level:Benign
                          End ! If Access.ARCJOBLOHIS.PrimeRecord() = Level:Benign            
                      End ! If Access:ARCJOBLOHIS.TryFetch(ajlh:Record_Number_Key) = Level:Benign
                  End ! Loop
                                  
                  Access:JOBOUTFL.Clearkey(joo:JobNumberKey)
                  joo:JobNumber = job:Ref_Number
                  Set(joo:JobNumberKey,joo:JobNumberKey)
                  Loop ! Begin Loop
                      If Access:JOBOUTFL.Next()
                          Break
                      End ! If Access:JOBOUTFL.Next()                            
                      If joo:JobNumber <> job:Ref_Number
                          Break
                      End ! If joo:JobNumber <> job:Ref_Number
                      
                      Access:ARCJOBOUTFL.ClearKey(ajoo:RecordNumberKey)
                      ajoo:RecordNumber = joo:RecordNumber
                      If Access:ARCJOBOUTFL.TryFetch(ajoo:RecordNumberKey) = Level:Benign
                          !Found
                          ajoo:Record :=: joo:Record
                          If Access:ARCJOBOUTFL.TryUpdate() = Level:Benign
                              ! OK
                              Delete(JOBOUTFL)
                          Else ! If Access:ARCJOBOUTFL.TryUpdate() = Level:Benign
                              ! Error
                              Cycle
                          End ! If Access:ARCJOBOUTFL.TryUpdate() = Level:Benign
                      Else ! If Access:ARCJOBOUTFL.TryFetch(ajoo:RecordNumberKey) = Level:Benign
                          !Error
                          If Access:ARCJOBOUTFL.PrimeRecord() = Level:Benign
                              ajoo:Record :=: joo:Record
                              If Access:ARCJOBOUTFL.TryInsert() = Level:Benign
                                  !Insert
                                  Delete(JOBOUTFL)
                              Else ! If Access:ARCJOBOUTFL.TryInsert() = Level:Benign
                                  Access:ARCJOBOUTFL.CancelAutoInc()
                              End ! If Access:ARCJOBOUTFL.TryInsert() = Level:Benign
                          End ! If Access.ARCJOBOUTFL.PrimeRecord() = Level:Benign                                    
                      End ! If Access:ARCJOBOUTFL.TryFetch(ajoo:RecordNumberKey) = Level:Benign
                  End ! Loop
                                  
                  Access:JOBSCONS.Clearkey(joc:DateKey)
                  joc:RefNumber = job:Ref_Number                        
                  Set(joc:DateKey,joc:DateKey)
                  Loop ! Begin Loop
                      If Access:JOBSCONS.Next()
                          Break
                      End ! If Access:JOBSCONS.Next()    
                      If joc:RefNumber <> job:Ref_Number
                          Break
                      End ! If joc:RefNumber <> job:Ref_Number
              
                      Access:ARCJOBSCONS.ClearKey(ajoc:RecordNumberKey)
                      ajoc:RecordNumber = joc:RecordNumber
                      If Access:ARCJOBSCONS.TryFetch(ajoc:RecordNumberKey) = Level:Benign
                          !Found
                          ajoc:Record :=: joc:Record
                          If Access:ARCJOBSCONS.TryUpdate() = Level:Benign
                              ! OK
                              Delete(ARCJOBSCONS)
                          Else ! If Access:ARCJOBSCONS.TryUpdate() = Level:Benign
                              ! Error
                              Cycle
                          End ! If Access:ARCJOBSCONS.TryUpdate() = Level:Benign
                      Else ! If Access:ARCJOBSCONS.TryFetch(ajoc:RecordNumberKey) = Level:Benign
                          !Error
                          If Access:ARCJOBSCONS.PrimeRecord() = Level:Benign
                              ajoc:Record :=: joc:Record
                              If Access:ARCJOBSCONS.TryInsert() = Level:Benign
                                  !Insert
                                  Delete(JOBSCONS)
                              Else ! If Access:ARCJOBSCONS.TryInsert() = Level:Benign
                                  Access:ARCJOBSCONS.CancelAutoInc()
                              End ! If Access:ARCJOBSCONS.TryInsert() = Level:Benign
                          End ! If Access.ARCJOBSCONS.PrimeRecord() = Level:Benign            
                      End ! If Access:ARCJOBSCONS.TryFetch(ajoc:RecordNumberKey) = Level:Benign                
                  End ! Loop
                                  
                  Access:JOBSE.ClearKey(jobe:RefNumberKey)
                  jobe:RefNumber = job:Ref_Number
                  If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                      !Found
                      Access:ARCJOBSE.ClearKey(ajobe:RecordNumberKey)
                      ajobe:RecordNumber = jobe:RecordNumber
                      If Access:ARCJOBSE.TryFetch(ajobe:RecordNumberKey) = Level:Benign
                          !Found
                          ajobe:Record :=: jobe:Record
                          If Access:ARCJOBSE.TryUpdate() = Level:Benign
                              ! OK
                              Delete(JOBSE)
                          Else ! If Access:ARCJOBSE.TryUpdate() = Level:Benign
                              ! Error
                          End ! If Access:ARCJOBSE.TryUpdate() = Level:Benign
                      Else ! If Access:ARCJOBE.TryFetch(ajobe:RecordNumberKey) = Level:Benign
                          !Error
                          If Access:ARCJOBSE.PrimeRecord() = Level:Benign
                              ajobe:Record :=: jobe:Record
                              If Access:ARCJOBSE.TryInsert() = Level:Benign
                                  !Insert
                                  Delete(JOBSE)
                              Else ! If Access:ARCJOBSE.TryInsert() = Level:Benign
                                  Access:ARCJOBSE.CancelAutoInc()
                              End ! If Access:ARCJOBSE.TryInsert() = Level:Benign
                          End ! If Access.ARCJOBSE.PrimeRecord() = Level:Benign            
                      End ! If Access:ARCJOBE.TryFetch(ajobe:RecordNumberKey) = Level:Benign
                  Else ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
                      !Error
                  End ! If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign    
                              
                  Access:JOBSE2.ClearKey(jobe2:RefNumberKey)
                  jobe2:RefNumber = job:Ref_Number
                  If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
                      !Found
                      Access:ARCJOBSE2.ClearKey(ajobe2:RecordNumberKey)
                      ajobe2:RecordNumber = jobe2:RecordNumber
                      If Access:ARCJOBSE2.TryFetch(ajobe2:RecordNumberKey) = Level:Benign
                          !Found
                          ajobe2:Record :=: jobe2:Record
                          If Access:ARCJOBSE2.TryUpdate() = Level:Benign
                              ! OK
                              Delete(JOBSE2)
                          Else ! If Access:ARCJOBSE2.TryUpdate() = Level:Benign
                              ! Error
                          End ! If Access:ARCJOBSE2.TryUpdate() = Level:Benign
                      Else ! If Access:ARCJOBSE2.TryFetch(ajobe2:RecordNumberKey) = Level:Benign
                          !Error
                          If Access:ARCJOBSE2.PrimeRecord() = Level:Benign
                              ajobe2:Record :=: jobe2:Record
                              If Access:ARCJOBSE2.TryInsert() = Level:Benign
                                  !Insert
                                  Delete(JOBSE2)
                              Else ! If Access:ARCJOBSE2.TryInsert() = Level:Benign
                                  Access:ARCJOBSE2.CancelAutoInc()
                              End ! If Access:ARCJOBSE2.TryInsert() = Level:Benign
                          End ! If Access.ARCJOBSE2.PrimeRecord() = Level:Benign            
                          
                      End ! If Access:ARCJOBSE2.TryFetch(ajobe2:RecordNumberKey) = Level:Benign
                  Else ! If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
                      !Error
                  End ! If Access:JOBSE2.TryFetch(jobe2:RefNumberKey) = Level:Benign
      
      
                  Access:JOBSE3.ClearKey(jobe3:KeyRefNumber)
                  jobe3:RefNumber = job:Ref_Number
                  If Access:JOBSE3.TryFetch(jobe3:KeyRefNumber) = Level:Benign
                      !Found
                      Access:ARCJOBSE3.ClearKey(ajobe3:KeyRecordNumber)
                      ajobe3:RecordNumber = jobe3:RecordNumber
                      If Access:ARCJOBSE3.TryFetch(ajobe3:KeyRecordNumber) = Level:Benign
                          !Found
                          ajobe3:Record :=: jobe3:Record
                          If Access:ARCJOBSE3.TryUpdate() = Level:Benign
                              ! OK
                              Delete(JOBSE3)
                          Else ! If Access:ARCJOBSE3.TryUpdate() = Level:Benign
                              ! Error
                          End ! If Access:ARCJOBSE3.TryUpdate() = Level:Benign
                      Else ! If Access:ARCJOBSE3.TryFetch(ajobe3:RecordNumberKey) = Level:Benign
                          !Error
                          If Access:ARCJOBSE3.PrimeRecord() = Level:Benign
                              ajobe3:Record :=: jobe3:Record
                              If Access:ARCJOBSE3.TryInsert() = Level:Benign
                                  !Insert
                                  Delete(JOBSE3)
                              Else ! If Access:ARCJOBSE2.TryInsert() = Level:Benign
                                  Access:ARCJOBSE3.CancelAutoInc()
                              End ! If Access:ARCJOBSE2.TryInsert() = Level:Benign
                          End ! If Access.ARCJOBSE2.PrimeRecord() = Level:Benign            
                          
                      End ! If Access:ARCJOBSE3.TryFetch(ajobe3:RecordNumberKey) = Level:Benign
                  Else ! If Access:JOBSE3.TryFetch(jobe3:RefNumberKey) = Level:Benign
                      !Error
                  End ! If Access:JOBSE3.TryFetch(jobe3:RefNumberKey) = Level:Benign
      
      
                  Access:JOBSTAMP.ClearKey(jos:JOBSRefNumberKey)
                  jos:JOBSRefNumber = job:Ref_Number
                  If Access:JOBSTAMP.TryFetch(jos:JOBSRefNumberKey) = Level:Benign
                      !Found
                      Access:ARCJOBSTAMP.ClearKey(ajos:RecordNumberKey)
                      ajos:RecordNumber = jos:RecordNumber
                      If Access:ARCJOBSTAMP.TryFetch(ajos:RecordNumberKey) = Level:Benign
                          !Found
                          ajos:Record :=: jos:Record
                          If Access:ARCJOBSTAMP.TryUpdate() = Level:Benign
                              ! OK
                              Delete(JOBSTAMP)
                          Else ! If Access:ARCJOBSTAMP.TryUpdate() = Level:Benign
                              ! Error
                          End ! If Access:ARCJOBSTAMP.TryUpdate() = Level:Benign
                      Else ! If Access:ARCJOBSTAMP.TryFetch(ajos:RecordNumberKey) = Level:Benign
                          !Error
                          If Access:ARCJOBSTAMP.PrimeRecord() = Level:Benign
                              ajos:Record :=: jos:Record
                              If Access:ARCJOBSTAMP.TryInsert() = Level:Benign
                                  !Insert
                                  Delete(JOBSTAMP)
                              Else ! If Access:ARCJOBSTAMP.TryInsert() = Level:Benign
                                  Access:ARCJOBSTAMP.CancelAutoInc()
                              End ! If Access:ARCJOBSTAMP.TryInsert() = Level:Benign
                          End ! If Access.ARCJOBSTAMP.PrimeRecord() = Level:Benign
      
                      End ! If Access:ARCJOBSTAMP.TryFetch(ajos:RecordNumberKey) = Level:Benign
                  Else ! If Access:JOBSTAMP.TryFetch(jos:RefNumberKey) = Level:Benign
                      !Error
                  End ! If Access:JOBSTAMP.TryFetch(jos:RefNumberKey) = Level:Benign
                              
                  Access:JOBSENG.Clearkey(joe:UserCodeKey)
                  joe:JobNumber = job:Ref_Number                        
                  Set(joe:UserCodeKey,joe:UserCodeKey)
                  Loop ! Begin Loop
                      If Access:JOBSENG.Next()
                          Break
                      End ! If Access:JOBSENG.Next()                                
                      If joe:JobNumber <> job:Ref_Number
                          Break
                      End ! If joe:JobNumber <> job:Ref_Number
                      
                      Access:ARCJOBSENG.ClearKey(ajoe:RecordNumberKey)
                      ajoe:RecordNumber = joe:RecordNumber
                      If Access:ARCJOBSENG.TryFetch(ajoe:RecordNumberKey) = Level:Benign
                          !Found
                          ajoe:Record :=: joe:Record
                          If Access:ARCJOBSENG.TryUpdate() = Level:Benign
                              ! OK
                              Delete(JOBSENG)
                          Else ! If Access:ARCJOBSENG.TryUpdate() = Level:Benign
                              ! Error
                              Cycle
                          End ! If Access:ARCJOBSENG.TryUpdate() = Level:Benign
                      Else ! If Access:ARCJOBSENG.TryFetch(ajoe:RecordNumberKey) = Level:Benign
                          !Error
                          If Access:ARCJOBSENG.PrimeRecord() = Level:Benign
                              ajoe:Record :=: joe:Record
                              If Access:ARCJOBSENG.TryInsert() = Level:Benign
                                  !Insert
                                  Delete(JOBSENG)
                              Else ! If Access:ARCJOBSENG.TryInsert() = Level:Benign
                                  Access:ARCJOBSENG.CancelAutoInc()
                              End ! If Access:ARCJOBSENG.TryInsert() = Level:Benign
                          End ! If Access.ARCJOBSENG.PrimeRecord() = Level:Benign            
                      End ! If Access:ARCJOBSENG.TryFetch(ajoe:RecordNumberKey) = Level:Benign
                  End ! Loop
                              
                  Access:JOBSTAGE.Clearkey(jst:Ref_Number_Key)
                  jst:Ref_Number = job:Ref_Number                        
                  Set(jst:Ref_Number_Key,jst:Ref_Number_Key)
                  Loop ! Begin Loop
                      If Access:JOBSTAGE.Next()
                          Break
                      End ! If Access:JOBSTAGE.Next()
                      If jst:Ref_Number <> job:Ref_Number
                          Break
                      End ! If jst:Ref_Number <> job:Ref_Number
                      
                      Access:ARCJOBSTAGE.ClearKey(ajst:Job_Stage_Key)
                      ajst:Ref_Number = jst:Ref_Number
                      ajst:Job_Stage = jst:Job_Stage
                      If Access:ARCJOBSTAGE.TryFetch(ajst:Job_Stage_Key) = Level:Benign
                          !Found
                          ajst:Record :=: jst:Record
                          If Access:ARCJOBSTAGE.TryUpdate() = Level:Benign
                              ! OK
                              Delete(JOBSTAGE)
                          Else ! If Access:ARCJOBSTAGE.TryUpdate() = Level:Benign
                              ! Error
                              Cycle
                          End ! If Access:ARCJOBSTAGE.TryUpdate() = Level:Benign
                      Else ! If Access:ARCJOBSTAGE.TryFetch(ajst:Job_Stage_Key) = Level:Benign
                          !Error
                          If Access:ARCJOBSTAGE.PrimeRecord() = Level:Benign
                              ajst:Record :=: jst:Record
                              If Access:ARCJOBSTAGE.TryInsert() = Level:Benign
                                  !Insert
                                  Delete(JOBSTAGE)
                              Else ! If Access:ARCJOBSTAGE.TryInsert() = Level:Benign
                                  Access:ARCJOBSTAGE.CancelAutoInc()
                              End ! If Access:ARCJOBSTAGE.TryInsert() = Level:Benign
                          End ! If Access.ARCJOBSTAGE.PrimeRecord() = Level:Benign            
                      End ! If Access:ARCJOBSTAGE.TryFetch(ajst:Job_Stage_Key) = Level:Benign
                  End ! Loop
                                  
                  Access:JOBTHIRD.Clearkey(jot:RefNumberKey)
                  jot:RefNumber = job:Ref_Number
                  Set(jot:RefNumberKey,jot:RefNumberKey)
                  Loop ! Begin Loop
                      If Access:JOBTHIRD.Next()
                          Break
                      End ! If Access:JOBTHIRD.Next()            
                      If jot:RefNumber <> job:Ref_Number
                          Break
                      End ! If jot:Ref_Number <> job:Ref_Number
                      
                      Access:ARCJOBTHIRD.ClearKey(ajot:RecordNumberKey)
                      ajot:RecordNumber = jot:RecordNumber
                      If Access:ARCJOBTHIRD.TryFetch(ajot:RecordNumberKey) = Level:Benign
                          !Found
                          ajot:Record :=: jot:Record
                          If Access:ARCJOBTHIRD.TryUpdate() = Level:Benign
                              ! OK
                              Delete(JOBTHIRD)
                          Else ! If Access:ARCJOBTHIRD.TryUpdate() = Level:Benign
                              ! Error
                              Cycle
                          End ! If Access:ARCJOBTHIRD.TryUpdate() = Level:Benign
                      Else ! If Access:ARCJOBTHIRD.TryFetch(ajot:RecordNumberKey) = Level:Benign
                          !Error
                          If Access:ARCJOBTHIRD.PrimeRecord() = Level:Benign
                              ajot:Record :=: jot:Record
                              If Access:ARCJOBTHIRD.TryInsert() = Level:Benign
                                  !Insert
                                  Delete(JOBTHIRD)
                              Else ! If Access:ARCJOBTHIRD.TryInsert() = Level:Benign
                                  Access:ARCJOBTHIRD.CancelAutoInc()
                              End ! If Access:ARCJOBTHIRD.TryInsert() = Level:Benign
                          End ! If Access.ARCJOBTHIRD.PrimeRecord() = Level:Benign            
                      End ! If Access:ARCJOBTHIRD.TryFetch(ajot:RecordNumberKey) = Level:Benign
                  End ! Loop
                              
                  Access:LOCATLOG.Clearkey(lot:DateKey)
                  lot:RefNumber = job:Ref_Number
                  Set(lot:DateKey,lot:DateKey)
                  Loop ! Begin Loop
                      If Access:LOCATLOG.Next()
                          Break
                      End ! If Access:LOCATLOG.Next()
                      If lot:RefNumber <> job:Ref_Number
                          Break
                      End ! If lot:Ref_Number <> job:Ref_Number
                      
                      Access:ARCLOCATLOG.ClearKey(alot:RecordNumberKey)
                      alot:RecordNumber = lot:RecordNumber
                      If Access:ARCLOCATLOG.TryFetch(alot:RecordNumberKey) = Level:Benign
                          !Found
                          alot:Record :=: lot:Record
                          If Access:ARCLOCATLOG.TryUpdate() = Level:Benign
                              ! OK
                              Delete(LOCATLOG)
                          Else ! If Access:ARCLOCATLOG.TryUpdate() = Level:Benign
                              ! Error
                              Cycle
                          End ! If Access:ARCLOCATLOG.TryUpdate() = Level:Benign
                      Else ! If Access:ARCLOCATLOG.TryFetch(alot:RecordNumberKey) = Level:Benign
                          !Error
                          If Access:ARCLOCATLOG.PrimeRecord() = Level:Benign
                              alot:RefNumber = job:Ref_Number
                              If Access:ARCLOCATLOG.TryInsert() = Level:Benign
                                  !Insert
                                  Delete(LOCATLOG)
                              Else ! If Access:ARCLOCATLOG.TryInsert() = Level:Benign
                                  Access:ARCLOCATLOG.CancelAutoInc()
                              End ! If Access:ARCLOCATLOG.TryInsert() = Level:Benign
                          End ! If Access.ARCLOCATLOG.PrimeRecord() = Level:Benign            
                      End ! If Access:ARCLOCATLOG.TryFetch(alot:RecordNumberKey) = Level:Benign
                  End ! Loop
                                  
                  Access:PARTS.Clearkey(par:Part_Number_Key)
                  par:Ref_Number = job:Ref_Number
                  Set(par:Part_Number_Key,par:Part_Number_Key)
                  Loop ! Begin Loop
                      If Access:PARTS.Next()
                          Break
                      End ! If Access:PARTS.Next()    
                      If par:Ref_Number <> job:Ref_Number
                          Break
                      End ! If par:Ref_Number <> job:Ref_Number
                      
                      Access:ARCPARTS.ClearKey(apar:RecordNumberKey)
                      apar:Record_Number = par:Record_Number
                      If Access:ARCPARTS.TryFetch(apar:RecordNumberKey) = Level:Benign
                          !Found
                          apar:Record :=: par:Record
                          If Access:ARCPARTS.TryUPdate() = Level:Benign
                              ! OK
                              Delete(PARTS)
                          Else ! If Access:ARCPARTS.TryUPdate() = Level:Benign
                              ! Error
                              Cycle
                          End ! If Access:ARCPARTS.TryUPdate() = Level:Benign
                      Else ! If Access:ARCPARTS.TryFetch(apar:RecordNumberKey) = Level:Benign
                          !Error
                          If Access:ARCPARTS.PrimeRecord() = Level:Benign
                              apar:Record :=: par:Record
                              If Access:ARCPARTS.TryInsert() = Level:Benign
                                  !Insert
                                  Delete(PARTS)
                              Else ! If Access:ARCPARTS.TryInsert() = Level:Benign
                                  Access:ARCPARTS.CancelAutoInc()
                              End ! If Access:ARCPARTS.TryInsert() = Level:Benign
                          End ! If Access.ARCPARTS.PrimeRecord() = Level:Benign            
                      End ! If Access:ARCPARTS.TryFetch(apar:RecordNumberKey) = Level:Benign
                  End ! Loop
                              
                  Access:WARPARTS.Clearkey(wpr:Part_Number_Key)
                  wpr:Ref_Number = job:Ref_Number
                  Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                  Loop ! Begin Loop
                      If Access:WARPARTS.Next()
                          Break
                      End ! If Access:WARPARTS.Next()
                      If wpr:Ref_Number <> job:Ref_Number
                          Break
                      End ! If wpr:Ref_Number <> job:Ref_Number
                      
                      Access:ARCWARPARTS.ClearKey(awpr:RecordNumberKEy)
                      awpr:Record_Number = wpr:Record_Number
                      If Access:ARCWARPARTS.TryFetch(awpr:RecordNumberKEy) = Level:Benign
                          !Found
                          awpr:Record :=: wpr:Record
                          If Access:ARCWARPARTS.TryUpdate() = Level:Benign
                              ! OK
                              Delete(WARPARTS)
                          Else ! If Access:ARCWARPARTS.TryUpdate() = Level:Benign
                              ! Error
                              Cycle
                          End ! If Access:ARCWARPARTS.TryUpdate() = Level:Benign
                      Else ! If Access:ARCWARPARTS.TryFetch(awpr:RecordNumberKEy) = Level:Benign
                          !Error
                          If Access:ARCWARPARTS.PrimeRecord() = Level:Benign
                              awpr:Record :=: wpr:Record
                              If Access:ARCWARPARTS.TryInsert() = Level:Benign
                                  !Insert
                                  Delete(WARPARTS)
                              Else ! If Access:ARCWARPARTS.TryInsert() = Level:Benign
                                  Access:ARCWARPARTS.CancelAutoInc()
                              End ! If Access:ARCWARPARTS.TryInsert() = Level:Benign
                          End ! If Access.ARCWARPARTS.PrimeRecord() = Level:Benign            
                      End ! If Access:ARCWARPARTS.TryFetch(awpr:RecordNumberKEy) = Level:Benign
                  End ! Loop
                                  
                  Access:WEBJOB.ClearKey(wob:RefNumberKey)
                  wob:RefNumber = job:Ref_Number
                  If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                      !Found
                      Access:ARCWEBJOB.ClearKey(awob:RecordNumberKey)
                      awob:RecordNumber = wob:RecordNumber
                      If Access:ARCWEBJOB.TryFetch(awob:RecordNumberKey) = Level:Benign
                          !Found
                          awob:Record :=: wob:Record
                          If Access:ARCWEBJOB.TryUpdate() = Level:Benign
                              ! OK
                              Delete(WEBJOB)
                          Else ! If Access:ARCWEBJOB.TryUpdate() = Level:Benign
                              ! Error
                          End ! If Access:ARCWEBJOB.TryUpdate() = Level:Benign
                      Else ! If Access:ARCWEBJOB.TryFetch(awob:RecordNumberKey) = Level:Benign
                          !Error
                          If Access:ARCWEBJOB.PrimeRecord() = Level:Benign
                              awob:Record :=: wob:Record
                              If Access:ARCWEBJOB.TryInsert() = Level:Benign
                                  !Insert
                                  Delete(WEBJOB)
                              Else ! If Access:ARCWEBJOB.TryInsert() = Level:Benign
                                  Access:ARCWEBJOB.CancelAutoInc()
                              End ! If Access:ARCWEBJOB.TryInsert() = Level:Benign
                          End ! If Access.ARCWEBJOB.PrimeRecord() = Level:Benign            
                      End ! If Access:ARCWEBJOB.TryFetch(awob:RecordNumberKey) = Level:Benign
                  Else ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
                      !Error
                  End ! If Access:WEBJOB.TryFetch(wob:RefNumberKey) = Level:Benign
      
                  Access:JOBSOBF.Clearkey(jof:RefNumberKey)
                  jof:RefNumber = job:Ref_Number
                  Set(jof:RefNumberKey,jof:RefNumberKey)
                  Loop ! Begin Loop
                      If Access:JOBSOBF.Next()
                          Break
                      End ! If Access:JOBSENG.Next()                                
                      If jof:RefNumber <> job:Ref_Number
                          Break
                      End ! If joe:JobNumber <> job:Ref_Number
                      
                      Access:ARCJOBSOBF.ClearKey(ajof:RecordNumberKey)
                      ajof:RecordNumber = jof:RecordNumber
                      If Access:ARCJOBSOBF.TryFetch(ajof:RecordNumberKey) = Level:Benign
                          !Found
                          ajof:Record :=: jof:Record
                          If Access:ARCJOBSOBF.TryUpdate() = Level:Benign
                              ! OK
                              Delete(JOBSOBF)
                          Else ! If Access:ARCJOBSENG.TryUpdate() = Level:Benign
                              ! Error
                              Cycle
                          End ! If Access:ARCJOBSENG.TryUpdate() = Level:Benign
                      Else ! If Access:ARCJOBSENG.TryFetch(ajoe:RecordNumberKey) = Level:Benign
                          !Error
                          If Access:ARCJOBSOBF.PrimeRecord() = Level:Benign
                              ajof:Record :=: jof:Record
                              If Access:ARCJOBSOBF.TryInsert() = Level:Benign
                                  !Insert
                                  Delete(JOBSOBF)
                              Else ! If Access:ARCJOBSENG.TryInsert() = Level:Benign
                                  Access:ARCJOBSOBF.CancelAutoInc()
                              End ! If Access:ARCJOBSENG.TryInsert() = Level:Benign
                          End ! If Access.ARCJOBSENG.PrimeRecord() = Level:Benign            
                      End ! If Access:ARCJOBSENG.TryFetch(ajoe:RecordNumberKey) = Level:Benign
                  End ! Loop
      
                  Access:JOBSWARR.ClearKey(jow:RefNumberKey)
                  jow:RefNumber = job:Ref_Number
                  If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
                      !Found
                      Access:ARCJOBSWARR.ClearKey(ajow:RecordNumberKey)
                      ajow:RecordNumber = jow:RecordNumber
                      If Access:ARCJOBSWARR.TryFetch(ajow:RecordNumberKey) = Level:Benign
                          !Found
                          ajow:Record :=: jow:Record
                          If Access:ARCJOBSWARR.TryUpdate() = Level:Benign
                              Delete(JOBSWARR)
                          End ! If Access:ARCJOBSWARR.TryUpdate() = Level:Benign
                      Else ! If Access:ARCJOBSWARR.TryFetch(ajow:RecordNumberKey) = Level:Benign
                          !Error
                          If Access:ARCJOBSWARR.PrimeRecord() = Level:Benign
                              ajow:Record :=: jow:Record
                              If Access:ARCJOBSWARR.TryInsert() = Level:Benign
                                  !Insert
                                  Delete(JOBSWARR)
                              Else ! If Access:ARCJOBSWARR.TryInsert() = Level:Benign
                                  Access:ARCJOBSWARR.CancelAutoInc()
                              End ! If Access:ARCJOBSWARR.TryInsert() = Level:Benign
                          End ! If Access.ARCJOBSWARR.PrimeRecord() = Level:Benign
                      End ! If Access:ARCJOBSWARR.TryFetch(ajow:RecordNumberKey) = Level:Benign
                  Else ! If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
                      !Error
                  End ! If Access:JOBSWARR.TryFetch(jow:RefNumberKey) = Level:Benign
      
      
                  Access:SMSMAIL.Clearkey(sms:DateTimeInsertedKey)
                  SMS:REfNumber = job:Ref_Number
                  Set(sms:DateTimeInsertedKey,sms:DateTimeInsertedKey)
                  Loop ! Begin Loop
                      If Access:SMSMAIL.Next()
                          Break
                      End ! If Access:WARPARTS.Next()
                      If  SMS:RefNumber <> job:Ref_Number
                          Break
                      End ! If wpr:Ref_Number <> job:Ref_Number
                      
                      Access:ARCSMSMAIL.ClearKey(arcsms:RecordNumberKEy)
                      arcsms:RecordNumber = sms:RecordNumber
                      If Access:ARCSMSMAIL.TryFetch(arcsms:RecordNumberKEy) = Level:Benign
                          !Found
                          arcsms:Record :=: sms:Record
                          If Access:ARCSMSMAIL.TryUpdate() = Level:Benign
                              ! OK
                              Delete(SMSMAIL)
                          Else ! If Access:ARCWARPARTS.TryUpdate() = Level:Benign
                              ! Error
                              Cycle
                          End ! If Access:ARCWARPARTS.TryUpdate() = Level:Benign
                      Else ! If Access:ARCWARPARTS.TryFetch(awpr:RecordNumberKEy) = Level:Benign
                          !Error
                          If Access:ARCSMSMAIL.PrimeRecord() = Level:Benign
                              arcsms:Record :=: sms:Record
                              If Access:ARCSMSMAIL.TryInsert() = Level:Benign
                                  !Insert
                                  Delete(SMSMAIL)
                              Else ! If Access:ARCWARPARTS.TryInsert() = Level:Benign
                                  Access:ARCSMSMAIL.CancelAutoInc()
                              End ! If Access:ARCWARPARTS.TryInsert() = Level:Benign
                          End ! If Access.ARCWARPARTS.PrimeRecord() = Level:Benign            
                      End ! If Access:ARCWARPARTS.TryFetch(awpr:RecordNumberKEy) = Level:Benign
                  End ! Loop
      
      
                  Delete(JOBS)
                  If tmp:AlreadyExists = True
                      LinePrint('Job No: ' & job:Ref_Number & ' Updated & Deleted.','ARCHIVE.LOG')    
                  Else ! If tmp:AlreadyExists = True
                      LinePrint('Job No: ' & job:Ref_Number & ' Deleted.','ARCHIVE.LOG')
                  End ! If tmp:AlreadyExists = True
                  
                  
                  arc# += 1
      
                  Prog.ProgressText('Archiving: ' & arc#)
              End ! Loop
      
              Prog.ProgressFinish()
      
      
              Relate:JOBS.Close()
              Relate:JOBSE2.Close()
              Relate:JOBSCONS.Close()
              Relate:JOBACCNO.Close()
              Relate:JOBOUTFL.Close()
              Relate:WEBJOB.Close()
              Relate:LOCATLOG.Close
              Relate:JOBSENG.CLose()
              Relate:AUDSTATS.Close()
              Relate:JOBSE.Close()
              Relate:JOBTHIRD.Close
              Relate:CONTHIST.Close()
              Relate:WARPARTS.Close()
              Relate:AUDIT.Close()
              Relate:ESTPARTS.Close()
              Relate:JOBSTAGE.Close()
              Relate:JOBEXHIS.CLose()
              Relate:JOBLOHIS.Close()
              Relate:PARTS.Close()
              Relate:JOBACC.Close()
              Relate:JOBSOBF.Close()
              Relate:JOBSWARR.Close()
              Relate:JOBSTAMP.Close()
              Relate:SMSMAIL.Close()
      
              Relate:ARCJOBS.Close()
              Relate:ARCJOBSE2.Close()
              Relate:ARCJOBSCONS.Close()
              Relate:ARCJOBACCNO.Close()
              Relate:ARCJOBOUTFL.Close()
              Relate:ARCWEBJOB.Close()
              Relate:ARCLOCATLOG.Close
              Relate:ARCJOBSENG.CLose()
              Relate:ARCAUDSTATS.Close()
              Relate:ARCJOBSE.Close()
              Relate:ARCJOBTHIRD.Close
              Relate:ARCCONTHIST.Close()
              Relate:ARCWARPARTS.Close()
              Relate:ARCAUDIT.Close()
              Relate:ARCESTPARTS.Close()
              Relate:ARCJOBSTAGE.Close()
              Relate:ARCJOBEXHIS.CLose()
              Relate:ARCJOBLOHIS.Close()
              Relate:ARCPARTS.Close()
              Relate:ARCJOBACC.Close()
              Relate:ARCJOBSOBF.Close()
              Relate:ARCJOBSWARR.Close()
              Relate:ARCJOBSTAMP.Close()
              Relate:ARCSMSMAIL.Close()
      
              LinePrint('Jobs Archived: ' & arc#,'ARCHIVE.LOG')
              LinePrint('Archive Finished. ' & Format(Today(),@d06) & ' ' & Format(Clock(),@t4),'ARCHIVE.LOG')
              LinePrint('=========================================','ARCHIVE.LOG')
      
              Case Missive(arc# & ' Job(s) Archived.','ServiceBase 3g',|
                             'mexclam.jpg','/OK') 
                  Of 1 ! OK Button
              End ! Case Missive
              Post(Event:CloseWindow)
          Of 1 ! Cancel Button
              Cycle
      End ! Case Missive
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      omit('***',ClarionetUsed=0)
      If ~clarionetserver:Active()
      ***
      IF EVENT() = Event:DDEexecute
          IF DDEVALUE() = 'INFRONT' !tells it to bring itself to the front
              AppFrame{PROP:Iconize} = FALSE
              BringWindowToTop(AppFrame{PROP:Handle}) !and this does it
          END
      END
      omit('***',ClarionetUsed=0)
      End !If clarionetserver:Active()
      ***
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()
Prog.Init                 PROCEDURE(LONG func:Records)
    CODE
        Prog.ProgressSetup(func:Records)
Prog.ProgressSetup        PROCEDURE(Long func:Records)  ! Template Type: Frame
windowXpos  Long()
windowYpos Long()
windowWidth Long()
windowHeight Long()
CODE

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        windowXpos = 0{prop:Xpos}
        windowYpos = 0{prop:Ypos}
        windowWidth = 0{prop:Width}
        windowHeight = 0{prop:Height}

        Open(Prog:ProgressWindow)
        0{prop:Xpos} = windowXpos + (windowWidth / 2) - 105
        0{prop:Ypos} = windowYpos + (windowHeight / 2) - 30
        Prog.ResetProgress(func:Records)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
CODE

    Prog.recordsToProcess = func:Records
    Prog.recordsprocessed = 0
    Prog.percentProgress = 0
    Prog.progressThermometer = 0
    Prog.CNprogressThermometer = 0
    Prog.skipRecords = 0
    Prog.userText = ''
    Prog.CNuserText = ''
    Prog.percentText = '0% Completed'
    Prog.CNpercentText = Prog.percentText


Prog.Update      Procedure(<String func:String>)
    CODE
        RETURN (Prog.InsideLoop(func:String))
Prog.InsideLoop     Procedure(<String func:String>)
CODE

    if (func:String <> '')
        Prog.UserText = Clip(func:String)
        Prog.CNUserText = Prog.UserText
    end !if (func:String <> '')

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Display()
        Prog.NextRecord()
        if (Prog.CancelLoop())
            return 1
        end ! if (Prog.CancelPressed())
        Return 0
Compile('***',ClarionetUsed = 1)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
CODE

    Prog.UserText = Clip(func:String)
    Prog.CNUserText = Prog.UserText
Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:UpdatePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        Display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.Kill     Procedure()
    CODE
        Prog.ProgressFinish()
Prog.ProgressFinish     Procedure()
CODE

    Prog.ProgressThermometer = 100
    Prog.CNProgressThermometer = 100
    Prog.PercentText = '100% Completed'
    Prog.CNPercentText = '100% Completed'

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        display()
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Compile('***',ClarionetUsed = 1)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(Prog:CNProgressWindow)
    Else ! If ClarionetServer:Active()
***
        close(Prog:ProgressWindow)
Compile('***',ClarionetUsed = 1)
    End ! If ClarionetServer:Active()
***

Prog.NextRecord      Procedure()
CODE
    Yield()
    Prog.RecordsProcessed += 1
    !If Prog.percentprogress < 100
        Prog.percentprogress = (Prog.recordsprocessed / Prog.recordstoprocess)*100
        If Prog.percentprogress > 100 or Prog.percentProgress < 0
            Prog.percentprogress = 0
        End
        If Prog.percentprogress <> Prog.ProgressThermometer then
            Prog.ProgressThermometer = Prog.percentprogress
            Prog.PercentText = format(Prog:percentprogress,@n3) & '% Completed'
        End
    !End
    Prog.CNPercentText = Prog.PercentText
    Prog.CNProgressThermometer = Prog.ProgressThermometer
    Display()

Prog.cancelLoop         Procedure()
    Code
    cancel# = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:CancelButton
                    cancel# = 1
                    Break
                End ! If Field() = ?Button1
        End ! Case Event()
    end ! accept
    If cancel# = 1
        Case Missive('Are you sure you want to cancel?','Progress...','MQuest.jpg','/Yes|\No')
            Of 1  !/Yes
                return 1
            Of 2  !\No
        End !Case Missive
    End!If cancel# = 1

    return 0
