

   MEMBER('vodr0037.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('VODR0002.INC'),ONCE        !Local module procedure declarations
                     END


PartsUsageExport PROCEDURE                            !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::6:TAGFLAG          BYTE(0)
DASBRW::6:TAGMOUSE         BYTE(0)
DASBRW::6:TAGDISPSTATUS    BYTE(0)
DASBRW::6:QUEUE           QUEUE
Pointer                       LIKE(glo:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
tmp:VersionNumber    STRING(30)
LocationTag          STRING(1)
Parameter_Group      GROUP,PRE(param)
StartDate            DATE
EndDate              DATE
                     END
LocalTag             STRING(1)
LocalTimeOut         LONG
DoAll                STRING(1)
GUIMode              BYTE
Count                LONG
Number_Of_Days       REAL
Days_To_Search       REAL
save_res_id          USHORT,AUTO
save_tra_id          USHORT,AUTO
save_par_id          USHORT,AUTO
save_wpr_id          USHORT,AUTO
save_orp_id          USHORT,AUTO
save_shi_id          USHORT,AUTO
save_ord_id          USHORT,AUTO
save_ret_id          USHORT,AUTO
tmp:StoresAccount    STRING(30)
Automatic            BYTE(0)
BRW1::View:Browse    VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
LocationQueue:Browse QUEUE                            !Queue declaration for browse/combo box using ?LocationList
LocationTag            LIKE(LocationTag)              !List box control field - type derived from local data
LocationTag_Icon       LONG                           !Entry's icon ID
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
MainWindow           WINDOW('Parts Usage Export'),AT(0,0,680,428),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),COLOR(0D6EAEFH),WALLPAPER('sbback.jpg'),TILED,GRAY,DOUBLE
                       PANEL,AT(160,64,360,300),USE(?PanelMain),FILL(0D6EAEFH)
                       PANEL,AT(164,68,352,12),USE(?PanelFalse),FILL(09A6A7CH)
                       PROMPT('SRN:0000000'),AT(464,70),USE(?SRNNumber),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PROMPT('Parts Usage Report Criteria'),AT(168,70),USE(?WindowTitle),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI)
                       PANEL,AT(164,332,352,28),USE(?PanelButton),FILL(09A6A7CH)
                       SHEET,AT(164,82,352,248),USE(?Sheet1),FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH),WIZARD,SPREAD
                         TAB('Criteria'),USE(?Tab1)
                           PROMPT('Number of Days'),AT(272,87),USE(?Number_Of_Days:Prompt),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n10.b),AT(348,87,,10),USE(Number_Of_Days),DECIMAL(14),FONT(,,010101H,FONT:bold),COLOR(COLOR:White),REQ
                           PROMPT('Sales History'),AT(272,102),USE(?Days_To_Search:Prompt),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           ENTRY(@n10b),AT(348,102,,10),USE(Days_To_Search),HIDE,DECIMAL(14),FONT(,,,FONT:bold),COLOR(COLOR:White)
                           STRING('(days to look back over)'),AT(412,103),USE(?String6),HIDE,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                           LIST,AT(261,132,159,192),USE(?LocationList),IMM,VSCROLL,LEFT,FONT(,,010101H,,CHARSET:ANSI),COLOR(COLOR:White,COLOR:White,09A6A7CH),MSG('Browsing Records'),FORMAT('11L(2)I@s1@200L(2)~Location~@s30@'),FROM(LocationQueue:Browse)
                           BUTTON,AT(428,238),USE(?LocationDASTAG),IMM,TRN,FLAT,LEFT,ICON('tagitemp.jpg')
                           BUTTON,AT(428,268),USE(?LocationDASTAGAll),IMM,TRN,FLAT,LEFT,ICON('tagallp.jpg')
                           BUTTON,AT(428,298),USE(?LocationDASUNTAGALL),IMM,TRN,FLAT,LEFT,ICON('untagalp.jpg')
                           BUTTON('&Rev tags'),AT(180,168,56,16),USE(?LocationDASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(196,196,56,16),USE(?LocationDASSHOWTAG),HIDE
                         END
                         TAB('Repair Categories'),USE(?Tab2)
                         END
                       END
                       STRING('End Date '),AT(320,188,44,12),USE(?String5),HIDE,RIGHT,FONT(,,,FONT:bold)
                       STRING('Start Date '),AT(320,204,44,12),USE(?String4),HIDE,RIGHT,FONT(,,,FONT:bold)
                       ENTRY(@D08B),AT(320,204,64,10),USE(param:StartDate),IMM,HIDE,FONT('Arial',8,,FONT:bold),TIP('Enter the earliest exchange date to show on report'),REQ
                       BUTTON('...'),AT(392,202,10,10),USE(?PopCalendar),IMM,FLAT,HIDE,LEFT,TIP('Click to show Calendar<13,10>'),ICON('CALENDA2.ICO')
                       ENTRY(@D08B),AT(320,220,64,10),USE(param:EndDate),IMM,HIDE,FONT('Arial',8,,FONT:bold),TIP('Enter the latest exchange date to show on report'),REQ
                       BUTTON('...'),AT(388,220,10,10),USE(?PopCalendarEnd),IMM,FLAT,HIDE,LEFT,TIP('Click to show Calendar'),ICON('CALENDA2.ICO')
                       BUTTON,AT(380,332),USE(?OkButton),TRN,FLAT,LEFT,TIP('Click to print report'),ICON('printp.jpg'),DEFAULT
                       BUTTON,AT(448,332),USE(?CancelButton),TRN,FLAT,LEFT,TIP('Click to close this form'),ICON('cancelp.jpg')
                       PROMPT('Report Version'),AT(168,340),USE(?ReportVersion),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(09A6A7CH)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ex                   Class(oiExcel)
Init                   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0),byte,proc,virtual,name('INIT@F17OIEXCEL')
Kill                   PROCEDURE (byte pUnloadCOM=1),byte,proc,virtual,name('KILL@F17OIEXCEL')
TakeEvent              PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0),virtual,name('TAKEEVENT@F17OIEXCEL')
                     End

BRWLocation          CLASS(BrowseClass)               !Browse using ?LocationList
Q                      &LocationQueue:Browse          !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
Bryan       Class
CompFieldColour       Procedure()
            End

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::6:DASTAGONOFF Routine
  GET(LocationQueue:Browse,CHOICE(?LocationList))
  BRWLocation.UpdateBuffer
   glo:Queue.Pointer = loc:Location
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = loc:Location
     ADD(glo:Queue,glo:Queue.Pointer)
    LocationTag = '*'
  ELSE
    DELETE(glo:Queue)
    LocationTag = ''
  END
    LocationQueue:Browse.LocationTag = LocationTag
  IF (locationtag = '*')
    LocationQueue:Browse.LocationTag_Icon = 2
  ELSE
    LocationQueue:Browse.LocationTag_Icon = 1
  END
  PUT(LocationQueue:Browse)
  SELECT(?LocationList,CHOICE(?LocationList))
DASBRW::6:DASTAGALL Routine
  ?LocationList{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRWLocation.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = loc:Location
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRWLocation.ResetSort(1)
  SELECT(?LocationList,CHOICE(?LocationList))
DASBRW::6:DASUNTAGALL Routine
  ?LocationList{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRWLocation.Reset
  SETCURSOR
  BRWLocation.ResetSort(1)
  SELECT(?LocationList,CHOICE(?LocationList))
DASBRW::6:DASREVTAGALL Routine
  ?LocationList{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::6:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::6:QUEUE = glo:Queue
    ADD(DASBRW::6:QUEUE)
  END
  FREE(glo:Queue)
  BRWLocation.Reset
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::6:QUEUE.Pointer = loc:Location
     GET(DASBRW::6:QUEUE,DASBRW::6:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = loc:Location
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRWLocation.ResetSort(1)
  SELECT(?LocationList,CHOICE(?LocationList))
DASBRW::6:DASSHOWTAG Routine
   CASE DASBRW::6:TAGDISPSTATUS
   OF 0
      DASBRW::6:TAGDISPSTATUS = 1    ! display tagged
      ?LocationDASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?LocationDASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?LocationDASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::6:TAGDISPSTATUS = 2    ! display untagged
      ?LocationDASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?LocationDASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?LocationDASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::6:TAGDISPSTATUS = 0    ! display all
      ?LocationDASSHOWTAG{PROP:Text} = 'Show All'
      ?LocationDASSHOWTAG{PROP:Msg}  = 'Show All'
      ?LocationDASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?LocationDASSHOWTAG{PROP:Text})
   BRWLocation.ResetSort(1)
   SELECT(?LocationList,CHOICE(?LocationList))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  Alias(0070H,0078H) !Alias f1 to f9 where the help is
  omit('***',ClarionetUsed=0)
  If Glo:WebJob = 1 then
       0{Prop:text}='ServiceBase 3g'
  ELSE
       0{prop:Text}='ServiceBase 3g'
  END
  ***
  omit('***',ClarionetUsed=1)
  0{prop:Text}='ServiceBase 3g'
  ***
  !Fetch The Top Tip
          ?SRNNumber{Prop:Text}= 'SRN:020604'&'0'
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('PartsUsageExport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PanelMain
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:JOBS.Open
  Relate:LOCATION_ALIAS.Open
  Relate:ORDSTOCK.Open
  Relate:REPSCHCR.Open
  Relate:RETSALES.Open
  Relate:STOCK_ALIAS.Open
  Relate:TRADEACC_ALIAS.Open
  Access:USERS.UseFile
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  Access:ORDPARTS.UseFile
  Access:ORDERS.UseFile
  Access:ORDPEND.UseFile
  Access:PARTS.UseFile
  Access:WARPARTS.UseFile
  Access:RETSTOCK.UseFile
  Access:TRADEACC.UseFile
  Access:REPSCHED.UseFile
  Access:REPSCHLC.UseFile
  Access:REPSCHLG.UseFile
  SELF.FilesOpened = True
  BRWLocation.Init(?LocationList,LocationQueue:Browse.ViewPosition,BRW1::View:Browse,LocationQueue:Browse,Relate:LOCATION,SELF)
  OPEN(MainWindow)
  SELF.Opened=True
  ! ========= Set Report Version =============
  Include('..\ReportVersion.inc')
  tmp:VersionNumber = 'Version Number: ' & Clip(tmp:VersionNumber) & '5003'
  ?ReportVersion{prop:Text} = tmp:VersionNumber
      param:EndDate   = TODAY()
      param:StartDate = DATE(MONTH(param:EndDate), 1, YEAR(param:EndDate))
      Number_Of_Days  = param:EndDate - param:StartDate
  
      !TB13266 - J - 03/04/14
      !with the changes made this should now run up to today from the date Number_of_days before
      !End date cannot be changed any more - removed from window - so as the call to report is (StartDate, no days)
      !keep end date and Number_of_days and calculate start date at the calling point
  
      SET(defaults)
      access:defaults.next()
  
  ! message('COMMAND()="' & CLIP(COMMAND()) & '"')
  
      !DO GetUserName
      IF GUIMode = 1 THEN
         LocalTimeOut             = 500
         MainWindow{PROP:ICONIZE} = TRUE
         DoAll = 'Y'
      END !IF
  
      !Check to see if the report has been run with one of the auto switches  (DBH: 07-05-2004)
      If Command('/DAILY') Or Command('/WEEKLY') Or Command('/MONTHLY')
          !LOC:UserName = 'AUTOMATED PROCEDURE'
          Automatic = True
      End !If Command('/DAILY') Or Command('/WEEKLY') Or Command('/MONTHLY')
  ?LocationList{prop:vcr} = TRUE
  Bryan.CompFieldColour()
  ?param:StartDate{Prop:Alrt,255} = MouseLeft2
  ?param:EndDate{Prop:Alrt,255} = MouseLeft2
      Alert(0473H) !Alt F4
      Alert(001BH) !Esc
  BRWLocation.Q &= LocationQueue:Browse
  BRWLocation.AddSortOrder(,loc:Location_Key)
  BRWLocation.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,loc:Location,1,BRWLocation)
  BIND('LocationTag',LocationTag)
  ?LocationList{PROP:IconList,1} = '~notick1.ico'
  ?LocationList{PROP:IconList,2} = '~tick1.ico'
  BRWLocation.AddField(LocationTag,BRWLocation.Q.LocationTag)
  BRWLocation.AddField(loc:Location,BRWLocation.Q.loc:Location)
  BRWLocation.AddField(loc:RecordNumber,BRWLocation.Q.loc:RecordNumber)
      !Set the report to automatic run  (DBH: 07-05-2004)
  
      IF Automatic = TRUE
  
          Number_Of_Days = 14
  
          
          !TB13266 - J - 03/04/14 Run the report for the last Number_Of_Days
          param:StartDate = today() - Number_Of_Days
          PartsUsageReport(param:StartDate,Number_Of_Days)
  
          POST(Event:CloseWindow)
      END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?LocationDASSHOWTAG{PROP:Text} = 'Show All'
  ?LocationDASSHOWTAG{PROP:Msg}  = 'Show All'
  ?LocationDASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?LocationList{Prop:Alrt,239} = SpaceKey
  ! Inserting (DBH 31/08/2007) # 9125 - Is this an automatic report?
  If Command('/SCHEDULE')
      x# = Instring('%',Command(),1,1)
      Access:REPSCHED.ClearKey(rpd:RecordNumberKey)
      rpd:RecordNumber = Clip(Sub(Command(),x# + 1,20))
      If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
          !Found
          Access:REPSCHCR.ClearKey(rpc:ReportCriteriaKey)
          rpc:ReportName = rpd:ReportName
          rpc:ReportCriteriaType = rpd:ReportCriteriaType
          If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
              !Found
              If rpc:AllLocations
                  DO DASBRW::6:DASTAGALL
              Else ! If rpc:AllLocations
                  Access:REPSCHLC.Clearkey(rpl:LocationKey)
                  rpl:REPSCHCRRecordNumber = rpc:RecordNumber
                  Set(rpl:LocationKey,rpl:LocationKey)
                  Loop ! Begin Loop
                      If Access:REPSCHLC.Next()
                          Break
                      End ! If Access:REPSCHLC.Next()
                      If rpl:REPSCHCRRecordNumber <> rpc:RecordNumber
                          Break
                      End ! If rpc:REPSCHCRRecordNumber <> rpc:RecordNumber
                      glo:Pointer = rpl:Location
                      Add(glo:Queue)
                  End ! Loop
              End ! If rpc:AllLocations
  
              Number_Of_Days = rpc:NumberOfDays
  !            Case rpc:DateRangeType
  !            Of 1 ! Today Only
  !                loc:StartDate = Today()
  !                loc:EndDate = Today()
  !            Of 2 ! 1st Of Month
  !                loc:StartDate = Date(Month(Today()),1,Year(Today()))
  !                loc:EndDate = Today()
  !            Of 3 ! Whole Month
  !                loc:StartDate = Date(Month(Today()) - 1, 1, Year(Today()))
  !                loc:EndDate = Date(Month(Today()),1,Year(Today())) - 1
  !            End ! Case rpc:DateRangeType
  
              !TB13266 - J - 03/04/14 Run the report for the last Number_Of_Days
              param:StartDate = today() - Number_Of_Days
              PartsUsageReport(param:StartDate,Number_Of_Days)
              Post(Event:CloseWindow)
          Else ! If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
              !Error
          End ! If Access:REPSCHCR.TryFetch(rpc:ReportCriteriaKey) = Level:Benign
  
      Else ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
          !Error
      End ! If Access:REPSCHED.TryFetch(rpd:RecordNumberKey) = Level:Benign
  
  End ! If Command('/SCHEDULE')
  ! End (DBH 31/08/2007) #9125
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:DEFAULTS.Close
    Relate:JOBS.Close
    Relate:LOCATION_ALIAS.Close
    Relate:ORDSTOCK.Close
    Relate:REPSCHCR.Close
    Relate:RETSALES.Close
    Relate:STOCK_ALIAS.Close
    Relate:TRADEACC_ALIAS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OkButton
    !TB13266 - J - 03/04/14 Run the report for the last Number_Of_Days
    param:StartDate = today() - Number_Of_Days
    PartsUsageReport(param:StartDate,Number_Of_Days)

    If Command('/SCHEDULE')
        If Access:REPSCHLG.PrimeRecord() = Level:Benign
            rlg:REPSCHEDRecordNumber = rpd:RecordNumber
            rlg:Information = 'Report Name: ' & Clip(rpd:ReportName) & ' - ' & Clip(rpd:ReportCriteriaType) & |
                              '<13,10>Report Finished'
            If Access:REPSCHLG.TryInsert() = Level:Benign
                !Insert
            Else ! If Access:REPSCHLG.TryInsert() = Level:Benign
                Access:REPSCHLG.CancelAutoInc()
            End ! If Access:REPSCHLG.TryInsert() = Level:Benign
        End ! If Access.REPSCHLG.PrimeRecord() = Level:Benign
    End

    Post(Event:CloseWindow)
    OF ?CancelButton
       POST(Event:CloseWindow)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LocationDASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?LocationDASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?LocationDASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?LocationDASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?LocationDASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          param:StartDate = TINCALENDARStyle1(param:StartDate)
          Display(?param:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendarEnd
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          param:EndDate = TINCALENDARStyle1(param:EndDate)
          Display(?param:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeEvent()
    ex.TakeEvent ('', '')
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?LocationList
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?LocationDASTAG)
         ! CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?param:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?param:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendarEnd)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?LocationList
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?LocationList{PROPLIST:MouseDownRow} > 0) 
        CASE ?LocationList{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::6:TAGMOUSE = 1
            POST(EVENT:Accepted,?LocationDASTAG)
               ?LocationList{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
  Of Event:AlertKey
      If KeyCode() = 0473H
      cycle
      End !If KeyCode() = ALTF4 = 0473H
      if KeyCode() = 001BH
          Cycle
      END !if keycode() = Esc = 001BH
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

!---------------------------------------------------------------------------------
ex.Init   PROCEDURE (byte pStartVisible=1, byte pEnableEvents=0)
ReturnValue   byte
  CODE
  ReturnValue = PARENT.Init (pStartVisible,pEnableEvents)
  self.TakeSnapShotOfWindowPos()
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
ex.Kill   PROCEDURE (byte pUnloadCOM=1)
ReturnValue   byte
  CODE
  self.RestoreSnapShotOfWindowPos()
  ReturnValue = PARENT.Kill (pUnloadCOM)
  Return ReturnValue
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
ex.TakeEvent   PROCEDURE (string pEventString1, string pEventString2, long pEventNumber=0, byte pEventType=0, byte pEventStatus=0)
  CODE
  PARENT.TakeEvent (pEventString1,pEventString2,pEventNumber,pEventType,pEventStatus)
  if pEventType = 0  ! Generated by CapeSoft Office Inside
    case event()
      of event:accepted
        case field()
      end
    end
  end
Bryan.CompFieldColour        Procedure()
DBHControl      Long()
Code
    Loop DBHControl = FirstField() To LastField()
        Case DBHControl{prop:Type}
            Of Create:Entry OrOf Create:Text OrOf Create:Spin
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} < 0
                            DBHControl{prop:Color} = 080FFFFH
                        !End ! If DBHControl{prop:Color} < 0
                    Else ! If DBHControl{prop:Req} = 1
                        !If DBHControl{prop:Color} = < 0
                            DBHControl{prop:Color} = color:White
                        !End ! If DBHControl{prop:Color} = < 0
                    End ! If DBHControl{prop:Req} = 1
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:Combo OrOf Create:DropList
                If DBHControl{prop:ReadOnly} = 1
                    !If DBHControl{Prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    !End ! If DBHControl{Prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    !If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    !End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                !If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                !End ! If DBHControl{prop:Color,2} < 0
                !If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                !End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:List
                If DBHControl{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:Silver
                    End ! If DBHControl{prop:Color} < 0
                Else ! If ReqControl#{prop:ReadOnly} = 1
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = color:White
                    End ! If DBHControl{prop:Color} < 0
                End ! If ReqControl#{prop:ReadOnly} = 1
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = 01010101H
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,2} < 0
                    DBHControl{prop:Color,2} = color:White
                End ! If DBHControl{prop:Color,2} < 0
                If DBHControl{prop:Color,3} < 0
                    DBHControl{prop:Color,3} = 09A6A7CH
                End ! If DBHControl{prop:Color,3} < 0
                DBHControl{prop:FontStyle} = font:Regular
            Of Create:Group OrOf Create:Option
                If DBHControl{prop:Text} = 'Top Tip'
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 010101H
                    End ! If DBHControl{prop:FontColor} < 0
                    IF DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 0D6E7EFH
                    End ! IF DBHControl{prop:Color} < 0
                    DBHControl{prop:FontStyle} = font:Regular
                Else ! If DBHControl{prop:Use} = '?GroupTip'
                    If DBHControl{prop:Color} < 0
                        DBHControl{prop:Color} = 09A6A7CH
                    End ! If DBHControl{prop:Color} < 0
                    If DBHControl{prop:FontColor} < 0
                        DBHControl{prop:FontColor} = 080FFFFH
                    End ! If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontStyle} = font:Bold
                End ! If DBHControl{prop:Use} = '?GroupTip'

            Of Create:Prompt OrOf Create:Radio OrOf Create:Check
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                DBHControl{prop:FontStyle} = font:Bold
            Of Create:String Orof Create:SString
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color} < 0
                    DBHControl{prop:Color} = 09A6A7CH
                End ! If DBHControl{prop:Color} < 0
                DBHControl{prop:FontStyle} = font:Bold
                DBHControl{prop:trn} = True
            Of Create:Sheet Orof Create:Tab
                DBHControl{prop:FontStyle} = font:Bold
                If DBHControl{prop:FontColor} < 0
                    DBHControl{prop:FontColor} = color:White
                End ! If DBHControl{prop:FontColor} < 0
                If DBHControl{prop:Color,1} < 0
                    DBHControl{prop:Color,1} = 09A6A7CH
                End ! If DBHControl{prop:Color,1} < 0
            Of Create:Panel
                If DBHControl{prop:Fill} < 0
                    DBHControl{prop:Fill} = 09A6A7CH
                End ! If DBHControl{prop:Fill} < 0
        End ! Case ReqControl#{prop:Type}
    End ! Loop ReqControl# = FirstField() To LastField()
    Display()

BRWLocation.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = loc:Location
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      LocationTag = ''
    ELSE
      LocationTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (locationtag = '*')
    SELF.Q.LocationTag_Icon = 2
  ELSE
    SELF.Q.LocationTag_Icon = 1
  END


BRWLocation.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRWLocation.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRWLocation.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW1::RecordStatus=ReturnValue
  IF BRW1::RecordStatus NOT=Record:OK THEN RETURN BRW1::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = loc:Location
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::6:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW1::RecordStatus
  RETURN ReturnValue

